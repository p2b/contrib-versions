Italian English Bulgarian Croatian Czech Danish Dutch Estonian Finnish French German Greek Hungarian Italian Latvian Lithuanian Polish Portuguese Romanian Slovak Slovenian Spanish Swedish Maltese Irish

English versions supersede any translations in the event of any ambiguity.

**Termini di servizio**
-----------------------

**Ultima modifica: 02 maggio 2024**

_**NOTA:**La formulazione originale è redatta in lingua inglese e l'eventuale traduzione viene fornita esclusivamente a scopo informativo. In caso di conflitto tra la versione in lingua inglese e la traduzione della stessa, prevarrà la lingua inglese._

**TERMINI DI SERVIZIO SINTESI**
-------------------------------

Il seguente riepilogo fornisce una panoramica per tua comodità dei termini più importanti di Pornhub che regolano l'utilizzo di Pornhub ([www.pornhub.com](http://www.pornhub.com/), "il sitoweb"). Questo è un riepilogo non vincolante dei Termini di servizio applicabili del Sito, che copre aspetti selezionati dello stesso in modo semplificato. Non sostituisce i nostri Termini di servizio vincolanti, che includono le nostre politiche e linee guida ("i nostri Termini"). In tutti i casi, prevalgono i nostri Termini.

_**NOTA :**Eventuali termini in maiuscolo non definiti nel presente documento avranno il significato ad essi attribuito nei Termini._

1. Nostro[**Termini**](https://www.pornhub.com/information/terms)contengono i termini e le condizioni che regolano l'utilizzo e l'accesso a Pornhub, i suoi Contenuti, funzionalità e servizi e il tuo rapporto contrattuale con noi, Aylo Freesites Ltd, Block 1, 195-197 Old Nicosia-Limassol Road, Dali Industrial Zone , 2540, Nicosia, Cipro0, in qualità di operatore di Pornhub (“noi”, “ci”). Affrontano, tra gli altri, i seguenti punti:
    

1. **Accettazione**: Accedendo, utilizzando o visitando il sito Web e/o qualsiasi dei suoi contenuti, funzionalità e servizi, comunichi il tuo accordo con i nostri Termini.
    
2. **Terminazione:**Puoi risolvere i nostri Termini in qualsiasi momento cancellando il tuo account e astenendoti dall'ulteriore utilizzo dei nostri servizi.
    
3. **Età e restrizioni:**Dichiari di avere almeno diciotto (18) anni o la maggiore età nella giurisdizione da cui accedi al sito web. Dichiari inoltre che la giurisdizione da cui accedi al sito Web non vieta la ricezione o la visualizzazione di contenuti sessualmente espliciti.
    
4. **I cambiamenti:**Ci riserviamo il diritto di ritirare o modificare il sito Web e qualsiasi servizio o materiale che forniamo sul sito Web a nostra esclusiva discrezione e senza preavviso. Potremmo modificare i nostri Termini. In tal caso, ti forniremo un preavviso ragionevole in modo trasparente. Salvo diversamente specificato da noi, si riterrà che tu abbia accettato tali modifiche a meno che non ci comunichi, per iscritto e prima della data di entrata in vigore delle modifiche, che non sei d'accordo. Se non accetti le modifiche, non potrai più utilizzare il sito web dopo l'entrata in vigore delle modifiche proposte. In questo caso, tu o noi potremo risolvere i presenti Termini con effetto immediato.
    
5. **Contenuti di terze parti:**Quando utilizzi il sito web, potresti essere esposto a contenuti provenienti da una varietà di fonti per le quali non siamo responsabili. Inoltre, non ci assumiamo alcuna responsabilità per i siti Web di terzi a cui è possibile accedere tramite collegamenti trovati, inseriti o altrimenti visualizzati sul sito Web.
    
6. **Comunicazione e punto di contatto unico:**Registrandoti come Utente su questo Sito (e utilizzando lo stesso), acconsenti espressamente e specificatamente a ricevere da noi comunicazioni elettroniche relative al tuo account. Pornhub fornisce un unico punto di contatto in conformità con il Digital Services Act (DSA), che puoi utilizzare per comunicazioni su argomenti specifici del DSA:[dsa@pornhub.com](mailto:dsa@pornhub.com).
    
7. **Licenza:**Ti concediamo una licenza condizionale, revocabile, non cedibile in sublicenza, non trasferibile, non esclusiva e limitata per accedere e utilizzare il nostro sito Web esclusivamente per uso personale, a condizione che tu rispetti i nostri Termini. È necessario visualizzare il sito Web nella sua interezza e come presentato dal suo host.
    
8. **Regole per contenuti e uploader:**Il sito Web è destinato a contenuti per adulti. Alcuni Contenuti possono essere caricati solo dopo un processo di verifica o dopo aver aderito a un programma partner
    
    Dichiari e garantisci che, rispetto a tutti i Contenuti che puoi caricare su questo sito Web:
    
    \-Il Contenuto non deve violare alcuna legge applicabile o ledere i diritti di altri.
    
    \-Hai ottenuto il consenso e il rilascio per ogni individuo che appare nei tuoi Contenuti.
    
    \-Hai verificato che ogni individuo che appare nei tuoi Contenuti avesse almeno diciotto (18) anni al momento della produzione e hai raccolto, esaminato e mantenuto una registrazione sufficiente di questa verifica dell'età.
    
    Nessun individuo visualizzato nei tuoi Contenuti è stato condannato per crimini relativi alla violenza sessuale, allo sfruttamento sessuale di minori, alla tratta e ad altri reati correlati.
    
    Il Contenuto è conforme ai nostri Termini. Il Contenuto non contiene né comprende, ad esempio: materiale pedopornografico (CSAM), contenuto non consensuale (NCC), vendetta porno, ricatto, intimidazione, contenuto fraudolento, contenuto fuorviante e gioco d'azzardo.
    
    Inoltre, ci sono ulteriori requisiti nei nostri Termini, inclusi termini speciali per modelli e partner di contenuti, che devono essere accettati aderendo al nostro programma di partner di modelli o contenuti.
    
9. **Uso vietato:**Tutti i contenuti caricati e l'utilizzo del sito Web devono essere legali e seguire i nostri Termini e devono, in particolare, rispettare la sezione "Usi vietati e segnalazioni" dei nostri Termini.
    
10. **Segnalazione**: gli utenti sono invitati a segnalare i contenuti che sospettano siano illegali o in violazione dei nostri Termini.
    
11. **Monitoraggio e applicazione; Terminazione:**Abbiamo il diritto di far rispettare i nostri Termini attraverso le misure appropriate che riteniamo appropriate, che possono includere:
    
    \-Avvertimenti scritti
    
    \-Limitazione della visibilità o dell'accessibilità dei Contenuti, dell'accesso ai nostri servizi, della monetizzazione, dei guadagni e dei pagamenti
    
    \-Segnalazione alle forze dell'ordine
    
    Puoi presentare un reclamo entro 6 mesi dalla notifica della nostra decisione. Non puoi abusare di questa procedura. Se ritieni che qualsiasi Contenuto violi il tuo diritto d'autore, puoi anche fare riferimento al nostro[Politica sul diritto d'autore](https://www.pornhub.com/information/dmca)per istruzioni su come inviarci un avviso di violazione del copyright.
    
12. **Informazioni su di te e sulle tue visite:**Tutte le informazioni che raccogliamo sul sito Web sono soggette alla nostra[Informativa sulla Privacy.](https://www.pornhub.com/information/privacy)Il Cookie Banner può essere utilizzato per personalizzare le tue preferenze sui cookie. Puoi accedere al Cookie Banner in qualsiasi momento facendo clic sull'opzione "Gestisci cookie" che si trova a piè di pagina del sito web.
    
13. **Raccolta e utilizzo delle informazioni sull'utilizzo da parte di inserzionisti e altri:**Terze parti che inseriscono pubblicità su questo sito Web potrebbero avere la possibilità di utilizzare cookie e/o web beacon per raccogliere informazioni, comprese informazioni sull'utilizzo di questo sito Web. Non controlliamo i processi che gli inserzionisti possono utilizzare per raccogliere tali informazioni.
    
14. **Collegamento a questo sito Web e alle funzionalità dei social media:**È possibile collegarsi a questo sito Web e utilizzare determinate funzionalità dei social media, ma sempre soggetti ai Termini.
    
15. **Raccolta e divulgazione consentita di informazioni personali:**Questo sito Web non divulgherà alcuna informazione di identificazione personale che raccogliamo o otteniamo, tranne nei casi espressamente previsti nei nostri Termini.
    
16. **Indennizzo:**Nella misura consentita dalla legge applicabile, l'utente accetta di difendere, indennizzare e tenere indenne questo sito Web, noi come suo operatore e altri come dettagliato nei Termini.
    
17. **Disclaimer:**L'utilizzo del sito Web è a tuo esclusivo rischio. Forniamo il sito web “così com’è” e “come disponibile”. Ulteriori dettagli sono forniti nei nostri Termini.
    
18. **Limitazione di responsabilità:**La nostra responsabilità è limitata, come previsto nei nostri Termini.
    
19. **Limitazione del tempo per presentare reclami:**Qualsiasi causa di azione o reclamo deve essere avviata entro un massimo di un (1) anno dalla causa.
    
20. **Arbitrato e rinuncia a determinati diritti (USA):**Gli utenti situati negli Stati Uniti d'America risolveranno eventuali controversie tra di noi tramite un arbitrato vincolante e finale anziché tramite procedimenti giudiziari, come previsto più dettagliatamente nei nostri Termini.
    
21. **Arbitrato e rinuncia ad alcuni diritti (UE):**Se tu o il tuo luogo di stabilimento siete nell'Unione Europea, potreste avere il diritto di selezionare un organismo di conciliazione per assisterci nella risoluzione di eventuali controversie con noi. Non siamo né disposti né obbligati a partecipare a procedimenti di risoluzione delle controversie con i consumatori dinanzi a un collegio arbitrale per i consumatori ai sensi della Direttiva UE sull'ADR per i consumatori.
    
22. **Legge e giurisdizione:**I nostri Termini, l'utilizzo di questo sito Web e il rapporto tra te e noi saranno regolati dalle leggi della Repubblica di Cipro. L'unica ed esclusiva giurisdizione e sede per qualsiasi azione o procedimento derivante da o correlato ai nostri Termini sarà un tribunale competente situato nella Repubblica di Cipro.
    
23. **Terminazione:**Possiamo risolvere i nostri Termini per qualsiasi motivo o senza motivo in qualsiasi momento avvisandoti tramite un avviso su questo sito Web, tramite e-mail o tramite qualsiasi altro metodo di comunicazione.
    
    È possibile trovare i Termini di servizio completi [Qui](https://www.pornhub.com/information/terms) e ti consigliamo di leggerli prima di utilizzare il sito Web e accedere a qualsiasi suo contenuto, funzionalità e servizio.
    

2. [**Conformità**](https://help.pornhub.com/hc/en-us/articles/4419839721875-What-sort-of-content-is-not-allowed-on-the-site)**:**Questa sezione fornisce ulteriori informazioni sul tipo di contenuto vietato sul sito web. Fornisce inoltre collegamenti utili aNostro[Linee guida della comunità](https://help.pornhub.com/hc/en-us/articles/4419900587155-Community-Guidelines), nonché ad altre informazioni esplicative ed utili di cui devi essere a conoscenza quando utilizzi il Sito. 
    
3. [**Richiesta di rimozione di contenuti anonimi:**](https://www.pornhub.com/anonymous-content-removal) Questo è il modulo con il quale puoi segnalarci in modo anonimo qualsiasi Contenuto che potresti sospettare violi la nostra[Politica contro la lotta contro la violenza domestica](https://help.pornhub.com/hc/en-us/articles/4419869793683-Child-Sexual-Abuse-Material-Policy). Inoltre, fornisce il collegamento al file general[Richiesta di rimozione dei contenuti](https://www.pornhub.com/content-removal), nonché collegamenti a politiche e linee guida selezionate per assisterti nel completamento della tua richiesta.
    
4. [**Linee guida del sistema di raccomandazione**](https://www.pornhub.com/information/recommender): questa politica descrive il modo in cui adattiamo i consigli agli Utenti, dando priorità alla loro privacy e sicurezza, nonché al rispetto delle leggi applicabili.
    
5. [**Fiducia e sicurezza**](https://help.pornhub.com/hc/en-us/categories/4419836212499)**:**Questa sezione include le politiche e le linee guida applicabili che tutti gli Utenti dovrebbero rispettare, inclusi, ma non limitati a:
    
    a. **La comunità di Pornhub:**
    
    * [Visualizza frode](https://help.pornhub.com/hc/en-us/articles/4419892571155-View-Fraud):Questa sezione delinea la nostra posizione sulle visualizzazioni fraudolente, sottolineando che l'acquisto o la falsificazione delle visualizzazioni viola i nostri Termini. Funziona anche come avvertimento per gli Utenti di non impegnarsi o partecipare a servizi che offrono incrementi di visualizzazione falsi che potrebbero comportare gravi conseguenze, tra cui la chiusura dell'account e il ban permanente, nonché la demonetizzazione al primo evento e l'esilio permanente con perdita dei guadagni al secondo. occorrenza.
        
    * [Iniziative per la fiducia e la sicurezza](https://help.pornhub.com/hc/en-us/articles/19159382668435-Trust-and-Safety-Initiatives):Questa sezione fornisce un elenco delle misure e iniziative che adottiamo per prevenire e rispondere ai contenuti illegali sul sito Web e per garantire la sicurezza degli utenti.
        
    * [Valori fondamentali](https://help.pornhub.com/hc/en-us/articles/4419871944723-Core-Values): questa sezione fornisce un elenco completo dei nostri valori fondamentali, nonché linee guida su "cosa fare" e "non fare" quando si utilizza il sito Web.
        
    
    b. **Politiche e linee guida correlate:**Le politiche e le linee guida seguenti illustrano la nostra posizione contro diverse azioni proibite, che tutti gli Utenti dovrebbero rispettare quando utilizzano il Sito web. Il mancato rispetto delle politiche e delle linee guida riportate di seguito potrebbe portare a conseguenze negative tra cui, tra l'altro, la chiusura del tuo account.
    

* [Politica sui materiali pedopornografici](https://help.pornhub.com/hc/en-us/articles/4419869793683-Child-Sexual-Abuse-Material-Policy)
    
* [Politica sui contenuti non consensuali](https://help.pornhub.com/hc/en-us/articles/4419871787027-Non-Consensual-Content-Policy)
    
* [Politica sui contenuti violenti](https://help.pornhub.com/hc/en-us/articles/4419863430291-Violent-Content-Policy)
    
* [Politica sull’incitamento all’odio](https://help.pornhub.com/hc/en-us/articles/14512634908819-Hate-Speech-Policy)
    
* [Politica sul benessere degli animali](https://help.pornhub.com/hc/en-us/articles/7906741373971-Animal-Welfare-Policy)
    
* [Diritto d'autore](https://help.pornhub.com/hc/en-us/articles/4419861225107-Copyright)
    
* [Linee guida della comunità](https://help.pornhub.com/hc/en-us/articles/4419900587155-Community-Guidelines)
    

   C. **Segnalazione di abusi e violazioni:**

* [Segnala contenuti dannosi](https://help.pornhub.com/hc/en-us/articles/4419875025043-Report-Harmful-Content):Fornisce informazioni e indicazioni su come segnalarci i seguenti contenuti in violazione:
    
    * video, foto o GIF; O
        
    * Commenti.
        
* [Programma di segnalazione affidabile di Pornhub](https://help.pornhub.com/hc/en-us/articles/4419879221907-Pornhub-Trusted-Flagger-Program):Questa sezione fornisce informazioni sul programma di segnalazione attendibile sviluppato da Pornhub per fornire un modo solido ed efficiente alle organizzazioni non governative (ONG) e agli uffici statutari per segnalarci qualsiasi Contenuto che sia in potenziale violazione della legge applicabile e/o dei nostri Termini. Fornisce inoltre informazioni su come gestiremo tali segnalazioni e su come partecipare al programma (se sei idoneo).
    

**TERMINI DI SERVIZIO COMPLETI**
--------------------------------

**Accettazione dei Termini di servizio**

Accedendo, utilizzando o visitando [www.pornhub.com](http://www.pornhub.com/) **("noi",** **"ci","noi"** o questo **"Sito Web"),** uno qualsiasi dei suoi contenuti, funzionalità e servizi, l'utente firma il suo consenso a questi Termini di servizio, comprese le politiche e le linee guida correlate (ad esempio, la politica sui materiali per gli abusi sessuali sui minori e l'Informativa sui contenuti non consensuali) (collettivamente i **"Termini"** o **"Termini di servizio"),** e la nostra [Informativa sulla privacy](https://www.pornhub.com/information/privacy) qui incorporata.

 Puoi risolvere i presenti Termini di servizio in qualsiasi momento eliminando il tuo account e astenendoti dall'ulteriore utilizzo dei nostri servizi.

 I presenti Termini di servizio si applicano a tutti gli utenti, inclusi partner di contenuti, modelli e caricatori verificati che possono anche essere contributori del Contenuto, di questo sito Web (collettivamente **"utente","Utente"** o " **Utenti"** come richiesto dal contesto), sia che si accedi tramite computer, dispositivo mobile o altra tecnologia, modo o mezzo.

 **"Contenuto"** include il testo, il software, gli script, la grafica, le foto, i suoni, la musica, i video, le combinazioni audiovisive, le funzionalità interattive, i contenuti testuali e altri materiali che puoi visualizzare, caricare, pubblicare, inviare, rendere disponibile, visualizzare, comunicare o pubblicare su questo sito Web.

 **“Contenuti** partner” indica gli utenti di questo sito Web che hanno aderito al nostro [programma Partner di contenuti.](https://www.pornhub.com/partners/cpp)

 **Models**“Modelli” indica gli Utenti di questo Sito Web che hanno aderito al nostro [Programma Partner Modello](https://www.pornhub.com/partners/models) e sono soggetti ai presenti [Termini di servizio.](https://www.pornhub.com/information/terms)

 “ **Caricatori** verificati” indica gli Utenti di questo Sito Web che hanno un account identificato validamente.

 Se non si accettano nessuno dei presenti Termini di servizio o della nostra [Informativa sulla privacy](https://www.pornhub.com/information/privacy), si prega di non accedere o utilizzare questo sito Web.

 L'utente acconsente a inserire i presenti Termini di servizio elettronicamente e di archiviare i registri relativi ai presenti Termini di servizio in forma elettronica.

 **Capacità di accettare i Termini di servizio**

L'utente afferma di avere almeno 18 anni di età o la maggiore età nella giurisdizione da cui si accede a questo sito Web e sono pienamente in grado e competenti di stipulare i termini, le condizioni, gli obblighi, le affermazioni, le dichiarazioni e le garanzie stabiliti in questi Termini di servizio e di rispettare e rispettare i presenti Termini di servizio. Se hai meno di 18 anni o la maggiore età applicabile, ti preghiamo di non utilizzare questo sito web. Dichiari inoltre che la giurisdizione di cui accedi a questo sito Web non vieta la ricezione o la visualizzazione di contenuti sessualmente espliciti. Sia che tu sia un utente registrato o non registrato, potremmo chiederti di fornire a noi e / o ai nostri fornitori di servizi di verifica dell'età di terze parti informazioni che ci aiuteranno a determinare che hai più della maggiore età richiesta per avere accesso a questo sito Web e a visualizzare i suoi contenuti. Per ulteriori informazioni su come vengono elaborate queste informazioni, si prega di consultare la nostra Informativa sulla privacy. La mancata, quando richiesto, di fornire a noi e / o ai nostri fornitori di servizi di verifica dell'età di terze parti informazioni che ci aiuteranno a determinare che hai più della maggiore età richiesta per avere accesso a questo sito Web e a visualizzare i suoi contenuti porterà alla cessazione o alla sospensione dei tuoi diritti di accesso a tutto o parte di questo sito Web.

 **Modifiche ai Termini di Servizio**

Possiamo modificare questi Termini di servizio, trovati all'indirizzo [https://www.pornhub.com/information/terms](https://www.pornhub.com/information/terms), di tanto in tanto. In caso di utilizzo, ti daremo un preavviso ragionevole e anticipato in modo trasparente, in forma di testo, ad esempio tramite una notifica pop-up sul sito Web quando si accede al tuo account o via e-mail. Salvo diversa comunicazione da parte nostra, ti si riterrà che tu abbia accettato le modifiche a meno che tu non ci informi, per iscritto e prima della data di entrata in vigore proposta delle modifiche, che non sei d'accordo.

 Se non si accettano le modifiche, non è più possibile utilizzare il nostro sito Web dopo l'entrata in vigore delle modifiche proposte. In questo caso, voi o noi possiamo risolvere questi Termini di servizio con effetto immediato.

 Ogni versione aggiornata dei presenti Termini di servizio sostituisce qualsiasi versione precedente alla data "Ultima modifica" che si trova in alto e qualsiasi versione precedente non avrà alcun effetto legale continuo.

 **A proposito di questo sito**

Questo sito Web consente la visualizzazione generale dei Contenuti orientati agli adulti da parte degli Utenti, registrati e non registrati. Inoltre, questo sito Web consente il caricamento di contenuti per adulti da parte di partner di contenuti, caricatori verificati e modelli.

 Questo sito Web può contenere collegamenti a siti di terzi che non sono di proprietà o controllati da questo sito Web o dal suo gestore. Questo sito Web non ha alcun controllo e non si assume alcuna responsabilità per il contenuto, le politiche sulla privacy o le pratiche di qualsiasi sito di terze parti. Inoltre, questo sito Web non e non può censurare o modificare il contenuto di qualsiasi sito di terze parti. Utilizzando uno qualsiasi di questo sito Web, ci esonali espressamente da ogni responsabilità derivante dall'utilizzo di siti di terze parti. Di conseguenza, ti invitiamo a essere consapevole quando lasci questo sito Web e a leggere i termini, le condizioni e le politiche sulla privacy degli altri siti che visiti.

Questo sito Web è per uso personale e non deve essere utilizzato per qualsiasi attività commerciale ad eccezione di quelle specificamente approvate o approvate da questo sito Web.

 Questo sito web è per i contenuti per adulti. Altre categorie di Contenuti possono essere rifiutate o cancellate a nostra esclusiva discrezione. Comprendi e riconosci che quando utilizzi questo sito Web, sarai esposto al Contenuto da una varietà di fonti e che questo sito Web non è responsabile per l'accuratezza, l'utilità, la sicurezza o i diritti di proprietà intellettuale o relativi a tali Contenuti. Comprendi e riconosci inoltre che potresti essere esposto a Contenuti inaccurati, offensivi, indecenti o discutibili, e accetti di rinunciare, e con la presente rinuncia a qualsiasi diritto o rimedio legale o equo che hai o potresti avere contro questo sito Web in relazione, e accetti di indennizzare e tenere indenne questo sito Web, il suo operatore, la sua società madre, le sue affiliate, i licenziatari, i fornitori di servizi, i funzionari, i direttori, i dipendenti, gli agenti, i successori e i successivi.

 **Preferenze di comunicazione / Punto unico di contatto**

Utilizzando e registrandosi come utente su questo sito Web, l'utente acconsente espressamente e specificamente a ricevere comunicazioni elettroniche da noi relative al proprio account. Queste comunicazioni possono comportare l'invio di e-mail al tuo indirizzo e-mail fornito durante la registrazione, o la pubblicazione di comunicazioni su questo sito Web (ad esempio, attraverso l'area dei membri su questo sito Web al momento del login per garantire la ricezione nel caso in cui tu abbia disiscrizione dalle comunicazioni e-mail), o nella pagina "Il mio account" o "Il mio profilo" e possa includere avvisi sul tuo account (come la modifica della password o e-mail di conferma) e che fanno parte del tuo rapporto con noi. L'utente accetta che eventuali avvisi, accordi, divulgazioni o altre comunicazioni che ti inviamo elettronicamente soddisferanno qualsiasi requisito di comunicazione legale, incluso che tali comunicazioni siano in forma scritta. È necessario conservare copie di comunicazioni elettroniche stampando una copia cartacea o salvando una copia elettronica.

 Conformemente agli articoli 11 e 12 del regolamento (UE) 2022/2065 (la **"Legge sui servizi digitali"** o **“DSA”**"DSA"), [dsa-pornhub.com](mailto:dsa@pornhub.com) è il punto di contatto unico designato per le comunicazioni con i destinatari dei servizi qui offerti, nonché per le autorità degli Stati membri dell'Unione europea, la Commissione e il Consiglio per i servizi digitali. Si prega di condurre tutte le comunicazioni in inglese o greco.

Per qualsiasi ordine di rimozione ai sensi del Regolamento (UE) 2021/784 (il "Regolamento sui contenuti terroristici online" o "TCO"), [le autorità competenti dell'UE designate](https://home-affairs.ec.europa.eu/policies/internal-security/counter-terrorism-and-radicalisation/prevention-radicalisation/terrorist-content-online/list-national-competent-authority-authorities-and-contact-points) possono compilare il nostro [modulo di rimozione](https://www.pornhub.com/tco-removal). Dopo l'invio di questo modulo, riceverai ulteriori istruzioni via e-mail, alle quali potrai rispondere con un ordine di rimozione. Per tali ordini di rimozione, si prega di utilizzare il modello fornito nell'[allegato I del TCO](https://eur-lex.europa.eu/eli/reg/2021/784#:~:text=p.%C2%A0385).-,ANNEX%20I,-REMOVAL%20ORDER) e di effettuare tutte le comunicazioni in inglese o in greco.

**Accedere a questo sito Web e all'account Security**

Ci riserviamo il diritto di ritirare o modificare questo sito Web, e qualsiasi servizio o materiale che forniamo su questo sito Web, a nostra esclusiva discrezione senza preavviso. Non saremo responsabili se per qualsiasi motivo tutto o parte di questo sito Web non è disponibile in qualsiasi momento o per qualsiasi periodo. Di tanto in tanto, potremmo limitare l'accesso ad alcune parti di questo sito Web, o all'intero sito Web, agli utenti, inclusi gli utenti registrati.

 Sei responsabile di:

 prendere tutte le disposizioni necessarie per avere accesso a questo sito Web, e

* garantire che tutte le persone che accedono a questo sito Web tramite la connessione Internet siano a conoscenza di questi Termini di servizio e li rispettino.

 Per accedere a questo sito Web o ad alcune delle risorse che offre, potrebbe essere richiesto di fornire alcuni dettagli di registrazione o altre informazioni. Per saperne di più sulle informazioni richieste per utilizzare questo sito Web per caricare il Contenuto, si prega di fare riferimento alle sezioni intitolate ["Regole applicabili a tutti i contenuti](https://www.pornhub.com/information/terms#rulesForCU) [e caricatori su questo sito Web",](https://www.pornhub.com/information/terms#rulesForCU) ["Regole applicabili ai caricatori verificati",](https://www.pornhub.com/information/terms#rulesForVU) ["Regole applicabili ai](https://www.pornhub.com/information/terms#rulesForCP) [partner di contenuti"](https://www.pornhub.com/information/terms#rulesForCP) e ["Regole applicabili ai modelli",](https://www.pornhub.com/information/terms#rulesForModels) nonché [all'Informativa sulla privacy](https://www.pornhub.com/information/privacy). È una condizione per l'utilizzo di questo sito Web che tutte le informazioni fornite su questo sito Web sono corrette, aggiornate e complete. Accetti che tutte le informazioni fornite per registrarti o in altro modo, incluso ma non limitato all'uso di funzionalità interattive su questo sito Web, siano regolate dalla nostra [Informativa sulla privacy](https://www.pornhub.com/information/privacy) e acconsenti a tutte le azioni che inseriamo in relazione alle tue informazioni coerenti con la nostra [Informativa sulla privacy.](https://www.pornhub.com/information/privacy)

 Se scegli o ti viene fornita un nome utente, una password o qualsiasi altra informazione come parte delle nostre procedure di sicurezza, devi trattare tali informazioni come riservate e non devi divulgarle a nessun'altra persona. Sei pienamente responsabile di tutte le attività che si verificano sotto il tuo nome utente o password. Riconosci inoltre che il tuo account è personale nei tuoi confronti e accetti di non fornire a nessun'altra persona l'accesso a questo sito Web o parti di questo sito Web utilizzando il tuo nome utente, la password o altre informazioni di sicurezza. Accetti di informarci immediatamente di qualsiasi accesso non autorizzato o utilizzo del tuo nome utente o password o qualsiasi altra violazione della sicurezza contattandoci [all'utente all'utente all'utente](mailto:support@pornhub.com). Accetti inoltre di assicurarti di uscire dal tuo account al termine di ogni sessione. Dovresti usare particolare cautela quando accedi al tuo account da un computer pubblico o condiviso in modo che altri non siano in grado di visualizzare o registrare la tua password o altre informazioni personali. Sebbene questo sito Web non sarà responsabile per le perdite causate da qualsiasi uso non autorizzato del tuo account, potresti essere responsabile per le perdite di questo sito Web o di altri a causa di tale uso non autorizzato.

 Se interagisci con noi o con fornitori di servizi di terze parti, accetti che tutte le informazioni fornite sono e saranno accurate, complete e aggiornate. Esaminerai tutte le politiche e gli accordi applicabili all'uso dei servizi di terze parti. Nel caso in cui utilizzi il nostro sito Web su dispositivi mobili, l'utente riconosce che le normali tariffe e le tariffe del tuo operatore, come le tariffe in eccesso per la banda larga, saranno ancora applicate.

 Abbiamo il diritto di disabilitare qualsiasi nome utente, password o altro identificatore, sia scelto da te o fornito da noi, in qualsiasi momento a nostra esclusiva discrezione per qualsiasi motivo, incluso se, a nostro avviso, hai violato qualsiasi disposizione di questi Termini di servizio.

 **Licenza limitata e condizionale per l'utilizzo della nostra proprietà intellettuale**

Pornhub e i loghi e i nomi associati sono i nostri marchi e/o marchi di servizio. Altri marchi, marchi di servizio, nomi e loghi utilizzati su o attraverso questo sito Web, come marchi, marchi di servizio, nomi o loghi associati a fornitori di contenuti di terze parti, sono marchi, marchi di servizio o loghi dei rispettivi proprietari. Non ti è concesso alcun diritto o licenza in relazione a uno qualsiasi dei suddetti marchi, marchi di servizio o loghi.

 L'inclusione di immagini o testo contenenti i marchi o i marchi di servizio o il nome o la somiglianza di qualsiasi persona, inclusa qualsiasi celebrità, non costituisce un'approvazione, espressa o implicita, da parte di tale persona, di questo sito Web o viceversa.

 Questo sito Web e alcuni materiali disponibili su o attraverso questo sito Web sono contenuti che abbiamo, creato, creato, acquistato o concesso in licenza (collettivamente, le nostre " **Opere").** Le nostre Opere possono essere protette da copyright, marchi, brevetti, segreti commerciali e / o altre leggi, e ci riserviamo e conserviamo tutti i diritti nelle nostre Opere e questo sito Web.

 Ti concediamo una licenza condizionale, revocabile, non sublicenziabile, non trasferibile e non esclusiva e limitata per accedere e utilizzare il nostro sito Web e le nostre Opere esclusivamente per uso personale, condizionato alla conformità con i presenti Termini di servizio e al tuo consenso a visualizzare questo sito Web intero e intatto come presentato dall'host di questo sito Web, completo di qualsiasi pubblicità, per non interferire con la visualizzazione di qualsiasi pubblicità e di non utilizzare il software di blocco degli annunci di qualsiasi tipo. Questa licenza limitata è inoltre a condizione che l'utente non utilizzi alcuna informazione ottenuta da o attraverso questo sito Web per bloccare o interferire con la visualizzazione di qualsiasi pubblicità su questo sito Web, o allo scopo di implementare, modificare o aggiornare qualsiasi software o elenco di elenchi che blocchi o interferiscano con la visualizzazione di qualsiasi pubblicità su questo sito Web. Interferenza con la visualizzazione di qualsiasi pubblicità su questo sito Web, l'uso di software di blocco degli annunci per bloccare o disabilitare qualsiasi pubblicità durante la visualizzazione di questo sito Web, o l'uso di informazioni ottenute da o attraverso questo sito Web per aggiornare qualsiasi software di blocco degli annunci o elenchi di filtri, è vietato, viola le condizioni della licenza limitata per visualizzare questo sito Web e opere e costituire violazione del copyright.

 Non è possibile riprodurre, distribuire, comunicare al pubblico, rendere disponibile, adattare, eseguire pubblicamente, collegare o visualizzare pubblicamente questo sito Web e opere o qualsiasi adattamento dello stesso, a meno che non sia espressamente stabilito nel presente documento. Tale condotta supererebbe la portata della licenza e costituirebbe una violazione del copyright.

 Questo sito Web può fornire una funzione "di giocatore incorporabile", che è possibile incorporare nel proprio sito Web per l'utilizzo nell'accesso al contenuto di questo sito Web. Non è possibile modificare, costruire o bloccare alcuna parte o funzionalità del giocatore incorporabile in alcun modo, incluso ma non limitato ai collegamenti a questo sito Web.

 La licenza di cui sopra è condizionata al rispetto di questi Termini di servizio, incluso, in particolare, il tuo consenso a visualizzare questo sito Web intero e intatto come presentato dall'host di questo sito Web, completo di qualsiasi pubblicità e terminerà alla risoluzione dei presenti Termini di servizio. In caso di violazione di qualsiasi disposizione dei presenti Termini di servizio, qualsiasi licenza che hai ottenuto verrà automaticamente annullata e terminata. Al fine di proteggere i nostri diritti, alcuni Contenuti resi disponibili su questo sito Web possono essere controllati da tecnologie di gestione dei diritti digitali, che limiteranno il modo in cui è possibile utilizzare il Contenuto. Non devi eludere, rimuovere, eliminare, disabilitare, alterare o altrimenti interferire con qualsiasi tecnologia di gestione dei diritti digitali. Tale comportamento è proibito dalla legge.

 Non è consentito riprodurre, scaricare, distribuire, trasmettere, trasmettere, visualizzare, vendere, concedere in licenza, alterare, modificare o altrimenti copiare o riprodurre le nostre Opere o Contenuti che non appartengono a te, in tutto o in parte.

 **Regole applicabili a tutti i contenuti e caricatori su questo sito**

Per caricare il Contenuto su questo sito Web, è necessario essere un caricatore verificato, un partner di contenuto o un modello, salvare e, salvo che tale Contenuto sia limitato ai commenti, a un'immagine di copertina e / o a un avatar. Se le informazioni che fornisci per la creazione del tuo account non sono vere e corrette, devi affrontare l'immediata chiusura del tuo account; se del caso, la sospensione immediata di tutti i pagamenti in sospeso da parte nostra a te; e possiamo, a nostra esclusiva discrezione, trattenere il pagamento di qualsiasi importo dovuto a tempo indeterminato e potresti essere soggetto a sanzioni legali.

L'utente dichiara e garantisce che in relazione a tutti i Contenuti si caricherà su questo sito Web:

1. il Contenuto non contravviene a nessuna legge applicabile e non sottopone questo sito Web a reclami, richieste, azioni legali, azioni normative o qualsiasi rischio effettivo, potenziale o rischio di responsabilità, o qualsiasi minaccia di ciò;
2. possiedi i diritti di utilizzare il Contenuto su questo sito Web e che il Contenuto non violi i diritti (inclusi i diritti di proprietà intellettuale) di terzi e che tu abbia ottenuto il consenso e il rilascio per ogni individuo che appare nel tuo Contenuto, incluso il diritto di utilizzare e caricare il Contenuto su questo sito Web;
3. per ogni individuo che appare nel tuo Contenuto hai accuratamente accertato ed esaminato un documento d'identità valido con foto governative che dimostri che hanno almeno 18 anni di età nel giorno in cui vengono fotografati, filmati o altrimenti visualizzati in tale Contenuto (questo vale per tutti gli individui, sia identificabili che nonno o no);
4. Per quanto riguarda il Contenuto, l'utente o il produttore del Contenuto, avete raccolto e mantenuto i record richiesti ai sensi di 18 U.S.C. 2257 et. segg., come modificata di volta in volta nel rispetto delle norme e dei regolamenti di cui al 28 C.F.R. 75 et. segg., come modificato di volta in volta, e qualsiasi altra legge applicabile di conservazione o verifica dell'età;
5. che il Content Partner afferma che nessun individuo che appare nel Contenuto è stato condannato per reati relativi a violenza sessuale, sfruttamento sessuale di un minore, traffico e altri reati correlati;
6. il Contenuto può essere utilizzato per scopi di formazione;
7. il Contenuto non è un duplicato di un altro contenuto caricato da te o da chiunque altro;
8. Il Contenuto è conforme ai [Termini di servizio di](https://www.pornhub.com/information/terms) questo Sito Web e alla nostra Informativa sulla privacy.

Il mancato rispetto dei presenti Termini di servizio o di qualsiasi contratto correlato che stipuli con noi, o se riteniamo che una qualsiasi delle dichiarazioni di cui sopra non sia vera e non sia soddisfatta, possiamo, a nostra esclusiva discrezione, rifiutare di includere il Contenuto o parte di esso o qualsiasi riferimento a tale Contenuto su questo Sito Web, rimuovere il Contenuto in questione da questo Sito Web, perdere tutti i guadagni maturati o in sospeso, annullare tutti i pagamenti attuali o in sospeso, chiudere l'account dell'utente.

 **Ulteriori informazioni applicabili a tutti gli utenti**

In combinazione con le disposizioni dei presenti [Termini di servizio,](https://www.pornhub.com/information/terms) la [politica sui contenuti non consensuali](https://help.pornhub.com/hc/en-us/articles/4419871787027-Non-Consensual-Content-Policy), la [politica](https://help.pornhub.com/hc/en-us/articles/4419869793683-Child-Sexual-Abuse-Material-Policy) sui [materiali per gli abusi sessuali su minori](https://help.pornhub.com/hc/en-us/articles/4419869793683-Child-Sexual-Abuse-Material-Policy) e la nostra Informativa sulla privacy, le linee guida che [qui](https://help.pornhub.com/hc/en-us/sections/4419836270483-Policies-and-Related-Guidelines) si trovano sono anche in vigore. Sei l'unico responsabile per familiarizzare con l'ultima versione di queste linee guida, poiché vengono aggiornate di volta in volta.

 **Obbligo di rispettare i requisiti di Conservazione dei record di 18 U.S.C. ? 2257.**

L'utente certifica che il Contenuto che carichi su questo Sito Web è stato prodotto e che i registri vengono conservati in conformità con 18 U.S.C. 2257 et. segg., come modificata di volta in volta, con le regole e i regolamenti di cui al 28 C.F.R. 75 e s. segg., come modificato di volta in volta, e qualsiasi altra legge applicabile di conservazione o verifica dell'età. Su nostra richiesta, l'utente deve consegnarci prontamente copie leggibili (come possono essere legittimamente redatte), di validità (a partire dalla data di produzione del Contenuto) i documenti di identità delle foto governative riconoscibili per qualsiasi o tutte le persone che appaiono in uno o tutti i tuoi Contenuti (dimostrazione che avevano almeno 18 anni di età il giorno in cui è stato prodotto il Contenuto) insieme ai moduli di identificazione, documenti e rilasci richiesti. In questo contesto, quando ci riferiamo a tutte le persone che appaiono nel Contenuto, intendiamo, senza limitazioni, filmate, fotografate o registrate individui o individui in qualsiasi modo nel Contenuto, sia che appaiano nudi, semi-nudi o completamente vestiti, impegnati in rapporti sessuali simulati o effettivi (incluse scene soliste). Non accettiamo o accogliamono la voce o i suoni di persone di età inferiore ai 18 anni che appaiono in background o sincronizzati con il Contenuto, in primo luogo o meno. La mancata consegna tempestiva delle informazioni richieste su richiesta, può comportare la sospensione temporanea o permanente del tuo account. L'utente, a vostre spese, indennizzerai, difenderai e ci manterrai indenne da, qualsiasi responsabilità, perdita, danni, multe, commissioni, costi, costi e spese (comprese le ragionevoli spese legali) sostenute o subite da noi da qualsiasi reclamo derivante o derivante dal tuo fallimento o da negligenza nel rispettare il mantenimento di eentuali record legalmente obbligatori.

 Riconosci di essere l'unico responsabile per l'attività che si verifica sul tuo account. Si prega di notare che non è possibile consentire a nessun'altra persona di utilizzare il proprio account e che è necessario informarci immediatamente di qualsiasi apparente violazione della sicurezza, come perdita, furto o divulgazione non autorizzata o l'uso di un nome o password dello schermo. Non puoi mai usare l'account di nessun altro, proprio come nessuno potrà mai usare il tuo.

 Sarai responsabile per eventuali perdite subite da noi a causa dell'uso non autorizzato del tuo account. Non siamo responsabili per le tue perdite causate da qualsiasi uso non autorizzato del tuo account, e rinunci specificamente a qualsiasi reclamo di questo tipo e accetti di difenderci e indennizzare da tali reclami fatti contro il tuo account da terzi.

 Nella misura in cui crei volontariamente un profilo utente per partecipare a determinati servizi selezionati da noi offerti, il tuo profilo (e i suoi contenuti) potrebbero essere ricercabili da altri Utenti registrati attraverso questo sito Web e da altri partner o collegati in rete con noi. Allo stesso modo, il tuo profilo (e i suoi contenuti) potrebbero essere ricercabili dai motori di ricerca disponibili al pubblico.

 Comprendi che non garantiamo alcuna riservatezza in relazione a qualsiasi contenuto con cui contribuisci.

 In base a quanto consentito dalla legge applicabile, l'utente è libero di scegliere il tipo di Contenuto che si produce e pubblica. L'utente sarà l'unico responsabile per i propri Contenuti e le conseguenze della pubblicazione o pubblicazione di tali Contenuti.

 Non siamo responsabili per qualsiasi contenuto che violi gli standard della comunità nella tua comunità. Se stai cercando informazioni su attività illegali o inappropriate, accetti di lasciare immediatamente questo sito web. Ci aspettiamo e richiediamo che tu rispetti tutte le leggi applicabili quando utilizzi questo sito Web e quando invii o pubblichi Contenuti su questo sito Web. Se non sei sicuro che il Contenuto violi o meno una legge, ti invitiamo a contattare un avvocato prima di pubblicare il Contenuto. Non possiamo far rispettare le leggi di ogni giurisdizione per tutti i Contenuti pubblicati su questo sito web. In quanto tale, non siamo responsabili per il contenuto di questo sito web.

 È necessario valutare e sopportare tutti i rischi associati all'uso di qualsiasi Contenuto, incluso qualsiasi affidamento sull'accuratezza, la completezza o l'utilità o la liceità di tale Contenuto. A questo proposito, l'utente riconosce di non poter fare affidamento su alcun Contenuto creato da noi o sui Contenuti trasmessi a questo Sito Web. L'utente è responsabile di tutti i Contenuti caricati, pubblicati, inviati via e-mail, trasmessi o altrimenti resi disponibili tramite questo sito Web.

Se abbiamo un motivo per sospettare che il tuo Contenuto violi qualsiasi diritto di terzi, inclusi, senza limitazioni, copyright, marchio o diritto di proprietà, possiamo richiederti di fornirci la prova scritta della tua proprietà o del diritto di utilizzo del materiale in questione. Se richiediamo tali prove scritte, l'utente accetta di fornircelo entro cinque (5) giorni lavorativi dalla data della nostra richiesta. La tua incapacità di fornirci tali prove scritte richieste entro tale periodo di tempo può portare alla chiusura immediata del tuo account, noi chiediamo un risarcimento per eventuali costi maturati e danni relativi a tali Contenuti.

 Sarai l'unico responsabile per il tuo Contenuto e le conseguenze di pubblicazione, caricamento, pubblicazione, trasmissione o altrimenti renditi disponibile il tuo Contenuto su questo sito web. Comprendi e riconosci di essere responsabile per qualsiasi Contenuto che invii o contribuisca, e tu, non noi, hai la piena responsabilità di tali Contenuti, inclusa la sua legalità, affidabilità, accuratezza e adeguatezza. Non controlliamo i Contenuti che invii o contribuisca e non facciamo alcuna garanzia relativa ai Contenuti inviati o forniti dagli Utenti. In nessun caso saremo responsabili o responsabili in alcun modo per qualsiasi reclamo relativo ai Contenuti inviati o forniti dagli Utenti.

 L'utente afferma, dichiari e garantisci di possedere o di avere le licenze, i diritti, i consensi e le autorizzazioni necessarie per pubblicare i Contenuti inviati e concedere in licenza a questo Sito Web tutti i diritti di brevetto, marchio, segreto commerciale, copyright o altri diritti di proprietà su e su tali Contenuti per la pubblicazione su questo Sito Web ai sensi dei presenti Termini di servizio.

 L'utente accetta inoltre che il Contenuto inviato a questo sito Web non conterrà materiale protetto da copyright di terzi, o materiale soggetto ad altri diritti di proprietà di terzi, a meno che non si disponga del legittimo proprietario del materiale, o si ha altrimenti legalmente il diritto di pubblicare il materiale e di concedere a questo sito Web tutti i diritti di licenza qui concessi.

 L'utente accetta e comprende che questo sito Web (e il suo successore e le sue affiliate) può utilizzare il tuo Contenuto per scopi promozionali o commerciali e per fornire i servizi ai sensi dei presenti Termini di servizio. Per chiarezza, mantieni tutti i tuoi diritti di proprietà nei tuoi Contenuti. In considerazione della data di ultima modifica, inviando contenuti a questo sito Web, l'utente concede agli operatori di questo sito Web una licenza illimitata, mondiale, perpetua, non esclusiva, esente da royalty, sublicenziabile e trasferibile per utilizzare, riprodurre, pubblicare, distribuire, trasmettere, commercializzare, creare opere derivate, adattare, tradurre, visualizzare pubblicamente, comunicare o eseguire, rendere disponibili o altrimenti utilizzare tutti i contenuti, senza limitazione per promuovere e ridistribuire tali formati. Rinunci anche nella misura massima consentita dalla legge, qualsiasi rivendicazione nei nostri confronti in relazione ai diritti morali sul Contenuto. In nessun caso saremo responsabili nei tuoi confronti per qualsiasi sfruttamento di qualsiasi Contenuto che pubblichi. Con la presente concedi inoltre a ciascun Utente di questo Sito Web una licenza non esclusiva e gratuita per accedere al Contenuto tramite questo Sito Web come consentito dai presenti Termini di servizio. Con la presente comprendi e accetti di non poter utilizzare questo sito Web per consentire ad altri utenti di scaricare o altrimenti salvare il contenuto che pubblichi (o parte di esso). Le licenze di cui sopra concesse dall'utente nei Contenuti video che invii a questo sito Web terminano entro un periodo commercialmente ragionevole dopo che l'utente rimuove o elimina il contenuto da questo sito Web. L'utente comprende e accetta, tuttavia, che questo sito Web può conservare, ma non visualizzare, distribuire o eseguire copie del server dei video che sono stati rimossi o eliminati. Le licenze di cui sopra concesse da te nei commenti degli utenti che invii sono perpetui e irrevocabili.

 Questo sito Web non approva alcun Contenuto inviato da alcun Utente o altro licenziante, né alcuna opinione, raccomandazione o consulenza ivi espressa, e questo Sito Web declina espressamente ogni e qualsiasi responsabilità in relazione al Contenuto. Questo sito Web non consente attività di violazione del copyright e violazione dei diritti di proprietà intellettuale su questo sito Web e questo sito Web rimuoverà tutti i contenuti se correttamente notificato che tale Contenuto viola i diritti di proprietà intellettuale di un altro. Questo sito Web si riserva il diritto di rimuovere il contenuto senza preavviso. Questo sito Web si riserva il diritto di hash qualsiasi Contenuto caricato per scopi di formazione, inclusi, ma non limitati a, la formazione per l'identificazione o la lotta contro le attività illegali e questi hash possono essere condivisi con terze parti, comprese le forze dell'ordine.

 Tutti i Contenuti inviati devono rispettare gli standard di contenuto stabiliti nei presenti Termini di servizio.

 Se uno qualsiasi dei Contenuti che pubblichi su o attraverso questo Sito Web contiene idee, suggerimenti, documenti e / o proposte a noi, non avremo alcun obbligo di riservatezza, espresso o implicito, in relazione a tale Contenuto, e avremo il diritto di utilizzare, sfruttare o divulgare (o scegliere di non utilizzare o divulgare) tali Contenuti a nostra esclusiva discrezione senza alcun obbligo nei tuoi confronti (cioè non avrai diritto ad alcun risarcimento di alcun tipo da parte nostra in nessuna circostanza).

 **Regole applicabili ai partner dei contenuti**

Se si desidera diventare un Partner di contenuto e pubblicare Contenuti su questo sito Web, è necessario prima aderire al nostro Programma Partner per i Contenuti e accettare i suoi termini e condizioni.

**Norme applicabili ai modelli**

Se si desidera diventare un modello e pubblicare contenuti attraverso questo sito Web, è necessario prima aderire al nostro programma Partner modello e accettare i suoi termini e condizioni.

 Possiamo, a nostra esclusiva discrezione, rifiutare la tua domanda di adesione alla nostra comunità di fornitori di contenuti per qualsiasi motivo.

 Potrebbe essere richiesto di fornire un indirizzo e-mail valido a fini di verifica. Sceglirai il tuo nome sullo schermo, che deve essere unico per te, non offensivo per gli altri e non in violazione del copyright o del marchio di un altro. Sceglirai anche la tua password, che potrai cambiare in seguito. È imperativo che tu non perda a nessun altro di utilizzare il tuo account (devi mantenere la password segreta e sicura). Alcune modifiche alle tue informazioni personali come il tuo nome e il tuo nome utente possono essere apportate solo dal nostro staff. Pertanto, se le tue informazioni non vengono corrette o devono essere modificate, potrebbe essere necessario contattare il nostro staff per farlo.

 Prima di poter caricare il contenuto su questo sito web, è necessario verificare la propria identità. Per fare ciò, è necessario inviarci immagini ad alta risoluzione o scansioni di un minimo di uno o due documenti informativi, contenenti la data di nascita, la data di scadenza dell'ID, la tua foto, il tuo nome legale completo e il tuo indirizzo. Questa potrebbe essere, ad esempio, la patente di guida (nei paesi in cui un documento d'identità nazionale non è obbligatorio), passaporto internazionale, carta di cittadinanza, documento d'identità statale, passaporto nazionale o carta d'identità nazionale. L'altra forma di identificazione può essere una bolletta. Se tutte le informazioni richieste sono stabilite sul tuo documento d'identità con foto emesse dal governo, non è necessario un secondo documento. Possiamo, a nostra esclusiva discrezione, richiedere l'utente di fornirci molteplici forme di identificazione per stabilire la prova dell'età adulta e dell'identità. La verifica facciale e una valutazione dell'autenticità dei suddetti documenti sono richieste anche nell'ambito della nostra verifica della tua età e identità. La nostra raccolta, utilizzo e divulgazione di tali informazioni e documentazione è regolata dalla nostra [Informativa sulla privacy.](https://www.pornhub.com/information/privacy)

 **La Commissione dei modelli**

I modelli avranno il diritto di ricevere i ricavi delle azioni pubblicitarie del programma modello (una percentuale delle entrate pubblicitarie che il video guadagna) gratuitamente per guardare video su questi siti Web.

 Non riceverai mensilmente una dichiarazione che mostra gli importi guadagnati. Tuttavia, potrai visualizzare gli importi guadagnati nel tuo account. I pagamenti sono generalmente effettuati entro dieci

(10) giorni lavorativi dopo la fine di ogni mese (anche se, a seconda del metodo di pagamento utilizzato, potrebbe richiedere più tempo per ricevere effettivamente il pagamento); a condizione, tuttavia, che il pagamento sarà effettuato solo quando è stato raggiunto il pagamento minimo, basato sul reddito netto, di cento dollari (US $ 100,00). Qualsiasi importo inferiore a cento dollari (US $ 100,00) verrà riportato fino al raggiungimento del pagamento minimo e sarà pagato alla successiva data di pagamento successiva. Se ci fornisci dettagli di pagamento errati, eventuali commissioni per i risconvolti di pagamento derivanti da tali dettagli di pagamento errati saranno compensate con eventuali importi altrimenti dovuti a te.

 Nel caso in cui l'utente violi qualsiasi parte dei presenti Termini di servizio o se violi un diritto di terzi, incluso, senza limitazione, copyright, proprietà o diritto alla privacy, o laddove una terza parte sostenirà che tutto o parte del tuo Contenuto ha causato danni, potremmo, a nostra esclusiva discrezione, trattenere i pagamenti a tempo indeterminato.

 Ti ricordiamo che è tuo obbligo e obbligo di adempiere a tutti gli obblighi fiscali in relazione alle tue attività per noi, inclusi ma non limitati a obblighi di registrazione, dichiarazione e pagamento relativi alle imposte sul reddito e all'IVA. Se ti aiutiamo con questi obblighi, non ti esoneri dai tuoi obblighi. È tuo dovere informarci in modo tempestivo di modifiche ai tuoi dati personali come una modifica dell'indirizzo o una nuova e-mail.

**Regole applicabili ai caricatori verificati**

In qualità di Caricatore verificato, sarai un titolare di un account validamente identificato e potrai inviare Contenuti a questo Sito Web.

 Possiamo, a nostra esclusiva discrezione, rifiutare la tua domanda di adesione alla nostra comunità di fornitori di contenuti per qualsiasi motivo.

 Potrebbe essere richiesto di fornire un indirizzo e-mail valido a fini di verifica. Sceglirai il tuo nome sullo schermo, che deve essere unico per te, non offensivo per gli altri e non in violazione del copyright o del marchio di un altro. Sceglirai anche la tua password, che potrai cambiare in seguito. È imperativo che tu non perda a nessun altro di utilizzare il tuo account (devi mantenere la password segreta e sicura). Alcune modifiche alle tue informazioni personali come il tuo nome e il tuo nome utente possono essere apportate solo dal nostro staff. Pertanto, se le tue informazioni non vengono corrette o devono essere modificate, potrebbe essere necessario contattare il nostro staff per farlo.

 Prima di poter caricare il contenuto su questo sito web, è necessario verificare la propria identità. Per fare ciò, è necessario inviarci immagini ad alta risoluzione o scansioni di un minimo di uno o due documenti informativi, contenenti la data di nascita, la data di scadenza dell'ID, la tua foto, il tuo nome legale completo e il tuo indirizzo. Questa potrebbe essere, ad esempio, la patente di guida (nei paesi in cui un documento d'identità nazionale non è obbligatorio), passaporto internazionale, carta di cittadinanza, documento d'identità statale, passaporto nazionale o carta d'identità nazionale. L'altra forma di identificazione può essere una bolletta. Se tutte le informazioni richieste sono stabilite sul tuo documento d'identità con foto emesse dal governo, non è necessario un secondo documento. Possiamo, a nostra esclusiva discrezione, richiedere l'utente di fornirci molteplici forme di identificazione per stabilire la prova dell'età adulta e dell'identità. La verifica facciale e una valutazione dell'autenticità dei suddetti documenti sono richieste anche nell'ambito della nostra verifica della tua età e identità. La nostra raccolta, utilizzo e divulgazione di tali informazioni e documentazione è regolata dalla nostra [Informativa sulla privacy.](https://www.pornhub.com/information/privacy)

 **Utilizzo del sito web**

L'utente accetta di utilizzare questo sito Web e i nostri servizi solo per gli scopi legittimi espressamente consentiti e contemplati da questi Termini di servizio. Non è possibile utilizzare questo sito Web e i nostri servizi per altri scopi, inclusi ma non limitati a scopi commerciali, senza il nostro esplicito consenso scritto.

 L'utente accetta di visualizzare questo sito Web e il suo contenuto inalterato e non modificato. L'utente riconosce e comprende che è vietato modificare questo sito Web o eliminare il contenuto di questo sito Web, inclusi gli annunci. Non è consentito eludere, rimuovere, eliminare, disabilitare, alterare o altrimenti interferire con qualsiasi processo di verifica dell'età e della biometria, le tecnologie o gli strumenti di sicurezza utilizzati in qualsiasi luogo di questo sito Web o in connessione con i nostri servizi. Utilizzando questo sito Web, l'utente accetta espressamente di accettare la pubblicità utilizzata su e attraverso questo sito Web e di astenersi dall'utilizzare software di blocco degli annunci o di disabilitare il software di blocco degli annunci prima di visitare questo sito Web.

 Il contenuto è fornito a voi COME SONO. È possibile accedere al Contenuto per le proprie informazioni e uso personale esclusivamente come previsto attraverso la funzionalità fornita di questo sito Web e come consentito dai presenti Termini di servizio. L'utente non deve scaricare, copiare, riprodurre, distribuire, trasmettere, trasmettere, visualizzare, vendere, concedere in licenza o altrimenti sfruttare qualsiasi Contenuto che non sia consentito nei Termini di servizio. 

**Usi e reporting proibiti**

I contenuti che si tentano di pubblicare sul sito Web non devono violare alcuna legge o diritto applicabile di terzi. Inoltre, i Contenuti pubblicati non devono violare i presenti Termini di servizio, incluse le politiche o i documenti a cui si fa riferimento nei presenti Termini di servizio, e in particolare nella sezione "Uso vietato" qui trovato.

Se ritieni che i Contenuti sospetti violino la legge applicabile, i diritti di terzi o i presenti Termini di servizio, ti preghiamo di segnalarci tale Contenuto, utilizzando il modulo elettronico disponibile all'indirizzo [https://pornhub.com/content-removal o](https://pornhub.com/content-removal) il pulsante "Segnala" disponibile sotto ogni contenuto. Inoltre, il sito Web non consente alcuna forma di revenge porn, ricatto o intimidazione, e tali violazioni possono anche essere segnalate utilizzando il link di rimozione dei contenuti qui riportato.

 Daremo la dovuta priorità alle relazioni presentate dai nostri Trusted Flaggers, che sono stati concessi dal coordinatore dei servizi digitali dello Stato membro interessato dell'Unione europea. Inoltre, le organizzazioni non governative e gli uffici statutari possono anche aderire al [programma Pornhub Trusted Flagger.](https://help.pornhub.com/hc/en-us/articles/4419879221907-Pornhub-Trusted-Flagger-Program)

 L'utente accetta di non utilizzare o tentare di utilizzare alcun metodo, dispositivo, software o routine per danneggiare altri o interferire con il funzionamento di questo sito Web o utilizzare e / o monitorare qualsiasi informazione in o relativa a questo sito Web per scopi non autorizzati.

 Oltre alle restrizioni generali di cui sopra, le seguenti restrizioni e condizioni si applicano specificamente all'utilizzo dei Contenuti. Qualsiasi determinazione in merito alla violazione di una delle seguenti condizioni è definitiva. Si prega di rivedere attentamente il seguente elenco di usi vietati prima di utilizzare questo sito web. In particolare, l'utente accetta di non utilizzare nessuno di questi siti Web per:

* violare qualsiasi legge o incoraggiare o fornire istruzioni a un altro per farlo;
* agire in modo da influire negativamente sulla capacità di altri utenti di utilizzare questo sito Web, incluso senza limitazioni impegnandosi in comportamenti dannosi, minacciosi, abusivi, infiammanti, intimidatori, violenti o incoraggianti di violenza a persone o animali, molesti, stalking, invasivi della privacy di un altro, o razzialmente, etnicamente o altrimenti discutibili;
* pubblicare qualsiasi Contenuto che rappresenti qualsiasi persona di età inferiore ai 18 anni (o più in qualsiasi altro luogo in cui 18 non è la maggiore età minima) sia reale che simulato;
* pubblicare qualsiasi Contenuto per il quale non hai mantenuto la documentazione scritta sufficiente a confermare che tutti i soggetti dei tuoi post sono, infatti, oltre i 18 anni di età (o più in qualsiasi altro luogo in cui 18 non è l'età minima della maggiore);
* pubblicare qualsiasi Contenuto che descriva attività sessuale minorile, attività sessuale non consensuale, revenge porn, ricatto, intimidazioni, snuff, torture, morte, violenza, incesto, insulti razziali o incitamento all'odio, (sia oralmente che tramite la parola scritta);
* pubblicare qualsiasi Contenuto che contenga falsità o false dichiarazioni che potrebbero danneggiare questo sito Web o qualsiasi terza parte;
* pubblicare qualsiasi Contenuto osceno, illegale, illegale, fraudolento, diffamatorio, calunnioso, molesto, odioso, razzialmente o etnicamente offensivo, o incoraggi comportamenti che sarebbero considerati un reato penale, darebbero luogo a responsabilità civile, violino qualsiasi legge o sia altrimenti inappropriato;
* pubblicare qualsiasi Contenuto contenente pubblicità non richiesta o non autorizzata, materiale promozionale, spam, posta indesiderata, catene di cattedrone, schemi piramidali o qualsiasi altra forma di sollecitazione non autorizzata;
* pubblicare qualsiasi Contenuto contenente lotterie, concorsi o lotterie, o altrimenti correlata al gioco d'azzardo;
* pubblicare qualsiasi Contenuto contenente materiali protetti da copyright, o materiali protetti da altre leggi sulla proprietà intellettuale, che non possiedi o per i quali non hai ottenuto tutte le autorizzazioni e i rilasci scritti necessari;
* pubblicare qualsiasi Contenuto che impersoni un'altra persona o che indichi falsamente o che altrimenti travisa la tua affiliazione con una persona;
* utilizzare questo sito Web (o pubblicare qualsiasi contenuto che) in qualsiasi modo che promuova o faciliti la prostituzione, la sollecitazione di prostituzione, traffico di esseri umani o traffico sessuale;
* utilizzare questo sito Web per organizzare eventuali incontri di persona per scopi di attività sessuali a noleggio;
* distribuire programmi, software o applicazioni progettati per interrompere, distruggere o limitare la funzionalità di qualsiasi software o hardware o apparecchiatura di telecomunicazione, anche coinvolgendo qualsiasi attacco di negazione del servizio o condotta simile;
* distribuire o utilizzare programmi, software o applicazioni progettati per danneggiare, interferire con il funzionamento o l'accesso in modo non autorizzato, servizi, reti, server o altre infrastrutture;
* superare l'accesso autorizzato a qualsiasi parte di questo sito Web;
* rimuovere, eliminare, alterare, eludere, evitare o bypassare qualsiasi tecnologia di gestione dei diritti digitali, crittografia o strumenti di sicurezza utilizzati in qualsiasi punto di questo sito Web o in connessione con i nostri servizi;
* rimuovere, eliminare, alterare, eludere, evitare o bypassare qualsiasi processo di verifica dell'età, tecnologie o strumenti di sicurezza utilizzati in qualsiasi punto di questo sito Web o in connessione con i nostri servizi;
* raccogliere o archiviare dati personali su chiunque;
* alterare o modificare senza autorizzazione alcuna parte di questo sito Web o del suo contenuto, inclusi gli annunci;
* ottenere o tentare di accedere o altrimenti ottenere qualsiasi Contenuto o informazione attraverso qualsiasi mezzo non intenzionalmente reso disponibile o previsto attraverso questo Sito Web;
* Errori di exploit nel design, caratteristiche non documentate e/o bug per ottenere l'accesso che altrimenti non sarebbero disponibili.

Inoltre, l'utente accetta di non:

* utilizzare questo sito Web in modo da poter disabilitare, sovraccaricare, danneggiare o compromettere l’utilizzo di questo sito da parte di terzi, inclusa la sua capacità di intraprendere attività in tempo reale attraverso questo sito web;
* utilizzare qualsiasi robot, spider o altro dispositivo, processo o mezzo automatico per accedere a questo sito Web per qualsiasi scopo, incluso il monitoraggio o la copia di qualsiasi materiale su questo sito Web senza il nostro previo consenso scritto;
* utilizzare qualsiasi processo manuale per scaricare, monitorare o copiare qualsiasi materiale su questo sito Web o per qualsiasi altro scopo non autorizzato;
* utilizzare qualsiasi informazione ottenuta da o attraverso questo sito Web per bloccare o interferire con la visualizzazione di qualsiasi pubblicità su questo sito Web, o allo scopo di implementare, modificare o aggiornare qualsiasi software o elenco di filtri che blocchi o interferiscano con la visualizzazione di qualsiasi pubblicità su questo sito Web;
* utilizzare qualsiasi dispositivo, bot, script, software o routine che interferisce con il corretto funzionamento di questo sito Web o che la scorciatoia o altera le funzioni del sito Web da eseguire o apparire in modi che non sono destinati dal design di questo sito Web;
* introdurre o caricare virus, trojan horse, worm, bombe logiche, bombe a orologeria, cancelbot, file corrotti o qualsiasi altro software, programma o materiale simile che sia dannoso o tecnologicamente dannoso o che possa danneggiare il funzionamento di proprietà di un altro o dei nostri servizi di questo sito Web;
* tentare di ottenere l'accesso non autorizzato, interferire, danneggiare o interrompere qualsiasi parte di questo sito Web, il server su cui è memorizzato questo sito Web, o qualsiasi server, computer o database collegato a questo sito Web;
* rimuovere qualsiasi copyright o altri avvisi di proprietà dal nostro sito Web o da uno qualsiasi dei materiali in esso contenuti;
* attaccare questo sito Web tramite un attacco denial-of-service o un attacco denial-of-service distribuito;
* tentare di interferire con il corretto funzionamento di questo sito web.

**Monitoraggio e applicazione; Cessazione**

Abbiamo il diritto ma non l'obbligo di intraprendere una delle seguenti azioni, anche in base a quando rileviamo o siamo informati di qualsiasi attività o contenuto caricato sul sito Web che violi le leggi applicabili, i diritti di terzi o questi Termini di servizio. Nel decidere un'azione appropriata, consideriamo tutti i fattori rilevanti come la frequenza, la gravità e l'impatto di una violazione, nonché tutte le misure precedentemente imposte a un trasgressore:

* Emettere un avviso scritto al trasgressore. Prima di sospendere temporaneamente o definitivamente l'accesso di un trasgressore ai nostri servizi a seguito della fornitura di Contenuti o azioni manifestamente illegali da parte di tale trasgressore, possiamo emettere un avviso al trasgressore, a condizione che tale avviso non sarebbe in conflitto con gli scopi della decisione di moderazione. Esempi di uso improprio che possono essere sanzionati con una sospensione o una risoluzione includono, ma non sono limitati a, il caricamento frequente di Contenuti che viola la legge applicabile, i diritti di terzi o i presenti Termini di servizio, o il caricamento di Contenuti che violano manifestamente e gravemente la legge applicabile, i diritti di terzi o questi Termini di servizi, come il materiale per abusi sessuali sui minori o il revenge porn.
* Limitare la visibilità o l'accessibilità di contenuti.
* Limitare l'accesso, sospendere o chiudere l'account di un utente.
* Limitare, sospendere o terminare in altro modo la capacità di un Caricatore verificato di monetizzare il proprio Contenuto.
* Limitare, sospendere o trattenere guadagni e pagamenti.
* Segnalare l'autore di violazione alle autorità di contrasto.
* monitorare qualsiasi comunicazione che si verifica su o attraverso questo sito Web per confermare la conformità con i presenti Termini di servizio, la sicurezza di questo sito Web o qualsiasi obbligo legale;
* intraprendere azioni legali appropriate, incluso, senza limitazione, il deferimento alle forze dell'ordine, per qualsiasi contenuto illegale o illecito o qualsiasi uso illegale o non autorizzato di questo sito Web;

Possiamo anche rimuovere o rifiutare di pubblicare qualsiasi Contenuto che invii o contribuisca a questo Sito Web per qualsiasi motivo a nostra esclusiva discrezione, incluso, a titolo esemplificativo, se qualsiasi informazione o documentazione fornita da te è inadeguata, incompleta o imprecisa o non ci consente di valutare e confermare la tua identità.

Se abbiamo motivo di credere che il Contenuto violi qualsiasi legge applicabile, diritto di terzi o questi Termini di servizio, possiamo intraprendere le opportune azioni provvisorie (come descritto nel presente documento), per prevenire potenziali danni alle nostre ulteriori indagini. Nel decidere il corso di azione appropriato e proporzionato, daremo la dovuta considerazione agli interessi legittimi dell'Utente interessato, a qualsiasi altro titolare dei diritti potenzialmente interessati e ai nostri legittimi interessi. In particolare, valuteremo la natura del Contenuto interessato, la gravità della rispettiva violazione o illegalità e qualsiasi indicazione sul fatto che il contributore del Contenuto sia stato o meno colpevole. Nel caso in cui un'indagine sia stata richiesta a seguito di un rapporto da parte di un Utente, considereremo anche le informazioni esplicative e le prove fornite da tale Utente e, se disponibili, la cronologia di presentazione dell'Utente.

Utilizziamo una varietà di procedure e strumenti per identificare, rivedere e moderare i contenuti. Ciò include sia la revisione umana che quella automatizzata o una combinazione di entrambi, a seconda di ciò che è appropriato e richiesto in ogni singolo caso.

La revisione automatizzata può includere l'uso di sistemi che ci aiutano a identificare la violazione dei contenuti, la determinazione della definizione delle priorità di determinati problemi e l'applicazione di misure appropriate. I contenuti possono essere confrontati con database di contenuti stampati digitalmente e altri repository simili, come (ma non solo) CSAI Match di YouTube. La revisione automatizzata può essere soggetta a ulteriori verifiche manuali, nel contesto in cui i contenuti e le misure possono essere rivalutati.

Senza limitare quanto sopra, abbiamo il diritto di cooperare pienamente con le autorità preposte all'applicazione della legge o all'ordine del tribunale che ci richiede o ci ordina di divulgare l'identità o altre informazioni di chiunque pubblichi qualsiasi Contenuto su o attraverso questo sito Web. L'UTENTE È ACQUE E HOLD US, I NOSTRI UFFICI, DIRETTORI, DIPENDENTI, SUCCESSORI E SESSIBILI DALLE NESSUN RECLAMI RISULTANTI DA QUALSIASI RISULTAZIONE DI QUALSIASI AZIONE TAKEN DA US COME CONSEGUENTE DI DISCOLGILANZA INFORMAZIONI PERSONALI IN RELAZIONE A RICHIESTA DI DISCLUSSCO DA DALLA LEGATA

Questo sito web assume una posizione potente contro qualsiasi forma di sfruttamento minorile o traffico di esseri umani. Se scopriamo che qualsiasi Contenuto coinvolge individui minorenni, o qualsiasi forma di forza, frode o coercizione, rimuoveremo il Contenuto e presenteremo un rapporto alle autorità preposte all'applicazione della legge. Se l'utente viene a conoscenza di tali Contenuti, l'utente accetta di segnalarlo a questo Sito Web contattando [l'articolo di legal-pornhub.com.](mailto:legal@pornhub.com)

Per mantenere i nostri servizi in un modo che riteniamo appropriato per la nostra sede e nella misura massima consentita dalle leggi applicabili, questo sito Web può, ma non avrà alcun obbligo di visualizzare, rifiutare, archiviare, mantenere, accettare o rimuovere qualsiasi Contenuto pubblicato (inclusi, senza limitazione, messaggi privati, chat di gruppo pubblico, messaggi di chat di gruppo privati o messaggi istantanei privati) e possiamo, a nostra esclusiva discrezione, eliminare, spostare, spostare, re-re-re-re-re-Con il nostro funzionamento di questo sito Web in modo appropriato. Inoltre, questo sito Web può, ma non avrà alcun obbligo, di rivedere e monitorare messaggi privati, commenti pubblici, messaggi di chat di gruppo pubblici, messaggi di chat di gruppo privati o messaggi istantanei privati pubblicati da te. Senza limitazioni, possiamo farlo per affrontare il Contenuto che viene alla nostra attenzione che riteniamo offensivo, osceno, violento, molesto, minaccioso, offensivo, abusivo, illegale o altrimenti discutibile o inappropriato, o per far rispettare i diritti di terzi o questi Termini di servizio o qualsiasi termine aggiuntivo applicabile, incluse, senza limitazione, le restrizioni sul Contenuto stabilite nel presente documento.

Non ci assumiamo alcuna responsabilità per qualsiasi azione o inazione in merito a trasmissioni, comunicazioni o contenuti forniti da qualsiasi Utente o terza parte. Non abbiamo alcuna responsabilità nei confronti di nessuno per prestazioni o mancata esecuzione delle attività descritte in questa sezione.

**Procedura di gestione della denuncia**

Se inviamo qualsiasi azione che ti riguarda, il tuo account o qualsiasi Contenuto che hai tentato o caricato sul Sito Web, come indicato nella sezione "Monitoraggio e applicazione; Risoluzione", puoi presentare un reclamo contro tale decisione. Allo stesso modo, è possibile presentare un reclamo se non sei d'accordo con la nostra decisione a seguito di un avviso che hai inviato per la segnalazione o la rimozione dei contenuti.

 In entrambi i casi, il termine per la presentazione di un reclamo è di sei (6) mesi dopo la notifica della rispettiva decisione che l'utente è stato comunicato. Per presentare un reclamo è possibile inviare un'e-mail a [dsa-pornhub.com](mailto:dsa@pornhub.com) dall'e-mail associata al proprio account o da cui è stata comunicata l'avviso della decisione e la corrispondenza deve includere quante più informazioni possibili per consentirci di indagare sul reclamo e una spiegazione dei motivi per i quali ritieni che il tuo reclamo sia giustificato.

 Gestiamo i reclami in modo tempestivo, non discriminatorio, diligente e oggettivo. Possiamo invertire le decisioni precedenti se un reclamo dimostra sufficientemente che:

* Il Contenuto a cui la nostra decisione non ha effettivamente violato la legge applicabile, i diritti di terzi o i presenti Termini di servizio.
* La nostra decisione di non agire su un avviso era ingiustificata.
* La nostra decisione precedente è stata ingiustificata o sproporzionata in qualsiasi altro modo.

 **Abuso del sistema di segnalazione e/o di gestione dei reclami**

Potremmo sospendere l'accesso ai nostri sistemi di segnalazione e di gestione dei reclami interni per un periodo di tempo ragionevole se invii frequentemente avvisi o reclami che sono manifestamente infondati. Prima di tale sospensione possiamo emettere un avvertimento, a condizione che ciò non sia in conflitto con lo scopo della sospensione o con altre misure appropriate che possono essere applicate. Al momento di decidere sulla sospensione, consideriamo fattori quali la frequenza, la gravità e l'impatto delle vostre violazioni, nonché eventuali misure preliminari imposte. Esempi di uso improprio del nostro sistema di segnalazione e / o di gestione dei reclami che possono essere soggetti a sospensione includono, ma non sono limitati a:

* Inviare frequentemente avvisi o reclami multipli, identici e infondati.
* Inviare frequentemente avvisi o reclami che sono ovviamente non smentiti.

**Politica di cessazione dell'account**

Sebbene venga accettato il contenuto pornografico e per adulti, questo sito Web si riserva il diritto di decidere se il Contenuto è appropriato o viola i presenti Termini di servizio per motivi diversi dalla violazione del copyright e dalle violazioni dei diritti di proprietà intellettuale, come, ma non limitato a materiale osceno offare o diffamatorio. In linea con le disposizioni della sezione intitolata "Monitoring and Enforcement; Cessazione", possiamo, a nostra esclusiva discrezione, emettere avvertimenti o intraprendere azioni analoghe e appropriate (su recidivi) contro tali violazioni

 In caso di violazione della lettera o dello spirito di questi Termini di servizio, o altrimenti si crea un rischio o una possibile esposizione legale per noi, possiamo interrompere l'accesso a questo sito Web o interrompere la fornitura di tutto o parte di questo sito Web a voi per qualsiasi motivo a nostro vantaggio.

 **Diritto d'autore e altra proprietà intellettuale**

Questo sito Web gestisce una chiara [politica](https://www.pornhub.com/information/dmca) di [copyright](https://www.pornhub.com/information/dmca) in relazione a qualsiasi contenuto presunto che violi il copyright di una terza parte. Se ritieni che qualsiasi Contenuto violi il tuo copyright, consulta la nostra [Politica sul copyright](https://www.pornhub.com/information/dmca) per istruzioni sull'invio di una notifica di violazione del copyright. Come parte della nostra [Politica](https://www.pornhub.com/information/dmca) sul [copyright](https://www.pornhub.com/information/dmca), questo sito Web può interrompere l'accesso dell'utente a questo sito Web se, in circostanze appropriate, un utente è stato determinato per essere un trasgressore ripetuto e in linea con le disposizioni della sezione intitolata "Monitoraggio e applicazione; risoluzione" dei presenti Termini.

 Questo sito Web non è in grado di mediare le controversie sui marchi tra gli utenti e i proprietari dei marchi. Di conseguenza, incoraggiamo i proprietari dei marchi a risolvere qualsiasi controversia direttamente con l'Utente in questione o a cercare qualsiasi risoluzione in tribunale o con altri mezzi giudiziari. Se sei sicuro di voler segnalare il Contenuto su questo sito Web che ritieni violi il tuo marchio, puoi farlo contattandoci [qui.](https://www.pornhub.com/support) Questo sito Web è disposto a eseguire un'indagine limitata su reclami ragionevoli e rimuoverà il Contenuto in casi chiari di violazione. Solo il proprietario del marchio o il loro rappresentante autorizzato possono presentare una segnalazione di violazione del marchio. Si prega di notare che forniamo regolarmente il nome del proprietario dei diritti, il tuo indirizzo e-mail e i dettagli del tuo rapporto alla persona che ha pubblicato il contenuto che stai segnalando. Questa persona può contattarti con le informazioni fornite.

**Affidanza sulle informazioni**

Le informazioni presentate su o attraverso questo sito Web sono rese disponibili esclusivamente a scopo informativo generale. Non garantiamo l'accuratezza, la completezza o l'utilità di queste informazioni. Qualsiasi affidamento che metti su tali informazioni è strettamente a tuo rischio. Dichiariamo ogni responsabilità derivante da qualsiasi affidamento posto su tali materiali da te o da qualsiasi altro visitatore di questo sito Web, o da chiunque possa essere informato di uno qualsiasi dei suoi Contenuti.

 Questo sito Web include Contenuti forniti da terze parti, inclusi materiali forniti da altri utenti, blogger e licenziatari di terze parti, sindacati, aggregatori e/o servizi di segnalazione. Tutte le dichiarazioni e / o le opinioni espresse in questi materiali, e tutti gli articoli e le risposte a domande e altri contenuti, diversi dal Contenuto da noi fornito, sono esclusivamente le opinioni e la responsabilità della persona o dell'entità che fornisce tali materiali. Questi materiali non riflettono necessariamente la nostra opinione. Non siamo responsabili, o responsabili nei confronti dell'utente o di terzi, per il Contenuto o l'accuratezza di qualsiasi materiale fornito da terzi.

 **Modifiche a questo sito**

Possiamo aggiornare il contenuto di questo sito Web di tanto in tanto, ma il suo contenuto non è necessariamente completo o aggiornato. Qualsiasi materiale su questo sito Web potrebbe non essere aggiornato in un dato momento e non abbiamo alcun obbligo di aggiornare tale materiale.

**Informazioni su di te e le tue visite a questo sito**

Tutte le informazioni che raccogliamo su questo sito Web sono soggette alla nostra [Informativa sulla privacy.](https://www.pornhub.com/information/privacy) Utilizzando questo sito Web, l'utente riconosce di aver letto e compreso i termini [dell'Informativa sulla privacy](https://www.pornhub.com/information/privacy) e di acconsentire a tutte le azioni intraprese da noi in relazione alle tue informazioni in conformità con [l'Informativa](https://www.pornhub.com/information/privacy) [sulla privacy.](https://www.pornhub.com/information/privacy)

 **Raccolta e utilizzo delle informazioni di utilizzo da parte di inserzionisti e altri**

Questo sito Web consente ad altri di visualizzare annunci pubblicitari utilizzando questo sito web. Queste terze parti utilizzano la tecnologia per fornire pubblicità che vedi utilizzando questo sito Web direttamente al tuo browser. In tal modo, possono ricevere automaticamente il tuo indirizzo IP (Internet Protocol). Altri che inseriscono pubblicità utilizzando questo sito Web possono avere la possibilità di utilizzare cookie e / o web beacon per raccogliere informazioni, comprese le informazioni sull'utilizzo di questo sito Web. Non controlliamo i processi che gli inserzionisti utilizzano per raccogliere informazioni. Tuttavia, gli indirizzi IP, i cookie e i web beacon da soli in genere non possono essere utilizzati per identificare gli individui, solo le macchine. Pertanto, gli inserzionisti e altri i cui annunci pubblicitari o contenuti possono essere forniti attraverso il servizio generalmente non sapranno chi sei a meno che tu non fornisca loro ulteriori informazioni, rispondendo a un annuncio, stipulando un accordo con loro o con altri mezzi.

 **Collegamento a questo sito Web e funzionalità dei social media**

Puoi collegarti a questo sito Web, a condizione che lo faccia in modo equo e legale e non danneggi la nostra reputazione o ne approfitti, ma non devi stabilire un collegamento in modo tale da suggerire qualsiasi forma di associazione, approvazione o approvazione da parte nostra senza il nostro esplicito consenso scritto.

 Questo sito Web può fornire alcune funzionalità dei social media che consentono di:

* link dal tuo sito web o da determinati siti Web di terze parti a determinati contenuti su questo sito Web;
* inviare e-mail o altre comunicazioni con determinati Contenuti, o collegamenti a determinati Contenuti, su questo Sito Web;
* Le parti limitate dei Contenuti su questo Sito Web devono essere visualizzate o appaiono visualizzate su proprio o su determinati siti Web di terzi.

È possibile utilizzare queste funzionalità esclusivamente in quanto sono fornite da noi e esclusivamente in relazione al Contenuto con cui vengono visualizzate e in altro modo in conformità con eventuali termini e condizioni aggiuntivi che forniamo in relazione a tali funzionalità. Soggetto a quanto precede, non devi:

* far sì che questo sito Web o parti di questo sito Web vengano visualizzati o visualizzati, ad esempio, inquadratura, deep linking o in-line linking, su qualsiasi altro sito,
* Per non utilizzare qualsiasi azione in relazione ai materiali di questo sito Web che non sia coerente con qualsiasi altra disposizione dei presenti Termini di servizio.

I siti da cui ti colleghi o su cui rendi accessibili determinati contenuti devono rispettare in tutti gli aspetti gli standard di contenuto stabiliti nei presenti Termini di servizio.

Accetti di collaborare con noi per causare la cessazione immediata di qualsiasi inquadratura o collegamento non autorizzato. Ci riserviamo il diritto di ritirare il permesso di collegamento senza preavviso.

Possiamo disabilitare tutte o eventuali funzionalità dei social media e qualsiasi link in qualsiasi momento senza preavviso a nostra esclusiva discrezione.

 **Link da questo sito web**

Se questo sito Web contiene collegamenti ad altri siti e risorse fornite da terze parti, questi collegamenti sono forniti solo per comodità dell'utente. Ciò include i link contenuti negli annunci pubblicitari, inclusi banner pubblicitari e link sponsorizzati. Non abbiamo alcun controllo e non ci assumiamo alcuna responsabilità per i Contenuti, le politiche sulla privacy o le pratiche di tali siti o risorse, e non accettiamo alcuna responsabilità per loro o per eventuali perdite o danni che potrebbero derivare dall'utilizzo da parte dell'utente. L'inclusione, il collegamento o la responsabilità dell'uso o dell'installazione di siti, applicazioni, software, contenuti o pubblicità di terze parti non implica l'approvazione o l'approvazione da parte nostra. Se si decide di accedere a uno qualsiasi dei siti di terze parti collegati a questo sito Web, lo si fa interamente a proprio rischio e soggetti ai termini e alle condizioni di utilizzo per tali siti. Inoltre, l'utente accetta di esonerarci da qualsiasi responsabilità derivante dall'utilizzo di qualsiasi sito Web, contenuto, servizio o software di terze parti a cui si accede tramite questo sito Web.

 Le tue comunicazioni o rapporti o la partecipazione a promozioni, sponsor, inserzionisti o altre terze parti trovate attraverso questo sito Web sono esclusivamente tra te e tali terze parti. L'utente accetta che questo sito Web non sarà responsabile per eventuali perdite o danni di qualsiasi tipo sostenuti a seguito di qualsiasi rapporto con tali sponsor, terze parti o inserzionisti, o come risultato della loro presenza su questo sito Web.

**Divulgazione delle informazioni personali**

Questo sito Web generalmente non raccoglie informazioni di identificazione personale (dati come il tuo nome, indirizzo e-mail, password e il contenuto delle tue comunicazioni) a meno che tu non invii o comunichi il contenuto tramite questo sito Web o ti registri con noi al fine di utilizzare determinate funzionalità di questo sito Web. Questo sito Web non divulgherà alcuna informazione di identificazione personale che raccoglie o ottiene tranne: (i) come descritto in questi Termini di servizio o nella nostra [Informativa sulla privacy;](https://www.pornhub.com/information/privacy) (ii) dopo aver ottenuto il permesso dell'utente per un uso o una divulgazione specifica; (iii) se questo sito Web è tenuto a farlo al fine di rispettare qualsiasi processo legale valido o richiesta governativa (come un ordine del tribunale, un mandato di perquisizione, un mandato di sottopoena, una richiesta di scoperta civile o un requisito legale) e può, a nostra esclusiva discrezione,Proprietà o sicurezza di altri; o (v) a una persona che acquista questo sito Web o ai beni del gestore di questo sito Web in relazione a cui tali informazioni sono state raccolte o utilizzate; o (vi) come altrimenti richiesto dalla legge. Se questo sito Web è tenuto a rispondere o rispettare una qualsiasi di queste richieste di informazioni, ci riserviamo il diritto di addebitarti il costo di rispondere o di soddisfare tale richiesta di informazioni, inclusi, a titolo esemplificativo ma non esaustivo, i costi di ricerca, copie, memoria multimediale, posta e consegna dei documenti, nonché eventuali spese legali applicabili.

Questo sito Web avrà accesso a tutte le informazioni che hai inviato o creato per il tempo ragionevolmente necessario per soddisfare o indagare su qualsiasi richiesta di informazioni o per proteggere questo sito Web. Al fine di far rispettare i presenti Termini di servizio, per proteggere i diritti di proprietà intellettuale, di rispettare i processi legali e la legge e per proteggere questo sito Web, l'utente accetta di consentire a questo sito Web di accedere alle proprie informazioni.

 **Indennizzo**

Nella misura consentita dalla legge applicabile, l'utente accetta di difendere, indennizzare e tenere indenne questo sito Web, il suo gestore del sito, la sua società madre, le sue affiliate, licenziatari, fornitori di servizi, funzionari, direttori, dipendenti, agenti, successori e assegnatari da e contro qualsiasi reclamo, danni, sentenze, premi, obblighi, perdite, responsabilità, costi o debito e spese (inclusi ma non limitati a spese legali) derivanti da: (i)violazione di qualsiasi diritto di terzi, inclusi, a titolo esemplificativo, qualsiasi diritto di copyright, proprietà o privacy; o (iv) qualsiasi reclamo che il tuo Contenuto abbia causato danni a terzi. Questo obbligo di difesa e indennizzo sopravvivrà a questi Termini di servizio e all'utilizzo di questo sito web. Accetti che avremo il solo diritto e l'obbligo di controllare la difesa legale da tali reclami, richieste o contenziosi, incluso il diritto di selezionare un avvocato di nostra scelta e di compromettere o risolvere tali reclami, richieste o contenziosi.

 **Le esclusie non sono che si e**

L'UTENTE UTILIZZARE IL SITO AL TUO RISCHIO SOLE. FORNIAMO IL SITO WEB “COME È” E “COME DISPONIBILI”. NELLA MISURA MENSILE CONSENTITA DALLA LEGGE, IL SITO WEB, DISCLIA TUTTE LE GARANZIE DI QUALSIASI TIPO RELATO RELATIVO AL SITO E BENE O SERVIZI OTTENUTI ATTRAVERSO IL SITO WEB, SIA ESPRESSE O IMPLICITE, INCLUSE, MA NON LIMITATE A, LE GARANZIE IMPLICITE DI COMMERCIO SARA' UNA RISPONSABILE RISPONSABILE DI QUALSIASI DANNO AL TUO SISTEMA COMPUTER O PERDITA DI DATI CHE RISULTATI DAL TUO USO DEL SITO WEB. NON RENDIAMO LA GARANZIA O LA RAPPRESENTAZIONE SULL'ACPI ACCURATEZZA O COMPLETENTE DEL CONTENUTO DEL SITO WEB O IL CONTENUTO DI QUALSIASI SITI LINKATI AL SITO WEB O CHE IL SITO WEB SIELDAMENTE LE VOSTRE PRESTRE E NON PERSONO RESPONSABILITA' O RESPONSABILITA' PER QUALSIASI (I) ERRORI,AMBITO, DI QUALSIASI NATURA CHE COSASQUESITE, RISULTARE DALLA VOSTRA ACCESSO E UTILIZZARE DEL SITO WEB O DEI NOSTRI SERVIZI, (III) QUALSIASI ACCESSO NON AUTORIZZATA O UTILIZZO DEI NOSTRI SERVIZI DI SESSIONE E/NOTUTTI INFORMATIVI PERSONALI E/O UTILI INFORMAZIONI FINANZIARIE CONSERVE, (IV) QUASI INTERRUZIONE O LA CESSAZIONEPERICOLI, TROJAN HORSES, O PIACE IL COME CHE POSSONO ESSERE TRASFORMATI O ATTRAVERSO IL SITO WEB O I NOSTRI SERVIZI DA QUALSIASI TERZA PARTE, E/O (V) QUALSIASI E QUALSIASI ERRORI O OMISSIONI IN QUALSIASI CONTENUTO O PER QUALSIASI PERDAGGIO O DANNO DI QUALSIASI TIPO INCURATO COME RISULTATO D’USUTENTESITO WEB O I NOSTRI SERVIZI. IL SITO WEB NON GARANTISCE, ENDORSE, GARANZIA O ASSUME RESPONSABILITA' PER QUALSIASI PRODOTTO O SERVIZIO ANNUNCIO O APPARTITO O APPARTITO DA UN TERZO PARTI ATTRAVERSO IL SITO WEB O I NOSTRI SERVIZI O QUALSIASI SERVIZI O QUALSIASI SERVIZI HYPEROCED O CARATTERISTICHE IN QUALSIAZZONZIO O AL PUBBLICLA TRANSAZIONE DINY TRA L'UTENTE E I PRODOTTI DI SEPARTI DI SEGNALTI O SERVIZI. COME IL ACQUISTO DI UN PRODOTTO O SERVIZIO ATTRAVERSO QUALSIASI MEDIO O IN QUALSIASI AMBIENTE, DEVI UTILIZZARE LA MIGLIORE GIUDIZIO ED ESORCENZA DOVE APPROPRIATO.

NESSUNA INFORMAZIONE OVOPINATA DAI DAI O ATTRAVERSO IL SITO WEB CREARE ALCUNA GARANZIA NON ESPRESSAMENTE STATIVA NELLE QUESTI TERMINI.

 **La limitazione della responsabilità**

IN NESSUN CASO IL SITO, I SUOI UFFICIALI, I SUOOI DIRETTORI, O DIPENDENTI ESSERE RESPONSABILI A TE PER QUALSIASI DIRETTO, INDIRETTO, INCIDENTE, SPECIALE, PUNITIVI O CONSEQUENZIALI DANNI CHE RISULTANO DA NESSUN (I) ERRORI, MISTAKES, O INACSIONICOSA IN CASA, RISULTANTE DALLA VOSTRA ACCESSO E UTILIZZO DEL NOSTRO SITO WEB O DEI SERVIZI, (III) QUALSIASI ACCESSO NON AUTORIZZATA O UTILIZZO DEI NOSTRI SERVER SICURE E/O QUALSIASI E TUTTE LE INFORMAZIONI PERSONALI E/O FINANZIARI INFORMAZIONI STOREDREIN, (IV) QUALSIA INTERRUZIONE O LA CESSAZIONE DI TRASMISSIONE O LA STORIZIONE DA O LA SITO WEBCOME, CUI POSSONO ESSERE TRASFORMATI O ATTRAVERSO IL NOSTRO SITO WEB O SERVIZI DA QUALSIASI TERZA PARTE, (V) QUALSIASI ERRORI O OMISSIONI IN QUALSIASI CONTENUTO O PERDAGGIO DI QUALSIASI PERDONE O DI QUALSIASI KINDURITO COME RISULTATO DEL TUO UTILIZZATO DI QUALSIASI CONTENUTO O DI UTILIZZOHANNO CON PUBBLICITAZIONI DI PVERTIMENTI O FORNITORI DI SERVIZIO, O SITI DI TERZA PARTE, TROVATI O ATTRAVERSO QUESTO SITO WEB, INCLUDE LA PAGAMENTO E CONSEGNA DI OBBLIGO CORRELATI O SERVIZI, E QUALSIASI ALTRI TERMINI, CONDIZIONI, POLITICHE, GARANZIE O DICHIARANTI ASPITATIIn PIORA, E NECESSO O NON IL SITO WEB O IL SITO O IL SITO L’OPERERTORE SITO SONO CONSIDERATI DELLA POSSIBILITA’ DI TALI DANNI. LA LIMITAZIONE DI RESPONSABILITA' DI LA RESPONSABILITA' SI APPLICATA ALIMENTE PIENO NEL PROPRIE DALLA LEGGE NELLA GIURISDIZIONE APPLICABILE.

 L'UTOI SPECIFICAMENTE ALLA RICONOSCE CHE QUESTO SITO WEB OFFICERS, DIRETTORI, DIPENDENTI, NON SARANNO RESPONSABILI AL CONTENUTO O PER IL DEFAMATORIO, UFFICIE, O ILLEGALE CONDUTA DI QUALSIASI PARETA TERZI, E CHE IL RISCHIO DI ARMA O DANNI DALLE RISPOSTE DI RISPOSTA CONTROLLARE.

 L’UTENTE ACKNOWLEGE CHE QUALSIASI CONTENUTO SCARICATO A QUESTO SITO WEB POTREBBE ESSERE VISTA, SCARICATO, CONDIVISO E DISTRIBUTO, POTENZIALMENTE IN VIOLAZIONE DEI VOSTI DIRITTI O QUESTO TERMINI DI SERVIZIO – E CHE L’UTENTE A SULLA SULLA RISCHIO SULLA RISCHIALI COME PARTI DI QUESTI TERMINE.

 L'UTENTE ACCETTA DI NON FIRE QUALSIASI INFORMATIVA DI ARBITRATO, LAWSUIT O PROVIZZANDO INCONSISTENTE CON LEIMITAZIONI DI RESPONSABILITA' FOREGO DI RESPONSABILITA'.

Qualsiasi reclamo da parte dell'utente che possa sorgere in relazione ai presenti Termini di servizio può essere compensabile da danni monetari fino a, se del caso, una tassa di abbonamento pagata negli ultimi dodici mesi e in nessun caso avrà diritto a un provvedimento ingiuntivo o altro equo.

 **Limitazione del tempo per i reclami**

RIGUARDA DI QUALSIASI STATUTE O LA DIRITTO ALLA CONTRAIZIONE, QUALSIASI CAUSA DI AZIONE O RICHIESTA L'UTENTE POTREBBE DI ARRIVARE O RELATIVA ALLE QUESTI TERMINI DI SERVIZIO O IL SITO WEB DEVE ESSERE COME DEGLI UN (1) ANNO DOPO LA CAUSA DELLE ACCURREZIONI, ALTRE O ALTRI CAUSA, CAUSA DI AZIONE O

 **Avviso di contenzioso**

L'utente dovrà informarci per iscritto entro due (2) giorni lavorativi dopo essere venuto a conoscenza o a conoscenza di, qualsiasi procedimento o procedimento minacciato, compresi qualsiasi contenzioso o procedimento governativo in sospeso nei confronti di voi che potrebbe influire negativamente negativamente sulla nostra attività, operazioni, prospettive, proprietà, beni o condizioni (finanziari o meno); e qualsiasi altro evento che possa influire materialmente negativamente sulla nostra attività, operazioni, prospettive, proprietà, attività o condizioni (finanziarie o meno).

**I tuoi commenti e preoccupazioni**

Questo sito Web è gestito da Aylo Freesites Ltd, Block 1, 195-197 Old Nicosia-Limassol Road, Dali Industrial Zone, Cyprus 2540. Tutte le comunicazioni di reclamo per violazione del copyright devono essere inviate all'agente del copyright indicato nella nostra [politica](https://www.pornhub.com/information/dmca) di [copyright](https://www.pornhub.com/information/dmca) nel modo e con i mezzi ivi stabiliti.

 Tutti gli altri feedback, commenti, richieste di supporto tecnico e altre comunicazioni relative a questo sito Web devono essere indirizzati a: [support-pornhub.com.](mailto:support@pornhub.com)

 **Assegnazione**

I presenti Termini di servizio e tutti i diritti e le licenze concessi ai sensi del presente documento non possono essere trasferiti o assegnati da te, ma possono essere assegnati da noi senza restrizioni.

 **Accordo arbitrato e rinuncia a determinati diritti (USA)**

Questa sezione si applica solo agli Utenti situati negli Stati Uniti d'America.

Ad eccezione di quanto stabilito quiinabove, tu e noi accettiamo che risolveremo qualsiasi controversia tra di noi (incluse eventuali controversie tra te e un nostro agente di terze parti) attraverso un arbitrato vincolante e definitivo invece che attraverso procedimenti giudiziari. Tu e la presente rinunciamo a qualsiasi diritto a un processo con giuria di qualsiasi reclamo (definito di seguito). Tutte le controversie, reclami, controversie o altre controversie derivanti tra te e noi o te e un nostro agente terzo (ciascuno un **Claim**"Reclamo") saranno sottoposte per arbitrato vincolante in conformità con le Regole dell'Associazione Arbitrale Americana **("Regole AAA").** L'arbitrato sarà ascoltato e determinato da un singolo arbitro. La decisione dell'arbitro in tale arbitrato sarà definitiva e vincolante per le parti e può essere eseguita in qualsiasi tribunale della giurisdizione competente. Tu e noi accettiamo che il procedimento arbitrale sarà mantenuto confidenziale e che l'esistenza del procedimento e di qualsiasi elemento di esso (incluse, a titolo esemplificativo, qualsiasi dichiarazione, brief o altri documenti presentati o scambiati e qualsiasi testimonianza o altre osservazioni o altre osservazioni orali e lodi) non saranno divulgate al di fuori dei procedimenti arbitrali, ad eccezione di quanto possa essere legalmente richiesto nei procedimenti giudiziari relativi all'arbitrato, da norme di divulgazione applicabili e regolamenti delle autorità di regolamentazione dei titoli o di altre agenzie governative. La legge federale sull'arbitrato e la legge federale sull'arbitrato si applicano a questo accordo. Tuttavia, l'arbitro, e non qualsiasi tribunale o agenzia federale, statale o locale, avrà l'autorità esclusiva per risolvere qualsiasi controversia relativa all'interpretazione, all'applicabilità, all'applicabilità o alla formazione di questi Termini di servizio, incluso, ma non limitato a, un reclamo che tutta o parte di questi Termini di servizio è nullo o annullabile.

Se dimostri che i costi dell'arbitrato saranno proibitivi rispetto ai costi del contenzioso, pagheremo la maggior parte dei costi amministrativi e delle commissioni dell'arbitro necessarie per l'arbitrato che l'arbitro ritiene necessario per impedire che il costo dell'arbitrato sia proibitivo. Nel lodo finale, l'arbitro può ripartire i costi dell'arbitrato e il risarcimento dell'arbitro tra le parti in importi che l'arbitro ritiene appropriati.

Questo accordo di arbitrato ai sensi della presente sezione non impedisce a nessuna delle parti di cercare un'azione da parte di agenzie governative federali, statali o locali. Tu e noi abbiamo anche il diritto di presentare reclami qualificanti in tribunale per le controversie di modesta'. Inoltre, tu e noi conserviamo il diritto di applicarvi a qualsiasi tribunale della giurisdizione competente per il sollievo provvisorio, inclusi gli allegati pre-arbitrale o le ingiunzioni preliminari, e qualsiasi richiesta di questo tipo non sarà ritenuta incompatibile con i presenti Termini di servizio, né una rinuncia al diritto di presentare controversie inviate all'arbitrato come previsto in questi Termini di servizio.

Neither you nor we may act as a class representative or private attorney general, nor participate as a member of a class of claimants, with respect to any Claim. Claims may not be arbitrated on a class or representative basis. The arbitrator can decide only your and/or our individual Claims. The arbitrator may not consolidate or join the claims of other persons or parties who may be similarly situated. The arbitrator may award in the arbitration the same damages or other relief available under applicable law, including injunctive and declaratory relief, as if the action were brought in court on an individual basis. Notwithstanding anything to the contrary in the foregoing or herein, the arbitrator may not issue a “public injunction” and any such “public injunction” may be awarded only by a federal or state court. If either party seeks a “public injunction,” all other claims and prayers for relief must be adjudicated in arbitration first and any prayer or claim for a “public injunction” in federal or state court stayed until the arbitration is completed, after which the federal or state court can adjudicate the party’s claim or prayer for “public injunctive relief.” In doing so, the federal or state court is bound under principles of claim or issue preclusion by the decision of the arbitrator.

If any provision of this section is found to be invalid or unenforceable, then that specific provision shall be of no force and effect and shall be severed, but the remainder of this Section shall continue in full force and effect. No waiver of any provision of this Section of the Terms of Service will be effective or enforceable unless recorded in a writing signed by the party waiving such a right or requirement. Such a waiver shall not waive or affect any other portion of this Terms of Service. This Section of the Terms will survive the termination of your relationship with us.

 THIS SECTION LIMITS CERTAIN RIGHTS, INCLUDING THE RIGHT TO MAINTAIN A COURT ACTION, THE RIGHT TO A JURY TRIAL, THE RIGHT TO PARTICIPATE IN ANY FORM OF CLASS OR REPRESENTATIVE CLAIM, THE RIGHT TO ENGAGE IN DISCOVERY EXCEPT AS PROVIDED IN AAA RULES, AND THE RIGHT TO CERTAIN REMEDIES AND FORMS OF RELIEF. OTHER RIGHTS THAT YOU OR WE WOULD HAVE IN COURT ALSO MAY NOT BE AVAILABLE IN ARBITRATION.

 **Arbitration Agreement & Waiver of Certain Rights (EU)**

This section shall only apply to Users located in the European Union.

 We cooperate with out-of-court dispute settlement bodies (**“Dispute Settlement Bodies”**) that have been certified in accordance with Art. 21(3) of the DSA. The European Commission publishes a list of these bodies.

If you have your place of establishment or are located in the European Union, you have the right to select a Dispute Settlement Body to assist in resolving disputes relating to decisions previously taken by us regarding Content uploaded by you, or notices you submitted to us. This includes cases in which complaints have remained unresolved by our internal complaint-handling system, as described in section “Complaint Handling Procedure”.

We reserve the right to refuse to cooperate with your selected Dispute Settlement Body if:

* A dispute has already been resolved or is already subject to an ongoing procedure before a competent court of relevant jurisdiction, or before another Dispute Settlement Body.
* The Dispute Settlement Body has been contacted after the six-month period from notification to you of our decision has lapsed, and you have not previously filed a complaint through our internal complaint-handling system over a particular issue.

 Any decisions taken by Dispute Settlement Bodies shall not be binding on either you or us.

 We are neither willing nor obligated to participate in dispute resolution proceedings with consumers before a consumer arbitration board under the EU Directive on Consumer ADR.

 **Miscellaneous**

These Terms of Service, your use of this Website, and the relationship between you and us shall be governed by the laws of the Republic of Cyprus, without regard to conflict of law rules. Nothing contained in these Terms of Service shall constitute an agreement to the application of the laws of any other nation to this Website. You agree that this Website shall be deemed a passive Website that does not give rise to personal jurisdiction over us, either specific or general, in jurisdictions other than the Republic of Cyprus. The sole and exclusive jurisdiction and venue for any action or proceeding arising out of or related to these Terms of Service shall be in an appropriate court located in Limassol, Cyprus. You hereby submit to the jurisdiction and venue of said Courts.

 Nessuna rinuncia da parte nostra a qualsiasi termine o condizione stabilito nei presenti Termini di servizio sarà considerata una rinuncia ulteriore o continua a tale termine o condizione o una rinuncia a qualsiasi altro termine o condizione, e qualsiasi incapacità da parte nostra di far valere un diritto o una fornitura ai sensi dei presenti Termini di servizio non costituirà una rinuncia a tale diritto o disposizione.

Se una qualsiasi disposizione dei presenti Termini di servizio è ritenuta non valida da un tribunale della giurisdizione competente, l'invalidità di tale disposizione non pregiudica la validità delle restanti disposizioni dei presenti Termini di servizio, che rimarranno in vigore a pieno titolo ed effetto.

 I Termini di servizio, la nostra [Informativa sulla privacy,](https://www.pornhub.com/information/privacy) la nostra [Informativa sul copyright](https://www.pornhub.com/information/dmca) e tutti i documenti che incorporano espressamente per riferimento costituiscono l'unico e completo accordo tra te e noi in relazione a questo sito Web.

 Possiamo risolvere i presenti Termini di servizio per qualsiasi motivo o senza motivo in qualsiasi momento notificando l'utente tramite un avviso su questo sito Web, via e-mail o con qualsiasi altro metodo di comunicazione. Qualsiasi risoluzione di questo tipo non pregiudicherà i nostri diritti, rimedi, rivendicazioni o difese qui di seguito. In caso di cessazione dei Termini di servizio, non avrai più il diritto di accedere al tuo account o al tuo Contenuto. Non avremo alcun obbligo di aiutarti a migrare i tuoi dati o i tuoi Contenuti e potremmo non tenere il backup di nessuno dei tuoi Contenuti. Non ci assumiamo alcuna responsabilità per l'eliminazione dei tuoi contenuti ai sensi dei presenti Termini di servizio. Si noti che, anche se il tuo Contenuto viene eliminato dai nostri server attivi, potrebbe rimanere nei nostri archivi (ma non abbiamo alcun obbligo di archiviare o inviare il backup dei tuoi Contenuti) e soggetto alle licenze stabilite nei presenti Termini di servizio.