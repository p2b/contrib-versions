Villkor och meddelanden för webbplatsen Tripadvisor
===================================================

Tripadvisors användarvillkor

Senast uppdaterade: 16 februari 2024

**Användarvillkor**

[ANVÄNDA TJÄNSTERNA](#_USE_OF_THE)

[TILLÄGGSPRODUKTER](#_ADDITIONAL_PRODUCTS)

[FÖRBJUDNA AKTIVITETER](#_PROHIBITED_ACTIVITIES)

[SEKRETESSPOLICY OCH UTLÄMNANDEN](#_PRIVACY_POLICY_AND)

[OMDÖMEN, KOMMENTARER OCH ANVÄNDNING AV ANDRA INTERAKTIVA OMRÅDEN; BEVILJANDE AV LICENS](#_REVIEWS,_COMMENTS_AND)

* [Begränsa Tripadvisors licensrättigheter](#_Restricting_Tripadvisor%E2%80%99s_Licence)

[BOKNING MED TREDJEPARTSLEVERANTÖRER VIA Tripadvisor](#_BOOKING_WITH_THIRD-PARTY)

* [Använda Tripadvisors bokningstjänster](#_Use_of_Tripadvisor)
* [Tredjepartsleverantörer](#_Third-Party_Suppliers._The)
* [Boka semesterbostäder, restauranger och upplevelser med tredjepartsleverantörer på företagsfilialers webbplatser](#_Booking_Holiday_Rentals,)

[RESMÅL](#_TRAVEL_DESTINATIONS)

* [Internationella resor](#_International_Travel._When)

[ANSVARSFRISKRIVNING](#_LIABILITY_DISCLAIMER_1)

[SKADEERSÄTTNING](#_Indemnification)

[LÄNKAR TILL TREDJEPARTSWEBBPLATSER](#_LINKS_TO_THIRD-PARTY)

[PROGRAMVARA SOM EN DEL AV TJÄNSTER; YTTERLIGARE MOBILA LICENSER](#_SOFTWARE_AS_PART)

[UPPHOVSRÄTTS- OCH VARUMÄRKESMEDDELANDEN](#_COPYRIGHT_AND_TRADEMARK)

* [Riktlinjer för anmälan om och borttagning av olagligt innehåll](#_Notice_and_Take-Down)

[ÄNDRINGAR I DETTA AVTAL; TJÄNSTER; UPPDATERINGAR; UPPSÄGNING OCH TILLBAKADRAGANDE](#_MODIFICATIONS_TO_THE)

[JURISDIKTION OCH GÄLLANDE LAG](#_JURISDICTION_AND_GOVERNING)

[VALUTAOMVANDLARE](#_CURRENCY_CONVERTER)

[ALLMÄNNA BESTÄMMELSER](#_GENERAL_PROVISIONS)

[TJÄNSTEHJÄLP](#_SERVICE_HELP)

Välkommen till Tripadvisors webbplatser och mobiltjänster på www.Tripadvisor.se och tillämpliga landsdomäner på högsta nivå (inklusive underdomäner som är kopplade till dem), relaterade programvaror (ibland kallade ”appar”), data, sms, API:er, e-post, chatt- och telefonkorrespondens, knappar, widgetar och annonser (alla dessa objekt ska häri benämnas som ”**Tjänsterna**”; mer generellt ska Tripadvisors webbplatser och mobiltjänster hädanefter benämnas som ”webbplatser”).

Tjänsterna tillhandahålls av Tripadvisor LLC, ett Delaware-aktiebolag beläget i USA (”**Tripadvisor**”) med huvudkontor på \[Tripadvisor LLC, 400 1st Avenue, Needham, MA 02494, USA\]. Begreppen ”vi”, ”oss” och ”vår” ska tolkas i enlighet med detta.

Genom att skapa ett Tripadvisor-konto samtycker du till att omfattas av de regler, villkor och meddelanden som anges nedan (tillsammans, detta ”Avtal”). Detta Avtal gäller inte om du bara besöker webbplatserna eller använder Tjänsterna utan att skapa ett Tripadvisor-konto.  Läs noga igenom detta Avtal, då det innehåller information gällande dina rättigheter enligt lag samt begränsningar av dessa rättigheter. Det innehåller även ett avsnitt om gällande lagar och jurisdiktion vid eventuella tvister. Om du är konsument i EU eller Storbritannien har du en lagstadgad rätt till tillbakadragande från detta Avtal inom 14 dagar efter att Avtalet har slutits. Du kan göra det eller säga upp detta Avtal när som helst genom att avsluta ditt konto (genom att kontakta oss eller genom att gå till ”Kontoinformation” när du är inloggad och välja alternativet ”Avsluta konto”) och inte längre besöka eller använda Tjänsterna.

Om du är konsument i EU eller Storbritannien har du vissa obligatoriska konsumenträttigheter. Om du är konsument som bor någon annanstans kan du även ha konsumenträttigheter enligt lagstiftningen i ditt område. Inget i detta Avtal påverkar dina obligatoriska konsumenträttigheter.

All(a) information, text, länkar, grafik, bilder, ljud, videoklipp, data, kod eller annat material eller arrangemang av material som du kan visa, komma åt eller på annat sätt interagera med genom Tjänsterna ska benämnas som ”Innehåll”. ”Tjänsterna”, enligt definitionen ovan, avser de som tillhandahålls av Tripadvisor eller våra företagsfilialer (vid hänvisning till Tripadvisor och sådana parter ska de gemensamt definieras som ”Tripadvisor-företag”). För att undvika oklarheter ägs och regleras alla webbplatserna av Tripadvisor.  Vissa specifika Tjänster som görs tillgängliga via webbplatserna kan dock ägas och regleras av Tripadvisors företagsfilialer, till exempel Tjänster som möjliggör [**bokning av semesterbostäder, restauranger och upplevelser med tredjepartsleverantörer**](https://tripadvisor.mediaroom.com/se-terms-of-use#OLE_LINK11) (se nedan). Som en del av våra Tjänster kanske vi skickar meddelanden till dig om specialerbjudanden, produkter eller extra tjänster som är tillgängliga från oss, våra filialer eller våra partner, som kan vara av intresse för dig. Sådana meddelanden skickas vanligtvis via nyhetsbrev och marknadsföringsmeddelanden och är i syfte att lära känna dig och dina preferenser bättre, både vad gäller våra egna och våra filialers Tjänster. I sin tur möjliggör detta anpassning av tjänsterna enligt dessa preferenser.

Termen ”du” eller ”användare” avser den individ, det företag, den företagsorganisation eller den juridiska person som har skapat ett Tripadvisor-konto och använder Tjänsterna och/eller bidrar med Innehåll till dem. Det Innehåll som du bidrar med, skickar in, överför och/eller lägger till i eller genom Tjänsterna ska benämnas som ”ditt Innehåll”, ”Innehåll tillhörande dig” och/eller ”Innehåll du skickar in”.

Tjänsterna tillhandahålls kontinuerligt och endast för att:

1. hjälpa användare att samla in reseinformation, publicera Innehåll och söka efter och boka resetjänster och bokningar; och
2. hjälpa rese-, turism- och restaurangverksamheter att interagera med användare och potentiella kunder genom kostnadsfria tjänster och/eller betaltjänster som erbjuds av eller genom Tripadvisor-företagen.

#### Produkter, tjänster, information och Innehåll i våra Tjänster ändras regelbundet så att de är uppdaterade (till exempel med aktuell information och aktuella erbjudanden). Det innebär t.ex. att nya transporttjänster, boenden, restauranger, rundturer, aktiviteter eller upplevelser som tillhandahålls av tredje parter kan bli tillgängliga för dig för bokning medan andra tjänster kanske blir otillgängliga.

Vi kan i framtiden komma att ändra eller på annat sätt uppdatera detta Avtal i enlighet med reglerna och villkoren häri. Du förstår och samtycker till att fortsatt åtkomst till och användning av Tjänsterna efter en sådan ändring innebär att du samtycker till det ändrade eller uppdaterade Avtalet. Längst ned i detta Avtal anger vi det datum då Avtalet senast ändrades och uppdateringar träder i kraft vid publicering. Vi meddelar dig om väsentliga ändringar av dessa regler och villkor genom att antingen skicka ett meddelande till den e-postadress som är kopplad till din profil eller genom att publicera ett meddelande på våra webbplatser. Kom tillbaka till den här sidan med jämna mellanrum för att granska den senaste versionen av detta Avtal. Om du inte vill samtycka till några ändringar av detta Avtal kan du avsluta ditt konto och sluta använda Tjänsterna (det finns instruktioner för detta [här](https://www.tripadvisorsupport.com/sv-SE/hc/traveler/articles/510)).

**ANVÄNDA TJÄNSTERNA**

Som ett villkor för din användning av Tjänsterna samtycker du till (i) att all information som du tillhandahåller till Tripadvisor-företagen via Tjänsterna är sann, korrekt, aktuell och fullständig, (ii) att skydda din kontoinformation och övervaka och helt ansvara för all användning av ditt konto av andra än dig och (iii) att du är minst 13 år. Tripadvisor-företagen samlar inte medvetet in information från någon under 13 år. Om du är konsument som är bosatt i EU eller Storbritannien kan vi neka dig tillgång till Tjänsterna om du bryter mot detta Avtal eller av någon annan rimlig anledning. Om du inte är konsument som är bosatt i EU eller Storbritannien förbehåller vi oss rätten att efter eget gottfinnande neka dig tillgång till Tjänsterna, när som helst och oavsett anledning, inklusive men inte begränsat till för brott mot detta Avtal. Genom att använda Tjänsterna, inklusive eventuella produkter eller tjänster som möjliggör delningen av Innehåll till eller från tredjepartswebbplatser, förstår du att du är ensamt ansvarig för all information du delar med Tripadvisor-företagen. Du får enbart komma åt Tjänsterna på det sätt som avsetts genom Tjänsternas tillhandahållna funktionalitet och enligt vad som tillåts i detta Avtal.

Kopiering, överföring, reproduktion, replikation, publicering eller omdistribution av (a) innehåll eller delar av det och/eller (b) tjänsterna mer generellt sett är strängt förbjudet utan föregående skriftligt tillstånd från Tripadvisor-företagen. Om du vill be om tillstånd ska du skicka din begäran till:

Director, Partnerships and Business Development

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, USA

För att få tillgång till vissa funktioner i Tjänsterna måste du skapa ett konto. När du skapar ett konto måste du uppge fullständig och korrekt information. Du är ensamt ansvarig för den aktivitet som sker på ditt konto, inklusive interaktion och kommunikation med andra, och du måste skydda ditt konto. I detta syfte samtycker du till att hålla dina kontaktuppgifter uppdaterade. 

Om du skapar ett Tripadvisor-konto för kommersiella ändamål och godkänner detta Avtal på uppdrag av ett företag, en organisation eller annan juridisk person intygar du att du är auktoriserad att göra detta och har behörighet att binda parten till detta Avtal, i vilket fall ”du” och ”din” såsom de används i detta Avtal ska hänvisa till denna part och den individ som agerar på uppdrag av företaget ska benämnas som ”Företagsrepresentant.”

När du använder Tjänsterna kan du stöta på länkar till sidor och appar från tredje part eller få möjlighet att interagera med sidor och appar från tredje part. Detta kan omfatta möjligheten att dela Innehåll från Tjänsterna, inklusive ditt Innehåll, med sådana sidor och appar från tredje part. Du bör vara medveten om att sidor och appar från tredje part kan visa sådant delat Innehåll offentligt. Sådana tredje parter kan debitera en avgift för användning av visst innehåll eller vissa tjänster på eller genom sina webbplatser. Du bör därför göra vad du anser vara nödvändiga eller lämpliga undersökningar innan du genomför transaktioner med en tredje part så att du är medveten om huruvida du kommer att debiteras. Där Tripadvisor-företagen anger information om avgifter för denna typ av innehåll eller tjänster från tredje part anges denna information enbart i praktiskt och informerande syfte. All interaktion med sidor och appar från tredje part sker på egen risk. Du samtycker uttryckligen till och godkänner att Tripadvisor-företagen inte på något sätt kan hållas ansvariga för sådana sidor och appar från tredje part.

Tripadvisor-företag rangordnar och visar objekt för dig med hjälp av rekommendationssystem. Olika kategorier av objekt (t.ex. hotell och restauranger, saker att göra och flygresor) rangordnas baserat på olika kriterier, t.ex. pris, ”resenärers favoriter” och varaktighet. Fullständig information om vår policy och våra rangordningskriterier finns på [sidan Så här fungerar webbplatsen](https://www.tripadvisor.se/pages/service_sv.html).

Visst Innehåll som du ser eller på annat sätt kommer åt i eller via Tjänsterna används för kommersiella ändamål. Du samtycker till och förstår att Tripadvisor-företagen kan visa annonser och kampanjer i Tjänsterna bredvid, nära, intill eller på annat sätt i närheten av ditt Innehåll (inklusive, för videoklipp eller annat dynamiskt innehåll, innan, under eller efter dess visning), samt andras Innehåll.

**TILLÄGGSPRODUKTER**

Tripadvisor-företagen kan då och då bestämma sig för att ändra, uppdatera eller avsluta vissa produkter och funktioner i Tjänsterna. Du samtycker till och förstår att Tripadvisor-företagen inte har någon skyldighet att lagra eller behålla ditt Innehåll eller annan information du uppger, utom i den utsträckning som krävs enligt tillämplig lag.

Vi erbjuder även andra tjänster som regleras av ytterligare villkor eller avtal. Om du använder andra sådana tjänster, görs de ytterligare villkoren tillgängliga och blir del av detta Avtal, förutom om sådana ytterligare villkor uttryckligen exkluderar eller på annat sätt ersätter detta Avtal. Om du till exempel använder eller köper sådana tilläggstjänster för kommersiellt ändamål eller affärsändamål, måste du godkänna de tillämpliga ytterligare villkoren. I den utsträckning övriga villkor strider mot villkoren i detta Avtal ska de ytterligare villkoren gälla enligt konfliktens utsträckning när det gäller dessa särskilda tjänster.

**FÖRBJUDNA AKTIVITETER**

Det Innehåll och den information som är tillgänglig i och via Tjänsterna (inklusive men inte begränsat till meddelanden, data, information, text, musik, ljud, bilder, grafik, video, kartor, ikoner, programvara, kod eller annat material) och den infrastruktur som används för att tillhandahålla sådant Innehåll och sådan information tillhör Tripadvisor-företagen eller är licensierad till Tripadvisor-företagen av tredjeparter. Vad gäller allt annat Innehåll än ditt Innehåll samtycker du till att inte på annat sätt ändra, kopiera, distribuera, överföra, visa, framföra, reproducera, publicera, licensiera, skapa härledda verk från eller sälja eller sälja vidare information, programvara, produkter eller tjänster som har erhållits från eller via Tjänsterna. Du samtycker även till att inte göra följande:

*  (i) använda Tjänsterna eller Innehåll för kommersiellt syfte, utom omfattningen för de kommersiella ändamål som uttryckligen tillåts enligt detta Avtal och relaterade riktlinjer som gjorts tillgängliga av Tripadvisor-företagen;
* (ii) komma åt, övervaka, reproducera, distribuera, överföra, sända, visa, sälja, licensiera, kopiera eller på annat sätt utnyttja något Innehåll i Tjänsterna, bland annat användarprofiler och bilder, med hjälp av någon robot, spindel, skrapa eller annan automatiserad metod eller manuell process för något syfte som inte omfattas av detta Avtal eller utan vårt uttryckliga skriftliga tillstånd;
* (iii) Bryta mot restriktionerna i något robotförbud i Tjänsterna eller kringgå andra åtgärder som syftar till att förhindra eller begränsa åtkomst till Tjänsterna.
* (iv) vidta åtgärder som ålägger eller kan ålägga, efter vårt eget gottfinnande (eller, om du är konsument bosatt i Storbritannien eller EU, efter vårt rimliga gottfinnande), en orimlig eller oproportionerligt stor belastning på vår infrastruktur;
* (v) Skapa djupa länkar till någon del av Tjänsterna i något syfte utan vårt uttryckliga skriftliga tillstånd.
* (vi) ”Avbilda”, ”spegla” eller på annat sätt införliva någon del av Tjänsterna på/i en annan webbplats eller tjänst utan vårt föregående skriftliga tillstånd.
* (vii) försöka ändra, översätta, anpassa, redigera, bryta ned, plocka isär eller använda omvänd ingenjörskonst på någon programvara som används av Tripadvisor-företagen i anslutning till Tjänsterna;
* (viii) kringgå, inaktivera eller på annat sätt störa säkerhetsrelaterade funktioner i Tjänsterna eller funktioner som förhindrar eller begränsar användning eller kopiering av Innehåll;
* (ix) ladda ned något Innehåll såvida det inte uttryckligen görs tillgängligt för nedladdning av Tripadvisor-företagen; eller
* (x) använda, eller hjälpa, uppmuntra eller göra det möjligt för en tredje part att använda, en robot, en spindel, ett AI-system (artificiell intelligens) eller någon annan automatiserad enhet eller behandla eller ämna komma åt, hämta, kopiera, skrapa, samla, ladda ned eller på annat sätt indexera någon del av Tjänsterna eller något Innehåll, förutom enligt vad som uttryckligen tillåts av Tripadvisor skriftligen.

**SEKRETESSPOLICY OCH UTLÄMNANDEN**

Personuppgifter som du publicerar i eller på annat sätt skickar in i samband med Tjänsterna kommer att användas enligt vår Sekretess- och cookiepolicy. Klicka [här](https://tripadvisor.mediaroom.com/se-privacy-policy) för att se vår Sekretess- och cookiepolicy.

**OMDÖMEN, KOMMENTARER OCH ANVÄNDNING AV ANDRA INTERAKTIVA OMRÅDEN; BEVILJANDE AV LICENS**

Vi uppskattar dina åsikter. Du bör vara medveten om att genom att tillhandahålla ditt Innehåll i eller via Tjänsterna, vare sig det sker via e-post, publicering via Tripadvisors synkroniseringsprodukt, via andras tjänster och program eller på annat sätt, inklusive någon del av ditt Innehåll som överförs till ditt Tripadvisor-konto genom någon av Tripadvisor-företagens produkter eller tjänster, omdömen, frågor, bilder eller videoklipp, kommentarer, förslag, idéer eller liknande i något av ditt Innehåll, ger du Tripadvisor-företagen en icke-exklusiv, royaltyfri, oupphörlig, överförbar, oåterkallelig och helt underlicensierbar rätt att (a) verka som värd för, använda, reproducera, ändra, köra, anpassa, översätta, distribuera, publicera, skapa härledda verk från och offentligt visa och framföra sådant Innehåll tillhörande dig över hela världen i alla typer av befintliga eller framtida medier, (b) göra ditt Innehåll tillgängligt för resten av världen och låta andra göra detsamma, (c) tillhandahålla, marknadsföra och förbättra Tjänsterna och göra ditt Innehåll som delas i Tjänsterna tillgängligt för andra företag, organisationer eller individer för syndikering, sändning, distribution, marknadsföring eller publicering av sådant Innehåll tillhörande dig i andra medier och tjänster, enligt vår Sekretesspolicy och detta Avtal, och (d) använda det namn och/eller varumärke som du skickar in i samband med sådant Innehåll tillhörande dig. Du samtycker till att Tripadvisor kan välja att tillhandahålla tillskrivning av ditt Innehåll efter eget gottfinnande (eller, om du är konsument bosatt i Storbritannien eller EU, efter vårt rimliga gottfinnande). Du ger dessutom Tripadvisor-företagen rätt att vidta juridiska åtgärder mot en fysisk eller juridisk person som bryter mot dina eller Tripadvisor-företagens rättigheter i ditt Innehåll genom ett brott mot detta Avtal. Du godkänner och samtycker till att ditt Innehåll inte är konfidentiellt eller äganderättsligt. Du intygar att du äger eller har de licenser, rättigheter (inklusive upphovsrätt och andra äganderätter), tillstånd och behörigheter som krävs för att publicera och på annat sätt använda (och för att Tripadvisor-företagen ska kunna publicera och på annat sätt använda) ditt Innehåll enligt vad som auktoriseras i detta Avtal.

Om det fastställs att du har ideell upphovsrätt (inklusive tillskrivnings- eller sekretessrättigheter) för ditt Innehåll intygar du härmed att, i den utsträckning som tillåts enligt gällande lag, (a) du inte kräver att någon personligt identifierbar information ska användas i anslutning till Innehållet eller några av dess härledda verk, uppgraderingar eller uppdateringar, (b) att du inte motsätter dig att Tripadvisor-företagen eller deras licensinnehavare, efterträdare och ombud publicerar, använder, ändrar, raderar och utnyttjar ditt Innehåll, (c) att du för all framtid avsäger dig och samtycker till att inte göra anspråk på eller hävda någon ideell upphovsrätt som författare för någon del av ditt Innehåll och (d) att du för all framtid befriar Tripadvisor-företagen och dess licensinnehavare, efterträdare och ombud från eventuella anspråk som du i annat fall skulle kunna framföra mot Tripadvisor-företagen i kraft av sådan ideell upphovsrätt.

Observera att all feedback och andra förslag du tillhandahåller när som helst kan användas och att vi inte har någon skyldighet att hålla dem konfidentiella.

Tjänsterna kan omfatta diskussionsforum, anslagstavlor, omdömestjänster, reseflöden eller andra forum där du kan publicera ditt Innehåll, t.ex. omdömen om reseupplevelser, meddelanden, material eller andra objekt (”Interaktiva områden”). Om Tripadvisor tillhandahåller sådana Interaktiva områden på webbplatserna är du ensamt ansvarig för din användning av sådana Interaktiva områden och du använder dem på egen risk. Tripadvisor-företagen garanterar inte någon konfidentialitet med hänsyn till något av ditt Innehåll som du tillhandahåller i Tjänsterna eller i något Interaktivt område.  I den utsträckning som en enhet som utgör ett av Tripadvisor-företagen tillhandahåller någon form av privat kommunikationskanal mellan användare samtycker du till att denna/dessa enhet(er) kan övervaka innehållet i kommunikationen i syfte att bidra till att skydda vårt forum och Tjänsterna. Du förstår att Tripadvisor-företagen inte redigerar eller kontrollerar de användarmeddelanden som publiceras eller distribueras via Tjänsterna, inklusive via chattrum, elektroniska anslagstavlor eller andra kommunikationsforum, och att vi inte på något sätt är ansvariga eller ansvarsskyldiga för sådana meddelanden.  Tripadvisor redigerar eller kontrollerar i synnerhet inte användares Innehåll som visas på webbplatserna.  Tripadvisor-företagen förbehåller sig dock rätten att utan föregående meddelande ta bort och/eller begränsa sådana meddelanden eller annat Innehåll från Tjänsterna, om de i god tro anser att sådant Innehåll bryter mot detta Avtal eller på annat sätt anser att borttagningen och/eller begränsningen är skäligen nödvändig för att skydda rättigheterna för Tripadvisor-företagen och/eller andra användare av Tjänsterna. Om du inte håller med om att ditt Innehåll bör tas bort från webbplatserna kan du överklaga vårt beslut via Tripadvisors interna process för hantering av klagomål. Du hittar mer information om hur du invänder mot och överklagar ett beslut om att ta bort eller begränsa ditt Innehåll [här](https://www.tripadvisorsupport.com/sv-SE/hc/traveler/articles/623). Genom att använda Interaktiva områden samtycker du uttryckligen till att endast skicka in Innehåll tillhörande dig som följer Tripadvisors riktlinjer för publicering, som gäller vid tidpunkten för inskicket och som gjorts tillgängliga för dig av Tripadvisor.  Du samtycker uttryckligen till att inte publicera, ladda upp till, överföra, distribuera, lagra, skapa eller på annat sätt publicera via Tjänsterna något Innehåll tillhörande dig som

1. är falskt, oäkta, vilseledande, ärekränkande, nedsättande, obscent, pornografiskt, oanständigt, otuktigt, suggestivt, trakasserande (eller som förespråkar trakasserier av en annan person), hotfullt, kränkande mot sekretess eller publiceringsrättigheter, smädligt, provocerande, bedrägligt eller på annat sätt stötande
2. uppenbarligen är stötande mot onlineforumet, till exempel som främjar rasism, bigotteri, hat eller fysisk skada av något slag mot någon grupp eller person
3. kan utgöra, uppmuntra, främja eller tillhandahålla instruktioner för en olaglig aktivitet, en brottslig handling, utgöra grund för skadeståndsansvar, kränka rättigheterna för någon part i något land i världen eller som på annat sätt skulle skapa ansvarsskyldighet eller bryta mot lokala, nationella eller internationella lagar, inklusive och utan begränsning bestämmelser från United States Securities and Exchange Commission (SEC) eller bestämmelser för någon typ av säkerhetsutbyte, inklusive men inte begränsat till New York Stock Exchange (NYSE), NASDAQ eller London Stock Exchange;
4. ger instruktioner om olaglig verksamhet såsom att tillverka eller köpa olagliga vapen, kränka någons sekretess eller tillhandahålla eller skapa datorvirus;
5. kan inkräkta på någon parts patent, affärshemligheter, upphovsrätt eller andra immateriella rättigheter eller ägarrättigheter, särskilt innehåll som främjar olaglig eller obehörig kopiering av någon annans upphovsrättsskyddade verk, såsom att tillhandahålla piratprogram eller länkar till dem, tillhandahålla information till enheter som kringgår tillverkarinstallerade kopieringsskydd eller tillhandahålla piratmusik eller länkar till musikfiler för piratkopiering;
6. utgör massmeddelanden eller ”spam”, ”skräppost”, ”kedjebrev” eller ”pyramidspel”;
7. efterliknar en person eller enhet eller på annat sätt felaktigt framställer din koppling till en person eller enhet, inklusive Tripadvisor-företagen
8. utgör privat information från någon tredje part, inklusive men inte begränsat till adresser, telefonnummer, e-postadresser, personnummer och kreditkortsnummer.  observera att en individs efternamn kan publiceras på våra webbplatser, men endast då den identifierade individen på förhand uttryckligen givit sin tillåtelse till detta
9. innehåller begränsade eller lösenordsskyddade sidor eller dolda sidor eller bilder (som inte länkas till eller från en annan åtkomlig sida);
10. innehåller eller har som syfte att underlätta virus, korrupta data eller andra skadliga, störande eller destruktiva filer
11. inte är relaterat till ämnet för de Interaktiva områden där sådant Innehåll publiceras
12. helt efter Tripadvisors gottfinnande (eller, om du är konsument bosatt i Storbritannien eller EU, efter vårt rimliga omdöme) (a) bryter mot de tidigare underavsnitten häri, (b) strider mot Tripadvisors relaterade riktlinjer som gjorts tillgängliga för dig av Tripadvisor, (c) är stötande, (d) begränsar eller hämmar någon annan person från att använda eller utnyttja de Interaktiva områdena eller någon annan aspekt av Tjänsterna eller (e) kan utsätta något av Tripadvisor-företagen eller deras användare för skada eller ansvarsskyldighet av något slag.

I den mån som tillåts enligt tillämplig lagstiftning tar Tripadvisor-företagen inget som helst ansvar för något Innehåll som publiceras, lagras, överförs eller laddas upp till Tjänsterna av dig (vad gäller ditt Innehåll) eller av någon tredje part (vad gäller samtligt Innehåll, mer generellt sett) eller för förlust eller skada av detta, och Tripadvisor-företagen är heller inte ansvariga för eventuella misstag, ärekränkningar, förtal, förolämpningar, utelämnanden, lögner, obsceniteter, pornografi eller svordomar som du stöter på. Som leverantör av interaktiva tjänster, och i den mån som tillåts enligt tillämplig lagstiftning, ansvarar inte Tripadvisor för några uttalanden, framställningar eller annat Innehåll som tillhandahålls av dess användare (inklusive dig vad gäller ditt Innehåll) på webbplatserna eller i andra forum. Tripadvisor har ingen skyldighet att avskärma, redigera eller övervaka något av det Innehåll som publiceras på eller distribueras via något Interaktivt område, men Tripadvisor förbehåller sig rätten att och kan helt efter eget gottfinnande (eller, om du är konsument bosatt i Storbritannien eller EU, efter vårt rimliga gottfinnande) ta bort, avskärma, översätta eller redigera utan föregående meddelande allt Innehåll som publiceras eller lagras på Tjänsterna när som helst och av vilken anledning som helst, eller låta sådana åtgärder utföras av tredje part för deras räkning. I den utsträckning som tillåts enligt tillämplig lagstiftning ansvarar du helt för att skapa säkerhetskopior av och ersätta Innehåll som du publicerar eller på annat sätt skickar till oss eller lagrar på Tjänsterna, helt på egen bekostnad. Fullständig information om våra policyer för moderering av innehåll, inklusive de begränsningar som vi kan införa för ditt innehåll, finns [här](https://www.tripadvisor.se/Trust).

**BEGRÄNSNING OCH/ELLER AVSTÄNGNING AV DIN ANVÄNDNING AV TJÄNSTERNA**

All användning av de Interaktiva områdena eller andra aspekter av Tjänsterna som bryter mot föregående bryter mot villkoren i detta Avtal och kan bland annat leda till att du förlorar din rätt, tillfälligt eller permanent, att använda de Interaktiva områdena och/eller mer generellt sett Tjänsterna. Tripadvisor kan även avbryta hanteringen av meddelanden och klagomål som skickas in via Tjänsterna om du ofta skickar in uppenbart ogrundade meddelanden eller klagomål.

Det finns mer information om vår metod för att avbryta din användning av plattformen [här](https://www.tripadvisor.se/Trust).

**Begränsa Tripadvisors licensrättigheter.** Du kan välja att framöver begränsa Tripadvisor-företagens användning av ditt Innehåll enligt detta Avtal (enligt beskrivningen ovan) genom att välja att ge Tripadvisor-företagen en mer begränsad licens, enligt vad som beskrivs nedan (en sådan begränsad licens benämns häri som en ”Begränsad licens”).  Du kan göra detta genom att välja att bevilja en sådan Begränsad licens [här](https://www.tripadvisor.se/Settings-cs) (observera att du måste vara inloggad på ditt konto). Om du gör detta val ska de rättigheter du ger Tripadvisor-företagen till ditt Innehåll enligt de licensvillkor som anges ovan (kallade ”Standardlicensen”) begränsas på vissa avgörande sätt, vilka beskrivs i paragraf 1–6 direkt här nedanför, så att Tripadvisor-företagen inte ska inneha en Standardlicens till något av ditt Innehåll förutom de textbaserade omdömen och tillhörande totalbetyg du publicerar (för vilka Tripadvisor-företagen ska beviljas en Standardlicens), utan beviljas en ”Begränsad licens” vad gäller ditt resterande Innehåll enligt definitionen nedan:

1. När du publicerar ditt Innehåll i Tjänsterna ska den licens du beviljar Tripadvisor-företag för Ditt innehåll vara begränsad till en icke-exklusiv, royaltyfri, överförbar, underlicensierbar och global licens att verka som värd för, använda, distribuera, ändra, köra, reproducera, offentligt visa eller framföra, översätta och skapa härledda verk av ditt Innehåll i syfte att visa detta i Tjänsterna, samt att använda ditt namn och/eller varumärke i samband med Innehållet. Enligt paragraf 6 nedan tillämpas den Begränsade licensen för allt ditt Innehåll (återigen, förutom textbaserade omdömen och tillhörande totalbetyg) som du eller någon annan på dina vägnar (t.ex. en tredje part som bidrar till eller på annat sätt hanterar ditt konto) har gjort tillgängligt i eller i samband med Tjänsterna.

1. Vad gäller enskilda objekt i ditt Innehåll som omfattas av den Begränsade licensen kan du säga upp Tripadvisor-företagens licensrättigheter enligt detta genom att radera objekten från Tjänsterna.  Du kan även säga upp Tripadvisor-företagens licensrättigheter till allt ditt Innehåll som omfattas av den Begränsade licensen genom att avsluta ditt konto (det finns information om hur du gör det [här](https://www.tripadvisorsupport.com/sv-SE/hc/traveler/articles/510)). Ditt Innehåll, oaktat annat som anges, (a) ska förbli i Tjänsterna i den utsträckning du har delat det med andra och de har kopierat eller lagrat det innan du raderade det eller avslutade ditt konto, (b) kan fortsätta att visas i Tjänsterna under en rimlig tidsperiod efter att du har raderat det eller avslutat ditt konto medan vi arbetar med att ta bort det och/eller (c) kan kvarhållas (men inte visas offentligt) av tekniska, bedrägeribegränsningsmässiga, regelmässiga eller juridiska anledningar i säkerhetskopierad form under en tidsperiod.

1. Tripadvisor-företagen kommer inte att använda ditt Innehåll i annonsering av tredje parters produkter och tjänster till andra utan ditt enskilda samtycke (inklusive sponsrat Innehåll), men du samtycker till och förstår att Tripadvisor-företagen kan visa annonser och kampanjer i Tjänsterna bredvid, nära, intill eller på annat sätt i närheten av ditt Innehåll (inklusive, för videoklipp eller annat dynamiskt innehåll, innan, under eller efter dess visning), samt andras Innehåll.  I alla fall där ditt Innehåll visas i Tjänsterna ska vi ange källan genom att använda det namn och/eller varumärke som du skickar in i samband med ditt Innehåll.  

1. Tripadvisor-företagen kommer inte att ge tredje parter rätten att publicera ditt Innehåll utanför Tjänsterna. Delningen av ditt Innehåll i Tjänsterna (förutom vår funktion ”Resor”, som kan göras privat) leder till att ditt Innehåll görs ”offentligt” och vi möjliggör en funktion som tillåter att andra användare delar (genom inbäddning av det offentliga objektet eller på annat sätt) sådant Innehåll tillhörande dig (förutom, som angetts, Resor som du konfigurerar som privat) till tredjepartstjänster och gör det möjligt för sökmotorer att göra det offentliga Innehållet tillhörande dig sökbart via sina tjänster.

1. Förutom enligt vad som ändrats i paragraf 1–6 i detta avsnitt i detta Avtal ska dina och våra rättigheter och förpliktelser omfattas av de resterande villkoren i detta Avtal. Den licens som du beviljar Tripadvisor-företag enligt ändringarna i dessa paragrafer 1–6 ska benämnas som en ”Begränsad licens”.

1. För tydlighetens skull omfattas inte det Innehåll du skickar in till Tjänsterna i samband med Tripadvisor-företagens andra tjänster eller program av den Begränsade licensen, utan omfattas av de regler och villkor som är kopplade till Tripadvisors specifika tjänst eller program.

 

**BOKNING MED TREDJEPARTSLEVERANTÖRER VIA Tripadvisor**

**Användning av Tripadvisors bokningstjänster.** Tripadvisor-företagen erbjuder möjligheten att söka efter, välja och boka resor med tredjepartsleverantörer utan att du behöver lämna Tjänsterna.

#### VIKTIGT! Tripadvisor säljer inte resebokningar. Tripadvisor tillåter tredjeparter att sälja resebokningar på Tripadvisor.

#### Tripadvisor kan underlätta tredje parters marknadsföring och försäljning av resebokningar till dig. Tripadvisor är dock inte den part som genomför kampanjen och/eller försäljningen. Om du gör en bokning hos en tredjepartsleverantör är avtalet för bokningen alltid enbart mellan dig och den tredje parten. Tripadvisor är inte (a) bokningens köpare eller säljare, (b) en part i avtalet med den tredje parten eller ansvarig för att avtalet uppfylls eller (c) ett ombud för dig eller den tredje parten i samband med köp eller försäljning. Om du gör en bokning med en tredjepartsleverantör samtycker du till att granska och omfattas av leverantörens regler och villkor för köp och användning av webbplatsen, sekretesspolicy samt andra regler och policyer som gäller för leverantörens webbplats eller egendom. Din kommunikation med tredjepartsleverantörer sker på egen risk. Tripadvisor-företagen har ingen ansvarsskyldighet gällande tredje parters eventuella åtgärder, underlåtelser, fel, utfästelser, garantier, regelöverträdelser eller försumlighet eller för personskador, dödsfall, skada på egendom eller andra skador eller utgifter till följd av din kommunikation med tredjepartsleverantörer.

Din regressrätt i samband med ett avtal mellan dig och en tredjepartsleverantör (till exempel när det gäller återbetalningar och avbokningar) gäller mellan dig och den tredje parten enligt vad som anges i avtalet med den tredje parten. I det osannolika fall att en bokning är tillgänglig när du gör en beställning men blir otillgänglig innan du checkar in är ditt enda alternativ mellan oss och dig att kontakta leverantören för alternativa lösningar eller för att avboka och erhålla en återbetalning (i tillämpliga fall och i enlighet med tillämplig lagstiftning och ditt avtal med tredjepartsleverantören).

#### Tredjepartsleverantörer kan vara företag eller konsumenter. Notera att om du är konsument som är bosatt i Storbritannien eller EU och den tredjepartsleverantör du har ett avtal med också är konsument kan du inte genomdriva några konsumenträttigheter gentemot leverantören.

När du gör resebokningar via webbplatserna måste du skapa ett konto om du inte redan har gjort det, och du samtycker till de förfaranden som beskrivs i vår Sekretesspolicy och i detta Avtal.

SOM ANVÄNDARE AV TJÄNSTERNA, INKLUSIVE TRIPADVISOR-FÖRETAGENS BOKNINGSTJÄNSTER, FÖRSTÅR OCH SAMTYCKER DU TILL FÖLJANDE, I DEN UTSTRÄCKNING SOM TILLÅTS ENLIGT TILLÄMPLIG LAG: (1) TRIPADVISOR-FÖRETAGEN HAR INGEN ANSVARSSKYLDIGHET GENTEMOT DIG ELLER ANDRA GÄLLANDE OBEHÖRIGA TRANSAKTIONER SOM GÖRS MED DITT LÖSENORD ELLER KONTO. (2) OBEHÖRIG ANVÄNDNING AV DITT LÖSENORD ELLER KONTO KAN GÖRA ATT DU BLIR ANSVARIG GENTEMOT TRIPADVISOR, DESS FÖRETAGSFILIALER OCH/ELLER ANDRA.

Det är kostnadsfritt att använda våra Tjänster, inklusive att skapa ett Tripadvisor-konto,och en betalning behandlas endast när du gör en bokning via Tjänsterna. När du gör en bokning som möjliggörs av Tripadvisor-företagen samlas dina betalningsuppgifter in och överförs till leverantören som slutför transaktionen, enligt beskrivningen i vår Sekretesspolicy. Observera att leverantören, inte Tripadvisor-företagen, ansvarar för behandlingen av din betalning och slutförandet av din bokning.

Tripadvisor-företagen ingriper inte godtyckligt i bokningar, men förbehåller sig rätten att återkalla bokningstjänsterna på grund av särskilda förmildrande omständigheter, till exempel när en bokning inte längre finns tillgänglig eller när vi har rimliga skäl att tro att en bokning kan vara bedräglig. Tripadvisor-företagen förbehåller sig även rätten att vidta åtgärder för att bekräfta din identitet så att din bokningsbegäran kan behandlas.

**Tredjepartsleverantörer.** Tripadvisor-företagen är inte resebyråer och tillhandahåller inte egna transporttjänster, boenden, restauranger, rundturer, aktiviteter eller upplevelser. Tripadvisor-företagen visar information om egendomar som ägs av tredjepartsleverantörer och möjliggör bokningar med vissa leverantörer på eller via Tripadvisor-företagens webbplatser, men sådana handlingar antyder eller utgör inte Tripadvisor-företagens sponsring eller godkännande av tredjepartsleverantörer eller något sorts samarbete mellan Tripadvisor-företagen och tredjepartsleverantörer. Användare kan betygsätta och recensera vissa transporttjänster, boenden, restauranger, rundturer, aktiviteter eller upplevelser utifrån sina egna erfarenheter, men Tripadvisor-företagen står inte bakom eller rekommenderar tredjepartsleverantörers produkter eller tjänster, förutom att Tripadvisor tilldelar vissa företag utmärkelser som baseras på omdömen som publiceras av användare. Tripadvisor-företagen står inte bakom något Innehåll som publiceras, skickas in eller på annat sätt tillhandahålls av någon användare eller något företag eller åsikter, rekommendationer eller råd som uttrycks däri, och Tripadvisor-företagen frånsäger sig uttryckligen allt ansvar i samband med sådant Innehåll. Du samtycker till att Tripadvisor-företagen inte är ansvariga för att den information som de erhåller från tredjepartsleverantörer och som visas i Tjänsterna är korrekt eller fullständig.

Tjänsterna kan länka dig till leverantörers webbplatser eller andra webbplatser som Tripadvisor inte driver eller kontrollerar. Det finns mer information i avsnittet ”Länkar till tredjepartswebbplatser” här nedan.

**Boka resetjänster via Tripadvisors mobilapp.**  Vissa av de resetjänster som bokas via Tripadvisors mobilapp omfattas av en ytterligare uppsättning användarvillkor.  Dessa villkor är tillgängliga via själva appen och finns [här](https://tripadvisor.mediaroom.com/us-terms-of-use-booking).

**_Bokning av semesterbostäder, restauranger och upplevelser med tredjepartsleverantörer på företagsfilialers webbplatser._** Några av Tripadvisors företagsfilialer fungerar som marknadsplatser som möjliggör för resenärer att (1) ingå hyresavtal för semesterbostäder med egendomsägare och uthyrare (”Semesterbostäder”), (2) boka restauranger (”Restauranger”) och/eller (3) boka rundturer, aktiviteter och sevärdheter (på olika sätt, ”Upplevelser”) med tredjepartsleverantörer av dessa Upplevelser (alla sådana leverantörer av en semesterbostad och/eller Upplevelse ska benämnas som en ”Annonsör”). Dessa Tripadvisor-företagsfilialer syndikerar sina annonser till andra enheter inom Tripadvisor-företagens koncern och det är därför du ser dem på Tripadvisor-företagens webbplatser.  Som användare måste du ansvara för din användning av Tjänsterna (inklusive, i synnerhet, Tripadvisor-företagens webbplatser) och alla transaktioner som rör Semesterbostäder, Restauranger eller Upplevelser som möjliggörs av Tripadvisor-företagens företagsfilialer. Vi äger, hanterar eller tecknar inte avtal för några Semesterbostäder, Restauranger eller Upplevelser i Tjänsterna.

Eftersom varken Tripadvisor eller dess företagsfilialer är parter i transaktioner för Semesterbostäder, bokningar av Restauranger eller transaktioner relaterade till Upplevelser mellan resenärer och Annonsörer ansvarar varje användare helt för eventuella tvister eller konflikter gällande en faktisk eller potentiell transaktion mellan dig och en Annonsör, inklusive kvalitet, skick, säkerhet eller lagenlighet för en listad Semesterbostad, Restaurang eller Upplevelse, det listade Innehållets korrekthet, Annonsörens förmåga att hyra ut en Semesterbostad, tillhandahålla en bokning, måltid eller annan tjänst på en Restaurang eller tillhandahålla en Upplevelse samt din förmåga att betala för en Semesterbostad, en måltid eller tjänst på en Restaurang eller en Upplevelse.

En av Tripadvisors företagsfilialer kan agera som begränsat ombud för en Annonsör enbart i syfte att överföra din betalning till Annonsören. Du samtycker till att betala en Annonsör eller en Tripadvisor-företagsfilial som agerar som begränsat ombud för inkassering av betalningar på uppdrag av en Annonsör eventuella avgifter som anges till dig innan du gör en bokning för en Semesterbostad eller Upplevelse och eventuella avgifter som debiteras dig till följd av skada som orsakas av din användning av Semesterbostaden eller Upplevelsen i enlighet med tillämpliga villkor eller policyer.

Det finns information om avgifter för Semesterbostäder, säkerhetsdepositioner, avgifter för Upplevelser, betalningsbehandling, återbetalningar och liknande i våra företagsfilialers regler och villkor (besök [Viator](https://www.viator.com/sv-SE/support/termsAndConditions) för Upplevelser, [The Fork](https://www.thefork.se/#geo_dmn_redirect=20210629AT100) för Restauranger och [Tripadvisor-företagens Semesterbostäder](https://rentals.tripadvisor.com/sv_SE/termsandconditions/traveler) för Semesterbostäder. När du bokar en Semesterbostad, Restaurang eller Upplevelse som möjliggörs av en av våra företagsfilialer måste du bekräfta och samtycka till dess regler och villkor, samt dess sekretesspolicy.

Om du hamnar i en tvist med en Annonsör inom EU finns alternativa metoder för att lösa tvisten online här: [http://ec.europa.eu/consumers/odr/](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=SV).

**RESMÅL**

**Internationella resor.** När du bokar internationella resor med tredjepartsleverantörer eller planerar internationella resor med hjälp av Tjänsterna är du ansvarig för att se till att du uppfyller inträdeskrav och att dina resedokument, till exempel pass och visum, är i ordning.

När det gäller pass- och visumkrav måste du rådfråga den berörda ambassaden eller konsulatet. Eftersom kraven kan komma att ändras när som helst bör du kontrollera aktuell information före bokning och avresa. Tripadvisor-företagen tar inget ansvar för resenärer som nekas inträde på ett flyg eller i ett land på grund av att de inte medfört de resedokument som krävs av ett flygbolag, en myndighet eller ett land, inklusive länder som resenärer bara mellanlandar i.

Det är även ditt ansvar att se till att du rådfrågar din läkare om aktuella rekommendationer gällande vaccinationer innan du reser utrikes samt att du uppfyller alla hälsokrav och följer alla medicinska råd i samband med resan.

Trots att de flesta resor, inklusive resor till internationella resmål, sker utan incidenter kan resor till vissa resmål innebära större risker än andra. Tripadvisor uppmanar resenärer att granska reseförbud, varningar, meddelanden och råd som utfärdas av både sina egna och resmålets myndigheter innan de bokar en resa till internationella resmål. Information om förhållanden i olika länder och de risknivåer som förknippas med resor till vissa internationella resmål tillhandahålls exempelvis av USA:s regering på[www.state.gov](http://www.state.gov/), [www.tsa.gov](https://www.tsa.gov/), [www.dot.gov](http://www.dot.gov/), [www.faa.gov](http://www.faa.gov/), [www.cdc.gov](http://www.cdc.gov/), [www.treas.gov/ofac](http://www.treas.gov/ofac) och[www.customs.gov](http://www.customs.gov/).

GENOM ATT PUBLICERA INFORMATION SOM ÄR RELEVANT FÖR RESOR TILL VISSA INTERNATIONELLA RESMÅL GARANTERAR ELLER UTFÄSTER INTE TRIPADVISOR-FÖRETAGEN ATT RESOR TILL SÅDANA RESMÅL REKOMMENDERAS ELLER KAN SKE RISKFRITT OCH ANSVARAR INTE FÖR SKADOR ELLER FÖRLUSTER I SAMBAND MED RESOR TILL SÅDANA RESMÅL.

**ANSVARSFRISKRIVNING**

LÄS DETTA AVSNITT NOGA. DETTA AVSNITT BEGRÄNSAR TRIPADVISOR-FÖRETAGENS ANSVAR GENTEMOT DIG FÖR PROBLEM SOM KAN UPPSTÅ I ANSLUTNING TILL DIN ANVÄNDNING AV TJÄNSTERNA. OM DU INTE FÖRSTÅR VILLKOREN I DETTA AVSNITT ELLER NÅGON ANNANSTANS I DETTA AVTAL

DEN INFORMATION, PROGRAMVARA OCH DE PRODUKTER OCH TJÄNSTER SOM FINNS PUBLICERADE I, ELLER PÅ ANNAT SÄTT TILLHANDAHÅLLS VIA, TJÄNSTERNA (INKLUSIVE, UTAN BEGRÄNSNING, DE SOM SKAPAS ELLER DRIVS AV AI) KAN INNEHÅLLA FELAKTIGHETER, INKLUSIVE TILLGÄNGLIGHETS- OCH PRISFEL. TRIPADVISOR-FÖRETAGEN GARANTERAR INTE ATT INFORMATIONEN OM OCH BESKRIVNINGEN AV DE BOENDEN, UPPLEVELSER, FLYG, KRYSSNINGAR OCH RESTAURANGER ELLER ANDRA RESEPRODUKTER SOM VISAS I TJÄNSTERNA (INKLUSIVE, UTAN BEGRÄNSNING, PRISSÄTTNINGEN AV, TILLGÄNGLIGHETEN FÖR, BILDERNA PÅ OCH ANNONSERINGEN AV BOENDEN, UPPLEVELSER, FLYG, KRYSSNINGAR OCH RESTAURANGER ELLER ANDRA RESEPRODUKTERS BEKVÄMLIGHETER, ALLMÄNNA PRODUKTBESKRIVNINGAR, OMDÖMEN OCH BETYG OSV.) ÄR KORREKT OCH FRISKRIVER SIG FRÅN ALLT ANSVAR FÖR EVENTUELLA FELAKTIGHETER ELLER ÖVRIGA ORIKTIGHETER RELATERADE TILL DENNA INFORMATION. DESSUTOM FÖRBEHÅLLER TRIPADVISOR-FÖRETAGEN SIG UTTRYCKLIGEN RÄTTEN ATT KORRIGERA EVENTUELLA FEL GÄLLANDE TILLGÄNGLIGHET OCH UPPENBARA PRISFEL I TJÄNSTERNA OCH/ELLER VÄNTANDE BOKNINGAR SOM GJORTS MED ETT UPPENBART FELAKTIGT PRIS.

TRIPADVISOR LÄMNAR INGA UTFÄSTELSER AV NÅGOT SLAG AVSEENDE TJÄNSTERNAS LÄMPLIGHET, INKLUSIVE DEN INFORMATION SOM ÄR TILLGÄNGLIG PÅ DESS WEBBPLATSER ELLER DELAR AV DESSA, I NÅGOT SYFTE, OCH TILLHANDAHÅLLANDET ELLER ERBJUDANDET AV PRODUKTER ELLER TJÄNSTER PÅ DESS WEBBPLATSER ELLER PÅ ANNAT SÄTT VIA TJÄNSTERNA INNEBÄR INGA GODKÄNNANDEN ELLER REKOMMENDATIONER AVSEENDE SÅDANA PRODUKTER ELLER TJÄNSTER FRÅN TRIPADVISORS SIDA, OAKTAT EVENTUELLA UTMÄRKELSER SOM DELAS UT BASERAT PÅ ANVÄNDAROMDÖMEN. INFORMATION, PROGRAMVARA, PRODUKTER OCH TJÄNSTER SOM GÖRS TILLGÄNGLIGA GENOM ELLER VIA TJÄNSTERNA ERBJUDS ”I BEFINTLIGT SKICK” UTAN NÅGON TYP AV GARANTI. TRIPADVISOR FRISKRIVER SIG, I DEN STÖRSTA UTSTRÄCKNING SOM TILLÅTS ENLIGT TILLÄMPLIG LAGSTIFTNING, FRÅN ALLA GARANTIER, FÖRUTSÄTTNINGAR ELLER ANDRA VILLKOR AV NÅGOT SLAG OM ATT TJÄNSTERNA, DESS SERVRAR ELLER DATA (INKLUSIVE E-POST) SOM SKICKAS FRÅN TRIPADVISOR ÄR FRIA FRÅN VIRUS ELLER ANNAT SKADLIGT INNEHÅLL. I DEN MAXIMALA UTSTRÄCKNING SOM TILLÅTS ENLIGT TILLÄMPLIG LAGSTIFTNING FRISKRIVER SIG TRIPADVISOR HÄRMED FRÅN ALLA GARANTIER OCH VILLKOR AVSEENDE INFORMATIONEN, PROGRAMVARAN, PRODUKTERNA OCH TJÄNSTERNA, INKLUSIVE UNDERFÖRSTÅDDA GARANTIER OCH VILLKOR AV ALLA SLAG AVSEENDE SÄLJBARHET, LÄMPLIGHET FÖR ETT SÄRSKILT ÄNDAMÅL, ÄGANDERÄTT, TYST INNEHAV SAMT FRÅNVARO AV INTRÅNG.

TRIPADVISOR-FÖRETAGEN FRISKRIVER SIG OCKSÅ UTTRYCKLIGEN FRÅN ALLA GARANTIER, FRAMSTÄLLNINGAR ELLER VILLKOR AV NÅGOT SLAG VAD GÄLLER KORREKTHET ELLER ÄGANDETYP FÖR DET INNEHÅLL SOM ÄR TILLGÄNGLIGT I OCH VIA TJÄNSTERNA.

DE TREDJEPARTSLEVERANTÖRER SOM TILLHANDAHÅLLER INFORMATION OM BOENDEN, FLYGRESOR, SEMESTERBOSTÄDER, UPPLEVELSER, RESTAURANGER, KRYSSNINGAR, RESOR ELLER ANDRA TJÄNSTER I ELLER VIA TJÄNSTERNA ÄR OBEROENDE LEVERANTÖRER OCH INTE OMBUD FÖR ELLER ANSTÄLLDA HOS TRIPADVISOR-FÖRETAGEN. TRIPADVISOR-FÖRETAGEN ANSVARAR INTE FÖR SÅDANA LEVERANTÖRERS EVENTUELLA ÅTGÄRDER, FEL, UNDERLÅTANDEN, UTFÄSTELSER, GARANTIER, REGELÖVERTRÄDELSER ELLER FÖRSUMLIGHET ELLER FÖR PERSONSKADOR, DÖDSFALL, SKADA PÅ EGENDOM ELLER ANDRA SKADOR ELLER UTGIFTER TILL FÖLJD AV SÅDANA SKADOR. TRIPADVISOR ANSVARAR INTE FÖR, OCH GÖR INGA ÅTERBETALNINGAR VID, EVENTUELL FÖRSENING, AVBOKNING, ÖVERBOKNING, STREJK, FORCE MAJEURE ELLER ANDRA ANLEDNINGAR UTANFÖR DESS DIREKTA KONTROLL, OCH ANSVARAR INTE FÖR YTTERLIGARE KOSTNADER, UNDERLÅTANDEN, FÖRSENINGAR, OMDIRIGERINGAR ELLER ÅTGÄRDER UTFÖRDA AV NÅGON MYNDIGHET.

I ENLIGHET MED FÖREGÅENDE ANVÄNDER DU TJÄNSTERNA PÅ EGEN RISK OCH TRIPADVISOR-FÖRETAGEN (ELLER DERAS BEFATTNINGSHAVARE, CHEFER OCH/ELLER ANSTÄLLDA) SKA UNDER INGA OMSTÄNDIGHETER HÅLLAS ANSVARIGA FÖR NÅGRA DIREKTA, INDIREKTA, STRAFFRELATERADE, OAVSIKTLIGA, SÄRSKILDA ELLER PÅFÖLJANDE FÖRLUSTER ELLER SKADOR ELLER NÅGON FÖRLUST AV INKOMST, VINST, GOODWILL, DATA, AVTAL, ANVÄNDNING AV PENGAR ELLER FÖRLUST ELLER SKADOR SOM UPPSTÅR AV ELLER PÅ NÅGOT SÄTT ÄR KOPPLADE TILL DRIFTSAVBROTT AV NÅGOT SLAG SOM UPPSTÅR AV ELLER PÅ NÅGOT SÄTT ÄR KOPPLADE TILL DIN ÅTKOMST TILL, VISNING AV ELLER ANVÄNDNING AV TJÄNSTERNA ELLER TILL FÖRSENINGEN MED ELLER OFÖRMÅGAN ATT KOMMA ÅT, VISA ELLER ANVÄNDA TJÄNSTERNA (INKLUSIVE, MEN INTE BEGRÄNSAT TILL, DIN TILLIT TILL OMDÖMEN OCH ÅSIKTER SOM VISAS I ELLER VIA TJÄNSTERNA, EVENTUELLA VIRUS, BUGGAR, TROJANER, INFORMATION, PROGRAMVARA, LÄNKADE WEBBPLATSER, PRODUKTER OCH TJÄNSTER SOM ERHÅLLS VIA TJÄNSTERNA (INKLUSIVE, MEN INTE BEGRÄNSAT TILL, EVENTUELLA SYNKRONISERINGSPRODUKTER FRÅN TRIPADVISOR-FÖRETAGEN), PERSONSKADA ELLER SKADA PÅ EGENDOM AV ALLA TYPER SOM UPPSTÅR SOM ETT RESULTAT AV DIN ANVÄNDNING AV TJÄNSTERNAS SERVRAR OCH/ELLER SAMTLIG PERSONLIG INFORMATION OCH FINANSIELL INFORMATION SOM LAGRAS DÄRPÅ, EVENTUELLA FEL ELLER UTELÄMNANDEN I NÅGOT INNEHÅLL ELLER FÖR FÖRLUST ELLER SKADA AV NÅGOT SLAG SOM UPPSTÅTT TILL FÖLJD AV ANVÄNDNINGEN AV NÅGOT INNEHÅLL ELLER SOM PÅ ANNAT SÄTT UPPSTÅR TILL FÖLJD AV ANSLUTNING TILL ELLER VISNING ELLER ANVÄNDNING AV TJÄNSTERNA), OAVSETT OM DETTA ÄR BASERAT PÅ FÖRSUMLIGHET, AVTALSBROTT, ÅTALBARA HANDLINGAR, ABSOLUT ANSVAR ELLER ANNAT, ÄVEN OM TRIPADVISOR ELLER DESS FÖRETAGSFILIALER HAR FÅTT INFORMATION OM ATT SÅDANA SKADOR KAN UPPSTÅ.

Om Tripadvisor-företagen bedöms vara ansvariga för någon förlust eller skada som uppstår av eller på något sätt är kopplad till din användning av Tjänsterna ska Tripadvisor-företagens ansvar under inga omständigheter sammanlagt överstiga det större av (a) de transaktionsavgifter som betalas till Tripadvisor-företagen för de transaktioner i eller via Tjänsterna som givit upphov till anspråket eller (b) ett hundra dollar (100,00 USD).

Ansvarsbegränsningen avspeglar riskfördelningen mellan parterna. De begränsningar som beskrivs i detta avsnitt gäller även om eventuella begränsade påföljder som anges i dessa villkor inte fått avsedd effekt. Ansvarsbegränsningarna i de här villkoren utfaller till fördel för Tripadvisor-företagen.

DETTA AVTAL OCH FÖREGÅENDE ANSVARSFRISKRIVNING PÅVERKAR INTE DE OBLIGATORISKA LAGSTADGADE RÄTTIGHETER SOM INTE KAN EXKLUDERAS ENLIGT TILLÄMPLIG LAGSTIFTNING, TILL EXEMPEL TILLÄMPLIGA KONSUMENTSKYDDSLAGAR I VISSA LÄNDER. I STORBRITANNIEN OCH EU OMFATTAR DETTA ANSVAR FÖR DÖDSFALL ELLER PERSONSKADOR SOM ORSAKATS AV VÅR FÖRSUMLIGHET ELLER VÅRA ANSTÄLLDAS, OMBUDS ELLER UNDERLEVERANTÖRERS FÖRSUMLIGHET, SAMT FÖR BEDRÄGERI ELLER BEDRÄGLIGT VILSELEDANDE INFORMATION.

Om du är konsument i Storbritannien eller EU exkluderar detta Avtal inte något ansvar för förluster och skador som orsakas av brott mot någon skyldighet att agera med rimlig omsorg och färdighet eller av brott mot detta Avtal, under förutsättning att de resulterande skadorna kunde förutses när förpliktelserna uppstod.     

OM LAGEN I DET LAND DÄR DU BOR INTE TILLÅTER NÅGON SÄRSKILD BEGRÄNSNING AV ELLER UNDANTAG FRÅN ANSVAR SOM ANGES I DENNA KLAUSUL GÄLLER INTE DEN BEGRÄNSNINGEN. ANSVARSFRISKRIVNINGEN GÄLLER I ÖVRIGT I DEN MAXIMALA UTSTRÄCKNING SOM TILLÅTS ENLIGT LOKAL LAG.

**SKADEERSÄTTNING**

Detta avsnitt ”Skadeersättning” gäller inte för konsumenter som är bosatta i EU eller Storbritannien.

Du samtycker till att försvara och hålla Tripadvisor-företagen och dess befattningshavare, chefer, anställda och ombud skadelösa från samtliga former av anspråk, åtgärder, krav, förluster, skador, böter, stämningar eller kostnader av andra slag, inklusive men inte begränsat till juristarvoden och revisorarvoden som framställs av tredje part till följd av

* (i) att du överträder detta Avtal eller de dokument som nämns häri;
* (ii) att du bryter mot lagar eller tredje parters rättigheter; eller
* (iii) att du använder Tjänsterna, inklusive Tripadvisor-företagens webbplatser.

**LÄNKAR TILL TREDJEPARTSWEBBPLATSER**

Tjänsterna kan innehålla hyperlänkar till webbplatser som drivs av andra parter än Tripadvisor-företagen. Dessa hyperlänkar tillhandahålls bara som referens. Tripadvisor-företagen kontrollerar inte dessa webbplatser och ansvarar inte för innehållet därpå eller sekretessen och andra förfaranden för dessa webbplatser. Det är även upp till dig att vidta försiktighetsåtgärder för att se till att de länkar du klickar på eller programvara du laddar ned (från den här webbplatsen eller en annan webbplats) är fria från sådant som virus, maskar, trojaner, skador och andra föremål av destruktivt slag. Tripadvisor-företagens inkluderande av hyperlänkar till sådana webbplatser innebär inga godkännanden av materialet på eller i sådana webbplatser eller appar från tredje part eller någon form av koppling till de som driver dem.

I vissa fall kan en webbplats eller app från tredje part be dig att länka din Tripadvisor-kontoprofil till en profil på en annan tredjepartswebbplats. Du ansvarar för att bestämma om du väljer att göra det, det är helt valfritt och beslutet att tillåta att informationen länkas kan när som helst ångras (på/i webbplatsen eller appen från tredje part). Om du väljer att länka ditt Tripadvisor-konto till en webbplats eller app från tredje part kan webbplatsen eller appen från den tredje parten komma åt information du har lagrat i ditt Tripadvisor-konto, inklusive information om andra användare som du delar information med. Du bör läsa reglerna och villkoren och sekretesspolicyn för de webbplatser och appar från tredje part som du besöker då de har regler och behörigheter för hur de använder din information som kan skilja sig från Tjänsternas, inklusive våra webbplatsers. Vi uppmuntrar dig att granska de här webbplatserna och apparna från tredje part och använda dem på egen risk.

**PROGRAMVARA SOM EN DEL AV TJÄNSTER; YTTERLIGARE MOBILA LICENSER**

Programvara från Tjänsterna lyder under exportlagar i USA. Ingen programvara från Tjänsterna får laddas ned eller på annat sätt exporteras eller omexporteras (a) till (eller till en medborgare eller invånare i) Kuba, Irak, Sudan, Nordkorea, Iran, Syrien eller något annat land för vilket USA har infört handelsförbud eller (b) till någon på USA:s finansdepartements lista med Specially Designated Nationals eller USA:s handelsdepartements Table of Deny Orders. Genom att använda Tjänsterna intygar du att du inte befinner dig i, kontrolleras av eller är medborgare eller boende i något av länderna eller inkluderas på dessa listor.

Som nämnts ovan innehåller Tjänsterna programvara, som ibland kan benämnas som ”appar”.  All programvara som finns tillgänglig för nedladdning från Tjänsterna (”Programvara”) är upphovsrättsligt skyddade verk som tillhör Tripadvisor eller en angiven part. Din användning av denna Programvara styrs av villkoren i det eventuella licensavtalet för slutanvändare, som medföljer, eller ingår i, Programvaran. Du får inte installera eller använda någon Programvara som åtföljs av eller inkluderar ett licensavtal om du inte först samtycker till villkoren i licensavtalet. För all Programvara som görs tillgänglig för nedladdning via Tjänsterna och som inte åtföljs av ett licensavtal beviljar vi dig, användaren, en begränsad personlig, oöverlåtlig licens för användning av Programvaran för att besöka eller på annat sätt använda Tjänsterna i överensstämmelse med reglerna och villkoren i detta Avtal (inklusive de policyer som refereras till häri) och för inga andra ändamål.

Observera att Programvaran, bland annat all HTML-, XML- eller Java-kod och alla Active X-kontroller som finns i Tjänsterna, ägs eller licensieras av Tripadvisor och skyddas av upphovsrättsliga lagar och internationella avtalsvillkor. All reproduktion eller omdistribution av Programvaran är uttryckligen förbjuden, och kan leda till hårda civil- och straffrättsliga påföljder. De som överträder detta kommer att åtalas i största möjliga utsträckning.

Delar av Tripadvisors mobilprogramvara kan använda upphovsrättsskyddat material, vilket bekräftas av Tripadvisor. Utöver detta finns det särskilda villkor som gäller för användning av vissa mobilapplikationer från Tripadvisor. På sidan [Mobila licenser](https://tripadvisor.mediaroom.com/SE-mobile-licenses) finns information som gäller specifikt för Tripadvisors mobilapplikationer.

UTAN BEGRÄNSNING AV FÖREGÅENDE ÄR ALL KOPIERING ELLER REPRODUKTION AV PROGRAMVARAN TILL EN ANNAN SERVER ELLER PLATS FÖR VIDARE REPRODUKTION ELLER DISTRIBUTION UTTRYCKLIGEN FÖRBJUDEN. OM PROGRAMVARAN ALLS GARANTERAS ÄR DET ENBART I ENLIGHET MED VILLKOREN I LICENSAVTALET ELLER DETTA AVTAL (ENLIGT VAD SOM ÄR TILLÄMPLIGT), MED FÖRBEHÅLL FÖR TILLÄMPLIGA LAGAR.

**UPPHOVSRÄTTS- OCH VARUMÄRKESMEDDELANDEN**

Tripadvisor, ugglelogotypen, betygsbubblorna och alla andra produkt- eller tjänstenamn eller slogans som visas i Tjänsterna är registrerade varumärken och/eller varumärken baserade på sedvanerätten som tillhör Tripadvisor LLC och/eller dess leverantörer eller licensgivare och får inte kopieras, efterliknas eller användas, i sin helhet eller delar därav, utan föregående skriftligt tillstånd från Tripadvisor eller varumärkesinnehavaren. Dessutom är utseendet och känslan av Tjänsterna, däribland våra webbplatser samt alla sidrubriker, anpassad grafik, knappikoner och skript som rör desamma, servicemärken, varumärken och/eller företagsprofilen som tillhör Tripadvisor och får inte kopieras, efterliknas eller användas, i sin helhet eller delar därav, utan föregående skriftligt tillstånd från Tripadvisor. Alla andra varumärken, registrerade varumärken, produktnamn och företagsnamn eller logotyper som nämns i Tjänsterna tillhör respektive ägare. Förutom i den utsträckning som anges på andra ställen i detta Avtal utgör hänvisningar till produkter, tjänster, processer eller annan information genom handelsnamn, varumärke, tillverkare, leverantör eller annat inte, eller innebär inga, godkännanden, sponsring eller rekommendationer för detta av Tripadvisor.

Alla rättigheter förbehålles. Tripadvisor ansvarar inte för innehåll på webbplatser som drivs av andra parter än Tripadvisor.

**Riktlinjer för anmälan om och borttagning av olagligt innehåll**

Tripadvisor verkar enligt principen om borttagning efter meddelande. Om du har klagomål eller invändningar mot Innehåll, inklusive användarmeddelanden som publiceras i Tjänsterna, eller om du anser att material eller innehåll som publiceras i Tjänsterna gör intrång på upphovsrätt som du innehar ska du kontakta oss omedelbart genom att följa vår process för anmälan och borttagning. [Klicka här för att se Upphovsrättspolicyn och förfarandet](https://www.tripadvisor.se/pages/noticetakedown.html). När du har följt detta förfarande svarar Tripadvisor på giltiga och korrekt underbyggda klagomål genom att vidta alla rimliga åtgärder för att ta bort uppenbart olagligt innehåll inom en rimlig tid.

**ÄNDRINGAR I DETTA AVTAL; TJÄNSTER; UPPDATERINGAR; UPPSÄGNING OCH TILLBAKADRAGANDE**

**Detta Avtal:**

Tripadvisor kan komma att ändra, utöka eller radera reglerna och villkoren i detta Avtal eller delar av dem efter eget gottfinnande (eller efter rimligt gottfinnande för konsumenter som är bosatta i Storbritannien eller EU) om vi bedömer att det är nödvändigt av juridiska, allmänt regelmässiga eller tekniska skäl eller på grund av förändringar i de Tjänster som tillhandahålls eller på grund av Tjänsternas upplägg eller layout. Därefter samtycker du uttryckligen till att vara bunden av reglerna och villkoren i detta Avtal i dess ändrade form.

**Ändringar:**

Tripadvisor-företagen kan när som helst komma att ändra, avbryta eller sluta tillhandahålla någon del av Tjänsterna, inklusive tillgänglighet för Tjänsternas funktioner, databaser eller Innehåll, utan kostnad för dig, inklusive:

(a)        för att säkerställa efterlevnad av tillämpliga lagar och/eller återspegla ändringar i relevanta lagar och lagstadgade krav, t.ex. obligatoriska konsumentlagar;

(b)        för att utföra tillfälligt underhåll, åtgärda fel, genomföra tekniska justeringar, i driftsyfte eller göra förbättringar, till exempel anpassa Tjänsterna till en ny teknisk miljö, överföra Tjänsterna till en ny värdplattform eller säkerställa Tjänsternas kompatibilitet med enheterna och programvaran (kan uppdateras då och då);

(c)        för att uppgradera eller ändra Tjänsterna, inklusive att släppa nya versioner av webbplatserna på vissa enheter eller på annat sätt göra tillägg eller ändra befintliga funktioner;

(d)        för att ändra Tjänsternas struktur, utformning eller utseende, inklusive ändra namnet på eller varumärket för Tjänsterna, eller ändra, förbättra och/eller utöka tillgängliga funktioner; och

(e)        av säkerhetsskäl.

För användare i Storbritannien och EU: Om vi gör några ändringar av Tjänsterna som vi rimligen anser kommer att avsevärt påverka din åtkomst till eller användning av Tjänsterna på ett negativt sätt meddelar vi dig om ändringarna minst 30 dagar i förväg. Om du inte säger upp ditt avtal med oss innan dessa ändringar äger rum tar vi det som ditt godkännande av ändringarna.

**Uppdateringar:**

Vi kan komma att göra uppdateringar för att se till att Tjänsterna uppfyller tillämplig konsumentlagstiftning. Om automatiska uppdateringar är aktiverade i dina inställningar uppdaterar vi Tjänsterna automatiskt. Du kan ändra dina inställningar när som helst om du föredrar att inte längre få automatiska uppdateringar.

För att få den bästa upplevelsen och säkerställa att Tjänsterna fungerar korrekt rekommenderar vi att du godkänner alla uppdateringar av Tjänsterna som vi informerar dig om när de blir tillgängliga. Du kan även behöva uppdatera enhetens operativsystem. I takt med att nya operativsystem och enheter lanseras kan vi med tiden sluta stödja äldre versioner.

Om du väljer att inte ladda ned eller installera uppdateringar kanske Tjänsterna inte längre är tillgängliga, stöds eller har tidigare funktionalitet.

Vi ansvarar inte för din underlåtenhet att ladda ned eller installera uppdateringar eller den senaste publicerade versionen av Tjänsterna för att dra nytta av nya eller förbättrade funktioner och/eller funktionalitet och/eller uppfylla kompatibilitetskrav när vi har informerat dig om uppdateringen, förklarat konsekvenserna av att inte installera den och tillhandahållit installationsanvisningar.

Ändringar av Tjänsten som inte följer detta avsnitt ”Uppdateringar” omfattas av avsnittet ”Ändringar” ovan.

**Uppsägning:**

Tripadvisor-företagen kan även utan föregående meddelande eller ansvarsskyldighet införa gränser eller på annat sätt begränsa åtkomsten till hela eller delar av Tjänsterna av tekniska eller säkerhetsmässiga skäl för att förhindra obehörig åtkomst eller förlust eller förstörelse av data eller om Tripadvisor och/eller dess företagsfilialer efter eget gottfinnande (eller efter rimligt gottfinnande för konsumenter som är bosatta i Storbritannien eller EU) anser att du bryter mot någon bestämmelse i detta Avtal eller någon lag eller föreskrift och om Tripadvisor och/eller dess företagsfilialer beslutar att sluta tillhandahålla en aspekt av Tjänsterna.

Tripadvisor kan säga upp detta Avtal med dig när som helst, utan föregående meddelande, om de i god tro anser att du har brutit mot detta Avtal eller på annat sätt anser att en uppsägning är skäligen nödvändig för att skydda rättigheterna för Tripadvisor-företagen och/eller andra användare av Tjänsterna. Det innebär att vi kan sluta tillhandahålla Tjänsterna till dig.

**JURISDIKTION OCH GÄLLANDE LAG**

Den här webbplatsen ägs och regleras av Tripadvisor LLC, ett amerikanskt aktiebolag. Detta Avtal och eventuella tvister och anspråk (inklusive icke-avtalsmässiga tvister eller anspråk) som uppstår av eller i samband med det eller dess ämne eller utformning ska regleras av och tolkas enligt lagen i delstaten Massachusetts i USA. Du samtycker härmed till den exklusiva rättsliga behörigheten för domstolarna i Massachusetts i USA, och stipulerar att det är rättvist och lämpligt att alla tvister som härrör från eller uppstår i samband med din eller en tredje parts användning av Tjänsterna, både avtalsmässiga och icke-avtalsmässiga, hanteras av dessa domstolar. Du godkänner att eventuella krav du har gentemot Tripadvisor LLC som härrör från eller uppstår i samband med användningen av Tjänsterna måste hanteras och lösas av en behörig domstol med ämneskompetens och som finns i delstaten Massachusetts. Användningen av Tjänsterna är ej godkänd i jurisdiktioner som inte tillåter alla bestämmelser i dessa regler och villkor, bland annat denna punkt. Inget i denna klausul begränsar Tripadvisor LLC:s rätt att vidta rättsliga åtgärder mot dig i någon annan behörig domstol eller andra behöriga domstolar. Ovanstående ska inte gälla i den utsträckning att tillämplig lag i det land där du är bosatt kräver tillämpning av en annan lag och/eller jurisdiktion – i synnerhet om du använder Tjänsterna som konsument – och detta inte kan exkluderas genom ett avtal och inte styras av Förenta nationernas konvention angående avtal om internationella köp av varor, om det annars är tillämpligt.  Om du använder Tjänsterna som konsument och inte som företag eller Företagsrepresentant kan du vara berättigad att göra anspråk mot Tripadvisor i domstolarna i det land där du är bosatt. Denna klausul ska i övrigt tillämpas i den maximala utsträckning som tillåts i ditt land eller vistelseland.

**VALUTAOMVANDLARE**

Valutakurser baseras på olika allmänt tillgängliga källor och ska endast betraktas som riktlinjer. Valutakurserna kontrolleras inte och de faktiska kurserna kan variera. Valutakurserna kanske inte uppdateras dagligen. Den angivna informationen tros vara korrekt, men Tripadvisor-företagen lämnar inga garantier för sådan korrekthet. När du använder informationen i något ekonomiskt syfte rekommenderar vi att du konsulterar en kvalificerad yrkesman för att kontrollera att växelkurserna är riktiga. Vi godkänner inte användningen av denna information för något annat syfte än personligt bruk och det är uttryckligen förbjudet att vidaresälja, omdistribuera och använda denna information i kommersiella syften.

**ALLMÄNNA BESTÄMMELSER**

Vi förbehåller oss rätten att återta samtliga användarnamn, kontonamn, alias, smeknamn, referenser eller annan användaridentifierare av valfri anledning utan ansvar gentemot dig.

Du är införstådd med att detta Avtal och din användning av Tjänsterna inte på något sätt kan betraktas som ett upprättande av ett samarbetsavtal, partnerskap eller anställnings- eller agenturförhållande mellan dig och Tripadvisor och/eller dess företagsfilialer.

Våra åtaganden enligt detta Avtal regleras av gällande lagstiftning och juridiska processer. Ingenting i detta Avtal upphäver vår rätt att uppfylla juridiska krav och bestämmelser från statliga myndigheter och rättsväsendet avseende din användning av Tjänsterna och information som har skickats till eller samlats in av oss med hänsyn till sådan användning. I den utsträckning som det är tillåtet enligt gällande lag samtycker du till att väcka talan eller lägga fram anspråk till följd av eller i samband med din åtkomst eller användning av Tjänsterna inom två (2) år efter den dag då detta anspråk eller denna talan uppstod eller ackumulerades, annars kommer detta anspråk eller denna talan oåterkalleligt att förfalla.

Om någon del av detta Avtal skulle vara ogiltig eller overkställbar enligt tillämplig lagstiftning, bland annat ovan angivna garanti- och ansvarsbegränsningar, ska den ogiltiga eller overkställbara bestämmelsen ersättas med en bestämmelse som så nära som möjligt överensstämmer med den ursprungliga bestämmelsens syfte och återstoden av detta Avtal ska fortsätta gälla med full verkan.

Detta Avtal (och övriga regler och villkor som refereras till häri) utgör hela avtalet mellan dig och Tripadvisor beträffande din användning av Tjänsterna och ersätter alla tidigare eller samtida elektroniska, muntliga eller skriftliga meddelanden och erbjudanden mellan dig och Tripadvisor vad beträffar Tjänsterna. Ett tryckt exemplar av detta Avtal och eventuella meddelanden som förmedlats i elektronisk form utgör en godtagbar och giltig handling vid rättsliga eller administrativa processer som har sitt ursprung i eller berör Avtalet, i samma utsträckning och under samma villkor som andra affärsdokument som ursprungligen framställts och handhafts i skriftlig form.

Följande avsnitt ska fortsätta att gälla vid uppsägning av detta Avtal, om tillämpligt:

* #### Tilläggsprodukter
    
* #### Förbjudna aktiviteter
    
* #### Omdömen, kommentarer och användning av andra Interaktiva områden; Beviljande av licens
    
    * #### Begränsa Tripadvisors licensrättigheter
        
* Resmål
    * Internationella resor
* Ansvarsfriskrivning
* Skadeersättning
* Programvara som en del av tjänster; Ytterligare mobila licenser
* #### Upphovsrätts- och varumärkesmeddelanden
    
    * #### Riktlinjer för anmälan om och borttagning av olagligt innehåll
        
* #### Ändringar av tjänsterna; Uppsägning
    
* #### Jurisdiktion och gällande lag
    
* #### Allmänna bestämmelser
    
* #### Tjänstehjälp
    

Reglerna och villkoren i detta Avtal är tillgängliga på det språk som används på Tripadvisors webbplatser och/eller appar där Tjänsterna kan kommas åt.

Webbplatserna och/eller apparna där Tjänsterna kan kommas åt kanske inte alltid uppdateras periodvis eller regelbundet och därmed kräver ingen lag att de registreras under utgivaransvar.

Fiktiva namn på företag, produkter, personer, karaktärer och/eller data som nämns i eller via Tjänsterna är inte avsedda att representera någon verklig person, något verkligt företag eller någon verklig produkt eller händelse.

Ingenting i detta Avtal ska anses tilldela någon tredje part rättigheter eller förmåner, undantaget att Tripadvisors företagsfilialer ska anses vara tredjepartsförmånstagare av detta Avtal.

Du förbjuds att överföra några av dina rättigheter eller skyldigheter enligt detta Avtal till någon annan utan vårt samtycke.

Alla rättigheter som inte uttryckligen beviljas häri förbehålles.

**TJÄNSTEHJÄLP**

För svar på dina frågor eller om du behöver få reda på hur du kan kontakta oss går du till vårt [hjälpcenter](https://www.tripadvisorsupport.com/sv-SE/hc/traveler), eller så skickar du ett e-postmeddelande till oss på [help@tripadvisor.com](mailto:help@tripadvisor.com). Eller så kan du skicka ett brev till:

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, USA

Observera att Tripadvisor LLC inte accepterar rättsliga meddelanden eller delgivning på något annat sätt än via papperskopior som levereras till den direkt ovanstående adressen, såvida inte tillämplig lagstiftning kräver det.  För att undvika oklarheter och utan begränsning accepterar vi därför inte meddelanden eller delgivning som görs till någon av våra företagsfilialer eller dotterbolag, förutom enligt vad som anges nedan.

**KONTAKTPERSON FÖR FÖRORDNINGEN OM DIGITALA TJÄNSTER FÖR ANVÄNDARE**

Du kan kontakta oss direkt med kontaktuppgifterna nedan:

E-postadress: [help@tripadvisor.com](mailto:help@tripadvisor.com)

**KONTAKTPERSON FÖR FÖRORDNINGEN OM DIGITALA TJÄNSTER OCH JURIDISKT OMBUD FÖR MYNDIGHETER**

Om du är en nationell myndighet i en medlemsstat, Europeiska kommissionen eller Europeiska rådet för digitala tjänster kan du kontakta oss direkt med kontaktuppgifterna nedan:

E-postadress: [poc-dsa@tripadvisor.com](mailto:poc-dsa@tripadvisor.com)

Pålitliga flaggare och professionella organ kan också kontakta oss med den här e-postadressen.

Offentliga myndigheter kan även kontakta Tripadvisor Ireland Limited, som är vårt juridiska ombud vid tillämpning av förordningen om digitala tjänster, med uppgifterna nedan:

Adress: 70 Sir John Rogerson’s Quay, Dublin 2, Irland

E-postadress: [dsa.notifications@tripadvisor.com](mailto:dsa.notifications@tripadvisor.com)

Telefonnummer: +35 315126124

Kontakta oss inte genom vårt juridiska ombud med de här uppgifterna om du inte kontaktar oss på uppdrag av ett av de offentliga organ som anges ovan eftersom Tripadvisor då inte svarar på din begäran. Om du har frågor om vår plattform eller vill skicka in ett klagomål kan du kontakta oss med kontaktuppgifterna i avsnitten ”TJÄNSTEHJÄLP” eller ”KONTAKTPERSON FÖR FÖRORDNINGEN OM DIGITALA TJÄNSTER FÖR ANVÄNDARE” ovan.

©2024 Tripadvisor LLC. Alla rättigheter förbehålles.

Senast uppdaterad 16 februari 2024

* [](#print "print")
* [](https://twitter.com/share?url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34344%26item%3D32204 "Twitter Share")
* [](https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34344%26item%3D32204 "Facebook Share")
* [](https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34344%26item%3D32204 "Linkedin Share")
* [](#email "email")