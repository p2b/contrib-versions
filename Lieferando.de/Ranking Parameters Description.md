How are the search results ranked?
==================================

**How are the search results ranked?** 

We use several different criteria when we’re deciding which restaurants, shops and businesses we will highlight. 

On the homescreen of our apps and website, we’ll often shine the spotlight on certain businesses. We do this to make it easier for you to find the businesses and items we think you’ll be interested in, whether it’s re-ordering an old favourite or discovering something new. We do this based on:

* What kind of food or items they sell
    
* The time of day
    
* The popularity of different cuisines or kinds of shops in your region
    
* Your order history 
    
* The business’s customer rating and performance
    
* The business’s delivery area and times
    
* Strategic partnerships between us and businesses
    
* Sponsoring
    

**The order of businesses**

After the homescreen section as discussed above the order in which businesses appear on our platform is subject to a wide variety of factors that interact in a dynamic way. Our goal is to show our customers the businesses that they are likely to order from, in a way which is fair for businesses connected to our platform, whilst optimising for a great ordering experience. We’ve designed a ranking algorithm that balances the needs of our customers, our partners and our couriers.

In addition to the default sorting which is referred to as the _‘’best match’’_, customers can use filters to make their selection more specific, for example by only displaying certain cuisine types. Customers can also use the sorting options that are available, which will allow customers to sort their results on selected criteria, such as delivery fees. Filters and sorting options affect the order and types of businesses displayed to a specific customer based on how well those businesses meet the applicable filtering, sorting, or searching criteria of that customer.

The search results for the ‘’_best match_‘’ sorting and the position where companies appear on our platform is influenced by the following factors: 

**Organic**

The search results are displayed based on a weighted average which is influenced by various factors. The parameters as described below all impact a business's organic position, and each parameter comes with its own weight. The search results are then shown in descending order, where businesses with higher organic position will appear first. 

**Distance to the customer - (around 50%):** we prioritise businesses that are closer to the customer’s delivery address to ensure waiting times are short and that couriers only need to travel short distances for delivery. When selecting ‘Pick-up’ instead of ‘Delivery’, we sort all businesses based on distance. When a business is too far from the customer, this parameter does no longer contribute to a businesses position in the search results.

**New customer retention - (around 40%):** The capability of a business to convert new customers that return to place additional orders on our platform within a short time frame is an important factor for determining the position of a business. Businesses with this capability will appear higher up in the search results, as we value businesses that attract new loyal customers.

**Operational performance - (around 10%):** the operational performance of a business on our platform is measured via a variety of factors including order volume, reorder rates, successful orders and accuracy of order updates provided to customers. The better the operational performance the higher up the search list a business will appear as this helps us predict the probability of an order reaching the customer without any issues.

**New businesses**

Up to 20 new businesses temporarily receive an improved position in the search results. To help these businesses get started as well as for us to evaluate their performance, they receive a higher position for their first weeks. Where there are more than 20 new businesses operating in an area, the selection is randomised. We indicate new businesses with the ‘_New’_ label.

**Sponsoring**

In addition to the organic search results, we offer sponsorship services to our businesses in return for payment or other commercial conditions. Where enabled, this sponsorship service improves the organic position of a business on our platform. The effect that these sponsorship services have greatly depends on the businesses organic position and how each business uses the service. For example, where there are many restaurants operating in the same area, several restaurants could use the service to improve their positions to stand out. However, businesses are limited in the extent they can improve their position in the search results as the amount of improvement is linked to their original organic position. We want to ensure that even when businesses are using our sponsored services, the other ranking parameters will maintain their influence on a business’s position in the search results. Businesses using the sponsorship service can be recognised via the ‘_sponsored_’ label displayed on their listing. 

**Other factors** 

We may temporarily move businesses down the search results if we know that their orders are unlikely to be delivered within a reasonable time frame due to capacity issues at the business or our delivery services, for example due to severe weather conditions. In addition, both we or the relevant business can decide to temporarily "close" a business on the platform if it is too busy. This means the business will temporarily disappear from the search results. For chain businesses that operate multiple locations that can deliver to a customer's location, we show the closest by open location first and display other locations further across the list. We do this to ensure we can offer customers a wide variety of options to choose from.

Apart from the main ranking parameters as explained above, there are other parameters or circumstances which can influence the search results. For example and from time to time, we may run tests or experiments to analyse and improve our service, and these may have an impact on the search results.

* * *