If you find content that shouldn't be on Pinterest, you can report it for us to review. You’ll still be able to see someone’s account, boards, Pins and comments after you report them. To see less of their content, you can unfollow or block this person.

You can learn more about what's allowed on Pinterest our Community guidelines.

Report a Pin

* Click into the Pin
* Click  the ellipsis icon
* Click **Report Pin**
* Select your reason for reporting, then click **Next**
* Click **Report** to confirm

* Tap the Pin to open it
* Tap  the ellipsis icon
* Tap **Report Pin**
* Select your reason for reporting the Pin
* Tap **Report** to confirm

* Tap the Pin to open it
* Tap  the ellipsis icon
* Tap **Report**
* Select your reason for reporting the Pin
* Tap **Report** to confirm

Report an account

* Open the profile of the person you want to report
* Click  the ellipsis icon  at the top-right corner of their profile
* Click **Report**
* Select your reason for reporting, then click **Next**
* Click **Report** to confirm

* Open the profile of the person you want to report
* Tap  the ellipsis icon  to the right of **Follow**
* Tap **Report account**
* Select your reason for reporting
* Tap **Report** to confirm

* Open the profile of the person you want to report
* Tap  the ellipsis icon  to the right of **Follow**
* Tap **Report**
* Select your reason for reporting
* Tap **Report** to confirm

Report a comment or photo comment

* Click into the Pin
* Click  the dialog icon  or scroll down to the comment section
* Click  the ellipsis icon  under the comment you want to report 
* Click **Report this content**
* Select the reason for your report, then click **Next**
* Click **Report**

To report a photo comment for Intellectual Property rights, contact us. Include the Pin URL and the username of the person who posted the comment in your message.

* Tap the Pin to open it  
* Tap  the dialog icon  to view comments
* Tap  the ellipsis icon  under the comment you want to report
* Tap **Report**
* Select the reason for your report
* Tap **Report** to confirm

To report a photo comment for Intellectual Property rights, contact us. Include the Pin URL and the username of the person who posted the comment in your message.

* Tap the Pin to open it
* Tap  the dialog icon  to view comments
* Tap  the ellipsis icon  under the comment you want to report
* Tap **Report**
* Select the reason for your report
* Tap **Report** to confirm

To report a photo comment for Intellectual Property rights, contact us. Include the Pin URL and the username of the person who posted the comment in your message.

Report a message

* Click  the speech-ellipsis icon  to open your **Inbox**
* Select the conversation you want to report
* Click  the ellipsis icon  at the top-right corner of the conversation
* Select the reason for your report, then click **Next**
* Click **Report** to confirm

* Tap  the dialog-ellipsis icon , then tap **Inbox**
* Select the conversation you want to report
* Tap  the ellipsis icon  at the top-right corner of the conversation
* Tap **Report this conversation**
* Select the reason for your report
* Tap **Report** to confirm

* Tap  the dialog-ellipsis icon , then tap **Messages**
* Select the conversation you want to report
* Tap  the ellipsis icon  at the top-right corner of the conversation
* Tap **Report conversation**
* Select the reason for your report
* Tap **Report** to confirm

Report a board

* Open the board you want to report
* Click  the ellipsis icon  at the top-right corner of the board
* Click **Report**
* Select your reason for your report, then click **Next**
* Click **Report** to confirm * Open the board you want to report
* Tap  the ellipsis icon  at the top-right corner of the board
* Tap **Report**
* Select the reason for your report
* Tap **Report** to confirm * Open the board you want to report
* Tap  the ellipsis icon  at the top-right corner of the board
* Tap **Report**
* Select the reason for your report
* Tap **Report** to confirm