Obchodné podmienky vstupujúce v platnosť dňa **20\. augusta 2024**.

**1\. Predmet**
---------------

Comuto SA (ďalej len „**BlaBlaCar**“) vyvinula platformu zdieľania jázd (ďalej len „**Spolujazdu**“), ktorá je k dispozícii na webovej stránke na adrese [www.blablacar.sk](https://www.blablacar.sk/) alebo vo forme mobilnej aplikácie, a ktorá je navrhnutá tak, (i) aby zabezpečila kontakt vodičov idúcich do určitého miesta s cestujúcimi, ktorí idú rovnakým smerom s cieľom umožniť im zdieľanie jazdy, a teda aj s tým spojených nákladov a (ii) objednávať lístky na cesty autobusom prevádzkované Prevádzkovateľmi autobusov (ďalej len ako „**Platforma**“).

Účelom týchto podmienok je regulácia prístupu a podmienok použitia platformy. Pozorne si ich prečítajte. Beriete na vedomie a uznávate, že BlaBlaCar nie je stranou žiadnej dohody, zmluvy alebo zmluvných vzťahov, akejkoľvek povahy, uzavretej medzi členmi jej platformy.

Kliknutím na tlačidlo „Prihlásiť sa cez Facebook“ alebo „Registrácia e-mailovou adresou“ uznávate, že ste si prečítali a prijali všetky tieto všeobecné podmienky použitia.

Ak používate svoj účet na prihlásenie na platformu BlaBlaCar v inej krajine (napríklad www.blablacar.com.br), vezmite na vedomie, že (i) zmluvné podmienky tejto platformy, (ii) zásady ochrany osobných údajov tejto platformy a (iii) budú platiť zákonné pravidlá a predpisy danej krajiny. To tiež znamená, že informácie o vašom účte vrátane osobných údajov možno bude potrebné preniesť právnickej osobe prevádzkujúcej túto inú platformu. Upozorňujeme, že spoločnosť BlaBlaCar si vyhradzuje právo obmedziť prístup k platforme pre používateľov, ktorí sú odôvodnene identifikovaní ako osoby nachádzajúce sa mimo územia Európskej únie.

**2\. Definície**
-----------------

V tomto dokumente,

„**Inzerát**“ znamená inzerát o jazde pridaný na platforme vodičom;

„**Inzerát na spolujazdu**“ znamená inzerát týkajúci sa jazdy uverejnený vodičom na platforme;

„**Inzerát na autobus**“ znamená inzerát týkajúci sa cesty autobusom prevádzkovateľa autobusovej dopravy uverejnený na platforme;

„**Prevádzkovateľ autobusovej dopravy**“ označuje spoločnosť profesionálne prepravujúcu cestujúcich a lístky na cesty, ktoré BlaBlaCar distribuuje na platforme;

„**Lístok**“ znamená nominatívne platné oprávnenie na prepravu odovzdané spotrebiteľovi po rezervácii cesty autobusom, ako doklad o existujúcej zmluve o preprave medzi cestujúcimi a prevádzkovateľom autobusovej dopravy, ktorú upravujú VOP predaja bez ovplyvnenia ľubovoľnej konkrétnej podmienky dodatočne stanovenej medzi cestujúcim a prevádzkovateľom autobusovej dopravy a odkazovanej na lístku;

„**BlaBlaCar**“ je definovaný uvedený v článku 1 vyššie;

„**Podmienky**“ znamenajú tieto podmienky;

„**VOP predaja**“ znamenajú všeobecné obchodné podmienky predaja príslušného prevádzkovateľa autobusovej dopravy v závislosti od cesty autobusom vybranej zákazníkom a osobitné obchodné podmienky dostupné na internetovej stránke, a ktoré zákazník pred objednaním potvrdil ako prečítané;

„**Zákazník**“ znamená ľubovoľnú fyzickú osobu (člena alebo nie) kupujúcu si, buď pre seba, alebo v mene niekoho iného, kto bude cestujúcim, lístok na autobus prostredníctvom platformy na uskutočnenie cesty realizovanej prevádzkovateľom autobusovej dopravy;

„**Objednávka**“ znamená operáciu, ktorou si zákazník rezervuje svoje služby u BlaBlaCar Bus, bez ohľadu na použité prostriedky, s jedinou výnimkou nákupu lístkov vykonaného priamo na mieste predaja, a ktorá zahŕňa povinnosť cestujúceho alebo možno agenta zaplatiť cenu týkajúcu sa príslušných služieb;

„**Účet**“ znamená účet, ktorý musí byť vytvorený za účelom stať sa členom a získať prístup k určitým službám, ktoré ponúka platforma;

„**Facebook účet**“ je definovaný v článku 3.2 nižšie;

„**Vodič**“ je člen využívajúci platformu, ktorý ponúka prepravu inej osobe výmenou za príspevok na náklady na jazdu a v čase, ktoré sú definované samotným vodičom;

Význam „**Potvrdenie rezervácie**“ je uvedený v článku 4.2.1 nižšie;

Význam „**Členský obsah**“ je uvedený v článku 11.2 nižšie;

„**Člen**“ znamená akýkoľvek jednotlivec, ktorý si vytvoril účet na platforme;

„**Cestujúci**“ znamená člen, ktorý prijal ponuku byť prepravovaný vodičom alebo prípadne osoba, v ktorej mene člen rezervoval miesto;

„**Príspevok na náklady**“ znamená sumu peňazí za danú jazdu požadovanú vodičom a akceptovanú cestujúcim predstavujúcu príspevok na náklady na jazdu;

„**Miesto**“ znamená sedadlo rezervované cestujúcim vo vozidle vodiča;

Význam „**Platforma**“ je uvedený v článku 1 vyššie;

„**Miesto predaja**“ označuje fyzické priehradky a terminály uvedené na internetovej stránke a kde sú lístky ponúkané na predaj;

„**Prepravné služby**“ označujú prepravné služby predplatené cestujúcim cesty autobusom a poskytované prevádzkovateľom autobusovej dopravy;

„**Cena**“ označuje pre uvedenú cestu autobusom cenu zahŕňajúcu všetky dane, poplatky a náklady príslušných zahrnutých služieb, uhradenú zákazníkom na platforme, v okamihu potvrdenia objednávky, pre miesto na danej ceste autobusom;

Význam „**Rezervácia**“ je uvedený v článku 4.2.1 nižšie;

„**Služby**“ znamená všetky služby, ktoré poskytuje BlaBlaCar prostredníctvom platformy;

„**Internetová stránka**“ je webová stránka, ktorá je prístupná na adrese [www.blablacar.sk](https://www.blablacar.sk/);

význam „**Úsek**“ je uvedený v článku 4.1 nižšie;

„**Jazda**“ je jazda, ktorá je predmetom inzerátu zverejneného na platforme vodičom, a v rámci ktorej súhlasí s prepravou cestujúcich výmenou za príspevok na náklady;

„**Spolujazda**“ znamená cestu, ktorá je predmetom inzerátu na spolujazdu  uverejneného vodičom na platforme a pri ktorej súhlasí s prepravou cestujúcich výmenou za príspevok na náklady;

„**Cesta autobusom**“ znamená cestu, ktorá je predmetom inzerátu na autobus na platforme a pri ktorej prevádzkovateľ autobusovej dopravy navrhne miesta v autobusových vozidlách výmenou za cenu;

„**Servisné poplatky**” majú význam uvedený v článku 5.2 nižšie.

**3\. Registrácia na platforme a vytvorenie účtu**
--------------------------------------------------

### **3.1. Podmienky registrácie na platforme**

Platforma môže byť používaná osobami vo veku 18 a viac rokov. Registrácia neplnoletých osôb na platforme je prísne zakázaná. Prístupom, používaním alebo registráciou na platforme potvrdzujete, že ste vo veku 18 a viac rokov.

### **3.2. Vytvorenie účtu**

Platforma umožňuje členom pridávanie, prezeranie inzerátov a vzájomnú komunikáciu za účelom rezervácie miesta. Ak nie ste registrovaný na platforme, môžete si prezerať inzeráty. Bez predchádzajúceho vytvorenia účtu a stania sa členom však nie je možné pridanie inzerátu na spolujazdu alebo rezervácia miesta.

Ak chcete vytvoriť účet, môžete:

(i) buď vyplniť všetky povinné polia v registračnom formulári;

(ii) alebo sa prihlásiť k Facebook účtu prostredníctvom našej platformy (ďalej len ako váš „**Facebook účet**“). Pri použití takejto funkcie beriete na vedomie, že BlaBlaCar bude mať prístup k vášmu Facebook účtu, bude ho zverejňovať na platforme a bude z neho získavať určité informácie. Pomocou sekcie „Overenia“ vášho profilu môžete kedykoľvek odstrániť prepojenie vášho účtu a Facebook účtu. Ak sa chcete dozvedieť viac o používaní vašich údajov z vášho Facebook účtu, prečítajte si naše [Zásady ochrany osobných údajov](https://blog.blablacar.sk/about-us/privacy-policy) a zásady ochrany osobných údajov Facebooku.

Ak sa chcete zaregistrovať na platforme, musíte si prečítať a prijať tieto Obchodné podmienky.

Pri vytváraní účtu bez ohľadu na zvolenú metódu súhlasíte s poskytovaním presných a pravdivých informácií a ich aktualizáciou prostredníctvom vášho profilu alebo upovedomenia BlaBlaCar s cieľom zaručenia ich relevantnosti a presnosti počas celej doby trvania zmluvných vzťahov s BlaBlaCar.

V prípade registrácie e-mailom súhlasíte, že heslo zvolené pri vytváraní účtu budete udržiavať v tajnosti a nikomu ho nezverejníte. Ak dôjde k strate alebo zverejneniu vášho hesla, musíte o tom bezodkladne informovať BlaBlaCar. Za používanie vášho účtu tretími osobami ste zodpovední, pokiaľ ste BlaBlaCar výslovne neupozornili na stratu, podvodné použitie treťou osobou alebo prezradenie hesla tretej osobe.

Súhlasíte, že v rámci vašej vlastnej identity alebo identity tretej osoby nebudete vytvárať alebo používať iné účty než tie, ktoré boli pôvodne vytvorené.

### **3.3. Overenie**

BlaBlaCar môže za účelom transparentnosti, zlepšenia dôvery, alebo prevencie alebo odhaľovania podvodov zriadiť systém na overovanie niektorých z informácií, ktoré poskytujete vo vašom profile. Ide najmä o prípad, keď zadáte svoje telefónne číslo alebo nám poskytnete doklad totožnosti.

Uznávate a akceptujete, že akýkoľvek odkaz na platforme alebo v službách na „overené“ informácie (vrátane „Overeného profilu“), alebo akýkoľvek podobný termín, znamená len to, že člen úspešne prešiel overovacím postupom existujúcim na platforme alebo v službách s cieľom poskytnúť vám viac informácií o členovi, nad ktorým uvažujete, že s ním budete cestovať. BlaBlaCar nemôže zaručiť pravdivosť, spoľahlivosť alebo platnosť informácií, ktoré sú predmetom overovacieho postupu.

**4\. Využívanie služieb**
--------------------------

### **4.1. Pridávanie inzerátov**

Za predpokladu, že spĺňate podmienky uvedené nižšie, môžete ako člen na platforme vytvárať a pridávať inzeráty na spolujazdu zadaním informácií o jazde, ktorú máte v úmysle vykonať (dátumy/časy, miesta stretnutia, príchod, počet ponúkaných miest, možnosti k dispozícii, suma príspevku na náklady, atď.).

Pri pridávaní inzerátu na spolujazdu môžete uviesť zastávky v rámci jazdy, v ktorých súhlasíte so zastavením, vyzdvihnutím alebo vysadením cestujúcich. Úseky jazdy medzi týmito zastávkami v rámci jazdy alebo medzi jedným z týchto miest v rámci jazdy a nástupným miestom alebo výstupným miestom predstavujú „**Úseky**“.

Pridať inzerát na spolujazdu ste oprávnení len vtedy, ak spĺňate všetky nasledujúce podmienky:

(i) ste držiteľom platného vodičského preukazu;

(ii) ponúkate len inzeráty s vozidlami, ktoré vlastníte alebo používate s výslovným súhlasom vlastníka a v každom prípade ste oprávnení k ich používaniu na účely spolujazdy;

(iii) ste a zostanete hlavným vodičom vozidla, ktoré je predmetom inzerátu;

(iv) vozidlo má platné poistenie tretích osôb, predovšetkým povinné zmluvné poistenie;

(v) nemáte žiadne kontraindikácie alebo zdravotnú nespôsobilosť na vedenie vozidla;

(vi) vozidlo, ktoré máte v úmysle použiť na jazdu, je osobné vozidlo so 4 kolesami a maximálne 7 sedadlami;

(vii) nemáte v úmysle pridať na platforme ďalší inzerát na rovnakú jazdu;

(viii) neponúkate viac miest, ako je počet dostupných sedadiel vo vozidle;

(ix) všetky ponúkané miesta majú bezpečnostný pás, aj keď je vozidlo schválené so sedadlami, ktoré nemajú bezpečnostný pás;

(x) používate vozidlo, ktoré je v dobrom technickom stave a je v súlade s platnými právnymi predpismi a zvyklosťami, najmä s platným osvedčením o technickej kontrole vozidla;

(xi) ste zákazníkom a nekonáte ako profesionál.

Uznávate, že ste výhradne zodpovední za obsah inzerátu na spolujazdu, ktorý pridáte na platforme. V dôsledku toho vyjadrujete a zaručujete správnosť a pravdivosť všetkých informácií obsiahnutých vo vašom inzeráte a zaručujete sa za to, že absolvujete jazdu za podmienok popísaných vo vašom inzeráte na spolujazdu.

Váš inzerát na spolujazdu bude pridaný na platformu a bude tak viditeľný členom a všetkým návštevníkom, a to aj nečlenom vykonávajúcim vyhľadávanie na platforme alebo na webových stránkach alebo v mobilných aplikáciách partnerov BlaBlaCar. BlaBlaCar si podľa svojho uváženia vyhradzuje právo kedykoľvek nepridať a odstrániť akýkoľvek inzerát na spolujazdu, ktorý nie je v súlade s podmienkami alebo ktorý považuje za škodlivý pre svoj imidž, imidž platformy alebo služieb, a/alebo pozastaviť účet člena pridávajúci takéto inzeráty podľa 9. sekcie týchto Obchodných podmienok.

Beriete na vedomie, že v prípade, ak sa pri používaní platformy prezentujete ako spotrebiteľ, hoci v skutočnosti konáte ako profesionál, vystavujete sa sankciám podľa príslušných právnych predpisov.

### **4.2. Rezervácia miesta**

Spoločnosť BlaBlaCar vytvorila systém na rezerváciu Sedadiel online („**Rezervácia**“) pre jazdy ponúkané na platforme.

Metódy rezervácie miesta závisia od povahy predpokladanej jazdy. U niektorých jázd BlaBlaCar zriadila on-line rezervačný systém.

Spoločnosť BlaBlaCar poskytuje používateľom na platforme vyhľadávač založený na rôznych kritériách vyhľadávania (miesto odjazdu, cieľ, dátum, počet cestujúcich atď.). Určité ďalšie funkcie sú poskytované vo vyhľadávači, keď je používateľ pripojený k svojmu účtu. Spoločnosť BlaBlaCar vyzýva používateľa, aby bez ohľadu na použitý postup rezervácie starostlivo sledoval a používal vyhľadávač na určenie ponuky, ktorá je najviac prispôsobená jeho potrebám. Ďalšie informácie nájdete [tu](https://blog.blablacar.sk/about-us/transparentnost-platforiem). Zákazník rezervujúci si cestu autobusom na niektorom mieste predaja môže tiež požiadať prevádzkovateľa autobusovej dopravy alebo pracovníka za priehradkou, aby vykonal hľadanie.

**4.2.1. Inzerát na spolujazdu**

Keď má cestujúci záujem o inzerát na spolujazdu, u ktorého je možnosť rezervácie, môže vykonať on-line žiadosť o rezerváciu. Táto žiadosť o rezerváciu je buď (i) prijatá automaticky (ak vodič zvolil pri pridávaní svojho inzerátu túto možnosť ) alebo je (ii) manuálne potvrdená vodičom. V čase rezervácie cestujúci zaplatí online poplatky za služby, ak sa uplatňujú. Po prijatí platby spoločnosťou BlaBlaCar a overení žiadosti o rezerváciu vodičom cestujúci (ak je žiadosť prijatá) dostane potvrdenie rezervácie („**Potvrdenie rezervácie**“).

Ak ste vodič a pri pridávaní svojich inzerátov na spolujazdu ste si zvolili manuálne spravovanie žiadostí o rezerváciu, ste povinní odpovedať na žiadosť o rezerváciu v časovom rámci stanovenom cestujúcim pri žiadosti o rezerváciu. V opačnom prípade žiadosť o rezerváciu automaticky vyprší a cestujúcemu sa vrátia všetky čiastky zaplatené v čase požiadavky o rezerváciu, ak také existujú.

BlaBlaCar odošle v čase potvrdenia rezervácie telefónne číslo vodiča (ak ste cestujúci) alebo cestujúceho (ak ste vodič), ak člen súhlasí so zobrazením svojho telefónneho čísla. Potom ste výhradne zodpovední za plnenie zmluvy, ktorá vás zaväzuje k ostatným členom.

**4.2.2.****Cesta autobusom**

V prípade ciest autobusom umožňuje BlaBlaCar objednávanie autobusových lístkov na danú cestu autobusom prostredníctvom platformy.

Prepravné služby upravujú VOP predaja príslušného prevádzkovateľa autobusovej dopravy, v závislosti od cesty vybranej zákazníkom, ktoré musí zákazník akceptovať pred zadaním objednávky. BlaBlaCar neposkytuje žiadne prepravné služby týkajúce sa ciest autobusom, prevádzkovatelia autobusovej dopravy sú jedinou stranou VOP predaja. Beriete na vedomie, že objednávanie miest na danú cestu autobusom podlieha VOP predaja príslušného prevádzkovateľa autobusovej dopravy.

BlaBlaCar vás upozorňuje na skutočnosť, že určité prepravné služby ponúkané prevádzkovateľom autobusovej dopravy a uvedené na internetovej stránke môžu byť zrušené, predovšetkým z klimatických dôvodov, sezónnej špičky alebo v prípade zásahu vyššej moci.

**4.2.3 Viazanosť na meno pri rezervácii miesta a podmienky použitia služieb v mene tretej osoby**

Akékoľvek použitie služieb zo strany cestujúceho alebo vodiča je viazané na konkrétne meno. Vodič a cestujúci musia zodpovedať identite oznámenej spoločnosti BlaBlaCar a ostatným členom, ktorí sa zúčastňujú jazdy.

BlaBlaCar aj napriek tomu umožňuje svojim členom rezerváciu jedného alebo viacerých miest pre tretiu stranu. V tomto prípade sa zaväzujete, že vodičovi v čase rezervácie presne uvediete krstné mená, vek a telefónne číslo osoby, v mene ktorej ste rezervovali miesto. Rezervácia miesta pre maloletú osobu mladšiu ako 13 rokov, ktorá cestuje sama, je prísne zakázaná. V prípade rezervácie miesta pre maloletú osobu vo veku nad 13, ktorá cestuje sama, sa zaväzujete vyžiadať si predchádzajúci súhlas vodiča a poskytnúť mu riadne vyplnený a podpísaný súhlas jeho/jej zákonných zástupcov.

Platforma je určená na rezerváciu miest pre osoby. Rezervácia miesta na prepravu akéhokoľvek objektu, balíčka, zvieraťa cestujúceho samostatne alebo akéhokoľvek materiálneho predmetu je zakázaná.

Okrem toho je zakázané zverejňovať inzerát na spolujazdu pre iného vodiča, než ste Vy.

### **4.3. Členský obsah, moderácia a systém hodnotenia**

**4.3.1. Prevádzka systému hodnotenia**

BlaBlaCar vám odporúča napísať hodnotenie vodiča (ak ste cestujúci) alebo cestujúceho (ak ste vodič), s ktorým ste cestovali alebo s ktorým ste mali cestovať (t. j. rezervovali ste si cestu). Hodnotenie ale nemôžete napísať na ďalšieho cestujúceho, ak ste bol cestujúci, alebo na člena, s ktorým ste necestovali alebo s ktorým ste nemali cestovať. Máte možnosť zanechať hodnotenie  v 14-dňovej lehote po skončení cesty.

Vaše hodnotenie a hodnotenie napísané iným členom o vás, ak existuje, sú viditeľné a zverejnené na platforme krátko po týchto obdobiach: (i) ihneď potom, ako ste obaja napísali hodnotenie alebo (ii) po uplynutí doby 14 dní po prvom hodnotení.

Máte možnosť reagovať na hodnotenie, ktoré na vás vo vašom profile napísal iný člen, so 14-dňovým oneskorením po dátume prijatia hodnotenia. Hodnotenie a vaša odpoveď budú podľa potreby zverejnené vo vašom profile.

**4.3.2. Moderácia**

**a. Členský obsah**

Uznávate a akceptujete, že BlaBlaCar môže členský obsah pred jeho zverejnením moderovať použitím automatizovaných nástrojov alebo ručne, ako je definované v článku 11.2. Ak BlaBlaCar usúdi, že takýto členský obsah porušuje platné zákony alebo tieto Obchodné podmienky, vyhradzuje si právo:

* Zabrániť zverejneniu alebo odstrániť takýto členský obsah,
* Odoslať upozornenie členovi s pripomenutím povinnosti dodržiavať platné zákony alebo tieto Obchodné podmienky, a/alebo
* Uplatniť reštriktívne opatrenia podľa 9. sekcie týchto Obchodných podmienok.

Použitie takýchto automatizovaných nástrojov alebo manuálneho moderovania spoločnosťou BlaBlaCar sa nemá chápať ako záväzok monitorovať či povinnosť aktívne vyhľadávať nezákonné aktivity a/alebo členský obsah zverejnený na platforme a do rozsahu povoleného platnými zákonmi neukladá spoločnosti BlaBlaCar žiadnu zodpovednosť.

**b. Správy**

Výmena správ medzi členmi prostredníctvom našej platformy („správy“) slúži výhradne na výmenu informácií týkajúcich sa jázd.

BlaBlaCar môže pomocou automatizovaného softvéru a algoritmov rozpoznávať obsah správ za účelom prevencie podvodov, zlepšovania služieb a zákazníckej podpory a realizácie zmlúv uzavretých s našimi členmi (ako sú tieto Obchodné podmienky). V prípade detekcie ľubovoľného takéhoto obsahu v správe, ktorý vykazuje znaky podvodného alebo nezákonného správania, obchádzania platformy alebo ak inak porušuje tieto Obchodné podmienky:

* Takýto obsah nesmie byť zverejnený.
* Člen, ktorý správu odoslal, môže dostať upozornenie s pripomenutím povinnosti dodržiavať platné zákony alebo tieto Obchodné podmienky, alebo
* Účet člena môže byť pozastavený podľa 9. Sekcie týchto Obchodných podmienok.

Použitie takéhoto automatizovaného softvéru spoločnosťou BlaBlaCar sa nemá chápať ako záväzok monitorovať či povinnosť aktívne vyhľadávať nezákonné aktivity a/alebo obsah na platforme, a do rozsahu povoleného platnými zákonmi neukladá spoločnosti BlaBlaCar žiadnu zodpovednosť.

**4.3.3. Obmedzenie**

Podľa 9. sekcie týchto Obchodných podmienok si BlaBlaCar vyhradzuje právo pozastaviť váš účet, obmedziť váš prístup k službám alebo ukončiť tieto podmienky v prípade, (i) ak ste dostali najmenej tri hodnotenia a (ii) ak je jedno alebo priemerné hodnotenie, ktoré ste dostali, menšie alebo rovné 3, v závislosti od závažnosti spätnej väzby zanechanej členom v hodnotení.

**5\. Finančné podmienky**
--------------------------

Prístup na platformu a registrácia na nej, ako aj vyhľadávanie, prezeranie a zverejňovanie inzerátov sú bezplatné. Rezervácia je však spoplatnená za podmienok opísaných nižšie.

### **5.1. Príspevok na náklady a cena**

5.1.1. V prípade spolujazdy príspevok na náklady určujete vy ako vodič na vašu výhradnú zodpovednosť. Zarábanie akýmkoľvek spôsobom pomocou našej platformy je prísne zakázané. V dôsledku toho súhlasíte s obmedzením príspevku na náklady, ktorý žiadate od vašich cestujúcich, na úhradu nákladov na jazdu, ktoré vám skutočne vzniknú. V opačnom prípade vy sami ponesiete riziká reklasifikácie transakcie uskutočnenej prostredníctvom platformy.

Pri pridávaní inzerátu na spolujazdu vám BlaBlaCar navrhne sumu príspevku na náklady, ktorá najmä zohľadňuje povahu jazdy a prejdenú vzdialenosť. Táto suma je daná len ako návod a je len na vás, či ju zvýšite alebo znížite, aby ste zohľadnili náklady na jazdu, ktoré vám skutočne vzniknú. Aby sa zabránilo zneužívaniu, BlaBlaCar obmedzuje možnosť úpravy príspevku na náklady.

5.1.2. V súvislosti s cestou autobusom je cena za miesto definovaná podľa výlučného uváženia prevádzkovateľa autobusovej dopravy. Vyzývame zákazníka, aby si preštudoval príslušné VOP predaja a oboznámil sa s platnými spôsobmi v súvislosti s vytvorením objednávky lístkov a spôsobmi platby.

### **5.2. Servisné poplatky**

Spoločnosť BlaBlaCar výmenou za používanie platformy v čase rezervácie vyberá poplatky za služby (ďalej len „**Servisné poplatky**“). Používateľ bude informovaný pred akýmkoľvek uplatnením servisných poplatkov, ak sa to vyžaduje. 

Spôsoby výpočtu servisných poplatkov, ktoré nájdete [tu](https://www.blablacar.co.uk/blablalife/lp/service-fees), môže BlaBlaCar zverejniť samostatne, majú len informatívny charakter a nemajú zmluvnú povahu. Servisné poplatky môžu byť vypočítané podľa rôznych faktorov, najmä podľa dĺžky a ceny jazdy. Spoločnosť BlaBlaCar si vyhradzuje právo kedykoľvek zmeniť spôsoby výpočtu servisných poplatkov. Tieto zmeny nebudú mať vplyv na servisné poplatky, ktoré používateľ akceptoval pred dátumom nadobudnutia účinnosti týchto zmien.

V prípade cezhraničných jázd upozorňujeme, že metódy výpočtu výšky servisných poplatkov a uplatniteľnej DPH sa líšia v závislosti od miesta vyzdvihnutia a/alebo cieľa jazdy.

Pri používaní Platformy na cezhraničné jazdy alebo mimo Slovenska môže servisné poplatky účtovať pridružená spoločnosť BlaBlaCar prevádzkujúca miestnu platformu.

### **5.3. Zaokrúhľovanie**

Uznávate a akceptujete fakt, že BlaBlaCar môže podľa svojho uváženia zaokrúhliť nahor alebo nadol na celé čísla alebo desatinné miesta príspevok na náklady a servisný poplatok.

### **5.4. Spôsoby platby a zaplatenie príspevku na náklady vodičovi alebo ceny prevádzkovateľom autobusovej dopravy**

**5.4.1. Platba príspevku na náklady vodičovi**

Cestujúci sa zaväzuje zaplatiť príspevok na náklady vodičovi najneskôr na výstupnom mieste.

Vodič sa zaväzuje, že nebude pred začiatkom jazdy požadovať vyplatenie celého príspevku alebo časti príspevku na náklady.

**5.4.2. Platba ceny**

Platba za ľubovoľnú objednávku uskutočnenú prostredníctvom platformy sa realizuje jedným z nižšie povolených spôsobov. Zákazník má možnosť uložiť si informácie súvisiace s jednou alebo viacerými kreditnými kartami vo svojom účte zákazníka, aby nemusel systematicky zadávať tieto informácie pri vykonávaní následných platieb.

Spôsoby platby sa môžu líšiť v závislosti od platformy BlaBlaCar, ktorú používate. Povolené spôsoby platby sú tieto:

* Banková karta (akceptované značky sú zobrazené na platforme. V prípade, že máte kobrandovanú kartu, môžete vybrať konkrétnu značku vo fáze platby)
* Paypal
* Poukazy
* Apple Pay, Google Pay.

Pred úplným a platným zaplatením ceny prepravných služieb vybraných zákazníkom nebude vystavené žiadne potvrdenie objednávky. Ak je platba nepravidelná, neúplná alebo neprebehne z nejakého dôvodu pripísateľného zákazníkovi, objednávka bude okamžite zrušená.

Objednávka je potvrdená odoslaním e-mailu zákazníkovi, ktorý obsahuje podrobnosti objednávky.

Cestujúceho vyzývame, aby si skontroloval nastavenia doručenej pošty svojej e-mailovej adresy a predovšetkým aby sa uistil, že potvrdzovací e-mail nie je odoslaný priamo do spamu.

Potvrdenie objednávky je konečné. Akékoľvek zmeny preto povedú buď k výmene, alebo zrušeniu podľa podmienok platných VOP predaja. Je povinnosťou zákazníka uistiť sa, že prepravné služby sa vyberajú v súlade s potrebami a očakávaniami cestujúceho. Za presnosť osobných údajov zadaných zákazníkom zodpovedá on sám okrem prípadu, keď sa preukáže, že BlaBlaCar nedokáže zhromažďovať, uchovávať alebo chrániť takéto osobné údaje.

**6\. Nekomerčný a neobchodný účel služieb a platformy**
--------------------------------------------------------

Súhlasíte s tým, že služby a platformu budete používať len na neobchodnom a nekomerčnom základe na skontaktovanie sa s ľuďmi, ktorí chcú s vami zdieľať spolujazdu alebo si objednať miesto v súvislosti s cestou autobusom.

V súvislosti so spolujazdou ste uznali, že práva spotrebiteľa vyplývajúce z práva Únie na ochranu spotrebiteľa sa nevzťahujú na váš vzťah s ostatnými členmi.

Ako vodič súhlasíte s tým, že nebudete požadovať príspevok na náklady, ktorý je vyšší, než sú skutočne vzniknuté náklady a ktorý môže vytvárať zisk, pričom je stanovené, že v rámci zdieľania nákladov budete ako vodič musieť znášať vlastnú časť nákladov na jazdu. Ste výhradne zodpovední za výpočet nákladov, ktoré vám vzniknú na jazdu a za kontrolu, že suma požadovaná od vašich cestujúcich neprekračuje skutočne vzniknuté náklady (s výnimkou vášho podielu na nákladoch).

BlaBlaCar si vyhradzuje právo pozastaviť váš účet v prípade, ak používate vozidlo riadené osobným vodičom, iné podnikateľské vozidlo, taxi alebo služobné vozidlo a vďaka tomu zarábate na platforme. Súhlasíte s tým, že BlaBlaCar poskytnete na základe jej jednoduchej žiadosti, kópiu osvedčenia o evidencii vozidla a/alebo iný doklad, ktorý potvrdzuje, že ste oprávnení používať toto vozidlo na platforme a že na tom nezarábate.

Podľa 9. sekcie týchto Obchodných podmienok si BlaBlaCar tiež vyhradzuje právo pozastaviť váš účet, obmedziť vám prístup k službám, alebo ukončiť tieto podmienky, v prípade aktivity na platforme, ktorá vzhľadom na povahu ponúkaných jázd, ich frekvenciu, počet prepravených cestujúcich a požadovaný príspevok na náklady, vám vytvára zisk, alebo z akéhokoľvek dôvodu, na základe ktorého sa BlaBlaCar domnieva, že na platforme vytvárate zisk.

**7\. Zrušenie**
----------------

### **7.1. Podmienky vrátenia peňazí v prípade zrušenia**

Týmto podmienkam zrušenia podliehajú len spolujazdy. BlaBlaCar neponúka v prípade zrušenia z ľubovoľného dôvodu žiadnu záruku akéhokoľvek druhu. V súvislosti s cestou autobusom podliehajú podmienky zrušenia VOP predaja prevádzkovateľov autobusovej dopravy. Zákazníka vyzývame, aby si preštudoval príslušné VOP predaja a zobral na vedomie podmienky výmen a zrušenia svojej cesty autobusom.

Zrušenie miesta na spolujazduovej jazde zo strany vodiča alebo cestujúceho po potvrdení rezervácie podlieha nižšie uvedeným ustanoveniam: 

– V prípade zrušenia z dôvodu na strane vodiča sa cestujúcemu vráti servisný poplatok. To platí najmä v prípade, ak vodič:

* nepotvrdil žiadosť o rezerváciu v stanovenej lehote (ak si vodič takúto možnosť zvolil);
* zruší spolujazdu alebo sa nedostaví na miesto stretnutia 15 minút po dohodnutom čase;

\-V prípade zrušenia z dôvodu na strane cestujúceho si spoločnosť BlaBlaCar ponecháva servisné poplatky.

Spôsob vrátenia servisného poplatku sa určuje v závislosti od spôsobu, ktorý bol použitý na zaplatenie servisného poplatku, pričom lehota na vrátenie servisného poplatku závisí od termínov prevodov bankovými organizáciami.

Ak pred odchodom dôjde k zrušeniu vinou cestujúceho, mieso (-a) zrušené cestujúcim sa automaticky sprístupnia ostatným cestujúcim, ktorí si ich môžu on-line rezervovať, čo znamená, že sa riadia týmito podmienkami.

BlaBlaCar podľa vlastného uváženia na základe dostupných informácií oceňuje oprávnenosť žiadostí o vrátenie peňazí.

### **7.2.  Právo na odstúpenie od zmluvy**

Prijatím týchto obchodných podmienok výslovne súhlasíte s tým, že zmluva medzi vami a spoločnosťou BlaBlaCar spočívajúca v spojení s iným členom má byť uzavretá pred uplynutím lehoty na odstúpenie od zmluvy od potvrdenia rezervácie a výslovne sa vzdávate svojho práva na odstúpenie od zmluvy v súlade s dispozíciou. § 7 Zákona č. 102/2014 Z. z. o ochrane spotrebiteľa pri predaji tovaru alebo poskytovaní služieb na základe zmluvy uzavretej na diaľku alebo zmluvy uzavretej mimo prevádzkových priestorov predávajúceho.

**8\. Správanie používateľov platformy a členov**
-------------------------------------------------

### **8.1. Záväzok všetkých používateľov platformy**

Uznávate, že ste výhradne zodpovední za rešpektovanie všetkých zákonov, predpisov a povinnosti týkajúcich sa používania platformy.

Okrem toho sa pri použití platformy a pri jazdách zaväzujete:

(i) nepoužívať platformu na profesionálne, komerčné alebo zárobkové účely;

(ii) neposielať BlaBlaCar (najmä po vytvorení alebo aktualizácii vášho účtu) alebo ostatným členom akékoľvek nepravdivé, zavádzajúce, škodlivé alebo podvodné informácie;

(iii) v žiadnom prípade na platforme vyjadrovať a ani sa správať, alebo na ňu pridávať akýkoľvek obsah (vrátane správ) hanlivej, poškodzujúcej, obscénnej, pornografickej, vulgárnej, urážlivej, agresívnej, nemiestnej, nevyžiadanej, násilnej, výhražnej, obťažujúcej, rasistickej či xenofóbnej povahy, alebo so sexuálnymi konotáciami, s podnecovaním k násiliu, diskriminácii alebo nenávisti, k povzbudzovaniu k činnosti alebo používaniu zakázaných látok alebo všeobecnejšie nezákonný obsah, ktorý je v rozpore s platnými zákonmi, týmito Obchodnými podmienkami a cieľmi tejto platformy, ktorý by mohol porušovať práva BlaBlaCar alebo tretej osoby alebo je v rozpore s dobrými mravmi;

(iv) neporušovať práva a imidž BlaBlaCar, najmä jej práva duševného vlastníctva;

(v) neotvoriť si na platforme viac ako jeden účet a neotvoriť si účet na meno tretej osoby;

(vi) nesnažiť sa obchádzať on-line systém rezervácie platformy, najmä pokúšaním sa odoslať inému členovi vlastné kontaktné údaje, aby vykonal rezerváciu mimo platformy a neplatil servisné poplatky;

(vii) nekontaktovať iného člena, a to najmä prostredníctvom platformy, za iným účelom, aký vymedzujú podmienky pre zdieľanie jázd;

(viii) neprijímať alebo nevykonávať platbu mimo platformy;

(ix) podrobiť sa týmto obchodným podmienkam.

### **8.2. Záväzky vodiča**

Ak platformu používate ako vodič, okrem tohto sa ďalej zaväzujete:

(i) dodržiavať všetky zákony a predpisy vzťahujúce sa na jazdu a vozidlo, najmä mať v čase jazdy zo zákona povinné platné zmluvné poistenie a byť držiteľom platného vodičského preukazu

(ii) overiť si, či sa vaše poistenie vzťahuje na zdieľanie jázd a či sú vaši cestujúci vo vašom vozidle považovaní za tretie osoby a sú preto počas celej jazdy, aj cezhraničnej, kryté poistením

(iii) neriskovať pri jazde, nepožívať alebo nepoužívať akýkoľvek výrobok, ktorý môže oslabiť vašu pozornosť a vaše schopnosti ostražitého a úplne bezpečného vedenia vozidla;

(iv) pridávať len inzeráty na spolujazdu, ktoré zodpovedajú skutočne plánovaným jazdám;

(v) uskutočniť spolujazdu tak, ako je popísaná v inzeráte na spolujazdu (najmä s ohľadom na použitie alebo nepoužitie diaľnice), ako aj rešpektovať časy a miesta dohodnuté s ostatnými členmi (najmä nástupné a výstupné miesta);

(vi) nebrať viac cestujúcich, ako je počet miest uvedený v inzeráte na spolujazdu;

(vii) používať vozidlo, ktoré je v dobrom stave a je v súlade s platnými právnymi predpismi a zvyklosťami, najmä s platným osvedčením o technickej kontrole vozidla;

(viii) predložiť BlaBlaCar alebo akémukoľvek cestujúcemu, ktorý o to požiada, váš vodičský preukaz, osvedčenie o evidencii vozidla, potvrdenie o poistení, osvedčenie o technickej kontrole vozidla a akýkoľvek doklad preukazujúci vašu schopnosť používať na platforme vozidlo ako vodič;

(ix) v prípade omeškania alebo zmeny času alebo spolujazdy bezodkladne informovať vašich cestujúcich;

(x) v prípade cezhraničnej spolujazdy držať a uchovávať pre cestujúceho a pre akýkoľvek iný orgán, ktorý o to môže požiadať, akýkoľvek dokument preukazujúci vašu identitu a vaše právo na prekročeniu hranice;

(xi) počkať na cestujúcich na dohodnutom mieste stretnutia po dobu najmenej 15 minút po dohodnutom čase;

(xii) nepridávať na účely zdieľania jázd inzerát na spolujazdu s vozidlom, ktoré vám nepatrí, alebo ktoré nie ste oprávnení používať;

(xiii) zabezpečiť, aby ste mohli byť vašimi cestujúcimi telefonicky kontaktovaní na čísle evidovanom vo vašom profile;

(xiv) nevytvárať prostredníctvom platformy žiadny zisk;

(xv) nemať žiadne kontraindikácie alebo zdravotnú nespôsobilosť na vedenie vozidla

(xvi) počas spolujazdy sa správať primerane a zodpovedne a v súlade s duchom spolujazdy;

(xvii) neodmietnuť akúkoľvek rezerváciu na základe rasy, farby pleti, etnického pôvodu, národnosti, náboženstva, sexuálnej orientácie, rodinného stavu, zdravotného postihnutia, fyzického vzhľadu, tehotenstva, osobitnej zraniteľnosti vzhľadom k ich ekonomickej situácii, mena, bydliska, zdravotného stavu, politického presvedčenia, veku;

(xviii) zabezpečiť, aby ste mohli byť vašimi cestujúcimi telefonicky kontaktovaní na čísle evidovanom vo vašom profile, a to aj na mieste vyzdvihnutia.

### **8.3. Záväzky cestujúcich**

**8.3.1. Pre spolujazdu**

Ak platformu používate ako cestujúci, zaväzujete sa:

(i) počas spolujazdy sa vhodne správať tak, aby ste nerušili koncentráciu alebo jazdu vodiča alebo pokoj a pohodu ostatných cestujúcich;

(ii) rešpektovať vozidlo vodiča a jeho čistotu;

(iii) v prípade meškania bezodkladne informovať vodiča;

(iv) zaplatiť vodičovi dohodnutý príspevok na náklady;

(v) počkať na vodiča na dohodnutom miesto stretnutia po dobu najmenej 15 minút po dohodnutom čase;

(vi) predložiť BlaBlaCar alebo akémukoľvek vodičovi, ktorý o to požiada, váš občiansky preukaz alebo akýkoľvek iný doklad preukazujúci vašu totožnosť;

(vii) neprevážať počas spolujazdy akýkoľvek predmet, tovar, látku alebo zviera, ktoré by mohlo brániť v jazde a rušiť koncentráciu vodiča, alebo ktorý má takú povahu, že jeho držanie alebo preprava je v rozpore s platnými právnymi predpismi;

(viii) v prípade cezhraničnej spolujazdy držať a uchovávať pre vodiča a pre akýkoľvek iný orgán, ktorý o to môže požiadať, akýkoľvek dokument preukazujúci vašu identitu a vaše právo na prekročeniu hranice;

(ix) zabezpečiť, aby ste mohli byť vaším vodičom telefonicky kontaktovaní na čísle evidovanom vo vašom profile, a to aj na mieste vyzdvihnutia.

Ak ste za tretie osoby vykonali rezerváciu jedného alebo viacerých miest, v súlade s ustanovením článku 4.2.3 vyššie, sa zaručujte za rešpektovanie ustanovení tohto článku a všeobecne týchto podmienok touto treťou osobou. BlaBlaCar si vyhradzuje právo pozastaviť váš účet, obmedziť vám prístup k službám, alebo ukončiť tieto podmienky, v prípade porušenia treťou osobou, v mene ktorej ste rezervovali miesto na základe týchto podmienok, podľa 9. sekcie týchto Obchodných podmienok.

**8.3.2. Pre cestu autobusom**

V súvislosti s cestou autobusom sa cestujúci zaväzuje dodržiavať VOP predaja príslušného prevádzkovateľa autobusovej dopravy.

**8.4. Nahlasovanie nevhodného alebo nezákonného obsahu (mechanizmus upozornenia a opatrení)**

Podozrivý, nevhodný alebo nezákonný obsah člena alebo správu môžete nahlásiť, ako je popísané [tu](https://support.blablacar.com/s/article/Nahlasovanie-neleg%C3%A1lneho-obsahu-1729197120609?language=sk).  

Spoločnosť BlaBlaCar po riadnom oznámení v súlade s týmto oddielom alebo príslušnými orgánmi bezodkladne odstráni akýkoľvek nezákonný obsah člena, ak Členský obsah je zjavne nezákonný alebo v rozpore s platnými predpismi; alebo  
spoločnosť BlaBlaCar považuje takýto obsah za porušujúci tieto VOP.  

V takýchto prípadoch si spoločnosť BlaBlaCar vyhradzuje právo odstrániť riadne nahlásený členský obsah dostatočne podrobným a jasným spôsobom a/alebo okamžite pozastaviť nahlásené konto.  

Príslušný člen sa môže proti uvedeným rozhodnutiam spoločnosti BlaBlaCar odvolať, ako je opísané v časti 15.1 nižšie.

**9\. Obmedzenia súvisiace s používaním platformy, pozastavenie účtov, obmedzenie prístupu a ukončenie**
--------------------------------------------------------------------------------------------------------

Vaše zmluvné vzťahy s BlaBlaCar môžete bez nákladov a bez dôvodu ukončiť kedykoľvek. K tomu stačí prejsť na kartu vášho profilu „Zrušiť účet“.

V prípade (i) vášho porušenia týchto podmienok, vrátane, ale s obmedzením sa na vaše povinnosti ako člena uvedené v článkoch 6 a 8 vyššie, (ii) prekročenia limitu uvedeného v článku 4.3.3 vyššie, alebo (iii) ak má BlaBlaCar skutočný dôvod sa domnievať, že je nevyhnutná ochrana jej bezpečnosti a jej integrity, a tiež bezpečnosti a integrity členov alebo tretích osôb, alebo za účelom vyšetrovaní alebo predchádzania podvodom, si BlaBlaCar vyhradzuje právo:

(i) s okamžitou účinnosťou a bez predchádzajúceho upozornenia ukončiť podmienky, ktoré vás viažu s BlaBlaCar; a/alebo

(ii) zabrániť pridaniu alebo odstráneniu akéhokoľvek členského obsahu pridaného na platformu; a/alebo

(iii) obmedziť váš prístup a vaše používanie platformy; a/alebo

(iv) dočasne alebo trvalo pozastaviť váš účet.

Pozastavenie účtu môže tiež znamenať, že v príslušných prípadoch nebudete dostávať žiadne čakajúce výplaty.

V príslušných prípadoch vám BlaBlaCar tiež môže poslať upozornenie s pripomenutím povinnosti dodržiavať platné zákony a/alebo tieto Obchodné podmienky.

Ak je to nevyhnutné, budete upozornení na zavedenie takýchto opatrení, aby vám bolo umožnené potvrdiť váš záväzok dodržiavať tieto Obchodné podmienky a platné zákony, poskytnúť BlaBlaCar vysvetlenia alebo sa odvolať proti tomuto rozhodnutiu. BlaBlaCar po zohľadnení všetkých okolností každého prípadu a závažnosti porušenia na základe vlastného uváženia rozhodne, či zruší alebo nezruší zavedené opatrenia.

**10\. Osobné údaje**
---------------------

V súvislosti s používaním platformy bude spoločnosť BlaBlaCar zhromažďovať a spracovávať niektoré z vašich osobných údajov, ako je opísané v Zásadách [ochrany osobných údajov](https://blog.blablacar.sk/about-us/privacy-policy).

**11\. Duševné vlastníctvo**
----------------------------

### **11.1. Obsah zverejnený spoločnosťou BlaBlaCar**

Okrem obsahov poskytnutých svojimi členmi je BlaBlaCar jediným držiteľom všetkých práv duševného vlastníctva vzťahujúcich sa k tejto službe, platforme, ich obsahu (najmä texty, obrázky, vzory, logá, videá, zvuky, dáta, grafika) a softvéru a databázam, ktoré zabezpečujú ich prevádzku.

BlaBlaCar vám udeľuje nevýhradné, osobné a neprenosné právo na používanie platformy a služieb na osobné a súkromné použitie, na nekomerčnom základe a v súlade s cieľmi platformy a služieb.

Akékoľvek iné použitie alebo využitie platformy a služieb a ich obsahu bez predchádzajúceho písomného súhlasu BlaBlaCar je zakázané. Obzvlášť máte zakázané:

(i) reprodukovanie, úpravy, prispôsobenie, distribúciu, verejné reprezentovanie a šírenie platformy, služieb a obsahu, s výnimkou prípadov s výslovným povolením spoločnosťou BlaBlaCar;

(ii) dekompiláciu a reverzné inžinierstvo platformy alebo služieb, s výnimkami ustanovenými textami, ktoré sú v platnosti;

(iii) extrahovanie alebo pokus o extrahovanie (najmä pri použití robotov na dolovanie údajov alebo iného podobného nástroja na zber údajov) podstatnej časti údajov platformy.

### **11.2. Obsah, ktorý zverejníte na platforme**

S cieľom umožniť poskytovanie služieb a v súlade s účelom platformy udeľujete spoločnosti BlaBlaCar nevýhradnú licenciu na používanie obsahu a údajov, ktoré poskytujete v rámci vášho používania služieb, ktoré môžu zahŕňať vaše žiadosti o rezervácie, inzeráty a komentáre v nich, životopisy, fotky, hodnotenia a reakcie na hodnotenia (ďalej len ako váš „**Členský obsah**“) a správy. S cieľom umožniť BlaBlaCar distribuovať a zverejniť obsah platformy cez digitálne siete a v súlade s akýmkoľvek komunikačným protokolom (najmä internet a mobilná sieť), môžete BlaBlaCar udeliť povolenie pre celý svet a po celú dobu trvania vašich zmluvných vzťahov s BlaBlaCar, reprodukovať, reprezentovať, prispôsobovať a prekladať váš členský obsah takto:

(i) oprávňujete BlaBlaCar reprodukovať celý alebo časť vášho členského obsahu na akomkoľvek známom alebo doteraz neznámom digitálnom záznamovom médiu, a to najmä na akomkoľvek serveri, pevnom disku, na pamäťovej karte alebo akomkoľvek inom rovnocennom médiu, v akomkoľvek formáte a akýmkoľvek známym alebo doteraz neznámym spôsobom, a to v rozsahu potrebnom na akékoľvek ukladanie, zálohovanie, prenos alebo sťahovanie spojené s fungovaním platformy a poskytovaním služby;

(ii) oprávňujete BlaBlaCar upraviť a preložiť členský obsah a reprodukovať tieto úpravy na všetkých súčasných alebo budúcich digitálnych médiách, uvedených v bode (i), s cieľom poskytovania služieb, a to najmä v rôznych jazykoch. Toto právo zahŕňa najmä možnosť vykonávania zmien formátovania vášho členského obsahu, pokiaľ ide o vaše morálne právo, za účelom rešpektovania grafickej charty platformy a/alebo zaistenia jeho technickej zlučiteľnosti s ohľadom na jeho zverejňovanie prostredníctvom platformy.

Naďalej zodpovedáte za členský obsah a správy, ktoré nahráte na našu platformu, a máte k nim všetky práva.

**12\. Úloha BlaBlaCar**
------------------------

Platforma predstavuje on-line platformu, na ktorej môžu členovia vytvárať a pridávať inzeráty na spolujazdy za účelom spolujazdy. Tieto inzeráty na spolujazdy si môžu prezrieť najmä ostatní členovia, aby si zistili podmienky jazdy a prípadne si priamo rezervovali miesto v danom vozidle s členom, ktorý pridal inzerát na platformu. Platforma tiež umožňuje rezerváciu miest na cesty autobusom.

Používaním platformy a akceptovaním týchto podmienok uznávate, že BlaBlaCar nie je stranou (i) akejkoľvek dohody uzavretej medzi vami a ostatnými členmi za účelom zdieľania nákladov súvisiacich so spolujazdou, (ii) ani akejkoľvek dohody uzavretej medzi vami a prevádzkovateľom autobusovej dopravy.

BlaBlaCar nemá žiadnu kontrolu nad správaním sa svojich členov, prevádzkovateľov autobusovej dopravy a ich agentov, a používateľov platformy. Nevlastní, nevyužíva, nedodáva alebo nespravuje vozidlá, ktoré sú predmetom inzerátu, a neponúka na platforme žiadne jazdy.

Uznávate a akceptujete fakt, že BlaBlaCar nekontroluje platnosť, pravdivosť alebo zákonnosť inzerátov, miest a ponúkaných jázd. V rámci svojej funkcie prostredníka zdieľania jázd BlaBlaCar neposkytuje žiadne prepravné služby a nepôsobí vo funkcii prepravcu; úloha BlaBlaCar je obmedzená na uľahčenie prístupu k platforme.

V súvislosti so spolujazdami členovia (vodiči alebo cestujúci) konajú vo svojej výlučnej a plnej zodpovednosti.

V rámci svojej funkcie prostredníka BlaBlaCar nemôže byť zodpovedná za efektívny výskyt jazdy, a to najmä z dôvodu:

(i) chybných informácií týkajúcich sa jazdy a jej podmienok oznámených vodičom alebo prevádzkovateľom autobusovej dopravy v jeho inzeráte alebo akýmkoľvek iným spôsobom;

(ii) zrušenia alebo zmeny jazdy členom alebo prevádzkovateľom autobusovej dopravy;

(iii) nezaplatenia príspevku na náklady cestujúcim;

(iv) správania sa jej členov počas, pred alebo po jazde.

**13\. Fungovanie, dostupnosť a funkčnosť platformy**
-----------------------------------------------------

BlaBlaCar sa maximálnym možným spôsobom snaží zachovať platformu prístupnú 7 dní v týždni a 24 hodín denne. Napriek tomu môže byť prístup k platforme bez predchádzajúceho upozornenia dočasne pozastavený z dôvodu technickej údržby, migrácie, aktualizácie, v dôsledku výpadku alebo obmedzenia spojeného s prevádzkou siete.

Spoločnosť BlaBlaCar si navyše vyhradzuje právo zmeniť platformu alebo jej časti pre niektorých používateľov s cieľom otestovať nové funkcie a ponúknuť lepšiu používateľskú skúsenosť, ako aj zmeniť alebo pozastaviť celý alebo čiastočný prístup k platforme alebo jej funkciám podľa vlastného uváženia, dočasne alebo natrvalo.

**14\. Úprava podmienok**
-------------------------

Tieto podmienky a dokumenty integrované odkazom vyjadrujú úplnú dohodu medzi vami a BlaBlaCar ohľadom užívania služieb. Akýkoľvek iný dokument, najmä akákoľvek zmienka o platforme (často kladené otázky, atď.), slúžia len na účely všeobecných zásad.

BlaBlaCar môže tieto podmienky upraviť za účelom prispôsobenia sa svojmu technologickému a komerčnému prostrediu a zosúladenia s platnými právnymi predpismi. Akákoľvek zmena týchto podmienok bude zverejnené na platforme s uvedením dňa nadobudnutia účinnosti, a pred začiatkom ich platnosti budete upozornení spoločnosťou BlaBlaCar.

**15\. Rozhodné právo – Spor**
------------------------------

Tieto podmienky sú písané v slovenčine a podliehajú slovenskému právu.

**15.1 Interný systém riešenia sťažností**

Môžete sa odvolať proti rozhodnutiam, ktoré môžeme prijať v súvislosti s:

* Členským obsahom: napríklad sme odstránili, obmedzili viditeľnosť alebo odmietli odstrániť ľubovoľný členský obsah, ktorý poskytnete pri používaní platformy, alebo
* Vaším účtom: pozastavili sme váš prístup k platforme tam, kde sme prijali takéto rozhodnutia na základe faktu, že členský obsah predstavuje nezákonný obsah alebo je nekompatibilný s týmito Obchodnými podmienkami. Proces odvolania je [opísaný tu](https://support.blablacar.com/s/article/Ako-sa-odvola%C5%A5-vo%C4%8Di-odstr%C3%A1neniu-obsahu-alebo-pozastaveniu-%C3%BA%C4%8Dtu-1729197123042?language=sk).

**15.2 Mimosúdne riešenie sporov**

V prípade potreby môžete tiež predložiť vaše sťažnosti týkajúce sa našej platformy alebo našich služieb na platforme pre riešenie sporov, umiestnenej on-line Európskou komisiou, ktorá je prístupná [tu](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage). Európska komisia odošle vašu sťažnosť príslušnému národnému ombudsmanovi. V súlade s pravidlami platnými pre mediáciu ste za účelom dosiahnutia priateľského vyriešenie pred akoukoľvek žiadosťou o mediáciu povinní písomne upovedomiť BlaBlaCar o akomkoľvek spore.

Môžete tiež podať žiadosť orgánu pre riešenie sporov vo svojej krajine (zoznam nájdete [tu](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)). 

**16\. Právne oznámenia**
-------------------------

Platforma je publikovaná spoločnosťou Comuto SA, ktorá je spoločnosťou s ručením obmedzeným so základným imaním 155,880.999 EUR, je zapísaná do obchodného registra v Paríži pod číslom 491.904.546 (identifikačné číslo pre DPH v rámci Spoločenstva: FR76491904546), má sídlo na adrese 84, avenue de la République, 75011 Paris (Francúzsko) a je zastúpená svojím generánym riaditeľom Nicolasom Brussonom, zodpovedným zástupcom webovej platformy.

Internetová stránka je hostovaná na serveroch Google Cloud v Holandsku.

Spoločnosť Comuto SA je zapísaná v registri prevádzkovateľov cestovných a pobytových služieb pod číslom: IM075180037.

Finančnú záruku poskytuje: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Paríž – Francúzsko.

Poistenie zodpovednosti za škodu spôsobenú pri výkone povolania je uzatvorené: Spoločnosť Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paríž – Francúzsko.

Spoločnosť Comuto SA je zapísaná v registri sprostredkovateľov poistenia, bankovníctva a financií pod registračným číslom (Orias) 15003890.

V prípade akýchkoľvek otázok sa môžete obrátiť na Comuto SA prostredníctvom [tohto kontaktného formulára](https://support.blablacar.com/s/contactsupport?language=sk).

**17\. Digital Services Act (Akt o digitálnych službách)**
----------------------------------------------------------

**17.1.  Informácie o priemernom počte mesačných aktívnych príjemcov služby v Únii**

V súlade s článkom 24(2) nariadenia Európskeho parlamentu a Rady o internom trhu pre digitálne služby a pozmeňujúcej smernice 2000/31/ES („DSA“) sú poskytovatelia online platforiem povinní zverejňovať informácie o mesačnom priemere aktívnych príjemcov svojej služby v Únii, ktorý sa počíta ako priemer za posledných šesť mesiacov. Účelom tohto zverejňovania je zistiť, či poskytovateľ online platformy spĺňa kritériá „veľmi veľkých online platforiem“ podľa DSA, t. j. či prekračuje hranicu 45 miliónov priemerne mesačne aktívnych príjemcov v Únii.

K 17. augusta bol priemerný mesačný počet aktívnych príjemcov BlaBlaCar za obdobie medzi februárom a júlom 2024 vypočítaný pri zohľadnení úvodnej časti 77 a článku 3 DSA (započítavanie používateľov vystavených obsahu platformy iba raz počas obdobia 6 mesiacov, pred prijatím konkrétneho delegovaného zákona, približne 3,28 milióna v EÚ.

Tieto informácie sú zverejnené iba na účely splnenia požiadaviek DSA a nemalo by sa na ne spoliehať na iné účely. Budú aktualizované aspoň raz za šesť mesiacov. Náš prístup k vytvoreniu tohto výpočtu sa môže vyvíjať alebo si môže v priebehu času vyžadovať úpravy, napríklad kvôli zmenám produktov alebo novým technológiám.

**17.2. Kontaktné miesto pre úrady**

V súlade s článkom 11 DSA platí, že ak ste členom kompetentných úradov EÚ, Komisie EÚ alebo Európskej rady pre digitálne služby, môžete nás kontaktovať ohľadom záležitostí týkajúcich sa DSA e-mailom na [\[email protected\]](https://blog.blablacar.sk/cdn-cgi/l/email-protection).

Môžete nás kontaktovať v angličtine a francúzštine.

Upozorňujeme, že táto e-mailová adresa sa nepoužíva na komunikáciu s členmi. V prípade akýchkoľvek otázok týkajúcich sa použitia BlaBlaCar nás vy ako člen môžete kontaktovať prostredníctvom tohto [kontaktného formulára](https://support.blablacar.com/s/contactsupport?language=sk).

* * *

Obchodné podmienky vstupujúce v platnosť dňa 20. augusta 2024.

**1\. Predmet**
---------------

Comuto SA (ďalej len „**BlaBlaCar**“) vyvinula platformu zdieľania jázd (ďalej len „**Spolujazdu**“), ktorá je k dispozícii na webovej stránke na adrese [www.blablacar.sk](https://www.blablacar.sk/) alebo vo forme mobilnej aplikácie, a ktorá je navrhnutá tak, aby zabezpečila kontakt vodičov idúcich do určitého miesta s cestujúcimi, ktorí idú rovnakým smerom s cieľom umožniť im zdieľanie jazdy, a teda aj s tým spojených nákladov (ďalej len ako „**Platforma**“).

Účelom týchto podmienok je regulácia prístupu a podmienok použitia platformy. Pozorne si ich prečítajte. Beriete na vedomie a uznávate, že BlaBlaCar nie je stranou žiadnej dohody, zmluvy alebo zmluvných vzťahov, akejkoľvek povahy, uzavretej medzi členmi jej platformy.

Kliknutím na tlačidlo „Prihlásiť sa cez Facebook“ alebo „Registrácia e-mailovou adresou“ uznávate, že ste si prečítali a prijali všetky tieto všeobecné podmienky použitia.

Ak používate svoj účet na prihlásenie na platformu BlaBlaCar v inej krajine (napríklad www.blablacar.com.br), vezmite na vedomie, že (i) zmluvné podmienky tejto platformy, (ii) zásady ochrany osobných údajov tejto platformy a (iii) budú platiť zákonné pravidlá a predpisy danej krajiny. To tiež znamená, že informácie o vašom účte vrátane osobných údajov možno bude potrebné preniesť právnickej osobe prevádzkujúcej túto inú platformu. Upozorňujeme, že spoločnosť BlaBlaCar si vyhradzuje právo obmedziť prístup k platforme pre používateľov, ktorí sú odôvodnene identifikovaní ako osoby nachádzajúce sa mimo územia Európskej únie.

**2\. Definície**
-----------------

V tomto dokumente,

„**Inzerát**“ znamená inzerát o jazde pridaný na platforme vodičom;

„**BlaBlaCar**“ je definovaný uvedený v článku 1 vyššie;

„**Podmienky**“ znamenajú tieto podmienky;

„**Účet**“ znamená účet, ktorý musí byť vytvorený za účelom stať sa členom a získať prístup k určitým službám, ktoré ponúka platforma;

„**Facebook účet**“ je definovaný v článku 3.2 nižšie;

„**Vodič**“ je člen využívajúci platformu, ktorý ponúka prepravu inej osobe výmenou za príspevok na náklady na jazdu a v čase, ktoré sú definované samotným vodičom;

Význam „**Potvrdenie rezervácie**“ je uvedený v článku 4.2.1 nižšie;

Význam „**Členský obsah**“ je uvedený v článku 11.2 nižšie;

„**Člen**“ znamená akýkoľvek jednotlivec, ktorý si vytvoril účet na platforme;

„**Cestujúci**“ znamená člen, ktorý prijal ponuku byť prepravovaný vodičom alebo prípadne osoba, v ktorej mene člen rezervoval miesto;

„**Príspevok na náklady**“ znamená sumu peňazí za danú jazdu požadovanú vodičom a akceptovanú cestujúcim predstavujúcu príspevok na náklady na jazdu;

„**Miesto**“ znamená sedadlo rezervované cestujúcim vo vozidle vodiča;

Význam „**Platforma**“ je uvedený v článku 1 vyššie;

Význam „**Rezervácia**“ je uvedený v článku 4.2.1 nižšie;

„**Služby**“ znamená všetky služby, ktoré poskytuje BlaBlaCar prostredníctvom platformy;

„**Internetová stránka**“ je webová stránka, ktorá je prístupná na adrese [www.blablacar.sk](https://www.blablacar.sk/);

význam „**Úsek**“ je uvedený v článku 4.1 nižšie;

„**Jazda**“ je jazda, ktorá je predmetom inzerátu zverejneného na platforme vodičom, a v rámci ktorej súhlasí s prepravou cestujúcich výmenou za príspevok na náklady;

„**Servisné poplatky**” majú význam uvedený v článku 5.2 nižšie.

**3\. Registrácia na platforme a vytvorenie účtu**
--------------------------------------------------

### **3.1. Podmienky registrácie na platforme**

Platforma môže byť používaná osobami vo veku 18 a viac rokov. Registrácia neplnoletých osôb na platforme je prísne zakázaná. Prístupom, používaním alebo registráciou na platforme potvrdzujete, že ste vo veku 18 a viac rokov.

### **3.2. Vytvorenie účtu**

Platforma umožňuje členom pridávanie a prezeranie inzerátov a vzájomnú komunikáciu za účelom rezervácie miesta. Ak nie ste registrovaný na platforme, môžete si prezerať inzeráty. Bez predchádzajúceho vytvorenia účtu a stania sa členom však nie je možné pridanie inzerátu alebo rezervácia miesta.

Ak chcete vytvoriť účet, môžete:

(i) buď vyplniť všetky povinné polia v registračnom formulári;

(ii) alebo sa prihlásiť k Facebook účtu prostredníctvom našej platformy (ďalej len ako váš „**Facebook účet**“). Pri použití takejto funkcie beriete na vedomie, že BlaBlaCar bude mať prístup k vášmu Facebook účtu, bude ho zverejňovať na platforme a bude z neho získavať určité informácie. Pomocou sekcie „Overenia“ vášho profilu môžete kedykoľvek odstrániť prepojenie vášho účtu a Facebook účtu. Ak sa chcete dozvedieť viac o používaní vašich údajov z vášho Facebook účtu, prečítajte si naše [Zásady ochrany osobných údajov](https://blog.blablacar.sk/about-us/privacy-policy) a zásady ochrany osobných údajov Facebooku.

Ak sa chcete zaregistrovať na platforme, musíte si prečítať a prijať tieto Obchodné podmienky.

Pri vytváraní účtu bez ohľadu na zvolenú metódu súhlasíte s poskytovaním presných a pravdivých informácií a ich aktualizáciou prostredníctvom vášho profilu alebo upovedomenia BlaBlaCar s cieľom zaručenia ich relevantnosti a presnosti počas celej doby trvania zmluvných vzťahov s BlaBlaCar.

V prípade registrácie e-mailom súhlasíte, že heslo zvolené pri vytváraní účtu budete udržiavať v tajnosti a nikomu ho nezverejníte. Ak dôjde k strate alebo zverejneniu vášho hesla, musíte o tom bezodkladne informovať BlaBlaCar. Za používanie vášho účtu tretími osobami ste zodpovední, pokiaľ ste BlaBlaCar výslovne neupozornili na stratu, podvodné použitie treťou osobou alebo prezradenie hesla tretej osobe.

Súhlasíte, že v rámci vašej vlastnej identity alebo identity tretej osoby nebudete vytvárať alebo používať iné účty než tie, ktoré boli pôvodne vytvorené.

### **3.3. Overenie**

BlaBlaCar môže za účelom transparentnosti, zlepšenia dôvery, alebo prevencie alebo odhaľovania podvodov zriadiť systém na overovanie niektorých z informácií, ktoré poskytujete vo vašom profile. Ide najmä o prípad, keď zadáte svoje telefónne číslo alebo nám poskytnete doklad totožnosti.

Uznávate a akceptujete, že akýkoľvek odkaz na platforme alebo v službách na „overené“ informácie (vrátane „Overeného profilu“), alebo akýkoľvek podobný termín, znamená len to, že člen úspešne prešiel overovacím postupom existujúcim na platforme alebo v službách s cieľom poskytnúť vám viac informácií o členovi, nad ktorým uvažujete, že s ním budete cestovať. BlaBlaCar nemôže zaručiť pravdivosť, spoľahlivosť alebo platnosť informácií, ktoré sú predmetom overovacieho postupu.

**4\. Využívanie služieb**
--------------------------

### **4.1. Pridávanie inzerátov**

Za predpokladu, že spĺňate podmienky uvedené nižšie, môžete ako člen na platforme vytvárať a pridávať inzeráty zadaním informácií o jazde, ktorú máte v úmysle vykonať (dátumy/časy, miesta stretnutia, príchod, počet ponúkaných miest, možnosti k dispozícii, suma príspevku na náklady, atď.).

Pri pridávaní inzerátu môžete uviesť zastávky v rámci jazdy, v ktorých súhlasíte so zastavením, vyzdvihnutím alebo vysadením cestujúcich. Úseky jazdy medzi týmito zastávkami v rámci jazdy alebo medzi jedným z týchto miest v rámci jazdy a nástupným miestom alebo výstupným miestom predstavujú „**Úseky**“.

Pridať inzerát ste oprávnení len vtedy, ak spĺňate všetky nasledujúce podmienky:

(i) ste držiteľom platného vodičského preukazu;

(ii) ponúkate len inzeráty s vozidlami, ktoré vlastníte alebo používate s výslovným súhlasom vlastníka a v každom prípade ste oprávnení k ich používaniu na účely spolujazdy;

(iii) ste a zostanete hlavným vodičom vozidla, ktoré je predmetom inzerátu;

(iv) vozidlo má platné poistenie tretích osôb, predovšetkým povinné zmluvné poistenie;

(v) nemáte žiadne kontraindikácie alebo zdravotnú nespôsobilosť na vedenie vozidla;

(vi) vozidlo, ktoré máte v úmysle použiť na jazdu, je osobné vozidlo so 4 kolesami a maximálne 7 sedadlami;

(vii) nemáte v úmysle pridať na platforme ďalší inzerát na rovnakú jazdu;

(viii) neponúkate viac miest, ako je počet dostupných sedadiel vo vozidle;

(ix) všetky ponúkané miesta majú bezpečnostný pás, aj keď je vozidlo schválené so sedadlami, ktoré nemajú bezpečnostný pás;

(x) používate vozidlo, ktoré je v dobrom technickom stave a je v súlade s platnými právnymi predpismi a zvyklosťami, najmä s platným osvedčením o technickej kontrole vozidla;

(xi) ste zákazníkom a nekonáte ako profesionál.

Uznávate, že ste výhradne zodpovední za obsah inzerátu, ktorý pridáte na platforme. V dôsledku toho vyjadrujete a zaručujete správnosť a pravdivosť všetkých informácií obsiahnutých vo vašom inzeráte a zaručujete sa za to, že absolvujete jazdu za podmienok popísaných vo vašom inzeráte.

Váš inzerát bude pridaný na platformu a bude tak viditeľný členom a všetkým návštevníkom, a to aj nečlenom vykonávajúcim vyhľadávanie na platforme alebo na webových stránkach alebo v mobilných aplikáciách partnerov BlaBlaCar. BlaBlaCar si podľa svojho uváženia vyhradzuje právo kedykoľvek nepridať a odstrániť akýkoľvek inzerát, ktorý nie je v súlade s podmienkami alebo ktorý považuje za škodlivý pre svoj imidž, imidž platformy alebo služieb, a/alebo pozastaviť účet člena pridávajúci takéto inzeráty podľa 9. sekcie týchto Obchodných podmienok.

Beriete na vedomie, že v prípade, ak sa pri používaní platformy prezentujete ako spotrebiteľ, hoci v skutočnosti konáte ako profesionál, vystavujete sa sankciám podľa príslušných právnych predpisov.

Uznávate a prijímate, že kritériá, ktoré sú brané do úvahy pri klasifikácii a poradí zobrazenia vášho inzerátu medzi ostatnými inzerátmi, sú výhradne na uvážení BlaBlaCar.

### **4.2. Rezervácia miesta**

Spoločnosť BlaBlaCar vytvorila systém na rezerváciu Sedadiel online („**Rezervácia**“) pre jazdy ponúkané na platforme.

Metódy rezervácie miesta závisia od povahy predpokladanej jazdy. U niektorých jázd BlaBlaCar zriadila on-line rezervačný systém.

Spoločnosť BlaBlaCar poskytuje používateľom na platforme vyhľadávač založený na rôznych kritériách vyhľadávania (miesto odjazdu, cieľ, dátum, počet cestujúcich atď.). Určité ďalšie funkcie sú poskytované vo vyhľadávači, keď je používateľ pripojený k svojmu účtu. Spoločnosť BlaBlaCar vyzýva používateľa, aby bez ohľadu na použitý postup rezervácie starostlivo sledoval a používal vyhľadávač na určenie ponuky, ktorá je najviac prispôsobená jeho potrebám. Ďalšie informácie nájdete tu. Zákazník rezervujúci si cestu autobusom na niektorom mieste predaja môže tiež požiadať prevádzkovateľa autobusovej dopravy alebo pracovníka za priehradkou, aby vykonal hľadanie.

**4.2.1. Inzerát na spolujazdu**

Keď má cestujúci záujem o inzerát, u ktorého je možnosť rezervácie, môže vykonať on-line žiadosť o rezerváciu. Táto žiadosť o rezerváciu je buď (i) prijatá automaticky (ak vodič zvolil pri pridávaní svojho inzerátu túto možnosť ) alebo je (ii) manuálne potvrdená vodičom. V čase rezervácie cestujúci zaplatí online poplatky za služby, ak sa uplatňujú. Po prijatí platby spoločnosťou BlaBlaCar a overení žiadosti o rezerváciu vodičom cestujúci (ak je žiadosť prijatá) dostane potvrdenie rezervácie („**Potvrdenie rezervácie**“).

Ak ste vodič a pri pridávaní Vášho inzerátu ste si zvolili manuálne spravovanie žiadostí o rezerváciu, ste povinní odpovedať na žiadosť o rezerváciu v časovom rámci stanovenom cestujúcim pri žiadosti o rezerváciu. V opačnom prípade žiadosť o rezerváciu automaticky vyprší a cestujúcemu sa vrátia všetky čiastky zaplatené v čase požiadavky o rezerváciu, ak také existujú.

BlaBlaCar odošle v čase potvrdenia rezervácie telefónne číslo vodiča (ak ste cestujúci) alebo cestujúceho (ak ste vodič), ak člen súhlasí so zobrazením svojho telefónneho čísla. Potom ste výhradne zodpovední za plnenie zmluvy, ktorá vás zaväzuje k ostatným členom.

**4.2.2. Viazanosť na meno pri rezervácii miesta a podmienky použitia služieb v mene tretej osoby**

Akékoľvek použitie služieb zo strany cestujúceho alebo vodiča je viazané na konkrétne meno. Vodič a cestujúci musia zodpovedať identite oznámenej spoločnosti BlaBlaCar a ostatným členom, ktorí sa zúčastňujú jazdy.

BlaBlaCar aj napriek tomu umožňuje svojim členom rezerváciu jedného alebo viacerých miest pre tretiu stranu. V tomto prípade sa zaväzujete, že vodičovi v čase rezervácie presne uvediete krstné mená, vek a telefónne číslo osoby, v mene ktorej ste rezervovali miesto. Rezervácia miesta pre maloletú osobu mladšiu ako 13 rokov, ktorá cestuje sama, je prísne zakázaná. V prípade rezervácie miesta pre maloletú osobu vo veku nad 13, ktorá cestuje sama, sa zaväzujete vyžiadať si predchádzajúci súhlas vodiča a poskytnúť mu riadne vyplnený a podpísaný súhlas jeho/jej zákonných zástupcov.

Platforma je určená na rezerváciu miest pre osoby. Rezervácia miesta na prepravu akéhokoľvek objektu, balíčka, zvieraťa cestujúceho samostatne alebo akéhokoľvek materiálneho predmetu je zakázaná.

Okrem toho je zakázané zverejňovať inzerát na alebo menom iného vodiča.

### **4.3. Členský obsah, moderácia a systém hodnotenia**

**4.3.1. Prevádzka systému hodnotenia**

BlaBlaCar vám odporúča napísať hodnotenie vodiča (ak ste cestujúci) alebo cestujúceho (ak ste vodič), s ktorým ste cestovali alebo s ktorým ste mali cestovať (t. j. rezervovali ste si cestu). Hodnotenie ale nemôžete napísať na ďalšieho cestujúceho, ak ste bol cestujúci, alebo na člena, s ktorým ste necestovali alebo s ktorým ste nemali cestovať. Máte možnosť zanechať hodnotenie  v 14-dňovej lehote po skončení cesty.

Vaše hodnotenie a hodnotenie napísané iným členom o vás, ak existuje, sú viditeľné a zverejnené na platforme krátko po týchto obdobiach: (i) ihneď potom, ako ste obaja napísali hodnotenie alebo (ii) po uplynutí doby 14 dní po prvom hodnotení.

Máte možnosť reagovať na hodnotenie, ktoré na vás vo vašom profile napísal iný člen, so 14-dňovým oneskorením po dátume prijatia hodnotenia. Hodnotenie a vaša odpoveď budú podľa potreby zverejnené vo vašom profile.

**4.3.2. Moderácia**

**a. Členský obsah**

Uznávate a akceptujete, že BlaBlaCar môže členský obsah pred jeho zverejnením moderovať použitím automatizovaných nástrojov alebo ručne, ako je definované v článku 11.2. Ak BlaBlaCar usúdi, že takýto členský obsah porušuje platné zákony alebo tieto Obchodné podmienky, vyhradzuje si právo:

* Zabrániť zverejneniu alebo odstrániť takýto členský obsah,
* Odoslať upozornenie členovi s pripomenutím povinnosti dodržiavať platné zákony alebo tieto Obchodné podmienky, a/alebo
* Uplatniť reštriktívne opatrenia podľa 9. sekcie týchto Obchodných podmienok.

Použitie takýchto automatizovaných nástrojov alebo manuálneho moderovania spoločnosťou BlaBlaCar sa nemá chápať ako záväzok monitorovať či povinnosť aktívne vyhľadávať nezákonné aktivity a/alebo členský obsah zverejnený na platforme a do rozsahu povoleného platnými zákonmi neukladá spoločnosti BlaBlaCar žiadnu zodpovednosť.

**b. Správy**

Výmena správ medzi členmi prostredníctvom našej platformy („správy“) slúži výhradne na výmenu informácií týkajúcich sa jázd.

BlaBlaCar môže pomocou automatizovaného softvéru a algoritmov rozpoznávať obsah správ za účelom prevencie podvodov, zlepšovania služieb a zákazníckej podpory a realizácie zmlúv uzavretých s našimi členmi (ako sú tieto Obchodné podmienky). V prípade detekcie ľubovoľného takéhoto obsahu v správe, ktorý vykazuje znaky podvodného alebo nezákonného správania, obchádzania platformy alebo ak inak porušuje tieto Obchodné podmienky:

* Takýto obsah nesmie byť zverejnený.
* Člen, ktorý správu odoslal, môže dostať upozornenie s pripomenutím povinnosti dodržiavať platné zákony alebo tieto Obchodné podmienky, alebo
* Účet člena môže byť pozastavený podľa 9. Sekcie týchto Obchodných podmienok.

Použitie takéhoto automatizovaného softvéru spoločnosťou BlaBlaCar sa nemá chápať ako záväzok monitorovať či povinnosť aktívne vyhľadávať nezákonné aktivity a/alebo obsah na platforme, a do rozsahu povoleného platnými zákonmi neukladá spoločnosti BlaBlaCar žiadnu zodpovednosť.

**4.3.3. Obmedzenie**

Podľa 9. sekcie týchto Obchodných podmienok si BlaBlaCar vyhradzuje právo pozastaviť váš účet, obmedziť váš prístup k službám alebo ukončiť tieto podmienky v prípade, (i) ak ste dostali najmenej tri hodnotenia a (ii) ak je jedno alebo priemerné hodnotenie, ktoré ste dostali, menšie alebo rovné 3, v závislosti od závažnosti spätnej väzby zanechanej členom v hodnotení.

**5\. Finančné podmienky**
--------------------------

Prístup na platformu a registrácia na nej, ako aj vyhľadávanie, prezeranie a zverejňovanie inzerátov sú bezplatné. Rezervácia je však spoplatnená za podmienok opísaných nižšie.

### **5.1. Príspevok na náklady**

Príspevok na náklady určujete vy ako vodič na vašu výhradnú zodpovednosť. Zarábanie akýmkoľvek spôsobom pomocou našej platformy je prísne zakázané. V dôsledku toho súhlasíte s obmedzením príspevku na náklady, ktorý žiadate od vašich cestujúcich, na úhradu nákladov na jazdu, ktoré vám skutočne vzniknú. V opačnom prípade vy sami ponesiete riziká reklasifikácie transakcie uskutočnenej prostredníctvom platformy.

Pri pridávaní inzerátu vám BlaBlaCar navrhne sumu príspevku na náklady, ktorá najmä zohľadňuje povahu jazdy a prejdenú vzdialenosť. Táto suma je daná len ako návod a je len na vás, či ju zvýšite alebo znížite, aby ste zohľadnili náklady na jazdu, ktoré vám skutočne vzniknú. Aby sa zabránilo zneužívaniu, BlaBlaCar obmedzuje možnosť úpravy príspevku na náklady.

### **5.2. Servisné poplatky**

Spoločnosť BlaBlaCar výmenou za používanie platformy v čase rezervácie vyberá poplatky za služby (ďalej len „**Servisné poplatky**“). Používateľ bude informovaný pred akýmkoľvek uplatnením servisných poplatkov, ak sa to vyžaduje. 

Spôsoby výpočtu servisných poplatkov, ktoré nájdete [tu](https://www.blablacar.co.uk/blablalife/lp/service-fees), môže BlaBlaCar zverejniť samostatne, majú len informatívny charakter a nemajú zmluvnú povahu. Servisné poplatky môžu byť vypočítané podľa rôznych faktorov, najmä podľa dĺžky a ceny jazdy. Spoločnosť BlaBlaCar si vyhradzuje právo kedykoľvek zmeniť spôsoby výpočtu servisných poplatkov. Tieto zmeny nebudú mať vplyv na servisné poplatky, ktoré používateľ akceptoval pred dátumom nadobudnutia účinnosti týchto zmien.

V prípade cezhraničných jázd upozorňujeme, že metódy výpočtu výšky servisných poplatkov a uplatniteľnej DPH sa líšia v závislosti od miesta vyzdvihnutia a/alebo cieľa jazdy.

Pri používaní Platformy na cezhraničné jazdy alebo mimo Slovenska môže servisné poplatky účtovať pridružená spoločnosť BlaBlaCar prevádzkujúca miestnu platformu.

### **5.3. Zaokrúhľovanie**

Uznávate a akceptujete fakt, že BlaBlaCar môže podľa svojho uváženia zaokrúhliť nahor alebo nadol na celé čísla alebo desatinné miesta príspevok na náklady a servisný poplatok.

### **5.4. Zaplatenie príspevku na náklady vodičovi**

Cestujúci sa zaväzuje zaplatiť príspevok na náklady vodičovi najneskôr na výstupnom mieste.

Vodič sa zaväzuje, že nebude pred začiatkom jazdy požadovať vyplatenie celého príspevku alebo časti príspevku na náklady.

**6\. Nekomerčný a neobchodný účel služieb a platformy**
--------------------------------------------------------

Súhlasíte s tým, že služby a platformu budete používať len na neobchodnom a nekomerčnom základe na skontaktovanie sa s ľuďmi, ktorí chcú s vami cestovať.

V súvislosti so spolujazdou ste uznali, že práva spotrebiteľa vyplývajúce z práva Únie na ochranu spotrebiteľa sa nevzťahujú na váš vzťah s ostatnými členmi.

Ako vodič súhlasíte s tým, že nebudete požadovať príspevok na náklady, ktorý je vyšší, než sú skutočne vzniknuté náklady a ktorý môže vytvárať zisk, pričom je stanovené, že v rámci zdieľania nákladov budete ako vodič musieť znášať vlastnú časť nákladov na jazdu. Ste výhradne zodpovední za výpočet nákladov, ktoré vám vzniknú na jazdu a za kontrolu, že suma požadovaná od vašich cestujúcich neprekračuje skutočne vzniknuté náklady (s výnimkou vášho podielu na nákladoch).

BlaBlaCar si vyhradzuje právo pozastaviť váš účet v prípade, ak používate vozidlo riadené osobným vodičom, iné podnikateľské vozidlo, taxi alebo služobné vozidlo a vďaka tomu zarábate na platforme. Súhlasíte s tým, že BlaBlaCar poskytnete na základe jej jednoduchej žiadosti, kópiu osvedčenia o evidencii vozidla a/alebo iný doklad, ktorý potvrdzuje, že ste oprávnení používať toto vozidlo na platforme a že na tom nezarábate.

Podľa 9. sekcie týchto Obchodných podmienok si BlaBlaCar tiež vyhradzuje právo pozastaviť váš účet, obmedziť vám prístup k službám, alebo ukončiť tieto podmienky, v prípade aktivity na platforme, ktorá vzhľadom na povahu ponúkaných jázd, ich frekvenciu, počet prepravených cestujúcich a požadovaný príspevok na náklady, vám vytvára zisk, alebo z akéhokoľvek dôvodu, na základe ktorého sa BlaBlaCar domnieva, že na platforme vytvárate zisk.

**7\. Zrušenie**
----------------

### **7.1. Podmienky vrátenia peňazí v prípade zrušenia**

Zrušenie miesta na spolujazde zo strany vodiča alebo cestujúceho po potvrdení rezervácie podlieha nižšie uvedeným ustanoveniam: 

– V prípade zrušenia z dôvodu na strane vodiča sa cestujúcemu vráti servisný poplatok. To platí najmä v prípade, ak vodič:

* nepotvrdil žiadosť o rezerváciu v stanovenej lehote (ak si vodič takúto možnosť zvolil);
* zruší spolujazdu alebo sa nedostaví na miesto stretnutia 15 minút po dohodnutom čase;

\-V prípade zrušenia z dôvodu na strane cestujúceho si spoločnosť BlaBlaCar ponecháva servisné poplatky.

Spôsob vrátenia servisného poplatku sa určuje v závislosti od spôsobu, ktorý bol použitý na zaplatenie servisného poplatku, pričom lehota na vrátenie servisného poplatku závisí od termínov prevodov bankovými organizáciami.

Ak pred odchodom dôjde k zrušeniu vinou cestujúceho, mieso (-a) zrušené cestujúcim sa automaticky sprístupnia ostatným cestujúcim, ktorí si ich môžu on-line rezervovať, čo znamená, že sa riadia týmito podmienkami.

BlaBlaCar podľa vlastného uváženia na základe dostupných informácií oceňuje oprávnenosť žiadostí o vrátenie peňazí.

### **7.2.  Právo na odstúpenie od zmluvy**

Prijatím týchto obchodných podmienok výslovne súhlasíte s tým, že zmluva medzi vami a spoločnosťou BlaBlaCar spočívajúca v spojení s iným členom má byť uzavretá pred uplynutím lehoty na odstúpenie od zmluvy od potvrdenia rezervácie a výslovne sa vzdávate svojho práva na odstúpenie od zmluvy v súlade s dispozíciou. § 7 Zákona č. 102/2014 Z. z. o ochrane spotrebiteľa pri predaji tovaru alebo poskytovaní služieb na základe zmluvy uzavretej na diaľku alebo zmluvy uzavretej mimo prevádzkových priestorov predávajúceho.

**8\. Správanie používateľov platformy a členov**
-------------------------------------------------

### **8.1. Záväzok všetkých používateľov platformy**

Uznávate, že ste výhradne zodpovední za rešpektovanie všetkých zákonov, predpisov a povinnosti týkajúcich sa používania platformy.

Okrem toho sa pri použití platformy a pri jazdách zaväzujete:

(i) nepoužívať platformu na profesionálne, komerčné alebo zárobkové účely;

(ii) neposielať BlaBlaCar (najmä po vytvorení alebo aktualizácii vášho účtu) alebo ostatným členom akékoľvek nepravdivé, zavádzajúce, škodlivé alebo podvodné informácie;

(iii) v žiadnom prípade na platforme vyjadrovať a ani sa správať, alebo na ňu pridávať akýkoľvek obsah (vrátane správ) hanlivej, poškodzujúcej, obscénnej, pornografickej, vulgárnej, urážlivej, agresívnej, nemiestnej, nevyžiadanej, násilnej, výhražnej, obťažujúcej, rasistickej či xenofóbnej povahy, alebo so sexuálnymi konotáciami, s podnecovaním k násiliu, diskriminácii alebo nenávisti, k povzbudzovaniu k činnosti alebo používaniu zakázaných látok alebo všeobecnejšie nezákonný obsah, ktorý je v rozpore s platnými zákonmi, týmito Obchodnými podmienkami a cieľmi tejto platformy, ktorý by mohol porušovať práva BlaBlaCar alebo tretej osoby alebo je v rozpore s dobrými mravmi;

(iv) neporušovať práva a imidž BlaBlaCar, najmä jej práva duševného vlastníctva;

(v) neotvoriť si na platforme viac ako jeden účet a neotvoriť si účet na meno tretej osoby;

(vi) nesnažiť sa obchádzať on-line systém rezervácie platformy, najmä pokúšaním sa odoslať inému členovi vlastné kontaktné údaje, aby vykonal rezerváciu mimo platformy a neplatil servisné poplatky;

(vii) nekontaktovať iného člena, a to najmä prostredníctvom platformy, za iným účelom, aký vymedzujú podmienky pre zdieľanie jázd;

(viii) neprijímať alebo nevykonávať platbu mimo platformy;

(ix) podrobiť sa týmto obchodným podmienkam a akýmkoľvek iným podmienkam platformy, ktoré sa na vás vzťahujú.

### **8.2. Záväzky vodiča**

Ak platformu používate ako vodič, okrem tohto sa ďalej zaväzujete:

(i) dodržiavať všetky zákony a predpisy vzťahujúce sa na jazdu a vozidlo, najmä mať v čase jazdy zo zákona povinné platné zmluvné poistenie a byť držiteľom platného vodičského preukazu

(ii) overiť si, či sa vaše poistenie vzťahuje na zdieľanie jázd a či sú vaši cestujúci vo vašom vozidle považovaní za tretie osoby a sú preto počas celej jazdy, aj cezhraničnej, kryté poistením

(iii) neriskovať pri jazde, nepožívať alebo nepoužívať akýkoľvek výrobok, ktorý môže oslabiť vašu pozornosť a vaše schopnosti ostražitého a úplne bezpečného vedenia vozidla;

(iv) pridávať len inzeráty, ktoré zodpovedajú skutočne plánovaným jazdám;

(v) uskutočniť jazdu tak, ako je popísaná v inzeráte (najmä s ohľadom na použitie alebo nepoužitie diaľnice), ako aj rešpektovať časy a miesta dohodnuté s ostatnými členmi (najmä nástupné a výstupné miesta);

(vi) nebrať viac cestujúcich, ako je počet miest uvedený v inzeráte;

(vii) používať vozidlo, ktoré je v dobrom stave a je v súlade s platnými právnymi predpismi a zvyklosťami, najmä s platným osvedčením o technickej kontrole vozidla;

(viii) predložiť BlaBlaCar alebo akémukoľvek cestujúcemu, ktorý o to požiada, váš vodičský preukaz, osvedčenie o evidencii vozidla, potvrdenie o poistení, osvedčenie o technickej kontrole vozidla a akýkoľvek doklad preukazujúci vašu schopnosť používať na platforme vozidlo ako vodič;

(ix) v prípade omeškania alebo zmeny času alebo jazdy bezodkladne informovať vašich cestujúcich;

(x) v prípade cezhraničnej jazdy držať a uchovávať pre cestujúceho a pre akýkoľvek iný orgán, ktorý o to môže požiadať, akýkoľvek dokument preukazujúci vašu identitu a vaše právo na prekročeniu hranice;

(xi) počkať na cestujúcich na dohodnutom mieste stretnutia po dobu najmenej 15 minút po dohodnutom čase;

(xii) nepridávať na účely zdieľania jázd inzerát s vozidlom, ktoré vám nepatrí, alebo ktoré nie ste oprávnení používať;

(xiii) zabezpečiť, aby ste mohli byť vašimi cestujúcimi telefonicky kontaktovaní na čísle evidovanom vo vašom profile;

(xiv) nevytvárať prostredníctvom platformy žiadny zisk;

(xv) nemať žiadne kontraindikácie alebo zdravotnú nespôsobilosť na vedenie vozidla

(xvi) počas jazdy sa správať primerane a zodpovedne a v súlade s duchom spolujazdy;

(xvii) neodmietnuť akúkoľvek rezerváciu na základe rasy, farby pleti, etnického pôvodu, národnosti, náboženstva, sexuálnej orientácie, rodinného stavu, zdravotného postihnutia, fyzického vzhľadu, tehotenstva, osobitnej zraniteľnosti vzhľadom k ich ekonomickej situácii, mena, bydliska, zdravotného stavu, politického presvedčenia, veku;

(xviii) zabezpečiť, aby ste mohli byť vašimi cestujúcimi telefonicky kontaktovaní na čísle evidovanom vo vašom profile, a to aj na mieste vyzdvihnutia.

### **8.3. Záväzky cestujúcich**

Ak platformu používate ako cestujúci, zaväzujete sa:

(i) počas jazdy sa vhodne správať tak, aby ste nerušili koncentráciu alebo jazdu vodiča alebo pokoj a pohodu ostatných cestujúcich;

(ii) rešpektovať vozidlo vodiča a jeho čistotu;

(iii) v prípade meškania bezodkladne informovať vodiča;

(iv) zaplatiť vodičovi dohodnutý príspevok na náklady;

(v) počkať na vodiča na dohodnutom miesto stretnutia po dobu najmenej 15 minút po dohodnutom čase;

(vi) predložiť BlaBlaCar alebo akémukoľvek vodičovi, ktorý o to požiada, váš občiansky preukaz alebo akýkoľvek iný doklad preukazujúci vašu totožnosť;

(vii) neprevážať počas jazdy akýkoľvek predmet, tovar, látku alebo zviera, ktoré by mohlo brániť v jazde a rušiť koncentráciu vodiča, alebo ktorý má takú povahu, že jeho držanie alebo preprava je v rozpore s platnými právnymi predpismi;

(viii) v prípade cezhraničnej jazdy držať a uchovávať pre vodiča a pre akýkoľvek iný orgán, ktorý o to môže požiadať, akýkoľvek dokument preukazujúci vašu identitu a vaše právo na prekročeniu hranice;

(ix) zabezpečiť, aby ste mohli byť vaším vodičom telefonicky kontaktovaní na čísle evidovanom vo vašom profile, a to aj na mieste vyzdvihnutia.

Ak ste za tretie osoby vykonali rezerváciu jedného alebo viacerých miest, v súlade s ustanovením článku 4.2.3 vyššie, sa zaručujte za rešpektovanie ustanovení tohto článku a všeobecne týchto podmienok touto treťou osobou. BlaBlaCar si vyhradzuje právo pozastaviť váš účet, obmedziť vám prístup k službám, alebo ukončiť tieto podmienky, v prípade porušenia treťou osobou, v mene ktorej ste na základe týchto podmienok rezervovali miesto podľa 9. sekcie týchto Obchodných podmienok.

**8.4. Nahlasovanie nevhodného alebo nezákonného obsahu (mechanizmus upozornenia a opatrení)**

Podozrivý, nevhodný alebo nezákonný obsah člena alebo správu môžete nahlásiť, ako je popísané [tu](https://support.blablacar.com/s/article/Nahlasovanie-neleg%C3%A1lneho-obsahu-1729197120609?language=sk).  

Spoločnosť BlaBlaCar po riadnom oznámení v súlade s týmto oddielom alebo príslušnými orgánmi bezodkladne odstráni akýkoľvek nezákonný obsah člena, ak Členský obsah je zjavne nezákonný alebo v rozpore s platnými predpismi; alebo  
spoločnosť BlaBlaCar považuje takýto obsah za porušujúci tieto VOP.  

V takýchto prípadoch si spoločnosť BlaBlaCar vyhradzuje právo odstrániť riadne nahlásený členský obsah dostatočne podrobným a jasným spôsobom a/alebo okamžite pozastaviť nahlásené konto.  

Príslušný člen sa môže proti uvedeným rozhodnutiam spoločnosti BlaBlaCar odvolať, ako je opísané v časti 15.1 nižšie.

**9\. Obmedzenia súvisiace s používaním platformy, pozastavenie účtov, obmedzenie prístupu a ukončenie**
--------------------------------------------------------------------------------------------------------

Vaše zmluvné vzťahy s BlaBlaCar môžete bez nákladov a bez dôvodu ukončiť kedykoľvek. K tomu stačí prejsť na kartu vášho profilu „Zrušiť účet“.

V prípade (i) vášho porušenia týchto podmienok, vrátane, ale s obmedzením sa na vaše povinnosti ako člena uvedené v článkoch 6 a 8 vyššie, (ii) prekročenia limitu uvedeného v článku 4.3.3 vyššie, alebo (iii) ak má BlaBlaCar skutočný dôvod sa domnievať, že je nevyhnutná ochrana jej bezpečnosti a jej integrity, a tiež bezpečnosti a integrity členov alebo tretích osôb, alebo za účelom vyšetrovaní alebo predchádzania podvodom, si BlaBlaCar vyhradzuje právo:

(i) s okamžitou účinnosťou a bez predchádzajúceho upozornenia ukončiť podmienky, ktoré vás viažu s BlaBlaCar; a/alebo

(ii) zabrániť pridaniu alebo odstráneniu akéhokoľvek členského obsahu pridaného na platformu; a/alebo

(iii) obmedziť váš prístup a vaše používanie platformy; a/alebo

(iv) dočasne alebo trvalo pozastaviť váš účet.

Pozastavenie účtu môže tiež znamenať, že v príslušných prípadoch nebudete dostávať žiadne čakajúce výplaty.

V príslušných prípadoch vám BlaBlaCar tiež môže poslať upozornenie s pripomenutím povinnosti dodržiavať platné zákony a/alebo tieto Obchodné podmienky.

Ak je to nevyhnutné, budete upozornení na zavedenie takýchto opatrení, aby vám bolo umožnené potvrdiť váš záväzok dodržiavať tieto Obchodné podmienky a platné zákony, poskytnúť BlaBlaCar vysvetlenia alebo sa odvolať proti tomuto rozhodnutiu. BlaBlaCar po zohľadnení všetkých okolností každého prípadu a závažnosti porušenia na základe vlastného uváženia rozhodne, či zruší alebo nezruší zavedené opatrenia.

**10\. Osobné údaje**
---------------------

V súvislosti s používaním platformy bude BlaBlaCar zhromažďovať a spracovávať niektoré z vašich osobných údajov, ako je opísané v Zásadách [ochrany osobných údajov](https://blog.blablacar.sk/about-us/privacy-policy).

**11\. Duševné vlastníctvo**
----------------------------

### **11.1. Obsah zverejnený spoločnosťou BlaBlaCar**

Okrem obsahov poskytnutých svojimi členmi je BlaBlaCar jediným držiteľom všetkých práv duševného vlastníctva vzťahujúcich sa k tejto službe, platforme, ich obsahu (najmä texty, obrázky, vzory, logá, videá, zvuky, dáta, grafika) a softvéru a databázam, ktoré zabezpečujú ich prevádzku.

BlaBlaCar vám udeľuje nevýhradné, osobné a neprenosné právo na používanie platformy a služieb na osobné a súkromné použitie, na nekomerčnom základe a v súlade s cieľmi platformy a služieb.

Akékoľvek iné použitie alebo využitie platformy a služieb a ich obsahu bez predchádzajúceho písomného súhlasu BlaBlaCar je zakázané. Obzvlášť máte zakázané:

(i) reprodukovanie, úpravy, prispôsobenie, distribúciu, verejné reprezentovanie a šírenie platformy, služieb a obsahu, s výnimkou prípadov s výslovným povolením spoločnosťou BlaBlaCar;

(ii) dekompiláciu a reverzné inžinierstvo platformy alebo služieb, s výnimkami ustanovenými textami, ktoré sú v platnosti;

(iii) extrahovanie alebo pokus o extrahovanie (najmä pri použití robotov na dolovanie údajov alebo iného podobného nástroja na zber údajov) podstatnej časti údajov platformy.

### **11.2. Obsah, ktorý zverejníte na platforme**

S cieľom umožniť poskytovanie služieb a v súlade s účelom platformy udeľujete spoločnosti BlaBlaCar nevýhradnú licenciu na používanie obsahu a údajov, ktoré poskytujete v rámci vášho používania služieb, ktoré môžu zahŕňať vaše žiadosti o rezervácie, inzeráty a komentáre v nich, životopisy, fotky, hodnotenia a reakcie na hodnotenia (ďalej len ako váš „**Členský obsah**“) a správy. S cieľom umožniť BlaBlaCar distribuovať a zverejniť obsah platformy cez digitálne siete a v súlade s akýmkoľvek komunikačným protokolom (najmä internet a mobilná sieť), môžete BlaBlaCar udeliť povolenie pre celý svet a po celú dobu trvania vašich zmluvných vzťahov s BlaBlaCar, reprodukovať, reprezentovať, prispôsobovať a prekladať váš členský obsah takto:

(i) oprávňujete BlaBlaCar reprodukovať celý alebo časť vášho členského obsahu na akomkoľvek známom alebo doteraz neznámom digitálnom záznamovom médiu, a to najmä na akomkoľvek serveri, pevnom disku, na pamäťovej karte alebo akomkoľvek inom rovnocennom médiu, v akomkoľvek formáte a akýmkoľvek známym alebo doteraz neznámym spôsobom, a to v rozsahu potrebnom na akékoľvek ukladanie, zálohovanie, prenos alebo sťahovanie spojené s fungovaním platformy a poskytovaním služby;

(ii) oprávňujete BlaBlaCar upraviť a preložiť členský obsah a reprodukovať tieto úpravy na všetkých súčasných alebo budúcich digitálnych médiách, uvedených v bode (i), s cieľom poskytovania služieb, a to najmä v rôznych jazykoch. Toto právo zahŕňa najmä možnosť vykonávania zmien formátovania vášho členského obsahu, pokiaľ ide o vaše morálne právo, za účelom rešpektovania grafickej charty platformy a/alebo zaistenia jeho technickej zlučiteľnosti s ohľadom na jeho zverejňovanie prostredníctvom platformy.

Naďalej zodpovedáte za členský obsah a správy, ktoré nahráte na našu platformu, a máte k nim všetky práva.

**12\. Úloha BlaBlaCar**
------------------------

Platforma predstavuje on-line platformu, na ktorej môžu členovia vytvárať a pridávať inzeráty na jazdy za účelom spolujazdy. Tieto inzeráty si môžu prezrieť najmä ostatní členovia, aby si zistili podmienky jazdy a prípadne si priamo rezervovali miesto v danom vozidle s členom, ktorý pridal inzerát na platformu.

Používaním platformy a akceptovaním týchto podmienok uznávate, že BlaBlaCar nie je stranou akejkoľvek dohody uzavretej medzi vami a ostatnými členmi za účelom zdieľania nákladov súvisiacich s jazdou.

BlaBlaCar nemá žiadnu kontrolu nad správaním sa svojich členov a používateľov platformy. Nevlastní, nevyužíva, nedodáva alebo nespravuje vozidlá, ktoré sú predmetom inzerátu, a neponúka na platforme žiadne jazdy.

Uznávate a akceptujete fakt, že BlaBlaCar nekontroluje platnosť, pravdivosť alebo zákonnosť inzerátov, miest a ponúkaných jázd. V rámci svojej funkcie prostredníka zdieľania jázd BlaBlaCar neposkytuje žiadne prepravné služby a nepôsobí vo funkcii prepravcu; úloha BlaBlaCar je obmedzená na uľahčenie prístupu k platforme.

Členovia (vodiči alebo cestujúci) konajú v ich výlučnej a plnej zodpovednosti.

V rámci svojej funkcie prostredníka BlaBlaCar nemôže byť zodpovedná za efektívny výskyt jazdy, a to najmä z dôvodu:

(i) chybných informácií týkajúcich sa jazdy a jej podmienok oznámených vodičom v jeho inzeráte alebo akýmkoľvek iným spôsobom;

(ii) zrušenia alebo zmeny jazdy členom;

(iii) nezaplatenia príspevku na náklady cestujúcim;

(iv) správania sa jej členov počas, pred alebo po jazde.

**13\. Fungovanie, dostupnosť a funkčnosť platformy**
-----------------------------------------------------

BlaBlaCar sa maximálnym možným spôsobom snaží zachovať platformu prístupnú 7 dní v týždni a 24 hodín denne. Napriek tomu môže byť prístup k platforme bez predchádzajúceho upozornenia dočasne pozastavený z dôvodu technickej údržby, migrácie, aktualizácie, v dôsledku výpadku alebo obmedzenia spojeného s prevádzkou siete.

Spoločnosť BlaBlaCar si navyše vyhradzuje právo zmeniť platformu alebo jej časti pre niektorých používateľov s cieľom otestovať nové funkcie a ponúknuť lepšiu používateľskú skúsenosť, ako aj zmeniť alebo pozastaviť celý alebo čiastočný prístup k platforme alebo jej funkciám podľa vlastného uváženia, dočasne alebo natrvalo.

**14\. Úprava podmienok**
-------------------------

Tieto podmienky a dokumenty integrované odkazom vyjadrujú úplnú dohodu medzi vami a BlaBlaCar ohľadom užívania služieb. Akýkoľvek iný dokument, najmä akákoľvek zmienka o platforme (často kladené otázky, atď.), slúžia len na účely všeobecných zásad.

BlaBlaCar môže tieto podmienky upraviť za účelom prispôsobenia sa svojmu technologickému a komerčnému prostrediu a zosúladenia s platnými právnymi predpismi. Akákoľvek zmena týchto podmienok bude zverejnené na platforme s uvedením dňa nadobudnutia účinnosti, a pred začiatkom ich platnosti budete upozornení spoločnosťou BlaBlaCar.

**15\. Rozhodné právo – Spor**
------------------------------

Tieto podmienky sú písané v slovenčine a podliehajú slovenskému právu.

**15.1 Interný systém riešenia sťažností**

Môžete sa odvolať proti rozhodnutiam, ktoré môžeme prijať v súvislosti s:

* Členským obsahom: napríklad sme odstránili, obmedzili viditeľnosť alebo odmietli odstrániť ľubovoľný členský obsah, ktorý poskytnete pri používaní platformy, alebo
* Vaším účtom: pozastavili sme váš prístup k platforme tam, kde sme prijali takéto rozhodnutia na základe faktu, že členský obsah predstavuje nezákonný obsah alebo je nekompatibilný s týmito Obchodnými podmienkami. Proces odvolania je [opísaný tu](https://support.blablacar.com/s/article/Ako-sa-odvola%C5%A5-vo%C4%8Di-odstr%C3%A1neniu-obsahu-alebo-pozastaveniu-%C3%BA%C4%8Dtu-1729197123042?language=sk).

**15.2 Mimosúdne riešenie sporov**

V prípade potreby môžete tiež predložiť vaše sťažnosti týkajúce sa našej platformy alebo našich služieb na platforme pre riešenie sporov, umiestnenej on-line Európskou komisiou, ktorá je prístupná [tu](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage). Európska komisia odošle vašu sťažnosť príslušnému národnému ombudsmanovi. V súlade s pravidlami platnými pre mediáciu ste za účelom dosiahnutia priateľského vyriešenie pred akoukoľvek žiadosťou o mediáciu povinní písomne upovedomiť BlaBlaCar o akomkoľvek spore.

Môžete tiež podať žiadosť orgánu pre riešenie sporov vo svojej krajine (zoznam nájdete [tu](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)). 

**16\. Právne oznámenia**
-------------------------

Platforma je publikovaná spoločnosťou Comuto SA, ktorá je spoločnosťou s ručením obmedzeným so základným imaním 155,880.999 EUR, je zapísaná do obchodného registra v Paríži pod číslom 491.904.546 (identifikačné číslo pre DPH v rámci Spoločenstva: FR76491904546), má sídlo na adrese 84, avenue de la République, 75011 Paris (Francúzsko) a je zastúpená svojím generánym riaditeľom Nicolasom Brussonom, zodpovedným zástupcom webovej platformy.

Internetová stránka je hostovaná na serveroch Google Cloud v Holandsku.

Spoločnosť Comuto SA je zapísaná v registri prevádzkovateľov cestovných a pobytových služieb pod číslom: IM075180037.

Finančnú záruku poskytuje: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Paríž – Francúzsko.

Poistenie zodpovednosti za škodu spôsobenú pri výkone povolania je uzatvorené: Spoločnosť Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paríž – Francúzsko.

Spoločnosť Comuto SA je zapísaná v registri sprostredkovateľov poistenia, bankovníctva a financií pod registračným číslom (Orias) 15003890.

V prípade akýchkoľvek otázok sa môžete obrátiť na Comuto SA prostredníctvom [tohto kontaktného formulára](https://support.blablacar.com/s/contactsupport?language=sk).

**17\. Digital Services Act (Akt o digitálnych službách)**
----------------------------------------------------------

**17.1.  Informácie o priemernom počte mesačných aktívnych príjemcov služby v Únii**

V súlade s článkom 24(2) nariadenia Európskeho parlamentu a Rady o internom trhu pre digitálne služby a pozmeňujúcej smernice 2000/31/ES („DSA“) sú poskytovatelia online platforiem povinní zverejňovať informácie o mesačnom priemere aktívnych príjemcov svojej služby v Únii, ktorý sa počíta ako priemer za posledných šesť mesiacov. Účelom tohto zverejňovania je zistiť, či poskytovateľ online platformy spĺňa kritériá „veľmi veľkých online platforiem“ podľa DSA, t. j. či prekračuje hranicu 45 miliónov priemerne mesačne aktívnych príjemcov v Únii.

K 17. augusta bol priemerný mesačný počet aktívnych príjemcov BlaBlaCar za obdobie medzi februárom a júlom 2024 vypočítaný pri zohľadnení úvodnej časti 77 a článku 3 DSA (započítavanie používateľov vystavených obsahu platformy iba raz počas obdobia 6 mesiacov, pred prijatím konkrétneho delegovaného zákona, približne 3,28 milióna v EÚ.

Tieto informácie sú zverejnené iba na účely splnenia požiadaviek DSA a nemalo by sa na ne spoliehať na iné účely. Budú aktualizované aspoň raz za šesť mesiacov. Náš prístup k vytvoreniu tohto výpočtu sa môže vyvíjať alebo si môže v priebehu času vyžadovať úpravy, napríklad kvôli zmenám produktov alebo novým technológiám.

**17.2. Kontaktné miesto pre úrady**

V súlade s článkom 11 DSA platí, že ak ste členom kompetentných úradov EÚ, Komisie EÚ alebo Európskej rady pre digitálne služby, môžete nás kontaktovať ohľadom záležitostí týkajúcich sa DSA e-mailom na [\[email protected\]](https://blog.blablacar.sk/cdn-cgi/l/email-protection).

Môžete nás kontaktovať v angličtine a francúzštine.

Upozorňujeme, že táto e-mailová adresa sa nepoužíva na komunikáciu s členmi. V prípade akýchkoľvek otázok týkajúcich sa použitia BlaBlaCar nás vy ako člen môžete kontaktovať prostredníctvom tohto [kontaktného formulára](https://support.blablacar.com/s/contactsupport?language=sk).