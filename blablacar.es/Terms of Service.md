**Versión vigente a partir del 28 de agosto de 2024:**

**1\. Descripción**

Comuto SA (de aquí en adelante denominada “BlaBlaCar”) ha desarrollado una plataforma de trayectos compartidos a la que se puede acceder desde el sitio web [www.blablacar.es](http://www.blablacar.es/) o desde una aplicación móvil, diseñada (i) para poner en contacto a los conductores que viajan a un determinado destino con pasajeros que se dirigen al mismo lugar, de forma que puedan compartir el Trayecto así como los costes asociados al mismo; (ii) para reservar billetes para trayectos en autobús operados por Operadores de autobuses; y (iii) para buscar y comprar en BlaBlaCar Billetes de tren de alguno de los operadores de transporte (las “Empresas ferroviarias”) que ofrecemos (en adelante denominada la “Plataforma”).

Las presentes condiciones generales de uso tienen por objeto regular el acceso y los términos de uso de la Plataforma. Se ruega leer atentamente estas condiciones. Entiendes y reconoces que BlaBlaCar no es parte en ningún contrato, acuerdo o relación contractual, de ninguna clase, suscrito entre los Usuarios de la Plataforma entre el Operador de autobuses o Empresa ferroviaria y tú.

Al hacer clic en “Iniciar sesión con Facebook”, “Registrarse con una dirección de correo electrónico” o “Pagar”, reconoces que has leído y aceptado las presentes condiciones generales de uso.

Si inicias sesión con tu Cuenta en la plataforma de BlaBlaCar de otro país (por ejemplo, [www.blablacar.com.br](http://www.blablacar.com.br/)), por favor, ten en cuenta que se aplicarán (i) las condiciones generales de uso de esa plataforma, (ii) la política de privacidad de dicha plataforma y (iii) las normativas y reglamentos legales de dicho país. Asimismo, la información de tu Cuenta, datos personales incluidos, se tendrá que transferir a la entidad legal que opere la otra plataforma. Ten en cuenta que BlaBlaCar se reserva el derecho de restringir el acceso a la Plataforma a los usuarios razonablemente identificados como ubicados fuera del territorio de la Unión Europea.

**2\. Definiciones**

En este documento,

“Anuncio” significa un anuncio relacionado con un Trayecto publicado por un usuario Conductor en la Plataforma o un Anuncio de tren;

“Anuncio de coche compartido” significa un anuncio relativo a un Desplazamiento publicado en la Plataforma por un Conductor; 

“Anuncio de autobús” se refiere a un anuncio relativo a un Viaje en autobús de un Operador de autobús publicado en la Plataforma;

“Anuncio de tren” se refiere a un anuncio relativo a un Trayecto en tren operado por una Empresa ferroviaria y ofrecido en la Plataforma;

“BlaBlaCar” tiene el significado que se le atribuye en el Artículo 1 arriba;

“Billete” significa el título nominativo de transporte válido dado al Consumidor después de la Reserva de un Viaje en autobús o en tren como prueba del contrato de transporte existente entre pasajeros y el Operador de autobuses o la Empresa ferroviaria regido por los Términos y condiciones generales de venta, sin perjuicio de cualquier condición particular estipulada adicionalmente entre el Pasajero y el Operador de autobuses o la Empresa ferroviaria, y referida en el Billete;

“CGU” significa las presentes Condiciones Generales de Uso;

“CGV” hace referencia a los [Términos y condiciones generales de venta](https://blog.blablacar.es/blablalife/viaje-en-carretera/condiciones-generales-de-venta-de-transporte) del Operador de autobuses en cuestión, en función del Viaje en autobús seleccionado por el cliente, y a los términos y condiciones especiales disponibles en el Sitio web y que hayan sido reconocidos como leídos y aceptados por el Cliente antes de realizar el pedido;

“CGV de tren” se refiere a los Términos y [condiciones generales de transportes de la Empresa ferroviaria](https://blog.blablacar.es/condiciones-generales-de-venta-de-la-empresa-ferroviaria) en cuestión (en adelante, “CGV de tren”), en función del Viaje en tren seleccionado por el cliente y a los términos y condiciones que hayan sido reconocidos y leídos por el Cliente antes de realizar el pedido y que dependen de la Empresa ferroviaria que opere el Trayecto;

“Cliente” significa cualquier persona física (Usuario o no) que compre, ya sea para sí mismo o en nombre de otra persona que vaya a ser el Pasajero, un Billete de autobús o un Billete de tren a través de la Plataforma para realizar un Viaje realizado por el Operador de autobuses o la Empresa ferroviaria;

“Cuenta” significa la cuenta que debe crearse para ser Usuario y poder acceder a determinados servicios ofrecidos por la Plataforma;

“Cuenta de Facebook” tiene el significado que se le atribuye en el Artículo 3.2 a continuación;

“Conductor” significa el Usuario que utiliza la Plataforma para ofrecer la compartición de un Trayecto a otra persona a cambio de compartir los costes asociados al trayecto, y transportar a dicha persona en un Trayecto determinado en la fecha y hora en las que el Conductor ha organizado el Trayecto con anterioridad;

“Confirmación de la reserva” tiene el significado que se le atribuye en el Artículo 4.2.1 a continuación;

“Contenido desarrollado por el Usuario” tiene el significado que se le atribuye en el Artículo 11.2 a continuación;

“Empresa ferroviaria” se refiere a una empresa profesional de transporte de pasajeros cuyos Trayectos en tren se ofrecen en la Plataforma;

“Gastos compartidos” significa, para un Trayecto de coche compartido determinado, la suma de dinero solicitada por el Conductor y aceptada por el Pasajero por su contribución a los gastos del viaje;

“Gastos de Gestión” tiene el significado que se le atribuye en el Artículo 5.2 a continuación;

“Operador de autobús” se refiere a una empresa dedicada al transporte profesional de viajeros, y cuyos billetes de viaje son distribuidos en la Plataforma por BlaBlaCar;

“Pasajero” significa (i) el Usuario que ha aceptado compartir un Trayecto con el Conductor, o, si procede, la persona en cuyo nombre ha reservado una Plaza un Usuario o (ii) el Cliente que ha adquirido un Billete de un Operador de autobuses o Empresa ferroviaria o, si procede, la persona en cuyo nombre el Cliente ha adquirido un Billete de un Operador de autobuses o Empresa ferroviaria;

“Pedido” se refiere a la operación por la que el Cliente reserva sus Servicios con BlaBlaCar Bus, cualquiera que sea el medio utilizado, con la única excepción de la compra de Billetes directamente realizadas en un Punto de venta, y que implica la obligación para el Pasajero o posiblemente un Agente de pagar el precio relativo a los Servicios pertinentes;

“Plaza” significa el asiento reservado por un Pasajero en el vehículo del Conductor, o en el vehículo llevado a cabo por el Operador del autobús o a bordo de un tren;

“Plataforma” tiene el significado que se le atribuye en el Artículo 1 arriba;

“Precio” se refiere, para un Trayecto en autobús o un Trayecto en tren determinado, al precio que incluye todos los impuestos, tasas y costes de los servicios pertinentes incluidos, pagados por el Cliente en la Plataforma, en el momento de la validación del Pedido, para un Asiento en un Trayecto en autobús o en un Trayecto en tren determinado;

“Punto de venta” hace referencia a los mostradores y terminales físicos que aparecen en el Sitio web y en los que se ofrecen los Billetes a la venta;

“Servicios” significa todos los servicios ofrecidos por BlaBlaCar por medio de la Plataforma;

“Servicios de transporte” se refiere a los servicios de transporte suscritos por un Pasajero de Viaje en Autobús o Tren y proporcionados por el Operador de autobuses o la Empresa ferroviaria;

“Sitio web” significa el sitio web al que se puede acceder desde la dirección [www.blablacar.es](http://www.blablacar.es/);

“Tramo” tiene el significado que se le atribuye en el Artículo 4.1 a continuación;

“Trayecto” indistintamente se refiere a un Trayecto en autobús, a un Trayecto en tren o a un Trayecto en coche compartido;

“Trayecto en coche compartido” significa el recorrido sujeto de un Anuncio de coche compartido publicado por un Conductor en la Plataforma por el cual el Conductor acepta compartir un Trayecto con Pasajeros, Usuarios de la plataforma, a cambio de la compartición de los gastos asociados al Trayecto;

“Trayecto en autobús” significa el viaje sujeto de un Anuncio de autobús en la Plataforma y para el cual el Operador de autobuses propone asientos en los vehículos de autobús a cambio del Precio;

“Trayecto en tren” significa el viaje sujeto a un Anuncio de tren en la Plataforma y para el cual la Empresa ferroviaria proporciona asientos a bordo del tren a cambio del Precio.

“Usuario” significa cualquier persona que haya creado una Cuenta en la Plataforma.

“Solución de Pago Hyperwallet” tiene el significado que se le otorga en el artículo 5.4.2.a siguiente;

“Proveedor de la Solución de Pago Hyperwallet” se refiere a PayPal (Europe) S.à r.l. et Cie, S.C.A.

**3\. Registro en la Plataforma y creación de una Cuenta**

**3.1.    Condiciones de registro en la Plataforma**

La Plataforma solamente puede ser utilizada por personas mayores de 18 años. Está estrictamente prohibido el registro en la plataforma por parte de un menor. Al acceder, utilizar o registrarse en la Plataforma, confirmas y garantizas que tienes más de 18 años.

**3.2.    Creación de una Cuenta**

La Plataforma permite a los Usuarios publicar y ver Anuncios de coche compartido e interactuar entre ellos para reservar una Plaza. Es posible ver los Anuncios sin estar registrado en la Plataforma. No obstante, no podrás publicar un Anuncio de coche compartido ni reservar una Plaza sin haber creado primero una Cuenta para convertirte en Usuario.

Para crear tu Cuenta puedes utilizar cualquiera de los dos métodos utilizados a continuación:

(i)    rellenar todos los campos obligatorios en el formulario de registro;

(ii)    o iniciar sesión en tu cuenta de Facebook a través de nuestra Plataforma (en adelante denominada como su “Cuenta de Facebook”). Al utilizar dicha funcionalidad, entiendes que BlaBlaCar tendrá acceso a publicar en la Plataforma y conservar determinada información de tu Cuenta de Facebook. Puedes eliminar el enlace entre tu Cuenta y tu Cuenta de Facebook en cualquier momento en la sección “Certificaciones” de tu perfil. Si deseas obtener más información sobre la utilización de tus datos obtenidos de tu Cuenta de Facebook, puedes leer nuestra [Política de privacidad](https://www.blablacar.es/about-us/privacy-policy) y la Política de privacidad de Facebook.

Para registrarte en la Plataforma es necesario que hayas leído y aceptado las presentes CGU.

Al crear tu Cuenta, independientemente del método seleccionado, te comprometes a proporcionar información verídica y precisa, y a actualizar dicha información por medio de tu perfil o enviando una notificación a BlaBlaCar, con el objetivo de garantizar su relevancia y precisión durante la duración total de tu relación contractual con BlaBlaCar.

En caso de que te registres utilizando un correo electrónico, te comprometes a guardar en secreto la contraseña seleccionada durante la creación de tu Cuenta y a no comunicársela a ninguna otra persona. En caso de pérdida o divulgación de tu contraseña, deberás comunicárselo a BlaBlaCar inmediatamente. Tú eres el único responsable del uso de tu Cuenta por parte de terceras partes, salvo que hayas comunicado de forma expresa a BlaBlaCar la pérdida, el uso fraudulento por parte de un tercero, o la revelación de tu contraseña a un tercero.

Recuerda no crear ni utilizar, bajo tu propia identidad o bajo la identidad de un tercero, ninguna Cuenta adicional a la creada inicialmente.

**3.3.    Verificación**

BlaBlaCar podrá, con fines de transparencia, de mejora de la veracidad o de prevención o detección de fraude, establecer un sistema para verificar parte de la información proporcionada en tu perfil. Se trata, fundamentalmente, de aquellos casos en los que introduces tu número de teléfono o nos proporcionas un Documento de Identidad.

Reconoces y aceptas que cualquier referencia en la Plataforma o en los Servicios a la información “certificada”, o a cualquier otro término similar, significa solamente que un Usuario ha superado con éxito el procedimiento de verificación existente en la Plataforma o en los Servicios para proporcionarle más información sobre el Usuario con el que está pensando compartir un Trayecto. BlaBlaCar no puede garantizar la veracidad, fiabilidad o validez de la información sujeta al procedimiento de verificación.

**4\. Utilización de los Servicios**

**4.1.    Publicación de Anuncios**

Como Usuario, siempre y cuando cumplas las condiciones establecidas a continuación, podrás crear y publicar Anuncios de coche compartido en la Plataforma introduciendo información sobre el Trayecto de coche compartido que vas a realizar (fechas/horas/puntos de recogida y llegada, número de plazas ofrecidas, opciones disponibles, cantidad de Costes compartidos, etc.).

Al publicar su Anuncio de coche compartido, puede indicar las ciudades importantes en las que acepta detenerse, recoger o dejar a los pasajeros. Las secciones del Trayecto en coche compartido entre estas ciudades importantes o entre una de estas ciudades importantes y el punto de recogida o destino del Trayecto en coche compartido constituyen “Tramos”.

Solamente podrás publicar un Anuncio de coche compartido si cumples todas y cada una de las condiciones indicadas a continuación:

(i)    posees un carnet de conducir válido;

(ii)    publicas solamente Anuncios de coche compartido con vehículos de tu propiedad o utilizados con consentimiento expreso del propietario, y siempre y cuando estés autorizado para utilizar el vehículo con fines de desplazamiento compartido;

(iii)    eres y seguirás siendo el conductor principal del vehículo sujeto del Anuncio de coche compartido;

(iv)    el vehículo tiene un seguro a terceros en vigor;

(v)    no tienes ninguna contraindicación ni incapacidad médica para conducir;

(vi)    el vehículo que vas a utilizar para el Trayecto es un turismo de 4 ruedas y un máximo de 7 asientos, excluyendo los conocidos como vehículos “sin carnet”.

(vii)    no tienes intención de publicar otro anuncio para el mismo Trayecto de coche compartido en la Plataforma;

(viii)    no ofreces más Plazas de las disponibles en tu vehículo y de las permitidas en la Plataforma;

(ix)    todos las Plazas ofertadas tienen cinturón de seguridad, incluso en el caso de que el vehículo esté homologado con asientos sin cinturón de seguridad;

(x)    utilizas un vehículo en buen estado y que cumple con todas las normativas legales, y que el vehículo dispone del Certificado de ITV actualizado;

(xi) eres un consumidor y no actúas como profesional.

Reconoces que tú eres el único responsable del contenido del Anuncio de coche compartido que publicas en la Plataforma. Por lo tanto, reconoces y garantizas la veracidad y precisión de toda la información incluida en tu Anuncio de coche compartido, y te comprometes a realizar el Trayecto de coche compartido bajo las condiciones descritas en tu Anuncio de coche compartido.

Tu Anuncio de coche compartido será publicado en la Plataforma y por lo tanto podrá ser visto por todos los Usuarios y visitantes, incluso por las personas que no son Usuarios de la Plataforma, que realicen una búsqueda en la Plataforma o en el sitio web de las empresas asociadas a BlaBlaCar. BlaBlaCar se reserva el derecho, a su exclusivo criterio, a no publicar o a borrar, en cualquier momento, cualquier Anuncio de coche compartido que no cumpla con los presentes CGU o que considere que puede dañar su imagen, la imagen de la Plataforma o la imagen de los Servicios, y/o suspender la Cuenta del Miembro que publique dichos Anuncios de coche compartido con arreglo a la Sección 9 de las presentes CGU.

Se te informa de que en caso de que te presentes como consumidor utilizando la Plataforma cuando de hecho actúas como profesional, estás expuesto a las sanciones previstas por la ley aplicable.

**4.2.    Reservar una Plaza** 

BlaBlaCar ha establecido un sistema para la reserva de Plazas en línea (“Reserva”) para algunos de los Trayectos ofrecidos en la Plataforma.

Los métodos de reserva de un asiento dependen de la naturaleza del trayecto previsto.

BlaBlaCar proporciona a los usuarios de la Plataforma un motor de búsqueda basado en diferentes criterios de búsqueda (lugar de origen, destino, fechas, número de Trayectos, etc.). Algunas funcionalidades adicionales se proporcionan en el motor de búsqueda cuando el usuario está conectado con su cuenta. BlaBlaCar invita al usuario, independientemente del proceso de reserva utilizado, a consultar y utilizar cuidadosamente el buscador para determinar la oferta más adaptada a sus necesidades. Puedes encontrar más información [aquí](https://blog.blablacar.es/about-us/transparencia-de-las-plataformas). El Cliente, al reservar un Trayecto en autobús en un Punto de venta, también puede pedir al Operador de autobuses o al agente de recepción que realice la búsqueda.

**4.2.1 Trayecto en coche compartido**

Cuando un Pasajero esté interesado en un Anuncio con sistema de reserva, puede realizar una solicitud de reserva en línea. Esta solicitud de reserva puede ser (i) aceptada automáticamente (en caso de que el Conductor haya seleccionado esta opción al publicar el Anuncio), o (ii) aceptada por el Conductor de forma manual. En el momento de la reserva, el Pasajero realiza el pago en línea por la cantidad de Costes compartidos y los Gastos de Gestión relacionadas, si procede. Una vez recibido el pago por parte de BlaBlaCar y tras la validación de la reserva por parte del Conductor, si procede, el Pasajero recibe una confirmación de reserva (la “Confirmación de reserva”).

Si eres un Conductor y has seleccionado gestionar de forma manual las solicitudes de Reserva al publicar tu Anuncio de coche compartido, deberás responder a las solicitudes de Reserva dentro del tiempo especificado por el Pasajero durante la solicitud de Reserva. De no hacerlo, la solicitud de Reserva caducará de forma automática y el Pasajero recibirá el reembolso de la cantidad que haya pagado en el momento de realizar la solicitud de reserva, si procede.

En el momento de la Confirmación de reserva, BlaBlaCar te enviará el número de teléfono del Conductor (si eres el Pasajero) o del Pasajero (si eres el Conductor) si el Usuario aceptó mostrar su número de teléfono. A partir de este momento tú eres el único responsable del cumplimiento del contrato que te vincula con el otro Usuario.

**4.2.2. Trayecto en autobús** 

Para Trayectos en autobús, BlaBlaCar permite la reserva de Billetes de autobús para un Trayecto en autobús determinado a través de la Plataforma. 

Los Servicios de transporte se rigen por los Términos y condiciones generales de venta del Operador de autobuses correspondiente, dependiendo del Trayecto seleccionado por el Cliente, el cual debe ser aceptado por el Cliente antes de realizar el pedido. BlaBlaCar no proporciona ningún servicio de transporte con respecto a los Trayectos en autobús, los Operadores de autobuses son solamente una parte en los Términos y condiciones generales de la venta. Aceptas que la reserva de Asientos para un Trayecto en autobús determinado está sujeta a los Términos y condiciones generales de venta, el Operador de autobuses correspondiente. 

BlaBlaCar quiere que prestes atención al hecho de que ciertos Servicios de transporte ofrecidos por el Operador de autobuses y mencionados en el Sitio web pueden retirarse, en especial por razones climáticas, en temporada alta o en caso de fuerza mayor.

**4.2.3. Trayecto en tren**

Para Trayectos en tren, BlaBlaCar permite la reserva de billetes para un Trayecto en tren determinado a través de la Plataforma.

Los Servicios de transporte se rigen por las CGV de tren correspondientes en función del Trayecto seleccionado por el Cliente, las cuales deberán ser aceptadas por este antes de realizar el Pedido. BlaBlaCar no presta ningún servicio de transporte con respecto a los Trayectos en tren, siendo las Empresas ferroviarias las únicas partes de las CGV de tren. Aceptas que la reserva de Asientos para un Trayecto en tren determinado está sujeta a las CGV de tren de la Empresa ferroviaria correspondiente.

BlaBlaCar recomienda prestar atención al hecho de que ciertos Servicios de transporte ofrecidos por las Empresas ferroviarias y mencionados en el Sitio web pueden retirarse, en especial por razones climáticas, en temporada alta o en caso de fuerza mayor.

El Cliente recibirá un Billete una vez validado el Pedido.

**4.2.4 Derecho e información de los Pasajeros de tren**

**4.2.4.1 General**

En virtud del Reglamento (UE) 2021/782, los Pasajeros ferroviarios tienen amplios derechos y obligaciones, los cuales se pueden consultar en la siguiente página web: [https://eur-lex.europa.eu/legal-content/ES/TXT/HTML/?uri=CELEX:32021R0782](https://eur-lex.europa.eu/legal-content/ES/TXT/HTML/?uri=CELEX:32021R0782)

La información indicada anteriormente, en el presente Artículo 4.2.3.4, se facilita en los enlaces que figuran a continuación y/o en la página que figura a continuación: [https://support.blablacar.com/hc/es](https://support.blablacar.com/hc/es), entendiéndose que en caso de discrepancia entre la información de las Empresas ferroviarias y este enlace, prevalecerá la información proporcionada por las Empresas ferroviarias.

**4.2.4.2 Información antes del viaje**

Para mantenerte al corriente de las condiciones y modalidades que se indican a continuación, puedes consultar las CGV de tren de las Empresas ferroviarias correspondientes.

* Condiciones de acceso con bicicletas;
* Alteraciones, anulaciones, correspondencias perdidas y retrasos;
* Disponibilidad de las instalaciones a bordo, incluida la conexión Wi-Fi y los aseos, y los servicios a bordo, incluida la asistencia que el personal ofrece a los pasajeros;
* Procedimientos de recuperación de equipaje perdido.

**4.2.4.3 Información específica para personas con discapacidad y para personas con movilidad reducida:**

**Punto de contacto para obtener asistencia:**

El Pasajero que necesite asistencia deberá comunicarlo, a través de los enlaces indicados a continuación, a la Empresa ferroviaria correspondiente al menos 24 horas antes de que se requiera dicha asistencia. 

La asistencia la prestan las Empresas ferroviarias con la condición de que el Pasajero se presente en el punto indicado a la hora fijada por la Empresa ferroviaria.

Si tienes algún problema para ponerte en contacto con la Empresa ferroviaria, puedes contactar con BlaBlaCar a través del siguiente [Formulario](https://support.blablacar.com/hc/es/requests/new?ticket_form_id=18313652317597) o en el [Centro de Ayuda.](https://support.blablacar.com/hc/es)

**Condiciones de accesibilidad:**

Las condiciones de accesibilidad de la estación y a bordo de las instalaciones asociadas se pueden consultar en la página web de las estaciones correspondientes o en el [Centro de Ayuda.](https://support.blablacar.com/hc/es)

Información durante el viaje:

El pasajero puede consultar los datos que se indican a continuación en las CGV de tren de la Empresa ferroviaria correspondiente o en el Billete:

* Servicios e instalaciones a bordo, incluida la conexión Wi-Fi
* La siguiente estación
* Alteraciones y retrasos (planificados y en tiempo real)
* Servicios de conexión principales

Problemas de seguridad

**4.2.5 Naturaleza designada de la reserva de Asiento y condiciones de uso de los Servicios en nombre de un tercero**

Cualquier utilización de los Servicios, tanto en calidad de Pasajero como de Conductor, está relacionada con un nombre específico. La identidad del Conductor y del Pasajero debe corresponderse con la identidad comunicada por los mismos a BlaBlaCar y a los demás Usuarios que participan en el Trayecto de uso compartido, el Operador de autobuses o la Empresa ferroviaria.

Sin embargo, BlaBlaCar permite a sus Usuarios reservar uno o más Asientos en nombre de terceros. En este caso, te comprometes a indicar con precisión al Conductor, en el momento de la Reserva, los nombres, la edad y el número de teléfono de la persona en cuyo nombre se reserva un asiento. Está estrictamente prohibido reservar un Asiento para un menor de 13 años que viaja solo en un Trayecto en coche compartido. En el caso de que reserves un asiento para un Trayecto en coche compartido para un menor mayor de 13 años que viaja solo, te comprometes a solicitar el acuerdo previo del Conductor y a proporcionarle el permiso debidamente completado y firmado de sus representantes legales. En lo que respecta a los Trayectos en autobús o a los Trayectos en tren, te recomendamos consultar las CGV del Operador de autobús o las CGV de tren de la Empresa ferroviaria correspondiente.

La Plataforma está pensada para reservar Plazas para personas. Está prohibido reservar una Plaza para transportar cualquier objeto, paquete, animal que viaje solo o cualquier otro elemento material.

Está igualmente prohibido publicar un Anuncio para cualquier otro Conductor que no seas tú.

**4.3.    Contenido desarollado por el Usuario, moderación y sistema de evaluación**

**4.3.1.    Funcionamiento del sistema de evaluación**

BlaBlaCar te anima a publicar tus comentarios y/o valoraciones sobre un Conductor (en el caso de que seas un Pasajero) o sobre un Pasajero (en el caso de que seas un Conductor) con el cual hayas compartido un Trayecto o con quien se supone que hubieses debido compartir un Trayecto. Sin embargo, no está permitido dejar ningún comentario sobre otro Pasajero si eres un Pasajero, ni sobre ningún otro Usuario con quien no hayas viajado o con quien no tenías previsto viajar.

Tu opinión, al igual que las opiniones que hayan publicado otros Usuarios sobre ti, si existieran, solamente se podrán ver y se publicarán en la Plataforma una vez transcurrido el más breve de los siguientes periodos: (i) inmediatamente después de que ambos hayáis dejado un comentario, o (ii) después de un período de 14 días tras la primera opinión.

Podrás responder a un comentario realizado en tu perfil por parte de otro Usuario. El comentario y, si procede, su respuesta, se publicarán en tu perfil.

**4.3.2.    Moderación**

**a. Contenido de los Miembros**

Reconoces y aceptas que BlaBlaCar podrá moderar antes de su publicación, mediante herramientas automatizadas o manualmente, el Contenido desarollado por el Usuario, tal y como se define en la sección 11.2. Si BlaBlaCar considera que dicho Contenido desarollado por el Usuario infringe las leyes aplicables o estas CGU, se reservará el derecho a:

* impedir que se publique o se elimine tal Contenido desarollado por el Usuario
* enviar una advertencia al Usuario recordándole la obligación de cumplir la legislación aplicable o las presentes CGU, y/o
* aplicar las medidas restrictivas de acuerdo con la sección 9 de estas CGU.

El uso por parte de BlaBlaCar de dichas herramientas automatizadas o de la moderación manual no se interpretará como un compromiso de supervisar o una obligación de buscar activamente actividades ilegales o Contenido desarollado por el Usuario publicado en la Plataforma y, en la medida en que lo permita la legislación aplicable, no dará lugar a responsabilidad alguna para BlaBlaCar.

**4.3.3.    Límite**

Con arreglo a la sección 9 de las presentes CGU, BlaBlaCar se reserva el derecho a suspender tu Cuenta, limitar su acceso a los Servicios, o rescindir los presentes CGU en caso de que (i) hayas recibido al menos tres valoraciones negativas y (ii) una de las opiniones promedias que hayas recibido tengan una puntuación de 3 o inferior, en función de la gravedad de los comentarios dejados por un Miembro en la opinión.

**5\. Condiciones financieras**

El acceso y el registro en la Plataforma, así como la búsqueda, visualización y publicación de Anuncios, no implican coste alguno. No obstante, la reserva se realizará tras el pago correspondiente de una tarifa, bajo las condiciones descritas a continuación.

**5.1.    Costes compartidos y Precio**

5.1.1 En el caso de un Viaje de coche compartido, los Costes compartidos los determinas tú, como Conductor, bajo tu única responsabilidad. Está estrictamente prohibido obtener ningún tipo de beneficio mediante el uso de nuestra Plataforma. Por lo tanto, estás de acuerdo en limitar la Cantidad de costes compartidos solicitada a tus Pasajeros a los costes reales del Trayecto de coche compartido que vas a compartir con los Pasajeros. De lo contrario, asumirás los riesgos de recalificación de la transacción realizada a través de la Plataforma.

Cuando publiques un Anuncio de coche compartido, BlaBlaCar te sugerirá una Cantidad de Costes compartidos, que tiene en cuenta principalmente la naturaleza del Trayecto y la distancia recorrida. Esta cantidad se indica solamente a modo de guía y puedes aumentarla o reducirla teniendo en cuenta los costes reales del Trayecto de coche de compartido. Para evitar el uso abusivo de la Plataforma, BlaBlaCar limita la posibilidad de ajuste de los Costes compartidos.

5.1.2 Con respecto al Trayecto en autobús o al Trayecto en tren, el Precio por Asiento se define a discreción del Operador de autobuses o de la Empresa ferroviaria. Se invita al Cliente a consultar los Términos y condiciones generales de venta o las CGV de tren pertinentes para comprender las modalidades aplicables con respecto a la realización de la orden de emisión de Billetes y los métodos de pago. 

En cuanto a las condiciones de pago del Billete de Tren, están disponibles en la Plataforma en el momento de realizar el Pedido.

**5.2.    Gastos de Gestión**

BlaBlaCar, a cambio del uso de la Plataforma, en el momento de la Reserva, cobrará los gastos de gestión (en adelante, los “Gastos de Gestión”). Se informará al usuario antes de cualquier aplicación los Gastos de Gestión, según corresponda.

Los métodos de cálculo de los Gastos de Gestión vigentes que pueden encontrarse [aquí](https://blog.blablacar.es/blablalife/lp/gastos-de-gestion) se proporcionan solo con fines informativos y no tienen valor contractual. BlaBlaCar se reserva el derecho de modificar en cualquier momento los métodos de cálculo de los Gastos de Gestión. Estos cambios no tendrán ningún efecto en los Gastos de Gestión aceptados por el Usuario antes de la fecha efectiva de dichos cambios.

En el caso de trayectos transfronterizos, tenga en cuenta que los métodos para el cálculo de los importes de los Gastos de Gestión y el IVA aplicable podrán variar dependiendo del punto de recogida y/o el destino del Trayecto.

Al utilizar la Plataforma para la realización de trayectos transfronterizos o al exterior de España, puede que los Gastos de Gestión los cobre la empresa filial de BlaBlaCar que opera la plataforma local.

**5.3.    Redondeo**

Reconoces y aceptas que BlaBlaCar puede, a su única discreción, redondear de forma ascendente o descendente a números enteros o decimales los Gastos de Gestión y la Cantidad de costes compartidos.

**5.4.    Métodos de pago y repago de la Cantidad de costes compartidos al Conductor o el Precio a los Operadores de autobuses o las Empresas ferroviarias**

**5.4.1.    Instrucción del conductor (mandato)**

Al utilizar la Plataforma como Conductor para Trayectos de coche compartido, confirmas que has leído y aceptado [los términos y condiciones](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) del Proveedor de la Solución de Pago Hyperwallet, nuestro proveedor de pagos que procesa las transferencias de las Contribuciones de Costes a los conductores a través de la Solución de Pago Hyperwallet definidos en el apartado 5.4.2.a. Estas condiciones generales de uso se refieren más adelante como “CGU de Hyperwallet”.

Ten en cuenta que las exclusiones de la Directiva (UE) 2015/2366 del Parlamento Europeo y del Consejo, de 25 de noviembre de 2015, sobre servicios de pago en el mercado interior enumeradas en el artículo 11.4, letras (i) y (ii), de las CGU de Hyperwallet no se aplican a los Miembros de la Plataforma como no profesionales.

Como Conductor, das instrucciones:

1. a BlaBlaCar para que proceda a ordenar al Proveedor de la Solución de Pago Hyperwallet que efectúe la transferencia de la Contribución de Costes, y 
2. al Proveedor de la Solución de Pago Hyperwallet para que realice la transferencia de la Contribución de Costes a tu cuenta bancaria o de PayPal en tu nombre y por tu cuenta, de acuerdo con las CGU de Hyperwallet.

En el contexto de un Trayecto de coche compartido y tras la aceptación automática o manual de la Reserva, la cantidad total pagada por el Pasajero (Gastos de Gestión y Cantidad de costes compartidos) se mantiene en una cuenta de depósito en garantía gestionada por el Proveedor de la Solución de Pago Hyperwallet.

**5.4.2.    Pago de la Cantidad de costes compartidos al Conductor**

Tras el Trayecto de coche compartido, los Pasajeros tendrán un período de 24 horas  para presentar una reclamación a BlaBlaCar sobre el Trayecto. En ausencia de reclamación de los Pasajeros durante este período, BlaBlaCar deberá considerar el Trayecto como confirmado.

A partir del momento de esta confirmación expresa o tácita, tendrás, como Conductor, un crédito a ser abonado en tu Cuenta. Este crédito se corresponde a la cantidad total pagada por el Pasajero en el momento de confirmar la Reserva tras haberle restado los Gastos de Gestión, es decir, la Cantidad de costes compartidos pagada por el Pasajero.

**a. Solución de Pago Hyperwallet**

La Solución de Pago Hyperwallet es un servicio de pago proporcionado por el Proveedor de la Solución de Pago Hyperwallet. Para evitar cualquier duda, BlaBlaCar no presta ningún servicio de procesamiento de pagos para los Miembros y no entra en posesión de los fondos de los Miembros.

Cuando el Pasajero confirma el Trayecto de coche compartido, tú, como Conductor, tienes la opción de recibir tu Contribución de Costes en tu cuenta bancaria o en tu cuenta de PayPal.

Para ello, puedes utilizar la Solución de Pago Hyperwallet suscribiendo un contrato directamente con el Proveedor de la Solución de Pago Hyperwallet y aceptando las CGU de Hyperwallet.

A los efectos de recibir la primera transferencia de la Contribución de Costes, se te pedirá que selecciones un método de pago, es decir, facilites los datos de tu cuenta bancaria o de PayPal, así como la dirección postal y fecha de nacimiento. Si no lo haces, o si la información que facilitas es incorrecta, inexacta o no está actualizada, no se transferirá la Contribución de Costes.

La transferencia será realizada por el Proveedor de la Solución de Pago Hyperwallet.

Al final del plazo de 5 años, se considerará que cualquier cantidad de la Contribución de Costes que no hayas reclamado como Conductor desde la fecha en la que apareció en tu Cuenta a través de la Solución de Pago Hyperwallet pertenece a BlaBlaCar. 

**b. Comprobaciones de KYC**

Al utilizar la Solución de Pago Hyperwallet, aceptas que puedes estar sujeto a procedimientos normativos aplicados por el Proveedor de la Solución de Pago de acuerdo con [sus condiciones generales de uso](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml).

Estos procedimientos podrán incluir comprobaciones de identidad y otros requisitos del procedimiento “Conozca a su Cliente” (KYC) basados en los criterios establecidos por el Proveedor de la Solución de Pago Hyperwallet. Estos criterios podrán incluir umbrales financieros dependiendo de la cantidad total de pagos de Contribución de Costes que realices. 

A efectos de comprobación de KYC, quizá se te pida que proporciones la información adicional que solicite el Proveedor de la Solución de Pago Hyperwallet. Si no proporcionas los documentos requeridos, no podrás recibir la Contribución de Costes hasta que se confirme tu identidad.

Es posible que se den situaciones en las que el Proveedor de la Solución de Pago Hyperwallet deba, de conformidad con las leyes aplicables, suspender la transferencia de la Contribución de Costes o el acceso a la Solución de Pago. BlaBlaCar no tiene ninguna influencia sobre dichas acciones del Proveedor de la Solución de Pago Hyperwallet.

Las comprobaciones de KYC llevadas a cabo por el Proveedor de la Solución de Pago Hyperwallet son distintas e independientes de las comprobaciones que BlaBlaCar pueda llevar a cabo en relación con el uso de la Plataforma de conformidad con estas Condiciones y la Política de privacidad.

**5.4.3 Pago del Precio al Operador de autobuses o del Billete de Tren a BlaBlaCar**

El pago de cualquier Pedido realizado a través de la Plataforma se realiza por uno de los medios autorizados a continuación. El Cliente tiene la opción de guardar la información relacionada con una o más tarjetas de crédito en su Cuenta de cliente para que no tenga que introducir sistemáticamente esta información al realizar pagos posteriores.

Los métodos de pago pueden variar dependiendo de la plataforma BlaBlaCar que utilices. Los métodos de pago autorizados son los siguientes: 

* Tarjeta de crédito bancaria (las marcas aceptadas se muestran en la plataforma. En el caso de que tengas una tarjeta de marca compartida, podrás seleccionar una marca específica en la fase de pago) 
* PayPal
* Vales
* Apple Pay
* Google Pay

No se emitirá confirmación de Pedido antes del pago completo y efectivo del precio de los Servicios de transporte seleccionados por el Cliente. Si el pago es irregular, incompleto o no se realiza por cualquier motivo atribuible al Cliente, el Pedido se cancelará inmediatamente.

El Pedido se confirma enviando un correo electrónico al Cliente con los detalles del Pedido.

Se invita al Pasajero a comprobar la configuración de la bandeja de entrada de su dirección de correo electrónico y, en especial, a asegurarse de que el correo electrónico de confirmación no se envía directamente al Correo no deseado.

La Confirmación del Pedido es definitiva. En consecuencia, cualquier cambio dará lugar a un intercambio o cancelación en las condiciones de los Términos y condiciones generales de ventas aplicables. Es responsabilidad del Cliente asegurarse de que los Servicios de transporte se elijan de acuerdo con las necesidades y expectativas del Pasajero. La exactitud de la Información personal introducida por el Cliente es responsabilidad de este último, excepto en el caso de que BlaBlaCar haya demostrado que tuvo un fallo en la recopilación, el almacenamiento o la protección de dicha Información personal.

**6\. Objetivo no comercial y no empresarial de los Servicios y la Plataforma**

Aceptas utilizar los Servicios y la Plataforma solamente para ponerte en contacto, con fines no comerciales y no empresariales, con personas que desean compartir un Trayecto de coche compartido contigo o reservar un Asiento en el contexto de un Trayecto de autobús o un Trayecto en tren.

En el contexto del Trayecto en coche compartido, reconociste que los derechos de los consumidores derivados de la legislación de la Unión en materia de protección del consumidor no se aplican a su relación con otros Usuarios.

Como Conductor, aceptas no solicitar una Cantidad de costes compartidos superior a los costes reales del trayecto y que puedan generarte un beneficio, dejando especificado que, en el contexto de compartir un coste, tú tienes que asumir como Conductor tu propia parte de los costes incurridos en el Trayecto en coche compartido. Tú eres el único responsable de calcular los costes en los que incurres durante el Trayecto en coche compartido y de asegurarte de que la cantidad solicitada a tus Pasajeros no supera los costes reales del trayecto (excluyendo tu parte de los costes), en particular haciendo referencia a la escala impositiva aplicable**_._**

BlaBlaCar se reserva el derecho a suspender tu Cuenta en caso de que utilices un vehículo de alquiler con conductor u otro vehículo comercial, taxi o vehículo de empresa con el objetivo de generar un beneficio a través de la Plataforma. Recuerda proporcionar a BlaBlaCar, en caso de que te lo solicite, una copia del certificado de registro de tu vehículo y cualquier otra información que demuestre que estás autorizado a utilizar el vehículo en cuestión en la Plataforma y que no estás obteniendo beneficio alguno por el uso de la misma.

Con arreglo a la sección 9 de las presentes CGU, BlaBlaCar también se reserva el derecho a suspender tu Cuenta, limitar tu acceso a los Servicios o resolver los presentes CGU en caso de que tu actividad en la Plataforma, dada la naturaleza de los Trayectos ofrecidos, tu frecuencia, el número de Pasajeros transportados y la Cantidad de costes compartidos solicitada, te conlleve a una posición en la que generes beneficio, o por cualquier otro motivo que sugiera a BlaBlaCar que estás generando beneficio a través de la Plataforma.

**7\. Política de cancelación**

**7.1.    Términos del pago en caso de cancelación**

Solo los Trayectos de coche compartido están sujetos a esta política de cancelación; BlaBlaCar no ofrece garantía alguna en caso de cancelación por cualquier motivo por parte de un Conductor o Pasajero. En el contexto del Trayecto en autobús, la política de cancelación está sujeta a los Términos y condiciones generales de venta de los Operadores de autobuses o a las CGV de tren. Se invita al usuario a consultar los Términos y condiciones generales de venta o las CGV de tren correspondientes para aceptar las condiciones de intercambio y cancelación de su Trayecto.

La cancelación de una Plaza en un Trayecto  de coche compartido por parte del Conductor o el Pasajero una vez Confirmada la reserva, estará sujeta a lo siguiente:

–    En caso de que la cancelación la realice el Conductor, al Pasajero se le devolverá la suma total que haya pagado (es decir, la Cantidad de costes compartidos y los Gastos de Gestión relacionadas). Fundamentalmente se trata de los casos en los que el Conductor cancela el viaje o no llega al punto de encuentro en un plazo de 15 minutos tras la hora acordada;

–    En caso de cancelación por parte del Pasajero:

* Si el Pasajero cancela con una antelación superior a 24 horas antes de la hora de salida programada y mencionada en el Anuncio de coche compartido, al Pasajero solamente se le devolverá la Cantidad de costes compartidos. Los Gastos de Gestión se las quedará BlaBlaCar y el Conductor no tendrá derecho a recibir ninguna suma de cualquier naturaleza;
* Si el Pasajero cancela el viaje con una antelación inferior a 24 horas antes de la hora de salida mencionada en el Anuncio  de coche compartido y una vez hubieran pasado 30 minutos tras la Confirmación de la reserva, el Pasajero tendrá derecho al reembolso de la mitad de la Cantidad de costes compartidos pagada en el momento de la realización de la Reserva. Los Gastos de Gestión se las quedará BlaBlaCar y al Conductor se le pagará el 50 % de la Contribución de Costes a través de la Solución de Pago Hyperwallet;
* Si el Pasajero cancela con una antelación inferior a 24 horas antes de la hora de salida programada mencionada en el Anuncio de coche compartido, y en treinta minutos o menos tras la Confirmación de la reserva, al Pasajero se le devolverá la Cantidad total de gastos compartidos a través de la Solución de Pago Hyperwallet. Los Gastos de Gestión se las quedará BlaBlaCar y el Conductor no tendrá derecho a recibir ninguna suma de ninguna naturaleza;
* Si el Pasajero cancela una vez pasada la hora de salida programada mencionada en el Anuncio de coche compartido, o si no ha llegado al punto de encuentro en un período de 15 minutos tras la hora acordada, no tendrá derecho a ningún reembolso. El Conductor será compensado con el coste total de la Cantidad de costes compartidos a través de la Solución de Pago Hyperwallet y los Gastos de Gestión se las quedará BlaBlaCar.

En caso de que la cancelación se produzca antes de la fecha de salida y por causa imputable al Pasajero, la Plaza o Plazas canceladas por el Pasajero quedarán automáticamente disponibles para otros Pasajeros, quienes podrán reservarlos en línea bajo las condiciones de los presentes CGU.

BlaBlaCar valorará, a su criterio y según la información disponible, la legitimidad de las solicitudes de reembolso.

**7.2.    Derecho de desistimiento**

De conformidad con lo establecido en el Artículo 103 a) de la Ley General para la Defensa de Consumidores y Usuarios, no tendrás derecho a desistimiento a partir del momento de entrega de la Confirmación de reserva, siempre y cuando se haya ejecutado en su totalidad el Contrato entre tú y BlaBlaCar, consistente en ponerse en contacto con otro Usuario.

**7.3 Política de cancelación y cambio de Trayectos en tren**

Los Billetes de tren no se pueden cambiar.

Las condiciones de cancelación varían en función de la Empresa ferroviaria. Se puede consultar la política de cancelación en los CGV de tren o en el [Centro de Ayuda](https://support.blablacar.com/hc/es).

**8\. Comportamiento de las personas que visitan la Plataforma y los Usuarios**

**8.1.    Compromiso de todas las personas que visitan la Plataforma**

Reconoces ser el único responsable de respetar todas las leyes, normativas y obligaciones aplicables a la utilización de la Plataforma.

Además, al utilizar la Plataforma y durante los Trayectos, te comprometes a:

(i) no utilizar la Plataforma con fines profesionales, comerciales o con ánimo de lucro, si no eres un Operador de autobuses o una Empresa ferroviaria;

(ii) no enviar a BlaBlaCar (especialmente en el momento de crear o actualizar tu Cuenta) ni a los demás Usuarios de la Plataforma, Operadores de autobuses o Empresas ferroviarias información falsa, confusa, maliciosa o fraudulenta;

(iii) no hablar ni comportarse, ni publicar ningún contenido (incluidos Mensajes) en la Plataforma que pueda resultar difamatorio, injurioso, obsceno, pornográfico, vulgar, ofensivo, agresivo, improcedente, violento, amenazante, acosador, de naturaleza racista o xenófoba, que tenga connotaciones sexuales, incite a la violencia, discriminación u odio, o que fomente actividades o el uso de substancias ilegales, o de forma más general el contenido illegal que sea contrario a la legislación aplicable, las presentes CGU y los objetivos de la Plataforma y que pueda infringir los derechos de BlaBlaCar o una tercera parte, o que pueda ser contrario a las buenas costumbres;

(iv) no infringir los derechos y no dañar la imagen de BlaBlaCar, en especial en lo que respecta a sus derechos de propiedad intelectual;

(v) no abrir más de una Cuenta en la Plataforma y no abrir una Cuenta en nombre de un tercero;

(vi) no intentar eludir el sistema de reserva en línea de la Plataforma, principalmente intentando enviar a otro Usuario o un Operador de autobuses tu información de contacto para realizar la reserva fuera de la Plataforma con el objetivo de evitar el pago de los Gastos de Gestión;

(vii) no ponerse en contacto con otro Usuario ni con un Operador de autobuses o Empresa ferroviaria, especialmente por medio de la Plataforma, con otra finalidad que no sea la definición de las condiciones del desplazamiento compartido;

(viii) no aceptar ni realizar el pago fuera de la Plataforma o de la solución de pago Hyperwallet;

(ix) cumplir con las presentes CGU.

**8.2.    Compromisos de los Conductores**

Cuando utilizas la Plataforma como Conductor, te comprometes a:

(i) respetar todas las leyes, normativas y códigos con respecto a la conducción y al vehículo, especialmente contar con un seguro de responsabilidad civil en vigor en el momento de realizar el Trayecto de coche compartido y estar en posesión de un carnet de conducir válido;

(ii) comprobar que tu póliza de seguro cubre los desplazamientos compartidos y que los Pasajeros con los que compartes el trayecto son considerados como terceros en tu vehículo y, por lo tanto, están cubiertos por tu seguro;

(iii) no asumir ningún riesgo durante la conducción, ni tomar ningún producto que pueda afectar negativamente a tu atención y a tu capacidad de conducir de manera atenta y completamente segura;

(iv) publicar Anuncios de coche compartido que se correspondan solamente con Trayectos ya programados;

(v) realizar el Trayecto de coche compartido según la descripción en el Anuncio (en especial con respecto a utilizar o no autopista) y a respetar las horas y lugares acordados con los demás Usuarios (en especial el punto de encuentro y el lugar de destino);

(vi) no llevar a más Pasajeros que el número de Plazas indicados en el Anuncio de coche compartido;

(vii) utilizar un vehículo en buen estado y que cumpla con todas las normativas legales, en especial con el Certificado de ITV en vigor;

(viii) a compartir con BlaBlaCar o con cualquier Pasajero que lo solicite tu carnet de conducir, el certificado de registro de tu vehículo, tu póliza de seguro, el certificado de la ITV y cualquier otro documento que demuestre tu capacidad para utilizar el vehículo como Conductor en la Plataforma;

(ix) en caso de retraso o cambio en la hora o en el propio Trayecto, informar a tus Pasajeros sin demora;

(x) en el caso de un Trayecto transfronterizo, mantener y tener disponible para el Pasajero y para cualquier autoridad que lo solicite, un documento válido que muestre tu identidad y tu derecho a cruzar la frontera;

(xi) esperar a los Pasajeros en el punto de encuentro acordado al menos hasta 15 minutos después de la hora acordada;

(xii) no publicar ningún Anuncio de coche compartido relativo a un vehículo que no sea tuyo o para el cual no tengas permiso para utilizarlo como transporte compartido;

(xiii) asegurarte de que los Pasajeros pueden contactarte al número de teléfono registrado en tu perfil

(xiv) no generar ningún beneficio a través de la Plataforma;

(xv) no tener ninguna contraindicación ni incapacidad médica para conducir;

(xvi) comportarse de forma apropiada y responsable durante el Trayecto de coche compartido, de conformidad con la filosofía del desplazamiento compartido.

**8.3.    Compromisos de los Pasajeros**

**8.3.1  Para el Trayecto de coche compartido**

Cuando utilizas la Plataforma como Pasajero de un Trayecto de coche compartido, te comprometes a:

(i) adoptar un comportamiento apropiado durante el Trayecto de coche compartido de modo que no interrumpa la concentración o la conducción del Conductor ni la paz y tranquilidad del resto de los Pasajeros;

(ii) respetar el vehículo del Conductor y su limpieza;

(iii) en caso de retraso, informar al Conductor sin demora;

(iv) esperar al Conductor en el punto de encuentro acordado al menos hasta 15 minutos después de la hora acordada;

(v) comunicar a BlaBlaCar, o a cualquier Conductor que te lo solicite, tu documento de identidad o cualquier otro documento que pruebe tu identidad;

(vi) no transportar durante un Trayecto de coche compartido ningún artículo, sustancia o animal que pueda distraer la concentración y conducción del Conductor, o cuya naturaleza, posesión o transporte sea ilegal;

(vii) en el caso de un Trayecto de coche compartido transfronterizo, mantener y tener disponible para el Conductor y para cualquier autoridad que lo solicite, un documento válido que muestre tu identidad y tu derecho a cruzar la frontera;

(viii) asegurarse de que los Conductores pueden contactarte en el número de teléfono registrado en tu perfil, incluso en el punto de reunión.

En el caso de que hayas realizado una Reserva para una o más Plazas en nombre de terceros, de conformidad con lo establecido en el anterior Artículo 4.2.3, garantizas que dicho tercero respetará las condiciones establecidas en dicho Artículo y, en general, los presentes CGU. BlaBlaCar se reserva el derecho a suspender tu Cuenta, limitar tu acceso a los Servicios o rescindir las presentes CGU en caso de incumplimiento por parte del tercero en cuyo nombre has reservado la Plaza de conformidad con estas CGU.

**8.3.2 Para el Trayecto en autobús**

En el contexto del Trayecto en autobús, el Pasajero se compromete a cumplir los Términos y condiciones generales de venta del Operador de autobuses pertinente.

**8.3.3 Para el Trayecto en tren**

En el contexto del Trayecto en tren, el Pasajero se compromete a cumplir las CGV de tren pertinentes.

**8.4. Notificación de contenidos inapropiados o ilegales (mecanismo de notificación y acción)**

Puedes denunciar un Contenido de los Miembros o un Mensaje sospechoso, inapropiado o ilegal tal y como se describe [aquí](https://support.blablacar.com/hc/es/articles/16765207097117-C%C3%B3mo-denunciar-contenido-ilegal).

BlaBlaCar, tras haber sido debidamente notificado con arreglo a esta sección o por las autoridades competentes, eliminará rápidamente cualquier Contenido de Miembro ilegal si:

* el Contenido de los Miembros es manifiestamente ilegal o contrario a la normativa aplicable; o 
* BlaBlaCar considera que dicho contenido infringe estos CGU.

En tales casos, BlaBlaCar se reserva el derecho a eliminar el Contenido del Miembro que haya sido debidamente denunciado, de forma suficientemente detallada y clara, y/o a suspender inmediatamente la Cuenta denunciada.

El Miembro en cuestión puede apelar las decisiones anteriores de BlaBlaCar tal y como se describe en la sección 15.1 a continuación.

**9\. Restricciones relacionadas con el uso de la Plataforma, suspensión de cuentas, limitación de acceso y rescisión**

Puedes rescindir tus relaciones contractuales con BlaBlaCar, en cualquier momento, de forma gratuita y sin tener que explicar el motivo. Para ello, simplemente debes ir a la pestaña “Darse de baja” en la página de tu Perfil.

En el caso de (i) incumplimiento por tu parte de las presentes CGU, incluidos, entre otros el incumplimiento de tus obligaciones como Usuario según lo establecido en los Artículos 6 y 8 de arriba, (ii) superación del límite establecido arriba en el Artículo 4.3.3, o (iii) en el caso de que BlaBlaCar tenga algún motivo real para creer que es necesario proteger su seguridad e integridad, la de los Usuarios o terceros, o para los fines de investigacioneso prevención de fraude, BlaBlaCar se reserva el derecho a:

(i)  rescindir las CGU que te vinculan con BlaBlaCar de forma inmediata y sin previo aviso; y/o

(ii)  evitar la publicación, o eliminación, de Contenidos de Miembros; y/o

(iii)    limitarte el acceso y el uso de la Plataforma; y/o

(iv)    suspender de forma temporal o permanente tu Cuenta.

La suspensión de la cuenta también puede significar, en su caso, que no recibirá sus pagos pendientes.  
Cuando corresponda, BlaBlaCar también podrá enviarte una advertencia recordándote la obligación de cumplir con las leyes aplicables y/o las presentes CGU.

Cuando sea necesario, se te notificará el establecimiento de dichas medidas para que puedas confirmar tu compromiso de adherirte a estos CGU y a la legislación aplicable, dar explicaciones a BlaBlaCar o impugnar su decisión. BlaBlaCar decidirá, teniendo en cuenta todas las circunstancias de cada caso y la gravedad de la infracción, a su discreción, si levantar o no la rescisión aplicada.

**10\. Datos de carácter personal**

En el contexto del uso de la Plataforma por tu parte, BlaBlaCar recopilará y procesará tu información personal tal y como se describe en su [Política de Privacidad](https://www.blablacar.es/about-us/privacy-policy).

**11\. Propiedad intelectual**

**11.1.    Contenido publicado por BlaBlaCar**

Con sujeción al contenido proporcionado por sus Usuarios, BlaBlaCar es el titular único de todos los derechos de propiedad intelectual relacionados con el Servicio, la Plataforma, su contenido (en concreto los textos, imágenes, diseños, logotipos, vídeos, sonido, datos y gráficos) y con el software y las bases de datos que aseguran su funcionamiento.

BlaBlaCar te garantiza un derecho personal, no exclusivo y no transferible para utilizar la Plataforma y los Servicios, para tu uso personal y privado, con un objetivo no comercial y de conformidad con la finalidad de la Plataforma y los Servicios.

Está totalmente prohibido utilizar o explotar la Plataforma y los Servicios, incluido su contenido, con cualquier otra finalidad que no sea la prevista sin previo consentimiento por escrito de BlaBlaCar. En concreto, está totalmente prohibido lo siguiente:

(i) reproducir, modificar, adaptar, distribuir, representar públicamente y diseminar la Plataforma, los Servicios y todo su contenido, salvo previa autorización expresa de BlaBlaCar;

(ii) descompilar y aplicar ingeniería inversa a la Plataforma o los Servicios, salvo las excepciones estipuladas por los textos aplicables;

(iii) extraer o intentar extraer (en concreto utilizando robots de extracción de datos o herramientas similares de recolección de datos) una parte sustancial de los datos de la Plataforma.

**11.2.    Contenido publicado por el Usuario en la Plataforma**

Con el objetivo de permitir la provisión de los Servicios, y en cumplimiento con el objetivo de la Plataforma, le otorgas a BlaBlaCar una licencia no exclusiva para usar el contenido y los datos que has proporcionado en el contexto de la utilización de los Servicios que podrá incluir tus solicitudes de Reserva, Anuncios de coches compartidos y comentarios en ellos, biografías, fotos, opiniones y respuestas a las opiniones (de aquí en adelante denominados como el “**Contenido desarrollado por el Usuario**”) y Mensajes. Para poder permitir a BlaBlaCar distribuir por medio de la red digital y de conformidad con cualquier protocolo de comunicación (en especial Internet y las redes móviles) y para poder ofrecer al público el contenido de la Plataforma, autorizas a BlaBlaCar a reproducir, representar, adaptar y traducir tu Contenido desarrollado por el Usuario para todo el mundo y durante la duración completa de tus relaciones contractuales con BlaBlaCar, de conformidad con lo siguiente:

(i) autorizas a BlaBlacar a reproducir todo o parte de tu Contenido desarrollado por el Usuario en cualquier medio de grabación digital, conocido o por conocer, en concreto en cualquier servidor, disco duro, tarjeta de memoria u otro medio equivalente, en cualquier formato y por medio de cualquier proceso, ya conocido o por conocer, en el grado necesario para cualquier operación de almacenamiento, copia de seguridad, transmisión o descarga relacionada con el funcionamiento de la Plataforma y el suministro del Servicio;

(ii) autorizas a BlaBlaCar a adaptar y traducir su Contenido desarrollado por el Usuario, y a reproducir dichas adaptaciones en cualquier medio digital, conocido o por conocer, establecido en el punto (i) arriba, con el objetivo de suministrar los Servicios en diferentes idiomas. Este derecho incluye la opción de realizar modificaciones en el formato de tu Contenido desarrollado por el Usuario, con respecto a tu derecho moral, con el objetivo de respetar el diseño gráfico de la Plataforma o para hacer que tu Contenido sea compatible a nivel técnico con nuestro software para su posterior publicación por medio de la Plataforma.

Sigues siendo responsable y tienes todos los derechos sobre el Contenido de los Miembros y los Mensajes que subas en nuestra Plataforma.

**12\. Función de BlaBlaCar**

La Plataforma constituye una plataforma de movilidad de red en línea en la cual los Usuarios pueden crear y publicar Anuncios de coche de compartido de Trayectos de coche compartido con el objetivo de compartir los mismos y los costes asociados al Trayecto. Estos Anuncios de coche compartido serán visualizados por los demás Usuarios para comprobar las condiciones del Trayecto, y, cuando corresponda, para reservar directamente una Plaza en el vehículo en cuestión con el Usuario que ha publicado el Anuncio en la Plataforma. La Plataforma  también la reserva de Asientos para Trayectos en autobús o Trayectos en tren.

Al utilizar la Plataforma y aceptar las presentes CGU, reconoces que BlaBlaCar no es parte de ningún acuerdo formalizado entre tú y los demás Usuarios con el objetivo de compartir los costes relacionados con un Trayecto ni ningún acuerdo realizado entre tú y un Operador de autobuses o una Empresa ferroviaria.

BlaBlaCar no tiene ningún tipo de control sobre el comportamiento de sus Usuarios, los Operadores de autobuses, las Empresas ferroviarias ni sus representantes y los usuarios de la Plataforma. No posee, explota, suministra ni administra los vehículos incluidos en los Anuncios, y no ofrece Trayectos en la Plataforma.

Reconoces y aceptas que BlaBlaCar no controla la validez, veracidad o legalidad de los Anuncios, Plazas y Trayectos ofrecidos. En su capacidad de intermediario, BlaBlaCar no ofrece servicio de transporte ni actúa en calidad de transportista, la función de BlaBlaCar se limita a facilitar el acceso a la Plataforma.

En el contexto de Trayectos de coche compartido, los Usuarios (Conductores o Pasajeros) actúan bajo su única y total responsabilidad.

En su capacidad de intermediario, BlaBlaCar no será considerado responsable de ningún incidente que tenga lugar durante un Trayecto, en especial en lo relacionado con:

(i) información errónea comunicada por el Conductor, por el Operador de autobuses o la Empresa ferroviaria en su Anuncio o por otros medios , con respecto al Trayecto y sus términos;

(ii) la cancelación o modificación de un Trayecto por parte de un Usuario o de un Operador de autobuses o una Empresa ferroviaria;

(iii) el comportamiento de sus Usuarios durante, antes o después del Trayecto.

**13\. Funcionamiento, disponibilidad y funcionalidades de la Plataforma**

BlaBlaCar intentará en la medida de lo posible mantener el funcionamiento constante de la Plataforma los 7 días de la semana y las 24 horas del día. Sin embargo, es posible que se pueda suspender de forma temporal el acceso a la Plataforma, sin previo aviso, por motivos técnicos, de mantenimiento, migración u operaciones de actualización, o debido a cortes de suministro o restricciones relacionadas con el funcionamiento de la red.

Además, BlaBlaCar se reserva el derecho a cambiar la Plataforma o partes de la misma para ciertos usuarios para probar nuevas características y proporcionar una mejor experiencia de usuario, así como modificar o suspender en su totalidad o en parte el acceso a la Plataforma o sus funcionalidades, a su discreción, bien sea de forma temporal o permanente.

**14\. Modificación de los CGU**

Estos CGU y los documentos integrados por referencia explícita forman el acuerdo completo entre tú y BlaBlaCar con relación a la utilización de los Servicios. Cualquier otro documento mencionado en la Plataforma (Preguntas frecuentes, por ejemplo), tiene solamente fines indicativos.

BlaBlaCar podrá modificar los presentes CGU para adaptarlos a su entorno tecnológico y comercial, y para cumplir con la legislación en vigor. Cualquier modificación realizada en los CGU será publicada en la Plataforma indicando la fecha de entrada en vigor, y recibirás una notificación de dicho cambio antes de la entrada en vigor del mismo.

**15\. Ley aplicable y litigios**

Estos CGU se han redactado en español, de conformidad con la legislación española.

**15.1 Sistema interno de tramitación de reclamaciones**

Puedes apelar las decisiones que podamos tomar en relación con:

* el Contenido desarollado por el Usuario por ejemplo, eliminamos, restringimos la visibilidad o nos negamos a eliminar cualquier Contenido desarollado por el Usuario que proporciones al utilizar la Plataforma, o bien

* tu Cuenta: suspendimos tu acceso a la Plataforma,

cuando tomamos tales decisiones basándonos en que el Contenido desarollado por el Usuario constituye contenido ilegal o es incompatible con las presentes CGV. El proceso de apelación se describe [aquí](https://support.blablacar.com/hc/es/articles/16767371351069-C%C3%B3mo-apelar-la-retirada-de-contenido-o-suspensi%C3%B3n-de-una-cuenta).

**15.2 Resolución extrajudicial de conflicto**

Si fuese necesario, también podrás presentar tus reclamaciones con respecto a nuestra Plataforma o Servicios en la plataforma de resolución de litigios en línea de la Comisión Europea, a la que podrás acceder desde el siguiente [enlace](https://webgate.ec.europa.eu/odr/main/?event=main.home.show&lng=ES). La Comisión Europea enviará tu reclamación al organismo nacional competente. En cumplimiento de la legislación aplicable al arbitraje, tienes la obligación de, antes de solicitar el arbitraje, comunicar a BlaBlaCar por escrito cualquier reclamación o litigio con el objetivo de obtener una solución amistosa.

También podrás presentar una solicitud ante un organismo de resolución de litigios de tu país (puedes encontrar la lista [aquí](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)).

**16\. Aviso legal**

La Plataforma está editada por Comuto SA, sociedad limitada, con un capital social de 154,912.382 EUR, registrada en el Registro Empresarial de París con el número 491.904.546 (Número de IVA: FR76491904546), teniendo su sede registrada en el N° 84, Avenue de la République, 75011 París (Francia), representada por su CEO, Nicolas Brusson, Director de la edición del Sitio Web. 

La Plataforma está alojada en los servidores de Google Limited Irlanda, cuya sede social se encuentra en Gordon House, Barrow Street, Dublín 4 (Irlanda).

Comuto SA está registrado en el registro de Operadores de Trayectos y Estancias con el siguiente número: IM075180037

La garantía financiera es proporcionada por: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 París – Francia.

El seguro de responsabilidad civil profesional está suscrito a: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 París – Francia.

Comuto SA está inscrita en el registro de Intermediarios de Seguros, Banca y Finanzas con el número de registro (Orias) 15003890.

Para cualquier pregunta, puede ponerse en contacto con Comuto SA utilizando este [formulario de contacto](https://support.blablacar.com/hc/es/requests/new?ticket_form_id=360000240999).

**17\. Reglamento de Servicios Digitales**

**17.1.  Información sobre la media mensual de destinatarios activos del servicio en la Unión**

De conformidad con el artículo 24, apartado 2, del Reglamento del Parlamento Europeo y del Consejo relativo al mercado interior de servicios digitales y por el que se modifica la Directiva 2000/31/CE (“DSA”), los proveedores de plataformas en línea deben publicar información sobre la media mensual de destinatarios activos de su servicio en la Unión, calculada como media de los últimos seis meses. El objetivo de esta publicación es determinar si un proveedor de plataformas en línea cumple el criterio de “plataformas en línea muy grandes” en virtud de la Ley de Servicios Digitales; es decir, superar el umbral de 45 millones de destinatarios activos mensuales medios en la Unión.

A 17 de agosto de 2024, el número medio mensual de destinatarios activos de BlaBlaCar durante el período comprendido entre febrero y julio de 2024, calculado teniendo en cuenta el considerando 77 y el artículo 3 de la DSA (se cuenta usuarios expuestos al contenido de la Plataforma solo una vez en el período de 6 meses), antes de que se adopte una ley delegada específica, era de aproximadamente 3,28 millones en la UE.

Esta información se publica únicamente a efectos del cumplimiento de los requisitos de la DSA y no debe utilizarse para otros fines. Se actualizará al menos una vez cada seis meses. Nuestro enfoque para elaborar este cálculo puede evolucionar o requerir modificaciones a lo largo del tiempo, por ejemplo, debido a cambios en los productos o a nuevas tecnologías.

Esta información se publica únicamente a efectos del cumplimiento de los requisitos de la DSA y no debería utilizarse para otros fines. Se actualizará al menos una vez cada seis meses. Nuestro planteamiento para elaborar este cálculo puede evolucionar o requerir modificaciones a lo largo del tiempo, por ejemplo, debido a cambios en los productos o a nuevas tecnologías.

**17.2. Punto de contacto para las autoridades**

De conformidad con el artículo 11 de la DSA, si eres miembro de las autoridades competentes de la UE, la Comisión de la UE o el Consejo Europeo de Servicios Digitales, podrá ponerse en contacto con nosotros en relación con asuntos relativos a la DSA a través del correo electrónico [\[email protected\]](https://blog.blablacar.es/cdn-cgi/l/email-protection).

Puedes ponerse en contacto con nosotros en inglés y francés.

Ten en cuenta que esta dirección de correo electrónico no se utiliza para la comunicación con los miembros. Para cualquier cuestión relacionada con el uso de BlaBlaCar, en calidad de Miembro puedes ponerte en contacto con nosotros a través de este [formulario de contacto](https://support.blablacar.com/hc/es/requests/new?ticket_form_id=360000240999).

**18\.** **Incentivo de certificados de ahorro energético (únicamente para España)**

**18.1 Descripción del incentivo**

En el marco de la legislación sobre certificados de ahorro energético (CAE), BlaBlaCar y Delcae (sociedad limitada con domicilio social en Paseo de la Castellana 140, 6ºB, 28046 Madrid, España, registrada con el número B56305618) te permiten, según tu criterio obtener un incentivo derivado del ahorro energético que genera el hecho de compartir coche utilizando la Plataforma como Conductor y/o Pasajero bajo las condiciones establecidas por el Real Decreto 36/2023 del 24 de enero de 2023 que establece un sistema de certificados de ahorro energético en España, parcialmente desarrollado por la Orden TED/815/2023 de 18 de julio de 2023, la Orden TED/845/2023 de 18 de julio de 2023, Resolución de 3 de julio de 2024, de la Dirección General de Planificación y Coordinación Energética, por la que se actualiza el Anexo I de la Orden TED/845/2023, de 18 de julio, por la que se aprueba el catálogo de medidas estandarizadas de eficiencia energética, y las Fichas TRA030 y TRA040 según criterios adicionales de verificación previstos por el Ministerio para la Transición Ecológica y el Reto Demográfico aplicables a los viajes realizados a través de la Plataforma entre el 26 de enero de 2023 y el 18 de julio de 2024 incluido y sujeto a las condiciones descritas a continuación.

Si cumples con las condiciones de la oferta (ver artículo 18.2 a continuación) y sujeto a la validación de su expediente por parte del Ministerio para la Transición Ecológica y el Reto Demográfico, puedes recibir el siguiente incentivo sujeto a las condiciones de uso descritas a continuación:

(i) como Conductor, un pago mediante transferencia bancaria;

(ii) como Pasajero, un crédito en tu Cuenta BlaBlaCar para utilizar en la Plataforma en un plazo de 12 meses desde la fecha de emisión del crédito. Los Pasajeros podrán acceder a los detalles de las condiciones del crédito en la Plataforma en España.

Los detalles y el valor del incentivo serán comunicados una vez realizadas las verificaciones obligatorias mencionadas en la cláusula 18.3. La comunicación de esta información no garantiza el pago del incentivo, que estará sujeto a las validaciones de ahorro energético de conformidad con la cláusula 18.3.

BlaBlaCar puede en cualquier momento modificar, suspender o finalizar la oferta de incentivo con previo aviso. Entiendes y aceptas que la existencia y validez de la normativa del Sistema CAE es una condición esencial para la emisión y/o el pago del incentivo por parte de BlaBlaCar. Reconoces y aceptas que, en caso de anulación, invalidación o desaparición total o parcial de la normativa del Sistema CAE, la oferta de incentivo expirará automáticamente y que no tendrás derecho a ningún incentivo ni serás acreedor a ningún pago por parte de BlaBlaCar.

Además, BlaBlaCar se reserva el derecho de especificar condiciones adicionales para el uso de incentivos en la Plataforma. BlaBlaCar se reserva el derecho de anular o establecer una fecha de expiración para cualquier crédito emitido en los siguientes casos:

* el crédito no está siendo utilizado estrictamente de acuerdo con las condiciones de uso;
* BlaBlaCar sospecha de un uso fraudulento o indebido del crédito.

**18.2 Elegibilidad para la oferta de incentivo**

Sólo puedes beneficiarte de esta oferta de incentivo una vez como Conductor y una vez como Pasajero, siempre que cumplas con todas las siguientes condiciones:

(i) estás registrado en el sitio web [www.blablacar.es](http://www.blablacar.es/) y tienes una cuenta activa en la Plataforma;

(ii) has registrado tu nombre y apellido en tu cuenta (no se aceptan seudónimos, series de letras aleatorias, uso de números u otros registros similares) antes del 18 de julio de 2024 incluido;

(iii) has ingresado y certificado tu correo electrónico y número de teléfono móvil antes del 18 de julio de 2024 incluido;

(iv) has realizado entre el 26 de enero de 2023 y el 18 de julio de 2024 incluido al menos un viaje en España y entre dos ciudades de España (a) como Conductor con al menos un Pasajero que haya pagado los Gastos compartidos a través de la Plataforma, y/o (b) como Pasajero y has pagado los Gastos compartidos al Conductor a través de la Plataforma;

(v) Si eres Conductor, has registrado tus datos bancarios en tu cuenta de la Plataforma y has recibido el pago de los Gastos compartidos por el viaje mencionado anteriormente en tu cuenta bancaria;

(vi) has recibido un correo electrónico directamente de BlaBlaCar informándote sobre CAE y la posibilidad de beneficiarte de un incentivo a cambio de la transferencia de propiedad del ahorro energético que has generado a través del uso de la Plataforma y no has solicitado no beneficiarte de este incentivo ni rechazado el tratamiento de datos personales asociados como se describe en nuestra Política de Privacidad;

(vii) no te has beneficiado de un incentivo equivalente para el mismo viaje propuesto por otra plataforma de coche compartido.

**18.3 Verificación del cumplimiento de las condiciones de la oferta de incentivo y validación de tu acción de ahorro energético por las autoridades**

Las disposiciones relativas a la oferta vinculada a los CAE sólo son aplicables una vez en condición de Conductor y una vez en condición de Pasajero. Aceptas responder de manera oportuna a cualquier solicitud de BlaBlaCar si se verifica el cumplimiento de esta condición. En ausencia de una respuesta satisfactoria a estas solicitudes, BlaBlaCar se reserva el derecho de no entregar ni pagar el incentivo.

Además, la normativa del Sistema CAE impone verificaciones y validaciones obligatorias de los ahorros energéticos presentados por las entidades encargadas de los controles, como el Coordinador Nacional o los verificadores de ahorro energético. Estas verificaciones y validaciones son condiciones para la emisión y/o el pago del incentivo, que se realizará después de la validación final por parte del Coordinador Nacional. Reconoces que, si tu ahorro energético no es validado por el Coordinador Nacional, BlaBlaCar no podrá emitir ni pagar el incentivo.

* * *

**Versión vigente a partir del 3 de junio de 2024:**

**1\. Descripción**

Comuto SA (de aquí en adelante denominada “BlaBlaCar”) ha desarrollado una plataforma de trayectos compartidos a la que se puede acceder desde el sitio web [www.blablacar.es](http://www.blablacar.es/) o desde una aplicación móvil, diseñada (i) para poner en contacto a los conductores que viajan a un determinado destino con pasajeros que se dirigen al mismo lugar, de forma que puedan compartir el Trayecto así como los costes asociados al mismo; (ii) para reservar billetes para trayectos en autobús operados por Operadores de autobuses; y (iii) para buscar y comprar en BlaBlaCar Billetes de tren de alguno de los operadores de transporte (las “Empresas ferroviarias”) que ofrecemos (en adelante denominada la “Plataforma”).

Las presentes condiciones generales de uso tienen por objeto regular el acceso y los términos de uso de la Plataforma. Se ruega leer atentamente estas condiciones. Entiendes y reconoces que BlaBlaCar no es parte en ningún contrato, acuerdo o relación contractual, de ninguna clase, suscrito entre los Usuarios de la Plataforma entre el Operador de autobuses o Empresa ferroviaria y tú.

Al hacer clic en “Iniciar sesión con Facebook”, “Registrarse con una dirección de correo electrónico” o “Pagar”, reconoces que has leído y aceptado las presentes condiciones generales de uso.

Si inicias sesión con tu Cuenta en la plataforma de BlaBlaCar de otro país (por ejemplo, [www.blablacar.com.br](http://www.blablacar.com.br/)), por favor, ten en cuenta que se aplicarán (i) las condiciones generales de uso de esa plataforma, (ii) la política de privacidad de dicha plataforma y (iii) las normativas y reglamentos legales de dicho país. Asimismo, la información de tu Cuenta, datos personales incluidos, se tendrá que transferir a la entidad legal que opere la otra plataforma. Ten en cuenta que BlaBlaCar se reserva el derecho de restringir el acceso a la Plataforma a los usuarios razonablemente identificados como ubicados fuera del territorio de la Unión Europea.

**2\. Definiciones**

En este documento,

“Anuncio” significa un anuncio relacionado con un Trayecto publicado por un usuario Conductor en la Plataforma o un Anuncio de tren;

“Anuncio de coche compartido” significa un anuncio relativo a un Desplazamiento publicado en la Plataforma por un Conductor; 

“Anuncio de autobús” se refiere a un anuncio relativo a un Viaje en autobús de un Operador de autobús publicado en la Plataforma;

“Anuncio de tren” se refiere a un anuncio relativo a un Trayecto en tren operado por una Empresa ferroviaria y ofrecido en la Plataforma;

“BlaBlaCar” tiene el significado que se le atribuye en el Artículo 1 arriba;

“Billete” significa el título nominativo de transporte válido dado al Consumidor después de la Reserva de un Viaje en autobús o en tren como prueba del contrato de transporte existente entre pasajeros y el Operador de autobuses o la Empresa ferroviaria regido por los Términos y condiciones generales de venta, sin perjuicio de cualquier condición particular estipulada adicionalmente entre el Pasajero y el Operador de autobuses o la Empresa ferroviaria, y referida en el Billete;

“CGU” significa las presentes Condiciones Generales de Uso;

“CGV” hace referencia a los [Términos y condiciones generales de venta](https://blog.blablacar.es/blablalife/viaje-en-carretera/condiciones-generales-de-venta-de-transporte) del Operador de autobuses en cuestión, en función del Viaje en autobús seleccionado por el cliente, y a los términos y condiciones especiales disponibles en el Sitio web y que hayan sido reconocidos como leídos y aceptados por el Cliente antes de realizar el pedido;

“CGV de tren” se refiere a los Términos y [condiciones generales de transportes de la Empresa ferroviaria](https://blog.blablacar.es/condiciones-generales-de-venta-de-la-empresa-ferroviaria) en cuestión (en adelante, “CGV de tren”), en función del Viaje en tren seleccionado por el cliente y a los términos y condiciones que hayan sido reconocidos y leídos por el Cliente antes de realizar el pedido y que dependen de la Empresa ferroviaria que opere el Trayecto;

“Cliente” significa cualquier persona física (Usuario o no) que compre, ya sea para sí mismo o en nombre de otra persona que vaya a ser el Pasajero, un Billete de autobús o un Billete de tren a través de la Plataforma para realizar un Viaje realizado por el Operador de autobuses o la Empresa ferroviaria;

“Cuenta” significa la cuenta que debe crearse para ser Usuario y poder acceder a determinados servicios ofrecidos por la Plataforma;

“Cuenta de Facebook” tiene el significado que se le atribuye en el Artículo 3.2 a continuación;

“Conductor” significa el Usuario que utiliza la Plataforma para ofrecer la compartición de un Trayecto a otra persona a cambio de compartir los costes asociados al trayecto, y transportar a dicha persona en un Trayecto determinado en la fecha y hora en las que el Conductor ha organizado el Trayecto con anterioridad;

“Confirmación de la reserva” tiene el significado que se le atribuye en el Artículo 4.2.1 a continuación;

“Contenido desarrollado por el Usuario” tiene el significado que se le atribuye en el Artículo 11.2 a continuación;

“Empresa ferroviaria” se refiere a una empresa profesional de transporte de pasajeros cuyos Trayectos en tren se ofrecen en la Plataforma;

“Gastos compartidos” significa, para un Trayecto de coche compartido determinado, la suma de dinero solicitada por el Conductor y aceptada por el Pasajero por su contribución a los gastos del viaje;

“Gastos de Gestión” tiene el significado que se le atribuye en el Artículo 5.2 a continuación;

“Operador de autobús” se refiere a una empresa dedicada al transporte profesional de viajeros, y cuyos billetes de viaje son distribuidos en la Plataforma por BlaBlaCar;

“Pasajero” significa (i) el Usuario que ha aceptado compartir un Trayecto con el Conductor, o, si procede, la persona en cuyo nombre ha reservado una Plaza un Usuario o (ii) el Cliente que ha adquirido un Billete de un Operador de autobuses o Empresa ferroviaria o, si procede, la persona en cuyo nombre el Cliente ha adquirido un Billete de un Operador de autobuses o Empresa ferroviaria;

“Pedido” se refiere a la operación por la que el Cliente reserva sus Servicios con BlaBlaCar Bus, cualquiera que sea el medio utilizado, con la única excepción de la compra de Billetes directamente realizadas en un Punto de venta, y que implica la obligación para el Pasajero o posiblemente un Agente de pagar el precio relativo a los Servicios pertinentes;

“Plaza” significa el asiento reservado por un Pasajero en el vehículo del Conductor, o en el vehículo llevado a cabo por el Operador del autobús o a bordo de un tren;

“Plataforma” tiene el significado que se le atribuye en el Artículo 1 arriba;

“Precio” se refiere, para un Trayecto en autobús o un Trayecto en tren determinado, al precio que incluye todos los impuestos, tasas y costes de los servicios pertinentes incluidos, pagados por el Cliente en la Plataforma, en el momento de la validación del Pedido, para un Asiento en un Trayecto en autobús o en un Trayecto en tren determinado;

“Punto de venta” hace referencia a los mostradores y terminales físicos que aparecen en el Sitio web y en los que se ofrecen los Billetes a la venta;

“Servicios” significa todos los servicios ofrecidos por BlaBlaCar por medio de la Plataforma;

“Servicios de transporte” se refiere a los servicios de transporte suscritos por un Pasajero de Viaje en Autobús o Tren y proporcionados por el Operador de autobuses o la Empresa ferroviaria;

“Sitio web” significa el sitio web al que se puede acceder desde la dirección [www.blablacar.es](http://www.blablacar.es/);

“Tramo” tiene el significado que se le atribuye en el Artículo 4.1 a continuación;

“Trayecto” indistintamente se refiere a un Trayecto en autobús, a un Trayecto en tren o a un Trayecto en coche compartido;

“Trayecto en coche compartido” significa el recorrido sujeto de un Anuncio de coche compartido publicado por un Conductor en la Plataforma por el cual el Conductor acepta compartir un Trayecto con Pasajeros, Usuarios de la plataforma, a cambio de la compartición de los gastos asociados al Trayecto;

“Trayecto en autobús” significa el viaje sujeto de un Anuncio de autobús en la Plataforma y para el cual el Operador de autobuses propone asientos en los vehículos de autobús a cambio del Precio;

“Trayecto en tren” significa el viaje sujeto a un Anuncio de tren en la Plataforma y para el cual la Empresa ferroviaria proporciona asientos a bordo del tren a cambio del Precio.

“Usuario” significa cualquier persona que haya creado una Cuenta en la Plataforma.

“Solución de Pago Hyperwallet” tiene el significado que se le otorga en el artículo 5.4.2.a siguiente;

“Proveedor de la Solución de Pago Hyperwallet” se refiere a PayPal (Europe) S.à r.l. et Cie, S.C.A.

**3\. Registro en la Plataforma y creación de una Cuenta**

**3.1.    Condiciones de registro en la Plataforma**

La Plataforma solamente puede ser utilizada por personas mayores de 18 años. Está estrictamente prohibido el registro en la plataforma por parte de un menor. Al acceder, utilizar o registrarse en la Plataforma, confirmas y garantizas que tienes más de 18 años.

**3.2.    Creación de una Cuenta**

La Plataforma permite a los Usuarios publicar y ver Anuncios de coche compartido e interactuar entre ellos para reservar una Plaza. Es posible ver los Anuncios sin estar registrado en la Plataforma. No obstante, no podrás publicar un Anuncio de coche compartido ni reservar una Plaza sin haber creado primero una Cuenta para convertirte en Usuario.

Para crear tu Cuenta puedes utilizar cualquiera de los dos métodos utilizados a continuación:

(i)    rellenar todos los campos obligatorios en el formulario de registro;

(ii)    o iniciar sesión en tu cuenta de Facebook a través de nuestra Plataforma (en adelante denominada como su “Cuenta de Facebook”). Al utilizar dicha funcionalidad, entiendes que BlaBlaCar tendrá acceso a publicar en la Plataforma y conservar determinada información de tu Cuenta de Facebook. Puedes eliminar el enlace entre tu Cuenta y tu Cuenta de Facebook en cualquier momento en la sección “Certificaciones” de tu perfil. Si deseas obtener más información sobre la utilización de tus datos obtenidos de tu Cuenta de Facebook, puedes leer nuestra [Política de privacidad](https://www.blablacar.es/about-us/privacy-policy) y la Política de privacidad de Facebook.

Para registrarte en la Plataforma es necesario que hayas leído y aceptado las presentes CGU.

Al crear tu Cuenta, independientemente del método seleccionado, te comprometes a proporcionar información verídica y precisa, y a actualizar dicha información por medio de tu perfil o enviando una notificación a BlaBlaCar, con el objetivo de garantizar su relevancia y precisión durante la duración total de tu relación contractual con BlaBlaCar.

En caso de que te registres utilizando un correo electrónico, te comprometes a guardar en secreto la contraseña seleccionada durante la creación de tu Cuenta y a no comunicársela a ninguna otra persona. En caso de pérdida o divulgación de tu contraseña, deberás comunicárselo a BlaBlaCar inmediatamente. Tú eres el único responsable del uso de tu Cuenta por parte de terceras partes, salvo que hayas comunicado de forma expresa a BlaBlaCar la pérdida, el uso fraudulento por parte de un tercero, o la revelación de tu contraseña a un tercero.

Recuerda no crear ni utilizar, bajo tu propia identidad o bajo la identidad de un tercero, ninguna Cuenta adicional a la creada inicialmente.

**3.3.    Verificación**

BlaBlaCar podrá, con fines de transparencia, de mejora de la veracidad o de prevención o detección de fraude, establecer un sistema para verificar parte de la información proporcionada en tu perfil. Se trata, fundamentalmente, de aquellos casos en los que introduces tu número de teléfono o nos proporcionas un Documento de Identidad.

Reconoces y aceptas que cualquier referencia en la Plataforma o en los Servicios a la información “certificada”, o a cualquier otro término similar, significa solamente que un Usuario ha superado con éxito el procedimiento de verificación existente en la Plataforma o en los Servicios para proporcionarle más información sobre el Usuario con el que está pensando compartir un Trayecto. BlaBlaCar no puede garantizar la veracidad, fiabilidad o validez de la información sujeta al procedimiento de verificación.

**4\. Utilización de los Servicios**

**4.1.    Publicación de Anuncios**

Como Usuario, siempre y cuando cumplas las condiciones establecidas a continuación, podrás crear y publicar Anuncios de coche compartido en la Plataforma introduciendo información sobre el Trayecto de coche compartido que vas a realizar (fechas/horas/puntos de recogida y llegada, número de plazas ofrecidas, opciones disponibles, cantidad de Costes compartidos, etc.).

Al publicar su Anuncio de coche compartido, puede indicar las ciudades importantes en las que acepta detenerse, recoger o dejar a los pasajeros. Las secciones del Trayecto en coche compartido entre estas ciudades importantes o entre una de estas ciudades importantes y el punto de recogida o destino del Trayecto en coche compartido constituyen “Tramos”.

Solamente podrás publicar un Anuncio de coche compartido si cumples todas y cada una de las condiciones indicadas a continuación:

(i)    posees un carnet de conducir válido;

(ii)    publicas solamente Anuncios de coche compartido con vehículos de tu propiedad o utilizados con consentimiento expreso del propietario, y siempre y cuando estés autorizado para utilizar el vehículo con fines de desplazamiento compartido;

(iii)    eres y seguirás siendo el conductor principal del vehículo sujeto del Anuncio de coche compartido;

(iv)    el vehículo tiene un seguro a terceros en vigor;

(v)    no tienes ninguna contraindicación ni incapacidad médica para conducir;

(vi)    el vehículo que vas a utilizar para el Trayecto es un turismo de 4 ruedas y un máximo de 7 asientos, excluyendo los conocidos como vehículos “sin carnet”.

(vii)    no tienes intención de publicar otro anuncio para el mismo Trayecto de coche compartido en la Plataforma;

(viii)    no ofreces más Plazas de las disponibles en tu vehículo y de las permitidas en la Plataforma;

(ix)    todos las Plazas ofertadas tienen cinturón de seguridad, incluso en el caso de que el vehículo esté homologado con asientos sin cinturón de seguridad;

(x)    utilizas un vehículo en buen estado y que cumple con todas las normativas legales, y que el vehículo dispone del Certificado de ITV actualizado;

(xi) eres un consumidor y no actúas como profesional.

Reconoces que tú eres el único responsable del contenido del Anuncio de coche compartido que publicas en la Plataforma. Por lo tanto, reconoces y garantizas la veracidad y precisión de toda la información incluida en tu Anuncio de coche compartido, y te comprometes a realizar el Trayecto de coche compartido bajo las condiciones descritas en tu Anuncio de coche compartido.

Tu Anuncio de coche compartido será publicado en la Plataforma y por lo tanto podrá ser visto por todos los Usuarios y visitantes, incluso por las personas que no son Usuarios de la Plataforma, que realicen una búsqueda en la Plataforma o en el sitio web de las empresas asociadas a BlaBlaCar. BlaBlaCar se reserva el derecho, a su exclusivo criterio, a no publicar o a borrar, en cualquier momento, cualquier Anuncio de coche compartido que no cumpla con los presentes CGU o que considere que puede dañar su imagen, la imagen de la Plataforma o la imagen de los Servicios, y/o suspender la Cuenta del Miembro que publique dichos Anuncios de coche compartido con arreglo a la Sección 9 de las presentes CGU.

Se te informa de que en caso de que te presentes como consumidor utilizando la Plataforma cuando de hecho actúas como profesional, estás expuesto a las sanciones previstas por la ley aplicable.

**4.2.    Reservar una Plaza** 

BlaBlaCar ha establecido un sistema para la reserva de Plazas en línea (“Reserva”) para algunos de los Trayectos ofrecidos en la Plataforma.

Los métodos de reserva de un asiento dependen de la naturaleza del trayecto previsto.

BlaBlaCar proporciona a los usuarios de la Plataforma un motor de búsqueda basado en diferentes criterios de búsqueda (lugar de origen, destino, fechas, número de Trayectos, etc.). Algunas funcionalidades adicionales se proporcionan en el motor de búsqueda cuando el usuario está conectado con su cuenta. BlaBlaCar invita al usuario, independientemente del proceso de reserva utilizado, a consultar y utilizar cuidadosamente el buscador para determinar la oferta más adaptada a sus necesidades. Puedes encontrar más información [aquí](https://blog.blablacar.es/about-us/transparencia-de-las-plataformas). El Cliente, al reservar un Trayecto en autobús en un Punto de venta, también puede pedir al Operador de autobuses o al agente de recepción que realice la búsqueda.

**4.2.1 Trayecto en coche compartido**

Cuando un Pasajero esté interesado en un Anuncio con sistema de reserva, puede realizar una solicitud de reserva en línea. Esta solicitud de reserva puede ser (i) aceptada automáticamente (en caso de que el Conductor haya seleccionado esta opción al publicar el Anuncio), o (ii) aceptada por el Conductor de forma manual. En el momento de la reserva, el Pasajero realiza el pago en línea por la cantidad de Costes compartidos y los Gastos de Gestión relacionadas, si procede. Una vez recibido el pago por parte de BlaBlaCar y tras la validación de la reserva por parte del Conductor, si procede, el Pasajero recibe una confirmación de reserva (la “Confirmación de reserva”).

Si eres un Conductor y has seleccionado gestionar de forma manual las solicitudes de Reserva al publicar tu Anuncio de coche compartido, deberás responder a las solicitudes de Reserva dentro del tiempo especificado por el Pasajero durante la solicitud de Reserva. De no hacerlo, la solicitud de Reserva caducará de forma automática y el Pasajero recibirá el reembolso de la cantidad que haya pagado en el momento de realizar la solicitud de reserva, si procede.

En el momento de la Confirmación de reserva, BlaBlaCar te enviará el número de teléfono del Conductor (si eres el Pasajero) o del Pasajero (si eres el Conductor) si el Usuario aceptó mostrar su número de teléfono. A partir de este momento tú eres el único responsable del cumplimiento del contrato que te vincula con el otro Usuario.

**4.2.2. Trayecto en autobús** 

Para Trayectos en autobús, BlaBlaCar permite la reserva de Billetes de autobús para un Trayecto en autobús determinado a través de la Plataforma. 

Los Servicios de transporte se rigen por los Términos y condiciones generales de venta del Operador de autobuses correspondiente, dependiendo del Trayecto seleccionado por el Cliente, el cual debe ser aceptado por el Cliente antes de realizar el pedido. BlaBlaCar no proporciona ningún servicio de transporte con respecto a los Trayectos en autobús, los Operadores de autobuses son solamente una parte en los Términos y condiciones generales de la venta. Aceptas que la reserva de Asientos para un Trayecto en autobús determinado está sujeta a los Términos y condiciones generales de venta, el Operador de autobuses correspondiente. 

BlaBlaCar quiere que prestes atención al hecho de que ciertos Servicios de transporte ofrecidos por el Operador de autobuses y mencionados en el Sitio web pueden retirarse, en especial por razones climáticas, en temporada alta o en caso de fuerza mayor.

**4.2.3. Trayecto en tren**

Para Trayectos en tren, BlaBlaCar permite la reserva de billetes para un Trayecto en tren determinado a través de la Plataforma.

Los Servicios de transporte se rigen por las CGV de tren correspondientes en función del Trayecto seleccionado por el Cliente, las cuales deberán ser aceptadas por este antes de realizar el Pedido. BlaBlaCar no presta ningún servicio de transporte con respecto a los Trayectos en tren, siendo las Empresas ferroviarias las únicas partes de las CGV de tren. Aceptas que la reserva de Asientos para un Trayecto en tren determinado está sujeta a las CGV de tren de la Empresa ferroviaria correspondiente.

BlaBlaCar recomienda prestar atención al hecho de que ciertos Servicios de transporte ofrecidos por las Empresas ferroviarias y mencionados en el Sitio web pueden retirarse, en especial por razones climáticas, en temporada alta o en caso de fuerza mayor.

El Cliente recibirá un Billete una vez validado el Pedido.

**4.2.4 Derecho e información de los Pasajeros de tren**

**4.2.4.1 General**

En virtud del Reglamento (UE) 2021/782, los Pasajeros ferroviarios tienen amplios derechos y obligaciones, los cuales se pueden consultar en la siguiente página web: [https://eur-lex.europa.eu/legal-content/ES/TXT/HTML/?uri=CELEX:32021R0782](https://eur-lex.europa.eu/legal-content/ES/TXT/HTML/?uri=CELEX:32021R0782)

La información indicada anteriormente, en el presente Artículo 4.2.3.4, se facilita en los enlaces que figuran a continuación y/o en la página que figura a continuación: [https://support.blablacar.com/hc/es](https://support.blablacar.com/hc/es), entendiéndose que en caso de discrepancia entre la información de las Empresas ferroviarias y este enlace, prevalecerá la información proporcionada por las Empresas ferroviarias.

**4.2.4.2 Información antes del viaje**

Para mantenerte al corriente de las condiciones y modalidades que se indican a continuación, puedes consultar las CGV de tren de las Empresas ferroviarias correspondientes.

* Condiciones de acceso con bicicletas;
* Alteraciones, anulaciones, correspondencias perdidas y retrasos;
* Disponibilidad de las instalaciones a bordo, incluida la conexión Wi-Fi y los aseos, y los servicios a bordo, incluida la asistencia que el personal ofrece a los pasajeros;
* Procedimientos de recuperación de equipaje perdido.

**4.2.4.3 Información específica para personas con discapacidad y para personas con movilidad reducida:**

**Punto de contacto para obtener asistencia:**

El Pasajero que necesite asistencia deberá comunicarlo, a través de los enlaces indicados a continuación, a la Empresa ferroviaria correspondiente al menos 24 horas antes de que se requiera dicha asistencia. 

La asistencia la prestan las Empresas ferroviarias con la condición de que el Pasajero se presente en el punto indicado a la hora fijada por la Empresa ferroviaria.

Si tienes algún problema para ponerte en contacto con la Empresa ferroviaria, puedes contactar con BlaBlaCar a través del siguiente [Formulario](https://support.blablacar.com/hc/es/requests/new?ticket_form_id=18313652317597) o en el [Centro de Ayuda.](https://support.blablacar.com/hc/es)

**Condiciones de accesibilidad:**

Las condiciones de accesibilidad de la estación y a bordo de las instalaciones asociadas se pueden consultar en la página web de las estaciones correspondientes o en el [Centro de Ayuda.](https://support.blablacar.com/hc/es)

Información durante el viaje:

El pasajero puede consultar los datos que se indican a continuación en las CGV de tren de la Empresa ferroviaria correspondiente o en el Billete:

* Servicios e instalaciones a bordo, incluida la conexión Wi-Fi
* La siguiente estación
* Alteraciones y retrasos (planificados y en tiempo real)
* Servicios de conexión principales

Problemas de seguridad

**4.2.5 Naturaleza designada de la reserva de Asiento y condiciones de uso de los Servicios en nombre de un tercero**

Cualquier utilización de los Servicios, tanto en calidad de Pasajero como de Conductor, está relacionada con un nombre específico. La identidad del Conductor y del Pasajero debe corresponderse con la identidad comunicada por los mismos a BlaBlaCar y a los demás Usuarios que participan en el Trayecto de uso compartido, el Operador de autobuses o la Empresa ferroviaria.

Sin embargo, BlaBlaCar permite a sus Usuarios reservar uno o más Asientos en nombre de terceros. En este caso, te comprometes a indicar con precisión al Conductor, en el momento de la Reserva, los nombres, la edad y el número de teléfono de la persona en cuyo nombre se reserva un asiento. Está estrictamente prohibido reservar un Asiento para un menor de 13 años que viaja solo en un Trayecto en coche compartido. En el caso de que reserves un asiento para un Trayecto en coche compartido para un menor mayor de 13 años que viaja solo, te comprometes a solicitar el acuerdo previo del Conductor y a proporcionarle el permiso debidamente completado y firmado de sus representantes legales. En lo que respecta a los Trayectos en autobús o a los Trayectos en tren, te recomendamos consultar las CGV del Operador de autobús o las CGV de tren de la Empresa ferroviaria correspondiente.

La Plataforma está pensada para reservar Plazas para personas. Está prohibido reservar una Plaza para transportar cualquier objeto, paquete, animal que viaje solo o cualquier otro elemento material.

Está igualmente prohibido publicar un Anuncio para cualquier otro Conductor que no seas tú.

**4.3.    Contenido desarollado por el Usuario, moderación y sistema de evaluación**

**4.3.1.    Funcionamiento del sistema de evaluación**

BlaBlaCar te anima a publicar tus comentarios y/o valoraciones sobre un Conductor (en el caso de que seas un Pasajero) o sobre un Pasajero (en el caso de que seas un Conductor) con el cual hayas compartido un Trayecto o con quien se supone que hubieses debido compartir un Trayecto. Sin embargo, no está permitido dejar ningún comentario sobre otro Pasajero si eres un Pasajero, ni sobre ningún otro Usuario con quien no hayas viajado o con quien no tenías previsto viajar.

Tu opinión, al igual que las opiniones que hayan publicado otros Usuarios sobre ti, si existieran, solamente se podrán ver y se publicarán en la Plataforma una vez transcurrido el más breve de los siguientes periodos: (i) inmediatamente después de que ambos hayáis dejado un comentario, o (ii) después de un período de 14 días tras la primera opinión.

Podrás responder a un comentario realizado en tu perfil por parte de otro Usuario. El comentario y, si procede, su respuesta, se publicarán en tu perfil.

**4.3.2.    Moderación**

**a. Contenido de los Miembros**

Reconoces y aceptas que BlaBlaCar podrá moderar antes de su publicación, mediante herramientas automatizadas o manualmente, el Contenido desarollado por el Usuario, tal y como se define en la sección 11.2. Si BlaBlaCar considera que dicho Contenido desarollado por el Usuario infringe las leyes aplicables o estas CGU, se reservará el derecho a:

* impedir que se publique o se elimine tal Contenido desarollado por el Usuario
* enviar una advertencia al Usuario recordándole la obligación de cumplir la legislación aplicable o las presentes CGU, y/o
* aplicar las medidas restrictivas de acuerdo con la sección 9 de estas CGU.

El uso por parte de BlaBlaCar de dichas herramientas automatizadas o de la moderación manual no se interpretará como un compromiso de supervisar o una obligación de buscar activamente actividades ilegales o Contenido desarollado por el Usuario publicado en la Plataforma y, en la medida en que lo permita la legislación aplicable, no dará lugar a responsabilidad alguna para BlaBlaCar.

**4.3.3.    Límite**

Con arreglo a la sección 9 de las presentes CGU, BlaBlaCar se reserva el derecho a suspender tu Cuenta, limitar su acceso a los Servicios, o rescindir los presentes CGU en caso de que (i) hayas recibido al menos tres valoraciones negativas y (ii) una de las opiniones promedias que hayas recibido tengan una puntuación de 3 o inferior, en función de la gravedad de los comentarios dejados por un Miembro en la opinión.

**5\. Condiciones financieras**

El acceso y el registro en la Plataforma, así como la búsqueda, visualización y publicación de Anuncios, no implican coste alguno. No obstante, la reserva se realizará tras el pago correspondiente de una tarifa, bajo las condiciones descritas a continuación.

**5.1.    Costes compartidos y Precio**

5.1.1 En el caso de un Viaje de coche compartido, los Costes compartidos los determinas tú, como Conductor, bajo tu única responsabilidad. Está estrictamente prohibido obtener ningún tipo de beneficio mediante el uso de nuestra Plataforma. Por lo tanto, estás de acuerdo en limitar la Cantidad de costes compartidos solicitada a tus Pasajeros a los costes reales del Trayecto de coche compartido que vas a compartir con los Pasajeros. De lo contrario, asumirás los riesgos de recalificación de la transacción realizada a través de la Plataforma.

Cuando publiques un Anuncio de coche compartido, BlaBlaCar te sugerirá una Cantidad de Costes compartidos, que tiene en cuenta principalmente la naturaleza del Trayecto y la distancia recorrida. Esta cantidad se indica solamente a modo de guía y puedes aumentarla o reducirla teniendo en cuenta los costes reales del Trayecto de coche de compartido. Para evitar el uso abusivo de la Plataforma, BlaBlaCar limita la posibilidad de ajuste de los Costes compartidos.

5.1.2 Con respecto al Trayecto en autobús o al Trayecto en tren, el Precio por Asiento se define a discreción del Operador de autobuses o de la Empresa ferroviaria. Se invita al Cliente a consultar los Términos y condiciones generales de venta o las CGV de tren pertinentes para comprender las modalidades aplicables con respecto a la realización de la orden de emisión de Billetes y los métodos de pago. 

En cuanto a las condiciones de pago del Billete de Tren, están disponibles en la Plataforma en el momento de realizar el Pedido.

**5.2.    Gastos de Gestión**

BlaBlaCar, a cambio del uso de la Plataforma, en el momento de la Reserva, cobrará los gastos de gestión (en adelante, los “Gastos de Gestión”). Se informará al usuario antes de cualquier aplicación los Gastos de Gestión, según corresponda.

Los métodos de cálculo de los Gastos de Gestión vigentes que pueden encontrarse [aquí](https://blog.blablacar.es/blablalife/viaje-en-carretera/gastos-de-gestion*) se proporcionan solo con fines informativos y no tienen valor contractual. BlaBlaCar se reserva el derecho de modificar en cualquier momento los métodos de cálculo de los Gastos de Gestión. Estos cambios no tendrán ningún efecto en los Gastos de Gestión aceptados por el Usuario antes de la fecha efectiva de dichos cambios.

En el caso de trayectos transfronterizos, tenga en cuenta que los métodos para el cálculo de los importes de los Gastos de Gestión y el IVA aplicable podrán variar dependiendo del punto de recogida y/o el destino del Trayecto.

Al utilizar la Plataforma para la realización de trayectos transfronterizos o al exterior de España, puede que los Gastos de Gestión los cobre la empresa filial de BlaBlaCar que opera la plataforma local.

**5.3.    Redondeo**

Reconoces y aceptas que BlaBlaCar puede, a su única discreción, redondear de forma ascendente o descendente a números enteros o decimales los Gastos de Gestión y la Cantidad de costes compartidos.

**5.4.    Métodos de pago y repago de la Cantidad de costes compartidos al Conductor o el Precio a los Operadores de autobuses o las Empresas ferroviarias**

**5.4.1.    Instrucción del conductor (mandato)**

Al utilizar la Plataforma como Conductor para Trayectos de coche compartido, confirmas que has leído y aceptado [los términos y condiciones](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) del Proveedor de la Solución de Pago Hyperwallet, nuestro proveedor de pagos que procesa las transferencias de las Contribuciones de Costes a los conductores a través de la Solución de Pago Hyperwallet definidos en el apartado 5.4.2.a. Estas condiciones generales de uso se refieren más adelante como “CGU de Hyperwallet”.

Ten en cuenta que las exclusiones de la Directiva (UE) 2015/2366 del Parlamento Europeo y del Consejo, de 25 de noviembre de 2015, sobre servicios de pago en el mercado interior enumeradas en el artículo 11.4, letras (i) y (ii), de las CGU de Hyperwallet no se aplican a los Miembros de la Plataforma como no profesionales.

Como Conductor, das instrucciones:

1. a BlaBlaCar para que proceda a ordenar al Proveedor de la Solución de Pago Hyperwallet que efectúe la transferencia de la Contribución de Costes, y 
2. al Proveedor de la Solución de Pago Hyperwallet para que realice la transferencia de la Contribución de Costes a tu cuenta bancaria o de PayPal en tu nombre y por tu cuenta, de acuerdo con las CGU de Hyperwallet.

En el contexto de un Trayecto de coche compartido y tras la aceptación automática o manual de la Reserva, la cantidad total pagada por el Pasajero (Gastos de Gestión y Cantidad de costes compartidos) se mantiene en una cuenta de depósito en garantía gestionada por el Proveedor de la Solución de Pago Hyperwallet.

**5.4.2.    Pago de la Cantidad de costes compartidos al Conductor**

Tras el Trayecto de coche compartido, los Pasajeros tendrán un período de 24 horas  para presentar una reclamación a BlaBlaCar sobre el Trayecto. En ausencia de reclamación de los Pasajeros durante este período, BlaBlaCar deberá considerar el Trayecto como confirmado.

A partir del momento de esta confirmación expresa o tácita, tendrás, como Conductor, un crédito a ser abonado en tu Cuenta. Este crédito se corresponde a la cantidad total pagada por el Pasajero en el momento de confirmar la Reserva tras haberle restado los Gastos de Gestión, es decir, la Cantidad de costes compartidos pagada por el Pasajero.

**a. Solución de Pago Hyperwallet**

La Solución de Pago Hyperwallet es un servicio de pago proporcionado por el Proveedor de la Solución de Pago Hyperwallet. Para evitar cualquier duda, BlaBlaCar no presta ningún servicio de procesamiento de pagos para los Miembros y no entra en posesión de los fondos de los Miembros.

Cuando el Pasajero confirma el Trayecto de coche compartido, tú, como Conductor, tienes la opción de recibir tu Contribución de Costes en tu cuenta bancaria o en tu cuenta de PayPal.

Para ello, puedes utilizar la Solución de Pago Hyperwallet suscribiendo un contrato directamente con el Proveedor de la Solución de Pago Hyperwallet y aceptando las CGU de Hyperwallet.

A los efectos de recibir la primera transferencia de la Contribución de Costes, se te pedirá que selecciones un método de pago y facilites los datos de tu cuenta bancaria, dirección postal y fecha de nacimiento, o que conectes tu cuenta de PayPal. Si no lo haces, o si la información que facilitas es incorrecta, inexacta o no está actualizada, no se transferirá la Contribución de Costes.

La transferencia será realizada por el Proveedor de la Solución de Pago Hyperwallet.

Al final de los plazos de prescripción aplicables, tal y como se definen en las leyes aplicables, se considerará que cualquier cantidad de la Contribución de Costes que no hayas reclamado como Conductor a través de la Solución de Pago Hyperwallet pertenece a BlaBlaCar. 

**b. Comprobaciones de KYC**

Al utilizar la Solución de Pago Hyperwallet, aceptas que puedes estar sujeto a procedimientos normativos aplicados por el Proveedor de la Solución de Pago de acuerdo con [sus condiciones generales de uso](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml).

Estos procedimientos podrán incluir comprobaciones de identidad y otros requisitos del procedimiento “Conozca a su Cliente” (KYC) basados en los criterios establecidos por el Proveedor de la Solución de Pago Hyperwallet. Estos criterios podrán incluir umbrales financieros dependiendo de la cantidad total de pagos de Contribución de Costes que realices. 

A efectos de comprobación de KYC, quizá se te pida que proporciones la información adicional que solicite el Proveedor de la Solución de Pago Hyperwallet. Si no proporcionas los documentos requeridos, no podrás recibir la Contribución de Costes hasta que se confirme tu identidad.

Es posible que se den situaciones en las que el Proveedor de la Solución de Pago Hyperwallet deba, de conformidad con las leyes aplicables, suspender la transferencia de la Contribución de Costes o el acceso a la Solución de Pago. BlaBlaCar no tiene ninguna influencia sobre dichas acciones del Proveedor de la Solución de Pago Hyperwallet.

Las comprobaciones de KYC llevadas a cabo por el Proveedor de la Solución de Pago Hyperwallet son distintas e independientes de las comprobaciones que BlaBlaCar pueda llevar a cabo en relación con el uso de la Plataforma de conformidad con estas Condiciones y la Política de privacidad.

**5.4.3 Pago del Precio al Operador de autobuses o del Billete de Tren a BlaBlaCar**

El pago de cualquier Pedido realizado a través de la Plataforma se realiza por uno de los medios autorizados a continuación. El Cliente tiene la opción de guardar la información relacionada con una o más tarjetas de crédito en su Cuenta de cliente para que no tenga que introducir sistemáticamente esta información al realizar pagos posteriores.

Los métodos de pago pueden variar dependiendo de la plataforma BlaBlaCar que utilices. Los métodos de pago autorizados son los siguientes: 

* Tarjeta de crédito bancaria (las marcas aceptadas se muestran en la plataforma. En el caso de que tengas una tarjeta de marca compartida, podrás seleccionar una marca específica en la fase de pago) 
* PayPal
* Vales
* iDEAL
* Apple Pay
* Google Pay

No se emitirá confirmación de Pedido antes del pago completo y efectivo del precio de los Servicios de transporte seleccionados por el Cliente. Si el pago es irregular, incompleto o no se realiza por cualquier motivo atribuible al Cliente, el Pedido se cancelará inmediatamente.

El Pedido se confirma enviando un correo electrónico al Cliente con los detalles del Pedido.

Se invita al Pasajero a comprobar la configuración de la bandeja de entrada de su dirección de correo electrónico y, en especial, a asegurarse de que el correo electrónico de confirmación no se envía directamente al Correo no deseado.

La Confirmación del Pedido es definitiva. En consecuencia, cualquier cambio dará lugar a un intercambio o cancelación en las condiciones de los Términos y condiciones generales de ventas aplicables. Es responsabilidad del Cliente asegurarse de que los Servicios de transporte se elijan de acuerdo con las necesidades y expectativas del Pasajero. La exactitud de la Información personal introducida por el Cliente es responsabilidad de este último, excepto en el caso de que BlaBlaCar haya demostrado que tuvo un fallo en la recopilación, el almacenamiento o la protección de dicha Información personal.

**6\. Objetivo no comercial y no empresarial de los Servicios y la Plataforma**

Aceptas utilizar los Servicios y la Plataforma solamente para ponerte en contacto, con fines no comerciales y no empresariales, con personas que desean compartir un Trayecto de coche compartido contigo o reservar un Asiento en el contexto de un Trayecto de autobús o un Trayecto en tren.

En el contexto del Trayecto en coche compartido, reconociste que los derechos de los consumidores derivados de la legislación de la Unión en materia de protección del consumidor no se aplican a su relación con otros Usuarios.

Como Conductor, aceptas no solicitar una Cantidad de costes compartidos superior a los costes reales del trayecto y que puedan generarte un beneficio, dejando especificado que, en el contexto de compartir un coste, tú tienes que asumir como Conductor tu propia parte de los costes incurridos en el Trayecto en coche compartido. Tú eres el único responsable de calcular los costes en los que incurres durante el Trayecto en coche compartido y de asegurarte de que la cantidad solicitada a tus Pasajeros no supera los costes reales del trayecto (excluyendo tu parte de los costes), en particular haciendo referencia a la escala impositiva aplicable**_._**

BlaBlaCar se reserva el derecho a suspender tu Cuenta en caso de que utilices un vehículo de alquiler con conductor u otro vehículo comercial, taxi o vehículo de empresa con el objetivo de generar un beneficio a través de la Plataforma. Recuerda proporcionar a BlaBlaCar, en caso de que te lo solicite, una copia del certificado de registro de tu vehículo y cualquier otra información que demuestre que estás autorizado a utilizar el vehículo en cuestión en la Plataforma y que no estás obteniendo beneficio alguno por el uso de la misma.

Con arreglo a la sección 9 de las presentes CGU, BlaBlaCar también se reserva el derecho a suspender tu Cuenta, limitar tu acceso a los Servicios o resolver los presentes CGU en caso de que tu actividad en la Plataforma, dada la naturaleza de los Trayectos ofrecidos, tu frecuencia, el número de Pasajeros transportados y la Cantidad de costes compartidos solicitada, te conlleve a una posición en la que generes beneficio, o por cualquier otro motivo que sugiera a BlaBlaCar que estás generando beneficio a través de la Plataforma.

**7\. Política de cancelación**

**7.1.    Términos del pago en caso de cancelación**

Solo los Trayectos de coche compartido están sujetos a esta política de cancelación; BlaBlaCar no ofrece garantía alguna en caso de cancelación por cualquier motivo por parte de un Conductor o Pasajero. En el contexto del Trayecto en autobús, la política de cancelación está sujeta a los Términos y condiciones generales de venta de los Operadores de autobuses o a las CGV de tren. Se invita al usuario a consultar los Términos y condiciones generales de venta o las CGV de tren correspondientes para aceptar las condiciones de intercambio y cancelación de su Trayecto.

La cancelación de una Plaza en un Trayecto  de coche compartido por parte del Conductor o el Pasajero una vez Confirmada la reserva, estará sujeta a lo siguiente:

–    En caso de que la cancelación la realice el Conductor, al Pasajero se le devolverá la suma total que haya pagado (es decir, la Cantidad de costes compartidos y los Gastos de Gestión relacionadas). Fundamentalmente se trata de los casos en los que el Conductor cancela el viaje o no llega al punto de encuentro en un plazo de 15 minutos tras la hora acordada;

–    En caso de cancelación por parte del Pasajero:

* Si el Pasajero cancela con una antelación superior a 24 horas antes de la hora de salida programada y mencionada en el Anuncio de coche compartido, al Pasajero solamente se le devolverá la Cantidad de costes compartidos. Los Gastos de Gestión se las quedará BlaBlaCar y el Conductor no tendrá derecho a recibir ninguna suma de cualquier naturaleza;
* Si el Pasajero cancela el viaje con una antelación inferior a 24 horas antes de la hora de salida mencionada en el Anuncio  de coche compartido y una vez hubieran pasado 30 minutos tras la Confirmación de la reserva, el Pasajero tendrá derecho al reembolso de la mitad de la Cantidad de costes compartidos pagada en el momento de la realización de la Reserva. Los Gastos de Gestión se las quedará BlaBlaCar y al Conductor se le pagará el 50 % de la Contribución de Costes a través de la Solución de Pago Hyperwallet;
* Si el Pasajero cancela con una antelación inferior a 24 horas antes de la hora de salida programada mencionada en el Anuncio de coche compartido, y en treinta minutos o menos tras la Confirmación de la reserva, al Pasajero se le devolverá la Cantidad total de gastos compartidos a través de la Solución de Pago Hyperwallet. Los Gastos de Gestión se las quedará BlaBlaCar y el Conductor no tendrá derecho a recibir ninguna suma de ninguna naturaleza;
* Si el Pasajero cancela una vez pasada la hora de salida programada mencionada en el Anuncio de coche compartido, o si no ha llegado al punto de encuentro en un período de 15 minutos tras la hora acordada, no tendrá derecho a ningún reembolso. El Conductor será compensado con el coste total de la Cantidad de costes compartidos a través de la Solución de Pago Hyperwallet y los Gastos de Gestión se las quedará BlaBlaCar.

En caso de que la cancelación se produzca antes de la fecha de salida y por causa imputable al Pasajero, la Plaza o Plazas canceladas por el Pasajero quedarán automáticamente disponibles para otros Pasajeros, quienes podrán reservarlos en línea bajo las condiciones de los presentes CGU.

BlaBlaCar valorará, a su criterio y según la información disponible, la legitimidad de las solicitudes de reembolso.

**7.2.    Derecho de desistimiento**

De conformidad con lo establecido en el Artículo 103 a) de la Ley General para la Defensa de Consumidores y Usuarios, no tendrás derecho a desistimiento a partir del momento de entrega de la Confirmación de reserva, siempre y cuando se haya ejecutado en su totalidad el Contrato entre tú y BlaBlaCar, consistente en ponerse en contacto con otro Usuario.

**7.3 Política de cancelación y cambio de Trayectos en tren**

Los Billetes de tren no se pueden cambiar.

Las condiciones de cancelación varían en función de la Empresa ferroviaria. Se puede consultar la política de cancelación en los CGV de tren o en el [Centro de Ayuda](https://support.blablacar.com/hc/es).

**8\. Comportamiento de las personas que visitan la Plataforma y los Usuarios**

**8.1.    Compromiso de todas las personas que visitan la Plataforma**

Reconoces ser el único responsable de respetar todas las leyes, normativas y obligaciones aplicables a la utilización de la Plataforma.

Además, al utilizar la Plataforma y durante los Trayectos, te comprometes a:

(i) no utilizar la Plataforma con fines profesionales, comerciales o con ánimo de lucro, si no eres un Operador de autobuses o una Empresa ferroviaria;

(ii) no enviar a BlaBlaCar (especialmente en el momento de crear o actualizar tu Cuenta) ni a los demás Usuarios de la Plataforma, Operadores de autobuses o Empresas ferroviarias información falsa, confusa, maliciosa o fraudulenta;

(iii) no hablar ni comportarse, ni publicar ningún contenido (incluidos Mensajes) en la Plataforma que pueda resultar difamatorio, injurioso, obsceno, pornográfico, vulgar, ofensivo, agresivo, improcedente, violento, amenazante, acosador, de naturaleza racista o xenófoba, que tenga connotaciones sexuales, incite a la violencia, discriminación u odio, o que fomente actividades o el uso de substancias ilegales, o de forma más general el contenido illegal que sea contrario a la legislación aplicable, las presentes CGU y los objetivos de la Plataforma y que pueda infringir los derechos de BlaBlaCar o una tercera parte, o que pueda ser contrario a las buenas costumbres;

(iv) no infringir los derechos y no dañar la imagen de BlaBlaCar, en especial en lo que respecta a sus derechos de propiedad intelectual;

(v) no abrir más de una Cuenta en la Plataforma y no abrir una Cuenta en nombre de un tercero;

(vi) no intentar eludir el sistema de reserva en línea de la Plataforma, principalmente intentando enviar a otro Usuario o un Operador de autobuses tu información de contacto para realizar la reserva fuera de la Plataforma con el objetivo de evitar el pago de los Gastos de Gestión;

(vii) no ponerse en contacto con otro Usuario ni con un Operador de autobuses o Empresa ferroviaria, especialmente por medio de la Plataforma, con otra finalidad que no sea la definición de las condiciones del desplazamiento compartido;

(viii) no aceptar ni realizar el pago fuera de la Plataforma o de la solución de pago Hyperwallet;

(ix) cumplir con las presentes CGU.

**8.2.    Compromisos de los Conductores**

Cuando utilizas la Plataforma como Conductor, te comprometes a:

(i) respetar todas las leyes, normativas y códigos con respecto a la conducción y al vehículo, especialmente contar con un seguro de responsabilidad civil en vigor en el momento de realizar el Trayecto de coche compartido y estar en posesión de un carnet de conducir válido;

(ii) comprobar que tu póliza de seguro cubre los desplazamientos compartidos y que los Pasajeros con los que compartes el trayecto son considerados como terceros en tu vehículo y, por lo tanto, están cubiertos por tu seguro;

(iii) no asumir ningún riesgo durante la conducción, ni tomar ningún producto que pueda afectar negativamente a tu atención y a tu capacidad de conducir de manera atenta y completamente segura;

(iv) publicar Anuncios de coche compartido que se correspondan solamente con Trayectos ya programados;

(v) realizar el Trayecto de coche compartido según la descripción en el Anuncio (en especial con respecto a utilizar o no autopista) y a respetar las horas y lugares acordados con los demás Usuarios (en especial el punto de encuentro y el lugar de destino);

(vi) no llevar a más Pasajeros que el número de Plazas indicados en el Anuncio de coche compartido;

(vii) utilizar un vehículo en buen estado y que cumpla con todas las normativas legales, en especial con el Certificado de ITV en vigor;

(viii) a compartir con BlaBlaCar o con cualquier Pasajero que lo solicite tu carnet de conducir, el certificado de registro de tu vehículo, tu póliza de seguro, el certificado de la ITV y cualquier otro documento que demuestre tu capacidad para utilizar el vehículo como Conductor en la Plataforma;

(ix) en caso de retraso o cambio en la hora o en el propio Trayecto, informar a tus Pasajeros sin demora;

(x) en el caso de un Trayecto transfronterizo, mantener y tener disponible para el Pasajero y para cualquier autoridad que lo solicite, un documento válido que muestre tu identidad y tu derecho a cruzar la frontera;

(xi) esperar a los Pasajeros en el punto de encuentro acordado al menos hasta 15 minutos después de la hora acordada;

(xii) no publicar ningún Anuncio de coche compartido relativo a un vehículo que no sea tuyo o para el cual no tengas permiso para utilizarlo como transporte compartido;

(xiii) asegurarte de que los Pasajeros pueden contactarte al número de teléfono registrado en tu perfil

(xiv) no generar ningún beneficio a través de la Plataforma;

(xv) no tener ninguna contraindicación ni incapacidad médica para conducir;

(xvi) comportarse de forma apropiada y responsable durante el Trayecto de coche compartido, de conformidad con la filosofía del desplazamiento compartido.

**8.3.    Compromisos de los Pasajeros**

**8.3.1  Para el Trayecto de coche compartido**

Cuando utilizas la Plataforma como Pasajero de un Trayecto de coche compartido, te comprometes a:

(i) adoptar un comportamiento apropiado durante el Trayecto de coche compartido de modo que no interrumpa la concentración o la conducción del Conductor ni la paz y tranquilidad del resto de los Pasajeros;

(ii) respetar el vehículo del Conductor y su limpieza;

(iii) en caso de retraso, informar al Conductor sin demora;

(iv) esperar al Conductor en el punto de encuentro acordado al menos hasta 15 minutos después de la hora acordada;

(v) comunicar a BlaBlaCar, o a cualquier Conductor que te lo solicite, tu documento de identidad o cualquier otro documento que pruebe tu identidad;

(vi) no transportar durante un Trayecto de coche compartido ningún artículo, sustancia o animal que pueda distraer la concentración y conducción del Conductor, o cuya naturaleza, posesión o transporte sea ilegal;

(vii) en el caso de un Trayecto de coche compartido transfronterizo, mantener y tener disponible para el Conductor y para cualquier autoridad que lo solicite, un documento válido que muestre tu identidad y tu derecho a cruzar la frontera;

(viii) asegurarse de que los Conductores pueden contactarte en el número de teléfono registrado en tu perfil, incluso en el punto de reunión.

En el caso de que hayas realizado una Reserva para una o más Plazas en nombre de terceros, de conformidad con lo establecido en el anterior Artículo 4.2.3, garantizas que dicho tercero respetará las condiciones establecidas en dicho Artículo y, en general, los presentes CGU. BlaBlaCar se reserva el derecho a suspender tu Cuenta, limitar tu acceso a los Servicios o rescindir las presentes CGU en caso de incumplimiento por parte del tercero en cuyo nombre has reservado la Plaza de conformidad con estas CGU.

**8.3.2 Para el Trayecto en autobús**

En el contexto del Trayecto en autobús, el Pasajero se compromete a cumplir los Términos y condiciones generales de venta del Operador de autobuses pertinente.

**8.3.3 Para el Trayecto en tren**

En el contexto del Trayecto en tren, el Pasajero se compromete a cumplir las CGV de tren pertinentes.

**8.4. Notificación de contenidos inapropiados o ilegales (mecanismo de notificación y acción)**

Puedes denunciar un Contenido de los Miembros o un Mensaje sospechoso, inapropiado o ilegal tal y como se describe [aquí](https://support.blablacar.com/hc/es/articles/16765207097117-C%C3%B3mo-denunciar-contenido-ilegal).

BlaBlaCar, tras haber sido debidamente notificado con arreglo a esta sección o por las autoridades competentes, eliminará rápidamente cualquier Contenido de Miembro ilegal si:

* el Contenido de los Miembros es manifiestamente ilegal o contrario a la normativa aplicable; o 
* BlaBlaCar considera que dicho contenido infringe estos CGU.

En tales casos, BlaBlaCar se reserva el derecho a eliminar el Contenido del Miembro que haya sido debidamente denunciado, de forma suficientemente detallada y clara, y/o a suspender inmediatamente la Cuenta denunciada.

El Miembro en cuestión puede apelar las decisiones anteriores de BlaBlaCar tal y como se describe en la sección 15.1 a continuación.

**9\. Restricciones relacionadas con el uso de la Plataforma, suspensión de cuentas, limitación de acceso y rescisión**

Puedes rescindir tus relaciones contractuales con BlaBlaCar, en cualquier momento, de forma gratuita y sin tener que explicar el motivo. Para ello, simplemente debes ir a la pestaña “Darse de baja” en la página de tu Perfil.

En el caso de (i) incumplimiento por tu parte de las presentes CGU, incluidos, entre otros el incumplimiento de tus obligaciones como Usuario según lo establecido en los Artículos 6 y 8 de arriba, (ii) superación del límite establecido arriba en el Artículo 4.3.3, o (iii) en el caso de que BlaBlaCar tenga algún motivo real para creer que es necesario proteger su seguridad e integridad, la de los Usuarios o terceros, o para los fines de investigacioneso prevención de fraude, BlaBlaCar se reserva el derecho a:

(i)  rescindir las CGU que te vinculan con BlaBlaCar de forma inmediata y sin previo aviso; y/o

(ii)  evitar la publicación, o eliminación, de Contenidos de Miembros; y/o

(iii)    limitarte el acceso y el uso de la Plataforma; y/o

(iv)    suspender de forma temporal o permanente tu Cuenta.

La suspensión de la cuenta también puede significar, en su caso, que no recibirá sus pagos pendientes.  
Cuando corresponda, BlaBlaCar también podrá enviarte una advertencia recordándote la obligación de cumplir con las leyes aplicables y/o las presentes CGU.

Cuando sea necesario, se te notificará el establecimiento de dichas medidas para que puedas confirmar tu compromiso de adherirte a estos CGU y a la legislación aplicable, dar explicaciones a BlaBlaCar o impugnar su decisión. BlaBlaCar decidirá, teniendo en cuenta todas las circunstancias de cada caso y la gravedad de la infracción, a su discreción, si levantar o no la rescisión aplicada.

**10\. Datos de carácter personal**

En el contexto del uso de la Plataforma por tu parte, BlaBlaCar recopilará y procesará tu información personal tal y como se describe en su [Política de Privacidad](https://www.blablacar.es/about-us/privacy-policy).

**11\. Propiedad intelectual**

**11.1.    Contenido publicado por BlaBlaCar**

Con sujeción al contenido proporcionado por sus Usuarios, BlaBlaCar es el titular único de todos los derechos de propiedad intelectual relacionados con el Servicio, la Plataforma, su contenido (en concreto los textos, imágenes, diseños, logotipos, vídeos, sonido, datos y gráficos) y con el software y las bases de datos que aseguran su funcionamiento.

BlaBlaCar te garantiza un derecho personal, no exclusivo y no transferible para utilizar la Plataforma y los Servicios, para tu uso personal y privado, con un objetivo no comercial y de conformidad con la finalidad de la Plataforma y los Servicios.

Está totalmente prohibido utilizar o explotar la Plataforma y los Servicios, incluido su contenido, con cualquier otra finalidad que no sea la prevista sin previo consentimiento por escrito de BlaBlaCar. En concreto, está totalmente prohibido lo siguiente:

(i) reproducir, modificar, adaptar, distribuir, representar públicamente y diseminar la Plataforma, los Servicios y todo su contenido, salvo previa autorización expresa de BlaBlaCar;

(ii) descompilar y aplicar ingeniería inversa a la Plataforma o los Servicios, salvo las excepciones estipuladas por los textos aplicables;

(iii) extraer o intentar extraer (en concreto utilizando robots de extracción de datos o herramientas similares de recolección de datos) una parte sustancial de los datos de la Plataforma.

**11.2.    Contenido publicado por el Usuario en la Plataforma**

Con el objetivo de permitir la provisión de los Servicios, y en cumplimiento con el objetivo de la Plataforma, le otorgas a BlaBlaCar una licencia no exclusiva para usar el contenido y los datos que has proporcionado en el contexto de la utilización de los Servicios que podrá incluir tus solicitudes de Reserva, Anuncios de coches compartidos y comentarios en ellos, biografías, fotos, opiniones y respuestas a las opiniones (de aquí en adelante denominados como el “**Contenido desarrollado por el Usuario**”) y Mensajes. Para poder permitir a BlaBlaCar distribuir por medio de la red digital y de conformidad con cualquier protocolo de comunicación (en especial Internet y las redes móviles) y para poder ofrecer al público el contenido de la Plataforma, autorizas a BlaBlaCar a reproducir, representar, adaptar y traducir tu Contenido desarrollado por el Usuario para todo el mundo y durante la duración completa de tus relaciones contractuales con BlaBlaCar, de conformidad con lo siguiente:

(i) autorizas a BlaBlacar a reproducir todo o parte de tu Contenido desarrollado por el Usuario en cualquier medio de grabación digital, conocido o por conocer, en concreto en cualquier servidor, disco duro, tarjeta de memoria u otro medio equivalente, en cualquier formato y por medio de cualquier proceso, ya conocido o por conocer, en el grado necesario para cualquier operación de almacenamiento, copia de seguridad, transmisión o descarga relacionada con el funcionamiento de la Plataforma y el suministro del Servicio;

(ii) autorizas a BlaBlaCar a adaptar y traducir su Contenido desarrollado por el Usuario, y a reproducir dichas adaptaciones en cualquier medio digital, conocido o por conocer, establecido en el punto (i) arriba, con el objetivo de suministrar los Servicios en diferentes idiomas. Este derecho incluye la opción de realizar modificaciones en el formato de tu Contenido desarrollado por el Usuario, con respecto a tu derecho moral, con el objetivo de respetar el diseño gráfico de la Plataforma o para hacer que tu Contenido sea compatible a nivel técnico con nuestro software para su posterior publicación por medio de la Plataforma.

Sigues siendo responsable y tienes todos los derechos sobre el Contenido de los Miembros y los Mensajes que subas en nuestra Plataforma.

**12\. Función de BlaBlaCar**

La Plataforma constituye una plataforma de movilidad de red en línea en la cual los Usuarios pueden crear y publicar Anuncios de coche de compartido de Trayectos de coche compartido con el objetivo de compartir los mismos y los costes asociados al Trayecto. Estos Anuncios de coche compartido serán visualizados por los demás Usuarios para comprobar las condiciones del Trayecto, y, cuando corresponda, para reservar directamente una Plaza en el vehículo en cuestión con el Usuario que ha publicado el Anuncio en la Plataforma. La Plataforma  también la reserva de Asientos para Trayectos en autobús o Trayectos en tren.

Al utilizar la Plataforma y aceptar las presentes CGU, reconoces que BlaBlaCar no es parte de ningún acuerdo formalizado entre tú y los demás Usuarios con el objetivo de compartir los costes relacionados con un Trayecto ni ningún acuerdo realizado entre tú y un Operador de autobuses o una Empresa ferroviaria.

BlaBlaCar no tiene ningún tipo de control sobre el comportamiento de sus Usuarios, los Operadores de autobuses, las Empresas ferroviarias ni sus representantes y los usuarios de la Plataforma. No posee, explota, suministra ni administra los vehículos incluidos en los Anuncios, y no ofrece Trayectos en la Plataforma.

Reconoces y aceptas que BlaBlaCar no controla la validez, veracidad o legalidad de los Anuncios, Plazas y Trayectos ofrecidos. En su capacidad de intermediario, BlaBlaCar no ofrece servicio de transporte ni actúa en calidad de transportista, la función de BlaBlaCar se limita a facilitar el acceso a la Plataforma.

En el contexto de Trayectos de coche compartido, los Usuarios (Conductores o Pasajeros) actúan bajo su única y total responsabilidad.

En su capacidad de intermediario, BlaBlaCar no será considerado responsable de ningún incidente que tenga lugar durante un Trayecto, en especial en lo relacionado con:

(i) información errónea comunicada por el Conductor, por el Operador de autobuses o la Empresa ferroviaria en su Anuncio o por otros medios , con respecto al Trayecto y sus términos;

(ii) la cancelación o modificación de un Trayecto por parte de un Usuario o de un Operador de autobuses o una Empresa ferroviaria;

(iii) el comportamiento de sus Usuarios durante, antes o después del Trayecto.

**13\. Funcionamiento, disponibilidad y funcionalidades de la Plataforma**

BlaBlaCar intentará en la medida de lo posible mantener el funcionamiento constante de la Plataforma los 7 días de la semana y las 24 horas del día. Sin embargo, es posible que se pueda suspender de forma temporal el acceso a la Plataforma, sin previo aviso, por motivos técnicos, de mantenimiento, migración u operaciones de actualización, o debido a cortes de suministro o restricciones relacionadas con el funcionamiento de la red.

Además, BlaBlaCar se reserva el derecho a modificar o suspender en su totalidad o en parte el acceso a la Plataforma o sus funcionalidades, a su discreción, bien sea de forma temporal o permanente.

**14\. Modificación de los CGU**

Estos CGU y los documentos integrados por referencia explícita forman el acuerdo completo entre tú y BlaBlaCar con relación a la utilización de los Servicios. Cualquier otro documento mencionado en la Plataforma (Preguntas frecuentes, por ejemplo), tiene solamente fines indicativos.

BlaBlaCar podrá modificar los presentes CGU para adaptarlos a su entorno tecnológico y comercial, y para cumplir con la legislación en vigor. Cualquier modificación realizada en los CGU será publicada en la Plataforma indicando la fecha de entrada en vigor, y recibirás una notificación de dicho cambio antes de la entrada en vigor del mismo.

**15\. Ley aplicable y litigios**

Estos CGU se han redactado en español, de conformidad con la legislación española.

**15.1 Sistema interno de tramitación de reclamaciones**

Puedes apelar las decisiones que podamos tomar en relación con:

* el Contenido desarollado por el Usuario por ejemplo, eliminamos, restringimos la visibilidad o nos negamos a eliminar cualquier Contenido desarollado por el Usuario que proporciones al utilizar la Plataforma, o bien

* tu Cuenta: suspendimos tu acceso a la Plataforma,

cuando tomamos tales decisiones basándonos en que el Contenido desarollado por el Usuario constituye contenido ilegal o es incompatible con las presentes CGV. El proceso de apelación se describe [aquí](https://support.blablacar.com/hc/es/articles/16767371351069-C%C3%B3mo-apelar-la-retirada-de-contenido-o-suspensi%C3%B3n-de-una-cuenta).

**15.2 Resolución extrajudicial de conflicto**

Si fuese necesario, también podrás presentar tus reclamaciones con respecto a nuestra Plataforma o Servicios en la plataforma de resolución de litigios en línea de la Comisión Europea, a la que podrás acceder desde el siguiente [enlace](https://webgate.ec.europa.eu/odr/main/?event=main.home.show&lng=ES). La Comisión Europea enviará tu reclamación al organismo nacional competente. En cumplimiento de la legislación aplicable al arbitraje, tienes la obligación de, antes de solicitar el arbitraje, comunicar a BlaBlaCar por escrito cualquier reclamación o litigio con el objetivo de obtener una solución amistosa.

También podrás presentar una solicitud ante un organismo de resolución de litigios de tu país (puedes encontrar la lista [aquí](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)).

**16\. Aviso legal**

La Plataforma está editada por Comuto SA, sociedad limitada, con un capital social de 154,912.382 EUR, registrada en el Registro Empresarial de París con el número 491.904.546 (Número de IVA: FR76491904546), teniendo su sede registrada en el N° 84, Avenue de la République, 75011 París (Francia), representada por su CEO, Nicolas Brusson, Director de la edición del Sitio Web. 

La Plataforma está alojada en los servidores de Google Limited Irlanda, cuya sede social se encuentra en Gordon House, Barrow Street, Dublín 4 (Irlanda).

Comuto SA está registrado en el registro de Operadores de Trayectos y Estancias con el siguiente número: IM075180037

La garantía financiera es proporcionada por: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 París – Francia.

El seguro de responsabilidad civil profesional está suscrito a: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 París – Francia.

Comuto SA está inscrita en el registro de Intermediarios de Seguros, Banca y Finanzas con el número de registro (Orias) 15003890.

Para cualquier pregunta, puede ponerse en contacto con Comuto SA utilizando este [formulario de contacto](https://support.blablacar.com/hc/es/requests/new?ticket_form_id=360000240999).

**17\. Reglamento de Servicios Digitales**

17.1.  Información sobre la media mensual de destinatarios activos del servicio en la Unión

De conformidad con el artículo 24, apartado 2, del Reglamento del Parlamento Europeo y del Consejo relativo al mercado interior de servicios digitales y por el que se modifica la Directiva 2000/31/CE (“DSA”), los proveedores de plataformas en línea deben publicar información sobre la media mensual de destinatarios activos de su servicio en la Unión, calculada como media de los últimos seis meses. El objetivo de esta publicación es determinar si un proveedor de plataformas en línea cumple el criterio de “plataformas en línea muy grandes” en virtud de la Ley de Servicios Digitales; es decir, superar el umbral de 45 millones de destinatarios activos mensuales medios en la Unión.

A 31 de enero de 2024, el número medio mensual de destinatarios activos de BlaBlaCar durante el período comprendido entre agosto de 2023 y enero de 2024, calculado teniendo en cuenta el considerando 77 y el artículo 3 de la DSA, antes de que se adopte una ley delegada específica, era de aproximadamente 4,82 millones en la UE.

Esta información se publica únicamente a efectos del cumplimiento de los requisitos de la DSA y no debe utilizarse para otros fines. Se actualizará al menos una vez cada seis meses. Nuestro enfoque para elaborar este cálculo puede evolucionar o requerir modificaciones a lo largo del tiempo, por ejemplo, debido a cambios en los productos o a nuevas tecnologías.

Esta información se publica únicamente a efectos del cumplimiento de los requisitos de la DSA y no debería utilizarse para otros fines. Se actualizará al menos una vez cada seis meses. Nuestro planteamiento para elaborar este cálculo puede evolucionar o requerir modificaciones a lo largo del tiempo, por ejemplo, debido a cambios en los productos o a nuevas tecnologías.

**17.2. Punto de contacto para las autoridades**

De conformidad con el artículo 11 de la DSA, si eres miembro de las autoridades competentes de la UE, la Comisión de la UE o el Consejo Europeo de Servicios Digitales, podrá ponerse en contacto con nosotros en relación con asuntos relativos a la DSA a través del correo electrónico [\[email protected\]](https://blog.blablacar.es/cdn-cgi/l/email-protection).

Puedes ponerse en contacto con nosotros en inglés y francés.

Ten en cuenta que esta dirección de correo electrónico no se utiliza para la comunicación con los miembros. Para cualquier cuestión relacionada con el uso de BlaBlaCar, en calidad de Miembro puedes ponerte en contacto con nosotros a través de este [formulario de contacto](https://support.blablacar.com/hc/es/requests/new?ticket_form_id=360000240999).

* * *