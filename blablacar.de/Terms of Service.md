**Ab 20. August 2024 geltende Allgemeine Geschäftsbedingungen**

**1\. Gegenstand**
------------------

Comuto SA (im Folgenden „**BlaBlaCar**“) hat eine Mitfahrplattform entwickelt, die auf einer Website unter der Adresse www.blablacar.de oder in Form einer mobilen Anwendung zugänglich ist und (i) darauf ausgelegt ist, Fahrer zu einem bestimmten Ziel zu bringen in Kontakt mit Fahrgästen, die in die gleiche Richtung fahren, um es ihnen zu ermöglichen, die Fahrt und damit die damit verbundenen Kosten zu teilen, und (ii) um Tickets für Busreisen zu buchen, die von von BlaBlaCar autorisierten Busunternehmen (im Folgenden als „**Plattform**“ bezeichnet durchgeführt werden).

Diese Allgemeinen Geschäftsbedingungen haben den Zweck, den Zugang und die Nutzungsbedingungen der Plattform zu regeln. Bitte lesen Sie diese sorgfältig durch. Sie verstehen und erkennen an, dass BlaBlaCar nicht an Vereinbarungen, Verträgen oder vertraglichen Beziehungen jeglicher Art beteiligt ist, die zwischen den Mitgliedern seiner Plattform oder mit einem Busunternehmen eingegangen werden.

Indem Sie auf „Mit Facebook anmelden“ oder „Mit einer E-Mail-Adresse registrieren“ klicken, erkennen Sie an, dass Sie alle diese allgemeinen Nutzungsbedingungen gelesen und akzeptiert haben.

Wenn Sie Ihr Konto verwenden, um sich bei der BlaBlaCar-Plattform eines anderen Landes (z. B. www.blablacar.com.br) anzumelden, beachten Sie bitte, dass (i) die Geschäftsbedingungen dieser Plattform (ii) die Datenschutzrichtlinie dieser Plattform und (iii) die gesetzlichen Regeln und Vorschriften dieses Landes gelten. Dies bedeutet auch, dass Informationen Ihres Kontos, einschließlich personenbezogener Daten, möglicherweise an die juristische Person übermittelt werden müssen, die diese andere Plattform betreibt. Bitte beachten Sie, dass sich BlaBlaCar das Recht vorbehält, den Zugriff auf die Plattform für Benutzer zu beschränken, von denen vernünftigerweise festgestellt werden kann, dass sie sich außerhalb des Hoheitsgebiets der Europäischen Union befinden.

**2\. Definitionen**
--------------------

In diesem Dokument,

„**AGB**“ bezeichnet diese Allgemeinen Geschäftsbedingungen;

„**Anbieter der** **Hyperwallet-Zahlungslösung**” bezeichnetdie PayPal (Europe) S.à r.l. et Cie, S.C.A.

„**Anzeige**“ bezeichnet eine Anzeige über eine Fahrt, die von einem Fahrer auf der Plattform gepostet wird;

„**Bestellung**“ bezeichnet den Vorgang, durch den der Kunde seine Dienste bei BlaBlaCar Bus bucht, unabhängig von den verwendeten Mitteln, mit der einzigen Ausnahme des Kaufs von Tickets direkt an einer Verkaufsstelle, und der die Verpflichtung für den Fahrgast oder möglicherweise eine nach sich zieht Agent zur Zahlung des Preises für die entsprechenden Dienstleistungen;

„**BlaBlaCar**“ hat die in Artikel 1 oben festgelegte Bedeutung;

„**Buchung**“ hat die in Artikel 4.2.1 festgelegte Bedeutung. unter;

„**Buchungsbestätigung**“ hat die in Artikel 4.2.1 unten angegebene Bedeutung;

„**Busbetreiber**“ bezeichnet ein Unternehmen, das gewerbsmäßig Fahrgäste befördert und für dessen Fahrten Tickets auf der Plattform von BlaBlaCar vertrieben werden;

„**Busfahrt**“ bezeichnet die Fahrt, die Gegenstand einer Buswerbung auf der Plattform ist und für die der Busbetreiber im Austausch für den Preis Sitzplätze in den Busfahrzeugen vorschlägt;

„**Buswerbung**“ bezeichnet eine auf der Plattform eingestellte Anzeige für eine Busfahrt einer Busbetreiber;

„**Dienste**“ bezeichnet alle Dienste, die von BlaBlaCar über die Plattform bereitgestellt werden;

„**Etappe**“ hat die in Artikel 4.1 unten angegebene Bedeutung;

„**Facebook-Konto**“ hat die in Artikel 3.2 unten angegebene Bedeutung;

„**Fahrer**“ bezeichnet das Mitglied, das die Plattform nutzt, um anzubieten, eine andere Person gegen den Kostenbeitrag, für eine Fahrt und zu einem vom Fahrer allein festgelegten Zeitpunkt zu befördern;

„**Fahrgemeinschaftsfahrt**“ bezeichnet die Fahrt, die Gegenstand einer Fahrgemeinschaftsanzeige ist, die von einem Fahrer auf der Plattform veröffentlicht wird und für die er sich bereit erklärt, Fahrgäste gegen den Kostenbeitrag zu befördern;

„**Fahrt**“ bezieht sich unterschiedslos auf eine Busfahrt oder eine Fahrgemeinschaftsfahrt;

„**Hyperwallet-Zahlungslösung**” hat die Bedeutung, die ihm im untenstehenden Artikel 5.4.2.a zugewiesen wird;

„**Kunde**“ bezeichnet jede natürliche Person (Mitglied oder nicht), die entweder für sich selbst oder im Namen einer anderen Person, die der Fahrgast sein wird, ein Busticket über die Plattform kauft, um eine vom Busunternehmen durchgeführte Fahrt zu realisieren;

„**Konto**“ bezeichnet das Konto, das erstellt werden muss, um Mitglied zu werden und auf bestimmte von der Plattform angebotene Dienste zuzugreifen;

„**Kostenbeitrag**“ bezeichnet für eine bestimmte Fahrgemeinschaftsfahrt den Geldbetrag, der vom Fahrer angefordert und vom Fahrgast für seinen Beitrag zu den Reisekosten akzeptiert wird;

„**Mitfahrgelegenheitsanzeige**“ bezeichnet eine Anzeige für eine Fahrt, die von einem Fahrer auf der Plattform gepostet wird;

„**Mitglied**“ bezeichnet jede Person, die ein Konto auf der Plattform erstellt hat;

„**Mitgliederinhalt**“ hat die ihm in Artikel 11.2 unten zugewiesene Bedeutung;

„**Passagier**“ bezeichnet das Mitglied, das das Angebot angenommen hat, vom Fahrer befördert zu werden, oder gegebenenfalls die Person, in deren Namen ein Mitglied einen Sitzplatz gebucht hat;

„**Plattform**“ hat die in Artikel 1 oben festgelegte Bedeutung;

„**Preis**“ bezieht sich für eine bestimmte Busfahrt auf den Preis einschließlich aller Steuern, Gebühren und Kosten für relevante Dienstleistungen, die vom Kunden auf der Plattform zum Zeitpunkt der Bestätigung der Bestellung für einen Sitzplatz auf einem bestimmten Preis bezahlt werden Busfahrt;

„**Servicegebühr**“ hat die Bedeutung, die ihr in nachstehendem Artikel 5.2 zugewiesen wird.

„**Sitzplatz**“ bezeichnet den von einem Fahrgast gebuchten Sitzplatz im Fahrzeug des Fahrers oder in dem vom Busunternehmen durchgeführten Fahrzeug;

„**Ticket**“ bezeichnet den nominellen gültigen Beförderungstitel, der dem Verbraucher nach der Buchung einer Busfahrt als Nachweis des bestehenden Beförderungsvertrags zwischen den Fahrgästen und dem Busunternehmen, der den AGB des Verkaufs unterliegt, erteilt wird, unbeschadet etwaiger zusätzlich festgelegter besonderer Bedingungen zwischen dem Fahrgast und dem Busunternehmen und auf dem Ticket angegeben;

„**Transportdienste**“ bezieht sich auf die Transportdienste, die von einem Busreisenden abonniert und vom Busunternehmen bereitgestellt werden;

„**Verkaufsstelle**“ bezieht sich auf die physischen Schalter und Terminals, die auf der Website aufgeführt sind und an denen die Tickets zum Verkauf angeboten werden;

„**Verkaufsbedingungen**“ bezeichnet die Allgemeinen Verkaufsbedingungen des betreffenden Busunternehmens in Abhängigkeit von der vom Kunden ausgewählten Busreise und die besonderen Geschäftsbedingungen, die auf der Website verfügbar sind und vom Kunden als gelesen anerkannt wurden vor der Bestellung;

„**Website**“ bezeichnet die unter der Adresse www.blablacar.de erreichbare Website.

**3\. Registrierung auf der Plattform und Erstellung eines Kontos**
-------------------------------------------------------------------

### **3.1. Registrierungsbedingungen auf der Plattform**

Die Plattform darf von Personen ab 18 Jahren genutzt werden. Die Registrierung auf der Plattform durch Minderjährige ist strengstens untersagt. Durch den Zugriff auf, die Nutzung oder Registrierung auf der Plattform versichern und garantieren Sie, dass Sie mindestens 18 Jahre alt sind.

### **3.2. Erstellung eines Kontos**

Die Plattform ermöglicht es Mitgliedern, Fahrgemeinschaftsanzeigen zu posten und anzusehen und miteinander zu interagieren, um einen Sitzplatz zu buchen. Sie können die Anzeigen sehen, wenn Sie nicht auf der Plattform registriert sind. Sie können jedoch keine Fahrgemeinschaftsanzeige aufgeben oder einen Sitzplatz buchen, ohne zuvor ein Konto erstellt und Mitglied zu werden.

Um Ihr Konto zu erstellen, können Sie:

(i) entweder alle Pflichtfelder auf dem Registrierungsformular ausfüllen;  
(ii) oder melden Sie sich über unsere Plattform bei Ihrem Facebook-Konto an (im Folgenden als Ihr „Facebook-Konto“ bezeichnet). Durch die Nutzung dieser Funktionen erklären Sie sich damit einverstanden, dass BlaBlaCar auf Ihr Facebook-Konto zugreifen, auf der Plattform veröffentlichen und bestimmte Informationen von Ihrem Facebook-Konto aufbewahren wird. Sie können die Verknüpfung zwischen Ihrem Konto und Ihrem Facebook-Konto jederzeit über den Abschnitt „Verifizierungen“ Ihres Profils löschen. Wenn Sie mehr über die Verwendung Ihrer Daten aus Ihrem Facebook-Konto erfahren möchten, lesen Sie unsere Datenschutzrichtlinie und die von Facebook.

Um sich auf der Plattform zu registrieren, müssen Sie diese AGB gelesen und akzeptiert haben.

Bei der Erstellung Ihres Kontos verpflichten Sie sich unabhängig von der gewählten Methode, genaue und wahrheitsgemäße Informationen anzugeben und diese über Ihr Profil oder durch Benachrichtigung von BlaBlaCar zu aktualisieren, um ihre Relevanz und Genauigkeit während Ihrer gesamten Vertragsbeziehung mit BlaBlaCar zu gewährleisten.

Im Falle einer Registrierung per E-Mail erklären Sie sich damit einverstanden, das bei der Erstellung Ihres Kontos gewählte Passwort geheim zu halten und es niemandem mitzuteilen. Wenn Sie Ihr Passwort verlieren oder offenlegen, verpflichten Sie sich, BlaBlaCar unverzüglich zu informieren. Sie allein sind für die Nutzung Ihres Kontos durch Dritte verantwortlich, es sei denn, Sie haben BlaBlaCar ausdrücklich über den Verlust, die betrügerische Nutzung durch Dritte oder die Weitergabe Ihres Passworts an Dritte informiert.

Sie stimmen zu, keine anderen als die ursprünglich erstellten Konten unter Ihrer eigenen Identität oder der eines Dritten zu erstellen oder zu verwenden.

### **3.3. Überprüfung**

BlaBlaCar kann zum Zwecke der Transparenz, der Verbesserung des Vertrauens oder der Verhinderung oder Aufdeckung von Betrug ein System zur Überprüfung einiger der Informationen einrichten, die Sie in Ihrem Profil angeben. Dies ist insbesondere dann der Fall, wenn Sie Ihre Telefonnummer eingeben oder uns ein Ausweisdokument übermitteln.

Sie erkennen an und akzeptieren, dass jeder Verweis auf der Plattform oder den Diensten auf „verifizierte“ Informationen oder ähnliche Begriffe nur bedeutet, dass ein Mitglied das auf der Plattform oder den Diensten bestehende Verifizierungsverfahren erfolgreich bestanden hat, um Ihnen weitere Informationen zur Verfügung zu stellen über das Mitglied, mit dem Sie reisen möchten. BlaBlaCar kann die Wahrhaftigkeit, Zuverlässigkeit oder Gültigkeit der Informationen, die Gegenstand des Überprüfungsverfahrens sind, nicht garantieren.

**4\. Nutzung der Dienste**
---------------------------

### **4.1. Schalten von Anzeigen**

Als Mitglied können Sie unter der Voraussetzung, dass Sie die nachstehenden Bedingungen erfüllen, Mitfahrgelegenheitsanzeigen auf der Plattform erstellen und veröffentlichen, indem Sie Informationen über die Mitfahrgelegenheit eingeben, die Sie unternehmen möchten (Daten/Zeiten und Sammelstellen und Ankunft, Anzahl der angebotenen Sitzplätze, verfügbare Optionen, Höhe des Kostenbeitrags etc.).

Wenn Sie Ihre Mitfahrgelegenheitsanzeige aufgeben, können Sie die Meilensteinstädte angeben, in denen Sie zustimmen, anzuhalten, Passagiere abzuholen oder abzusetzen. Die Abschnitte der Mitfahrgelegenheit zwischen diesen Meilensteinstädten oder zwischen einer dieser Meilensteinstädte und dem Sammelpunkt oder Zielort der Mitfahrgelegenheit bilden „Etappen“.

Sie sind nur berechtigt, eine Anzeige zu schalten, wenn Sie alle folgenden Bedingungen erfüllen:

(i) Sie besitzen einen gültigen Führerschein;

(ii) Sie bieten Fahrgemeinschaftsanzeigen nur für Fahrzeuge an, die Sie besitzen oder mit ausdrücklicher Genehmigung des Eigentümers nutzen, und in allen Fällen, zu deren Nutzung Sie zum Zweck der Mitfahrgelegenheit berechtigt sind;

(iii) Sie sind und bleiben der Hauptfahrer des Fahrzeugs, das Gegenstand der Anzeige ist;

(iv) das Fahrzeug hat eine gültige Haftpflichtversicherung;

(v) Sie haben keine Kontraindikation oder medizinische Unfähigkeit zum Fahren;

(vi) das Fahrzeug, das Sie für die Reise verwenden möchten, ein Tourenwagen mit 4 Rädern und maximal 7 Sitzen ist, mit Ausnahme von Autos, die keinen Führerschein benötigen;

(vii) Sie beabsichtigen nicht, eine weitere Anzeige für dieselbe Mitfahrgelegenheit auf der Plattform zu veröffentlichen;

(viii) Sie bieten nicht mehr Sitzplätze an, als in Ihrem Fahrzeug verfügbar sind;

(ix) alle angebotenen Sitze haben einen Sicherheitsgurt, auch wenn das Fahrzeug mit Sitzen ohne Sicherheitsgurt zugelassen ist;

(x) ein Fahrzeug in einwandfreiem Zustand zu verwenden, das den geltenden gesetzlichen Bestimmungen und Gepflogenheiten entspricht, insbesondere mit einem aktuellen TÜV-Zertifikat;

(xi) Sie sind Verbraucher und handeln nicht als Gewerbetreibender.

Sie erkennen an, dass Sie allein für den Inhalt der Mitfahrgelegenheitsanzeige verantwortlich sind, die Sie auf der Plattform posten. Folglich versichern und garantieren Sie die Genauigkeit und Wahrhaftigkeit aller in Ihrer Mitfahrgelegenheitsanzeige enthaltenen Informationen, und Sie verpflichten sich, die Mitfahrgelegenheitsfahrt unter den in Ihrer Mitfahrgelegenheitsanzeige beschriebenen Bedingungen durchzuführen.

Ihre Mitfahrgelegenheitsanzeige wird auf der Plattform veröffentlicht und ist daher für Mitglieder und alle Besucher, auch Nichtmitglieder, sichtbar, die eine Suche auf der Plattform oder auf der Website der Partner von BlaBlaCar durchführen. BlaBlaCar behält sich das Recht vor, nach eigenem Ermessen eine Mitfahrgelegenheitsanzeige nicht zu veröffentlichen oder zu entfernen, wenn diese nicht den AGB entspricht oder es sie als sein Image, das der Plattform oder der Dienste schädigend betrachtet bzw. das Konto des Mitglieds, das eine solche Mitfahrgelegenheitsanzeige gemäß Abschnitt 9 dieser AGB veröffentlicht zu sperren.

Sie werden darüber informiert, dass Sie für den Fall, dass Sie sich durch die Nutzung der Plattform als Verbraucher ausgeben, obwohl Sie tatsächlich als Gewerbetreibender handeln, Sanktionen ausgesetzt sind, die durch das geltende Recht vorgesehen sind.

### **4.2. Sitzplatz reservieren**

BlaBlaCar hat ein System zur Online-Buchung von Sitzplätzen („Buchung“) für auf der Plattform angebotene Fahrten eingerichtet.

Die Buchungsmethoden für einen Sitzplatz hängen von der Art der geplanten Reise ab.

BlaBlaCar stellt den Nutzern auf der Plattform eine Suchmaschine zur Verfügung, die auf verschiedenen Suchkriterien basiert (Ursprungsort, Ziel, Daten, Anzahl der Reisenden usw.). Bestimmte zusätzliche Funktionalitäten werden auf der Suchmaschine bereitgestellt, wenn der Benutzer mit seinem Konto verbunden ist. BlaBlaCar lädt den Benutzer ein, unabhängig vom verwendeten Buchungsverfahren, die Suchmaschine sorgfältig zu konsultieren und zu verwenden, um das Angebot zu ermitteln, das seinen Bedürfnissen am besten entspricht. [Hier](https://blog.blablacar.de/about-us/transparenz-der-plattformen) können weitere Informationen gefunden werden. Der Kunde, der eine Busfahrt an einer Verkaufsstelle bucht, kann auch den Busbetreiber oder den Schaltermitarbeiter bitten, die Suche durchzuführen.

### **4.2.1 Mitfahrgelegenheit**

Wenn ein Passagier an einer Anzeige für Mitfahrgelegenheiten interessiert ist, die von der Buchung profitiert, kann er eine Online-Buchungsanfrage stellen. Diese Buchungsanfrage wird entweder (i) automatisch akzeptiert (wenn der Fahrer diese Option beim Aufgeben seiner Anzeige ausgewählt hat) oder (ii) manuell vom Fahrer akzeptiert. Zum Zeitpunkt der Buchung zahlt der Passagier online den Kostenbeitrag und gegebenenfalls die Servicegebühr. Nach Eingang der Zahlung bei BlaBlaCar und ggf. Validierung der Buchungsanfrage durch den Fahrer erhält der Fahrgast eine Buchungsbestätigung (die „Buchungsbestätigung“).

Wenn Sie ein Fahrer sind und sich entschieden haben, die Buchungsanfragen beim Posten Ihrer Mitfahrgelegenheitsanzeige manuell zu bearbeiten, müssen Sie auf jede Buchungsanfrage innerhalb des vom Fahrgast während der Buchungsanfrage angegebenen Zeitraums antworten. Andernfalls erlischt die Buchungsanfrage automatisch und dem Passagier werden alle zum Zeitpunkt der Buchungsanfrage gezahlten Beträge zurückerstattet, sofern zutreffend.

Zum Zeitpunkt der Buchungsbestätigung sendet Ihnen BlaBlaCar die Telefonnummer des Fahrers (wenn Sie der Beifahrer sind) oder des Beifahrers (wenn Sie der Fahrer sind), wenn das Mitglied der Anzeige seiner Telefonnummer zugestimmt hat. Sie sind dann allein dafür verantwortlich, den Vertrag auszuführen, der Sie an das andere Mitglied bindet.

### **4.2.2 Busfahrt**

Für Busfahrten ermöglicht BlaBlaCar die Buchung von Bustickets für eine bestimmte Busfahrt über die Plattform.

Die Transportleistungen unterliegen den Verkaufsbedingungen des jeweiligen Busunternehmens, abhängig von der vom Kunden gewählten Fahrt, die vom Kunden vor der Bestellung akzeptiert werden müssen. BlaBlaCar bietet keine Transportdienstleistungen in Bezug auf Busreisen an, die Busunternehmen sind alleinige Vertragspartei der Verkaufsbedingungen. Sie erkennen an, dass die Buchung von Sitzplätzen für eine bestimmte Busreise den Verkaufsbedingungen des jeweiligen Busunternehmens unterliegt.

BlaBlaCar macht Sie darauf aufmerksam, dass bestimmte vom Busunternehmen angebotene und auf der Website erwähnte Transportdienste eingestellt werden können, insbesondere aus klimatischen Gründen, saisonalen Gründen oder im Falle höherer Gewalt.

### **4.2.3 Nominativer Charakter der Sitzplatzreservierung und Nutzungsbedingungen der Dienste im Namen eines Dritten**

Jede Nutzung der Dienste, sei es als Fahrgast oder als Fahrer, ist persönlich. Sowohl der Fahrer als auch der Beifahrer müssen der Identität entsprechen, die BlaBlaCar und gegebenenfalls den anderen an der Fahrgemeinschaftsfahrt teilnehmenden Mitgliedern oder dem Busunternehmen mitgeteilt wurde.

BlaBlaCar gestattet seinen Mitgliedern jedoch, einen oder mehrere Sitzplätze im Namen eines Dritten zu reservieren. In diesem Fall verpflichten Sie sich, dem Fahrer oder dem Busunternehmen zum Zeitpunkt der Buchung den Vornamen, das Alter und die Telefonnummer der Person, in deren Namen Sie einen Sitzplatz buchen, genau mitzuteilen. Es ist strengstens verboten, einen Sitzplatz für einen allein reisenden Minderjährigen unter 13 Jahren für eine Fahrgemeinschaftsfahrt zu reservieren. Für den Fall, dass Sie einen Sitzplatz für eine Fahrgemeinschaftsfahrt für einen allein reisenden Minderjährigen über 13 Jahren reservieren, stimmen Sie zu, die vorherige Zustimmung des Fahrers einzuholen und ihm eine ordnungsgemäß ausgefüllte und unterzeichnete Vollmacht der gesetzlichen Vertreter vorzulegen. In Bezug auf eine Busreise laden wir Sie ein, die AGB des betreffenden Busunternehmens zu lesen.

Darüber hinaus ist die Plattform für die Reservierung von Plätzen für natürliche Personen bestimmt. Es ist daher verboten, einen Sitzplatz für den Transport von Gegenständen, Paketen, allein reisenden Tieren oder anderen materiellen Gegenständen zu reservieren.

Darüber hinaus ist es untersagt, eine Mitfahrgelegenheitsanzeige für einen anderen Fahrer als Sie selbst zu veröffentlichen.

### **4.3. Mitgliederinhalte, Moderation und Bewertungssystem**

### **4.3.1. Funktionsweise des Bewertungssystems**

BlaBlaCar ermutigt Sie, eine Bewertung über einen Fahrer (wenn Sie ein Beifahrer sind) oder einen Beifahrer (wenn Sie ein Fahrer sind) zu hinterlassen, mit dem Sie eine Fahrt geteilt haben oder mit dem Sie eine Fahrt teilen sollten (was heißt, eine Fahrt gebucht haben). Andererseits sind Sie nicht berechtigt, eine Bewertung für einen anderen Passagier abzugeben, wenn Sie selbst ein Passagier waren, oder für ein Mitglied, mit dem Sie nicht gereist sind oder mit dem Sie nicht reisen sollten. Sie haben die Möglichkeit, innerhalb von 14 Tagen nach der Reise eine Bewertung abzugeben.

Ihre Bewertung sowie gegebenenfalls die von einem anderen Mitglied über Sie hinterlassene Bewertung wird erst nach dem kürzeren der folgenden Zeiträume sichtbar und auf der Plattform veröffentlicht: (i) unmittelbar nachdem Sie beide eine Mitteilung hinterlassen haben oder (ii) vergangen 14 Tage nach der ersten Benachrichtigung.

Sie haben die Möglichkeit, auf eine Bewertung zu antworten, die ein anderes Mitglied in Ihrem Profil hinterlassen hat. Die Bewertung und gegebenenfalls Ihre Antwort werden in Ihrem Profil veröffentlicht.

### **4.3.2. Moderation**

**a. Mitgliederinhalte**

Sie erkennen an und akzeptieren, dass BlaBlaCar Mitgliederinhalte gemäß der in Abschnitt 11.2. enthaltenen Definition vor deren Veröffentlichung unter Verwendung automatisierter Tools oder manuell moderieren kann. Wenn BlablaCar der Ansicht ist, dass ein solcher Mitgliederinhalt gegen geltende Gesetze oder diese AGB verstößt, behält es sich das Recht vor:

* Die Veröffentlichung zu unterbinden oder solche Mitgliederinhalte zu löschen
* Das Mitglied zu verwarnen, indem es an seine Verpflichtung erinnert wird, geltende Gesetze oder diese AGB einzuhalten, bzw.
* Einschränkende Maßnahmen gemäß Abschnitt 9 dieser AGB anzuwenden.

Die Verwendung solcher automatisierten Tools oder manuelle Moderation durch BlaBlaCar kann nicht als Verpflichtung zur Überwachung oder zur aktiven Suche nach rechtswidrigen Aktivitäten bzw. auf der Plattform veröffentlichten Mitgliederinhalten ausgelegt werden und begründet, soweit nach geltendem Recht zulässig, keinerlei Haftung seitens BlaBlaCar.

**b. Nachrichten**

Der Austausch von Nachrichten zwischen Mitgliedern über die Plattform (“Nachrichten”) dient nur dem Informationsaustausch über Fahrgemeinschaften.

BlaBlaCar kann zur Vorbeugung von Betrug, zur Verbesserung der Dienste, zu Zwecken der Kundenbetreuung und zur Ausführung der mit unseren Mitgliedern abgeschlossenen Verträge (wie diese AGB) durch die Verwendung automatisierter Software und Algorithmen den Inhalt von Nachrichten ermitteln. Im Falle der Ermittlung solcher Inhalte in einer Nachricht, die Anzeichen für betrügerisches oder illegales Verhalten, die Umgehung der Plattform oder einen sonstigen Verstoß gegen diese AGB darstellen:

* Kann ein solcher Inhalt nicht veröffentlicht werden
* Das Mitglied, das die Nachricht versandt hat, kann eine Verwarnung erhalten, in der es an seine Verpflichtung erinnert wird, geltende Gesetze oder diese AGB einzuhalten, oder
* Das Konto des Mitglieds kann gemäß Abschnitt 9 dieser AGB gesperrt werden.

Die Verwendung solcher automatisierter Software durch BlaBlaCar kann nicht als Verpflichtung zur Überwachung oder zur aktiven Suche nach rechtswidrigen Aktivitäten bzw. auf der Plattform veröffentlichten Mitgliederinhalten ausgelegt werden, und begründet, soweit nach geltendem Recht zulässig, keinerlei Haftung seitens BlaBlaCar. 

###  **4.3.3. Grenze**

Gemäß Abschnitt 9 dieser AGB behält sich BlaBlaCar das Recht vor, Ihr Konto zu sperren, Ihren Zugriff auf die Dienste einzuschränken oder diese AGB zu kündigen, falls (i) Sie mindestens drei Bewertungen erhalten haben und (ii) eine Bewertung oder der Durchschnitt der erhaltenen Bewertungen höchstens gleich 3 ist, je nachdem wie schwerwiegend die in der Bewertung ausgedrückte Kritik ist.

**5\. Finanzielle Konditionen**
-------------------------------

Der Zugriff auf und die Registrierung auf der Plattform sowie die Recherche, Konsultation und Veröffentlichung von Anzeigen sind kostenlos. Andererseits ist die Reservierung gemäß den unten beschriebenen Bedingungen zahlbar.

### **5.1. Kostenbeitrag und Preis**

5.1.1. Im Rahmen einer Fahrgemeinschaftsfahrt wird die Höhe des Kostenbeitrags von Ihnen als Fahrer in eigener Verantwortung festgelegt. Es ist strengstens untersagt, aus der Nutzung unserer Plattform Vorteile zu ziehen. Folglich verpflichten Sie sich, die Höhe des Kostenbeitrags, den Sie von Ihren Fahrgästen verlangen, auf die Kosten zu beschränken, die Sie tatsächlich für die Durchführung der Fahrgemeinschaftsfahrt tragen. Andernfalls tragen Sie allein das Risiko einer Neuqualifizierung der über die Plattform durchgeführten Transaktion.

Bei der Veröffentlichung einer Mitfahranzeige schlägt BlaBlaCar einen Kostenbeitrag vor, der insbesondere die Art der Mitfahrgelegenheit und die zurückgelegte Strecke berücksichtigt. Dieser Betrag ist nur ein Richtwert und es liegt an Ihnen, ihn nach oben oder unten zu ändern, um die Kosten zu berücksichtigen, die Sie tatsächlich für die Mitfahrgelegenheit tragen. Um Missbrauch zu vermeiden, schränkt BlaBlaCar die Möglichkeiten zur Anpassung der Höhe des Kostenbeitrags ein.

5.1.2. Bei Busfahrten wird der Preis pro Sitzplatz nach eigenem Ermessen vom Busunternehmen festgelegt. Der Kunde wird gebeten, sich auf die entsprechenden Verkaufsbedingungen zu beziehen, um herauszufinden, wie er die Bestellung aufgibt und das Ticket bezahlt.

### **5.2. Servicegebühr**

BlaBlaCar erhebt als Gegenleistung für die Nutzung der Plattform zum Zeitpunkt der Buchung eine Servicegebühr (im Folgenden „**Servicegebühr**“ genannt). Falls diese anfällt, werden Sie vorher darüber informiert, dass die Servicegebühr anfällt.

Die geltenden Methoden zur Berechnung der Servicegebühr sind [hier](https://blog.blablacar.de/blablalife/lp/servicegebuehr) zu finden und dienen lediglich der Information und haben keinen vertraglichen Wert. Die Servicegebühr kann nach verschiedenen Faktoren berechnet werden, insbesondere nach der Länge der Fahrt und dem Kostenbeitrag. BlaBlaCar behält sich das Recht vor, die Methoden zur Berechnung der Servicegebühr jederzeit zu ändern. Diese Änderungen haben keinen Einfluss auf die Servicegebühr, die Sie vor dem Datum des Inkrafttretens dieser Änderungen akzeptiert haben.

Bei grenzüberschreitenden Fahrten beachten Sie bitte, dass die Methoden zur Berechnung der Servicegebühr und der geltenden Mehrwertsteuer je nach Abholort und/oder Zielort der Fahrt variieren.

Bei der Nutzung der Plattform für grenzüberschreitende Fahrten oder außerhalb Deutschlands kann die Servicegebühr von einer Tochtergesellschaft von BlaBlaCar erhoben werden, die die lokale Plattform betreibt.

### **5.3. Rundung**

Sie erkennen an und stimmen zu, dass BlaBlaCar nach eigenem Ermessen den Kostenbeitrag und die Servicegebühr auf ganze Zahlen oder Dezimalzahlen auf- oder abrunden kann.

### **5.4 Zahlungsmodalitäten und Rückzahlung des Kostenbeitrags an den Fahrer bzw. des Fahrpreises an die Busunternehmen**

Sie als Fahrer haben die Möglichkeit, sich den Kostenbeitrag von den Fahrgästen dieser Fahrt bezahlen zu lassen, indem Sie sich für eine Online-Zahlung entscheiden (siehe 5.4.1). Diese Möglichkeit kann in einigen Fällen bei Fahrten, bei denen Abfahrt oder Ankunft außerhalb der Landesgrenzen liegen, eingeschränkt sein.

In diesem Fall verpflichtet sich der Mitfahrer, den Kostenbeitrag spätestens beim Verlassen des Fahrzeugs am Zielort an den Fahrer zu zahlen. Der Fahrer ist nicht berechtigt, die Zahlung des Kostenbeitrags ganz oder teilweise vor Beendigung der Mitfahrgelegenheit zu verlangen. In allen anderen Fällen gelten die nachstehenden Bedingungen.

### **5.4.1 Anweisung des Fahrers (Mandat)**

Durch die Nutzung der Plattform als Fahrer für Fahrgemeinschaften bestätigen Sie die [allgemeinen Geschäftsbedingungen](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) des Anbieters der Hyperwallet-Zahlungslösung – unseres Zahlungsanbieters, der über die Hyperwallet-Zahlungslösung gemäß Artikel 5.4.2.a die Überweisungen der Kostenbeiträge an die Fahrer abwickelt – durchgelesen und akzeptiert haben. Diese allgemeinen Geschäftsbedingungen werden im Folgenden als „Hyperwallet AGB“ bezeichnet.

Beachten sie bitte, dass die Ausnahmen der Richtlinie (EU) 2015/2366 des Europäischen Parlaments und des Rats vom 25. November 2015 über Zahlungsdienste im Binnenmarkt, die in Artikel 11.4 (i) und (ii) der Hyperwallet-AGB aufgeführt sind, nicht für Plattformmietglieder gelten, die keine Gewerbetreibenden sind.

Als Fahrer weisen Sie an:

* BlaBlaCar beauftragt den Anbieters der Hyperwallet-Zahlungslösung, die Übertragung des Kostenbeitrags vorzunehmen, und
* Der Anbieters der Hyperwallet-Zahlungslösung muss die Überweisung des Kostenbeitrags auf Ihr Bank- oder PayPal-Konto in Ihrem Namen und in Ihrem Auftrag gemäß den Hyperwallet-AGB vornehmen.

Im Rahmen einer Mitfahrgelegenheit und nach manueller oder automatischer Annahme der Buchung wird der vom Fahrgast gezahlte Kostenbeitrag und die Servicegebühr auf einem vom Anbieter der Hyperwallet-Zahlungslösung verwalteten Treuhandkonto verwahrt.

### **5.4.2 Zahlung des Kostenbeitrags an den Fahrer**

Nach der Fahrgemeinschaftsreise haben die Fahrgäste eine Frist von 24 Stunden, um einen Anspruch auf die Reise bei BlaBlaCar geltend zu machen. Erfolgt innerhalb dieser Frist kein Anspruch des Fahrgastes, betrachtet BlaBlaCar die Reise als bestätigt.

Ab dem Zeitpunkt dieser ausdrücklichen oder stillschweigenden Bestätigung haben Sie als Fahrer ein Guthaben auf Ihrem Konto. Diese Gutschrift entspricht dem vom Passagier zum Zeitpunkt der Bestätigung der Buchung gezahlten Gesamtbetrag abzüglich der Servicegebühr, d. h. dem vom Passagier gezahlten Kostenbeitrag.

**a. Hyperwallet-Zahlungslösung**

Die Hyperwallet-Zahlungslösung ist eine vom Anbieter der Hyperwallet-Zahlungslösung bereitgestellte Zahlungsdienstleistung. Um Zweifel auszuschließen wird darauf hingewiesen, dass BlaBlaCar Mitgliedern keine Zahlungsabwicklungsdienste zur Verfügung stellt und nicht in den Besitz der Gelder von Mitgliedern gelangt.

Wenn die Fahrgemeinschaftsfahrt vom Fahrgast bestätigt wird, haben Sie als Fahrer die Möglichkeit, Ihren Kostenbeitrag auf Ihr Bankkonto oder auf Ihr PayPal-Konto überweisen zu lassen.

Zu diesem Zwecke können Sie die Hyperwallet-Zahlungslösung nutzen, indem Sie in einen direkten Vertrag mit dem Anbieter der Hyperwallet-Zahlungslösung eintreten und die Hyperwallet-AGB annehmen.

Um die erste Überweisung des Kostenbeitrags zu erhalten, werden Sie aufgefordert, eine Zahlungsmethode auszuwählen, also Ihre Bankverbindung oder Ihr PayPal-Konto zu verknüpfen und Ihre Postanschrift und Ihr Geburtsdatum anzugeben. Sollten Sie dies unterlassen oder falsche, ungenaue oder veraltete Informationen angeben, wird der Kostenbeitrag nicht überwiesen.

Die Überweisung wird durch den Anbieter der Hyperwallet-Zahlungslösung ausgeführt.

Am Ende des Dreijahreszeitraums gilt jeder Kostenbeitragsbetrag, der ab dem Datum, an dem er von Ihnen als Fahrer über die Hyperwallet-Zahlungslösung in Ihrem Konto erschienen ist, nicht eingefordert wurde, als Eigentum von BlaBlaCar.

**b. KYC-Prüfungen**

Bei der Nutzung der Hyperwallet-Zahlungslösung akzeptieren Sie, dass Sie möglicherweise den vom Anbieter der Hyperwallet-Zahlungslösung angewandten Regulierungsverfahren gemäß dessen [Hyperwallet AGB](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) unterliegen.

Diese Verfahren können Identitätsprüfungen und andere Anforderungen des auf den vom Anbieter der Hyperwallet-Zahlungslösung festgelegten Kriterien beruhenden “Know Your Customer” (KYC)-Verfahrens umfassen. Diese Kriterien können finanzielle Schwellenwerte umfassen, die vom Gesamtbetrag der Auszahlungen des Kostenbeitrags abhängen, den Sie erhalten.

Zu Zwecken der KYC-Prüfung können Sie aufgefordert werden, zusätzliche, vom Anbieter der Hyperwallet-Zahlungslösung angeforderte Informationen zu übermitteln. Sollten Sie die angeforderten Unterlagen nicht übermitteln, können Sie den Kostenbeitrag erst dann erhalten, wenn Ihre Identität bestätigt wurde.

Möglicherweise muss der Anbieter der Hyperwallet-Zahlungslösung in manchen Situationen die Überweisung von Kostenbeiträgen oder den Zugriff auf die Zahlungslösung im Einklang mit den geltenden Gesetzen aussetzen. BlaBlaCar hat keinerlei Einfluss auf solche Maßnahmen des Anbieters der Hyperwallet-Zahlungslösung.

Die vom Anbieter der Hyperwallet-Zahlungslösung durchgeführten KYC-Prüfungen erfolgen getrennt und unabhängig von den Prüfungen, die BlaBlaCar in Verbindung mit der Nutzung der Plattform und in Übereinstimmung mit diesen Bestimmungen und der Datenschutzrichtlinie vornehmen kann.

### **5.4.3 Zahlung des Preises**

Die Zahlung für jede Bestellung, die über die Plattform getätigt wird, erfolgt auf eine der unten autorisierten Weisen. Der Kunde hat die Möglichkeit, die Informationen zu einer oder mehreren Kreditkarten in seinem Kundenkonto zu speichern, damit er diese Informationen bei späteren Zahlungen nicht systematisch eingeben muss.

Die Zahlungsmittel können je nach von Ihnen verwendeter BlaBlaCar-Plattform variieren. Die autorisierten Zahlungsmittel lauten wie folgt:  
– Bank-Kreditkarte (die akzeptierten Karten werden auf der Plattform angezeigt. Falls Sie eine Co-Branding-Karte besitzen, können Sie in der Zahlungsphase eine bestimmte Karte auswählen)  
– Paypal  
– Gutscheine  
– Apple Pay, Google Pay

Vor der vollständigen und wirksamen Zahlung des Preises der vom Kunden ausgewählten Transportdienstleistungen wird keine Auftragsbestätigung ausgestellt. Wenn die Zahlung aus einem vom Kunden zu vertretenden Grund unregelmäßig, unvollständig oder nicht erfolgt ist, wird die Bestellung sofort storniert.

Die Bestellung wird bestätigt, indem eine E-Mail an den Kunden gesendet wird, die die Einzelheiten der Bestellung enthält.

Der Passagier wird gebeten, die Einstellungen für den Posteingang seiner E-Mail-Adresse zu überprüfen und insbesondere sicherzustellen, dass die Bestätigungs-E-Mail nicht direkt an Spam gesendet wird.

Die Auftragsbestätigung ist endgültig. Folglich führen alle Änderungen entweder zu einem Umtausch oder einer Stornierung gemäß den Bedingungen der geltenden AGB. Es liegt in der Verantwortung des Kunden sicherzustellen, dass die Transportdienste gemäß den Bedürfnissen und Erwartungen des Passagiers ausgewählt werden. Die Richtigkeit der vom Kunden eingegebenen personenbezogenen Daten liegt in der Verantwortung des letzteren, es sei denn, BlaBlaCar hat nachweislich versäumt, diese personenbezogenen Daten zu erfassen, zu speichern oder zu schützen.

### **6\. Nicht kommerzieller und nicht geschäftlicher Zweck der Dienste und der Plattform**

Sie erklären sich damit einverstanden, die Dienste und die Plattform nur zu nutzen, um auf nicht geschäftlicher und nicht kommerzieller Basis mit Personen in Kontakt zu treten, die eine Mitfahrgelegenheit mit Ihnen teilen oder einen Sitzplatz im Rahmen einer Busfahrt buchen möchten.

Im Zusammenhang mit der Mitfahrgelegenheit haben Sie anerkannt, dass die Verbraucherrechte aus dem Verbraucherschutzrecht der Union nicht für Ihre Beziehung zu anderen Mitgliedern gelten.

Als Fahrer erklären Sie sich damit einverstanden, keinen höheren Kostenbeitrag zu verlangen, als die Kosten, die Ihnen tatsächlich entstehen und die einen Gewinn generieren können, mit der Maßgabe, dass Sie als Fahrer im Rahmen einer Kostenbeteiligung Ihren eigenen Anteil an den verursachten Kosten tragen müssen durch die Mitfahrgelegenheit. Sie sind allein dafür verantwortlich, die Kosten zu berechnen, die Ihnen für die Mitfahrgelegenheit entstehen, und zu überprüfen, ob der von Ihren Fahrgästen geforderte Betrag die Ihnen tatsächlich entstehenden Kosten (ohne Ihren Anteil an den Kosten) nicht übersteigt, insbesondere unter Bezugnahme auf den geltenden Steuertarif.

BlaBlaCar behält sich das Recht vor, Ihr Konto zu sperren, wenn Sie ein Fahrzeug mit Chauffeur oder ein anderes Geschäftsfahrzeug oder Taxi oder einen Firmenwagen nutzen und dadurch einen Gewinn mit der Plattform erzielen. Sie stimmen zu, BlaBlaCar auf einfache Anfrage hin eine Kopie Ihres Fahrzeugscheins und/oder eines anderen Dokuments zur Verfügung zu stellen, aus dem hervorgeht, dass Sie zur Nutzung dieses Fahrzeugs auf der Plattform berechtigt sind und keinen Gewinn daraus ziehen.

Gemäß Abschnitt 9 der AGB behält sich BlaBlaCar außerdem das Recht vor, Ihr Konto zu sperren, Ihren Zugang zu den Diensten einzuschränken oder diese AGB zu kündigen, falls Sie auf der Plattform Aktivitäten durchführen, die aufgrund der Art der angebotenen Fahrten, ihrer Häufigkeit oder der Anzahl der Passagiere auftreten durchgeführt und der angeforderte Kostenbeitrag für Sie zu einer Gewinnsituation führt oder BlaBlaCar aus irgendeinem Grund vermuten lässt, dass Sie auf der Plattform einen Gewinn erzielen.

**7\. Stornierungsbedingungen**
-------------------------------

### **7.1 Rückzahlungsbedingungen im Falle einer Stornierung**

Nur Fahrgemeinschaftsfahrten sind Gegenstand dieser Stornierungsbedingungen; BlaBlaCar bietet keinerlei Garantie im Falle einer Stornierung aus irgendeinem Grund. Im Zusammenhang mit Busreisen gelten für die Stornobedingungen die Verkaufs-AGB der Busunternehmen. Der Kunde wird gebeten, sich auf die jeweiligen Verkaufsbedingungen zu beziehen, um die Umtausch- und Stornierungsbedingungen seiner Busreise anzuerkennen.

7.1.1. Rückerstattungsbedingungen im Falle einer Stornierung einer Mitfahrgelegenheit mit Online-Zahlung des Kostenbeitrags

Die Stornierung eines Sitzplatzes auf einer Mitfahrgelegenheit durch den Fahrer oder den Beifahrer nach der Buchungsbestätigung unterliegt den nachstehenden Bestimmungen:

– Im Falle einer Stornierung durch den Fahrer wird dem Fahrgast der gesamte gezahlte Betrag (d.h. der Kostenbeitrag und die Servicegebühr) zurückerstattet. Dies ist insbesondere dann der Fall, wenn der Fahrer eine Fahrgemeinschaftsfahrt storniert oder 15 Minuten nach der vereinbarten Zeit nicht am Treffpunkt eingetroffen ist;

– Im Falle einer Stornierung durch den Passagier:

* Storniert der Passagier mehr als 24 Stunden vor der geplanten Abfahrtszeit, wie in der Anzeige angegeben, wird dem Passagier nur der Kostenbeitrag erstattet. Die Servicegebühr wird von BlaBlaCar einbehalten und der Fahrer erhält keinen Anteil des Betrags.
* Wenn der Passagier 24 Stunden oder weniger vor der geplanten Abfahrtszeit, wie in der Anzeige angegeben, und mehr als 30 Minuten nach der Buchungsbestätigung storniert, wird dem Passagier die Hälfte des zum Zeitpunkt der Buchung gezahlten Kostenbeitrags zurückerstattet, die Servicegebühr wird von BlaBlaCar einbehalten und der Fahrer wird 50 % des Kostenbeitrags über die Hyperwallet-Zahlungslösung erhalten;
* Wenn der Passagier 24 Stunden oder weniger vor der geplanten Abfahrtszeit, wie in der Mitfahrgelegenheitsanzeige angegeben, und 30 Minuten oder weniger nach der Buchungsbestätigung storniert, wird dem Passagier der gesamte Kostenbeitrag über die Hyperwallet-Zahlungslösung erstattet. Die Servicegebühr wird von BlaBlaCar einbehalten und der Fahrer erhält keinen Anteil des Betrags.
* Storniert der Fahrgast nach der in der Mitfahrgelegenheitsanzeige genannten geplanten Abfahrtszeit oder ist er 15 Minuten nach der vereinbarten Zeit nicht am Treffpunkt eingetroffen, erhält der Fahrer den gesamten Kostenbeitrag über die Hyperwallet-Zahlungslösung und BlaBlaCar behält die Servicegebühr ein.

Die Methode zur Rückerstattung der Servicegebühr hängt von der Methode ab, die zur Zahlung der Servicegebühr verwendet wurde, während der Zeitrahmen für die Rückerstattung der Servicegebühr vom Zeitpunkt der Überweisungen durch die Banken abhängt.

Erfolgt eine Stornierung vor der Abfahrt und zu Lasten des Passagiers, werden die vom Passagier stornierten Sitzplätze automatisch anderen Passagieren zur Verfügung gestellt, die sie online buchen können, und sie unterliegen dementsprechend den Bedingungen dieser AGB.

BlaBlaCar erkennt nach eigenem Ermessen auf der Grundlage der verfügbaren Informationen die Rechtmäßigkeit der Rückerstattungsanträge an.

7.1.2. Rückerstattungsbedingungen im Falle einer Stornierung einer Mitfahrgelegenheit mit Barzahlung des Kostenbeitrags

– Im Falle einer Stornierung durch den Fahrer wird dem Mitfahrer die Servicegebühr zurückerstattet. Dies ist insbesondere der Fall, wenn der Fahrer: Die Mitfahranfrage nicht innerhalb des vorgesehenen Zeitraums bestätigte (falls der Fahrer eine solche Option gewählt hat); Eine Mitfahrgelegenheit storniert oder 15 Minuten nach der vereinbarten Zeit nicht am Treffpunkt erschienen ist;

–Im Falle einer Stornierung durch den Mitfahrer, behält BlaBlaCar die Servicegebühr ein.

### **7.2 Rücktrittsrecht**

Indem Sie diese AGB akzeptieren, akzeptieren Sie ausdrücklich, dass der Vertrag zwischen Ihnen und BlaBlaCar, der in der Verbindung mit einem anderen Mitglied besteht, vor Ablauf der Widerrufsfrist seit der Buchungsbestätigung ausgeführt werden soll, und verzichten ausdrücklich auf Ihr Widerrufsrecht gemäß §356 (4) des Bürgerliches Gesetzbuch (BGB).

Bei einer Fahrt mit Barzahlung gibt es keine Entschädigung für den Fahrer (im Falle einer Stornierung durch den Fahrgast oder den Fahrer).

### **8\. Verhalten von Nutzern der Plattform und Mitgliedern**

### **8.1. Verpflichtung aller Nutzer der Plattform**

Sie erkennen an, dass Sie allein dafür verantwortlich sind, alle Gesetze, Vorschriften und Verpflichtungen einzuhalten, die für Ihre Nutzung der Plattform gelten.

Darüber hinaus verpflichten Sie sich bei der Nutzung der Plattform und während Reisen:

(i) die Plattform nicht für berufliche, gewerbliche oder gewinnorientierte Zwecke zu nutzen, wenn Sie kein Busunternehmen sind;

(ii) BlaBlaCar (insbesondere bei der Erstellung oder Aktualisierung Ihres Kontos) oder den anderen Mitgliedern keine falschen, irreführenden, böswilligen oder betrügerischen Informationen zu senden;

(iii) keine verleumderischen, verletzenden, obszönen, pornografischen, vulgären, beleidigenden, aggressiven, unangebrachten, gewalttätigen, bedrohenden, belästigenden, rassistischen oder fremdenfeindlichen Inhalte (einschließlich Nachrichten) auf der Plattform zu sprechen oder sich in irgendeiner Weise zu verhalten oder Inhalte zu posten, oder mit sexuellen Konnotationen, Anstiftung zu Gewalt, Diskriminierung oder Hass, Förderung von Aktivitäten oder der Verwendung illegaler Substanzen oder allgemeiner illegal und im Widerspruch zu den geltenden Gesetzen, diesen AGB und den Zwecken der Plattform, die die Rechte von BlaBlaCar oder eines Dritten verletzen oder gegen die guten Sitten verstoßen können;

(iv) die Rechte und das Image von BlaBlaCar, insbesondere seine geistigen Eigentumsrechte, nicht zu verletzen;

(v) nicht mehr als ein Konto auf der Plattform zu eröffnen und kein Konto im Namen eines Dritten zu eröffnen;

(vi) nicht zu versuchen, das Online-Buchungssystem der Plattform zu umgehen, insbesondere indem Sie versuchen, einem anderen Mitglied oder einem Busunternehmen Ihre Kontaktdaten zu senden, um die Buchung außerhalb der Plattform vorzunehmen und die Servicegebühr zu umgehen;

(vii) kein anderes Mitglied, insbesondere über die Plattform, zu einem anderen Zweck als der Festlegung der Bedingungen der Mitfahrgelegenheit zu kontaktieren;

(viii) keine Zahlungen außerhalb der Plattform oder der Hyperwallet-Zahlungslösung zu akzeptieren oder zu tätigen;

(ix) diese AGB einzuhalten;

(x) um die Erlaubnis von BlaBlaCar einzuholen, bevor Sie beabsichtigen, kommerzielle Audio- oder Videoaufnahmen einer Fahrt zu machen.

### **8.2. Verpflichtungen der Fahrer**

Wenn Sie die Plattform als Fahrer nutzen, verpflichten Sie sich außerdem:

(i) alle für das Fahren und das Fahrzeug geltenden Gesetze, Vorschriften und Kodizes einzuhalten, insbesondere eine zum Zeitpunkt der Mitfahrgelegenheit gültige Haftpflichtversicherung zu unterhalten und im Besitz eines gültigen Führerscheins zu sein;

(ii) um zu überprüfen, ob Ihre Versicherung Mitfahrgelegenheiten abdeckt und ob Ihre Mitfahrer in Ihrem Fahrzeug als Dritte gelten und daher während der gesamten Reise durch Ihre Versicherung abgedeckt sind;

(iii) beim Autofahren kein Risiko einzugehen und keine Produkte mitzunehmen, die Ihre Aufmerksamkeit und Ihre Fähigkeit, wachsam und absolut sicher zu fahren, beeinträchtigen könnten;

(iv) um Anzeigen für Mitfahrgelegenheiten zu veröffentlichen, die nur tatsächlich geplanten Fahrten entsprechen;

(v) die Fahrgemeinschaftsfahrt wie in der Anzeige beschrieben durchzuführen (insbesondere in Bezug auf die Nutzung oder Nichtbenutzung der Autobahn) und die mit den anderen Mitgliedern vereinbarten Zeiten und Orte einzuhalten (insbesondere Treffpunkt und Ausstiegspunkt);

(vi) nicht mehr Passagiere als die in der Fahrgemeinschaftsanzeige angegebene Anzahl von Sitzplätzen mitzunehmen;

(vii) ein Fahrzeug in einwandfreiem Zustand zu verwenden, das den geltenden gesetzlichen Bestimmungen und Gepflogenheiten entspricht, insbesondere mit einem aktuellen TÜV-Zertifikat;

(viii) um BlaBlaCar oder jedem Passagier, der dies anfordert, Ihren Führerschein, Ihre Autozulassungsbescheinigung, Ihre Versicherungsbescheinigung, Ihre TÜV-Bescheinigung und alle Dokumente mitzuteilen, die Ihre Fähigkeit belegen, das Fahrzeug als Fahrer auf der Plattform zu nutzen;

(ix) im Falle einer Verzögerung oder Änderung der Zeit oder der Fahrgemeinschaftsreise Ihre Fahrgäste unverzüglich zu informieren;

(x) im Falle einer grenzüberschreitenden Fahrgemeinschaftsfahrt alle Dokumente, die Ihre Identität und Ihr Recht zum Grenzübertritt belegen, aufzubewahren und für den Passagier und jede Behörde, die dies anfordert, verfügbar zu halten;

(xi) mindestens 15 Minuten nach der vereinbarten Zeit am vereinbarten Treffpunkt auf die Passagiere zu warten;

(xii) keine Mitfahrgelegenheitsanzeige in Bezug auf ein Fahrzeug zu posten, das Ihnen nicht gehört oder für das Sie nicht berechtigt sind, es zum Zwecke der Mitfahrgelegenheit zu nutzen;

(xiii) um sicherzustellen, dass Sie von Ihren Passagieren telefonisch unter der in Ihrem Profil registrierten Nummer kontaktiert werden können;

(xiv) keinen Gewinn über die Plattform zu generieren;

(xv) keine Kontraindikation oder medizinische Unfähigkeit zum Fahren haben;

(xvi) sich während der Mitfahrgelegenheit angemessen und verantwortungsbewusst zu verhalten und den Geist der Mitfahrgelegenheit einzuhalten.

### **8.3 Verpflichtungen der Passagiere**

### **8.3.1 Für Mitfahrgelegenheiten**

Wenn Sie die Plattform als Passagier einer Mitfahrgelegenheit nutzen, verpflichten Sie sich:

(i) sich während der Fahrgemeinschaftsfahrt angemessen zu verhalten, um die Konzentration oder das Fahren des Fahrers oder die Ruhe der anderen Passagiere nicht zu beeinträchtigen;

(ii) das Fahrzeug des Fahrers und seine Sauberkeit zu respektieren;

(iii) im Falle eines Staus den Fahrer unverzüglich zu informieren;

(iv) am Treffpunkt mindestens 15 Minuten über die vereinbarte Zeit hinaus auf den Fahrer zu warten;

(v) um BlaBlaCar oder jedem Fahrer, der darum bittet, Ihren Personalausweis oder ein Dokument zum Nachweis Ihrer Identität mitzuteilen;

(vi) während einer Fahrgemeinschaftsfahrt keine Gegenstände, Güter, Substanzen oder Tiere mitzuführen, die das Fahren und die Konzentration des Fahrers behindern könnten oder deren Art, Besitz oder Beförderung den geltenden gesetzlichen Bestimmungen widerspricht;

(vii) im Falle einer grenzüberschreitenden Mitfahrgelegenheit alle Dokumente, die Ihre Identität und Ihr Recht zum Grenzübertritt belegen, aufzubewahren und für den Fahrer und jede Behörde, die dies anfordert, verfügbar zu halten;

(viii) um sicherzustellen, dass Sie von Ihrem Fahrer telefonisch unter der in Ihrem Profil registrierten Nummer kontaktiert werden können, auch am Treffpunkt;

(ix) dem Fahrer den vereinbarten Kostenbeitrag zu zahlen.

Für den Fall, dass Sie eine Buchung für einen oder mehrere Sitzplätze im Namen von Dritten vorgenommen haben, garantieren Sie in Übereinstimmung mit den Bestimmungen des obigen Artikels 4.2.3 die Einhaltung der Bestimmungen dieses Artikels und allgemein dieser Dritten durch diesen Dritten AGB. BlaBlaCar behält sich das Recht vor, Ihr Konto zu sperren, Ihren Zugang zu den Diensten einzuschränken oder diese AGB zu kündigen gemäß Abschnitt 9 dieser AGB, im Falle eines Verstoßes durch Dritte, in deren Namen Sie einen Sitzplatz gemäß diesen AGB, gebucht haben.

### **8.3.2 Für Busfahrt**

Im Zusammenhang mit der Busfahrt verpflichtet sich der Fahrgast, die Verkaufsbedingungen des jeweiligen Busunternehmens einzuhalten.

**8.4. Meldung unangemessener oder illegaler Inhalte  (Hinweis- und Aktionsmechanismus)**

Sie können verdächtige, unangemessene oder illegale Mitgliederinhalte oder Nachrichten, wie [hier](https://support.blablacar.com/s/article/Illegale-Inhalte-melden-1729197120592?language=de) beschrieben melden.

Sobald BlaBlaCar gemäß diesem Abschnitt oder von den zuständigen Behörden ordnungsgemäß benachrichtigt wurde, wird es jegliche gesetzeswidrigen Mitgliederinhalte unverzüglich entfernen wenn:

* die Mitgliederinhalte offensichtlich rechtswidrig sind oder gegen geltende Vorschriften verstoßen; oder 
* BlaBlaCar der Ansicht ist, dass ein solcher Inhalt gegen diese AGB verstößt.

In solchen Fällen behält sich BlaBlaCar das Recht vor, den ordnungsgemäß gemeldeten Mitgliederinhalt in hinreichend ausführlicher und deutlicher Form zu entfernen, bzw. das gemeldete Konto umgehend zu sperren.

Das betreffende Mitglied kann die obigen Entscheidungen von BlaBlaCar wie im untenstehenden Abschnitt 15.1 beschrieben anfechten.

**9\. Einschränkungen in Bezug auf die Nutzung der Plattform, Sperrung von Konten, Zugangsbeschränkung und Kündigung**
----------------------------------------------------------------------------------------------------------------------

Sie können Ihr Vertragsverhältnis mit BlaBlaCar jederzeit kostenlos und ohne Angabe von Gründen kündigen. Gehen Sie dazu einfach auf die Registerkarte „Mein Konto schließen“ auf Ihrer Profilseite.

Im Falle (i) eines Verstoßes Ihrerseits gegen diese AGB, einschließlich, aber nicht beschränkt auf Ihre Pflichten als Mitglied gemäß Artikel 6 und 8 oben, (ii) Überschreiten der in Artikel 4.3.3 oben festgelegten Grenze oder (iii) Wenn BlaBlaCar ernsthaften Grund zu der Annahme hat, dass dies zum Schutz seiner Sicherheit und seiner Integrität, der der Mitglieder oder Dritter oder zum Zwecke von Ermittlungen oder der Betrugsprävention erforderlich ist, behält sich BlaBlaCar das Recht vor:

(i) die Sie mit BlaBlaCar bindenden AGB sofort und fristlos zu kündigen; und/oder

(ii) das Posten jeglicher Mitgliederinhalte durch Sie auf der Plattform zu verhindern oder solche Inhalte zu entfernen; und/oder

(iii) Ihren Zugang und Ihre Nutzung der Plattform einschränken; und/oder

(iv) Ihr Konto vorübergehend oder dauerhaft sperren.

Eine Kontosperrung kann gegebenenfalls auch bedeuten, dass Sie Ihre ausstehenden Auszahlungen nicht erhalten.

Gegebenenfalls kann Ihnen BlaBlaCar eine Verwarnung senden, in der Sie an Ihre Verpflichtung erinnert werden, geltende Gesetze oder diese AGB einzuhalten.  

Wenn dies erforderlich ist, werden Sie über die Einrichtung solcher Maßnahmen informiert, damit Sie gegenüber BlaBlaCar Ihre Verpflichtung bekräftigen können, geltende Gesetze oder diese AGB einzuhalten, Erklärungen abgeben oder seine Entscheidung anfechten können. BlaBlaCar entscheidet unter Berücksichtigung aller Umstände jedes einzelnen Falles und der Schwere des Verstoßes nach eigenem Ermessen, ob die getroffenen Maßnahmen aufgehoben werden oder nicht.

**10\. Personenbezogene Daten**
-------------------------------

Im Zusammenhang mit Ihrer Nutzung der Plattform wird BlaBlaCar einige Ihrer personenbezogenen Daten erheben, wie in der [Datenschutzrichtlinie](https://blog.blablacar.de/about-us/privacy-policy) beschrieben wird.

**11\. Geistiges Eigentum**
---------------------------

### **11.1 Inhalt veröffentlicht von BlaBlaCar**

Vorbehaltlich der von seinen Mitgliedern bereitgestellten Inhalte ist BlaBlaCar der alleinige Inhaber aller geistigen Eigentumsrechte in Bezug auf den Dienst, die Plattform, ihre Inhalte (insbesondere Texte, Bilder, Designs, Logos, Videos, Töne, Daten, Grafiken) und die Software und Datenbanken, die ihren Betrieb sicherstellen.

BlaBlaCar gewährt Ihnen ein nicht ausschließliches, persönliches und nicht übertragbares Recht zur Nutzung der Plattform und der Dienste für Ihren persönlichen und privaten Gebrauch, auf nicht kommerzieller Basis und in Übereinstimmung mit den Zwecken der Plattform und der Dienste.

Ohne die vorherige schriftliche Genehmigung von BlaBlaCar ist Ihnen jede andere Nutzung oder Verwertung der Plattform und der Dienste und ihrer Inhalte untersagt. Insbesondere ist es Ihnen untersagt:

(i) Vervielfältigung, Änderung, Anpassung, Verteilung, öffentliche Darstellung und Verbreitung der Plattform, der Dienste und der Inhalte, mit Ausnahme der ausdrücklich von BlaBlaCar genehmigten;

(ii) die Plattform oder Dienste zu dekompilieren und zurückzuentwickeln, vorbehaltlich der in den geltenden Texten festgelegten Ausnahmen;

(iii) Extrahieren oder versuchen zu extrahieren (insbesondere unter Verwendung von Data-Mining-Robotern oder anderen ähnlichen Datenerfassungstools) eines wesentlichen Teils der Daten der Plattform.

### **11.2 Von Ihnen auf der Plattform gepostete Inhalte**

Um die Bereitstellung der Dienste zu ermöglichen und in Übereinstimmung mit dem Zweck der Plattform gewähren Sie BlaBlaCar eine nicht-exklusive Lizenz zur Nutzung der von Ihnen bereitgestellten Inhalte und Daten im Rahmen Ihrer Nutzung der Dienste, wozu Buchungsanfragen, Mitfahrgelegenheiten und die darin enthaltenen Kommentare, Biographien, Fotos, Bewertungen und Antworten auf Bewertungen (im Folgenden als „ Ihre „Mitgliederinhalte“ bezeichnet) und Nachrichten gehören können. Um es BlaBlaCar zu ermöglichen, über das digitale Netzwerk und in Übereinstimmung mit jedem Kommunikationsprotokoll (insbesondere Internet und Mobilfunknetz) zu verbreiten und den Inhalt der Plattform der Öffentlichkeit zur Verfügung zu stellen, autorisieren Sie BlaBlaCar für die ganze Welt und für die gesamte Dauer Ihrer Vertragsbeziehungen mit BlaBlaCar, um Ihre Mitgliederinhalte wie folgt zu reproduzieren, darzustellen, anzupassen und zu übersetzen:

(i) Sie ermächtigen BlaBlaCar, Ihre Mitgliederinhalte ganz oder teilweise auf beliebigen bekannten oder noch unbekannten digitalen Aufzeichnungsmedien zu reproduzieren, insbesondere auf Servern, Festplatten, Speicherkarten oder anderen gleichwertigen Medien, in jedem Format und durch alle bekannten oder noch unbekannten Prozesse, soweit dies für Speicher-, Sicherungs-, Übertragungs- oder Downloadvorgänge im Zusammenhang mit dem Betrieb der Plattform und der Bereitstellung des Dienstes erforderlich ist;

(ii) Sie ermächtigen BlaBlaCar, Ihre Mitgliederinhalte anzupassen und zu übersetzen und diese Anpassungen auf allen aktuellen oder zukünftigen digitalen Medien, wie in Punkt (i) oben festgelegt, zu reproduzieren, um die Dienste bereitzustellen, insbesondere in verschiedenen Sprachen. Dieses Recht umfasst insbesondere die Möglichkeit, Änderungen an der Formatierung Ihres Mitgliedsinhalts unter Berücksichtigung Ihres Urheberpersönlichkeitsrechts vorzunehmen, um die Grafikcharta der Plattform zu respektieren und/oder ihn im Hinblick auf seine Veröffentlichung über die technisch kompatibel zu machen Plattform.

Sie tragen die Verantwortung für alle Mitgliederinhalte und Nachrichten, die Sie auf unsere Plattform hochladen und halten alle diesbezüglichen Rechte. 

**12\. Rolle von BlaBlaCar**
----------------------------

Die Plattform stellt eine Online-Netzwerkplattform dar, auf der die Mitglieder Mitfahrinserate für Mitfahrgelegenheiten zum Zweck der Mitfahrgelegenheit erstellen und einstellen können. Diese Mitfahrgelegenheitsanzeigen können insbesondere von den anderen Mitgliedern eingesehen werden, um die Bedingungen der Reise zu erfahren und gegebenenfalls direkt einen Sitzplatz in dem betreffenden Fahrzeug bei dem Mitglied zu buchen, das die Anzeige auf der Plattform veröffentlicht hat. Die Plattform ermöglicht auch die Buchung von Sitzplätzen für Busfahrten.

Indem Sie die Plattform nutzen und diese AGB akzeptieren, erkennen Sie an, dass BlaBlaCar weder Partei einer Vereinbarung zwischen Ihnen und den anderen Mitgliedern im Hinblick auf die Aufteilung der Kosten im Zusammenhang mit einer Fahrt noch einer zwischen Ihnen und einem Bus geschlossenen Vereinbarung ist Operator.

BlaBlaCar hat keine Kontrolle über das Verhalten seiner Mitglieder, der Busunternehmen und ihrer Vertreter sowie der Nutzer der Plattform. Es besitzt, verwertet, liefert oder verwaltet die Fahrzeuge, die Gegenstand der Anzeigen sind, nicht und bietet keine Fahrten auf der Plattform an.

Sie erkennen an und akzeptieren, dass BlaBlaCar die Gültigkeit, Wahrhaftigkeit oder Rechtmäßigkeit der angebotenen Anzeigen, Sitzplätze und Fahrten nicht kontrolliert. BlaBlaCar erbringt als Vermittler von Mitfahrgelegenheiten keine Beförderungsleistung und tritt nicht als Beförderer auf; Die Rolle von BlaBlaCar beschränkt sich darauf, den Zugang zur Plattform zu erleichtern.

Im Zusammenhang mit Mitfahrgelegenheiten handeln die Mitglieder (Fahrer oder Mitfahrer) unter ihrer alleinigen und vollen Verantwortung.

In seiner Eigenschaft als Vermittler kann BlaBlaCar nicht für das tatsächliche Auftreten einer Mitfahrgelegenheit oder Busfahrt haftbar gemacht werden, insbesondere aus folgenden Gründen:

(i) fehlerhafte Informationen, die der Fahrer oder der Busunternehmer in seiner Anzeige oder auf andere Weise in Bezug auf die Reise und ihre Bedingungen mitgeteilt hat;

(ii) Stornierung oder Änderung einer Reise durch ein Mitglied oder einen Busbetreiber;

(iii) das Verhalten seiner Mitglieder während, vor oder nach der Reise.

**13\. Betrieb, Verfügbarkeit und Funktionalitäten der Plattform**
------------------------------------------------------------------

BlaBlaCar bemüht sich, die Plattform nach Möglichkeit 7 Tage die Woche und 24 Stunden am Tag zugänglich zu halten. Der Zugriff auf die Plattform kann jedoch aufgrund von technischen Wartungs-, Migrations- oder Aktualisierungsvorgängen oder aufgrund von Ausfällen oder Einschränkungen im Zusammenhang mit dem Betrieb des Netzwerks ohne Vorankündigung vorübergehend ausgesetzt werden.

Darüber hinaus behält sich BlaBlaCar das Recht vor, die Plattform oder Teile davon für einige Benutzer zu ändern, um neue Funktionen zu testen und ein besseres Benutzererlebnis zu bieten, sowie den Zugriff auf die Plattform oder ihre Funktionen nach eigenem Ermessen ganz oder teilweise vorübergehend oder dauerhaft zu ändern oder auszusetzen.

**14\. Änderung der AGB**
-------------------------

Diese AGB und die durch Bezugnahme integrierten Dokumente drücken die gesamte Vereinbarung zwischen Ihnen und BlaBlaCar in Bezug auf Ihre Nutzung der Dienste aus. Alle anderen Dokumente und Erwähnungen auf der Plattform (FAQ usw.) dienen nur als Richtlinie.

BlaBlaCar kann diese AGB ändern, um sie an ihr technologisches oder kommerzielles Umfeld anzupassen oder um neuen Rechtsvorschriften zu entsprechen. Falls eine solche Änderung erforderlich ist, werden wir Sie mindestens 14 Tage vor Inkrafttreten der Änderungen per E-Mail informieren. Die Änderungen treten in Kraft, wenn Sie nicht innerhalb der in der Benachrichtigungs-E-Mail genannten Frist (mindestens 14 Tage) ausdrücklich und in Textform widersprechen. Über diese Frist und die Folgen Ihres Widerspruchs werden wir Sie nochmals informieren.

**15\. Anwendbares Recht und Streitigkeiten**
---------------------------------------------

Diese AGB sind in deutscher Sprache verfasst und unterliegen deutschem Recht.

**15.1. Internes Beschwerdemanagement**

Sie können unsere Entscheidungen in Bezug auf folgende Punkte anfechten:

* Mitgliederinhalte: wir haben zum Beispiel Mitgliederinhalte entfernt, die Sie bei der Nutzung der Plattform bereitstellen, deren Sichtbarkeit eingeschränkt oder Entfernung verweigert oder
* Ihr Konto: wir haben Ihren Zugang zur Plattform gesperrt

wenn wir diese Entscheidungen aufgrund der Tatsache getroffen haben, dass die Mitgliederinhalte illegale Inhalte darstellen oder nicht mit diesen AGB vereinbar sind. Das Verfahren für die Einlegung eines Rechtsbehelfs ist [hier](https://support.blablacar.com/s/article/Wie-du-gegen-die-Entfernung-von-Inhalten-oder-eine-Kontosperrung-Einspruch-erheben-kannst-1729197123024?language=de) beschrieben.

**15.2. Außergerichtliche Streitbeilegung**

Sie können Ihre Beschwerden in Bezug auf unsere Plattform oder unsere Dienste ggf. auch auf der von der Europäischen Kommission online gestellten Streitbeilegungsplattform vorbringen, die Sie [hier](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage) finden. Wir sind nicht verpflichtet und grundsätzlich nicht bereit, an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.

Auch können Sie bei einer Streitbeilegungsstelle in Ihrem Land einen Antrag stellen (eine Liste finden Sie [hier](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)).

Sie können einen Schlichtungsantrag auch bei der Schlichtungsstelle Reise & Verkehr e.V. stellen.  
Fasanenstraße 81, D-10623 Berlin, http://schlichtung-reise-und-verkehr.de.

**16\. Rechtliche Hinweise**  

-------------------------------

Die Plattform wird von Comuto SA, Gesellschaft mit beschränkter Haftung mit einem Grundkapital von 166,591.952 Euro, eingetragen im Handelsregister von Paris unter der Nummer 491.904.546 (USt.-Identnr.:FR76491904546) (innergemeinschaftliche Umsatzsteuer-Identifikationsnummer: DE 08005) veröffentlicht /45532), mit Sitz in 84, avenue de la République, 75011 Paris (Frankreich), E-Mail: [\[email protected\]](https://blog.blablacar.de/cdn-cgi/l/email-protection), vertreten durch seinen Präsidenten Nicolas Brusson.

Die Website wird auf Servern von Google Ireland Limited mit Sitz in Gordon House, Barrow Street, Dublin 4 (Irland) gehostet.

Comuto SA ist im Register der Reise- und Aufenthaltsveranstalter unter der folgenden Nummer eingetragen: IM075180037

Die finanzielle Garantie wird bereitgestellt von: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Paris – Frankreich.

Die Berufshaftpflichtversicherung ist abgeschlossen bei: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paris – Frankreich.

Comuto SA ist im Versicherungsvermittler-, Banken- und Finanzregister unter der Registernummer (Orias) 15003890 eingetragen.

Bei Fragen können Sie sich über dieses [Kontaktformular](https://support.blablacar.com/s/contactsupport?language=de) an Comuto SA wenden oder uns telefonisch unter +33 (0) 185762227 anrufen.

**17\. Digital Services Act (Gesetz über digitale Dienste)**
------------------------------------------------------------

**17.1.  Informationen zu den im Durchschnitt monatlich aktiven Nutzern des Dienstes in der Union**

Gemäß Artikel 24 Absatz 2 der Verordnung des Europäischen Parlaments und des Rates über einen Binnenmarkt für digitale Dienste und zur Änderung der Richtlinie 2000/31/EG („DSA“) müssen Anbieter von Online-Plattformen Informationen zu den im Durchschnitt monatlich aktiven Nutzern ihres Dienstes in der Union veröffentlichen, der als Durchschnitt der letzten sechs Monate errechnet wird. Zweck dieser Veröffentlichung ist es festzustellen, ob eine Online-Plattformanbieter das Kriterium für “sehr große Online-Plattformen” gemäß dem DSA erfüllt, d.h. den Schwellenwert von durchschnittlich 45 Millionen monatlich aktiven Nutzern in der Union überschreitet.

Am 17. Februar 2025 belief sich die durchschnittliche Anzahl der aktiven Nutzer von BlaBlaCar für den Zeitraum von August 2024 bis Januar 2025, bei einer Berechnung unter Berücksichtigung des Erwägungsgrundes 77 und des Artikels 3 der DSA (unter Berücksichtigung der Benutzer, die den Inhalten der Plattform während des 6-Monats-Zeitraums nur einmal ausgesetzt waren), vor Erlassung eines spezifischen delegierten Rechtsaktes, auf etwa 3,75 Millionen in der EU.

Diese Informationen werden nur zur Einhaltung der DSA-anforderungen veröffentlicht und sollten nicht für andere Zwecke herangezogen werden. Sie werden alle sechs Monate mindestens einmal aktualisiert. Unser Ansatz für die Erstellung dieser Berechnung kann sich im Verlaufe der Zeit weiterentwickeln, oder eine Anpassung notwendig machen, beispielsweise aufgrund von Produktänderungen der neuen Technologien.

**17.2. Kontaktstelle für Behörden**

Als Mitglied der zuständigen EU-Behörden, der EU-Kommission oder des europäischen Rats für digitale Dienste können Sie sich gemäß Artikel 11 der DAS, in Angelegenheiten, die die DAS betreffen mit uns per E-Mail unter [\[email protected\]](https://blog.blablacar.de/cdn-cgi/l/email-protection) in Verbindung setzen.

Sie können uns in englischer und in französischer Sprache kontaktieren.

Beachten Sie bitte, dass diese E-Mail-Adresse nicht für die Kommunikation mit Mitgliedern verwendet wird. Bei Fragen zur Nutzung von BlaBlaCar können Sie uns als Mitglied über dieses [Kontaktformular](https://support.blablacar.com/s/contactsupport?language=de) kontaktieren.

**18\. Inanspruchnahme von BlaBlaBono Energético (nur für Spanien) – gültig ab dem 17. Januar 2025**
----------------------------------------------------------------------------------------------------

Fahrgäste, die berechtigt sind, das BlaBlaBono Energético-Angebot auf der Website [www.blablacar.es](http://www.blablacar.es/) in Anspruch zu nehmen, haben die Möglichkeit, mit ihrem BlaBlaBono-Bonus auf der Plattform [www.blablacar.de](http://www.blablacar.de/) oder in der App in Deutschland eine Mitfahrgelegenheit in Spanien zu buchen. Es gelten die [AGB](https://blog.blablacar.es/about-us/terms-and-conditions?_gl=1*ku9iqf*_gcl_au*MTA4MTk1MTk1OS4xNzMyNzg4NjE5).

* * *

**Ab 19. April 2024 geltende Allgemeine Geschäftsbedingungen**

**1\. Gegenstand**
------------------

Comuto SA (im Folgenden „**BlaBlaCar**“) hat eine Mitfahrplattform entwickelt, die auf einer Website unter der Adresse www.blablacar.de oder in Form einer mobilen Anwendung zugänglich ist und (i) darauf ausgelegt ist, Fahrer zu einem bestimmten Ziel zu bringen in Kontakt mit Fahrgästen, die in die gleiche Richtung fahren, um es ihnen zu ermöglichen, die Fahrt und damit die damit verbundenen Kosten zu teilen, und (ii) um Tickets für Busreisen zu buchen, die von von BlaBlaCar autorisierten Busunternehmen (im Folgenden als „**Plattform**“ bezeichnet durchgeführt werden).

Diese Allgemeinen Geschäftsbedingungen haben den Zweck, den Zugang und die Nutzungsbedingungen der Plattform zu regeln. Bitte lesen Sie diese sorgfältig durch. Sie verstehen und erkennen an, dass BlaBlaCar nicht an Vereinbarungen, Verträgen oder vertraglichen Beziehungen jeglicher Art beteiligt ist, die zwischen den Mitgliedern seiner Plattform oder mit einem Busunternehmen eingegangen werden.

Indem Sie auf „Mit Facebook anmelden“ oder „Mit einer E-Mail-Adresse registrieren“ klicken, erkennen Sie an, dass Sie alle diese allgemeinen Nutzungsbedingungen gelesen und akzeptiert haben.

Wenn Sie Ihr Konto verwenden, um sich bei der BlaBlaCar-Plattform eines anderen Landes (z. B. www.blablacar.com.br) anzumelden, beachten Sie bitte, dass (i) die Geschäftsbedingungen dieser Plattform (ii) die Datenschutzrichtlinie dieser Plattform und (iii) die gesetzlichen Regeln und Vorschriften dieses Landes gelten. Dies bedeutet auch, dass Informationen Ihres Kontos, einschließlich personenbezogener Daten, möglicherweise an die juristische Person übermittelt werden müssen, die diese andere Plattform betreibt. Bitte beachten Sie, dass sich BlaBlaCar das Recht vorbehält, den Zugriff auf die Plattform für Benutzer zu beschränken, von denen vernünftigerweise festgestellt werden kann, dass sie sich außerhalb des Hoheitsgebiets der Europäischen Union befinden.

**2\. Definitionen**
--------------------

In diesem Dokument,

„**AGB**“ bezeichnet diese Allgemeinen Geschäftsbedingungen;

„**Anbieter der** **Hyperwallet-Zahlungslösung**” bezeichnetdie PayPal (Europe) S.à r.l. et Cie, S.C.A.

„**Anzeige**“ bezeichnet eine Anzeige über eine Fahrt, die von einem Fahrer auf der Plattform gepostet wird;

„**Bestellung**“ bezeichnet den Vorgang, durch den der Kunde seine Dienste bei BlaBlaCar Bus bucht, unabhängig von den verwendeten Mitteln, mit der einzigen Ausnahme des Kaufs von Tickets direkt an einer Verkaufsstelle, und der die Verpflichtung für den Fahrgast oder möglicherweise eine nach sich zieht Agent zur Zahlung des Preises für die entsprechenden Dienstleistungen;

„**BlaBlaCar**“ hat die in Artikel 1 oben festgelegte Bedeutung;

„**Buchung**“ hat die in Artikel 4.2.1 festgelegte Bedeutung. unter;

„**Buchungsbestätigung**“ hat die in Artikel 4.2.1 unten angegebene Bedeutung;

„**Busbetreiber**“ bezeichnet ein Unternehmen, das gewerbsmäßig Fahrgäste befördert und für dessen Fahrten Tickets auf der Plattform von BlaBlaCar vertrieben werden;

„**Busfahrt**“ bezeichnet die Fahrt, die Gegenstand einer Buswerbung auf der Plattform ist und für die der Busbetreiber im Austausch für den Preis Sitzplätze in den Busfahrzeugen vorschlägt;

„**Buswerbung**“ bezeichnet eine auf der Plattform eingestellte Anzeige für eine Busfahrt einer Busbetreiber;

„**Dienste**“ bezeichnet alle Dienste, die von BlaBlaCar über die Plattform bereitgestellt werden;

„**Etappe**“ hat die in Artikel 4.1 unten angegebene Bedeutung;

„**Facebook-Konto**“ hat die in Artikel 3.2 unten angegebene Bedeutung;

„**Fahrer**“ bezeichnet das Mitglied, das die Plattform nutzt, um anzubieten, eine andere Person gegen den Kostenbeitrag, für eine Fahrt und zu einem vom Fahrer allein festgelegten Zeitpunkt zu befördern;

„**Fahrgemeinschaftsfahrt**“ bezeichnet die Fahrt, die Gegenstand einer Fahrgemeinschaftsanzeige ist, die von einem Fahrer auf der Plattform veröffentlicht wird und für die er sich bereit erklärt, Fahrgäste gegen den Kostenbeitrag zu befördern;

„**Fahrt**“ bezieht sich unterschiedslos auf eine Busfahrt oder eine Fahrgemeinschaftsfahrt;

„**Hyperwallet-Zahlungslösung**” hat die Bedeutung, die ihm im untenstehenden Artikel 5.4.2.a zugewiesen wird;

„**Kunde**“ bezeichnet jede natürliche Person (Mitglied oder nicht), die entweder für sich selbst oder im Namen einer anderen Person, die der Fahrgast sein wird, ein Busticket über die Plattform kauft, um eine vom Busunternehmen durchgeführte Fahrt zu realisieren;

„**Konto**“ bezeichnet das Konto, das erstellt werden muss, um Mitglied zu werden und auf bestimmte von der Plattform angebotene Dienste zuzugreifen;

„**Kostenbeitrag**“ bezeichnet für eine bestimmte Fahrgemeinschaftsfahrt den Geldbetrag, der vom Fahrer angefordert und vom Fahrgast für seinen Beitrag zu den Reisekosten akzeptiert wird;

„**Mitfahrgelegenheitsanzeige**“ bezeichnet eine Anzeige für eine Fahrt, die von einem Fahrer auf der Plattform gepostet wird;

„**Mitglied**“ bezeichnet jede Person, die ein Konto auf der Plattform erstellt hat;

„**Mitgliederinhalt**“ hat die ihm in Artikel 11.2 unten zugewiesene Bedeutung;

„**Passagier**“ bezeichnet das Mitglied, das das Angebot angenommen hat, vom Fahrer befördert zu werden, oder gegebenenfalls die Person, in deren Namen ein Mitglied einen Sitzplatz gebucht hat;

„**Plattform**“ hat die in Artikel 1 oben festgelegte Bedeutung;

„**Preis**“ bezieht sich für eine bestimmte Busfahrt auf den Preis einschließlich aller Steuern, Gebühren und Kosten für relevante Dienstleistungen, die vom Kunden auf der Plattform zum Zeitpunkt der Bestätigung der Bestellung für einen Sitzplatz auf einem bestimmten Preis bezahlt werden Busfahrt;

„**Servicegebühr**“ hat die Bedeutung, die ihr in nachstehendem Artikel 5.2 zugewiesen wird.

„**Sitzplatz**“ bezeichnet den von einem Fahrgast gebuchten Sitzplatz im Fahrzeug des Fahrers oder in dem vom Busunternehmen durchgeführten Fahrzeug;

„**Ticket**“ bezeichnet den nominellen gültigen Beförderungstitel, der dem Verbraucher nach der Buchung einer Busfahrt als Nachweis des bestehenden Beförderungsvertrags zwischen den Fahrgästen und dem Busunternehmen, der den AGB des Verkaufs unterliegt, erteilt wird, unbeschadet etwaiger zusätzlich festgelegter besonderer Bedingungen zwischen dem Fahrgast und dem Busunternehmen und auf dem Ticket angegeben;

„**Transportdienste**“ bezieht sich auf die Transportdienste, die von einem Busreisenden abonniert und vom Busunternehmen bereitgestellt werden;

„**Verkaufsstelle**“ bezieht sich auf die physischen Schalter und Terminals, die auf der Website aufgeführt sind und an denen die Tickets zum Verkauf angeboten werden;

„**Verkaufsbedingungen**“ bezeichnet die Allgemeinen Verkaufsbedingungen des betreffenden Busunternehmens in Abhängigkeit von der vom Kunden ausgewählten Busreise und die besonderen Geschäftsbedingungen, die auf der Website verfügbar sind und vom Kunden als gelesen anerkannt wurden vor der Bestellung;

„**Website**“ bezeichnet die unter der Adresse www.blablacar.de erreichbare Website.

**3\. Registrierung auf der Plattform und Erstellung eines Kontos**
-------------------------------------------------------------------

### **3.1. Registrierungsbedingungen auf der Plattform**

Die Plattform darf von Personen ab 18 Jahren genutzt werden. Die Registrierung auf der Plattform durch Minderjährige ist strengstens untersagt. Durch den Zugriff auf, die Nutzung oder Registrierung auf der Plattform versichern und garantieren Sie, dass Sie mindestens 18 Jahre alt sind.

### **3.2. Erstellung eines Kontos**

Die Plattform ermöglicht es Mitgliedern, Fahrgemeinschaftsanzeigen zu posten und anzusehen und miteinander zu interagieren, um einen Sitzplatz zu buchen. Sie können die Anzeigen sehen, wenn Sie nicht auf der Plattform registriert sind. Sie können jedoch keine Fahrgemeinschaftsanzeige aufgeben oder einen Sitzplatz buchen, ohne zuvor ein Konto erstellt und Mitglied zu werden.

Um Ihr Konto zu erstellen, können Sie:

(i) entweder alle Pflichtfelder auf dem Registrierungsformular ausfüllen;  
(ii) oder melden Sie sich über unsere Plattform bei Ihrem Facebook-Konto an (im Folgenden als Ihr „Facebook-Konto“ bezeichnet). Durch die Nutzung dieser Funktionen erklären Sie sich damit einverstanden, dass BlaBlaCar auf Ihr Facebook-Konto zugreifen, auf der Plattform veröffentlichen und bestimmte Informationen von Ihrem Facebook-Konto aufbewahren wird. Sie können die Verknüpfung zwischen Ihrem Konto und Ihrem Facebook-Konto jederzeit über den Abschnitt „Verifizierungen“ Ihres Profils löschen. Wenn Sie mehr über die Verwendung Ihrer Daten aus Ihrem Facebook-Konto erfahren möchten, lesen Sie unsere Datenschutzrichtlinie und die von Facebook.

Um sich auf der Plattform zu registrieren, müssen Sie diese AGB gelesen und akzeptiert haben.

Bei der Erstellung Ihres Kontos verpflichten Sie sich unabhängig von der gewählten Methode, genaue und wahrheitsgemäße Informationen anzugeben und diese über Ihr Profil oder durch Benachrichtigung von BlaBlaCar zu aktualisieren, um ihre Relevanz und Genauigkeit während Ihrer gesamten Vertragsbeziehung mit BlaBlaCar zu gewährleisten.

Im Falle einer Registrierung per E-Mail erklären Sie sich damit einverstanden, das bei der Erstellung Ihres Kontos gewählte Passwort geheim zu halten und es niemandem mitzuteilen. Wenn Sie Ihr Passwort verlieren oder offenlegen, verpflichten Sie sich, BlaBlaCar unverzüglich zu informieren. Sie allein sind für die Nutzung Ihres Kontos durch Dritte verantwortlich, es sei denn, Sie haben BlaBlaCar ausdrücklich über den Verlust, die betrügerische Nutzung durch Dritte oder die Weitergabe Ihres Passworts an Dritte informiert.

Sie stimmen zu, keine anderen als die ursprünglich erstellten Konten unter Ihrer eigenen Identität oder der eines Dritten zu erstellen oder zu verwenden.

### **3.3. Überprüfung**

BlaBlaCar kann zum Zwecke der Transparenz, der Verbesserung des Vertrauens oder der Verhinderung oder Aufdeckung von Betrug ein System zur Überprüfung einiger der Informationen einrichten, die Sie in Ihrem Profil angeben. Dies ist insbesondere dann der Fall, wenn Sie Ihre Telefonnummer eingeben oder uns ein Ausweisdokument übermitteln.

Sie erkennen an und akzeptieren, dass jeder Verweis auf der Plattform oder den Diensten auf „verifizierte“ Informationen oder ähnliche Begriffe nur bedeutet, dass ein Mitglied das auf der Plattform oder den Diensten bestehende Verifizierungsverfahren erfolgreich bestanden hat, um Ihnen weitere Informationen zur Verfügung zu stellen über das Mitglied, mit dem Sie reisen möchten. BlaBlaCar kann die Wahrhaftigkeit, Zuverlässigkeit oder Gültigkeit der Informationen, die Gegenstand des Überprüfungsverfahrens sind, nicht garantieren.

**4\. Nutzung der Dienste**
---------------------------

### **4.1. Schalten von Anzeigen**

Als Mitglied können Sie unter der Voraussetzung, dass Sie die nachstehenden Bedingungen erfüllen, Mitfahrgelegenheitsanzeigen auf der Plattform erstellen und veröffentlichen, indem Sie Informationen über die Mitfahrgelegenheit eingeben, die Sie unternehmen möchten (Daten/Zeiten und Sammelstellen und Ankunft, Anzahl der angebotenen Sitzplätze, verfügbare Optionen, Höhe des Kostenbeitrags etc.).

Wenn Sie Ihre Mitfahrgelegenheitsanzeige aufgeben, können Sie die Meilensteinstädte angeben, in denen Sie zustimmen, anzuhalten, Passagiere abzuholen oder abzusetzen. Die Abschnitte der Mitfahrgelegenheit zwischen diesen Meilensteinstädten oder zwischen einer dieser Meilensteinstädte und dem Sammelpunkt oder Zielort der Mitfahrgelegenheit bilden „Etappen“.

Sie sind nur berechtigt, eine Anzeige zu schalten, wenn Sie alle folgenden Bedingungen erfüllen:

(i) Sie besitzen einen gültigen Führerschein;

(ii) Sie bieten Fahrgemeinschaftsanzeigen nur für Fahrzeuge an, die Sie besitzen oder mit ausdrücklicher Genehmigung des Eigentümers nutzen, und in allen Fällen, zu deren Nutzung Sie zum Zweck der Mitfahrgelegenheit berechtigt sind;

(iii) Sie sind und bleiben der Hauptfahrer des Fahrzeugs, das Gegenstand der Anzeige ist;

(iv) das Fahrzeug hat eine gültige Haftpflichtversicherung;

(v) Sie haben keine Kontraindikation oder medizinische Unfähigkeit zum Fahren;

(vi) das Fahrzeug, das Sie für die Reise verwenden möchten, ein Tourenwagen mit 4 Rädern und maximal 7 Sitzen ist, mit Ausnahme von Autos, die keinen Führerschein benötigen;

(vii) Sie beabsichtigen nicht, eine weitere Anzeige für dieselbe Mitfahrgelegenheit auf der Plattform zu veröffentlichen;

(viii) Sie bieten nicht mehr Sitzplätze an, als in Ihrem Fahrzeug verfügbar sind;

(ix) alle angebotenen Sitze haben einen Sicherheitsgurt, auch wenn das Fahrzeug mit Sitzen ohne Sicherheitsgurt zugelassen ist;

(x) ein Fahrzeug in einwandfreiem Zustand zu verwenden, das den geltenden gesetzlichen Bestimmungen und Gepflogenheiten entspricht, insbesondere mit einem aktuellen TÜV-Zertifikat;

(xi) Sie sind Verbraucher und handeln nicht als Gewerbetreibender.

Sie erkennen an, dass Sie allein für den Inhalt der Mitfahrgelegenheitsanzeige verantwortlich sind, die Sie auf der Plattform posten. Folglich versichern und garantieren Sie die Genauigkeit und Wahrhaftigkeit aller in Ihrer Mitfahrgelegenheitsanzeige enthaltenen Informationen, und Sie verpflichten sich, die Mitfahrgelegenheitsfahrt unter den in Ihrer Mitfahrgelegenheitsanzeige beschriebenen Bedingungen durchzuführen.

Ihre Mitfahrgelegenheitsanzeige wird auf der Plattform veröffentlicht und ist daher für Mitglieder und alle Besucher, auch Nichtmitglieder, sichtbar, die eine Suche auf der Plattform oder auf der Website der Partner von BlaBlaCar durchführen. BlaBlaCar behält sich das Recht vor, nach eigenem Ermessen eine Mitfahrgelegenheitsanzeige nicht zu veröffentlichen oder zu entfernen, wenn diese nicht den AGB entspricht oder es sie als sein Image, das der Plattform oder der Dienste schädigend betrachtet bzw. das Konto des Mitglieds, das eine solche Mitfahrgelegenheitsanzeige gemäß Abschnitt 9 dieser AGB veröffentlicht zu sperren.

Sie werden darüber informiert, dass Sie für den Fall, dass Sie sich durch die Nutzung der Plattform als Verbraucher ausgeben, obwohl Sie tatsächlich als Gewerbetreibender handeln, Sanktionen ausgesetzt sind, die durch das geltende Recht vorgesehen sind.

### **4.2. Sitzplatz reservieren**

BlaBlaCar hat ein System zur Online-Buchung von Sitzplätzen („Buchung“) für auf der Plattform angebotene Fahrten eingerichtet.

Die Buchungsmethoden für einen Sitzplatz hängen von der Art der geplanten Reise ab.

BlaBlaCar stellt den Nutzern auf der Plattform eine Suchmaschine zur Verfügung, die auf verschiedenen Suchkriterien basiert (Ursprungsort, Ziel, Daten, Anzahl der Reisenden usw.). Bestimmte zusätzliche Funktionalitäten werden auf der Suchmaschine bereitgestellt, wenn der Benutzer mit seinem Konto verbunden ist. BlaBlaCar lädt den Benutzer ein, unabhängig vom verwendeten Buchungsverfahren, die Suchmaschine sorgfältig zu konsultieren und zu verwenden, um das Angebot zu ermitteln, das seinen Bedürfnissen am besten entspricht. [Hier](https://blog.blablacar.de/about-us/transparenz-der-plattformen) können weitere Informationen gefunden werden. Der Kunde, der eine Busfahrt an einer Verkaufsstelle bucht, kann auch den Busbetreiber oder den Schaltermitarbeiter bitten, die Suche durchzuführen.

### **4.2.1 Mitfahrgelegenheit**

Wenn ein Passagier an einer Anzeige für Mitfahrgelegenheiten interessiert ist, die von der Buchung profitiert, kann er eine Online-Buchungsanfrage stellen. Diese Buchungsanfrage wird entweder (i) automatisch akzeptiert (wenn der Fahrer diese Option beim Aufgeben seiner Anzeige ausgewählt hat) oder (ii) manuell vom Fahrer akzeptiert. Zum Zeitpunkt der Buchung zahlt der Passagier online den Kostenbeitrag und gegebenenfalls die Servicegebühr. Nach Eingang der Zahlung bei BlaBlaCar und ggf. Validierung der Buchungsanfrage durch den Fahrer erhält der Fahrgast eine Buchungsbestätigung (die „Buchungsbestätigung“).

Wenn Sie ein Fahrer sind und sich entschieden haben, die Buchungsanfragen beim Posten Ihrer Mitfahrgelegenheitsanzeige manuell zu bearbeiten, müssen Sie auf jede Buchungsanfrage innerhalb des vom Fahrgast während der Buchungsanfrage angegebenen Zeitraums antworten. Andernfalls erlischt die Buchungsanfrage automatisch und dem Passagier werden alle zum Zeitpunkt der Buchungsanfrage gezahlten Beträge zurückerstattet, sofern zutreffend.

Zum Zeitpunkt der Buchungsbestätigung sendet Ihnen BlaBlaCar die Telefonnummer des Fahrers (wenn Sie der Beifahrer sind) oder des Beifahrers (wenn Sie der Fahrer sind), wenn das Mitglied der Anzeige seiner Telefonnummer zugestimmt hat. Sie sind dann allein dafür verantwortlich, den Vertrag auszuführen, der Sie an das andere Mitglied bindet.

### **4.2.2 Busfahrt**

Für Busfahrten ermöglicht BlaBlaCar die Buchung von Bustickets für eine bestimmte Busfahrt über die Plattform.

Die Transportleistungen unterliegen den Verkaufsbedingungen des jeweiligen Busunternehmens, abhängig von der vom Kunden gewählten Fahrt, die vom Kunden vor der Bestellung akzeptiert werden müssen. BlaBlaCar bietet keine Transportdienstleistungen in Bezug auf Busreisen an, die Busunternehmen sind alleinige Vertragspartei der Verkaufsbedingungen. Sie erkennen an, dass die Buchung von Sitzplätzen für eine bestimmte Busreise den Verkaufsbedingungen des jeweiligen Busunternehmens unterliegt.

BlaBlaCar macht Sie darauf aufmerksam, dass bestimmte vom Busunternehmen angebotene und auf der Website erwähnte Transportdienste eingestellt werden können, insbesondere aus klimatischen Gründen, saisonalen Gründen oder im Falle höherer Gewalt.

### **4.2.3 Nominativer Charakter der Sitzplatzreservierung und Nutzungsbedingungen der Dienste im Namen eines Dritten**

Jede Nutzung der Dienste, sei es als Fahrgast oder als Fahrer, ist persönlich. Sowohl der Fahrer als auch der Beifahrer müssen der Identität entsprechen, die BlaBlaCar und gegebenenfalls den anderen an der Fahrgemeinschaftsfahrt teilnehmenden Mitgliedern oder dem Busunternehmen mitgeteilt wurde.

BlaBlaCar gestattet seinen Mitgliedern jedoch, einen oder mehrere Sitzplätze im Namen eines Dritten zu reservieren. In diesem Fall verpflichten Sie sich, dem Fahrer oder dem Busunternehmen zum Zeitpunkt der Buchung den Vornamen, das Alter und die Telefonnummer der Person, in deren Namen Sie einen Sitzplatz buchen, genau mitzuteilen. Es ist strengstens verboten, einen Sitzplatz für einen allein reisenden Minderjährigen unter 13 Jahren für eine Fahrgemeinschaftsfahrt zu reservieren. Für den Fall, dass Sie einen Sitzplatz für eine Fahrgemeinschaftsfahrt für einen allein reisenden Minderjährigen über 13 Jahren reservieren, stimmen Sie zu, die vorherige Zustimmung des Fahrers einzuholen und ihm eine ordnungsgemäß ausgefüllte und unterzeichnete Vollmacht der gesetzlichen Vertreter vorzulegen. In Bezug auf eine Busreise laden wir Sie ein, die AGB des betreffenden Busunternehmens zu lesen.

Darüber hinaus ist die Plattform für die Reservierung von Plätzen für natürliche Personen bestimmt. Es ist daher verboten, einen Sitzplatz für den Transport von Gegenständen, Paketen, allein reisenden Tieren oder anderen materiellen Gegenständen zu reservieren.

Darüber hinaus ist es untersagt, eine Mitfahrgelegenheitsanzeige für einen anderen Fahrer als Sie selbst zu veröffentlichen.

### **4.3. Mitgliederinhalte, Moderation und Bewertungssystem**

### **4.3.1. Funktionsweise des Bewertungssystems**

BlaBlaCar ermutigt Sie, eine Bewertung über einen Fahrer (wenn Sie ein Beifahrer sind) oder einen Beifahrer (wenn Sie ein Fahrer sind) zu hinterlassen, mit dem Sie eine Fahrt geteilt haben oder mit dem Sie eine Fahrt teilen sollten (was heißt, eine Fahrt gebucht haben). Andererseits sind Sie nicht berechtigt, eine Bewertung für einen anderen Passagier abzugeben, wenn Sie selbst ein Passagier waren, oder für ein Mitglied, mit dem Sie nicht gereist sind oder mit dem Sie nicht reisen sollten. Sie haben die Möglichkeit, innerhalb von 14 Tagen nach der Reise eine Bewertung abzugeben.

Ihre Bewertung sowie gegebenenfalls die von einem anderen Mitglied über Sie hinterlassene Bewertung wird erst nach dem kürzeren der folgenden Zeiträume sichtbar und auf der Plattform veröffentlicht: (i) unmittelbar nachdem Sie beide eine Mitteilung hinterlassen haben oder (ii) vergangen 14 Tage nach der ersten Benachrichtigung.

Sie haben die Möglichkeit, auf eine Bewertung zu antworten, die ein anderes Mitglied in Ihrem Profil hinterlassen hat. Die Bewertung und gegebenenfalls Ihre Antwort werden in Ihrem Profil veröffentlicht.

### **4.3.2. Moderation**

**a. Mitgliederinhalte**

Sie erkennen an und akzeptieren, dass BlaBlaCar Mitgliederinhalte gemäß der in Abschnitt 11.2. enthaltenen Definition vor deren Veröffentlichung unter Verwendung automatisierter Tools oder manuell moderieren kann. Wenn BlablaCar der Ansicht ist, dass ein solcher Mitgliederinhalt gegen geltende Gesetze oder diese AGB verstößt, behält es sich das Recht vor:

* Die Veröffentlichung zu unterbinden oder solche Mitgliederinhalte zu löschen
* Das Mitglied zu verwarnen, indem es an seine Verpflichtung erinnert wird, geltende Gesetze oder diese AGB einzuhalten, bzw.
* Einschränkende Maßnahmen gemäß Abschnitt 9 dieser AGB anzuwenden.

Die Verwendung solcher automatisierten Tools oder manuelle Moderation durch BlaBlaCar kann nicht als Verpflichtung zur Überwachung oder zur aktiven Suche nach rechtswidrigen Aktivitäten bzw. auf der Plattform veröffentlichten Mitgliederinhalten ausgelegt werden und begründet, soweit nach geltendem Recht zulässig, keinerlei Haftung seitens BlaBlaCar.

**b. Nachrichten**

Der Austausch von Nachrichten zwischen Mitgliedern über die Plattform (“Nachrichten”) dient nur dem Informationsaustausch über Fahrgemeinschaften.

BlaBlaCar kann zur Vorbeugung von Betrug, zur Verbesserung der Dienste, zu Zwecken der Kundenbetreuung und zur Ausführung der mit unseren Mitgliedern abgeschlossenen Verträge (wie diese AGB) durch die Verwendung automatisierter Software und Algorithmen den Inhalt von Nachrichten ermitteln. Im Falle der Ermittlung solcher Inhalte in einer Nachricht, die Anzeichen für betrügerisches oder illegales Verhalten, die Umgehung der Plattform oder einen sonstigen Verstoß gegen diese AGB darstellen:

* Kann ein solcher Inhalt nicht veröffentlicht werden
* Das Mitglied, das die Nachricht versandt hat, kann eine Verwarnung erhalten, in der es an seine Verpflichtung erinnert wird, geltende Gesetze oder diese AGB einzuhalten, oder
* Das Konto des Mitglieds kann gemäß Abschnitt 9 dieser AGB gesperrt werden.

Die Verwendung solcher automatisierter Software durch BlaBlaCar kann nicht als Verpflichtung zur Überwachung oder zur aktiven Suche nach rechtswidrigen Aktivitäten bzw. auf der Plattform veröffentlichten Mitgliederinhalten ausgelegt werden, und begründet, soweit nach geltendem Recht zulässig, keinerlei Haftung seitens BlaBlaCar. 

###  **4.3.3. Grenze**

Gemäß Abschnitt 9 dieser AGB behält sich BlaBlaCar das Recht vor, Ihr Konto zu sperren, Ihren Zugriff auf die Dienste einzuschränken oder diese AGB zu kündigen, falls (i) Sie mindestens drei Bewertungen erhalten haben und (ii) eine Bewertung oder der Durchschnitt der erhaltenen Bewertungen höchstens gleich 3 ist, je nachdem wie schwerwiegend die in der Bewertung ausgedrückte Kritik ist.

**5\. Finanzielle Konditionen**
-------------------------------

Der Zugriff auf und die Registrierung auf der Plattform sowie die Recherche, Konsultation und Veröffentlichung von Anzeigen sind kostenlos. Andererseits ist die Reservierung gemäß den unten beschriebenen Bedingungen zahlbar.

### **5.1. Kostenbeitrag und Preis**

5.1.1. Im Rahmen einer Fahrgemeinschaftsfahrt wird die Höhe des Kostenbeitrags von Ihnen als Fahrer in eigener Verantwortung festgelegt. Es ist strengstens untersagt, aus der Nutzung unserer Plattform Vorteile zu ziehen. Folglich verpflichten Sie sich, die Höhe des Kostenbeitrags, den Sie von Ihren Fahrgästen verlangen, auf die Kosten zu beschränken, die Sie tatsächlich für die Durchführung der Fahrgemeinschaftsfahrt tragen. Andernfalls tragen Sie allein das Risiko einer Neuqualifizierung der über die Plattform durchgeführten Transaktion.

Bei der Veröffentlichung einer Mitfahranzeige schlägt BlaBlaCar einen Kostenbeitrag vor, der insbesondere die Art der Mitfahrgelegenheit und die zurückgelegte Strecke berücksichtigt. Dieser Betrag ist nur ein Richtwert und es liegt an Ihnen, ihn nach oben oder unten zu ändern, um die Kosten zu berücksichtigen, die Sie tatsächlich für die Mitfahrgelegenheit tragen. Um Missbrauch zu vermeiden, schränkt BlaBlaCar die Möglichkeiten zur Anpassung der Höhe des Kostenbeitrags ein.

5.1.2. Bei Busfahrten wird der Preis pro Sitzplatz nach eigenem Ermessen vom Busunternehmen festgelegt. Der Kunde wird gebeten, sich auf die entsprechenden Verkaufsbedingungen zu beziehen, um herauszufinden, wie er die Bestellung aufgibt und das Ticket bezahlt.

### **5.2. Servicegebühr**

BlaBlaCar erhebt als Gegenleistung für die Nutzung der Plattform zum Zeitpunkt der Buchung eine Servicegebühr (im Folgenden „**Servicegebühr**“ genannt). Falls diese anfällt, werden Sie vorher darüber informiert, dass die Servicegebühr anfällt.

Die geltenden Methoden zur Berechnung der Servicegebühr sind [hier](https://blog.blablacar.de/blablalife/lp/servicegebuehr) zu finden und dienen lediglich der Information und haben keinen vertraglichen Wert. Die Servicegebühr kann nach verschiedenen Faktoren berechnet werden, insbesondere nach der Länge der Fahrt und dem Kostenbeitrag. BlaBlaCar behält sich das Recht vor, die Methoden zur Berechnung der Servicegebühr jederzeit zu ändern. Diese Änderungen haben keinen Einfluss auf die Servicegebühr, die Sie vor dem Datum des Inkrafttretens dieser Änderungen akzeptiert haben.

Bei grenzüberschreitenden Fahrten beachten Sie bitte, dass die Methoden zur Berechnung der Servicegebühr und der geltenden Mehrwertsteuer je nach Abholort und/oder Zielort der Fahrt variieren.

Bei der Nutzung der Plattform für grenzüberschreitende Fahrten oder außerhalb Deutschlands kann die Servicegebühr von einer Tochtergesellschaft von BlaBlaCar erhoben werden, die die lokale Plattform betreibt.

### **5.3. Rundung**

Sie erkennen an und stimmen zu, dass BlaBlaCar nach eigenem Ermessen den Kostenbeitrag und die Servicegebühr auf ganze Zahlen oder Dezimalzahlen auf- oder abrunden kann.

### **5.4 Zahlungsmodalitäten und Rückzahlung des Kostenbeitrags an den Fahrer bzw. des Fahrpreises an die Busunternehmen**

Sie als Fahrer haben die Möglichkeit, sich den Kostenbeitrag von den Fahrgästen dieser Fahrt bezahlen zu lassen, indem Sie sich für eine Online-Zahlung entscheiden (siehe 5.4.1). Diese Möglichkeit kann in einigen Fällen bei Fahrten, bei denen Abfahrt oder Ankunft außerhalb der Landesgrenzen liegen, eingeschränkt sein.

In diesem Fall verpflichtet sich der Mitfahrer, den Kostenbeitrag spätestens beim Verlassen des Fahrzeugs am Zielort an den Fahrer zu zahlen. Der Fahrer ist nicht berechtigt, die Zahlung des Kostenbeitrags ganz oder teilweise vor Beendigung der Mitfahrgelegenheit zu verlangen. In allen anderen Fällen gelten die nachstehenden Bedingungen.

### **5.4.1 Anweisung des Fahrers (Mandat)**

Durch die Nutzung der Plattform als Fahrer für Fahrgemeinschaften bestätigen Sie die [allgemeinen Geschäftsbedingungen](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) des Anbieters der Hyperwallet-Zahlungslösung – unseres Zahlungsanbieters, der über die Hyperwallet-Zahlungslösung gemäß Artikel 5.4.2.a die Überweisungen der Kostenbeiträge an die Fahrer abwickelt – durchgelesen und akzeptiert haben. Diese allgemeinen Geschäftsbedingungen werden im Folgenden als „Hyperwallet AGB“ bezeichnet.

Beachten sie bitte, dass die Ausnahmen der Richtlinie (EU) 2015/2366 des Europäischen Parlaments und des Rats vom 25. November 2015 über Zahlungsdienste im Binnenmarkt, die in Artikel 11.4 (i) und (ii) der Hyperwallet-AGB aufgeführt sind, nicht für Plattformmietglieder gelten, die keine Gewerbetreibenden sind.

Als Fahrer weisen Sie an:

* BlaBlaCar beauftragt den Anbieters der Hyperwallet-Zahlungslösung, die Übertragung des Kostenbeitrags vorzunehmen, und
* Der Anbieters der Hyperwallet-Zahlungslösung muss die Überweisung des Kostenbeitrags auf Ihr Bank- oder PayPal-Konto in Ihrem Namen und in Ihrem Auftrag gemäß den Hyperwallet-AGB vornehmen.

Im Rahmen einer Mitfahrgelegenheit und nach manueller oder automatischer Annahme der Buchung wird der vom Fahrgast gezahlte Kostenbeitrag und die Servicegebühr auf einem vom Anbieter der Hyperwallet-Zahlungslösung verwalteten Treuhandkonto verwahrt.

### **5.4.2 Zahlung des Kostenbeitrags an den Fahrer**

Nach der Fahrgemeinschaftsreise haben die Fahrgäste eine Frist von 24 Stunden, um einen Anspruch auf die Reise bei BlaBlaCar geltend zu machen. Erfolgt innerhalb dieser Frist kein Anspruch des Fahrgastes, betrachtet BlaBlaCar die Reise als bestätigt.

Ab dem Zeitpunkt dieser ausdrücklichen oder stillschweigenden Bestätigung haben Sie als Fahrer ein Guthaben auf Ihrem Konto. Diese Gutschrift entspricht dem vom Passagier zum Zeitpunkt der Bestätigung der Buchung gezahlten Gesamtbetrag abzüglich der Servicegebühr, d. h. dem vom Passagier gezahlten Kostenbeitrag.

**a. Hyperwallet-Zahlungslösung**

Die Hyperwallet-Zahlungslösung ist eine vom Anbieter der Hyperwallet-Zahlungslösung bereitgestellte Zahlungsdienstleistung. Um Zweifel auszuschließen wird darauf hingewiesen, dass BlaBlaCar Mitgliedern keine Zahlungsabwicklungsdienste zur Verfügung stellt und nicht in den Besitz der Gelder von Mitgliedern gelangt.

Wenn die Fahrgemeinschaftsfahrt vom Fahrgast bestätigt wird, haben Sie als Fahrer die Möglichkeit, Ihren Kostenbeitrag auf Ihr Bankkonto oder auf Ihr PayPal-Konto überweisen zu lassen.

Zu diesem Zwecke können Sie die Hyperwallet-Zahlungslösung nutzen, indem Sie in einen direkten Vertrag mit dem Anbieter der Hyperwallet-Zahlungslösung eintreten und die Hyperwallet-AGB annehmen.

Um die erste Überweisung des Kostenbeitrags zu erhalten, werden Sie aufgefordert, eine Zahlungsmethode auszuwählen, also Ihre Bankverbindung oder Ihr PayPal-Konto zu verknüpfen und Ihre Postanschrift und Ihr Geburtsdatum anzugeben. Sollten Sie dies unterlassen oder falsche, ungenaue oder veraltete Informationen angeben, wird der Kostenbeitrag nicht überwiesen.

Die Überweisung wird durch den Anbieter der Hyperwallet-Zahlungslösung ausgeführt.

Nach Ablauf der in den geltenden Gesetzen festgelegten Verjährungsfristen gilt jeder Kostenbeitrag, der von Ihnen nicht von Ihnen als Fahrer über die Hyperwallet-Zahlungslösung eingefordert wurde, als Eigentum von BlaBlaCar.

**b. KYC-Prüfungen**

Bei der Nutzung der Hyperwallet-Zahlungslösung akzeptieren Sie, dass Sie möglicherweise den vom Anbieter der Hyperwallet-Zahlungslösung angewandten Regulierungsverfahren gemäß dessen [Hyperwallet AGB](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) unterliegen.

Diese Verfahren können Identitätsprüfungen und andere Anforderungen des auf den vom Anbieter der Hyperwallet-Zahlungslösung festgelegten Kriterien beruhenden “Know Your Customer” (KYC)-Verfahrens umfassen. Diese Kriterien können finanzielle Schwellenwerte umfassen, die vom Gesamtbetrag der Auszahlungen des Kostenbeitrags abhängen, den Sie erhalten.

Zu Zwecken der KYC-Prüfung können Sie aufgefordert werden, zusätzliche, vom Anbieter der Hyperwallet-Zahlungslösung angeforderte Informationen zu übermitteln. Sollten Sie die angeforderten Unterlagen nicht übermitteln, können Sie den Kostenbeitrag erst dann erhalten, wenn Ihre Identität bestätigt wurde.

Möglicherweise muss der Anbieter der Hyperwallet-Zahlungslösung in manchen Situationen die Überweisung von Kostenbeiträgen oder den Zugriff auf die Zahlungslösung im Einklang mit den geltenden Gesetzen aussetzen. BlaBlaCar hat keinerlei Einfluss auf solche Maßnahmen des Anbieters der Hyperwallet-Zahlungslösung.

Die vom Anbieter der Hyperwallet-Zahlungslösung durchgeführten KYC-Prüfungen erfolgen getrennt und unabhängig von den Prüfungen, die BlaBlaCar in Verbindung mit der Nutzung der Plattform und in Übereinstimmung mit diesen Bestimmungen und der Datenschutzrichtlinie vornehmen kann.

### **5.4.3 Zahlung des Preises**

Die Zahlung für jede Bestellung, die über die Plattform getätigt wird, erfolgt auf eine der unten autorisierten Weisen. Der Kunde hat die Möglichkeit, die Informationen zu einer oder mehreren Kreditkarten in seinem Kundenkonto zu speichern, damit er diese Informationen bei späteren Zahlungen nicht systematisch eingeben muss.

Die Zahlungsmittel können je nach von Ihnen verwendeter BlaBlaCar-Plattform variieren. Die autorisierten Zahlungsmittel lauten wie folgt:  
– Bank-Kreditkarte (die akzeptierten Karten werden auf der Plattform angezeigt. Falls Sie eine Co-Branding-Karte besitzen, können Sie in der Zahlungsphase eine bestimmte Karte auswählen)  
– Paypal  
– Gutscheine  
– Apple Pay, Google Pay

Vor der vollständigen und wirksamen Zahlung des Preises der vom Kunden ausgewählten Transportdienstleistungen wird keine Auftragsbestätigung ausgestellt. Wenn die Zahlung aus einem vom Kunden zu vertretenden Grund unregelmäßig, unvollständig oder nicht erfolgt ist, wird die Bestellung sofort storniert.

Die Bestellung wird bestätigt, indem eine E-Mail an den Kunden gesendet wird, die die Einzelheiten der Bestellung enthält.

Der Passagier wird gebeten, die Einstellungen für den Posteingang seiner E-Mail-Adresse zu überprüfen und insbesondere sicherzustellen, dass die Bestätigungs-E-Mail nicht direkt an Spam gesendet wird.

Die Auftragsbestätigung ist endgültig. Folglich führen alle Änderungen entweder zu einem Umtausch oder einer Stornierung gemäß den Bedingungen der geltenden AGB. Es liegt in der Verantwortung des Kunden sicherzustellen, dass die Transportdienste gemäß den Bedürfnissen und Erwartungen des Passagiers ausgewählt werden. Die Richtigkeit der vom Kunden eingegebenen personenbezogenen Daten liegt in der Verantwortung des letzteren, es sei denn, BlaBlaCar hat nachweislich versäumt, diese personenbezogenen Daten zu erfassen, zu speichern oder zu schützen.

### **6\. Nicht kommerzieller und nicht geschäftlicher Zweck der Dienste und der Plattform**

Sie erklären sich damit einverstanden, die Dienste und die Plattform nur zu nutzen, um auf nicht geschäftlicher und nicht kommerzieller Basis mit Personen in Kontakt zu treten, die eine Mitfahrgelegenheit mit Ihnen teilen oder einen Sitzplatz im Rahmen einer Busfahrt buchen möchten.

Im Zusammenhang mit der Mitfahrgelegenheit haben Sie anerkannt, dass die Verbraucherrechte aus dem Verbraucherschutzrecht der Union nicht für Ihre Beziehung zu anderen Mitgliedern gelten.

Als Fahrer erklären Sie sich damit einverstanden, keinen höheren Kostenbeitrag zu verlangen, als die Kosten, die Ihnen tatsächlich entstehen und die einen Gewinn generieren können, mit der Maßgabe, dass Sie als Fahrer im Rahmen einer Kostenbeteiligung Ihren eigenen Anteil an den verursachten Kosten tragen müssen durch die Mitfahrgelegenheit. Sie sind allein dafür verantwortlich, die Kosten zu berechnen, die Ihnen für die Mitfahrgelegenheit entstehen, und zu überprüfen, ob der von Ihren Fahrgästen geforderte Betrag die Ihnen tatsächlich entstehenden Kosten (ohne Ihren Anteil an den Kosten) nicht übersteigt, insbesondere unter Bezugnahme auf den geltenden Steuertarif.

BlaBlaCar behält sich das Recht vor, Ihr Konto zu sperren, wenn Sie ein Fahrzeug mit Chauffeur oder ein anderes Geschäftsfahrzeug oder Taxi oder einen Firmenwagen nutzen und dadurch einen Gewinn mit der Plattform erzielen. Sie stimmen zu, BlaBlaCar auf einfache Anfrage hin eine Kopie Ihres Fahrzeugscheins und/oder eines anderen Dokuments zur Verfügung zu stellen, aus dem hervorgeht, dass Sie zur Nutzung dieses Fahrzeugs auf der Plattform berechtigt sind und keinen Gewinn daraus ziehen.

Gemäß Abschnitt 9 der AGB behält sich BlaBlaCar außerdem das Recht vor, Ihr Konto zu sperren, Ihren Zugang zu den Diensten einzuschränken oder diese AGB zu kündigen, falls Sie auf der Plattform Aktivitäten durchführen, die aufgrund der Art der angebotenen Fahrten, ihrer Häufigkeit oder der Anzahl der Passagiere auftreten durchgeführt und der angeforderte Kostenbeitrag für Sie zu einer Gewinnsituation führt oder BlaBlaCar aus irgendeinem Grund vermuten lässt, dass Sie auf der Plattform einen Gewinn erzielen.

**7\. Stornierungsbedingungen**
-------------------------------

### **7.1 Rückzahlungsbedingungen im Falle einer Stornierung**

Nur Fahrgemeinschaftsfahrten sind Gegenstand dieser Stornierungsbedingungen; BlaBlaCar bietet keinerlei Garantie im Falle einer Stornierung aus irgendeinem Grund. Im Zusammenhang mit Busreisen gelten für die Stornobedingungen die Verkaufs-AGB der Busunternehmen. Der Kunde wird gebeten, sich auf die jeweiligen Verkaufsbedingungen zu beziehen, um die Umtausch- und Stornierungsbedingungen seiner Busreise anzuerkennen.

7.1.1. Rückerstattungsbedingungen im Falle einer Stornierung einer Mitfahrgelegenheit mit Online-Zahlung des Kostenbeitrags

Die Stornierung eines Sitzplatzes auf einer Mitfahrgelegenheit durch den Fahrer oder den Beifahrer nach der Buchungsbestätigung unterliegt den nachstehenden Bestimmungen:

– Im Falle einer Stornierung durch den Fahrer wird dem Fahrgast der gesamte gezahlte Betrag (d.h. der Kostenbeitrag und die Servicegebühr) zurückerstattet. Dies ist insbesondere dann der Fall, wenn der Fahrer eine Fahrgemeinschaftsfahrt storniert oder 15 Minuten nach der vereinbarten Zeit nicht am Treffpunkt eingetroffen ist;

– Im Falle einer Stornierung durch den Passagier:

* Storniert der Passagier mehr als 24 Stunden vor der geplanten Abfahrtszeit, wie in der Anzeige angegeben, wird dem Passagier nur der Kostenbeitrag erstattet. Die Servicegebühr wird von BlaBlaCar einbehalten und der Fahrer erhält keinen Anteil des Betrags.
* Wenn der Passagier 24 Stunden oder weniger vor der geplanten Abfahrtszeit, wie in der Anzeige angegeben, und mehr als 30 Minuten nach der Buchungsbestätigung storniert, wird dem Passagier die Hälfte des zum Zeitpunkt der Buchung gezahlten Kostenbeitrags zurückerstattet, die Servicegebühr wird von BlaBlaCar einbehalten und der Fahrer wird 50 % des Kostenbeitrags über die Hyperwallet-Zahlungslösung erhalten;
* Wenn der Passagier 24 Stunden oder weniger vor der geplanten Abfahrtszeit, wie in der Mitfahrgelegenheitsanzeige angegeben, und 30 Minuten oder weniger nach der Buchungsbestätigung storniert, wird dem Passagier der gesamte Kostenbeitrag über die Hyperwallet-Zahlungslösung erstattet. Die Servicegebühr wird von BlaBlaCar einbehalten und der Fahrer erhält keinen Anteil des Betrags.
* Storniert der Fahrgast nach der in der Mitfahrgelegenheitsanzeige genannten geplanten Abfahrtszeit oder ist er 15 Minuten nach der vereinbarten Zeit nicht am Treffpunkt eingetroffen, erhält der Fahrer den gesamten Kostenbeitrag über die Hyperwallet-Zahlungslösung und BlaBlaCar behält die Servicegebühr ein.

Die Methode zur Rückerstattung der Servicegebühr hängt von der Methode ab, die zur Zahlung der Servicegebühr verwendet wurde, während der Zeitrahmen für die Rückerstattung der Servicegebühr vom Zeitpunkt der Überweisungen durch die Banken abhängt.

Erfolgt eine Stornierung vor der Abfahrt und zu Lasten des Passagiers, werden die vom Passagier stornierten Sitzplätze automatisch anderen Passagieren zur Verfügung gestellt, die sie online buchen können, und sie unterliegen dementsprechend den Bedingungen dieser AGB.

BlaBlaCar erkennt nach eigenem Ermessen auf der Grundlage der verfügbaren Informationen die Rechtmäßigkeit der Rückerstattungsanträge an.

7.1.2. Rückerstattungsbedingungen im Falle einer Stornierung einer Mitfahrgelegenheit mit Barzahlung des Kostenbeitrags

– Im Falle einer Stornierung durch den Fahrer wird dem Mitfahrer die Servicegebühr zurückerstattet. Dies ist insbesondere der Fall, wenn der Fahrer: Die Mitfahranfrage nicht innerhalb des vorgesehenen Zeitraums bestätigte (falls der Fahrer eine solche Option gewählt hat); Eine Mitfahrgelegenheit storniert oder 15 Minuten nach der vereinbarten Zeit nicht am Treffpunkt erschienen ist;

–Im Falle einer Stornierung durch den Mitfahrer, behält BlaBlaCar die Servicegebühr ein.

### **7.2 Rücktrittsrecht**

Indem Sie diese AGB akzeptieren, akzeptieren Sie ausdrücklich, dass der Vertrag zwischen Ihnen und BlaBlaCar, der in der Verbindung mit einem anderen Mitglied besteht, vor Ablauf der Widerrufsfrist seit der Buchungsbestätigung ausgeführt werden soll, und verzichten ausdrücklich auf Ihr Widerrufsrecht gemäß §356 (4) des Bürgerliches Gesetzbuch (BGB).

Bei einer Fahrt mit Barzahlung gibt es keine Entschädigung für den Fahrer (im Falle einer Stornierung durch den Fahrgast oder den Fahrer).

### **8\. Verhalten von Nutzern der Plattform und Mitgliedern**

### **8.1. Verpflichtung aller Nutzer der Plattform**

Sie erkennen an, dass Sie allein dafür verantwortlich sind, alle Gesetze, Vorschriften und Verpflichtungen einzuhalten, die für Ihre Nutzung der Plattform gelten.

Darüber hinaus verpflichten Sie sich bei der Nutzung der Plattform und während Reisen:

(i) die Plattform nicht für berufliche, gewerbliche oder gewinnorientierte Zwecke zu nutzen, wenn Sie kein Busunternehmen sind;

(ii) BlaBlaCar (insbesondere bei der Erstellung oder Aktualisierung Ihres Kontos) oder den anderen Mitgliedern keine falschen, irreführenden, böswilligen oder betrügerischen Informationen zu senden;

(iii) keine verleumderischen, verletzenden, obszönen, pornografischen, vulgären, beleidigenden, aggressiven, unangebrachten, gewalttätigen, bedrohenden, belästigenden, rassistischen oder fremdenfeindlichen Inhalte (einschließlich Nachrichten) auf der Plattform zu sprechen oder sich in irgendeiner Weise zu verhalten oder Inhalte zu posten, oder mit sexuellen Konnotationen, Anstiftung zu Gewalt, Diskriminierung oder Hass, Förderung von Aktivitäten oder der Verwendung illegaler Substanzen oder allgemeiner illegal und im Widerspruch zu den geltenden Gesetzen, diesen AGB und den Zwecken der Plattform, die die Rechte von BlaBlaCar oder eines Dritten verletzen oder gegen die guten Sitten verstoßen können;

(iv) die Rechte und das Image von BlaBlaCar, insbesondere seine geistigen Eigentumsrechte, nicht zu verletzen;

(v) nicht mehr als ein Konto auf der Plattform zu eröffnen und kein Konto im Namen eines Dritten zu eröffnen;

(vi) nicht zu versuchen, das Online-Buchungssystem der Plattform zu umgehen, insbesondere indem Sie versuchen, einem anderen Mitglied oder einem Busunternehmen Ihre Kontaktdaten zu senden, um die Buchung außerhalb der Plattform vorzunehmen und die Servicegebühr zu umgehen;

(vii) kein anderes Mitglied, insbesondere über die Plattform, zu einem anderen Zweck als der Festlegung der Bedingungen der Mitfahrgelegenheit zu kontaktieren;

(viii) keine Zahlungen außerhalb der Plattform oder der Hyperwallet-Zahlungslösung zu akzeptieren oder zu tätigen;

(ix) diese AGB einzuhalten;

(x) um die Erlaubnis von BlaBlaCar einzuholen, bevor Sie beabsichtigen, kommerzielle Audio- oder Videoaufnahmen einer Fahrt zu machen.

### **8.2. Verpflichtungen der Fahrer**

Wenn Sie die Plattform als Fahrer nutzen, verpflichten Sie sich außerdem:

(i) alle für das Fahren und das Fahrzeug geltenden Gesetze, Vorschriften und Kodizes einzuhalten, insbesondere eine zum Zeitpunkt der Mitfahrgelegenheit gültige Haftpflichtversicherung zu unterhalten und im Besitz eines gültigen Führerscheins zu sein;

(ii) um zu überprüfen, ob Ihre Versicherung Mitfahrgelegenheiten abdeckt und ob Ihre Mitfahrer in Ihrem Fahrzeug als Dritte gelten und daher während der gesamten Reise durch Ihre Versicherung abgedeckt sind;

(iii) beim Autofahren kein Risiko einzugehen und keine Produkte mitzunehmen, die Ihre Aufmerksamkeit und Ihre Fähigkeit, wachsam und absolut sicher zu fahren, beeinträchtigen könnten;

(iv) um Anzeigen für Mitfahrgelegenheiten zu veröffentlichen, die nur tatsächlich geplanten Fahrten entsprechen;

(v) die Fahrgemeinschaftsfahrt wie in der Anzeige beschrieben durchzuführen (insbesondere in Bezug auf die Nutzung oder Nichtbenutzung der Autobahn) und die mit den anderen Mitgliedern vereinbarten Zeiten und Orte einzuhalten (insbesondere Treffpunkt und Ausstiegspunkt);

(vi) nicht mehr Passagiere als die in der Fahrgemeinschaftsanzeige angegebene Anzahl von Sitzplätzen mitzunehmen;

(vii) ein Fahrzeug in einwandfreiem Zustand zu verwenden, das den geltenden gesetzlichen Bestimmungen und Gepflogenheiten entspricht, insbesondere mit einem aktuellen TÜV-Zertifikat;

(viii) um BlaBlaCar oder jedem Passagier, der dies anfordert, Ihren Führerschein, Ihre Autozulassungsbescheinigung, Ihre Versicherungsbescheinigung, Ihre TÜV-Bescheinigung und alle Dokumente mitzuteilen, die Ihre Fähigkeit belegen, das Fahrzeug als Fahrer auf der Plattform zu nutzen;

(ix) im Falle einer Verzögerung oder Änderung der Zeit oder der Fahrgemeinschaftsreise Ihre Fahrgäste unverzüglich zu informieren;

(x) im Falle einer grenzüberschreitenden Fahrgemeinschaftsfahrt alle Dokumente, die Ihre Identität und Ihr Recht zum Grenzübertritt belegen, aufzubewahren und für den Passagier und jede Behörde, die dies anfordert, verfügbar zu halten;

(xi) mindestens 15 Minuten nach der vereinbarten Zeit am vereinbarten Treffpunkt auf die Passagiere zu warten;

(xii) keine Mitfahrgelegenheitsanzeige in Bezug auf ein Fahrzeug zu posten, das Ihnen nicht gehört oder für das Sie nicht berechtigt sind, es zum Zwecke der Mitfahrgelegenheit zu nutzen;

(xiii) um sicherzustellen, dass Sie von Ihren Passagieren telefonisch unter der in Ihrem Profil registrierten Nummer kontaktiert werden können;

(xiv) keinen Gewinn über die Plattform zu generieren;

(xv) keine Kontraindikation oder medizinische Unfähigkeit zum Fahren haben;

(xvi) sich während der Mitfahrgelegenheit angemessen und verantwortungsbewusst zu verhalten und den Geist der Mitfahrgelegenheit einzuhalten.

### **8.3 Verpflichtungen der Passagiere**

### **8.3.1 Für Mitfahrgelegenheiten**

Wenn Sie die Plattform als Passagier einer Mitfahrgelegenheit nutzen, verpflichten Sie sich:

(i) sich während der Fahrgemeinschaftsfahrt angemessen zu verhalten, um die Konzentration oder das Fahren des Fahrers oder die Ruhe der anderen Passagiere nicht zu beeinträchtigen;

(ii) das Fahrzeug des Fahrers und seine Sauberkeit zu respektieren;

(iii) im Falle eines Staus den Fahrer unverzüglich zu informieren;

(iv) am Treffpunkt mindestens 15 Minuten über die vereinbarte Zeit hinaus auf den Fahrer zu warten;

(v) um BlaBlaCar oder jedem Fahrer, der darum bittet, Ihren Personalausweis oder ein Dokument zum Nachweis Ihrer Identität mitzuteilen;

(vi) während einer Fahrgemeinschaftsfahrt keine Gegenstände, Güter, Substanzen oder Tiere mitzuführen, die das Fahren und die Konzentration des Fahrers behindern könnten oder deren Art, Besitz oder Beförderung den geltenden gesetzlichen Bestimmungen widerspricht;

(vii) im Falle einer grenzüberschreitenden Mitfahrgelegenheit alle Dokumente, die Ihre Identität und Ihr Recht zum Grenzübertritt belegen, aufzubewahren und für den Fahrer und jede Behörde, die dies anfordert, verfügbar zu halten;

(viii) um sicherzustellen, dass Sie von Ihrem Fahrer telefonisch unter der in Ihrem Profil registrierten Nummer kontaktiert werden können, auch am Treffpunkt;

(ix) dem Fahrer den vereinbarten Kostenbeitrag zu zahlen.

Für den Fall, dass Sie eine Buchung für einen oder mehrere Sitzplätze im Namen von Dritten vorgenommen haben, garantieren Sie in Übereinstimmung mit den Bestimmungen des obigen Artikels 4.2.3 die Einhaltung der Bestimmungen dieses Artikels und allgemein dieser Dritten durch diesen Dritten AGB. BlaBlaCar behält sich das Recht vor, Ihr Konto zu sperren, Ihren Zugang zu den Diensten einzuschränken oder diese AGB zu kündigen gemäß Abschnitt 9 dieser AGB, im Falle eines Verstoßes durch Dritte, in deren Namen Sie einen Sitzplatz gemäß diesen AGB, gebucht haben.

### **8.3.2 Für Busfahrt**

Im Zusammenhang mit der Busfahrt verpflichtet sich der Fahrgast, die Verkaufsbedingungen des jeweiligen Busunternehmens einzuhalten.

**8.4. Meldung unangemessener oder illegaler Inhalte  (Hinweis- und Aktionsmechanismus)**

Sie können verdächtige, unangemessene oder illegale Mitgliederinhalte oder Nachrichten, wie [hier](https://support.blablacar.com/s/article/Illegale-Inhalte-melden-1729197120592?language=de) beschrieben melden.

Sobald BlaBlaCar gemäß diesem Abschnitt oder von den zuständigen Behörden ordnungsgemäß benachrichtigt wurde, wird es jegliche gesetzeswidrigen Mitgliederinhalte unverzüglich entfernen wenn:

* die Mitgliederinhalte offensichtlich rechtswidrig sind oder gegen geltende Vorschriften verstoßen; oder 
* BlaBlaCar der Ansicht ist, dass ein solcher Inhalt gegen diese AGB verstößt.

In solchen Fällen behält sich BlaBlaCar das Recht vor, den ordnungsgemäß gemeldeten Mitgliederinhalt in hinreichend ausführlicher und deutlicher Form zu entfernen, bzw. das gemeldete Konto umgehend zu sperren.

Das betreffende Mitglied kann die obigen Entscheidungen von BlaBlaCar wie im untenstehenden Abschnitt 15.1 beschrieben anfechten.

**9\. Einschränkungen in Bezug auf die Nutzung der Plattform, Sperrung von Konten, Zugangsbeschränkung und Kündigung**
----------------------------------------------------------------------------------------------------------------------

Sie können Ihr Vertragsverhältnis mit BlaBlaCar jederzeit kostenlos und ohne Angabe von Gründen kündigen. Gehen Sie dazu einfach auf die Registerkarte „Mein Konto schließen“ auf Ihrer Profilseite.

Im Falle (i) eines Verstoßes Ihrerseits gegen diese AGB, einschließlich, aber nicht beschränkt auf Ihre Pflichten als Mitglied gemäß Artikel 6 und 8 oben, (ii) Überschreiten der in Artikel 4.3.3 oben festgelegten Grenze oder (iii) Wenn BlaBlaCar ernsthaften Grund zu der Annahme hat, dass dies zum Schutz seiner Sicherheit und seiner Integrität, der der Mitglieder oder Dritter oder zum Zwecke von Ermittlungen oder der Betrugsprävention erforderlich ist, behält sich BlaBlaCar das Recht vor:

(i) die Sie mit BlaBlaCar bindenden AGB sofort und fristlos zu kündigen; und/oder

(ii) das Posten jeglicher Mitgliederinhalte durch Sie auf der Plattform zu verhindern oder solche Inhalte zu entfernen; und/oder

(iii) Ihren Zugang und Ihre Nutzung der Plattform einschränken; und/oder

(iv) Ihr Konto vorübergehend oder dauerhaft sperren.

Eine Kontosperrung kann gegebenenfalls auch bedeuten, dass Sie Ihre ausstehenden Auszahlungen nicht erhalten.

Gegebenenfalls kann Ihnen BlaBlaCar eine Verwarnung senden, in der Sie an Ihre Verpflichtung erinnert werden, geltende Gesetze oder diese AGB einzuhalten.  

Wenn dies erforderlich ist, werden Sie über die Einrichtung solcher Maßnahmen informiert, damit Sie gegenüber BlaBlaCar Ihre Verpflichtung bekräftigen können, geltende Gesetze oder diese AGB einzuhalten, Erklärungen abgeben oder seine Entscheidung anfechten können. BlaBlaCar entscheidet unter Berücksichtigung aller Umstände jedes einzelnen Falles und der Schwere des Verstoßes nach eigenem Ermessen, ob die getroffenen Maßnahmen aufgehoben werden oder nicht.

**10\. Personenbezogene Daten**
-------------------------------

Im Zusammenhang mit Ihrer Nutzung der Plattform wird BlaBlaCar einige Ihrer personenbezogenen Daten erheben, wie in der [Datenschutzrichtlinie](https://blog.blablacar.de/about-us/privacy-policy) beschrieben wird.

**11\. Geistiges Eigentum**
---------------------------

### **11.1 Inhalt veröffentlicht von BlaBlaCar**

Vorbehaltlich der von seinen Mitgliedern bereitgestellten Inhalte ist BlaBlaCar der alleinige Inhaber aller geistigen Eigentumsrechte in Bezug auf den Dienst, die Plattform, ihre Inhalte (insbesondere Texte, Bilder, Designs, Logos, Videos, Töne, Daten, Grafiken) und die Software und Datenbanken, die ihren Betrieb sicherstellen.

BlaBlaCar gewährt Ihnen ein nicht ausschließliches, persönliches und nicht übertragbares Recht zur Nutzung der Plattform und der Dienste für Ihren persönlichen und privaten Gebrauch, auf nicht kommerzieller Basis und in Übereinstimmung mit den Zwecken der Plattform und der Dienste.

Ohne die vorherige schriftliche Genehmigung von BlaBlaCar ist Ihnen jede andere Nutzung oder Verwertung der Plattform und der Dienste und ihrer Inhalte untersagt. Insbesondere ist es Ihnen untersagt:

(i) Vervielfältigung, Änderung, Anpassung, Verteilung, öffentliche Darstellung und Verbreitung der Plattform, der Dienste und der Inhalte, mit Ausnahme der ausdrücklich von BlaBlaCar genehmigten;

(ii) die Plattform oder Dienste zu dekompilieren und zurückzuentwickeln, vorbehaltlich der in den geltenden Texten festgelegten Ausnahmen;

(iii) Extrahieren oder versuchen zu extrahieren (insbesondere unter Verwendung von Data-Mining-Robotern oder anderen ähnlichen Datenerfassungstools) eines wesentlichen Teils der Daten der Plattform.

### **11.2 Von Ihnen auf der Plattform gepostete Inhalte**

Um die Bereitstellung der Dienste zu ermöglichen und in Übereinstimmung mit dem Zweck der Plattform gewähren Sie BlaBlaCar eine nicht-exklusive Lizenz zur Nutzung der von Ihnen bereitgestellten Inhalte und Daten im Rahmen Ihrer Nutzung der Dienste, wozu Buchungsanfragen, Mitfahrgelegenheiten und die darin enthaltenen Kommentare, Biographien, Fotos, Bewertungen und Antworten auf Bewertungen (im Folgenden als „ Ihre „Mitgliederinhalte“ bezeichnet) und Nachrichten gehören können. Um es BlaBlaCar zu ermöglichen, über das digitale Netzwerk und in Übereinstimmung mit jedem Kommunikationsprotokoll (insbesondere Internet und Mobilfunknetz) zu verbreiten und den Inhalt der Plattform der Öffentlichkeit zur Verfügung zu stellen, autorisieren Sie BlaBlaCar für die ganze Welt und für die gesamte Dauer Ihrer Vertragsbeziehungen mit BlaBlaCar, um Ihre Mitgliederinhalte wie folgt zu reproduzieren, darzustellen, anzupassen und zu übersetzen:

(i) Sie ermächtigen BlaBlaCar, Ihre Mitgliederinhalte ganz oder teilweise auf beliebigen bekannten oder noch unbekannten digitalen Aufzeichnungsmedien zu reproduzieren, insbesondere auf Servern, Festplatten, Speicherkarten oder anderen gleichwertigen Medien, in jedem Format und durch alle bekannten oder noch unbekannten Prozesse, soweit dies für Speicher-, Sicherungs-, Übertragungs- oder Downloadvorgänge im Zusammenhang mit dem Betrieb der Plattform und der Bereitstellung des Dienstes erforderlich ist;

(ii) Sie ermächtigen BlaBlaCar, Ihre Mitgliederinhalte anzupassen und zu übersetzen und diese Anpassungen auf allen aktuellen oder zukünftigen digitalen Medien, wie in Punkt (i) oben festgelegt, zu reproduzieren, um die Dienste bereitzustellen, insbesondere in verschiedenen Sprachen. Dieses Recht umfasst insbesondere die Möglichkeit, Änderungen an der Formatierung Ihres Mitgliedsinhalts unter Berücksichtigung Ihres Urheberpersönlichkeitsrechts vorzunehmen, um die Grafikcharta der Plattform zu respektieren und/oder ihn im Hinblick auf seine Veröffentlichung über die technisch kompatibel zu machen Plattform.

Sie tragen die Verantwortung für alle Mitgliederinhalte und Nachrichten, die Sie auf unsere Plattform hochladen und halten alle diesbezüglichen Rechte. 

**12\. Rolle von BlaBlaCar**
----------------------------

Die Plattform stellt eine Online-Netzwerkplattform dar, auf der die Mitglieder Mitfahrinserate für Mitfahrgelegenheiten zum Zweck der Mitfahrgelegenheit erstellen und einstellen können. Diese Mitfahrgelegenheitsanzeigen können insbesondere von den anderen Mitgliedern eingesehen werden, um die Bedingungen der Reise zu erfahren und gegebenenfalls direkt einen Sitzplatz in dem betreffenden Fahrzeug bei dem Mitglied zu buchen, das die Anzeige auf der Plattform veröffentlicht hat. Die Plattform ermöglicht auch die Buchung von Sitzplätzen für Busfahrten.

Indem Sie die Plattform nutzen und diese AGB akzeptieren, erkennen Sie an, dass BlaBlaCar weder Partei einer Vereinbarung zwischen Ihnen und den anderen Mitgliedern im Hinblick auf die Aufteilung der Kosten im Zusammenhang mit einer Fahrt noch einer zwischen Ihnen und einem Bus geschlossenen Vereinbarung ist Operator.

BlaBlaCar hat keine Kontrolle über das Verhalten seiner Mitglieder, der Busunternehmen und ihrer Vertreter sowie der Nutzer der Plattform. Es besitzt, verwertet, liefert oder verwaltet die Fahrzeuge, die Gegenstand der Anzeigen sind, nicht und bietet keine Fahrten auf der Plattform an.

Sie erkennen an und akzeptieren, dass BlaBlaCar die Gültigkeit, Wahrhaftigkeit oder Rechtmäßigkeit der angebotenen Anzeigen, Sitzplätze und Fahrten nicht kontrolliert. BlaBlaCar erbringt als Vermittler von Mitfahrgelegenheiten keine Beförderungsleistung und tritt nicht als Beförderer auf; Die Rolle von BlaBlaCar beschränkt sich darauf, den Zugang zur Plattform zu erleichtern.

Im Zusammenhang mit Mitfahrgelegenheiten handeln die Mitglieder (Fahrer oder Mitfahrer) unter ihrer alleinigen und vollen Verantwortung.

In seiner Eigenschaft als Vermittler kann BlaBlaCar nicht für das tatsächliche Auftreten einer Mitfahrgelegenheit oder Busfahrt haftbar gemacht werden, insbesondere aus folgenden Gründen:

(i) fehlerhafte Informationen, die der Fahrer oder der Busunternehmer in seiner Anzeige oder auf andere Weise in Bezug auf die Reise und ihre Bedingungen mitgeteilt hat;

(ii) Stornierung oder Änderung einer Reise durch ein Mitglied oder einen Busbetreiber;

(iii) das Verhalten seiner Mitglieder während, vor oder nach der Reise.

**13\. Betrieb, Verfügbarkeit und Funktionalitäten der Plattform**
------------------------------------------------------------------

BlaBlaCar bemüht sich, die Plattform nach Möglichkeit 7 Tage die Woche und 24 Stunden am Tag zugänglich zu halten. Der Zugriff auf die Plattform kann jedoch aufgrund von technischen Wartungs-, Migrations- oder Aktualisierungsvorgängen oder aufgrund von Ausfällen oder Einschränkungen im Zusammenhang mit dem Betrieb des Netzwerks ohne Vorankündigung vorübergehend ausgesetzt werden.

Darüber hinaus behält sich BlaBlaCar das Recht vor, den Zugriff auf die Plattform oder ihre Funktionen nach eigenem Ermessen ganz oder teilweise vorübergehend oder dauerhaft zu ändern oder auszusetzen.

**14\. Änderung der AGB**
-------------------------

Diese AGB und die durch Bezugnahme integrierten Dokumente drücken die gesamte Vereinbarung zwischen Ihnen und BlaBlaCar in Bezug auf Ihre Nutzung der Dienste aus. Alle anderen Dokumente und Erwähnungen auf der Plattform (FAQ usw.) dienen nur als Richtlinie.

BlaBlaCar kann diese AGB ändern, um sie an ihr technologisches oder kommerzielles Umfeld anzupassen oder um neuen Rechtsvorschriften zu entsprechen. Falls eine solche Änderung erforderlich ist, werden wir Sie mindestens 14 Tage vor Inkrafttreten der Änderungen per E-Mail informieren. Die Änderungen treten in Kraft, wenn Sie nicht innerhalb der in der Benachrichtigungs-E-Mail genannten Frist (mindestens 14 Tage) ausdrücklich und in Textform widersprechen. Über diese Frist und die Folgen Ihres Widerspruchs werden wir Sie nochmals informieren.

**15\. Anwendbares Recht und Streitigkeiten**
---------------------------------------------

Diese AGB sind in deutscher Sprache verfasst und unterliegen deutschem Recht.

**15.1. Internes Beschwerdemanagement**

Sie können unsere Entscheidungen in Bezug auf folgende Punkte anfechten:

* Mitgliederinhalte: wir haben zum Beispiel Mitgliederinhalte entfernt, die Sie bei der Nutzung der Plattform bereitstellen, deren Sichtbarkeit eingeschränkt oder Entfernung verweigert oder
* Ihr Konto: wir haben Ihren Zugang zur Plattform gesperrt

wenn wir diese Entscheidungen aufgrund der Tatsache getroffen haben, dass die Mitgliederinhalte illegale Inhalte darstellen oder nicht mit diesen AGB vereinbar sind. Das Verfahren für die Einlegung eines Rechtsbehelfs ist [hier](https://support.blablacar.com/s/article/Wie-du-gegen-die-Entfernung-von-Inhalten-oder-eine-Kontosperrung-Einspruch-erheben-kannst-1729197123024?language=de) beschrieben.

**15.2. Außergerichtliche Streitbeilegung**

Sie können Ihre Beschwerden in Bezug auf unsere Plattform oder unsere Dienste ggf. auch auf der von der Europäischen Kommission online gestellten Streitbeilegungsplattform vorbringen, die Sie [hier](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage) finden. Wir sind nicht verpflichtet und grundsätzlich nicht bereit, an einem Streitbeilegungsverfahren vor einer Verbraucherschlichtungsstelle teilzunehmen.

Auch können Sie bei einer Streitbeilegungsstelle in Ihrem Land einen Antrag stellen (eine Liste finden Sie [hier](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)).

Sie können einen Schlichtungsantrag auch bei der Schlichtungsstelle Reise & Verkehr e.V. stellen.  
Fasanenstraße 81, D-10623 Berlin, http://schlichtung-reise-und-verkehr.de.

**16\. Rechtliche Hinweise**  

-------------------------------

Die Plattform wird von Comuto SA, Gesellschaft mit beschränkter Haftung mit einem Grundkapital von 166,591.952 Euro, eingetragen im Handelsregister von Paris unter der Nummer 491.904.546 (USt.-Identnr.:FR76491904546) (innergemeinschaftliche Umsatzsteuer-Identifikationsnummer: DE 08005) veröffentlicht /45532), mit Sitz in 84, avenue de la République, 75011 Paris (Frankreich), E-Mail: [\[email protected\]](https://blog.blablacar.de/cdn-cgi/l/email-protection), vertreten durch seinen Präsidenten Nicolas Brusson.

Die Website wird auf Servern von Google Ireland Limited mit Sitz in Gordon House, Barrow Street, Dublin 4 (Irland) gehostet.

Comuto SA ist im Register der Reise- und Aufenthaltsveranstalter unter der folgenden Nummer eingetragen: IM075180037

Die finanzielle Garantie wird bereitgestellt von: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Paris – Frankreich.

Die Berufshaftpflichtversicherung ist abgeschlossen bei: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paris – Frankreich.

Comuto SA ist im Versicherungsvermittler-, Banken- und Finanzregister unter der Registernummer (Orias) 15003890 eingetragen.

Bei Fragen können Sie sich über dieses [Kontaktformular](https://support.blablacar.com/s/contactsupport?language=de) an Comuto SA wenden oder uns telefonisch unter +33 (0) 185762227 anrufen.

**17\. Digital Services Act (Gesetz über digitale Dienste)**
------------------------------------------------------------

**17.1.  Informationen zu den im Durchschnitt monatlich aktiven Nutzern des Dienstes in der Union**

Gemäß Artikel 24 Absatz 2 der Verordnung des Europäischen Parlaments und des Rates über einen Binnenmarkt für digitale Dienste und zur Änderung der Richtlinie 2000/31/EG („DSA“) müssen Anbieter von Online-Plattformen Informationen zu den im Durchschnitt monatlich aktiven Nutzern ihres Dienstes in der Union veröffentlichen, der als Durchschnitt der letzten sechs Monate errechnet wird. Zweck dieser Veröffentlichung ist es festzustellen, ob eine Online-Plattformanbieter das Kriterium für “sehr große Online-Plattformen” gemäß dem DSA erfüllt, d.h. den Schwellenwert von durchschnittlich 45 Millionen monatlich aktiven Nutzern in der Union überschreitet.

Am 31. Januar 2023 belief sich die durchschnittliche Anzahl der aktiven Nutzer von BlaBlaCar für den Zeitraum von August 2023 bis Januar 2024, bei einer Berechnung unter Berücksichtigung des Erwägungsgrundes 77 und des Artikels 3 der DSA, vor Erlassung eines spezifischen delegierten Rechtsaktes, auf etwa 4,82 Millionen in der EU.

Diese Informationen werden nur zur Einhaltung der DSA-anforderungen veröffentlicht und sollten nicht für andere Zwecke herangezogen werden. Sie werden alle sechs Monate mindestens einmal aktualisiert. Unser Ansatz für die Erstellung dieser Berechnung kann sich im Verlaufe der Zeit weiterentwickeln, oder eine Anpassung notwendig machen, beispielsweise aufgrund von Produktänderungen der neuen Technologien.

**17.2. Kontaktstelle für Behörden**

Als Mitglied der zuständigen EU-Behörden, der EU-Kommission oder des europäischen Rats für digitale Dienste können Sie sich gemäß Artikel 11 der DAS, in Angelegenheiten, die die DAS betreffen mit uns per E-Mail unter [\[email protected\]](https://blog.blablacar.de/cdn-cgi/l/email-protection) in Verbindung setzen.

Sie können uns in englischer und in französischer Sprache kontaktieren.

Beachten Sie bitte, dass diese E-Mail-Adresse nicht für die Kommunikation mit Mitgliedern verwendet wird. Bei Fragen zur Nutzung von BlaBlaCar können Sie uns als Mitglied über dieses [Kontaktformular](https://support.blablacar.com/s/contactsupport?language=de) kontaktieren.