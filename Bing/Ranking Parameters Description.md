How Bing delivers search results
================================

Last updated: March 2025

_For previous versions, please see the archives  [here](https://www.bing.com/bingpolicies/archives)._

Our Approach to Search
----------------------

We recognize that in our technology-driven world, search engines are in many ways the gateway to the internet and the primary way people find the content they are looking for amongst the trillions of ever-changing webpages available online. Search engines like Bing play a vital role in upholding the fundamental right to free and open access to information and free expression, and we strive to help users find the most useful and relevant information that they seek. At the same time, we recognize that we must often balance the right to access information against other key rights and interests, such as user privacy or safety. To help guide how we determine what pages should be provided in response to search queries, we rely on the following principles:

We provide credible and authoritative results relevant to user queries.

* We provide the highest quality, authoritative content relevant to users’ search terms.
    
* Our goal is to always provide fair, balanced, and comprehensive content. When there are multiple credible perspectives, we try to display them in informative ways. When there is no authoritative source, our goal is to avoid promoting bias or potentially misleading information.
    
* We respect user intent. When a user expresses a clear intent to access specific information, we provide relevant results even if they are less credible, while (as described in more detail below) working to ensure that users are not misled by such search results.
    

We promote free and open access to information within the bounds of the law and with respect for local law and other fundamental rights, such as privacy and public safety.

* We provide open access to as much of the web as possible, but in limited cases we may undertake certain interventions (such as removal of a website or downranking) such as where the content violates local law, or Microsoft’s policies or core values.
    
* When limiting access to content, we strive to ensure our actions are narrowly tailored, so we don't unduly restrict important interests such as freedom of expression, open access to information, and media pluralism, and we provide transparency regarding our actions.
    

We take steps to protect users from harmful and unexpected offensive content.

* We recognize that there are many reasons why someone might want to research or review harmful or controversial content, but also recognize the importance of ensuring users aren't inadvertently misled by such content.
    
* For certain types of content where we identified search results may include harmful or misleading information, we might provide supplemental information, such as warnings and public service announcements, to inform users about potential risks. We give users control over the type of content they encounter in Bing through features such as SafeSearch and Family Safety.
    
* Absent a clear intent to access specific content we assume an intent to find high authority results.
    

We're transparent about our principles and practices, as well as our decisions and actions.

* We provide users with information about our principles regarding ranking and relevance, and our moderation policies.
    
* When we limit access to content, where relevant we provide notice to users that content was removed.
    
* We publish regular transparency reports providing information about the complaints we receive and the actions taken.
    

**Tip:** If information has already been removed from the website but is still showing up in Bing search results, you can use the Content Removal Tool to submit a page removal or outdated cache removal request. To learn more about the Content Removal Tool, [go to Bing Webmaster Help & How-To](https://go.microsoft.com/fwlink?LinkId=282036).

The Basics of Search
--------------------

Returning search results is no easy feat, and involves complex, near-instantaneous algorithmic calculations. A search for something as simple as “cat” could yield over 55 million results. So how do we choose which results to show first? We start by crawling the web and building an index of available web pages. Then we use algorithms to rank and optimize that content so we can give users the best, highest quality search results available.

More details on each of these steps are below.

### 

How Bing Crawls the Web

The first step in building search is figuring out which pages exist on the internet in order to index them - commonly called crawling. At Bing, we affectionately call our crawler “Bingbot.” Crawling is how Bingbot discovers new and updated pages and content to add to our search index. Bingbot uses an algorithm to decide what to crawl and how often, working to minimize its impact on websites. We're all about efficiency, even as we crawl billions of URLs every day. We prioritize relevant known pages that aren’t indexed yet and ones we think have been updated to help deliver the freshest results.

### 

How Bing Builds an Index

As Bingbot crawls the web, it sends information to Bing about what it finds. These pages are then added to the Bing index and algorithms are used to analyze the pages so we can effectively include them in search results, including determining which sites, news articles, images, or videos will be included in the index and available when users search for specific keywords. Learn more about how Bing finds and indexes pages [here](https://www.bing.com/webmaster/help/webmaster-guidelines-30fba23a).

### 

How Bing Ranks Search Results

Complex algorithms generate Microsoft Bing search results using the user’s search query matching it with third-party webpages in our index. Bing designs and continually improves its algorithms to provide the most comprehensive, relevant, and valuable collection of search results available.

Given the scale of the internet and the complexity of operating a search engine, Bing relies on machine learning to ensure users see the best results for their query out of the trillions of pages of content on the web. Machine learning is the science of getting computers to act without being explicitly programmed by identifying patterns in data and generalizing based on a small set of examples. For web ranking, this means building a model that will look at some ideal results to learn which features are most predictive of their usefulness, relying on automated signals like user interactions with the Bing website, and training data labeled by human judges and/or via AI systems with human oversight. Bing guides its judges to make sure their evaluations align with Bing’s ranking goals and engages in ongoing monitoring and oversight of its ranking systems and adjusts its practices as needed to make sure search results remain aligned with Bing’s principles.

**Main parameters of ranking**

Below is a high-level overview of the main parameters Bing uses to rank pages in search results. These parameters are listed in general order of importance. Bing’s complex systems use these criteria to deliver search results. The relative importance of each of the parameters below may vary from search to search and evolve over time.

* **Relevance:** Relevance refers to how closely the content on the landing page matches a user’s intent behind a search query. This includes matching terms directly on the page and phrases used in links referring to the page. Bing also considers semantic equivalents, including synonyms or abbreviations, which may not be exact matches of the query terms but are understood to have the same meaning. Many queries may have more than one possible intent. Bing tries to provide a comprehensive set of results that reflect all possible intents, but Bing presumes the user seeks high-quality, authoritative content unless the user clearly indicates an intent to research low-quality content.
    
* **Quality and Credibility:** Determining the quality and credibility (QC) of a website includes evaluating the clarity of purpose of the site, its usability, and presentation. QC also consists of an evaluation of the page’s “authority”, which includes factors such as:
    
    * Reputation: What types of other websites link to the site? A well-known news site is considered to have a higher reputation than a brand-new blog.
        
    * Level of discourse: Is the purpose of the content solely to cause harm to individuals or groups of people? For example, a site that promotes violence or resorts to name-calling or bullying will be considered to have a low level of discourse, and therefore lower authority, than a balanced news article.
        
    * Level of distortion: How well does the site differentiate fact from opinion? A site that is clearly labeled as satire or parody will have more authority than one that tries to obscure its intent.
        
    * Origination and transparency of the ownership: Is the site reporting first-hand information, or does it summarize or republish content from others? If the site doesn’t publish original content, do they attribute the source? A first-hand account published on a personal blog could have more authority than unsourced content.
        
* **User engagement:** Bing also considers how users interact with search results. To determine user engagement, Bing asks questions like: Did users click through to search results for a given query, and if so, which results? Did users spend time on these search results they clicked through or quickly return to Bing? Did the user adjust or reformulate their query? For webmasters, the Bing Webmaster Dashboard provides insights into how users interact with their webpages.
    
* **Freshness:** Generally, Bing prefers fresh content. A page that consistently provides up-to-date information is considered fresh. In many cases, content produced today will still be relevant years from now. In some cases, however, content produced today will go out of date quickly.
    
* **Location and Language:** In ranking results, Bing considers the user’s location (country and city), where the page is hosted, the language of the page, and the location of other visitors to the page.
    
* **Page load time:** Slow page load times can lead a visitor to leave a website, potentially before the content has even loaded, to seek information elsewhere. In most cases, Bing will consider this a poor user experience and a less helpful search result.
    

**Use of personal data**

Bing uses information specific to an individual user to improve relevance and performance. This information includes search history, location, language, device characteristics such as operating system or browser, and whether a user is on a mobile device, tablet, or desktop. Users can update their location, language, and other settings via [Bing Settings](https://www.bing.com/account), and delete search history on the [Privacy Dashboard](https://account.microsoft.com/account/privacy).

**Paid content and Microsoft-promoted content**

Websites might choose to advertise on Bing through [Microsoft Advertising](https://ads.microsoft.com/); paid advertisements that appear in search results are labeled as such.  Bing does not allow websites to improve their organic ranking positions (the “blue links”) through payment. [Learn more](https://help.ads.microsoft.com/#apex/3/en/50825/0) about how paid search [ads are ranked](https://help.ads.microsoft.com/#apex/3/en/50825/0) within advertising search results.

Bing doesn't prioritize Microsoft products or services over third-party products and services in algorithmic search results. Outside of organic search results, Bing will sometimes provide a standalone answer box or banner promoting a Microsoft product or service when relevant to the search term provided.

**Nature of businesses** 

The characteristics of the goods or services offered on a site don't affect how Bing ranks results, except in cases where the content is potentially offensive or harmful to a user. For example, pages offering adult content are not available when users set [SafeSearch](https://help.ads.microsoft.com/apex/index/18/de-US/10003) to “Strict” mode. As another example, pages promoting potentially harmful content, such as where a site promotes methods of suicide or purports to sell opioids or other potentially harmful drugs, would be considered low authority content.

Enhanced Search Experiences
---------------------------

In addition to core algorithmic search functions, Bing provides users with additional features to help provide additional context and information and enhance the search experience. For example, if users want to know which team won last night's game, when users search on Bing for a team name, they're likely to see the final score and a recap at the top of the page, a snapshot with more info about the team, news, videos, and related searches along with the most relevant search results. To help users find what they are looking for more easily, we also let users narrow their search results with categories like images, videos, news, and shopping. These features also rely on the ranking principles and main parameters described above, but in some cases take additional considerations into account to provide users with the highest quality, most relevant results available. In some cases, Bing might determine that a particular search query isn't well suited for these enhanced search features, but will always make sure users can find relevant information in the blue links. More details on these features are below. 

### 

Answers

“Answers” are how Bing describes the enhanced search results provided often at the top or side of search results that provide richer content in response to a user’s query, often with a direct answer to the question asked, or with additional related resources.  For example, if a user types “How tall is the Eiffel Tower?” Bing will respond with the answer of “330 m” to enable users to quickly find the information they need, along with a sidebar pane with more information about the Eiffel Tower, including interesting facts about when it was built, where it's located, and other details. For 'these queries, Bing looks at search results across the web, returns a summarized answer, and links to its sources.  Some of these answers might be powered by generative AI to provide an even richer experience, linking to key sources.

 If the query is related to a business, Bing may return relevant information about the business, such as store hours and location. Business owners can claim and verify existing listings on Bing using [Bing Places for Business](https://www.bingplaces.com/) (available in limited markets) to create, edit or update their listing information. In some cases, Bing might partner with third-party content providers, such as local restaurant review sites, to further enhance the user experience

### 

Image and Video Experiences

Bing’s image and video experiences provide the user with image and video results relevant to their search queries, which can appear in the image and video verticals, as well as on the main search results page where image or video content is relevant to the user’s query. Ranking within the image and video experiences generally relies on the same parameters as the main web search results page, i.e., relevance, quality, freshness, authority, and popularity. When a user first lands on the image or video experience homepages, prior to entering a query, the homepage will show recommended content based on prior interactions with the site, like the user’s search history, engagement with image results, saved images, and, where a user allows it, Edge browsing history.  Users can control this experience by deleting data used to influence personalization from their Privacy Dashboard, leveraging [the consent for Microsoft Edge browsing activity for personalized advertising and experiences](https://support.microsoft.com/en-us/microsoft-edge/microsoft-edge-browsing-activity-for-personalized-advertising-and-experiences-37aa831e-6372-238e-f33f-7cd3f0e53679) or, if they are not logged in, starting a new session. In some markets, Bing also offers a feature called Bing Saves that allows authenticated users to maintain collections of relevant image links. User activity in Bing Saves is governed by the [Microsoft Services Agreement](https://www.microsoft.com/servicesagreement) and Code of Conduct. 

### 

Maps

In the Bing Maps experience, or on the main search results page where the query indicates a location intent, users can type queries into the search bar, and the map will display a list of locations in that vicinity responsive to that search or pinpoint a particular address. Bing Maps relies on licensed and publicly available location data and uses the same ranking parameters as outlined above. Businesses can improve the accuracy of Bing Maps results by registering with [Bing Places](https://www.bingplaces.com/) and providing up to date information for Bing users on information relevant to that business, such as location, contact information, and hours.

### 

News

Bing has a dedicated news [home page](https://www.bing.com/news) called Bing News, which curates news content for Bing users.  Where relevant, Bing might also display a news-focused answer box on the main search results page pulling from the Bing News content.

While Bing News relies on the same main parameters for ranking results as Bing, content that appears in Bing news search results must meet specific Bing News Publisher Guidelines, described in detail in the Bing News [Publication Hub](https://www.bing.com/webmasters/help/pubhub-publisher-guidelines-32ce5239). Websites interested in being included in the Bing News index might apply through PubHub. When the same article is available both on a third-party website and on MSN.com or Microsoft Start (as Microsoft licensed content), Bing News search results might display the Microsoft Start version of that content. This enables a more consistent, higher-quality user experience. Licensed Microsoft Start content appearing in Bing is marked by the label “powered by Microsoft Start.”

### 

Shopping

Bing Shopping is a search feature to help users discover products from multiple sellers on a single view by returning relevant shopping results from the web and from advertisers. In some cases, the shopping experience might use generative AI tools to provide better results for users. In some instances, Microsoft might receive compensation for users who click on algorithmically generated results, but such compensation has no effect on the ranking or relevance of algorithmically generated results shown to the user.

Bing Shopping also might provide coupons relevant to the user’s query, or where a user allows, based on prior engagement with Shopping experiences.  In some instances, Microsoft might receive revenue for the use of the coupons and purchases through Bing Shopping, although whether a revenue share payment will be obtained is not factored into Bing’s ranking of coupons shown to users.

Bing Shopping also offers generative AI features such as Buying Guides and Expert Review Summary / User Review Summary to further help users to efficiently make shopping decisions. These features are designed, built and tested in accordance of Microsoft Responsible AI standard. Since LLMs can occasionally make mistakes, appropriate information is provided to the users to ensure users are aware of the limitations. 

Users who allow Microsoft to collect and use personal data to personalize and advertise might see results in the Shopping vertical tailored to their interests; users can visit the [Microsoft Privacy Dashboard](https://account.microsoft.com/account/privacy?refd=privacy.microsoft.com&destrt=privacy-dashboard) or the Edge shopping toggle to opt-in or out of personalized ads and control the data that influences personalization.

### 

Safe Search

Safety is always the top priority at Bing. We have a number of ways to help keep users safe, including always using a secure https web connection, notifying users if we think a site contains malware, letting users control what they see with SafeSearch, and by integrating with Windows Family Safety to help caregivers protect their children. Our SafeSearch feature helps keep adult content out of search results. Visit the [SafeSearch setting](https://www.bing.com/account/general) to make changes to this experience.

### 

Suggestions

Autosuggest and related suggestions (“suggestions”) are query suggestions provided by Bing to help users use search and chat more conveniently.  Autosuggest is a feature that helps users complete searches faster by predicting the search queries as users start to type in the query box.  Related suggestions is a feature that shows topics that the users might be interested in next to search results (e.g., “People might also ask”).  Bing similarly provides suggestions for conversation topics in its chat experiences.

Suggestions are generated algorithmically and based on the popularity of related searches on Bing, along with other relevance signals such as search history, trends, location, and language. Bing’s suggestions also use natural language generation technology trained on query sets to help predict a user’s intended query.

Generative AI Features
----------------------

Bing includes certain generative AI features that run on a variety of advanced technologies from Microsoft and OpenAI, including GPT, a cutting-edge large language model (LLM), and DALL-E, a deep learning model to generate digital images from natural language descriptions, both from OpenAI. We worked with both models for months prior to public release to develop a customized set of capabilities and techniques to join this cutting-edge AI technology and web search in the new Bing. For example, Bing sometimes shows LLM-generated summarized results on top of the Bing main search results page in response to user queries, making the search experience even easier and faster. Another example: in Bing Shopping, we offer AI-generated Buying Guides and Expert Review Summaries / User Review Summaries to help users to efficiently make shopping decisions.  

At Microsoft, we take our commitment to responsible AI seriously. Bing's generative AI experiences has been developed in line with [Microsoft’s AI Principles](https://www.microsoft.com/ai/our-approach), [Microsoft’s Responsible AI Standard](https://blogs.microsoft.com/wp-content/uploads/prod/sites/5/2022/06/Microsoft-Responsible-AI-Standard-v2-General-Requirements-3.pdf), and in partnership with responsible AI experts across the company, including Microsoft’s Office of Responsible AI, our engineering teams, Microsoft Research, and Aether. You can learn more about responsible AI at Microsoft [here](https://www.microsoft.com/ai/responsible-ai).   

### Identifying, measuring, and mitigating Risks  

Like other transformational technologies, harnessing the benefits of AI is not risk-free, and a core part of [Microsoft’s Responsible AI program](https://www.microsoft.com/ai/responsible-ai) is designed to identify potential risks, measure their propensity to occur, and build mitigations to address them. Guided by our AI Principles and our Responsible AI Standard, we sought to identify, measure, and mitigate potential risks while securing the transformative and beneficial uses that the new experience provides. In the sections below we describe our iterative approach to identify, measure, and mitigate potential risks.     

**Identify**  

At the model level, our work began with exploratory analyses of GPT-4 in the late summer of 2022. This included conducting extensive red team testing in collaboration with OpenAI. This testing was designed to assess how the latest technology would work without any additional safeguards applied to it. Our specific intention at this time was to produce harmful responses, surface potential avenues for misuse, and identify capabilities and limitations. Our combined learnings across OpenAI and Microsoft contributed to advances in model development and, for us at Microsoft, informed our understanding of risks and contributed to early mitigation strategies for generative AI features in Bing. 

In addition to model-level red team testing, a multidisciplinary team of experts conducted numerous rounds of application-level red team testing on the previous Copilot in Bing AI experiences before making them publicly available in our limited release preview. This process helped us better understand how the system could be exploited by adversarial actors and improve our mitigations. Non-adversarial stress-testers also extensively evaluated new Bing features for shortcomings and vulnerabilities. Post-release, the new AI experiences in Bing are integrated into the Bing engineering organization’s existing production measurement and testing infrastructure. For example, red team testers from different regions and backgrounds continuously and systematically attempt to compromise the system, and their findings are used to expand the datasets that Bing uses for improving the system.  

**Measure** 

Red team testing and stress-testing can surface instances of specific risks, but in production users will have various queries in Bing with different levels of search intent. To better understand and address the potential for risks in Bing’s generative AI experiences, we developed metrics to measure potential risk of harmful content being shown to users. The metrics are used for new feature evaluation as a part of Responsible AI reviews as well as ongoing monitoring for features post launch. We also enabled measurement at scale through partially automated measurement pipelines. Each time the product changes, existing mitigations are updated, or new mitigations are proposed, we update our measurement pipelines to assess both product performance and the responsible AI metrics.   

Our measurement pipelines enable us to rapidly perform measurement for potential risks at scale. As we identify new issues through the preview period and ongoing red team testing, we continue to expand the measurement sets to assess additional risks.  

**Mitigate**   

As we identified potential risks and misuse through processes like red team testing and stress-testing and measured them with the innovative approaches described above, we developed additional mitigations to those used for traditional search. Below, we describe some of those mitigations. We will continue monitoring the generative AI experiences in Bing to improve product performance and mitigations.   

* **Phased release, continual evaluation.** We are committed to learning and improving our responsible AI approach continuously as our technologies and user behavior evolve. Our incremental release strategy has been a core part of how we move our technology safely from the labs into the world, and we’re committed to a deliberate, thoughtful process to secure the benefits of generative AI features in Bing. We are regularly making changes to generative AI features in Bing to improve product performance, improve existing mitigations, and implement new mitigations in response to our learnings during the preview period.
    
* **Grounding in search results.** Responses in Bing’s generative search that are based on search results include references to the source websites for users to verify the response and learn more. Users are also provided with explicit notice that that they are interacting with an AI system and advised to check the web result source materials to help them use their best judgment.
    
* **AI-based classifiers and metaprompting to mitigate potential risks or misuse.** The use of LLMs may produce problematic content that could lead to risks or misuse. Examples could include output related to self-harm, violence, graphic content, intellectual property, inaccurate information, hateful speech, or text that could relate to illegal activities. Classifiers and metaprompting are two examples of mitigations that have been implemented in Copilot in Bing to help reduce the risk of these types of content. Classifiers classify text to flag different types of potentially harmful content in search queries, chat prompts, or generated responses. Bing uses AI-based classifiers and content filters, which apply to all search results and relevant features; we designed additional prompt classifiers and content filters specifically to address possible risks raised by the generative AI features. Flags lead to potential mitigations, such as not returning generated content to the user or redirecting the user to traditional search. Metaprompting involves giving instructions to the model to guide its behavior, including so that the system behaves in accordance with Microsoft's AI Principles and user expectations.
    
* **AI disclosure.** Bing’s generative AI features provide AI disclosure where users are notified that they are interacting with an AI system as well as opportunities to learn more about the features. Empowering users with this knowledge can help them avoid over-relying on AI and learn about the system’s strengths and limitations.
    
* **Operations and rapid response.** We also use Bing’s ongoing monitoring and operational processes to address safety issues observed.
    
* **Feedback, monitoring, and oversight.** The generative AI  experiences in Bing build on existing tooling that allows users to submit feedback and report concerns, which are reviewed by Microsoft’s operations teams. Users may use the [Report a Concern page](https://www.microsoft.com/concern/bing) to report potentially harmful or misleading AI-generated content.
    

Our approach to identifying, measuring, and mitigating risks will continue to evolve as we learn more, and we are continuously making improvements based on feedback gathered.

Improving Search Results
------------------------

### 

Research and Testing

Before users ever see an improvement or new feature at Bing, we’ve been hard at work behind the scenes. Our researchers often recruit users to try out new features and give us their opinions. Once they’re satisfied, we release the feature to a small set of real users to see how they interact with it. When it gets the green light, we release it to everyone. But it doesn’t end there. After every feature is released, we regularly monitor usage and feedback to evaluate what’s working and how we can improve.

### 

Feedback

Your feedback helps make Bing better. At the bottom of every Bing page, you’ll see a Feedback link. We don’t require your name or other identifying information when you submit feedback.  This information is shared with teams across Bing to improve your experience and search results. We try to read every piece of feedback you submit. Plus, we can’t help but notice what users like and don't like about Bing as you use the site, such as which links you click on and which searches you have to revise to find what you are looking for, and we use that info when we are working on new or existing features. 

Controls
--------

### 

Bing Settings

For many common search scenarios, such as looking for local businesses like restaurants, shops, or medical care, in order to provide useful search results, it is essential that Bing understands the user’s language, location, and similar preferences.  At the same time, Bing wants to make sure the user stays in control of their experience.  Users can see what information Bing is using to provide relevant search results and make changes at any time in [Bing Settings](https://www.bing.com/account).

For location, Bing will attempt to identify the area in which you are located using signals like your IP address but you also can update your preferred location at any time [here](https://www.bing.com/account). In some cases, like Maps, it might be important for Bing to understand more specifically where the user is located in order to provide location-dependent services like directions.  Users can also control what specific location information is provided to Microsoft through in-product controls and their device’s operating system.  

If you allow it, we use your search history to help improve results and provide more relevant search results. To control this information, see your Bing search history. ([https://www.bing.com/profile/history)](https://www.bing.com/profile/history) 

Users also can access and modify other relevant controls in the Bing Settings experience, such as SafeSearch settings, voice search controls, search suggestions, and personalization options. 

### 

Microsoft Account

Users logged in to their Microsoft Account also have more robust controls over their location history, search history, and other data collected by Microsoft, in the [Microsoft Privacy Dashboard](https://account.microsoft.com/account/privacy?refd=privacy.microsoft.com&destrt=privacy-dashboard). 

Signing in with your Microsoft account is an easy way to [manage your data](https://account.microsoft.com/account/privacy?destrt=privacy-dashboard) and adjust controls, including targeted advertising.

To learn more about how Microsoft uses personal data, see Microsoft’s [privacy policy](https://privacy.microsoft.com/privacystatement).

Our Search Integrity
--------------------

Microsoft respects freedom of expression and the right to access information.  At the same time, in accordance with Microsoft policies and principles around Responsible AI, privacy, digital safety, information integrity, and other critical issues, Bing has developed a safety system including content filtering, operational monitoring, and abuse detection to provide a safe search experience for our users.

If Microsoft receives requests to remove content from individuals, businesses, and governments, in limited cases, where quality, safety, user demand, relevant laws, and/or public policy concerns exist, Bing might remove results, inform users of certain risks, or provide users with options for revising their content. Bing limits removal of search results to a narrow set of circumstances and conditions to avoid restricting Bing users’ access to relevant information.

Below we describe the circumstances where Bing might take action on content. When search results are removed, Bing endeavors to be transparent about removal. This includes providing users with notice of removal of search results at the bottom of the page. In addition, we publish information about search results removed by Bing as part of the [Microsoft Reports Hub](https://www.microsoft.com/corporate-responsibility/reports-hub), including [Copyright Content Removal Requests Report](https://www.microsoft.com/corporate-responsibility/copyright-removal-requests-report), [Right to be Forgotten Content Removal Requests Report](https://www.microsoft.com/corporate-responsibility/right-to-be-forgotten-removal-requests-report), and [Government Requests for Content Removal Report](https://www.microsoft.com/corporate-responsibility/government-content-removal-requests-report).

When a potential content or conduct violation on Bing is reported, a specially trained human reviewer might look at the content and conduct. 

### 

Content Moderation Based on Legal Demands

Certain countries have laws or regulations that apply to search service providers and require search engines to remove links to certain indexed pages from search results. Some of these laws allow specific individuals or entities to demand removal of results (such as for copyright infringement, libel, defamation, personally identifiable information, hate speech, or other personal rights), while others are administered and enforced by local governments.

When Microsoft receives a request or demand, Bing aims to balance its support for freedom of expression and for free access to relevant content with compliance with local laws. We review and assess the request or demand, including the reason and basis for the request or demand, the authority or rights of the requesting party, our applicable policies and our commitments to our users with regard to freedom of expression, human rights, and freedom of information and determine whether and to what extent we should remove access to the content. Examples of legal request areas with global approaches are below.

**Child sexual exploitation and assault imagery and related materials**

The production and distribution of, and access to, child sexual abuse materials is universally condemned and illegal in most jurisdictions. It is also a violation of Bing and Microsoft policies. Bing works with third party technology and industry groups, law enforcement, and governmental and non-governmental organizations to help stop the spread of this horrific content online. One way we do this is by removing pages that have been reviewed by credible agencies (or identified via [Microsoft PhotoDNA](https://go.microsoft.com/fwlink?LinkId=626929) content detection tools) and found to contain or relate to the sexual exploitation or abuse of children.

In particular, we remove pages from our index that have been identified by the [Internet Watch Foundation](https://go.microsoft.com/fwlink?LinkId=282332) (UK), [NCMEC](https://www.missingkids.org/) (US), and [FSM](https://go.microsoft.com/fwlink?LinkId=282336) (Germany) as, in their good faith judgment, hosting or providers of access to child sexual abuse material. Removing these links from displayed search results doesn’t block the materials from being accessed on the web or discovered through means other than Bing, but it does reduce the availability of these pages for those who would seek it out or profit from it.

Bing also uses PhotoDNA hash-matching technology to scan images uploaded by users in the visual search feature for potential exploitations and abuse imagery. User activity in Bing is governed by the Microsoft Services Agreement. More information about Microsoft and Bing’s use of PhotoDNA and content moderation practices are available at [Digital Safety Moderation and Enforcement](https://www.microsoft.com/DigitalSafety/moderation-and-enforcement). 

**Copyright infringement**

Bing encourages respect for intellectual property rights, including copyrights, while also recognizing the rights of users to engage in uses that may be permissible under applicable copyright laws. Bing may remove from its search results links to webpages containing material infringing the rights of the owner of copyrighted content, provided we receive a legally sufficient notice of copyright infringement from the copyright owner or its authorized agent.

If you are a rights holder and have an intellectual property concern about a website’s content that is linked to by Bing, or a Bing ad, please see our [Report Infringement page](https://go.microsoft.com/fwlink?LinkId=626930).

### Content Moderation Based on Quality, Safety and User Demand 

In certain circumstances relating to quality, safety, and user value, Bing might decide to remove certain results, or we might warn or educate users or provide options for tailoring results.

**Spam**

Certain pages captured in the Bing index might turn out to be pages of little or no value to users and/or can have characteristics that artificially manipulate the way search and advertising systems work in order to distort their relevance relative to pages that offer more relevant information. Some of these pages include only advertisements and/or links to other websites that contain mostly ads, and no, or only superficial, content relevant to the subject of the search. To improve the search experience for users and deliver more relevant content, Bing might remove such search results, or adjust Bing algorithms to prioritize more useful and relevant pages in search results.

**Sensitive personal information, including nonconsensual distribution of intimate images**

From time to time, webpages that are publicly available might intentionally or inadvertently contain sensitive personal information posted without the consent of the individual identified or in circumstances that create security or privacy risks. Examples include inadvertent posting of private records, private phone numbers, identification numbers and the like, or intentionally and maliciously posting email passwords, login credentials, credit card numbers, or other data intended to be used for fraud or hacking. Upon verification, Bing will remove such search results.

Another example is when someone shares adult or sexually explicit images of another person online – whether real or a “deepfake” image that is created using AI or other photo editing tools - without that person’s consent.  This is commonly referred to as nonconsensual intimate imagery, or "revenge porn". Microsoft considers this to be a gross invasion of personal privacy. To help victims get back control of their images and their privacy, Bing might remove links to such photos and videos from search results. To report unauthorized online photos and videos, including deepfakes, victims should complete a form on our [reporting web page](https://go.microsoft.com/fwlink?LinkId=626966).

Some webpages engage in deceptive and exploitative removal practices. Examples of this activity include requests or demands for payment for individuals to get their private information removed or a lack of any removal process for sensitive personal information on the site. Upon a report submitted and reviewed, Bing may remove such search results from the index. To report sites that engage in these practices, please navigate to our [reporting web page](https://go.microsoft.com/fwlink?LinkId=626966). 

As noted in the reporting form, it’s important to remember that information will remain available on the original website even if Bing has removed the link from search results. The website owner is in the best position to address privacy concerns about the information it publishes. Victims will need to contact owners of the website where the content is hosted to remove content from the web.

**Adult content**

Bing offers a SafeSearch feature, which allows most users to set the type of filtering for adult content that they prefer to apply to their search results. Bing endeavors to avoid delivering content that can be offensive or harmful when it wasn’t requested and provides SafeSearch settings to allow users to customize their search experience. By default, in most countries or regions, SafeSearch is set to **Moderate**, which restricts visually explicit search results but doesn't restrict explicit text.  Users can choose to change their SafeSearch settings at any time.

Different countries or regions may have different local customs, religious or cultural norms, or local laws regarding the display of adult content (or search results accessing adult content). This might affect default SafeSearch settings for Bing in some countries.

**Illegal pharmaceuticals**

In furtherance of Bing’s commitment to provide users with trustworthy search results, Bing has taken steps to minimize the likelihood that users are harmed through illegal pharmaceutical sales online. In the US, Bing blocks online pharmacies listed on the FDA's [Internet Pharmacy Warning Letters site](https://www.fda.gov/drugs/drug-supply-chain-integrity/internet-pharmacy-warning-letters). The FDA, as the federal regulatory body for the pharmaceutical industry in the United States, maintains this public list of online pharmacies engaged in illegal and unsafe practices. The FDA has more information at [BeSafeRx: Know Your Online Pharmacy](https://www.fda.gov/drugs/quick-tips-buying-medicines-over-internet/besaferx-your-source-online-pharmacy-information).

**Public service announcements and warnings**

For some topics, it can be difficult to find quality information. When we identify these gaps, we sometimes insert helpful content so users can explore a topic faster. These can take many different forms. You might see a ‘public service’ answer, an answer that explains breaking news, or a warning on a particular site known to contain potentially harmful information. To ensure we are getting the details right, we always look for authoritative sources for curated and original content. If we are using a third-party source, we’ll always show their name or website so you’ll know where the data is coming from.

For example, Bing provides links to suicide prevention resources when a user’s query expresses a possible suicide intent. Microsoft Bing also provides warning notices on certain URLs appearing in search results where it has reliable information that the link contains possibly harmful content. Such notices appear on links to sites Microsoft Bing has determined to contain harmful malware that could damage a user’s computer. Microsoft Bing also provides a warning to users in the United States on any link identified by the National Association of Boards of Pharmacy® (NABP®) as a “[Not Recommended Site](https://www.safe.pharmacy/not-recommended-sites/).” 

### Additional Ranking Interventions 

In some cases, Bing might discover that certain search topics are returning results not in line with Bing’s principles, such as where bad actors are intentionally manipulating search results to surface low authority content, exploiting a [data void](https://datasociety.net/library/data-voids/), or otherwise violating Bing’s Webmaster Guidelines in an effort to improperly manipulate search results. Prohibited practices are described further at [Webmaster Guidelines - Bing Webmaster Tools](https://www.bing.com/webmasters/help/webmasters-guidelines-30fba23a). When this occurs, Bing might apply additional algorithmic interventions to prevent users from being unexpectedly exposed to harmful or misleading content that they did not evince an intent to search for. Bing prioritizes interventions on known misinformation trends, topics where there are elevated risks of active manipulations, and areas where there are risks of harm to our users.  

While we do not remove such content entirely from our index, we apply algorithmic interventions so that users without a clear intent to find such information are protected from accidentally being presented with harmful or misleading information. Bing actively monitors manipulation trends in identified high risk areas and deploys mitigation methods as needed to ensure users are provided with high quality, high authority search results_._ 

### Safety in Enhanced Search Features 

​​​​​​​**Providing helpful search suggestions**

Bing takes steps to ensure that users are not inadvertently led to potentially harmful, offensive, or misleading content via search suggestions.  We set guardrails to prevent users from unexpectedly being exposed to potentially harmful or offensive content by using a combination of proactive and reactive algorithmic and manual interventions. These interventions only apply to search suggestions; users are never prevented from manually entering a search query.

You may turn on and off autosuggest feature in the Bing Setting page ([Search - Settings (bing.com))](https://www.bing.com/account/general) by selecting or unselecting the “See search suggestions as you type” checkbox.

### 

Reports and Appeals

**Reporting problematic content**

If you find objectionable content on Bing, [report it](https://www.microsoft.com/en-us/concern/bing). We value your reports and work continuously to improve your search experience.

**For webmasters: appealing webpage content removal decisions**

If Bing has removed your site or page from the index and you’ve addressed the issues and would like to have your site re-evaluated, you can appeal using the [Bing webmaster tools](https://www.bing.com/toolbox/webmaster/). [Learn more](https://www.bing.com/webmaster/help/help-center-661b2d18) about the process.

**User Appeals**

Use of all Bing products and features are governed by [Microsoft Services Agreement](https://www.microsoft.com/en-us/servicesagreement). Persistently engaging in activities that violate the Agreement may result in permanent suspension and/or restriction of a user’s Microsoft Account.

Users may appeal restrictions/suspensions via Bing’s Report a Concern form. Bing’s support team will review the decisions. If the original actions were found to have been taken in error, the actions will be reversed and the notifications will be sent to the affected users, if contact information was provided to Bing.

Transparency
------------

Every six months, Microsoft publishes transparency reports that include the number of requests for removal we’ve received in certain categories and approved during that timeframe. You can see some details about the requests in [Reports Hub | Microsoft CSR](https://www.microsoft.com/en-us/corporate-responsibility/reports-hub).

Related topics
--------------

[Remove cached pages from Bing](https://go.microsoft.com/fwlink?LinkId=282036)

[Submit a request to remove content from Bing](https://go.microsoft.com/fwlink?LinkId=313698)

[Report adult or offensive content](https://go.microsoft.com/fwlink?LinkId=275671)

[Block adult content with SafeSearch](https://support.microsoft.com/en-gb/topic/blocking-adult-content-with-safesearch-or-blocking-chat-946059ed-992b-46a0-944a-28e8fb8f1814)