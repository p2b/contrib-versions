Regulament persoane fizice
==========================

Termeni și condiții pentru clienții persoane fizice se aplică începând cu data de **03.01.2025**

Versiunea anterioare a Termenilor și Condițiilor o regasesti [aici](https://ajutor.autovit.ro/autovithelp/s/article/regulament-persoane-fizice-anterior-V2).

  
1\. Prevederi preliminare

1. Acești Termeni și Condiții definesc regulile pentru furnizarea, de către OLX Online Services SRL, către utilizatorii de Internet, de servicii constând în a le permite acestora să posteze Anunturi pe Site-ul Autovit, împreună cu regulile de accesare și utilizare a resurselor site-ului Autovit.
    

2. Site-ul Autovit este disponibil la adresa: autovit.ro și printr-o aplicație mobilă. Condiția prealabilă pentru obținerea accesului la funcționalitățile Site-ului Autovit este utilizarea unui dispozitiv care comunică cu Internetul și care are un browser web standard. Operatorul de telecomunicații poate percepe o taxă suplimentară pentru accesul la aplicații de pe dispozitivele mobile.
    
3. Folosirea site-ului [www.autovit.ro](https://www.autovit.ro/) și a serviciilor furnizate de acesta sunt rezervate doar persoanelor care au împlinit vârsta de 18 ani.
    

2\. Definiții  
  
Următorii termeni, așa cum sunt definiți mai jos, vor avea următoarele semnificații:  
  

1. Serviciul de Număr de Telefon Virtual - O funcție care înlocuiește numărul de telefon real al Autorului Anunțului din conținutul Anunțului cu un număr virtual gestionat de Furnizor Acest număr virtual permite medierea apelurilor telefonice între utilizatori și Autorului Anunțului, asigurând confidențialitatea și securitatea. Serviciul este furnizat la costul indicat în **Anexa 1** la Termeni și Condiții. În plus, serviciul asigură că Autorul Anunțului este notificat printr-un mesaj automat că apelul a fost inițiat prin Furnizor.
    
2. Furnizor/OLX Online Services SRL – Societatea OLX Online Services SRL, persoană juridică română, cu sediul în Bucuresti, Sector 1, Sos. Nicolae Titulescu, nr. 4-8, Clădirea America House– Aripa de Vest, Etaj 5, înregistrată la Oficiul Registrului Comertului sub nr. J40/1674/2011, având codul de identificare fiscală RO22457535, fiind înregistrată în Evidența Prelucrării Datelor cu Caracter Personal sub nr. 17272, si avand nr. de telefon: 031/860.90.90 și email: sugestii@autovit.ro.
    

2. Site-ul Autovit – un site web operat de Furnizor, în limba română, disponibil în domeniul autovit.ro, prin intermediul unei aplicații mobile, precum și pe alte site-uri web administrate de partenerii Furnizorului, la care se face referire în paginile Site-ului Autovit.
    

3. Anunț – oferta publică lansată de un utilizator cu scopul de a fi publicată de către Furnizor, prin intermediul Site-ului Autovit, cu privire la vânzarea de vehicule sau produse conexe, în conformitate cu termenii și condițiile prevăzute în prezentul document.
    

4. Utilizator – Autorul Anunțului, precum și orice altă persoană, în vârstă de minim 18 ani, care utilizează resursele Site-ului Autovit.
    

5. Autorul Anunțului – o persoană fizică, în vârstă de 18 ani și cu capacitate deplină de exercițiu sau o unitate organizatorică fără personalitate juridică, dar capabilă să dobândească drepturi și să își asume răspunderi în legătură cu Anunțul publicat de aceștia, care, în modul prevăzut prin prezenta, a încheiat un contract cu Furnizorul privind afișarea unui Anunț pe Site-ul Autovit și care utilizează serviciile furnizate în cadrul Site-ului Autovit.
    

6. Cont al Autorului Anunțului  – o înregistrare IT individualizată, care a fost creată pe Site-ul Autovit pentru Autorul Anunțului, pentru a face posibilă utilizarea de către acesta a Site-ului Autovit și care adună informații despre utilizarea de către Autorul Anunțului a Site-ului Autovit. Accesul la Cont este asigurat Autorulului Anunțului printr-o adresă de e-mail unică și o parolă. Contul Autorului Anunțului poate fi conectat doar la o singură adresă de e-mail.
    

7. Lista de Prețuri – o listă a prețurilor percepute de Furnizor Autorului Anunțului, pentru serviciile furnizate în cadrul Site-ului Autovit.
    

8. Articol – o mașină, dubă, camion, motocicletă, moped, scuter, quad, utilaj de construcții sau agricol, combină, motocicletă sau orice altă piesă, accesorii auto unice, precum și orice alte bunuri care, conform deciziei Furnizorului, pot face obiectul unui Anunț pe Site-ul Autovit.
    

9. Vehicul -  orice tip de autovehicul, ca parte a definiției de mai sus.
    

10. Fonduri Autovit – un portofel electronic virtual, alocat automat Contului Autorului Anunțului, pentru a permite acestuia să efectueze plăți în avans pentru serviciile contra cost ale Site-ului Autovit. Pentru informații detaliate despre plățile în avans consultați Anexa nr. 7 la acest document.
    

11. Website OLX - site-ul [www.olx.ro](https://www.olx.ro/), inclusiv orice secțiune sau sub-pagină a acestuia, administrat de Furnizor, întreținut în domeniul olx.ro, disponibil și în cadrul altor site-uri administrate de partenerii Furnizorului.
    

12. Termeni și Condiții – prevederile din acest document, create de OLX Online Services SRL, care definesc condițiile și regulile pe care Utilizatorul trebuie să le respecte în vederea utilizării Site-ului Autovit, aplicabile în egală măsură la achiziționarea de către Utilizator a serviciilor furnizate de OLX Online Services SRL, prin intermediul Autovit.
    

13. Consumator – Autorul Anunțului, care este consumator în sensul articolului 2 din  Ordonanța 21/1992 privind protecția consumatorilor, respectiv orice persoană fizică sau grup de persoane fizice constituite în asociaţii, care activează pe Site-ul Autovit în scopuri din afara activităţii sale comerciale, industriale sau de producţie, artizanale ori liberale.
    

14. Invalidare – o situație în care (1) Articolul a fost vândut sau (2) Utilizatorul a modificat Anunțul în așa fel încât să indice că se referă la un articol diferit de cel inițial.
    
15. Dealer – un antreprenor care comercializează vehicule sau produse conexe, oferind Articole spre vânzare în calitate de proprietar sau dealer al acestora, care a încheiat un contract cu Furnizorul conform procedurii prevăzute în prezentul document pentru publicarea Anunțurilor pe Site-ul Autovit și care utilizează serviciile oferite în cadrul Site-ului Autovit.
    

   
3\. Termeni și Condiții Generale de Utilizare a Site-ului Autovit  
  
1\. Pentru a obține funcționalitatea completă a Site-ului Autovit, Utilizatorul trebuie să înregistreze un Cont și să utilizeze Site-ul Autovit după autentificarea la Contul său. Contul oferă Utilizatorului posibilitatea de a folosi, printre altele, următoarele funcționalități:

1. publicarea și gestionarea Anunțurilor publicate;
    
2. urmărirea Anunțurilor;
    
3. gestionarea plăților și facturilor aferente serviciilor prestate pe Site-ul Autovit de către Furnizor;
    
4. trimiterea și primirea de mesaje către și de la alți Utilizatori;
    
5. comandarea serviciilor de promovare a Anunțurilor.
    

2\. Înregistrarea contului necesită:

1. completarea formularului disponibil pe Site-ul Autovit și furnizarea datelor solicitate, inclusiv adresa de e-mail și o parolă unică sau autentificare printr-un furnizor extern de servicii precum Facebook, Google sau Apple;
    
2. citirea Termenilor și Condițiilor și a anexelor la prezenta și acceptarea prevederilor acestora.
    

3\. Un Anunț este postat pe Site-ul Autovit de către Autorul Anunțului, care este singurul responsabil pentru aranjarea conținutului său.  
4\. Un Anunț trebuie să se refere, în mod clar, la vânzarea unui singur Articol. În timpul afișării Anunțului, Autorul Anunțului nu are posibilitatea de a schimba Articolul. În același timp, același Articol nu poate să facă obiectul unui alt Anunț la un moment dat. Pe Site-ul Autovit sunt afișate doar Anunțuri actualizate. Furnizorul are dreptul de a opri afișarea unui Anunț, dacă a stabilit, în mod rezonabil, că acesta a devenit invalid.  
5\. Anunțul trebuie să specifice categoria corectă a Articolului (de exemplu, mașină de pasageri din categoria „pasager”) și să includă numai informații relevante pentru promovarea Articolului.  
6\. Durata de bază de afișare a unui Anunț pe Site-ul Autovit este de cel mult 15 sau 30 de zile. Durata de afișare a Anunțului poate fi prelungită în mod corespunzător, în modul descris în prezentul document.  
7\. Nu pot fi adăugate Anunțuri care nu au legătură cu industria auto.  
8\. Conținutul unui Anunț trebuie să reflecte cu exactitate situația actuală de fapt și legală a Articolului, iar un Anunț poate fi postat numai dacă există o intenție reală de a vinde Articolul, la prețul specificat în Anunț. Orice informație despre Articol, inclusiv prețul, care rezultă din conținutul Anunțului, trebuie să fie în concordanță cu informațiile specificate de Autorul Anunțului în formularul completat în momentul adăugării Anunțului. Conținutul unui Anunț trebuie să precizeze în mod clar și fără ambiguitate dacă Anunțul are ca subiect un anumit set de Articole sau un singur Articol și să indice dacă prețul specificat de Anunț se referă la un set de Articole sau un singur Articol.  
9\. Conținutul Anunțului, sub formă de fotografii adăugate de către Autorul Anunțului, trebuie să includă numai Articolul care face obiectul Anunțului. Fotografiile adăugate la Anunț de către Autorul Anunțului nu trebuie să conțină adrese de e-mail, adrese de site-uri web, numere de telefon, numere de mesagerie de internet, filigrane sau alte mărci care identifică Autorul Anunțului.  
10\. Autorul Anunțului poate include un link video YouTube ca parte a conținutului Anunțului (în condițiile specificate pe youtube.com), care va fi o prezentare animată a Articolului (denumit în continuare „Videoclipul”). Videoclipurile pot fi adăugate de către Autorul Anunțului numai în câmpul desemnat; nu este permisă postarea videoclipurilor în câmpul Descriere Articol. Nu sunt permise videoclipuri care prezintă alt conținut decât prezentarea Articolului oferit, cum ar fi adrese de e-mail, adrese de site-uri web, numere de telefon, numere de mesagerie instantanee, filigrane sau alte mărci care identifică Autorul Anunțului.  
11\. Autorul Anunțului va fi responsabil pentru conținutul pe care îl publică (inclusiv fotografii și videoclipuri) și declară și garantează faptul că respectivul conținut reflectă cu exactitate starea actuală a Articolului și este în conformitate cu legile aplicabile, iar publicarea acestuia nu încalcă Termenii și Condițiile, drepturile și interesele Furnizorului și drepturile terților, inclusiv drepturile de autor. Furnizorul nu va fi responsabil pentru nicio acțiune din partea Autorilor de Anunțuri, nici pentru acuratețea, fiabilitatea și legalitatea conținutului pe care îl publică.  
12\. În scopul promovării Anunțului, Autorul Anunțului autorizează Furnizorul să publice fotografiile postate în Anunț pe rețelele sociale din România și străine în perioada în care Anunțul este afișat pe Site-ul Autovit. În plus, Autorul Anunțului, prin plasarea conținutului protejat prin drepturi de autor într-un Anunț, este de acord cu distribuirea sa gratuită, nelimitată în timp și teritoriu, pe site-urile web aparținând Furnizorului, precum și pe site-urile web partenere, cu care Furnizorul cooperează, în scopul:

1. înregistrării și reproducerii prin orice tehnică, inclusiv înregistrarea sau copierea pe orice suport;
    
2. intrării în memoria computerului și într-o rețea de computer și/sau multimedia;
    
3. expunerii și reproducerii publice;
    
4. utilizării în diverse formate, cu dreptul de a le încorpora integral sau parțial în alte lucrări și crearea de lucrări derivate.
    

13\. Descărcarea sau utilizarea materialelor disponibile în cadrul Site-ului Autovit necesită de fiecare dată acordul Furnizorului și nu poate încălca prevederile prezentului document și legile aplicabile, precum și interesele Furnizorului și ale Autorilor de Anunțuri. Datele și informațiile disponibile pe Site-ul Autovit nu trebuie să fie agregate sau prelucrate în scopul dezvăluirii către terți în cadrul altor site-uri de Internet și în afara Internetului. Marcajele Site-ului Autovit, inclusiv elementele grafice caracteristice, nu pot fi utilizate fără acordul Furnizorului.  
14\. Descărcarea și reutilizarea unei părți irelevante din bazele de date utilizate pentru îndeplinirea sarcinilor Site-ului Autovit este permisă cu condiția ca aceasta să nu conducă la nici o încălcare a regulilor de utilizare a Site-ului Autovit, definite în Termeni și Condiții și, în special, aceasta să nu implice descărcarea, agregarea sau prelucrarea repetată și sistematică a datelor cu încălcarea Termenilor și condițiilor și nu va afecta în mod nerezonabil interesele legitime ale Furnizorului.  
15\. Site-ul Autovit afișează de asemenea conținut publicitar. Reclamele disponibile în cadrul Site-ului Autovit sunt afișate în funcție de alegerile Utilizatorului privind procesarea cookie-urilor sau în funcție de setările browserului. Dacă Utilizatorul a consimțit la personalizarea conținutului, reclamele afișate sunt adaptate preferințelor Utilizatorului, pe baza activității acestuia în cadrul Site-ului Autovit. Dacă Furnizorul nu a obținut consimțământul Utilizatorului pentru personalizarea conținutului publicitar, reclamele afișate nu vor fi personalizate și se vor afișa în baza unor creații publicitare pregătite pentru baza generală de Utilizatori.  
16\. Utilizatorul are opțiunea de a raporta către Site-ul Autovit orice conținut ilegal sau conținut care încalcă Termenii și Condițiile, prin funcția "raportare încălcare", disponibilă în cadrul Anunțului ("Raport de Abuz"). Raportorul ar trebui, dacă este posibil, să furnizeze informații care să permită Site-ului Autovit să verifice Raportul de Abuz, incluzând, în special, ID-ul Anunțului, o explicație a motivelor pentru care consideră conținutul a fi ilegal sau contrar Termenilor și Condițiilor, precum și detaliile de contact, dacă acestea sunt solicitate în formularul de raportare.  
17\. Prevederile articolului 10 se aplică procedurii privind plângerile depuse de Utilizatori ai Site-ului Autovit, care au raportat conținut interzis și cărora le sunt direcționate deciziile legate de blocarea sau suspendarea conținutului disponibil în cadrul Site-ului Autovit. Cei care raportează conținut interzis pot depune o plângere privind decizia transmisă în legătură cu acțiunile întreprinse referitoare la blocarea sau suspendarea conținutului sau a Conturilor, care încalcă Termenii și Condițiile sau reglementările legale aplicabile. Plângerea poate fi formulată în termen de 6 luni de la data primirii deciziei.

18\. Utilizatorul poate urmări anunțurile care sunt de interes pentru acesta salvând căutările și adăugând anunțuri la Lista de Favorite. Pe baza acestor preferințe, Utilizatorul va primi notificări cu privire la anunțuri noi legate de căutările salvate sau de Favorite. Notificările pentru anunțuri noi legate de o căutare salvată se vor dezactiva automat dacă Utilizatorul nu accesează pagina cu rezultatele căutării după primirea a 30 de notificări. Utilizatorul poate gestiona setările de notificare, inclusiv reactivarea sau dezactivarea notificărilor, în orice moment prin intermediul setărilor contului său.

  
4\. Încheierea unui contract  
1\. Încheierea unui acord de către Autorul Anunțului cu privire la afișarea Anunțului necesită desfășurarea activităților specificate la punctul 5.1 de mai jos și are loc la primirea de către Autorul Anunțului a mesajului transmis de Furnizor conform punctului 5.2 de mai jos.  
2\. Regulile de retragere din contractul privind afișarea Anunțurilor de către un Consumator sunt prevăzute în Anexa nr. 5 la Termeni și Condiții.  
3\. Furnizorul are dreptul de a verifica statutul Autorului Anunțului și de a refuza să-l recunoască drept Consumator dacă se constată că Anunțurile postate au legătură cu activitatea comercială sau profesională a Autorului Anunțului. În acest sens, Furnizorul ține cont, în special, de numărul de Anunturi postate (mai mare decât numărul de anunțuri postate de Consumatori) și de conținutul acestora. Un Autor al Anunțului căruia i s-a refuzat statutul de Consumator are dreptul să depună explicații și documente. Pe baza acestora, Furnizorul ia decizia finală în această privință.  
  
  

4\. Utilizatorul poate avea un singur Cont în cadrul Site-ului Autovit. Această regulă nu se aplică atunci când:

a. Utilizatorul are conturi diferite, utilizate în scopuri private și legate de activitatea sa comercială, caz în care Utilizatorul poate avea cel mult un cont utilizat în scopuri private și cel mult un cont pentru fiecare entitate, pentru activitatea sa comercială;

b. va fi necesar să creați un alt Cont din cauza imposibilității de a avea acces la Contul vechi (exemplu: uitarea parolei).

Cu toate acestea, oricare dintre ipotezele de mai sus vor fi verificate în detaliu de către Furnizor, care are dreptul de a suspenda Contul în momentul verificării sau de a șterge Contul, în absența confirmării circumstanțelor care justifică aplicarea uneia dintre excepțiile de mai sus. Excepțiile prevăzute de prezenta secțiune nu se vor aplica, dacă sunt invocate în scopul evitării plății creanțelor pentru serviciile furnizate pe Site-ul Autovit.

  
5\. Postarea și editarea unui Anunț de către Autorul Anunțului  
  
1\. Postarea unui Anunț de către Autorul Anunțului pe Site-ul Autovit necesită:  
1.1. completarea formularului disponibil pe Site-ul Autovit, cel puțin în câmpurile marcate ca obligatorii, și acceptarea prealabilă a Termenilor și Condițiilor;  
1.2 activarea Anunțului prin efectuarea plății (în sumă datorată integrală) folosind următoarele metode:  
1.2.1. Transfer online, forma de plată electronică operată de PayU S.A.;  
1.2.2. Plata prin sms, operată de Netopia Payments SRL;  
1.2.3 Plata cu cardul, operată de PayU S.A;  
1.2.4. Plata prin Paypal, operată de PayU S.A;  
1.2.3 Plată prin Fonduri Autovit, colectate în Contul Autorului Anunțului.  
2\. Odată ce toate activitățile enumerate la punctul de mai sus au fost efectuate de către Autorul Anunțului, Furnizorul trimite un mesaj la adresa de e-mail indicată în formular pentru a confirma acceptarea comenzii de afișare a Anunțului și pentru a furniza alte informații cerute de lege.  
3\. Anunțurilor li se aplică regulile stabilite în Anexa nr. 4 – _Reguli pentru postare_, care stabilește și situațiile în care Furnizorul poate șterge, modifica și/sau bloca un Anunț.  
4\. Anunțul este activat:  
4.1. în cazul transferului online, adică a formei de plată electronică operată de PayU – la afișarea plății;  
4.2. în cazul plății prin sms – la virarea taxei de către Netopia;  
4.3. în cazul în care sunt utilizate fondurile Autovit colectate în Contul Autorului Anunțului – odată ce acestea au fost epuizate.  
5\. Afișarea Anunțului pe Site-ul Autovit și pe site-urile partenere începe în cel mult 2 ore de la activarea Anunțului.  
6\. Folosind formularul la care se face referire la punctul 5.1.1.1 de mai sus, Autorul Anunțului poate atașa de la 1 la 40 de fotografii ale Articolului la Anunț (în formatul specificat pe Site-ul Autovit).  
7\. În perioada de afișare și în termen de 6 luni de la încetarea afișării pe Site-ul Autovit, Autorul Anunțului are acces la panoul administrativ care permite editarea Anunțului, adică modificarea conținutului acestuia, în câmpurile editabile, precum și extinderea termenului de afișare a Anunțului cu încă 15 sau 30 de zile, împreună cu alegerea metodei de activare a acestuia.  
8\. Prelungirea termenului de afișare cu încă 15 sau 30 de zile necesită activarea în modul specificat la punctul 5.1.2 de mai sus și are loc nu mai târziu de 2 ore de la finalizarea acestuia. Dacă Anunțul nu este activat, așa cum este descris mai sus, în termen de 6 luni de la sfârșitul afișării sale, nu va fi posibilă reînnoirea afișarii acestuia.  
9\. În cazul încetării anticipate a afișării Anunțului, din cauza eliminării Anunțului de către Autorul Anunțului sau dacă Articolul a fost vândut sau Autorul Anunțului a schimbat obiectul Anunțului într-un mod care indică faptul că acesta se referă la un Articol diferit de cel inițial, taxa percepută pentru publicarea Anunțului respectiv și a eventualelor servicii de promovare Anunț asociate pentru perioada neutilizată nu va fi returnată.  
  
6\. Promovarea Anunţurilor  
  
1\. Autorul Anunțului poate să-și promoveze suplimentar Anunțurile, prin activarea unuia sau mai multor servicii de promovare Anunțuri, cu taxă, definite în Anexa nr. 2 la prezentul document.  
2\. Serviciile de promovare Anunțuri sunt furnizate din momentul activării lor și continuă pentru un anumit număr de zile, definit corespunzător de către Autorul Anunțului. În cazul în care metodele de promovare la care se face referire mai sus sunt selectate să dureze mai mult decât perioada de afișare a Anunțului, perioada de afișare se va prelungi automat cu încă 15 sau 30 de zile, după caz, ceea ce va avea ca rezultat perceperea unei taxe pentru afișarea Anunțului, conform Anexei nr. 1 - Lista de prețuri.  
3\. Sfera și termenii anumitor Servicii de promovare Anunț pot fi definite în Termeni și condiții separate. În scopul serviciilor în cauză, drepturile și responsabilitățile Autorului Anunțului pot diferi de cele specificate în acești Termeni și Condiții. Acceptarea termenilor și condițiilor aplicabile este o condiție prealabilă pentru utilizarea acestor servicii.  
  
7\. Reguli privind răspunderea  
  
1\. În cazul în care acțiunile Utilizatorului încalcă prevederile Termenilor și Condițiilor, Furnizorul poate, în funcție de tipul și amploarea încălcării, să condiționeze utilizarea Site-ului Autovit de depunerea, de către User, a unei documentații suplimentare, care să confirme datele Utilizatorului sau de confirmarea de către Utilizator a îndeplinirii obligațiilor față de Furnizor, inclusiv executarea plăților restante, a trimiterii unei avertizări Utilizatorului prin e-mail, a suspendării funcționalității Contului Autorului Anunțului sau a Anunțului pentru o perioadă determinată sau nedeterminată, în scopul accesului la serviciile individuale de pe Site-ul Autovit, sau poate supune înregistrarea Contului Autorului Anunțului pe Site-ul Autovit acordului prealabil scris al Furnizorului.  
2\. Permițând afișarea Anunțurilor, Furnizorul își rezervă dreptul de a le modifica în măsura în care este necesar și de a schimba categoria de Anunțuri selectate de Autorul Anunțului, în cazul în care se constată că Anunțul încalcă prezenții Termeni și Condiții, măsură care îi va fi comunicată Autorului Anunțului, fără întârzieri nejustificate.  
3\. Furnizorul nu este responsabil pentru neexecutarea sau executarea necorespunzătoare de către Autorul Anunțului a oricăror contracte încheiate în legătură cu Anunțul sau pentru consecințele oricăror acțiuni întreprinse de către Autorul Anunțului și de către terți, cu încălcarea Termenilor și condițiilor. Furnizorul nu este responsabil pentru starea tehnică, siguranța sau legitimitatea Articolelor oferite sau pentru acuratețea și fiabilitatea informațiilor furnizate în Anunțuri de către Autorul Anunțului, și nici pentru capacitatea vânzătorilor și cumpărătorilor de a urmări tranzacția.  
4\. Încălcările Termenilor și Condițiilor sunt considerate a include, de exemplu, afișarea unui Anunț care conține o ofertă de cumpărare sau schimb (în loc de vânzare) a Articolului sau o subestimare semnificativă a valorii Articolului, postarea unui Anunț privind unul sau mai multe Articole, manipularea cuvintelor cheie dintr-un Anunț, afișarea unui Anunț într-o categorie inadecvată sau furnizarea de informații înșelătoare într-un Anunț, precum și Anunțuri având un conținut care este în general considerat a fi ofensator, poartă semnele distinctive ale actelor de concurență neloială, încalcă principiile moralității, drepturi de autor sau alte drepturi de proprietate intelectuală, prejudiciază Furnizorul sau alte entități, precum și conținutul promoțional sau publicitar, link-uri sau logo-uri care direcționează către alte site-uri decât Autovit și conținut înșelător. Publicarea de Anunturi cu privire la Vehicule care sunt stricate sau arse, Anunțuri care reprezintă licitații pentru articole și adăugarea de fotografii cu produse sau servicii altele decât cele care fac obiectul Anunțului reprezintă, de asemenea, o încălcare a prezentului regulament. Acțiunile de mai sus vor duce la întreprinderea de către Furnizor a acțiunilor specificate la punctul 7.1 de mai sus.  
5\. În plus, Furnizorul nu își asumă răspunderea pentru:

1. lipsa de interes pentru Articol;
    
2. orice îndeplinire a obligațiilor care decurg din garanția expresă și implicită cu privire la Articol;
    
3. orice declarații făcute de Utilizator către cealaltă parte la tranzacție;
    
4. funcționarea oricăror sisteme sau dispozitive TIC aflate în afara controlului Furnizorului;
    
5. evenimente de forță majoră.
    

6\. Orice materiale, inclusiv elementele grafice, precum și aspectul și componență acestora, mărcile comerciale și alte informații disponibile pe paginile Site-ului Autovit fac obiectul drepturilor exclusive ale Furnizorului sau ale Autorilor de Anunțuri. Elementele menționate sunt acoperite de drepturi de autor, drepturi de proprietate industrială, inclusiv drepturi care decurg din înregistrarea mărcilor și drepturi asupra bazelor de date și, ca atare, sunt protejate prin lege.  
7\. Furnizorul își rezervă dreptul de a efectua verificări, atât în urma raportărilor primite, cât și în mod proactiv, și de a solicita informații despre Anunțurile Utilizatorilor (exemplu: actele de proprietate ale Vehiculului, precum factura, contract vânzare, carte Vehicul,  Seria de sasiu (V.I.N) și orice alte informații suplimentare necesare). Informațiile vor fi solicitate de Furnizor prin intermediul adresei de email, mesageriei sau telefonic. Furnizorul își rezervă dreptul de a dezactiva Anunțul/Contul Utilizatorului, în cazul unui refuz nejustificat de a transmite informațiile solicitate.  
8\. Furnizorul își rezervă dreptul de a efectua verificări cu privire la istoricul Vehiculelor listate pe Site-ul Autovit, cu ajutorul platformelor de specialitate (ex. Auto DNA).  
9\. Furnizorul își rezervă dreptul de a dezactiva Anunțurile Utilizatorilor, care, în urma verificărilor efectuate, prezintă Vehicule a căror situație nu corespunde informațiilor obținute sau rezultatului verificărilor efectuate de către Furnizor. Cu titlu de exemplu, dacă în urma verificărilor efectuate de către Furnizor, informațiile prezentate în cadrul Anunțului (exemplu: stare, kilometraj, dotări etc) diferă de cele obținute de Furnizor, Anunțul poate fi dezactivat.  
10\. Furnizorul își rezervă dreptul de a refuza crearea unui cont sau de a bloca un cont deja creat în cazul în care, din verificările legitime ale Furnizorului, rezultă că Utilizatorul a produs sau poate produce un prejudiciu financiar societății OLX Online Services SRL.  
  
11\. În cazul unei încălcări grave și repetate a legii prin utilizarea Site-ului Autovit, precum și a unei încălcări grave sau repetate a prevederilor Termenilor și Condițiilor, în special în cazul încercărilor repetate de a publica sau difuza conținut ilegal de către un anumit Utilizator sau diferiți Utilizatori care acționează în comun și în acord, inclusiv utilizarea diferitelor Conturi, Furnizorul poate, în conformitate cu principiile proporționalității și respectării libertății de tranzacționare, să suspende sau să blocheze temporar sau definitiv Contul sau Conturile, ceea ce va echivala cu o suspendare temporară a prestării serviciilor către Utilizator, ținând cont de următoarele reguli:  
  

1. Suspendarea Contului înseamnă suspendarea temporară a funcționalității acestuia. Utilizatorul al cărui Cont a fost suspendat pierde posibilitatea de a utiliza în mod activ Site-ul Autovit, ceea ce înseamnă că nu poate trimite mesaje folosind funcționalitatea acestuia sau nu poate publica Anunțuri. Cu toate acestea, Utilizatorul poate vizualiza Site-ul Autovit, istoricul mesajelor și Anunțurile publicate de acesta. Pentru a plăti fondurile alocate, Utilizatorul trebuie să contacteze Furnizorul la următoarea adresă de e-mail: plati@autovit.ro;
    
2. Blocarea Contului înseamnă că Utilizatorul pierde posibilitatea de a se conecta la Cont;
    
3. Utilizatorul va fi informat cu privire la blocarea unui anumit conținut, cel mai târziu la intrarea în vigoare a blocării, sub forma unui e-mail, indicând conținutul blocat, motivele deciziei privind blocarea și motivele decizie rezultate din Termeni și Condiții
    

12\. Furnizorul poate bloca permanent Contul dacă securitatea utilizării Contului este pusă în pericol sau în cazul unor încălcări ale Termenilor și Condițiilor, cum ar fi: utilizarea conturilor create de bot, solicitarea plății în avans în mesajele de chat, trimiterea de mesaje de tip phishing, încercarea de a obține date personale de la alți utilizatori, încercarea de a vinde produse inexistente sau oferirea de produse foarte căutate la un preț neobișnuit de mic, transmiterea unor date false Furnizorului sau Utilizatorilor, precum și încălcări repetate ale Termenilor și Condițiilor.  
13\. Utilizatorul care nu este de acord cu decizia de blocare sau suspendare a conținutului sau a Contului are dreptul să depună o plângere, în conformitate cu prevederile articolului 10 din Termeni și Condiții.  
14\. Utilizatorul care nu este de acord cu decizia Furnizorului are dreptul să depună o plângere, în conformitate cu prevederile articolului 3.17 din Termenii și Condițiile Site-ului Autovit. Pentru a evita orice confuzie, indiferent dacă se ridică sau nu o obiecție în situațiile prevăzute de articolul 3.17, acest lucru nu afectează dreptul Utilizatorului de a depune o plângere folosind celelalte mijloace disponibile conform articolului 10 din prezentul document.  
15\. Furnizorul poate lua decizii de a suspenda sau bloca conținutul sau Contul pe baza politicilor interne și a procedurilor de moderare. Aceste politici și proceduri de moderare, adoptate de Furnizor, conturează metodele și mijloacele de identificare a conținutului sau a acțiunilor utilizatorilor care sunt examinate pentru încălcări ale Termenilor și Condițiilor sau ale legilor aplicabile. Obiectivul principal al politicilor și procedurilor de moderare dezvoltate este de a menține siguranța utilizatorilor și de a combate orice tip de abuzuri.  
16\. Procesul de identificare a conținutului sau a acțiunilor utilizatorilor se poate baza pe mijloace și instrumente de moderare automatizate sau semi-automatizate. Aceste instrumente permit identificarea conținutului sau a acțiunilor care încalcă sau pot încălca Termenii și Condițiile sau reglementările legale aplicabile, prin analizarea conținutului Anunțurilor sau activității Utilizatorilor în cadrul Site-ului Autovit. Instrumentele folosite în procesul de moderare a conținutului disponibil în cadrul Site-ului Autovit utilizează soluții care permit analiza conținutului Anunțurilor sau identificarea acțiunilor care pot încălca Termenii și Condițiile sau reglementările legale, pe care le raportează echipelor interne de moderare ale Furnizorului. Deciziile legate de încălcările Termenilor și Condițiilor de Utilizare, prezentate mai sus, sau acțiunile legate de încălcarea securității Serviciului sau a Utilizatorilor pot fi luate într-o manieră complet automatizată. Alte decizii legate de conținutul adăugat de utilizatori sau acțiunile acestora sunt supuse evaluării de către echipele de moderare ale Furnizorului. Acțiunile legate de conținutul publicat sau activitățile utilizatorilor care încalcă sau pot încălca Termenii și Condițiile sau legile aplicabile sunt luate și ca răspuns la rapoartele transmise de alți Utilizatori, autorități sau organizații dedicate asigurării siguranței pe internet a utilizatorilor sau combaterii conținutului ilegal.  
  

8\. Modalități de plată  
  
1\. Serviciile furnizate în cadrul Site-ului Autovit, în beneficiul Autorilor de Anunțuri, sunt contra cost. Cuantumul taxelor este specificat în Anexa nr. 1 - _Lista de prețuri_.  
2\. Autorul Anunțului efectuează plăți în avans pentru utilizarea Site-ului Autovit, în condițiile definite în Anexa nr. 7.  
3\. Prețurile de vânzare afișate sunt în Euro si includ TVA.  
  
Prețul de vânzare va fi afișat în RON înainte de finalizarea achiziției, utilizând următoarea formulă de calcul: cursul de schimb EURO / RON comunicat de Banca Centrala Europeană valabil pentru ultima zi de curs publicată, anterioară plății, la care se adaugă 1%.  
Similar, formula de calcul se utilizează în conversie pentru orice alte valute în care se afișează prețul de vânzare. Cursurile de schimb valutar practicate de Banca Centrala Europeană pot fi consultate [aici](https://www.ecb.europa.eu/stats/policy_and_exchange_rates/euro_reference_exchange_rates/html/eurofxref-graph-ron.ro.html).  
  
_Exemplu: Prețul de vânzare afișat = 100 EURO (inclusiv TVA). Cursul Băncii Centrale Europene aplicabil tranzacției 5 RON = 1 EURO. Prețul de vânzare = 100 x (5+1/100 x 5) = 505 RON (inclusiv TVA)._4\. Pentru serviciile prestate în cadrul Site-ului Autovit, Furnizorul emite factură fiscală.  
5\. Domeniul de aplicare și regulile detaliate de emitere și trimitere a facturilor sunt reglementate de Anexa nr. 6 la Termeni și Condiții.  
  
9\. Confidențialitate  
  
1\. Furnizorul colectează și prelucrează datele cu caracter personal furnizate de Utilizatori în conformitate cu legile aplicabile și cu Politica de confidențialitate, care poate fi studiată aici ([link](https://ajutor.autovit.ro/hc/ro/articles/360004226394-Noua-politica-de-confidentialitate)).  
2\. Datele cu caracter personal ale Utilizatorilor sunt dezvăluite altor Utilizatori numai în cazurile expres prevăzute de Politica de Confidențialitate și în alte cazuri justificate, cu acordul prealabil al persoanei căreia îi aparțin aceste date.  
3\. Utilizatorii trebuie să arhiveze pe cont propriu informațiile despre conținutul Anunțurilor și despre contractele încheiate în cadrul Site-ului Autovit.  
  
10\. Procedura de depunere și soluționare a reclamațiilor și sesizărilor  
  
1\. Utilizatorul are dreptul de a depune reclamații în cazul neexecutării sau executării necorespunzătoare de către Furnizor a serviciilor prestate în cadrul Site-ului Autovit, precum și în legătură cu utilizarea resurselor Site-ului Autovit.  
2\. Reclamațiile trebuie formulate Furnizorului, prin completarea formularului de contact disponibil la  [https://ajutor.autovit.ro/autovithelp/s/contactsupport](https://ajutor.autovit.ro/autovithelp/s/contactsupport) sau prin e-mail, la următoarea adresă de e-mail: ajutor@autovit.ro.  
3\. O reclamație trebuie să conțină datele Utilizatorului (numele și prenumele, adresa de corespondență, adresa de e-mail și numărul de telefon), numărul Anunțului la care se referă reclamația (dacă există), precum și orice alte date care fac posibilă identificarea Anunțului, precum și împrejurările în susținerea reclamației.  
4\. Reclamațiile vor fi procesate în cel mai scurt timp posibil, dar nu mai târziu de 30 de zile de la data primirii lor.  
5\. În cazul în care detaliile furnizate în reclamație trebuie completate, Furnizorul va solicita Utilizatorului să indice informațiile suplimentare, în termenul specificat.  
6\. Utilizatorul va fi informat cu privire la decizia privind reclamația, prin e-mail, la adresa de e-mail indicată în reclamație sau adresa de corespondență indicată Furnizorului.  
7\. Utilizatorii care au raportat conținut ilegal și cărora le sunt direcționate deciziile legate de blocarea sau suspendarea conținutului disponibil în cadrul Site-ului Autovit, au dreptul să aleagă orice organism de soluționare alternativă a litigiilor, care deține un certificat emis de Coordonatorul Serviciilor Digitale.  
  
  
11\. Dezactivarea Anunțurilor  
  
1\. Dacă Anunțul nu este activat în decurs de 6 luni de la afișarea acestuia, nu va fi posibilă reînnoirea afișării acestuia.  
2\. Dacă afișarea unui Anunț este finalizată înainte de data programată (15 sau 30 de zile), taxa pentru zilele de afișare neutilizate nu va fi rambursată.  
  
12\. Durata și încetarea contractului  
  
1\. Furnizarea de servicii în cadrul Site-ului Autovit nu este limitată în timp, dar fiecare parte (furnizorul sau Autorul Anunțului) are dreptul de a rezilia contractul, în orice moment, cu un preaviz de o lună. Pentru a rezilia contractul, Autorul Anunțului trebuie să transmită o notificare la urmatoarea adresă de email: ajutor@autovit.ro.  
2\. Furnizorul își rezervă dreptul de a rezilia contractul, cu efect imediat, dacă Autorul Anunțului nu respectă acești Termeni și Condiții.  
3\. În cazul în care contractul a fost reziliat pe baza deciziei Furnizorului, Autorul Anunțului nu se poate reînregistra pe Site-ul Autovit, fără acordul prealabil separat și expres al Furnizorului.  
  
13\. Sistemul de evaluare  
  
1\. Site-ul AutovitPlatforma Serviciul AUTOVIT oferă un sistem de evaluare, limitat la categoria Autoturisme, care permite Utilizatorului să lase o evaluare subiectivă a satisfacției sale în urma interacțiunii cu Dealerul. Ori de câte ori acest articol 13 se referă la "Utilizator", trebuie înțeles ca un Utilizator care nu este și Dealer.

2\. Sistemul de evaluare permite exprimarea de opinii legate de interacțiunea dintre Utilizator și Dealer în toate etapele de contact. Evaluarea emisă de Utilizator ar trebui să ia în considerare toate aspectele interacțiunii, inclusiv, printre altele, conformitatea obiectului Anunțului cu descrierea, forma și modul de comunicare cu Dealerul, capacitatea acestuia de a răspunde, precum și angajamentul și nivelul serviciului pentru clienți. Baza pentru emiterea unei evaluări este stabilirea unei interacțiuni între Utilizator și Dealer, nu încheierea unei tranzacții.

3\. Utilizatorul poate trimite o evaluare în termen de două săptămâni de la primirea unui e-mail de la Furnizor. Solicitarea de a trimite o evaluare este trimisă către toți Utilizatorii care au interacționat cu Dealerul prin chatul disponibil pe Site-ul Autovit (prin trimiterea unui mesaj și primirea unui răspuns). De asemenea, Utilizatorul poate scana codul QR furnizat de Dealer pentru a trimite o evaluare sau poate fi invitat de Dealer să trimită o evaluare. Evaluările nu pot fi adăugate de către Utilizatori care nu au un Cont sau de către Utilizatori al căror Cont a fost blocat sau suspendat.

4\. Evaluările nu pot:

1. constitui o evaluare a obiectului Anunțului, cu excepția cazului în care evaluarea se referă la lipsa conformității obiectului Anunțului cu descrierea pregătită de Dealer;
    
2. fi emise în schimbul oricărui beneficiu obținut de la Dealer (de exemplu, reducere, cadou gratuit, despăgubire etc.);
    
3. în loc de a interacționa cu un Dealer, să se refere la serviciile oferite de Furnizor în cadrul Site-ului Autovit.
    

5\. Pentru a adăuga o evaluare, Utilizatorul trebuie să completeze formularul furnizat de Furnizor. Utilizatorul poate evalua fiecare interacțiune cu Dealerul o singură dată în raport cu un anumit Anunț, pe o scară de la 1 la 5 stele. O stea înseamnă cea mai proastă evaluare, iar cinci stele înseamnă cea mai bună evaluare. Utilizatorul poate adăuga și un comentariu ca parte a evaluării. Utilizatorii nu pot edita evaluările emise după publicarea acestora. Sub rezerva paragrafelor 8 și 9 de mai jos, toate evaluările adăugate sunt făcute publice pe Site-ul Autovit și fiecare evaluare este marcată cu numele de Utilizator, dacă este disponibil, adresa de e-mail anonimizată a Utilizatorului, numărul de stele acordate, conținutul evaluării, titlul anunțului, datele de identificare ale Dealerului și data emiterii.

6\. Evaluarea generală a interacțiunilor întreprinse cu un anumit Dealer este vizibilă pentru Utilizatori pe pagina Anunțului și în alte locații pe Site-ul Autovit. Evaluarea generală pentru fiecare Dealer este media aritmetică a tuturor evaluărilor obținute de un anumit Dealer în ultimele 12 luni. Evaluarea generală poate fi de la 1 la 5 stele, unde 1 stea este cea mai proastă evaluare posibilă, iar 5 stele este cea mai bună evaluare posibilă. Evaluarea generală a unui Dealer va fi vizibilă pe Site-ul Autovit atunci când Dealerul primește cel puțin 5 evaluări. Evaluarea generală este rotunjită la cel mai apropiat zecimal (cu o precizie de 0,1). Un Dealer poate avea o singură evaluare generală în toate categoriile Site-ului Autovit.

7\. Evaluările sunt afișate în ordine descrescătoare în funcție de data adăugării, cu cele mai recente evaluări afișate primele. Doar evaluările adăugate în termen de 12 luni de la data curentă vor fi afișate. Acest lucru este pentru a ne asigura că evaluările reflectă cele mai recente experiențe și opinii ale Utilizatorilor, menținând relevanța și acuratețea informațiilor furnizate.

8\. Furnizorul are dreptul de a nu publica o evaluare, de a elimina o evaluare dată și de a nu include o evaluare dată în calculul evaluării generale dacă o astfel de evaluare:

1. se referă la un alt Utilizator și nu la Dealerul care este Autorul Anunțului;
    
2. este emisă de către Dealer pentru el însuși;
    
3. este emisă de angajați, rude sau afini ai Dealerului sau de alte entități asociate cu Dealerul;
    
4. a fost furnizată de către concurenții Dealerului;
    
5. este emisă ca urmare a stabilirii unui contact cu Dealerul doar pentru scopul emiterii unei evaluări (de exemplu, pentru a crește sau a diminua artificial credibilitatea Dealerului);
    
6. a fost emisă dintr-un Cont creat utilizând o adresă de e-mail temporară;
    
7. a fost emisă într-un mod care indică că emiterea a fost automatizată sau utilizând soluții care evită analiza traficului de rețea;
    
8. a fost emisă în schimbul unui beneficiu de la Dealer sau de la altă persoană;
    
9. încalcă în alt mod Termenii și Condițiile Site-ului Autovit.
    

9\. Evaluarea nu poate încălca Termenii și Condițiile Site-ului Autovit sau legislația aplicabilă și nu poate conține:

1. vulgarități, conținut obscen și pornografic sau conținut care instigă la ură, texte de ură, rasism, xenofobie și conflicte între națiuni și alte forme de discriminare;
    
2. adrese de site-uri web sau linkuri către alte site-uri web, fraze în limbi străine;
    
3. conținut publicitar, alt conținut comercial;
    
4. datele utilizatorilor sau ale altor persoane fizice, în special: nume și prenume, domiciliul / reședința, număr de telefon sau adresă de e-mail, număr de cont bancar etc.;
    
5. conținut care încalcă drepturile terților, inclusiv drepturile de proprietate intelectuală, bunele moravuri, drepturile personale, inclusiv numele bun și reputația altor utilizatori sau terți;
    
6. conținut care este rezultatul unei înțelegeri cu alți utilizatori sau terți în scopul influențării evaluării sau opiniei reciproc;
    
7. conținut care este fals, defăimător sau constituie un act de concurență neloială;
    
8. conținut care se referă la un Anunț diferit de cel evaluat sau care nu se referă la evaluarea dată.
    

10\. Deciziile de a nu publica sau de a elimina o evaluare pot fi luate automat sau manual, pe baza regulilor interne și a procedurilor de moderare în vigoare ale Furnizorului și pot fi luate înainte sau după publicarea evaluării. Aceste reguli interne și proceduri de moderare menționează metodele și mijloacele de identificare a conținutului sau acțiunilor utilizatorilor, care vor fi verificate pentru încălcări ale Termenilor și Condițiilor Site-ului Autovit sau ale legilor aplicabile. Scopul principal al acestor reguli și proceduri de moderare este de a asigura siguranța utilizatorilor și combaterea abuzurilor.

11\. Furnizorul moderează evaluările utilizând instrumente interne sau în colaborare cu terți pentru a preveni evaluările nedrepte și pentru a menține integritatea Site-ului Autovit.

12\. Utilizatorii pot solicita Furnizorului să elimine o evaluare emisă sau primită în cazurile specificate la punctele 8 și 9 de mai sus, utilizând formularul de contact disponibil pe Site-ul Autovit. Când solicită eliminarea unei evaluări, Utilizatorul ar trebui să specifice cu precizie la ce evaluare se referă cererea, de exemplu, indicând data emiterii evaluării, numele și prenumele Utilizatorului care a emis-o, titlul anunțului la care se referă evaluarea și datele de identificare ale Dealerului. Utilizatorul ar trebui să indice motivul pentru care solicită eliminarea evaluării dintre motivele specificate la punctele 8 și 9 de mai sus și să furnizeze dovezi adecvate pentru a-și justifica cererea.

13\. Furnizorul permite raportarea electronică a cazurilor de suspiciune că evaluările afișate pe Site-ul Autovit constituie conținut interzis, conținut contrar Termenilor și Condițiilor Site-ului Autovit sau legislației aplicabile.

14\. Un Utilizator care nu este de acord cu decizia Furnizorului privind eliminarea unei evaluări, neeliminarea unei evaluări sau nepublicarea unei evaluări are dreptul de a face o plângere utilizând formularul de contact disponibil pe Site-ul Autovit, în conformitate cu cele stabilite în punctul 10 al Termenilor și Condițiilor Site-ului Autovit referitoare la procedura de plângere.

15\. Dealerul are dreptul de a răspunde în scris la fiecare evaluare primită de la Utilizator în termen de 6 luni de la publicarea acesteia. Răspunsul Dealerului va fi vizibil pe Site-ul  Autovit în termen de două zile lucrătoare de la publicarea acestuia. Prevederile paragrafelor 8-12 de mai sus se aplică în mod corespunzător răspunsului Dealerului.

16\. Evaluările menționate în acest articol 13 sunt în faza de testare a produsului, ceea ce înseamnă că, pe durata acestei funcționalități, nu este disponibilă:

1. partajarea publică a evaluărilor pe Site-ul Autovit și partajarea publică a evaluării generale menționate în secțiunile 5-7 de mai sus – evaluările emise și evaluarea generală vor fi vizibile doar pentru Dealer în timpul testelor (cu toate acestea, după finalizarea testelor, evaluările individuale și evaluarea generală vor fi publicate pe Site-ul Autovit conform principiilor descrise în acest punct);
    
2. punerea la dispoziție publică a răspunsului Dealerului pentru evaluările primite la care se face referire la paragraful 15 de mai sus.
    

1\. În cadrul Site-ului Autovit este pus la dispoziție un sistem de evaluare, care permite oricărui Utilizator să ofere o evaluare subiectivă a vânzătorilor profesioniști, în ceea ce privește experiența tranzacțională avută cu aceștia. Experiența tranzacțională se evaluează, în mod special, în funcție de: existența unei descrieri clare și serioase a Produsului, modul de comunicare între Utilizatori, receptivitate, implicare, timpul de achiziționare si/sau livrare a Produsului, punctualitate, etc. Baza evaluării este contactul dintre Utilizatori, sub orice formă, și nu exclusiv încheierea efectivă unei tranzacții.  
2\. Evaluarea se realizează în modalitatea, forma, termenul și categoriile indicate în cadrul Paginii Autovit. Evaluarea va fi prezentată pe website, în anunțurile Utilizatorului, pe o scară de la 1 la 5 (până la o zecimală), pe baza mediei tuturor evaluărilor, emise de Utilizatori. În plus, evaluarea va arăta procentul de utilizatori care recomandă un anumit vânzător profesionist.  
3\. Dacă unui utilizator i se acordă mai mult de o evaluare, fiecare astfel de evaluare va fi inclusă în evaluarea medie calculată pentru respectivul Utilizator.  
4\. Furnizorul își rezervă dreptul de a exclude anumite evaluări din ratingul mediu al Utilizatorului sau de a șterge o evaluare, total sau partial, atunci când o evaluare sau un feedback:

1. se referă la un alt Utilizator decât cel evaluat;
    
2. este dată de Utilizator pentru sine;
    
3. este dată de către angajații, rudele și afinii Utilizatorului;
    
4. este dată ca urmare a contactului cu Utilizatorul numai în scopul de a oferi o evaluare și o opinie (de exemplu, pentru a supraestima sau subestima în mod artificial credibilitatea Utilizatorului);
    
5. este legată de un Cont creat pe baza unei adrese de e-mail temporare;
    
6. a fost expusă într-un mod care indică automatizarea expunerii sau prin intermediul unor soluții care să evite analiza traficului în rețea;
    

încalcă Termenii și Condițiile în alt mod.

14\. Modificări ale Termenilor și Condițiilor  
  
1\. Furnizorul poate modifica oricând Termenii și Condițiile, inclusiv Lista de prețuri. Orice modificări, precum și datele de intrare în vigoare ale acestora vor fi comunicate de către Furnizor Utilizatorului pe pagina web a Site-ului Autovit și prin mijloace electronice de comunicare. În scopuri de comunicare, Furnizorul poate contacta Utilizatorul și prin chat-ul disponibil pe [www.autovit.ro](https://www.autovit.ro/).  
2\. Orice modificări ale acestor Termeni și Condiții și ale Listei de prețuri vor intra în vigoare nu mai devreme de 15 zile de la data postării lor pe Site-ul Autovit. Orice servicii activate de către Autorii de Anunțuri înainte de data intrării în vigoare a modificărilor acestor Termeni și Condiții vor fi furnizate în termenii anteriori.  
  
3\. Dacă modificările Termenilor și Condițiilor nu sunt acceptate de către Autorul Anunțului, acesta din urmă trebuie să trimită o notificare relevantă la adresa de e-mail ajutor@autovit.ro. Aceasta va avea ca rezultat rezilierea imediată a contractului cu Furnizorul, cu condiția ca rezilierea să intervină la sfârșitul perioadei de afișare a tuturor Anunțurilor existente.  
                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
15\. Prevederi finale  
  
1\. În cadrul serviciilor disponibile pe Site-ul Autovit, Autorul Anunțului solicită Furnizorului să publice (și să modifice) Anunțuri pe orice alte site-uri web deținute de Furnizor sau de afiliații acestuia, precum și în alte medii, precum presa, TV, Internet și bannere sau panouri publicitare. În același timp, Furnizorul poate pune și un filigran cu numele Site-ului Autovit pe fotografiile Articolului, pentru a proteja fotografia împotriva utilizării neautorizate de către terți.  
  
2\. Termenii și Condițiile sunt disponibile pe Site-ul Autovit. De asemenea, Furnizorul le transmite, prin e-mail, la adresele Autorilor de Anunțuri, în cazurile cerute de reglementările legale în vigoare.  
  
3\. Cu scopul de a dezvolta Serviciul, Furnizorul va putea introduce noi servicii și  
funcționalități, care pot fi precedate de teste de produs, fără a aduce atingere drepturilor  
dobândite de către Utilizatori.  
  
  
4\. Contractul încheiat între Autorul Anunțului și Furnizor, cu privire la serviciile furnizate pe Site-ul Autovit, în condițiile definite aici, este guvernat de legea română.  
  
5\. Orice dispută legată de serviciile furnizate în cadrul Site-ului Autovit va fi soluționată de instanțele competente din România.  
  
6\. Consumatorul are posibilitatea de a recurge la un mecanism de solutionare alternativă a litigiului („SAL”) prin intermediul Entității SAL din cadrul Autorității Naționale pentru Protecția Consumatorului („ANPC”), procedura fiind disponibilă la adresa: [https://anpc.ro/galerie/file/diversefg/CerereSAL2.pdf](https://anpc.ro/galerie/file/diversefg/CerereSAL2.pdf). În cazul în care Direcția SAL nu poate analiza reclamația, va transmite ambelor părți o explicație privind motivele neanalizării litigiului în termen de 21 de zile calendaristice de la primirea dosarului de reclamație.  
Direcția SAL dispune de un punct de contact la care consumatorii pot apela pentru a primi informații cu privire la procedura alternativă de soluționare a litigiilor si poate fi contactată la următoarele adrese: Blvd. Aviatorilor nr.72, Bucuresti, 011865, România, adresa de email dsal@anpc.ro sau la numărul de telefon 0213121275.  
  
  Anexe

* [Anexa Nr. 1 - Lista de Prețuri](https://ajutor.autovit.ro/autovithelp/s/article/Anexa-nr-1-Lista)
    
* [Anexa Nr. 2 - Servicii de promovare a Anunțurilor](https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr2-servicii-de-promovare-01032022-V12)
    
* [Anexa Nr. 3 - Serviciul de Reactualizări Automate și Pachete](https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr3-serviciul-de-reactualizari-automate-si-pachete-V2)
    
* [Anexa Nr. 4 - Reguli pentru postare](https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr4-actiuni-interzise-sau-conditionale-reguli-de-postare-V49)
    
* [Anexa Nr. 5 - Instrucțiuni privind retragerea din contract și modelul declarației privind retragerea](https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr5-instruc%C8%9Biuni-privind-retragerea-din-contract-%C8%99i-formular-de-declara%C8%9Bie-de-retragere-V12)
    
* [Anexa Nr. 6 - Facturi Electronice](https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr6-facturi-electronice-persoane-fizice-V19)
    
* [Anexa Nr. 7 - Fonduri pentru Autovit](https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr-5-fonduri-autovit-V1)
    

Fişiere
=======

0 elemente • Sortate după Ultima modificare

Controale în vizualizarea listă

Afișare filtre rapide

Se încarcă…

Se încarcă…

Așteptați sau selectați altă vizualizare listă.

| Număr element | [SortTitlu](javascript:void(0);)<br><br>Afișarea Titlu acțiuni în coloană | [SortProprietar](javascript:void(0);)<br><br>Afișarea Proprietar acțiuni în coloană | [SortUltima modificare](javascript:void(0);)Sortare în ordine descrescătoare<br><br>Afișarea Ultima modificare acțiuni în coloană | [SortDimensiune](javascript:void(0);)<br><br>Afișarea Dimensiune acțiuni în coloană | Acțiune |
| --- | --- | --- | --- | --- | --- |

Nu există elemente de afișat.

RevocareSalvare

* [](https://www.facebook.com/share.php?title=Regulament%20persoane%20fizice&u=https://ajutor.autovit.ro/autovithelp/s/article/regulament-persoane-fizice-V45)
* [](https://twitter.com/share?text=Regulament%20persoane%20fizice&url=https://ajutor.autovit.ro/autovithelp/s/article/regulament-persoane-fizice-V45)
* [](https://www.linkedin.com/shareArticle?mini=true&title=Regulament%20persoane%20fizice&url=https://ajutor.autovit.ro/autovithelp/s/article/regulament-persoane-fizice-V45)