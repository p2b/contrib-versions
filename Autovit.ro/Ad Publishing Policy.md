Anexa Nr.2 - Servicii de Promovare
==================================

#### **Anexa în vigoare la data de 03.01.2025**

#### **Versiunea anterioara poata fi accesata [aici](https://ajutor.autovit.ro/hc/ro/articles/360010628439-Regulament-persoane-fizice-anterior).**

1. #### Site-ul Autovit.ro permite vanzatorilor profesionisti sa foloseasca, contra cost, urmatoarele servicii de promovare:
    
    1. #### Promovare la inceput de lista
        
    2. #### Anunturile zilei
        
    3. #### Oferta zilei
        
    4. #### Adauga automat si pe OLX
        
    5. #### Promovarea anuntului pe OLX
        
    6. #### Reactualizarea automata
        

2. #### O singura activare a fiecarui serviciu descris la punctul 1, mentionat mai sus, se aplica unui singur anunt care este ales de catre vanzatorul profesionist.
    

3. #### Promovarea la inceput de lista presupune afisarea anuntului in pagina cu rezultatele cautarilor inaintea anunturilor care nu folosesc aceasta promovare. Serviciul este vizibil doar pentru utilizatorii care viziteaza pagina cu rezultatele cautarilor si care pastreaza ordonarea predefinita “Recomandate”. Un utilizator care viziteaza pagina cu rezultatele cautarilor poate, in oricare moment, sa modifice, de unul singur, criteriile de ordonare. Anunturile cu Promovare la inceput de lista sunt marcate cu o banda albastra, pe care este mentionat cuvantul “Promovat”.
    

4. #### Anuntul zilei presupune afisarea anuntului in pagina principala Autovit.ro, impreuna cu alte anunturi cu acelasi tip de promovare. Anunturile zilei sunt afisate pe prima pagina, in mod aleatoriu. Un anunt cu aceasta promovare poate fi afisat de mai multe ori, iar anunturile care au fost afisate de mai putine ori au prioritate.
    

5. #### Oferta zilei presupune afisarea anuntului in pagina principala Autovit.ro, in chenarul dedicat acestui tip de promovare, care este marcat cu o banda verde, pe care scrie “Oferta zilei”. Ofertele zilei sunt afisate pe prima pagina, in mod aleatoriu. Un anunt cu aceasta promovare poate fi afisat de mai multe ori, iar anunturile care au fost afisate de mai putine ori au prioritate.
    

6. #### Adauga automat si pe OLX presupune publicarea anuntului pe site-ul [olx.ro](http://www.olx.ro/).
    

7. #### Promovarea anuntului pe OLX le permite vanzatorilor sa promoveze anunturile pe site-ul olx.ro. Tipurile de promovari disponibile si lista de preturi sunt mentionate in listele de preturi de mai jos.
    

8. #### Reactualizarea automata presupune afisarea anuntului in lista cu rezultatele cautarilor ca si atunci cand anuntul este publicat pe Autovit.ro, urmand procedura standard; totusi, un utilizator care viziteaza pagina cu rezultatele cautarilor poate modifica, de unul singur, criteriile de ordonare.
    

9. #### Serviciile de promovare sunt disponibile si sub forma de Pachete de promovari. Activarea unui pachet de Servicii de promovare are loc la prima utilizare a Serviciilor de promovare incluse in respectivul Pachet. Regulile referitoare la Serviciile de promovare acoperite de un Pachet rămân neschimbate.
    

10. #### Serviciile de Promovare a Anunturilor si Pachetele aferente acestora sunt implementate in legatura cu un singur Anunt, ceea ce inseamna ca nu este posibila utilizarea unui pachet de Servicii de Promovare a Anunturilor pentru Anunturi diferite.
    

11. #### Un anumit Serviciu de Promovare a Anunturilor de pe site-ul AUTOVIT si de pe website-urile afiliate, incepe sa opereze in termen de maxim 2 ore de la activarea serviciului selectat.
    

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarele categorii: Autoturisme si Autoutilitare (preturile includ TVA):

|     |     |     |
| --- | --- | --- |
| Denumire Promovare | Numar zile | Pret |
| Promovare la inceput de lista | 7   | 14.99 € |
| Promovare la inceput de lista | 15  | 19.99 € |
| Promovare la inceput de lista | 30  | 34.99 € |
| 1 Reactualizare automata | N/A | 6.99 € |
| 3 Reactualizari automate | N/A | 14.99 € |
| 7 Reactualizari automate | N/A | 19.99 € |
| 15 Reactualizari automate | N/A | 29.99 € |
| 30 Reactualizari automate | N/A | 44.99 € |
| Anunturile zilei | 3   | 24.99 € |
| Anunturile zilei | 7   | 34.99 € |
| Anunturile zilei | 15  | 54.99 € |
| Oferta zilei | 1   | 69.99 € |
| Oferta zilei | 3   | 149.99 € |
| Oferta zilei | 5   | 224.99 € |
| Promovare in lista de anunturi OLX | 7   | 12.99 € |
| Promovare in lista de anunturi OLX | 30  | 21.99 € |

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarea categorie: Agro (preturile includ TVA):

|     |     |     |
| --- | --- | --- |
| Denumire Promovare | Numar zile | Pret |
| Promovare la inceput de lista | 7   | 9.99 € |
| Promovare la inceput de lista | 15  | 15.99 € |
| Promovare la inceput de lista | 30  | 27.99 € |
| 1 Reactualizare automata | N/A | 3.99 € |
| 3 Reactualizari automate | N/A | 7.99 € |
| 7 Reactualizari automate | N/A | 12.99 € |
| 15 Reactualizari automate | N/A | 19.99 € |
| 30 Reactualizari automate | N/A | 34.99 € |
| Anunturile zilei | 3   | 9.99 € |
| Anunturile zilei | 7   | 15.99 € |
| Anunturile zilei | 15  | 29.99 € |
| Oferta zilei | 1   | 39.99 € |
| Oferta zilei | 3   | 69.99 € |
| Oferta zilei | 5   | 99.99 € |
| Promovare in lista de anunturi OLX | 7   | 12.99 € |
| Promovare in lista de anunturi OLX | 30  | 21.99 € |

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarele categorii: Camioane, Remorci si Constructii (preturile includ TVA):

|     |     |     |
| --- | --- | --- |
| Denumire Promovare | Numar zile | Pret |
| Promovare la inceput de lista | 7   | 12.99 € |
| Promovare la inceput de lista | 15  | 19.99 € |
| Promovare la inceput de lista | 30  | 29.99 € |
| 1 Reactualizare automata | N/A | 3.99 € |
| 3 Reactualizari automate | N/A | 7.99 € |
| 7 Reactualizari automate | N/A | 12.99 € |
| 15 Reactualizari automate | N/A | 19.99 € |
| 30 Reactualizari automate | N/A | 34.99 € |
| Anunturile zilei | 3   | 9.99 € |
| Anunturile zilei | 7   | 15.99 € |
| Anunturile zilei | 15  | 29.99 € |
| Oferta zilei | 1   | 39.99 € |
| Oferta zilei | 3   | 69.99 € |
| Oferta zilei | 5   | 99.99 € |
| Promovare in lista de anunturi OLX | 7   | 12.99 € |
| Promovare in lista de anunturi OLX | 30  | 21.99 € |

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarea categorie: Piese (preturile includ TVA):

|     |     |     |     |
| --- | --- | --- | --- |
| Denumire Promovare | Numar zile | Pret pentru pachetele Bronze, Silver, Gold | Pret pentru pachet Platinum |
| Promovare la inceput de lista | 7   | 2.49 € | 1.74 € |
| Promovare la inceput de lista | 15  | 3.49 € | 2.44 € |
| Promovare la inceput de lista | 30  | 4.99 € | 3.49 € |
| 1 Reactualizare automata | N/A | 0.69 € | 0.48 € |
| 3 Reactualizari automate | N/A | 1.29 € | 0.90 € |
| 7 Reactualizari automate | N/A | 2.49 € | 1.74 € |
| 15 Reactualizari automate | N/A | 3.49 € | 2.44 € |
| 30 Reactualizari automate | N/A | 6.99 € | 4.89 € |
| Anunturile zilei | 3   | 3.99 € | 2.79 € |

|     |     |     |     |
| --- | --- | --- | --- |
| Anunturile zilei | 7   | 5.99 € | 4.19 € |
| Anunturile zilei | 15  | 11.99 € | 8.39 € |
| Oferta zilei | 1   | 13.99 € | 9.79 € |
| Oferta zilei | 3   | 21.99 € | 15.39 € |
| Oferta zilei | 5   | 29.99 € | 20.99 € |
| Promovare in lista de anunturi OLX | 7   | 2.49 € | 1.74 € |
| Promovare in lista de anunturi OLX | 30  | 3.99 € | 2.79 € |

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarea categorie: Motociclete (preturile includ TVA):

|     |     |     |
| --- | --- | --- |
| Denumire Promovare | Numar zile | Pret |
| Promovare la inceput de lista | 7   | 6.99 € |
| Promovare la inceput de lista | 15  | 9.99 € |
| Promovare la inceput de lista | 30  | 14.99 € |
| 1 Reactualizare automata | N/A | 2.99 € |
| 3 Reactualizari automate | N/A | 6.99 € |
| 7 Reactualizari automate | N/A | 9.99 € |
| 15 Reactualizari automate | N/A | 13.99 € |
| 30 Reactualizari automate | N/A | 19.99 € |
| Anunturile zilei | 3   | 9.99 € |
| Anunturile zilei | 7   | 15.99 € |
| Anunturile zilei | 15  | 29.99 € |
| Oferta zilei | 1   | 39.99 € |
| Oferta zilei | 3   | 69.99 € |
| Oferta zilei | 5   | 99.99 € |
| Promovare in lista de anunturi OLX | 7   | 5.99 € |
| Promovare in lista de anunturi OLX | 30  | 11.99 € |

#### Promovarile sunt disponibile si sub forma de pachete pentru urmatoarea categorie: Autoturisme (preturile includ TVA):

|     |     |     |
| --- | --- | --- |
| Denumire pachet | Continut pachet | Pret |
| Standard | Promovare la inceput de lista in Autovit.ro timp de 30 de zile<br><br>Promovare in lista de anunturi OLX timp de 30 de zile | 44.99 € |
| Advanced | 30 Reactualizari automate pe Autovit.ro si OLX<br><br>Promovare la inceput de lista in Autovit.ro timp de 30 de zile | 54.99 € |
| Ultra | 30 Reactualizari automate pe Autovit.ro si OLX<br><br>Promovare la inceput de lista in Autovit.ro timp de 30 de zile<br><br>Pachetul OLX Pro | 79.99 € |

#### Lista de preturi pentru pachetele de promovare OLX disponibile pe Autovit.ro pentru urmatoarele categorii: Autoturisme, Autoutilitare, Camioane, Remorci, Constructii, Agro si Moto (preturile includ TVA)

|     |     |     |     |
| --- | --- | --- | --- |
| Denumire pachet OLX | Promovarile incluse in pachet | Interval pret vehicul | Cost pachet |
| OLX Start | Promovare in lista de anunturi (3 zile) | Sub 2500 Eur | 6.99 Eur |
| 2500 Eur  – 4999 Eur | 8.99 Eur |
| 5000 Eur – 9999 Eur | 10.99 Eur |
| 10000 Eur – 14999 Eur | 11.99 Eur |
| 15000 Eur – 24999 Eur | 12.99 Eur |
| 25000 Eur – 34999 Eur | 14.99 Eur |
| Peste 35000 Eur | 15.99 Eur |

#### Servicii adiționale:

1. #### Autorul Anunțului, utilizând Serviciul de Număr de Telefon Virtual, consimte la următoarele:
    

#### a) Furnizorul să înlocuiască numărul de telefon al Autorului Anunțului din conținutul Anunțului cu numărul virtual furnizat de Furnizor;

#### b) prezentarea unui mesaj automat Autorului Anunțului, înainte de a primi un apel, informându-l că apelul a fost efectuat prin Furnizor;

#### c) colectarea și procesarea datelor referitoare la apelurile telefonice făcute de utilizatori, exclusiv în scopul furnizării Serviciului de Număr de Telefon Virtual.

2. #### Furnizorul asigură că numărul virtual la care se face referire în Clauza 1 de mai sus provine din lista națională de numere de telefon și că utilizarea Serviciului de Număr de Telefon Virtual nu va genera costuri suplimentare pentru utilizatori, pentru Autorul Anunțului sau pentru furnizorii de servicii de telecomunicații ai părților care fac apeluri către Autorul Anunțului. Serviciul este furnizat Autorului Anunțului la costul specificat în Anexa 1 a Termenilor și Condițiilor. De asemenea, Furnizorul garantează că nu va folosi numerele de telefon ale Autorului Anunțului sau ale utilizatorilor în scopuri care nu au legătură cu Serviciul de Număr de Telefon Virtual sau cu alte servicii conexe furnizate de Furnizor Autorului Anunțului prin intermediul Site-ul Autovit sau al oricărei alte platforme unde Furnizorul își oferă serviciile, cu excepția cazurilor permise de legea aplicabilă sau cu consimțământul expres al Autorului Anunțului.
    
3. #### Furnizorul permite Autorului Anunțului să utilizeze Serviciul de Număr de Telefon Virtual pentru mai multe numere de telefon. Cu toate acestea, Furnizorul își rezervă dreptul de a impune restricții asupra numărului de numere de telefon acoperite de Serviciul de Număr de Telefon Virtual.
    

#### 1\. Site-ul Autovit.ro permite Utilizatorilor sa foloseasca, contra cost, urmatoarele servicii de promovare:

* #### Promovare la inceput de lista 
    
* #### Anunturile zilei
    
* #### Oferta zilei
    
* #### Adauga automat si pe OLX
    
* #### Promovarea anuntului pe OLX
    
* #### Reactualizarea automata
    

#### 2\. O singura activare a fiecarui serviciu descris la punctul 1, mentionat mai sus, se aplica unui singur anunt care este ales de catre Utilizator.

#### 3\. Promovarea la inceput de lista presupune afisarea anuntului in pagina cu rezultatele cautarilor inaintea anunturilor care nu folosesc aceasta promovare. Serviciul este vizibil doar pentru utilizatorii care viziteaza pagina cu rezultatele cautarilor si care pastreaza ordonarea predefinita “Recomandate”. Un utilizator care viziteaza pagina cu rezultatele cautarilor poate, in oricare moment, sa modifice, de unul singur, criteriile de ordonare. Anunturile cu Promovare la inceput de lista sunt marcate cu o banda albastra, pe care este mentionat cuvantul “Promovat”.

#### 4\. Anuntul zilei presupune afisarea anuntului in pagina principala Autovit.ro, impreuna cu alte anunturi cu acelasi tip de promovare. Anunturile zilei sunt afisate pe prima pagina, in mod aleatoriu. Un anunt cu aceasta promovare poate fi afisat de mai multe ori, iar anunturile care au fost afisate de mai putine ori au prioritate.

#### 5\. Oferta zilei presupune afisarea anuntului in pagina principala Autovit.ro, in chenarul dedicat acestui tip de promovare, care este marcat cu o banda verde, pe care scrie “Oferta zilei”. Ofertele zilei sunt afisate pe prima pagina, in mod aleatoriu. Un anunt cu aceasta promovare poate fi afisat de mai multe ori, iar anunturile care au fost afisate de mai putine ori au prioritate.

#### 6\. Adauga automat si pe OLX presupune publicarea anuntului pe site-ul [www.olx.ro](http://www.olx.ro/).

#### 7.Promovarea anuntului pe OLX le permite vanzatorilor sa promoveze anunturile pe site-ul olx.ro. Tipurile de promovari disponibile si lista de preturi sunt mentionate in listele de preturi de mai jos.

#### 8\. Reactualizarea automata presupune afisarea anuntului in lista cu rezultatele cautarilor ca si atunci cand anuntul este publicat pe Autovit.ro, urmand procedura standard; totusi, un utilizator care viziteaza pagina cu rezultatele cautarilor poate modifica, de unul singur, criteriile de ordonare.

#### 9\. Serviciile de promovare sunt disponibile si sub forma de Pachete de promovari. Activarea unui pachet de Servicii de promovare are loc la prima utilizare a Serviciilor de promovare incluse in respectivul Pachet. Regulile referitoare la Serviciile de promovare acoperite de un Pachet rămân neschimbate.

#### 10\. Serviciile de Promovare a Anunturilor si Pachetele aferente acestora sunt implementate in legatura cu un singur Anunt, ceea ce inseamna ca nu este posibila utilizarea unui pachet de Servicii de Promovare a Anunturilor pentru Anunturi diferite.

#### 11\. Un anumit Serviciu de Promovare a Anunturilor de pe site-ul Autovit incepe sa opereze in termen de maxim 2 ore de la activarea serviciului selectat.

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarele categorii: Autoturisme si Autoutilitare

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| #### Denumire promovare | #### Pret vehicul | #### Durata | #### Plata cu Cardul | #### Plata prin SMS |
| #### Promovare la inceput de lista | #### <2500 € | #### 7 zile | #### 7.99 Eur | #### 9.52 Eur |
| #### Promovare la inceput de lista | #### 2500 € - 4999 € | #### 7 zile | #### 8.99 Eur | #### 10.71 Eur |
| #### Promovare la inceput de lista | #### 5000 € - 9999 € | #### 7 zile | #### 9.99 Eur | #### 11.90 Eur |
| #### Promovare la inceput de lista | #### 10000 € - 14999 € | #### 7 zile | #### 9.99 Eur | #### 11.90 Eur |
| #### Promovare la inceput de lista | #### 15000 € -24999 € | #### 7 zile | #### 10.99 Eur | #### 13.09 Eur |
| #### Promovare la inceput de lista | #### \>25000 € | #### 7 zile | #### 11.99 Eur | #### 13.09 Eur |
| #### Promovare la inceput de lista | #### <2500 € | #### 15 zile | #### 11.99 Eur | #### 13.09 Eur |
| #### Promovare la inceput de lista | #### 2500 € - 4999 € | #### 15 zile | #### 12.99 Eur | #### 14.28 Eur |
| #### Promovare la inceput de lista | #### 5000 € - 9999 € | #### 15 zile | #### 13.99 Eur | #### 15.47 Eur |
| #### Promovare la inceput de lista | #### 10000 € - 14999 € | #### 15 zile | #### 14.99 Eur | #### 16.66 Eur |
| #### Promovare la inceput de lista | #### 15000 € -24999 € | #### 15 zile | #### 15.99 Eur | #### 17.85 Eur |
| #### Promovare la inceput de lista | #### \>25000 € | #### 15 zile | #### 17.99 Eur | #### 19.04 Eur |
| #### Reactualizare automata | #### Orice pret | #### 3 zile | #### 9.99 Eur | #### 11.90 Eur |
| #### Reactualizare automata | #### Orice pret | #### 7 zile | #### 14.99 Eur | #### 16.66 Eur |
| #### Reactualizare automata | #### Orice pret | #### 15 zile | #### 22.99 Eur | #### 23.80 Eur |
| #### Anuntul zilei | #### Orice pret | #### 3 zile | #### 15.99 Eur | #### 17.85 Eur |
| #### Anuntul zilei | #### Orice pret | #### 7 zile | #### 24.99 Eur | #### Indisponibil |
| #### Anuntul zilei | #### Orice pret | #### 15 zile | #### 34.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### Orice pret | #### 1 zi | #### 49.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### Orice pret | #### 3 zile | #### 99.99 Eur | #### Indisponibil |
| #### Publicare automata pe OLX.ro | #### Orice pret | #### 30 zile | #### 1.00 Eur | #### 2.38 Eur |

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarele categorii: Camioane, Remorci si Constructii

|     |     |     |     |
| --- | --- | --- | --- |
| #### Denumire promovare | #### Durata | #### Plata cu Cardul | #### Plata prin SMS |
| #### Promovare la inceput de lista | #### 7 zile | #### 9.99 Eur | #### 11.90 Eur |
| #### Promovare la inceput de lista | #### 15 zile | #### 14.99 Eur | #### 16.66 Eur |
| #### Reactualizare automata | #### 3 zile | #### 8.99 Eur | #### 10.71 Eur |
| #### Reactualizare automata | #### 7 zile | #### 12.99 Eur | #### 15.47 Eur |
| #### Reactualizare automata | #### 15 zile | #### 19.99 Eur | #### 21.42 Eur |
| #### Anuntul zilei | #### 3 zile | #### 12.99 Eur | #### 15.47 Eur |
| #### Anuntul zilei | #### 7 zile | #### 19.99 Eur | #### 21.42 Eur |
| #### Anuntul zilei | #### 15 zile | #### 29.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### 1 zi | #### 59.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### 3 zile | #### 99.99 Eur | #### Indisponibil |
| #### Publicare automata pe OLX.ro | #### 30 zile | #### 1.00 Eur | #### 2.38 Eur |

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarea categorie: Piese

|     |     |     |     |
| --- | --- | --- | --- |
| #### Denumire promovare | #### Durata | #### Plata cu Cardul | #### Plata prin SMS |
| #### Promovare la inceput de lista | #### 7 zile | #### 1.99 Eur | #### 4.76 Eur |
| #### Promovare la inceput de lista | #### 15 zile | #### 2.99 Eur | #### 5.95 Eur |
| #### Reactualizare automata | #### 1 zi | #### 0.49 Eur | #### 2.38 Eur |
| #### Reactualizare automata | #### 3 zile | #### 0.99 Eur | #### 2.38 Eur |
| #### Reactualizare automata | #### 7 zile | #### 1.99 Eur | #### 4.76 Eur |
| #### Reactualizare automata | #### 15 zile | #### 2.99 Eur | #### 5.95 Eur |
| #### Anuntul zilei | #### 3 zile | #### 12.99 Eur | #### 15.47 Eur |
| #### Anuntul zilei | #### 7 zile | #### 19.99 Eur | #### 21.42 Eur |
| #### Anuntul zilei | #### 15 zile | #### 29.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### 1 zi | #### 59.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### 3 zile | #### 99.99 Eur | #### Indisponibil |
| #### Publicare automata pe OLX.ro | #### 30 zile | #### 1.00 Eur | #### 2.38 Eur |

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarea categorie: Agro

|     |     |     |     |
| --- | --- | --- | --- |
| #### Denumire promovare | #### Durata | #### Plata cu Cardul | #### Plata prin SMS |
| #### Promovare la inceput de lista | #### 7 zile | #### 8.99 Eur | #### 10.71 Eur |
| #### Promovare la inceput de lista | #### 15 zile | #### 12.99 Eur | #### 15.47 Eur |
| #### Reactualizare automata | #### 3 zile | #### 8.99 Eur | #### 10.71 Eur |
| #### Reactualizare automata | #### 7 zile | #### 12.99 Eur | #### 15.47 Eur |
| #### Reactualizare automata | #### 15 zile | #### 18.99 Eur | #### 21.42 Eur |
| #### Anuntul zilei | #### 3 zile | #### 12.99 Eur | #### 15.47 Eur |
| #### Anuntul zilei | #### 7 zile | #### 19.99 Eur | #### 21.42 Eur |
| #### Anuntul zilei | #### 15 zile | #### 29.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### 1 zi | #### 59.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### 3 zile | #### 99.99 Eur | #### Indisponibil |
| #### Publicare automata pe OLX.ro | #### 30 zile | #### 1.00 Eur | #### 2.38 Eur |

#### Lista de preturi pentru promovarile de pe Autovit.ro pentru urmatoarea categorie: Motociclete

|     |     |     |     |
| --- | --- | --- | --- |
| #### Denumire promovare | #### Durata | #### Plata cu Cardul | #### Plata prin SMS |
| #### Promovare la inceput de lista | #### 7 zile | #### 6.99 Eur | #### 9.52 Eur |
| #### Promovare la inceput de lista | #### 15 zile | #### 9.99 Eur | #### 11.90 Eur |
| #### Reactualizare automata | #### 3 zile | #### 6.99 Eur | #### 9.52 Eur |
| #### Reactualizare automata | #### 7 zile | #### 9.99 Eur | #### 11.90 Eur |
| #### Reactualizare automata | #### 15 zile | #### 13.99 Eur | #### 15.47 Eur |
| #### Anuntul zilei | #### 3 zile | #### 12.99 Eur | #### 15.47 Eur |
| #### Anuntul zilei | #### 7 zile | #### 19.99 Eur | #### 21.42 Eur |
| #### Anuntul zilei | #### 15 zile | #### 29.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### 1 zi | #### 59.99 Eur | #### Indisponibil |
| #### Oferta zilei | #### 3 zile | #### 99.99 Eur | #### Indisponibil |
| #### Publicare automata pe OLX.ro | #### 30 zile | #### 1.00 Eur | #### 2.38 Eur |

#### Lista de preturi pentru pachetele de promovare OLX disponibile pe Autovit.ro pentru urmatoarele categorii: Autoturisme

|     |     |     |     |     |
| --- | --- | --- | --- | --- |
| #### Denumire pachet OLX | #### Pret vehicul | #### Promovarile incluse in pachet | #### Plata cu Cardul | #### Plata prin SMS |
| #### OLX Start | #### <2500 € | #### Promovare in lista de anunturi (3 zile) | #### 5.99 Eur | #### 7.14 Eur |
| #### 2500 € - 4999 € | #### 6.99 Eur | #### 8.33 Eur |
| #### 5000 € - 9999 € | #### 8.99 Eur | #### 10.71 Eur |
| #### 10000 € - 14999 € | #### 8.99 Eur | #### 10.71 Eur |
| #### 15000 € -24999 € | #### 9.99 Eur | #### 11.90 Eur |
| #### \>25000 € | #### 10.99 Eur | #### 13.09 Eur |
| #### OLX Standard | #### <2500 € | #### Promovare in lista de anunturi (7 zile)<br><br>#### 3 x reactualizari automate | #### 12.99 Eur | #### 14.28 Eur |
| #### 2500 € - 4999 € | #### 13.99 Eur | #### 15.47 Eur |
| #### 5000 € - 9999 € | #### 16.99 Eur | #### 17.85 Eur |
| #### 10000 € - 14999 € | #### 16.99 Eur | #### 17.85 Eur |
| #### 15000 € -24999 € | #### 17.99 Eur | #### 19.04 Eur |
| #### \>25000 € | #### 19.99 Eur | #### 21.42 Eur |
| #### OLX Premium | #### <2500 € | #### Promovare in lista de anunturi (30 zile)<br><br>#### 9 x reactualizari automate<br><br>#### Promovare pe prima pagina (7 zile) | #### 17.99 Eur | #### 19.04 Eur |
| #### 2500 € - 4999 € | #### 19.99 Eur | #### 21.42 Eur |
| #### 5000 € - 9999 € | #### 22.99 Eur | #### 23.80 Eur |
| #### 10000 € - 14999 € | #### 23.99 Eur | ####  Indisponibil |
| #### 15000 € -24999 € | #### 24.99 Eur | ####  Indisponibil |
| #### \>25000 € | #### 26.99 Eur | ####  Indisponibil |

#### Lista de preturi pentru pachetele de promovare OLX disponibile pe Autovit.ro pentru urmatoarele categorii: Camioane, Remorci si Constructii

|     |     |     |     |
| --- | --- | --- | --- |
| #### Denumire pachet OLX | #### Promovarile incluse in pachet | #### Plata cu Cardul | #### Plata prin SMS |
| #### OLX Start | #### Promovare in lista de anunturi (3 zile) | #### 8.99 Eur | #### 10.71 Eur |
| #### OLX Standard | #### Promovare in lista de anunturi (7 zile) | #### 15.99 Eur | #### 17.85 Eur |
| #### 3 x reactualizari automate |
| #### OLX Premium | #### Promovare in lista de anunturi (30 zile) | #### 22.99 Eur | #### 23.80 Eur |
| #### 9 x reactualizari automate |
| #### Promovare pe prima pagina (7 zile) |

#### Lista de preturi pentru pachetele de promovare OLX disponibile pe Autovit.ro pentru urmatoarea categorie: Piese

|     |     |     |     |
| --- | --- | --- | --- |
| #### Denumire pachet OLX | #### Promovarile incluse in pachet | #### Plata cu Cardul | #### Plata prin SMS |
| #### OLX Start | #### Promovare in lista de anunturi (3 zile) | #### 0.99 Eur | #### 2.38 Eur |
| #### OLX Standard | #### Promovare in lista de anunturi (7 zile) | #### 1.99 Eur | #### 3.57 Eur |
| #### 3 x reactualizari automate |
| #### OLX Premium | #### Promovare in lista de anunturi (30 zile) | #### 3.49 Eur | #### 5.95 Eur |
| #### 9 x reactualizari automate |
| #### Promovare pe prima pagina (7 zile) |

#### Lista de preturi pentru pachetele de promovare OLX disponibile pe Autovit.ro pentru urmatoarea categorie: Agro

|     |     |     |     |
| --- | --- | --- | --- |
| #### Denumire pachet OLX | #### Promovarile incluse in pachet | #### Plata cu Cardul | #### Plata prin SMS |
| #### OLX Start | #### Promovare in lista de anunturi (3 zile) | #### 7.99 Eur | #### 9.52 Eur |
| #### OLX Standard | #### Promovare in lista de anunturi (7 zile) | #### 15.99 Eur | #### 17.85 Eur |
| #### 3 x reactualizari automate |
| #### OLX Premium | #### Promovare in lista de anunturi (30 zile) | #### 21.99 Eur | #### 23.80 Eur |
| #### 9 x reactualizari automate |
| #### Promovare pe prima pagina (7 zile) |

#### Lista de preturi pentru pachetele de promovare OLX disponibile pe Autovit.ro pentru urmatoarea categorie: Motociclete

|     |     |     |     |
| --- | --- | --- | --- |
| #### Denumire pachet OLX | #### Promovarile incluse in pachet | #### Plata cu Cardul | #### Plata prin SMS |
| #### OLX Start | #### Promovare in lista de anunturi (3 zile) | #### 7.99 Eur | #### 9.52 Eur |
| #### OLX Standard | #### Promovare in lista de anunturi (7 zile) | #### 12.99 Eur | #### 14.28 Eur |
| #### 3 x reactualizari automate |
| #### OLX Premium | #### Promovare in lista de anunturi (30 zile) | #### 19.99 Eur | #### 21.42 Eur |
| #### 9 x reactualizari automate |
| #### Promovare pe prima pagina (7 zile) | 

Fişiere
=======

0 elemente • Sortate după Ultima modificare

Controale în vizualizarea listă

Afișare filtre rapide

Se încarcă…

Se încarcă…

Așteptați sau selectați altă vizualizare listă.

| Număr element | [SortTitlu](javascript:void(0);)<br><br>Afișarea Titlu acțiuni în coloană | [SortProprietar](javascript:void(0);)<br><br>Afișarea Proprietar acțiuni în coloană | [SortUltima modificare](javascript:void(0);)Sortare în ordine descrescătoare<br><br>Afișarea Ultima modificare acțiuni în coloană | [SortDimensiune](javascript:void(0);)<br><br>Afișarea Dimensiune acțiuni în coloană | Acțiune |
| --- | --- | --- | --- | --- | --- |

Nu există elemente de afișat.

RevocareSalvare

* [](https://www.facebook.com/share.php?title=Anexa%20Nr.2%20-%20Servicii%20de%20Promovare&u=https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr2-servicii-de-promovare-V27)
* [](https://twitter.com/share?text=Anexa%20Nr.2%20-%20Servicii%20de%20Promovare&url=https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr2-servicii-de-promovare-V27)
* [](https://www.linkedin.com/shareArticle?mini=true&title=Anexa%20Nr.2%20-%20Servicii%20de%20Promovare&url=https://ajutor.autovit.ro/autovithelp/s/article/anexa-nr2-servicii-de-promovare-V27)