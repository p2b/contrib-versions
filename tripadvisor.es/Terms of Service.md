Avisos, términos y condiciones del sitio web de Tripadvisor
===========================================================

Condiciones de uso de Tripadvisor

Última actualización: 16 de febrero de 2024

**Condiciones de uso**

[USO DE LOS SERVICIOS](#_USE_OF_THE)

[PRODUCTOS ADICIONALES](#_ADDITIONAL_PRODUCTS)

[ACTIVIDADES PROHIBIDAS](#_PROHIBITED_ACTIVITIES)

[POLÍTICA DE PRIVACIDAD Y DIVULGACIÓN DE INFORMACIÓN](#_PRIVACY_POLICY_AND)

[OPINIONES, COMENTARIOS Y USO DE OTRAS ÁREAS INTERACTIVAS; CONCESIÓN DE LICENCIAS](#_REVIEWS,_COMMENTS_AND)

* [Restricción de los derechos de licencia de Tripadvisor](#_Restricting_Tripadvisor%E2%80%99s_Licence)

[RESERVAR CON TERCEROS PROVEEDORES A TRAVÉS DE Tripadvisor](#_BOOKING_WITH_THIRD-PARTY)

* [Uso de los servicios de reserva de Tripadvisor](#_Use_of_Tripadvisor)
* [Terceros proveedores](#_Third-Party_Suppliers._The)
* [Reservar Alquileres vacacionales, reservar Experiencias y reservar en Restaurantes con terceros proveedores que figuran en sitios web filiales corporativos](#_Booking_Holiday_Rentals,)

[DESTINOS TURÍSTICOS](#_TRAVEL_DESTINATIONS)

* [Viajes internacionales](#_International_Travel._When)

[RENUNCIA DE RESPONSABILIDAD](#_LIABILITY_DISCLAIMER_1)

[INDEMNIZACIÓN](#_Indemnification)

[ENLACES A SITIOS WEB DE TERCEROS](#_LINKS_TO_THIRD-PARTY)

[SOFTWARE COMO PARTE DE LOS SERVICIOS; LICENCIAS DE MÓVILES ADICIONALES](#_SOFTWARE_AS_PART)

[AVISOS SOBRE DERECHOS DE AUTOR Y MARCAS COMERCIALES](#_COPYRIGHT_AND_TRADEMARK)

* [Política de notificación y retirada de contenido ilegal](#_Notice_and_Take-Down)

[MODIFICACIONES DE ESTE ACUERDO; LOS SERVICIOS; ACTUALIZACIONES; RESOLUCIÓN Y RETIRADA](#_MODIFICATIONS_TO_THE)

[JURISDICCIÓN Y LEGISLACIÓN VIGENTE](#_JURISDICTION_AND_GOVERNING)

[CONVERSOR DE DIVISAS](#_CURRENCY_CONVERTER)

[DISPOSICIONES GENERALES](#_GENERAL_PROVISIONS)

[SERVICIO DE AYUDA](#_SERVICE_HELP)

Le damos la bienvenida a los sitios web y propiedades para móviles de Tripadvisor (en adelante, “Sitios web”) que se encuentran en www.Tripadvisor.es y en los correspondientes dominios de país de nivel superior (incluidos los subdominios asociados), aplicaciones de software relacionadas (en ocasiones, “aplicaciones”), datos, mensajes SMS, API, correos electrónicos, comunicaciones por chat y teléfono, botones, widgets y anuncios (en conjunto, los “**Servicios**”).

Los Servicios los proporciona Tripadvisor LLC (en adelante “**Tripadvisor**”), una empresa con responsabilidad limitada ubicada en Delaware, Estados Unidos, con domicilio social en \[Tripadvisor LLC, 400 1st Avenue, Needham, MA 02494, USA\]. Los términos “nosotros” y “nuestro” se interpretarán de la misma forma.

Al crear una cuenta en Tripadvisor, acepta lo estipulado en los términos, condiciones y avisos que se establecen a continuación (en adelante, este “Acuerdo”). El presente Acuerdo no se aplica si usted se limita a navegar por los Sitios web o a utilizar los Servicios sin crear una cuenta Tripadvisor.  Lea detenidamente el presente Acuerdo, ya que contiene información relativa a sus derechos legales y las limitaciones sobre estos, así como una sección sobre la legislación aplicable y la jurisdicción de las disputas. Si usted es un consumidor de la UE o del Reino Unido, dispone del derecho de desistimiento del presente Acuerdo en un plazo de 14 días a partir de la fecha de celebración de este. Puede hacerlo, o resolver el presente Acuerdo en cualquier otro momento, cerrando su cuenta (poniéndose en contacto con nosotros o accediendo a “Información de la cuenta” mientras inicia sesión y seleccionando la opción “Cerrar cuenta”) y dejar de acceder o utilizar los Servicios.

Si es consumidor en la UE o en el Reino Unido, tendrá ciertos derechos obligatorios. Si usted es un consumidor que vive en otro lugar, también puede tener derechos de consumidor en virtud de las leyes de su territorio. Nada de lo dispuesto en el presente Acuerdo afectará a sus derechos de consumidor obligatorios.

La información, el texto, los enlaces, los gráficos, las fotos, los audios, los vídeos, los datos, el código u otros materiales o modificaciones de estos que pueda ver, a los que pueda acceder o con los que pueda interactuar a través de los Servicios se denominará “Contenido”. Tal y como aparecen definidos anteriormente, los “Servicios” son aquellos que proporciona Tripadvisor o nuestras empresas afiliadas corporativas (en conjunto “Empresas de Tripadvisor”). Para evitar cualquier tipo de duda, Tripadvisor es la propietaria de los Sitios web y es también quien los controla.  Sin embargo, hay ciertos Servicios que se ofrecen a través de los sitios web que pueden ser propiedad de las empresas afiliadas corporativas de Tripadvisor y estar bajo su control como, por ejemplo, Servicios con los que se pueden reservar Alquileres vacacionales, Experiencias y hacer reservas en Restaurantes con terceros proveedores (consulte la información que aparece a continuación). Como parte de nuestros Servicios, puede que le enviemos notificaciones sobre ofertas especiales, productos o servicios adicionales disponibles, ya sean nuestros, de nuestros afiliados o de nuestros socios, que puedan resultarle interesantes. Dichas notificaciones se enviarán normalmente a través de boletines de noticias y comunicaciones comerciales con el fin de conocerle mejor y saber cuáles son sus preferencias en relación con nuestros Servicios y los de nuestros afiliados. A su vez, esto posibilita la personalización de los servicios de acuerdo con esas preferencias.

El término “usted” o “usuario” se refiere a la persona física, empresa, organización empresarial u otra entidad jurídica que haya creado una cuenta de Tripadvisor y utilice los Servicios o contribuya con Contenido a ellos. El Contenido que aporte, envíe, transfiera o publique en los Servicios o a través de estos se denomina “su Contenido”, “Contenido de su propiedad”, “el Contenido que envíe” u otras variaciones.

Los Servicios se prestan de forma continua y únicamente para:

1. Ayudar a los usuarios a reunir información sobre viajes, publicar Contenido y buscar y reservar servicios de viajes.
2. Ayudar a las empresas de viajes, turismo y alojamiento a conectar con sus usuarios y con posibles clientes a través de servicios gratuitos o de pago que ofrezcan las Empresas de Tripadvisor o se proporcionen a través de estas.

#### Los productos, los servicios, la información y el Contenido de nuestros Servicios cambian con regularidad para estar al día (por ejemplo, con la información y las ofertas actuales). Esto significa, por ejemplo, que, en lo que respecta a los servicios de terceros, es posible que pueda reservar nuevos servicios de transporte, alojamientos, restaurantes, tours, actividades o experiencias, mientras que otros servicios pueden dejar de estar disponibles.

De acuerdo con los términos y condiciones que se especifican en este Acuerdo, es posible que cambiemos o modifiquemos de algún modo su contenido. En caso de que, una vez realizados los cambios, usted acceda a los Servicios o los utilice de forma continuada, se entenderá que acepta el Acuerdo modificado. Al final de este Acuerdo indicaremos la fecha en la que se han realizado las últimas revisiones del contenido, las cuales tendrán efecto inmediatamente después de su publicación. Le notificaremos de los cambios sustanciales en estos términos y condiciones enviando un aviso a la dirección de correo electrónico asociada a su perfil o colocando un aviso en nuestros sitios web. Consulte esta página periódicamente para conocer la última versión del Acuerdo. Si no desea aceptar ningún cambio en este Acuerdo, puede cerrar su cuenta y dejar de acceder a los Servicios (puede consultar una descripción de cómo hacerlo  [aquí](https://www.tripadvisorsupport.com/es-es/hc/traveler/articles/510) ).

**USO DE LOS SERVICIOS**

Como condición para el uso que haga de los Servicios, acepta que (i) toda la información que proporcione a través de los Servicios a las Empresas de Tripadvisor es verdadera, precisa, actual y completa, (ii) protegerá la información de su cuenta y supervisará y será completamente responsable del uso que haga de su cuenta cualquier persona que no sea usted, (iii) tiene 13 años o más. Las Empresas de Tripadvisor no recopilan conscientemente la información de ninguna persona que sea menor de 13 años. Si es un consumidor residente en la UE o el Reino Unido, podemos denegarle el acceso a los Servicios si infringe el presente Acuerdo o por cualquier otra causa razonable. Si no es un consumidor residente en la UE o el Reino Unido, nos reservamos el derecho, a nuestra entera discreción, de denegarle el acceso a los Servicios en cualquier momento y por cualquier motivo, incluido, entre otros, el incumplimiento del presente Acuerdo. Al usar los Servicios, incluidos los productos o servicios que permiten compartir Contenido con los sitios de terceros, acepta que usted es el único responsable de la información que comparta con las Empresas de Tripadvisor. Puede acceder a los Servicios únicamente con la finalidad para la que se han concebido y según lo permitido en este Acuerdo.

Copiar, transmitir, reproducir, replicar, publicar o redistribuir (a) Contenido o parte de este o (b) los Servicios en general está terminantemente prohibido sin el consentimiento previo por escrito de las Empresas de Tripadvisor. Para solicitar tal consentimiento, envíe su petición a la siguiente dirección:

Director, Partnerships and Business Development

Tripadvisor, LLC

400 1st Avenue

Needham, MA 02494, EE. UU.

Para acceder a determinadas funciones de los Servicios, tendrá que crear una cuenta. Al crearla, la información que proporcione deberá ser completa y exacta. Usted es el único responsable de la actividad que se produzca en ella como, por ejemplo, interacciones y comunicaciones que establezca con otras personas. Además, acepta mantener actualizados sus datos de contacto. 

Si crea una cuenta de Tripadvisor con fines comerciales y acepta el presente Acuerdo en nombre de una empresa, organización u otra entidad jurídica, acepta que está autorizado a hacerlo y que tiene la autoridad para vincular a dicha entidad al presente Acuerdo, en cuyo caso las palabras “usted” y “su” utilizadas en el presente Acuerdo se referirán a dicha entidad y la persona que actúe en nombre de la empresa se denominará “Representante comercial”.

Al usar los Servicios, es posible que encuentre enlaces a sitios y aplicaciones de terceros o que pueda interactuar con ellos,  por ejemplo, compartiendo Contenido desde los Servicios, incluido el suyo, con dichos sitios y aplicaciones de terceros. Tenga en cuenta que los sitios y aplicaciones de terceros pueden publicar el Contenido que comparta. Es posible que estos terceros cobren una tarifa por el uso de determinados servicios o contenidos que proporcionen en sus sitios web o a través de estos. Por lo tanto, deberá realizar todas las comprobaciones que considere necesarias o apropiadas antes de llevar a cabo cualquier tipo de transacción con un tercero para determinar si incurrirá en un cargo. Las tarifas o los cargos relativos a dicho contenido o servicios de terceros que puedan indicar las Empresas de Tripadvisor son meramente informativos. Las interacciones que realice con sitios y aplicaciones de terceros quedan bajo su responsabilidad. Por el presente documento, reconoce y acepta expresamente que las Empresas de Tripadvisor no son responsables en modo alguno de dichos sitios o aplicaciones de terceros.

Las empresas de Tripadvisor clasifican y presentan sus perfiles utilizando sistemas de recomendación. Las diferentes categorías de perfiles (como hoteles y restaurantes, cosas que hacer y vuelos) se clasifican en función de diferentes criterios, como el precio, los “favoritos de los viajeros” y la duración. Encontrará todos los detalles de nuestra política y criterios de clasificación en la página web [Cómo funciona el sitio web](https://www.tripadvisor.es/HowTheSiteWorks.html) .

Parte del Contenido que se muestra en los Servicios o a través de estos se utiliza para fines comerciales. Asimismo, acepta y reconoce que las Empresas de Tripadvisor pueden incluir publicidad y promociones en los Servicios, al lado o cerca de su Contenido o el de otras personas, o próximo a este de algún otro modo. Por ejemplo, en el caso de vídeos u otro contenido dinámico, la publicidad o las promociones podrían aparecer antes, durante o después de su presentación.

**PRODUCTOS ADICIONALES**

Si las Empresas de Tripadvisor lo estiman oportuno, pueden modificar, actualizar o dejar de ofrecer determinados productos y funciones de los Servicios. En virtud del presente Acuerdo, reconoce y acepta que las Empresas de Tripadvisor no tienen obligación alguna de almacenar o mantener su Contenido u otra información que proporcione, salvo que así lo exija la legislación aplicable.

Ofrecemos otros servicios que también pueden estar regidos por términos o acuerdos adicionales. Si utiliza tales servicios, tendrá a su disposición las condiciones adicionales, las cuales pasarán a formar parte de este Acuerdo salvo en los casos en que lo excluyan de forma expresa o lo sustituyan de algún otro modo. Por ejemplo, si usa o compra tales servicios adicionales con fines comerciales o empresariales, deberá aceptar las correspondientes condiciones adicionales. En la medida en que otras condiciones entren en conflicto con los términos y condiciones de este Acuerdo, prevalecerán las condiciones adicionales en lo relativo al conflicto con dichos servicios.

**ACTIVIDADES PROHIBIDAS**

El Contenido y la información disponibles en los Servicios o a través de estos (por ejemplo, mensajes, datos, información, texto, música, sonido, fotos, gráficos, vídeo, mapas, iconos, software, código u otros materiales), así como la infraestructura que se utiliza para proporcionarlos, son propiedad de las Empresas de Tripadvisor o bien estas cuentan con la correspondiente licencia otorgada por los propietarios terceros. Con respecto al Contenido que no sea suyo, acepta no modificar, copiar, distribuir, transmitir, mostrar, representar, reproducir, publicar, otorgar licencias, crear trabajos derivados, transferir, vender ni revender ningún tipo de información, software, producto o servicio obtenido de los Servicios o a través de estos. Asimismo, en virtud de este Acuerdo, acepta lo siguiente:

*  (i) No usar los Servicios ni el Contenido para fines comerciales que extralimiten el ámbito de aquellos permitidos expresamente en este Acuerdo y directrices asociadas que las Empresas de Tripadvisor hayan publicado.
* (ii) No acceder, controlar, reproducir, distribuir, transmitir, retransmitir, mostrar, vender, otorgar licencia, copiar o aprovechar de algún otro modo el Contenido de los Servicios (incluidos, entre otros, perfiles y fotos de usuarios), mediante el uso de robots, rastreadores, raspadores u otros medios automatizados o procesos manuales de otra clase para cualquier finalidad que no esté estipulada en este Acuerdo o que no cuente con nuestro consentimiento explícito por escrito.
* (iii) No infringir las restricciones que se incluyan en los apartados de exclusión del uso de robots en los Servicios ni eludir o sortear otras medidas que se utilicen para evitar o limitar el acceso a los Servicios.
* (iv) tomar cualquier medida que imponga, o pueda imponer, a nuestra entera discreción (o, si es un consumidor residente en el Reino Unido o la UE, a nuestra discreción razonable), una carga desproporcionada o desproporcionada en nuestra infraestructura;
* (v) No establecer enlaces profundos con ninguna parte de los Servicios para ningún fin sin nuestro permiso explícito por escrito.
* (vi) No insertar, replicar ni incorporar en modo alguno ninguna parte de los Servicios en otros sitios web o servicio sin nuestra autorización previa por escrito.
* (vii) No intentar modificar, traducir, adaptar, editar, descompilar, desmontar o aplicar ingeniería inversa a ningún programa de software que utilicen las Empresas de Tripadvisor en relación con los Servicios.
* (viii) No eludir, desactivar ni interferir en modo alguno con las funciones relativas a la seguridad de los Servicios ni aquellas que previenen o restringen el uso o la copia de Contenido.
* (ix) No descargar Contenido a menos que las Empresas de Tripadvisor indiquen explícitamente que es descargable.
* (x) usar, ayudar, alentar o permitir que un tercero utilice cualquier robot, araña, sistema de inteligencia artificial (IA) u otro dispositivo automatizado, proceso o medio para acceder, recuperar, copiar, raspar, agregar, recopilar, descargar o indexar de cualquier otra manera cualquier parte de los Servicios o cualquier Contenido, a menos que Tripadvisor lo permita expresamente por escrito.

**POLÍTICA DE PRIVACIDAD Y DIVULGACIÓN DE INFORMACIÓN**

La información personal que publique en los Servicios o que envíe en relación con estos se usará de acuerdo con lo estipulado en nuestra Declaración de privacidad y cookies. Haga clic [aquí](https://tripadvisor.mediaroom.com/ES-privacy-policy) para consultar nuestra Declaración de privacidad y cookies.

**OPINIONES, COMENTARIOS Y USO DE OTRAS ÁREAS INTERACTIVAS; CONCESIÓN DE LICENCIAS**

Sus comentarios siempre son bienvenidos. Tenga en cuenta que, al proporcionar su Contenido en los Servicios o a través de estos, ya sea enviándolo por correo electrónico, publicándolo en algún producto de sincronización de Tripadvisor o a través de los servicios y aplicaciones de un tercero, o al incluir Contenido suyo, o parte de este, que se transmite a su cuenta de Tripadvisor en virtud de algún producto, servicio, opinión, pregunta, foto, vídeo, comentario, sugerencia, idea de las Empresas de Tripadvisor o similar incluido en su Contenido, otorga a dichas empresas una licencia no exclusiva, libre de derechos de autor, perpetua, transferible, irrevocable y susceptible de someterse a otras licencias para (a) alojar, usar, reproducir, modificar, ejecutar, adaptar, traducir, distribuir, publicar, crear obras derivadas, y mostrar y representar públicamente su Contenido ante el mundo en cualquier formato y por cualquier medio conocido o que se conciba en el futuro; (b) ofrecer su Contenido al resto del mundo y que otros hagan lo mismo; (c) proporcionar, promocionar y mejorar los Servicios, así como poner a disposición de otras empresas, organizaciones o personas el Contenido de su propiedad que haya compartido en los Servicios para sindicarlo, retransmitirlo, distribuirlo, promocionarlo o publicarlo en otros medios y servicios, en virtud de nuestra política de privacidad y de este Acuerdo, y para (d) usar el nombre o la marca comercial que envíe asociado a su Contenido. Usted reconoce que Tripadvisor puede optar por atribuir su Contenido a nuestra entera discreción (o, si es un consumidor residente en el Reino Unido o la UE, a nuestra discreción razonable). También otorga a las Empresas de Tripadvisor el derecho de perseguir ante la ley a cualquier persona o entidad que infrinja sus derechos o los de dichas empresas en el Contenido por incumplimiento del presente Acuerdo. Reconoce y acepta que su Contenido no es confidencial ni de su propiedad. Acepta que es el propietario de su Contenido o de que cuenta con las licencias, derechos (incluidos el copyright y cualesquiera otros derechos de propiedad intelectual), consentimientos y permisos necesarios para publicarlo y usarlo de cualquier otra forma (tanto usted como las Empresas de Tripadvisor), tal y como se autoriza en virtud de este Acuerdo.

Si se determina que conserva derechos morales (incluidos los derechos al reconocimiento de la autoría o integridad) sobre el Contenido, en la medida en que lo permita la legislación vigente, manifiesta lo siguiente: (a) no exige el uso de información personal que le identifique en relación con el Contenido, con las obras derivadas de este ni con las actualizaciones o mejoras asociadas; (b) no opone objeción alguna a que las Empresas de Tripadvisor, sus licenciatarios, sucesores y cesionarios publiquen, utilicen, modifiquen, eliminen o aprovechen el Contenido; (c) renuncia para siempre a todos los derechos morales de autor sobre el Contenido en su totalidad o en parte y se compromete a no reclamar ni reivindicar ninguno de tales derechos, y (d) exime para siempre a las Empresas de Tripadvisor, sus licenciatarios, sucesores y cesionarios ante cualesquiera reclamaciones que usted pudiera reivindicar contra las Empresas de Tripadvisor en virtud de tales derechos morales.

Los comentarios o las sugerencias que proporcione podrán usarse en cualquier momento y sin obligación por nuestra parte de mantenerlos confidenciales.

Los Servicios pueden contener foros de debate, tablones de anuncios, servicios de opiniones, muros de viajes y otros foros en los cuales puede publicar su Contenido, como opiniones sobre sus experiencias de viajes, mensajes, materiales u otros elementos (las “Áreas interactivas”). Si Tripadvisor proporciona tales Áreas interactivas en los Sitios web, usted será el único responsable del uso que haga de ellas y las utilizará bajo su propio riesgo. Las Empresas de Tripadvisor no garantizan confidencialidad alguna con respecto al Contenido que proporcione en los Servicios ni en las Áreas interactivas.  En la medida en que algunas de las entidades que forman las Empresas de Tripadvisor proporcionen algún canal de comunicación privada entre los usuarios, acepta que dichas entidades pueden supervisar la esencia de tales comunicaciones con el fin de preservar nuestra comunidad y los Servicios. Por el presente documento, reconoce que las Empresas de Tripadvisor no editan ni controlan los mensajes de los usuarios que se publican o distribuyen a través de los Servicios, incluidos los salones de chat, los tablones de anuncios y otros foros de comunicación, y no será responsable ni asumirá ninguna obligación sobre dichos mensajes.  En concreto, Tripadvisor no edita ni controla el Contenido de los usuarios que aparece en los Sitios web.  No obstante, las Empresas de Tripadvisor se reservan el derecho a retirar o restringir sin previo aviso tales mensajes u otro Contenido de los Servicios si creen de buena fe que el Contenido incumple el presente Acuerdo o consideran por cualquier otro motivo que es razonablemente necesario retirarlos o restringirlos para preservar los derechos de las Empresas de Tripadvisor o de otros usuarios de los Servicios. Si no está de acuerdo con la eliminación de su Contenido de los sitios web, puede apelar nuestra decisión a través del proceso interno de gestión de quejas de Tripadvisor. Puede encontrar más información sobre cómo objetar y apelar una decisión de eliminar o restringir su Contenido  [aquí](https://www.tripadvisorsupport.com/es-es/hc/traveler/articles/623) . Al usar las Áreas interactivas, acepta explícitamente enviar solo Contenido de su propiedad que cumpla con las directrices que Tripadvisor haya publicado, vigentes en el momento del envío.  Asimismo, acepta explícitamente no publicar, cargar, transmitir, distribuir, almacenar, crear ni publicar de ningún otro modo a través de los Servicios Contenido de su propiedad que cumpla alguno de estos criterios:

1. Es falso, ilegal, engañoso, injurioso, difamatorio, obsceno, pornográfico, indecente, lascivo, sugerente, acosador (o fomenta el acoso hacia otra persona), intimidatorio, invasor de los derechos de privacidad o publicidad, abusivo, provocador, fraudulento o censurable por cualquier otra razón.
2. Es claramente ofensivo para la comunidad online, como el contenido que promueve el racismo, la intolerancia, el odio o el daño físico de cualquier tipo contra cualquier grupo o persona.
3. Constituye, fomenta, promueve o proporciona las instrucciones para llevar a cabo una actividad ilegal o un delito criminal, que dé lugar a responsabilidad civil, que infrinja los derechos de cualquier tercero en cualquier país del mundo, o que de cualquier otro modo cree responsabilidad o incumpla alguna ley local, nacional o internacional, incluidas, entre otras, las regulaciones de la Comisión del Mercado de Valores (SEC) de EE.UU. o cualquier norma de cualquier mercado de valores (incluidas, entre otras, la Bolsa de Valores de Nueva York \[NYSE\], la NASDAQ o la Bolsa de Valores de Londres).
4. Proporciona instrucciones para realizar actividades ilegales, como la fabricación o la compra de armas ilegales, la violación de la privacidad de las personas y el suministro o la creación de virus informáticos.
5. Puede infringir cualquier patente, marca, secreto comercial, derecho de autor u otro derecho de propiedad o intelectual de las partes. En particular, fomenta la copia no autorizada o ilegal de la obra protegida por copyright de otra persona, como el suministro de programas informáticos pirateados o sus enlaces, el suministro de información para eludir los dispositivos protegidos contra copia e instalados por el fabricante, o el suministro de música pirateada o enlaces a los archivos de música pirateada.
6. Es correo masivo o “spam”, “correo basura”, forma parte de “cadenas de mensajes” o de “sistemas piramidales”.
7. Suplanta la identidad de otra persona o entidad, o tergiversa de cualquier otro modo su afiliación con dicha persona o entidad, incluidas las Empresas de Tripadvisor.
8. Es información privada de terceros, como direcciones postales, números de teléfono, direcciones de correo electrónico, números de seguros de salud del país correspondiente o números de tarjetas de crédito.  En nuestros Sitios web se puede publicar el apellido de una persona, siempre y cuando esta haya dado su permiso explícitamente.
9. Incluye páginas restringidas o de acceso exclusivamente mediante contraseña, o páginas o imágenes ocultas (que no estén enlazadas a otra página accesible).
10. Incluye o tiene como objetivo proporcionar virus, datos corruptos y otros archivos dañinos, disruptivos o destructivos.
11. No guarda relación con el tema de las Áreas interactivas en las que se publica.
12. Según el exclusivo criterio de Tripadvisor (o bien, si usted es un consumidor residente en el Reino Unido o la UE, a nuestro juicio), que (a) infringe cualquiera de los anteriores subapartados, (b) infringe las directrices relacionadas que Tripadvisor ofrece, (c) es censurable, (d) restringe o inhibe la capacidad de otra persona para usar o disfrutar de las Áreas interactivas o cualquier otro aspecto de los Servicios o (e) puede exponer a las Empresas de Tripadvisor o sus usuarios a cualquier tipo de daño o responsabilidad.

En la medida en que lo permita la ley aplicable, las Empresas de Tripadvisor no se hacen responsables ni asumen ninguna obligación sobre ningún Contenido publicado, almacenado, transmitido o cargado a los Servicios por usted (si el Contenido es suyo) o por terceros (en el caso de Contenido en general), ni sobre ningún tipo de pérdida o daño de dicho Contenido. Las Empresas de Tripadvisor tampoco son responsables de ningún tipo de error, difamación, calumnia, injuria, omisión, falsedad, obscenidad, pornografía o blasfemia que pueda detectar. Tripadvisor, como proveedor de servicios interactivos, y en la medida en que lo permita la ley aplicable, no es responsable de las afirmaciones, manifestaciones o Contenido que facilitan sus usuarios (incluido usted con respecto a su Contenido) en los Sitios web ni en ningún otro foro. A pesar de que Tripadvisor no tiene la obligación de examinar, editar o supervisar el Contenido que se publica o distribuye a través de las Áreas interactivas, se reserva el derecho, a su total discreción (o bien, si es usted un consumidor residente en el Reino Unido o la UE, a nuestra razonable discreción), de eliminar, examinar, traducir o editar sin previo aviso cualquier Contenido publicado o almacenado en los Servicios en cualquier momento y por cualquier motivo, o de solicitar a terceros que realicen tales acciones en su nombre. En la medida en que lo permita la ley aplicable, usted es el único responsable de crear copias de seguridad del Contenido que publique, nos envíe o almacene en los Servicios, y de reponerlo, bajo su cuenta y riesgo. Puede consultar todos los detalles de nuestras políticas de moderación de contenido, incluidas las restricciones que podamos imponer a su contenido, [aquí](https://www.tripadvisor.es/Trust).

**RESTRICCIÓN O SUSPENSIÓN DEL USO DE LOS SERVICIOS**

Cualquier uso de las Áreas interactivas o de otras secciones de los Servicios que infrinja lo descrito anteriormente infringe también las condiciones del presente Acuerdo y puede dar lugar, entre otras consecuencias, a la rescisión o suspensión de sus derechos para utilizar las Áreas interactivas o los Servicios en general. Tripadvisor también puede suspender el tratamiento de los avisos y quejas presentados a través de los Servicios si usted envía con frecuencia avisos o quejas manifiestamente infundados.

Nuestro enfoque para la suspensión de su uso de la plataforma se detalla [aquí](https://www.tripadvisor.es/Trust).

**Restricción de los derechos de licencia de Tripadvisor** En adelante, puede limitar el uso que las Empresas de Tripadvisor hagan de su Contenido en virtud del presente Acuerdo (tal y como se describe anteriormente) otorgándoles una licencia más acotada, según se describe más adelante (en este documento, “Licencia restringida”).  Si decide conceder dicha Licencia restringida, deberá seleccionarla en esta página (debe iniciar sesión en su cuenta). En ese caso, los derechos que conceda a las Empresas de Tripadvisor sobre su Contenido de conformidad con las condiciones de licencia que se han estipulado anteriormente (“Licencia estándar”) estarán acotados por ciertas limitaciones importantes que se describen en los párrafos del 1 al 6 a continuación, como que las Empresas de Tripadvisor no ostentarán una Licencia estándar sobre su Contenido salvo para las opiniones en formato texto y las puntuaciones de burbujas relacionadas que publique (y para las que las Empresas de Tripadvisor seguirán ostentando una Licencia estándar), sino una “Licencia restringida” sobre el resto de su Contenido, según se define a continuación:

1. Al publicar su Contenido en los Servicios, la licencia que concede a las Empresas de Tripadvisor sobre este es una licencia internacional, no exclusiva, libre de derechos de autor, transferible y susceptible de someterse a otras licencias para alojar, usar, distribuir, modificar, ejecutar, reproducir, mostrar o representar públicamente, traducir y crear obras derivadas de su Contenido con el fin de mostrarlo en los Servicios, así como usar su nombre o marca comercial en relación con dicho Contenido. De conformidad con el párrafo 6 que se incluye a continuación, la Licencia restringida afecta a todo el Contenido de su propiedad (excepto a las opiniones en formato texto y las puntuaciones de burbujas relacionadas, tal y como se especifica anteriormente) que usted u otra persona en su nombre (por ejemplo, un tercero que colabore en su cuenta o que la gestione de algún modo) publiquen en los Servicios o a través de estos.

1. Si decide rescindir los derechos de licencia de las Empresas de Tripadvisor que se otorgan por el presente documento a alguno de los elementos de su Contenido sujeto a la Licencia restringida, deberá eliminar la publicación en cuestión de los Servicios.  En la misma medida, si decide revocar los derechos de licencia que tienen las Empresas de Tripadvisor sobre todo su Contenido sujeto a la Licencia restringida, deberá cancelar la cuenta; para hacerlo, encontrará las instrucciones en [esta página](https://www.tripadvisorsupport.com/es-es/hc/traveler/articles/510). Pese a las disposiciones que puedan especificar lo contrario, su Contenido (a) permanecerá en los Servicios en tanto que lo haya compartido con otras personas y estas lo hayan copiado o almacenado antes de que usted lo haya eliminado o haya cancelado su cuenta, (b) puede seguir mostrándose en los Servicios durante un periodo de tiempo razonable hasta que lo retiremos, después de que lo haya eliminado o haya cancelado su cuenta o (c) puede quedar retenido (aunque no se podrá mostrar públicamente) en un sistema de copia de seguridad durante un tiempo por motivos técnicos, normativos, legales o de moderación de engaños.

1. Las Empresas de Tripadvisor no usarán su Contenido en anuncios de productos y servicios de terceros que se muestren a otras personas sin un consentimiento aparte (incluido el Contenido patrocinado), aunque acepta y reconoce que dichas empresas pueden incluir publicidad y promociones en los Servicios, al lado o cerca de su Contenido o el de otras personas, o próximo a este de algún otro modo. Por ejemplo, en el caso de vídeos u otro contenido dinámico, la publicidad o las promociones podrían aparecer antes, durante o después de su presentación.  Cada vez que su Contenido aparezca en los Servicios, lo identificaremos mediante el nombre o la marca comercial asociado a su Contenido que nos envíe.  

1. Las Empresas de Tripadvisor no concederán a ningún tercero el derecho de publicar su Contenido en lugares distintos a los Servicios. Sin embargo, si comparte su Contenido en los Servicios (excepto en la función “Viajes”, donde el Contenido se puede mostrar de forma privada) este se mostrará públicamente y activaremos una función que permite a los demás usuarios compartirlo, insertando la publicación o por cualquier otro medio (salvo en la función Viajes que, tal y como hemos mencionado, el Contenido se puede mostrar de forma privada) en servicios de terceros. Además, permitiremos que los motores de búsqueda puedan realizar búsquedas.

1. Salvo las disposiciones que se modifiquen en los párrafos del 1 al 6 de esta sección de este Acuerdo, tanto sus derechos y obligaciones como los nuestros estarán sujetos al resto de las condiciones de este Acuerdo. La licencia que otorgue a las Empresas de Tripadvisor modificada por los mencionados párrafos del 1 al 6 se denominará “Licencia restringida”.

1. Con el fin de disipar cualquier tipo de duda, el Contenido que envíe a los Servicios relacionado con otros servicios o programas de las Empresas de Tripadvisor no está sujeto a la Licencia restringida, sino que se regirá por los términos y condiciones del servicio o programa de Tripadvisor en cuestión.

**RESERVAR CON TERCEROS PROVEEDORES A TRAVÉS DE Tripadvisor**

**Uso de los Servicios de reserva de Tripadvisor.** Las Empresas de Tripadvisor le permiten buscar, seleccionar y reservar viajes con terceros proveedores sin salir de los Servicios.

#### IMPORTANTE: Tripadvisor no vende reservas de viajes, sino que permite a terceros vender sus reservas de viaje en Tripadvisor.

#### Tripadvisor puede facilitarle la promoción y venta de reservas de viajes por parte de terceros. Sin embargo, Tripadvisor no es la parte que se encarga de la promoción o venta. Si reserva una reserva con un proveedor externo, el contrato de la reserva siempre es únicamente entre usted y el tercero. Tripadvisor no es (a) el comprador ni el vendedor de la reserva; (b) una parte en el contrato con el tercero o responsable del cumplimiento de dicho contrato; o (c) un agente suyo o del tercero en relación con la compra o venta. Si realiza una reserva con un tercer proveedor, acepta leer y comprometerse a cumplir los términos y condiciones de compra y de uso del sitio web del proveedor, la política de privacidad y cualquier otra norma o política relacionada con la propiedad o el sitio del proveedor. Las interacciones que establezca con los terceros proveedores son por cuenta y riesgo propio. Las Empresas de Tripadvisor no tendrán ninguna responsabilidad con respecto a los actos, las omisiones, los errores, las manifestaciones, las garantías, los incumplimientos o las negligencias de los terceros proveedores, ni por ningún tipo de lesión personal, muerte, daños a la propiedad u otros daños o gastos derivados de las interacciones entre usted y terceros proveedores.

Sus derechos de recurso en relación con un contrato entre usted y un proveedor externo (por ejemplo, en relación con reembolsos y cancelaciones) serán entre usted y el tercero tal como se establece en el contrato con dicho tercero. En el caso poco probable de que una reserva esté disponible al hacer el pedido, pero deje de estarlo antes del registro, el único recurso que dispone es ponerse en contacto con el proveedor para encontrar una solución alternativa o cancelar su reserva y obtener un reembolso (si procede y según lo que establezca la legislación aplicable y su contrato con el proveedor externo).

#### Los proveedores externos pueden ser negocios o consumidores. Si es un consumidor residente en el Reino Unido o en la UE y el proveedor tercero con el que contrata también es un consumidor, tenga en cuenta que no podrá hacer valer ningún derecho de los consumidores frente a dicho proveedor.

Al reservar reservas de viajes a través de los sitios web, tendrá que crear una cuenta si aún no lo ha hecho y reconoce que acepta las prácticas descritas en nuestra Política de privacidad y el presente Acuerdo.

EN CALIDAD DE USUARIO DE LOS SERVICIOS, INCLUIDOS LOS SERVICIOS DE RESERVA DE LAS EMPRESAS DE TRIPADVISOR, RECONOCE Y ACEPTA, EN LA MEDIDA QUE LO PERMITA LA LEY, QUE: (1) LAS EMPRESAS DE TRIPADVISOR NO TENDRÁN RESPONSABILIDAD ALGUNA ANTE USTED NI ANTE NINGÚN TERCERO POR LAS TRANSACCIONES NO AUTORIZADAS REALIZADAS MEDIANTE LA CONTRASEÑA O LA CUENTA DE USTED Y (2) EL USO NO AUTORIZADO DE SU CONTRASEÑA O CUENTA PODRÍA ACARREARLE RESPONSABILIDADES ANTE TRIPADVISOR, SUS EMPRESAS FILIALES CORPORATIVAS U OTROS TERCEROS.

El uso de nuestros Servicios, incluida la creación de una cuenta de Tripadvisor, es gratuito y el pago solo se procesará cuando reserve una reserva a través de los Servicios. Cuando realice una reserva facilitada por las Empresas de Tripadvisor, recopilaremos su información de pago y la transmitiremos al proveedor para poder llevar a cabo la transacción, tal y como se describe en nuestra Política de privacidad. Tenga en cuenta que es el proveedor, no las Empresas de Tripadvisor, el responsable de procesar su pago y formalizar la reserva.

Las Empresas de Tripadvisor no interferirán en las reservas arbitrariamente, pero se reservan el derecho de retirar los servicios de reserva debido a determinadas circunstancias atenuantes, por ejemplo, cuando una reserva ya no esté disponible o cuando tengamos motivos razonables para sospechar que una solicitud de reserva podría ser fraudulenta. Las Empresas de Tripadvisor también se reservan el derecho de tomar medidas para verificar su identidad a fin de procesar su petición de reserva.

**Terceros proveedores.** Las Empresas de Tripadvisor no son agencias de viajes ni tienen o proporcionan servicios de transporte, alojamientos, restaurantes, visitas, actividades o experiencias. A pesar de que las Empresas de Tripadvisor muestran información acerca de las propiedades que poseen los proveedores externos y facilitan reservas con determinados proveedores en los Sitios web de las Empresas de Tripadvisor o a través de estos, tales acciones no implican, sugieren ni constituyen en modo alguno el consentimiento ni la aprobación de terceros proveedores por parte de las Empresas de Tripadvisor, ni ningún tipo de afiliación entre ambos. Aunque los usuarios puedan puntuar y opinar sobre determinados servicios de transporte, alojamientos, restaurantes, visitas, actividades o experiencias en función de su propia experiencia, las Empresas de Tripadvisor no suscriben ni recomiendan los productos o servicios de los terceros proveedores, excepto cuando Tripadvisor otorga a ciertas empresas premios basados en las opiniones publicadas por los usuarios. Las Empresas de Tripadvisor no suscriben el Contenido que publican, envían o proporcionan de algún otro modo usuarios o empresas, ni tampoco las opiniones, recomendaciones o consejos que se puedan verter en él, por lo que renuncian explícitamente a toda responsabilidad relacionada con él. Acepta, además, que las Empresas de Tripadvisor no son responsables de la precisión ni de la integridad de la información que obtienen de terceros proveedores y muestran en los Servicios.

Es posible que los Servicios enlacen con los sitios de los proveedores o a otros sitios web que Tripadvisor no gestiona ni controla. Para obtener más información al respecto, consulte la sección “Enlaces a sitios web de terceros” más adelante.

**Reservar servicios de viajes a través de la aplicación de Tripadvisor para dispositivos móviles.**  Algunos de los servicios de viaje que se reservan a través de la aplicación para dispositivos móviles de Tripadvisor se rigen por un conjunto adicional de condiciones de uso.  Estas condiciones están disponibles a través de la aplicación y se pueden encontrar [aquí](https://tripadvisor.mediaroom.com/us-terms-of-use-booking).  

**Reservar Alquileres vacacionales, reservar Experiencias y reservar en Restaurantes con terceros proveedores que figuran en sitios web filiales corporativos.** Algunas de las empresas afiliadas corporativas de Tripadvisor hacen las funciones de mercado para facilitar que los viajeros puedan (1) formalizar acuerdos de alquileres vacacionales con dueños y gestores de propiedades (“Alquileres vacacionales”), (2) hacer reservas en restaurantes (“Restaurantes”) o (3) hacer reservas de tours, actividades y atracciones (en conjunto, “Experiencias”) con terceros proveedores de tales Experiencias; en adelante, cada uno de los proveedores de dichos Alquileres vacacionales o Experiencias se denominará “Anunciante”. Dado que estas empresas filiales corporativas de Tripadvisor sindican sus anuncios a otras entidades del grupo de Empresas de Tripadvisor, la publicidad aparece en los Sitios web de estas últimas.  Como usuario, usted es responsable del uso que haga de los Servicios (incluidos, en particular, los Sitios web de las Empresas de Tripadvisor) y de todas las transacciones asociadas a Alquileres vacacionales, Restaurantes o Experiencias proporcionadas por las empresas afiliadas corporativas de Tripadvisor. No somos propietarios de ningún Alquiler vacacional, Restaurante ni Experiencia que figura en los Servicios, y tampoco los gestionamos ni formalizamos los correspondientes acuerdos.

Debido a que Tripadvisor y sus empresas afiliadas corporativas no forman parte de las transacciones de Alquileres vacacionales, reservas en Restaurantes o de Experiencias entre los viajeros y los Anunciantes, cualquier disputa o conflicto que implique una transacción potencial o real entre usted y un Anunciante (incluidos la calidad, el acondicionamiento, la seguridad o la legalidad del Alquiler vacacional, Restaurante o Experiencia; la precisión del Contenido del anuncio; la capacidad del Anunciante de alquilar una propiedad de Alquiler vacacional, proporcionarle una reserva, comida u otro servicio en un Restaurante o una Experiencia; o su capacidad para pagar una propiedad de Alquiler vacacional, una comida o servicio en un Restaurante o una Experiencia) es responsabilidad exclusiva de cada usuario.

Una de las empresas afiliadas corporativas de Tripadvisor puede actuar como agente de responsabilidad limitada del Anunciante con el único propósito de transmitir su pago al Anunciante. Usted acepta pagar a un Anunciante, o a un afiliado corporativo de Tripadvisor que actúe como agente de cobro de pago limitado en nombre de un Anunciante, las tasas que se le especifiquen antes de que usted haga una reserva para cualquier reserva o experiencia de alquiler vacacional y las tasas que se le cobren como resultado de los daños causados por el uso de la reserva o experiencia de alquiler de acuerdo con los términos o políticas aplicables.

Para obtener más información sobre los cargos por Alquileres vacacionales, depósitos de seguridad, cargos por Experiencias, procesamiento de los pagos, reembolsos y cuestiones similares, consulte los términos y condiciones de nuestras empresas afiliadas (en el caso de Experiencias, visite [Viator](https://www.viator.com/es-ES/support/termsAndConditions?localeSwitch=1); en el caso de Restaurantes, visite [TheFork](https://www.thefork.es/#geo_dmn_redirect=20210629AT100); y, en el de Alquileres vacacionales, la página de [Alquileres vacacionales de las Empresas de Tripadvisor](https://rentals.tripadvisor.com/es_ES/termsandconditions/traveler). Al realizar una reserva de Alquiler vacacional, Restaurante o Experiencia proporcionada por una de nuestras empresas afiliadas corporativas, deberá reconocer y aceptar los correspondientes términos y condiciones y la política de privacidad.

Si surge una disputa con un Anunciante de la Unión Europea, consulte algunos de los métodos alternativos para resolverla en la siguiente página de Internet: [http://ec.europa.eu/consumers/odr/](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=ES).

**DESTINOS TURÍSTICOS**

**Viajes internacionales.** Cuando realiza reservas para un viaje internacional con terceros proveedores o planifica viajes internacionales a través de los Servicios, usted es el responsable de asegurarse de que cumple todos los requisitos de entrada de extranjeros y de que sus documentos de viaje, incluidos pasaportes y visados, están en regla.

En cuanto a los requisitos de pasaporte y visado, póngase en contacto con la embajada o el consulado pertinentes para obtener información. Puesto que los requisitos pueden cambiar en cualquier momento, asegúrese de consultar la información actualizada antes de realizar la reserva o salir de viaje. Las Empresas de Tripadvisor no asumen responsabilidad alguna ante los viajeros a los que se les deniegue la entrada a un vuelo o a cualquier país por no llevar los documentos de viaje que les requiera la compañía aérea, autoridad o país en cuestión, incluidos los países por los que el viajero tan solo tenga que pasar de camino hacia su destino.

También es su responsabilidad consultar con un médico las recomendaciones actuales sobre vacunas antes de realizar un viaje internacional, así como asegurarse de que cumple todos los requisitos sanitarios de acceso y seguir todas las prescripciones médicas en relación con su viaje.

Aunque la mayoría de los viajes, incluidos los que se realizan a destinos internacionales, transcurren sin incidentes, los viajes a determinados destinos pueden conllevar un riesgo superior comparado con otros. Tripadvisor insta a los viajeros a examinar y revisar las prohibiciones, las advertencias, los avisos y los anuncios sobre viajes que emitan sus Gobiernos y los Gobiernos de los países de destino antes de realizar reservas de viajes a destinos internacionales. Por ejemplo, el Gobierno de EE. UU. ofrece información sobre la situación en diversos países y los niveles de riesgo en ciertos destinos internacionales en los siguientes sitios web: [www.state.gov](http://www.state.gov/), [www.tsa.gov](https://www.tsa.gov/),[www.dot.gov](http://www.dot.gov/), [www.faa.gov](http://www.faa.gov/), [www.cdc.gov](http://www.cdc.gov/), [www.treas.gov/ofac](http://www.treas.gov/ofac) y [www.customs.gov](http://www.customs.gov/).

AL PROPORCIONAR INFORMACIÓN RELEVANTE PARA VIAJAR A DETERMINADOS DESTINOS INTERNACIONALES, LAS EMPRESAS DE TRIPADVISOR NO MANIFIESTAN NI GARANTIZAN QUE VIAJAR A TALES LUGARES SEA ACONSEJABLE O QUE NO CONLLEVE RIESGOS, Y NO SE RESPONSABILIZA DE LOS DAÑOS O LAS PÉRDIDAS QUE PUEDAN DERIVARSE DEL VIAJE A DICHOS DESTINOS.

**RENUNCIA DE RESPONSABILIDAD**

LEA DETENIDAMENTE ESTA SECCIÓN. LAS DISPOSICIONES DE ESTA SECCIÓN LIMITAN LA RESPONSABILIDAD DE LAS EMPRESAS DE TRIPADVISOR ANTE USTED POR LOS PROBLEMAS QUE PUEDAN SURGIR EN RELACIÓN CON EL USO QUE HAGA DE LOS SERVICIOS. SI NO ENTIENDE LOS TÉRMINOS DE ESTA SECCIÓN O DE CUALQUIER OTRA PARTE DEL PRESENTE ACUERDO,

LA INFORMACIÓN, EL SOFTWARE, LOS PRODUCTOS Y LOS SERVICIOS PUBLICADOS O PROPORCIONADOS DE OTRA MANERA A TRAVÉS DE LOS SERVICIOS (INCLUIDOS, ENTRE OTROS, LOS CREADOS O PROPORCIONADOS POR LA IA) PUEDEN INCLUIR IMPRECISIONES O ERRORES, COMO ERRORES DE DISPONIBILIDAD DE RESERVAS Y DE PRECIOS. LAS EMPRESAS DE TRIPADVISOR NO GARANTIZAN LA PRECISIÓN DE LA INFORMACIÓN NI DE LAS DESCRIPCIONES DE LOS ALOJAMIENTOS, EXPERIENCIAS, VUELOS, CRUCEROS, RESTAURANTES O CUALQUIER OTRO PRODUCTO DE VIAJE QUE APAREZCA EN LOS SERVICIOS (LO QUE INCLUYE, SIN QUE SIRVA DE LIMITACIÓN, LOS PRECIOS, DISPONIBILIDAD, FOTOGRAFÍAS, ANUNCIOS DE ALOJAMIENTO, EXPERIENCIAS, VUELOS, CRUCEROS, RESTAURANTES U OTRAS CARACTERÍSTICAS DE LOS PRODUCTOS DE VIAJE, DESCRIPCIONES GENERALES DE LOS PRODUCTOS, OPINIONES, PUNTUACIONES, ETC.). ADEMÁS, LAS EMPRESAS DE Tripadvisor SE RESERVAN EXPLÍCITAMENTE EL DERECHO A CORREGIR CUALQUIER ERROR DE DISPONIBILIDAD Y DE PRECIOS QUE SEA OBVIO EN LOS SERVICIOS O EN LAS RESERVAS PENDIENTES REALIZADAS CON UN PRECIO QUE SEA OBVIAMENTE INCORRECTO.

TRIPADVISOR NO GARANTIZA LA IDONEIDAD DE LOS SERVICIOS, INCLUIDA LA INFORMACIÓN QUE SE ENCUENTRA EN SUS SITIOS WEB NI EN NINGUNA DE SUS PARTES, PARA NINGÚN FIN, Y LA INCLUSIÓN O LA OFERTA DE CUALQUIERA DE LOS PRODUCTOS O SERVICIOS EN SUS SITIOS WEB O EN CUALQUIER OTRO LUGAR PROPORCIONADO A TRAVÉS DE LOS SERVICIOS NO CONSTITUYE NINGÚN TIPO DE REFRENDO O RECOMENDACIÓN DE TALES PRODUCTOS O SERVICIOS POR PARTE DE TRIPADVISOR, SIN PERJUICIO DE LOS PREMIOS QUE SE PUEDAN CONCEDER POR LAS OPINIONES DE LOS USUARIOS. TODA ESTA INFORMACIÓN, EL SOFTWARE, LOS PRODUCTOS Y LAS OFERTAS DE SERVICIOS QUE SE PROPORCIONAN EN LOS SERVICIOS O A TRAVÉS DE ESTOS SE SUMINISTRAN “TAL CUAL”, SIN GARANTÍA DE NINGÚN TIPO. EN LA MÁXIMA MEDIDA PERMITIDA POR LA LEGISLACIÓN APLICABLE, TRIPADVISOR DENIEGA TODAS LAS GARANTÍAS Y LAS CONDICIONES, ASÍ COMO OTROS TÉRMINOS DE CUALQUIER TIPO, CON RESPECTO A QUE LOS SERVICIOS, SUS SERVIDORES O CUALQUIER DATO (INCLUIDOS CORREOS ELECTRÓNICOS) ENVIADO DESDE TRIPADVISOR ESTÉN LIBRES DE VIRUS O DE OTROS COMPONENTES DAÑINOS. EN LA MÁXIMA MEDIDA PERMITIDA POR LA LEGISLACIÓN APLICABLE, POR EL PRESENTE DOCUMENTO, TRIPADVISOR DENIEGA TODAS LAS GARANTÍAS Y CONDICIONES CON RESPECTO A LA INFORMACIÓN, EL SOFTWARE, LOS PRODUCTOS Y LOS SERVICIOS, INCLUIDAS TODAS LAS GARANTÍAS Y CONDICIONES IMPLÍCITAS, O LOS TÉRMINOS DE CUALQUIER TIPO, EN LO REFERENTE A LA COMERCIABILIDAD, LA ADECUACIÓN PARA UN FIN PARTICULAR, LA TITULARIDAD, LA POSESIÓN PACÍFICA Y LA NO INFRACCIÓN.

LAS EMPRESAS DE TRIPADVISOR TAMBIÉN DENIEGAN EXPLÍCITAMENTE CUALQUIER GARANTÍA Y MANIFESTACIÓN, ASÍ COMO OTROS TÉRMINOS DE CUALQUIER TIPO, EN LO REFERENTE A LA PRECISIÓN O AL CARÁCTER PRIVADO DEL CONTENIDO DISPONIBLE EN LOS SERVICIOS Y A TRAVÉS DE ESTOS.

LOS TERCEROS PROVEEDORES QUE PROPORCIONAN INFORMACIÓN SOBRE ALOJAMIENTOS, VUELOS, ALQUILERES, EXPERIENCIAS, RESTAURANTES O CRUCEROS, VIAJES U OTROS SERVICIOS EN LOS SERVICIOS O A TRAVÉS DE ESTOS SON CONTRATISTAS INDEPENDIENTES, NO AGENTES NI EMPLEADOS DE LAS EMPRESAS DE TRIPADVISOR. LAS EMPRESAS DE TRIPADVISOR NO ASUMEN NINGUNA RESPONSABILIDAD POR LOS ACTOS, LOS ERRORES, LAS OMISIONES, LAS MANIFESTACIONES, LAS GARANTÍAS, LOS INCUMPLIMIENTOS O LAS NEGLIGENCIAS DE TALES PROVEEDORES, NI POR NINGÚN TIPO DE LESIÓN PERSONAL, MUERTE, DAÑO A LA PROPIEDAD, U OTROS DAÑOS O GASTOS DERIVADOS DE ELLOS. TRIPADVISOR NO SERÁ RESPONSABLE NI REALIZARÁ REEMBOLSOS EN EL CASO DE SUFRIR ALGÚN TIPO DE DEMORA, CANCELACIÓN, EXCESO DE RESERVAS, HUELGA, FUERZA MAYOR U OTRAS CAUSAS AJENAS A SU CONTROL DIRECTO, Y TAMPOCO SERÁ RESPONSABLE DE NINGÚN TIPO DE GASTO ADICIONAL, OMISIÓN, RETRASO, CAMBIO DE ITINERARIO O ACTUACIÓN DE CUALQUIER GOBIERNO O AUTORIDAD.

EN VIRTUD DE LO ESPECIFICADO ANTERIORMENTE, USTED UTILIZA LOS SERVICIOS POR SU CUENTA Y RIESGO Y LAS EMPRESAS DE TRIPADVISOR (O SUS EJECUTIVOS, DIRECTIVOS Y EMPLEADOS) NO SERÁN RESPONSABLES EN NINGÚN CASO POR NINGÚN TIPO DE PÉRDIDAS O DAÑOS, YA SEAN DIRECTOS, INDIRECTOS, PUNITIVOS, INCIDENTALES, ESPECIALES O DERIVADOS, NI POR NINGUNA PÉRDIDA DE INGRESOS, BENEFICIOS, FONDOS DE COMERCIO, DATOS, CONTRATOS O USO DE DINERO, NI TAMPOCO POR LAS PÉRDIDAS O LOS DAÑOS DERIVADOS DE LOS SIGUIENTES HECHOS O RELACIONADOS CON ELLOS: INTERRUPCIONES DEL NEGOCIO DE CUALQUIER TIPO; EL ACCESO, LA VISUALIZACIÓN O EL USO DE LOS SERVICIOS; LA DEMORA O LA INCAPACIDAD PARA ACCEDER O UTILIZAR LOS SERVICIOS (LO QUE INCLUYE, ENTRE OTROS ASPECTOS, SU CONFIANZA EN LOS COMENTARIOS Y LAS OPINIONES QUE APARECEN EN LOS SERVICIOS O A TRAVÉS DE ELLOS); CUALQUIER VIRUS INFORMÁTICO, ERROR, TROYANOS, INFORMACIÓN, SOFTWARE, SITIOS ENLAZADOS, PRODUCTOS Y SERVICIOS OBTENIDOS A TRAVÉS DE LOS SERVICIOS (INCLUIDOS, ENTRE OTROS, CUALQUIER PRODUCTO DE SINCRONIZACIÓN DE LAS EMPRESAS DE TRIPADVISOR); LESIONES PERSONALES O DAÑOS A LA PROPIEDAD DE CUALQUIER NATURALEZA, RESULTANTES DEL USO QUE USTED HAGA DE LOS SERVIDORES DE LOS SERVICIOS O TODA LA INFORMACIÓN PERSONAL O FINANCIERA QUE ESTOS ALMACENEN; LOS ERRORES U OMISIONES EN EL CONTENIDO O LAS PÉRDIDAS O DAÑOS DE CUALQUIER TIPO QUE SE INCURRAN COMO RESULTADO DEL USO DEL CONTENIDO O DERIVADOS, DE ALGÚN OTRO MODO, DEL ACCESO, LA VISUALIZACIÓN O EL USO DE LOS SERVICIOS. LO ANTERIOR SERÁ DE APLICACIÓN A LAS PÉRDIDAS O LOS DAÑOS CON INDEPENDENCIA DE SU FUNDAMENTO, YA SEA NEGLIGENCIA, CONTRATO, AGRAVIO O RESPONSABILIDAD ESTRICTA O DE OTRA ÍNDOLE, INCLUSO EN EL CASO DE QUE SE HAYA NOTIFICADO A TRIPADVISOR O A SUS EMPRESAS FILIALES CORPORATIVAS LA POSIBILIDAD DE QUE SE PRODUZCAN.

Si las Empresas de Tripadvisor resultaran ser responsables de alguna pérdida o daño derivado del uso que usted haga de los Servicios, en ningún caso la responsabilidad global de las Empresas de Tripadvisor superará la mayor de las sumas siguientes: (a) los costes abonados a las Empresas de Tripadvisor en concepto de las transacciones realizadas en los Servicios o a través de ellos que hayan dado lugar a la reclamación o bien (b) cien dólares (100 USD).

La limitación de responsabilidad refleja la asignación de riesgo entre las partes. Las limitaciones que se especifican en esta sección permanecerán vigentes y se aplicarán incluso si alguno de los recursos limitados indicados en las presentes condiciones se considera fallido en su propósito esencial. Las limitaciones de responsabilidad que se proporcionan en estas condiciones redundarán en beneficio de las Empresas de Tripadvisor.

TANTO ESTE ACUERDO, ASÍ COMO LA RENUNCIA DE RESPONSABILIDAD ANTERIOR NO AFECTAN A LOS DERECHOS LEGALES OBLIGATORIOS QUE NO PUEDEN EXCLUIRSE EN VIRTUD DE LA LEGISLACIÓN APLICABLE, POR EJEMPLO, LAS LEYES DE PROTECCIÓN DE LOS CONSUMIDORES VIGENTES EN CIERTOS PAÍSES. EN EL REINO UNIDO Y LA UE, ESTO INCLUYE LA RESPONSABILIDAD POR MUERTE O LESIONES PERSONALES CAUSADAS POR NUESTRA NEGLIGENCIA O LA NEGLIGENCIA DE NUESTROS EMPLEADOS, AGENTES O SUBCONTRATISTAS Y POR FRAUDE O FALSEDAD FRAUDULENTA.

Si usted es un consumidor del Reino Unido o de la UE, el presente Acuerdo no excluye ninguna responsabilidad por las pérdidas y los daños causados por el incumplimiento de cualquier obligación de actuar con diligencia y habilidad razonables o por el incumplimiento del presente Acuerdo, siempre que los daños resultantes fueran previsibles cuando surgieron las obligaciones.     

SI LA LAY DEL PAÍS EN EL QUE VIVE NO CONTEMPLA NINGUNA LIMITACIÓN EN PARTICULAR NI NINGUNA EXCLUSIÓN DE RESPONSABILIDAD EN ESTA CLÁUSULA, NO SE APLICARÁ LA LIMITACIÓN. LA RENUNCIA DE RESPONSABILIDAD SE APLICARÁ EN LA MÁXIMA MEDIDA PERMITIDA POR LA LEGISLACIÓN LOCAL.

**INDEMNIZACIÓN**

Esta sección "Indemnización" no se aplica a los consumidores residentes en la UE o en el Reino Unido.

Acepta defender e indemnizar a las Empresas de Tripadvisor, así como a cualquiera de sus funcionarios, directivos, empleados y agentes con respecto a cualquier reclamación, acción jurisdiccional, demanda, recuperación, pérdida, daño, multa o sanción, u otros costes o gastos de cualquier tipo y naturaleza, incluidos, entre otros, los costes jurídicos y financieros razonables de terceros que se deriven de las situaciones siguientes:

* (i) incumplimiento por su parte del presente Acuerdo o de los documentos a los que se hace referencia en él
* (ii) infracción por su parte de cualquier ley o cualquier derecho de terceros o
* (iii) uso que haga de los Servicios, incluidos los Sitios web de las Empresas de Tripadvisor.

**ENLACES A SITIOS WEB DE TERCEROS**

Los Servicios pueden contener hiperenlaces a sitios web gestionados por terceros distintos de las Empresas de Tripadvisor. Dichos hiperenlaces se proporcionan solo para su referencia. Las Empresas de Tripadvisor no controlan tales sitios web ni son responsables de su contenido ni de las prácticas de privacidad o de otro tipo. Además, le corresponde a usted adoptar las precauciones necesarias para garantizar que los enlaces que seleccione o el software que descargue (tanto de este como de otros sitios web) estén libres de elementos de naturaleza destructiva, tales como virus, gusanos, troyanos o defectos, entre otros. El hecho de que las Empresas de Tripadvisor incluyan hiperenlaces a dichos sitios web no implica que suscribamos el material que se encuentra en estos o en aplicaciones, ni ninguna asociación con sus operadores.

En algunos casos, es posible que un sitio o una aplicación de un tercero le solicite enlazar el perfil de su cuenta de Tripadvisor a un perfil de otro sitio de un tercero. La elección es totalmente opcional y la decisión de permitir que esta información se vincule puede deshacerse (a través del sitio web o la aplicación del tercero) en cualquier momento. Si decide enlazar su cuenta de Tripadvisor al sitio web o a la aplicación de un tercero, dicho sitio o aplicación podrá acceder a la información que haya almacenado en su cuenta de Tripadvisor, incluidos los datos de otros usuarios con los que comparte información. Deberá leer los términos y condiciones y la política de privacidad de los sitios y aplicaciones de terceros que visite, ya que incluyen normas y permisos sobre cómo utilizan su información y que pueden diferir de aquellos de los Servicios, incluidos nuestros Sitios web. Le recomendamos que revise dichos sitios y aplicaciones de terceros; el uso que haga de ellos queda bajo su responsabilidad.

**SOFTWARE COMO PARTE DE LOS SERVICIOS; LICENCIAS DE MÓVILES ADICIONALES**

El software de los Servicios también está sujeto a los controles de exportación de Estados Unidos. Queda prohibido descargar, exportar o reexportar de algún modo la totalidad o parte del software de los Servicios (a) a Cuba, Iraq, Sudán, Corea del Norte, Irán, Siria ni ningún otro país sometido a un embargo comercial de Estados Unidos, así como a los ciudadanos o residentes de dichos países, y (b) a ninguna persona o entidad que figure en la lista de ciudadanos especialmente designados publicada por el Departamento del Tesoro de Estados Unidos o en la tabla de personas sometidas a órdenes de denegación del Departamento de Comercio de Estados Unidos. Al utilizar los Servicios, usted acepta que no está ubicado en ninguno de dichos países, no es ciudadano ni residente de ellos ni se encuentra bajo su control, y que no figura en ninguna de las listas anteriores.

Según se especifica anteriormente, los Servicios incluyen software que, en ocasiones, podrá denominarse “aplicaciones”.  Cualquier software que esté disponible para su descarga en los Servicios (el “Software”) es una obra sujeta a copyright de Tripadvisor o de otros terceros, según se indique en cada caso. El uso que haga de dicho Software se rige por las condiciones del acuerdo de licencia de usuario final, si lo hubiera, que se adjunta o se incluye con el Software. No puede instalar ni utilizar ningún tipo de Software que adjunte o incluya un acuerdo de licencia, a no ser que acepte primero las condiciones de dicho acuerdo. En caso de que el Software disponible para su descarga en los Servicios no incluya un acuerdo de licencia, por el presente documento le otorgamos a usted, el usuario, una licencia limitada, personal e intransferible para utilizar dicho Software, con el fin de que visualice y utilice de cualquier otro modo los Servicios de conformidad con los términos y condiciones del presente Acuerdo (incluidas las políticas a las que en él se hace referencia). Dicha licencia limitada no tendrá ningún otro propósito.

Tenga en cuenta que el Software (incluidos, entre otros, todo el código Java, HTML, XML y los controles Active X que contienen los Servicios) es propiedad de Tripadvisor o esta empresa dispone de la correspondiente licencia, y que está protegido por las disposiciones de los tratados internacionales y las leyes de derechos de autor. Cualquier reproducción o redistribución del Software queda explícitamente prohibida y puede comportar graves sanciones civiles y penales. Se procesará a los infractores con la mayor severidad posible.

Es posible que en algunas partes del software para móviles de Tripadvisor se utilice material con derechos de autor, el uso del cual está reconocido por Tripadvisor. Además, existen condiciones específicas que rigen el uso de determinadas aplicaciones para móviles de Tripadvisor. Visite la página [Licencias de móviles](https://tripadvisor.mediaroom.com/ES-mobile-licenses) para conocer los avisos específicos sobre las aplicaciones para móviles de Tripadvisor.

SIN PERJUICIO DE LO ESPECIFICADO ANTERIORMENTE, QUEDA EXPLÍCITAMENTE PROHIBIDA LA COPIA O LA REPRODUCCIÓN DEL SOFTWARE EN CUALQUIER OTRO TIPO DE SERVIDOR O UBICACIÓN PARA SU REPRODUCCIÓN O REDISTRIBUCIÓN POSTERIOR. DE ACUERDO CON LA LEY APLICABLE, SI EXISTE ALGUNA GARANTÍA RELATIVA AL SOFTWARE, SOLO SERÁ DE CONFORMIDAD CON LAS CONDICIONES DEL ACUERDO DE LICENCIA O DEL PRESENTE ACUERDO, SEGÚN CORRESPONDA.

**AVISOS SOBRE DERECHOS DE AUTOR Y MARCAS COMERCIALES**

Tripadvisor, el logotipo del búho, los globos de puntuaciones y todos los demás lemas o nombres de servicios y productos que se muestran en los Servicios son marcas comerciales según el Derecho anglosajón o registradas de Tripadvisor LLC o de sus proveedores o licenciantes, y no se podrán copiar, imitar ni utilizar, en su totalidad o en parte, sin el permiso previo por escrito de Tripadvisor o del titular de la correspondiente marca comercial. Además, el aspecto y el estilo de los Servicios, incluidos nuestros Sitios web, los encabezados, los gráficos personalizados, los iconos de botones y las secuencias de comandos de todas las páginas conforman la marca de servicio, la marca comercial o la imagen comercial de Tripadvisor, y no se pueden copiar, imitar ni utilizar, en su totalidad o en parte, sin el permiso previo por escrito de Tripadvisor. El resto de las marcas comerciales, marcas comerciales registradas, nombres de productos y nombres o logotipos de empresas que se mencionan en los Servicios pertenecen a sus respectivos propietarios. A menos que se estipule lo contrario en alguna de las disposiciones de este Acuerdo, las referencias a cualquier producto, servicio, proceso u otra información, realizadas mediante el nombre comercial, la marca comercial, el fabricante, el proveedor o de cualquier otro modo, no constituyen ni implican su aprobación, patrocinio o recomendación por parte de Tripadvisor.

Todos los derechos reservados. Tripadvisor no es responsable del contenido disponible en los sitios web gestionados por terceros distintos a Tripadvisor.

**Política de notificación y retirada de contenido ilegal**

Tripadvisor funciona según un procedimiento de “notificación y retirada” de contenido. Si tiene alguna objeción o queja con respecto al Contenido, incluidos los mensajes de los usuarios publicados en los Servicios, o si cree que el material o el contenido publicados en los Servicios vulneran algún derecho de autor del cual es titular, póngase en contacto con nosotros inmediatamente; para ello, siga nuestro procedimiento de notificación y retirada de contenido. [**Haga clic en este enlace para consultar dicho procedimiento y la Política de derechos de autor**](https://www.tripadvisor.es/StaticVelocityXmlPage-a_xml.noticetakedown__2E__xml). Una vez que se haya completado el procedimiento, Tripadvisor responderá a las quejas válidas y fundamentadas llevando a cabo todos los pasos pertinentes para retirar el contenido ilegal, en un plazo razonable de tiempo.

**MODIFICACIONES DE ESTE ACUERDO; LOS SERVICIOS; ACTUALIZACIONES; RESOLUCIÓN Y RETIRADA**

**El presente Acuerdo:**

Tripadvisor puede modificar, añadir o eliminar los términos y condiciones del presente Acuerdo o cualquiera de sus partes cuando lo estime oportuno y a su entera discreción (o a su discreción razonable si se trata de consumidores residentes en el Reino Unido o la UE), si así lo considera necesario, por causas legales, con fines técnicos o normativos en general, o debido a cambios en los Servicios proporcionados o en la naturaleza o la disposición de los Servicios. A partir de entonces, usted acepta explícitamente estar sujeto a tales términos y condiciones con sus enmiendas.

**Modificaciones:**

Las Empresas de Tripadvisor pueden modificar, suspender o interrumpir cualquier aspecto de los Servicios en cualquier momento, incluidos la disponibilidad de cualquier función, la base de datos o el Contenido, sin ningún coste para usted, incluidas las opciones de:

(a)           garantizar el cumplimiento de la legislación aplicable y/o reflejar los cambios en la legislación y los requisitos reglamentarios pertinentes, como las leyes obligatorias sobre consumidores;

(b)           realizar un mantenimiento temporal, corregir errores, implementar ajustes técnicos con fines operativos o realizar mejoras, como adaptar los Servicios a un nuevo entorno técnico, transferir los Servicios a una nueva plataforma de alojamiento o garantizar la compatibilidad del Servicio con los dispositivos y el software (según se actualice cada cierto tiempo);

(c)           para actualizar o modificar los Servicios, incluida la publicación de nuevas versiones de los sitios web en determinados dispositivos o la modificación o modificación de las funciones y funciones existentes;

(d)           para modificar la estructura, el diseño o la disposición de los Servicios, lo que incluye cambiar el nombre o la nueva marca de los Servicios, o modificar, mejorar o ampliar las funciones y funcionalidades disponibles; y

(e)           por motivos de seguridad.

Para usuarios del Reino Unido y de la UE: Si realizamos cambios en los Servicios que consideremos razonablemente que afectarán de forma significativa y negativa a su acceso o uso de los Servicios, le avisaremos con al menos 30 días de antelación de los cambios. Si no rescinde el contrato con nosotros antes de que se produzcan estos cambios, entenderemos que acepta los cambios.

**Actualizaciones:**

Podemos hacer cambios para mantener los Servicios de conformidad con la legislación aplicable sobre consumidores. Si se activan las actualizaciones automáticas en sus ajustes, actualizaremos automáticamente los Servicios. Puede cambiar los ajustes en cualquier momento si prefiere no recibir más actualizaciones automáticas.

Para obtener la mejor experiencia y garantizar que los Servicios funcionen correctamente, le recomendamos que acepte cualquier actualización de los Servicios de la que le informemos cuando estén disponibles. Esto también puede requerir que actualice el sistema operativo de su dispositivo. A medida que se lancen nuevos sistemas operativos y dispositivos, es posible que, con el tiempo, dejemos de admitir versiones anteriores.

Si decide no descargar ni instalar ninguna actualización, es posible que los Servicios ya no estén disponibles, no sean compatibles o ya no tengan funciones anteriores.

No somos responsables de que no haya descargado o instalado ninguna actualización o la versión publicada más reciente de los Servicios para poder beneficiarse de funciones o funcionalidades nuevas o mejoradas y/o cumplir los requisitos de compatibilidad cuando le hayamos informado sobre la actualización, explicado las consecuencias de no instalarla y proporcionado instrucciones de instalación.

Cualquier cambio en el Servicio que no se ajuste a la presente disposición de “Actualizaciones” estará sujeto a la sección de “Modificaciones” anterior.

**Resolución:**

Las Empresas de Tripadvisor también pueden imponer límites o restringir de algún otro modo su acceso a los Servicios, en su totalidad o en parte, sin previo aviso ni obligación alguna, por motivos técnicos o de seguridad, para evitar el acceso no autorizado, la pérdida o la destrucción de datos, o si Tripadvisor o sus empresas afiliadas corporativas consideran, a su entera discreción, que ha incumplido alguna de las disposiciones de este Acuerdo o de alguna ley o regulación aplicable, y si esto implica interrumpir la prestación de los Servicios.

Tripadvisor puede rescindir el presente Acuerdo ante usted en cualquier momento, sin previo aviso, si cree de buena fe que ha incumplido sus disposiciones o si considera por cualquier otro motivo que es razonablemente necesario rescindirlo para preservar los derechos de las Empresas de Tripadvisor o de otros usuarios de los Servicios. En consecuencia, dejaríamos de proporcionarle dichos Servicios.

**JURISDICCIÓN Y LEGISLACIÓN VIGENTE**

Tripadvisor LLC, una empresa con responsabilidad limitada ubicada en Estados Unidos, es propietaria de este sitio web y es quien también lo controla. El presente Acuerdo y cualquier disputa o reclamación (incluidos disputas o reclamaciones no contractuales) derivados o relacionados con él, su objeto o creación, se regirán e interpretarán según la legislación de la mancomunidad de Massachusetts (EE. UU.). Por la presente, usted acepta la jurisdicción y el distrito de los tribunales de Massachusetts, EE. UU., en exclusiva, y estipula la equidad y la conveniencia de resolver los procesos judiciales en dichos tribunales para todos los litigios, ya sean contractuales o no contractuales, derivados o relacionados con el uso que usted u otros terceros hagan de los Servicios. Asimismo, acepta que todas las reclamaciones que pueda presentar contra Tripadvisor LLC derivadas o relacionadas con los Servicios deben oírse y resolverse en el tribunal de una jurisdicción competente en la materia y ubicado en la mancomunidad de Massachusetts. El uso de los Servicios no está autorizado en ninguna jurisdicción en la que no tengan efecto todas las disposiciones de los presentes términos y condiciones, lo que incluye, sin que sirva de limitación, este párrafo. Esta cláusula no limita el derecho de Tripadvisor LLC a emprender acciones contra usted en otros tribunales competentes. Lo especificado anteriormente no se aplicará en la medida en que la legislación aplicable de su país de residencia requiera la aplicación de otra ley o jurisdicción (en particular, si utiliza los Servicios como consumidor) y no se puede excluir por contrato ni regirse por la Convención de las Naciones Unidas sobre los Contratos de Compraventa Internacional de Mercaderías, si fuera aplicable por cualquier otro motivo.  Si utiliza los Servicios como consumidor y no como empresa o Representante legal de una empresa, estará autorizado a presentar reclamaciones contra Tripadvisor en los tribunales de su país de residencia. Esta cláusula se aplicará en la máxima medida permitida por la legislación de su país de residencia.

**CONVERSOR DE DIVISAS**

Los tipos de cambio de divisas se basan en varias fuentes disponibles públicamente y deben utilizarse solo a modo de guía. Los tipos de cambio no se verifican con precisión, por lo que los tipos de cambio reales pueden variar. Es posible que las cotizaciones de divisas no se actualicen diariamente. La información sobre divisas que se proporciona se supone exacta, pero las Empresas de Tripadvisor no garantizan que lo sea. Cuando utilice esta información con un objetivo financiero, le recomendamos que se ponga en contacto con un profesional cualificado para comprobar la precisión de los tipos de cambio de divisas. No autorizamos el uso de esta información para ningún propósito que no sea para su uso personal y se le prohíbe explícitamente revender, redistribuir y utilizar esta información para fines comerciales.

**DISPOSICIONES GENERALES**

Nos reservamos el derecho de reclamar nombres de usuario, nombres de cuenta, apodos, usuarios o cualquier otro identificador de usuario por cualquier motivo sin contraer por ello ninguna responsabilidad con usted.

Por el presente documento, acepta que no existe ninguna relación de empresa conjunta, agencia, sociedad ni empleo entre usted y Tripadvisor o sus afiliados como resultado del presente Acuerdo o del uso de los Servicios.

Nuestro cumplimiento del presente Acuerdo está sujeto a las leyes y los decretos existentes, y ninguna disposición que el presente Acuerdo contenga puede limitar nuestro derecho a exigir el cumplimiento de las solicitudes de imposición legal u otras solicitudes gubernamentales o judiciales, o los requisitos relativos al uso que haga de los Servicios o de la información proporcionada o recopilada por nosotros con respecto a dicho uso. En la medida permitida por la legislación aplicable, usted acepta interponer cualquier reclamación o acción jurisdiccional derivada de su acceso o su uso de los Servicios o relacionada con él antes de dos (2) años a partir de la fecha en la que tal reclamación o acción se presentó o se devengó, en ausencia de lo cual dicha reclamación o acción jurisdiccional se desestimará irrevocablemente.

Si cualquiera de las partes del presente Acuerdo se considerara no válida o sin efecto según la legislación aplicable, incluidas, entre otras, las exclusiones de responsabilidad en cuanto a garantías y las limitaciones de responsabilidad descritas anteriormente, entonces la disposición considerada no válida o sin efecto será reemplazada por una disposición válida y con efecto cuyo significado se asemeje lo máximo posible a la disposición original, y todas las disposiciones restantes de este Acuerdo continuarán en vigor.

El presente Acuerdo (y cualquier término y condición a los que se haga referencia en el presente documento) constituye el acuerdo completo entre usted y Tripadvisor con respecto a los Servicios, y reemplaza a todas las comunicaciones y propuestas anteriores o contemporáneas, ya estén en formato electrónico, oral o escrito, formalizadas entre usted y Tripadvisor con respecto a los Servicios. En los procedimientos judiciales o administrativos basados o relacionados con el presente Acuerdo se admitirá una versión impresa del mismo y de cualquier notificación que exista en formato electrónico, en la misma medida y en virtud de las mismas condiciones que otros documentos y registros comerciales, originalmente generados y conservados en formato impreso.

Las secciones que se incluyen a continuación seguirán vigentes, si procede, tras la resolución de este Acuerdo:

* #### Productos adicionales
    
* #### Actividades prohibidas
    
* #### Opiniones, comentarios y uso de otras Áreas interactivas; Concesión de licencias
    
    * #### Restricción de los derechos de licencia de Tripadvisor
        
* Destinos turísticos
    * Viajes internacionales
* Renuncia de responsabilidad
* Indemnización
* Software como parte de los Servicios; Licencias de móviles adicionales
* #### Avisos sobre derechos de autor y marcas comerciales
    
    * #### Política de notificación y retirada de contenido ilegal
        
* #### Modificaciones de los Servicios; Rescisión
    
* #### Jurisdicción y legislación vigente
    
* #### Disposiciones generales
    
* #### Servicio de ayuda
    

Los términos y condiciones de este Acuerdo están disponibles en el idioma de los Sitios web o aplicaciones de Tripadvisor desde los que se puede acceder a los Servicios.

Es posible que los Sitios web o aplicaciones desde los que se puede acceder a los Servicios no siempre se actualicen de manera regular y periódica y, en consecuencia, no es necesario registrarlos como un producto editorial bajo la legislación pertinente.

Los nombres ficticios de empresas, productos, personas, personajes o datos mencionados en los Servicios o a través de ellos no pretenden representar ningún tipo de persona, empresa, producto o acontecimiento real.

En ningún caso se considerará que las disposiciones expuestas en este Acuerdo otorgan a algún tercero derechos o beneficios, salvo si se considera explícitamente que las empresas filiales corporativas de Tripadvisor son terceras beneficiarias del presente Acuerdo.

Queda prohibido transferir cualesquiera de sus derechos u obligaciones en virtud de este Acuerdo a otras personas sin nuestro consentimiento.

Se reservan todos los derechos que no se otorguen explícitamente en el presente documento.

**SERVICIO DE AYUDA**

Para obtener respuestas a sus preguntas o formas de ponerse en contacto con nosotros, visite nuestro [Centro de ayuda](https://www.tripadvisorsupport.com/es-ES/hc/traveler) o envíenos un correo electrónico a  [help@tripadvisor.com](mailto:help@tripadvisor.com) . También puede escribirnos a la siguiente dirección:

Tripadvisor, LLC

400 1st Avenue

Needham, MA 02494, EE. UU.

Tenga en cuenta que Tripadvisor LLC no acepta correspondencia legal ni notificaciones de procesos legales por ningún medio que no sea una copia impresa enviada a la dirección inmediatamente anterior, a menos que esté obligada por la ley aplicable.  Para evitar cualquier género de dudas y sin que sirva de limitación, no aceptamos avisos ni notificaciones legales que se entreguen a nuestras empresas filiales o subsidiarias, salvo en los casos que se describen a continuación.

**CONTACTO DE LA LEY DE SERVICIOS DIGITALES PARA USUARIOS**

Puede ponerse en contacto directamente con nosotros a través de la siguiente información:

Dirección de correo electrónico:  [help@tripadvisor.com](mailto:help@tripadvisor.com) 

**CONTACTO DE DIGITAL SERVICES ACT Y REPRESENTANTE LEGAL DE LAS AUTORIDADES**

Si es una autoridad nacional de un Estado miembro, la Comisión Europea o el Consejo Europeo de Servicios Digitales, puede ponerse en contacto con nosotros directamente a través de la siguiente información de contacto:

Dirección de correo electrónico: [poc-dsa@tripadvisor.com](mailto:poc-dsa@tripadvisor.com) 

Los organismos profesionales y los organismos de confianza también pueden ponerse en contacto con nosotros a través de esta dirección de correo electrónico.

Como alternativa, las autoridades públicas pueden ponerse en contacto con Tripadvisor Ireland Limited, que es nuestro representante legal a efectos de la Ley de Servicios Digitales, utilizando los siguientes datos:

Dirección: 70 Sir John Rogerson's Quay, Dublín 2, Irlanda

Dirección de correo electrónico:  [dsa.notifications@tripadvisor.com](mailto:dsa.notifications@tripadvisor.com)

Número de teléfono: +35315126124

No se ponga en contacto con nosotros a través de nuestro representante legal a menos que se ponga en contacto con nosotros en nombre de uno de los organismos públicos mencionados anteriormente, ya que Tripadvisor no responderá a su solicitud. Si tiene alguna pregunta sobre nuestra plataforma o desea enviar una queja, póngase en contacto con nosotros a través de los datos de contacto que se indican en las secciones “AYUDA AL SERVICIO” o “CONTACTO DE LA LEY DE SERVICIOS DIGITALES PARA USUARIOS” anteriores.

©2024 Tripadvisor LLC. Todos los derechos reservados.

Última actualización: 16 de febrero de 2024

* [](#print "print")
* [](https://twitter.com/share?url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D20310%26item%3D32187 "Twitter Share")
* [](https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D20310%26item%3D32187 "Facebook Share")
* [](https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D20310%26item%3D32187 "Linkedin Share")
* [](#email "email")