**A** [**jofogas.hu**](http://www.jofogas.hu/) **működési szabályzata**

A jelen felhasználási feltételek (a továbbiakban: **Szabályzat**) a **Jófogás Korlátolt Felelősségű Társaság** (nyilvántartja a Fővárosi Törvényszék Cégbírósága a Cg. 01-09-433149 cégjegyzékszámon; székhelye: 1134 Budapest, Váci út 49. III. emelet, adószáma: 32606753-2-41; elektronikus levelezési címe: ugyfelszolgalat@jofogas.hu, telefon: + 36 1 808 8288; a továbbiakban: **Társaság**) által üzemeltetett Jófogás.hu internetes lapon valamint mobil alkalmazásokon keresztül  (a továbbiakban együttesen: **Weblap** vagy **Weboldal**) elérhető információs társadalommal összefüggő és kapcsolódó szolgáltatások (a továbbiakban: **Szolgáltatás**, több Szolgáltatásra vonatkozóan **Szolgáltatások**) igénybevételének és a Weblap használatának feltételeit tartalmazza. A Weblap használata kizárólag a jelen Szabályzatnak megfelelően történhet. A korábbi Szabályzatok az [alábbi linken](http://docs.jofogas.hu/archivum/) érhetők el.

**1.Általános rendelkezések**

A Társaság hirdetési helyet biztosít Felhasználói részére termékeik és szolgáltatásaik hirdetésére, illetve a hirdetéshez és hirdetések böngészéséhez kapcsolódó egyéb szolgáltatásokat nyújt a Weboldalon, azaz a Jófogás.hu internetes oldalon és az ahhoz hasonló feltételekkel működtetett mobil alkalmazásokon keresztül elérhető adatbázisban, illetve kereső rendszerben. A mobil alkalmazásokban a Jófogás.hu internetes oldalon elérhető egyes funkciók nem vagy korlátozott módon érhetőek el – így pl. a Bolt fiók csak webes böngészés esetén érhető el – ezekre a jelen Szabályzat rendelkezései értelemszerűen alkalmazandók. Jelen Szabályzat szempontjából felhasználó minden olyan személy, aki megtekinti a Weblapot, illetve aki hirdetést helyez el, aki terméket kínál eladásra, vagy aki termék megvételére ajánlatot tesz, azaz hirdetésre jelentkezik (a továbbiakban: **Felhasználó**).

A Weboldalon elérhető Szolgáltatásokat kizárólag 18. életévét betöltött cselekvőképes magánszemély és gazdálkodó szervezet Felhasználó veheti igénybe. A Szolgáltatás igénybevételével, így különösen, de nem kizárólagosan a regisztrációval, illetve a Szolgáltatás megrendelésével a Felhasználó elismeri, hogy teljes cselekvőképességgel és jogképességgel rendelkező, 18. életévét betöltött természetes személy, vagy olyan gazdálkodó szervezet, amely képviseletére jogosult képviselője által jár el, valós és helyes adatok megadásával.

A Társaság a hirdetést feladó Felhasználók két kategóriáját különbözteti meg aszerint, hogy 

* egy adott Felhasználó a fogyasztókkal szembeni tisztességtelen kereskedelmi gyakorlat tilalmáról szóló 2008. évi XLVII. törvény (Fttv.) 7. § (5) bekezdés f) pontjában foglaltak szerint kereskedőnek minősíti magát (továbbiakban: **Üzleti Felhasználó**);
* vagy nem minősíti magát kereskedőnek (továbbiakban: **Magánszemély** vagy **Magánszemély Felhasználó**). 

Az Üzleti Felhasználók a fiókregisztrációjuk során nyilatkoznak arról, hogy kereskedőnek minősülnek és elfogadják a jelen ÁSZF rendelkezéseit. A magukat kereskedőnek minősítő felhasználók ezen minősítéssel egyben arról is nyilatkoznak, hogy a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól szóló 45/2014. Korm. rendelet 11/A. § b) pontja szerint vállalkozásnak minősülnek. Az Üzleti Felhasználók Autó kategóriában Kereskedésként, Ingatlan kategóriában Ingatlanközvetítőként, állás, bolt és minden más, előzőekben nem felsorolt kategóriában Üzleti Felhasználóként jelennek meg. A Weboldalon az Üzleti Felhasználó köteles tájékoztatást nyújtani nyilvánosan elérhető profilján az alábbi adatok tekintetében: neve, székhelye, telefonszáma, e-mail címe és ezen adatok változása esetén köteles azt haladéktalanul, de legkésőbb a változás bekövetkeztétől számított 5 napon belül aktualizálni. Az Üzleti Felhasználók a Weboldalon meghirdetett áruikat, termékeiket, szolgáltatásaikat az Fttv. szerinti kereskedésként, továbbá a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól szóló 45/2014. Korm. rendelet szerinti vállalkozásként kínálja értékesítésre. Az Üzleti Felhasználó felel azért, hogy a hirdetésben vagy a fogyasztónak minősülő vevő Felhasználónak írt visszajelzésében eleget tegyen a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól szóló 45/2014. Korm. rendelet (a továbbiakban: **Kormányrendelet**) által megkövetelt fogyasztói jogokra vonatkozó tájékoztatásnak a fogyasztónak minősülő Magánszemély Felhasználók felé. A Társaság mindaddig jogosult megtagadni az Üzleti Felhasználótól a Szolgáltatás igénybevételét, ameddig

– a Weboldalon nem tesz eleget a szerződéskötés előtti tájékoztatás, a megfelelés és a termékbiztonságra vonatkozó tájékoztatás, így különösen a 45/2014. Korm. rendelet szerinti tájékoztatási kötelezettségének; illetve

– nem bocsátja a Társaság rendelkezésére nevét, székhelyét, telefonszámát, e-mail címét, az Üzleti Felhasználó képviseletében eljáró személy személyazonosító okmányának másolatát és adott esetben meghatalmazását, fizetési számlájának adatait, az Üzleti Felhasználót nyilvántartó nyilvántartás megjelölését és nyilvántartási számát (cégjegyzékszámát), valamint saját kiállítású tanúsítványát, amelyben vállalja, hogy csak olyan termékeket vagy szolgáltatásokat kínál a Weboldalon, amelyek megfelelnek az uniós jog alkalmazandó szabályainak.

Amennyiben a Társaság a Szolgáltatás Üzleti Felhasználó általi igénybevétele során észleli azt, hogy az Üzleti Felhasználó nem tesz eleget ezen tájékoztatási kötelezettségének, jogosult törölni (visszautasítani) az Üzleti Felhasználó hirdetéseit és felfüggeszteni a Szolgáltatás nyújtását mindaddig, ameddig az Üzleti Felhasználó nem tesz eleget a jelen Szabályzat szerinti kötelezettségének. A Társaság a felfüggesztésről tényéről és annak okáról ezen intézkedések alkalmazásának kezdetéig tájékoztatja az Üzleti Felhasználót. A Társaság kizárja a felelősségét azokért a károkért, igényekért, amelyek az Üzleti Felhasználó oldalán a tájékoztatási kötelezettsége elmulasztása miatt a Társaság által alkalmazott intézkedés következtében merülnek fel, az Üzleti Felhasználó köteles azonban a tájékoztatási kötelezettsége elmulasztásából fakadóan a Társaságnál felmerülő kár megtérítésére. A Magánszemély Felhasználó ezen minőségét a Weboldalon nem jeleníti meg külön a feladott hirdetéseknél.

A Felhasználó a Weboldal használatának megkezdésével, illetve regisztráció esetén a regisztráció során, továbbá a Szolgáltatások megrendelése során a Weboldalon tett kifejezett nyilatkozattal elfogadja és magára nézve kötelezőnek ismeri el a jelen Szabályzat, az annak részét képező Adatvédelmi Tájékoztató rendelkezéseit, továbbá kötelezettséget vállal a Szabályzat betartására. A Weboldalt minden Felhasználó kizárólag a saját kockázatára és felelősségére használhatja.

A Felhasználó a regisztráció során kifejezetten tudomásul veszi, hogy a Társaság a Felhasználó által megadott személyes adatokat, a Weboldal használata során begyűjtött felhasználói adatokat (például meglátogatott weboldalak listája, technikai adatok, a Weboldal Felhasználó által történő felhasználására vonatkozó információk, valamint esetlegesen harmadik személyektől beszerzett felhasználói információk) rögzíti és az Adatvédelmi Tájékoztatóban meghatározott módon és célokra felhasználja.

A Szolgáltatások vonatkozásában a Felhasználó és a Társaság között online szerződés jön létre, amely nem minősül írásbeli szerződésnek. A szerződéskötés nyelve kizárólag a magyar. Az online szerződést a Társaság nem rögzíti, így az a szerződéskötést követően nem érhető el. Az online szerződés létrejöttét az elektronikusan elmentett hirdetési adatok és az igénybe vett Szolgáltatásokra vonatkozó adatok igazolják. A Felhasználó által a Weboldalra feltöltött tartalmat a Felhasználó köteles archiválni.

A Társaság jogosult a jelen Szabályzat módosítására. A Társaság fenntartja magának a jogot arra, hogy az alábbiakban részletezett esetekben időről időre egészben vagy részben egyoldalúan módosítsa a Szolgáltatások tartalmát és díját, a Szolgáltatások igénybevételének feltételeit, a hirdetések közzétételének időtartamát. A Társaság a módosításról a nyitóoldalon, a Weblap felhasználási feltételeket tartalmazó részén, valamint egyes esetekben e-mail formájában tájékoztatja a Felhasználóit. A módosítás időpontját követően a Felhasználó bármely Szolgáltatás használatával, illetve megrendelésével egyben elfogadja a Szabályzat módosítását is. A Társaság egyoldalú módosításra jogosult különösen a jogszabályi környezet megváltozása esetén, az új szolgáltatások bevezetése, vagy a már meglévő szolgáltatások minőségének biztosítása céljából, az adatbázis minőségének biztosítása, vagy a hirdetés tartalmára vonatkozó alapfeltételek javítására szolgáló intézkedések megtétele céljából, vagy amennyiben azt gazdasági, műszaki, ill. egyéb körülményekben bekövetkezett lényeges változás azt indokolja. Amennyiben hirdető Felhasználó a módosított Szabályzat vagy díjak mellett nem kívánja tovább igénybe venni a Társaság Szolgáltatását, jogviszonyát jogosult megszüntetni, a Szabályzat rendelkezései szerint.

**Adatbázis kizárólagos felhasználási joga.** A Weboldalon (illetve az applikációkon keresztül) elérhető adatbázis előállítója a Társaság, így kizárólag a Társaság rendelkezik engedéllyel az adatbázis felhasználására, valamint arra, hogy harmadik személynek az adatbázis tekintetében felhasználási engedélyt adjon. A Társaság nem engedélyezi az általa előállított adatbázis egészének vagy annak bármely részének más weboldalon, vagy egyéb módon történő bármilyen üzleti célú felhasználását. 

**Megengedett felhasználás.** A Felhasználó a Weboldal (applikációk) használatának megkezdésével ráutaló magatartással elfogadja magára nézve kötelezőnek, hogy a Weboldalt (applikációt) kizárólag a jelen Szabályzat rendelkezéseivel összhangban, a Szolgáltatások igénybevételének és használatának céljára és az ahhoz szükséges mértékben használja és használhatja, továbbá Felhasználó a Weboldalt (applikációt) és az azon keresztül elérhető adatbázist kizárólag az alábbi nem kereskedelmi jellegű magáncélokra használhatja: i.) Weboldal megtekintése; ii.) Felhasználó termékeinek és szolgáltatásainak hirdetésére; iii.) Hirdetéshez kapcsolódó egyéb Társaság által nyújtott szolgáltatás igénybevételére; iv.) Online navigálás más honlapokra az e Weboldalon elhelyezett linkeken keresztül; v.) Az e Weboldalon alkalmasint rendelkezésre bocsátott egyéb funkciók használata. 

Automatizált rendszerek vagy automatikus szoftverek a Társaság által előállított adatbázisának az abból történő kereskedelmi célú adatgyűjtéshez (»screenscraping«) történő felhasználása tilos. E tilalom nem vonatkozik azokra az esetekre, amelyekben harmadik személyek olyan írásbeli felhasználási szerződést kötöttek közvetlenül a Társasággal, amely ezt a típusú adatgyűjtést harmadik személyek számára kizárólagosan lehetővé teszi.

**2.A Szolgáltatás igénybevétele**

A Weblap hirdetési Szolgáltatásai kizárólag felhasználói fiók létrehozásával érhetőek el. A Weblapon bizonyos Szolgáltatások ingyenesen, más Szolgáltatások ellenérték megfizetése mellett vehetők igénybe.

Társaság Szolgáltatásainak igénybevételéhez a Felhasználóknak olyan böngésző alkalmazással kell rendelkezniük, amelyet a Weboldal támogat. A jelenlegi támogatott böngészők a következők: Chrome, Firefox, Edge utoljára kiadott és azt megelőző verziói, Safari legutolsó verziója, továbbá minden olyan Magyarországon elérhető böngésző alkalmazás, amelyeknek a StatCounter alapján Magyarországon legalább 1%-os piaci részesedése van a nyilvános magyarországi trendek alapján. 

A Szolgáltatás igénybevételét megelőzően a vonatkozó gomb megnyomásával a Felhasználó megrendeli a kiválasztott Szolgáltatást. A Társaság a Felhasználó megrendelésének beérkezését követően a lehető legrövidebb időtartamon belül, de legkésőbb 48 órán belül automatikus aktivációs linket küld a Felhasználó részére, a Felhasználó által megadott e-mail címre, amivel a Társaság megerősíti a Felhasználó megrendelésének beérkezését (visszaigazoló e-mail). A nyilatkozatok akkor tekinthetők megérkezettnek, amikor az a címzett számára hozzáférhetővé válik. A megrendelés Társasághoz történő beérkezésével a Társaság és a Felhasználó közötti szerződés létrejön, ugyanakkor a megrendelt Szolgáltatásra vonatkozó szerződés akkor lép hatályba, ha a Felhasználó a visszaigazoló e-mailben Társaság által megadott aktivációs linkre kattintva megerősíti a Szolgáltatás igénybevételét (megrendelés megerősítése).

A Felhasználó megrendelésének megerősítését követően a Társaság az egyes Szolgáltatások leírásában meghatározott időtartamon belül biztosítja a Szolgáltatást, vagy a Társaság jogosult megtagadni a Szolgáltatás nyújtását és elállni a Szolgáltatásra vonatkozó szerződéstől, amennyiben (i) a Felhasználó fizetős  Szolgáltatást kíván igénybe venni, de a Társaság által megadott fizetési határidőn belül nem tesz eleget a fizetési kötelezettségének, vagy (ii) olyan körülmény merül fel, amely szerint a Felhasználó megrendelése bármilyen módon ütközik a jelen Szabályzat rendelkezéseivel. A Társaság a Szolgáltatás biztosításáról vagy megtagadásáról a Felhasználót e-mailben értesíti. A Szolgáltatás megtagadása esetén a Társaság jogosult a Felhasználóval szemben a jelen Szabályzat 4. pontjában foglalt rendelkezéseknek megfelelően eljárni.

A Felhasználó a Szolgáltatás igénybevétele, így többek között a hirdetés feltöltése érdekében önálló felhasználói fiókot köteles létrehozni regisztráció során, amelyet a Felhasználó által megadott e-mail cím, mint egyedi azonosító azonosít. A felhasználói fiók elérhető:

* a feltöltött hirdetés aktiválásáról a Társaság által küldött e-mailben megadott aktiváló felületre való belépéssel, vagy

* a Felhasználó által meghatározott, a felhasználói fiókot azonosító e-mail és jelszó megadásával.

A regisztráció elektronikus úton történt megküldésével és annak Társaság által történő visszaigazolásával felek között a Weboldal felhasználására és a Szolgáltatások igénybevételére (különösen hirdetések közzétételére) vonatkozó keretszerződés ráutaló magatartással jön létre. Az egyes Szolgáltatások megrendelésével létrejött egyedi szerződések a keretszerződés részét képezik. Amennyiben a Felhasználó fiókjában megadott adataiban változás történik, a Felhasználó e változásokat köteles személyes menüjében átvezetni, ennek elmaradásából eredő felelősség a Felhasználót terheli. A Felhasználó bármikor kezdeményezheti a felhasználói fiókja törlését, a törlés kezdeményezésének napjától számított egy hónapra (felmondási idő), amely a Társasággal létesített jogviszonya megszűnését eredményezi. A fiók törlésére vonatkozó felhasználói igényt a Társaság akkor teljesíti, ha a Felhasználónak nincs tartozása a Társaság felé. A felmondási idő alatt a Felhasználó felhasználói jogai szünetelnek, így Szolgáltatást nem vehet igénybe. A Felhasználó fiókregisztrációjának megszűnésével a Társaság és a Felhasználó között létrejött keretszerződés és az annak részét képező, megrendelt Szolgáltatások igénybevételére vonatkozó szerződések megszűnnek, a Felhasználó hirdetései a Weboldalról automatikusan eltávolításra kerülnek, többé nem elérhetőek.

Amennyiben a Társaság visszautasítja a hirdető Felhasználó hirdetésének feladását, akkor az adott hirdetés visszaaktíválhatatlan státuszba kerül, és a hirdetőnek újból fel kell adni a hirdetését a Társaság által megkövetelt feltételeknek megfelelően.

A regisztráció során, amennyiben a felhasználó telefonszámot is rögzíteni kíván a regisztrációs adatok között, a Társaság jogosult ellenőrizni a megadott mobiltelefonszám működését, a regisztráció hitelesítése érdekében. Az ellenőrzés a Társaság által a regisztráció során a Felhasználó részére megjelentetett azonosító kód Felhasználó által a Társaság által megadott, magyarországi mobilszolgáltató hálózatába tartozó (20-as, 30-as, 31-es, 50-es, 70-es előhívószámú) mobiltelefonszámra történő visszaküldésével valósul meg. Az üzenetküldés a Felhasználó által igénybe vett mobilszolgáltatás szerinti díjon történik, a Társaság nem téríti meg a regisztráció hitelesítésével felmerült díjat. 

A Társaság jogosult a Felhasználót a Weblap használatából kizárni és felfüggeszteni (tiltani) a Felhasználó fiókját, amennyiben kétséget kizáróan megállapítható, hogy a Felhasználó által megadott adatok köre nem valós elemeket is tartalmaz. A Társaság felé ezért a Felhasználó semmilyen megtérítési igénnyel nem élhet, azonban köteles a Társaság kárát megtéríteni. A felhasználói fiók Társaság általi törlése esetén a fiókhoz tartozó hirdetések a Weboldalról automatikusan eltávolításra kerülnek.

A Felhasználó és a Társaság között határozott időre létrejött jogviszony legkésőbb a szerződés tárgya szerinti Szolgáltatásra megállapított határozott idő elteltével szűnik meg. A hirdetési idő lejártával a hirdetés 179 napra archiválásra kerül, ezt követően a 180. napon véglegesen törlődik a Weboldalról, azt a Társaság a továbbiakban nem tárolja. A Felhasználó emellett bármikor kezdeményezheti az általa feltöltött hirdetések törlését az egyes hirdetések közzétételére vonatkozó határozott időtartam alatt. A hirdetés törlésének Felhasználó általi kezdeményezése esetén a hirdetés 14 napos időtartamra „törlés alatt” periódusba kerül, ezt követően a 15. napon véglegesen törlődik a Weboldalról, azt a Társaság a továbbiakban nem tárolja. Az archivált és „törlés alatt” álló hirdetések a Weboldal hirdetési felületén nem kerülnek harmadik személyek által hozzáférhetően közzétételre, megjelenítésre, azok kizárólag a hirdetést feladó Felhasználó részére hozzáférhetőek az archiválás időtartama alatt, illetve a törlés kezdeményezését követő negyedik napig.

A Felhasználó tudomásul veszi, hogy a felhasználói fiókja törlése esetén a Társaság a Felhasználó hirdetéseit nem köteles tárolni, azokat a saját belátása szerint jogosult törölni. A Társaság minden tőle telhetőt elkövet a Weblap és a Szolgáltatások folyamatos elérhetősége érdekében, azonban nem vállal kötelezettséget a Weboldal folyamatos működtetésére, a Szolgáltatás folyamatosságának biztosítására. A Társaság nem vállal felelősséget az olyan közvetlen, vagy közvetett károkért, amelyeket a Társaságtól független körülmények okoznak.

A Társaság nem vállal felelősséget a Weblapra feltöltött tartalmak, információk pontossága, megbízhatósága vonatkozásában, mivel a Weblap tartalma a Társaságtól függetlenül is módosul.

A Társaság a Szolgáltatásokhoz szorosan kapcsolódó, tájékoztató üzeneteket küld a Felhasználók e-mailcímére, bizonyos esetekben pedig a telefonkészülékére leküldéses értesítés (ún. push notification) formájában (rendszerüzenet), a Szolgáltatás teljesítésének részeként.

Visszterhes szolgáltatások esetén Társaság köteles évi 99%-os rendelkezésre állás mellett szerződésben vállalt Szolgáltatásainak biztosítására. A rendelkezésre állás számításába nem tartoznak bele a harmadik fél hatáskörébe tartozó szolgáltatások, valamint az előre bejelentett karbantartások.

**2.1. Ingyenesen igénybe vehető Szolgáltatások**

A regisztrált Felhasználók korlátlan számú hirdetést tölthetnek fel és szerkeszthetnek díjtalanul a Társaság által a Weboldalon biztosított hirdetési helyeken a jelen Szabályzatban foglalt feltételekkel és kivételekkel, amelyhez a Társaság keresőrendszert üzemeltet. A Felhasználók által feltöltött hirdetéseket a Társaság a feltöltéskor megadott régió és terméktípustól eltérő rendező-elvek szerint is csoportosíthatja, illetve közzéteheti. A Weboldalon biztosított valamennyi hirdetési Szolgáltatás igénybevétele Felhasználó részéről regisztrációhoz kötött, azaz csak felhasználói fiók létrehozása esetén lehetséges.

A Weboldalt látogató, felhasználói fiókkal nem rendelkező Felhasználók jogosultak a Weboldalon elhelyezett hirdetések között böngészni, szűrők alkalmazásával a keresést szűkíteni, valamint amennyiben a hirdetést feltöltő Felhasználó azt közzétette, a hirdetésben megjelenő telefonszámon a hirdető Felhasználóval kapcsolatba lépni.  

A Felhasználók által legutoljára megtekintett 8 hirdetés a könnyebb visszakereshetőség érdekében megjelenítésre kerül a listázási oldal alján.

Az Ingatlan kategóriában a hirdetés feladásának feltétele, hogy a hirdető megadja mobil telefonszámát, valamint a telefonszámot megerősítse úgy, hogy a megadott telefonszámra a Weboldal üzemeltetője által küldött automatikus SMS üzenetre válaszol.

A Szabályzatnak megfelelő felhasználói ajánlattételben foglalt hirdetéseket a Társaság a feladásra szolgáló gomb Felhasználó által történő használatát követően a lehető legrövidebb időn, legfeljebb 48 órán belül közzéteszi és 90 nap időtartamra elérhetővé teszi a Weboldalon. Amennyiben a Felhasználó a közzétett apróhirdetésre vonatkozóan a 2.2.1. (i) – (iv) pontokban, illetve a 2.2.2. (i)-(v) meghatározott Szolgáltatásokat vásárolja meg, a 90 napos közzétételi időtartam a Szolgáltatásokkal érintett apróhirdetések esetén a vásárlás napjától számítva újraindul. Ettől eltérően a jármű kategóriában eredetileg a Használtautó Weboldalon feladott egyedi szerződéssel hirdető Felhasználókhoz tartozó Hirdetések esetében a Társaság addig az időtartamig teszi közzé a hirdetést a Weboldalon, amíg az a Használtautó Weboldalon szerepel.

Felhasználónak a hirdetésfeladáskor félbehagyott hirdetés adatait Társaság elmenti. Ha a Felhasználó 72 órán belül újra megnyitja a hirdetésfeladást, akkor a Társaság felajánlja neki, hogy folytassa a félkész hirdetését. Felhasználó mindig csak a legutolsó hirdetést tudja folytatni, a felajánlás csak egyszer jelenik meg, ha elveti a piszkozatot, akkor többet nem tudja folytatni, és a hirdetést csak az adott böngészőben tudja megnyitni.

A Felhasználó által a Weboldalra feltöltött hirdetés a határozott hirdetési időtartam elteltével archiválásra kerül. Az archiválással a hirdetés a Weboldal hirdetési felületéről lekerül, azonban az archivált hirdetéseket a Társaság 179 napos határozott ideig megőrzi a Felhasználó részére a felhasználói fiókban. A 179 napos időtartam elteltével, a 180. napon az archivált hirdetés automatikusan eltávolításra, törlésre kerül a felhasználói fiókból is.

Az archiválás időtartama alatt a Felhasználó által a hirdetés vonatkozásában a 2.2.1 (i), (ii), (iv) és a 2.2.2 (ii), (iii), (v) Szolgáltatások igénybevétele esetén, a hirdetés aktiválódik, és 90 nap időtartamra ismét elérhetővé válik a Weboldalon.

A törlés kezdeményezésére vonatkozó funkció használatával a hirdetés 14 napra törlés alá kerül, amelynek leteltét követően, a 15. napon, a hirdetés automatikusan törlésre kerül. A „törlés alatti” hirdetések a Weboldal hirdetési felületéről a törlés kezdeményezésével egyidejűleg lekerülnek, azonban azok a törlés kezdeményezését követő negyedik napig a „törlés alatt” státuszú hirdetések között láthatók maradnak a Felhasználó számára a felhasználói fiókban. A Felhasználónak a törlés kezdeményezését követő negyedik napig van lehetősége a „törlés alatti” hirdetések kapcsán a 2.2.1 (i), (ii), (iv) és a 2.2.2 (ii), (iii), (v) Szolgáltatások bármelyikének igénybevételére, amely esetén a hirdetés aktiválódik, és 90 nap időtartamra ismét elérhetővé válik a Weboldalon. A törlés kezdeményezését követő ötödik naptól a hirdetés aktiválására nincs lehetőség és a Felhasználó ugyanazt a terméket, szolgáltatást vagy állást csak a törlési időszak leteltét követően hirdetheti meg a Weblapon.

A Társaság által nyújtott ingyenes Szolgáltatás továbbá a hirdetésekre való jelentkezés lehetővé tétele a hirdetővel való kapcsolatfelvétel biztosítása útján, a hirdető által a hirdetésben megadott elérhetőségeken, valamint a jelen Szabályzatban meghatározott egyéb módokon. A hirdetésekre való jelentkezés – az Ingatlan kategóriában feladott **Díjköteles** Hirdetések és előfizetéssel rendelkező Felhasználók hirdetései, az Álláshirdetések, valamint Jármű kategóriában eredetileg a Használtautó Weboldalon feladott **Díjköteles** Hirdetések esetében a jelen bekezdés végén szereplő eltérésekkel – a Társaság által üzemeltetett üzenetküldő rendszeren keresztül is történhet, a hirdetésben szereplő telefonszámon történő kapcsolatfelvétel mellett. Az üzenetküldő rendszer használatának feltételei: a Weboldalon történő regisztráció és felhasználói fiók létrehozása, valamint a fentiek szerinti felhasználói regisztráció telefonszámos hitelesítése. Az üzenetküldő rendszeren küldött üzenetek csak a Weboldal felületén tekinthetők meg. Új üzenet érkezéséről a Felhasználó ingyenesen e-mail és push értesítőt kap. A Weboldal üzenetküldő rendszerén keresztül folytatott kommunikációt a Társaság jogosult, de nem köteles ellenőrizni annak érdekében, hogy az csak a feladott hirdetésekkel kapcsolatos kommunikációra terjedjen ki, továbbá, hogy annak hangneme ne legyen sértő, bántó, illetve uszító, valamint az üzenetküldő rendszert ne használják csalásra vagy bárkinek a jogát, jogos érdekét sértő módon. Kommunikáció alatt érteni kell többek között: írásbeli üzenetet, fényképet, videót, illetve az üzenetben elküldött bármely csatolmányt, azzal, hogy abba beleértendő valamennyi tartalom, amelyet a Felhasználók az üzenetküldő rendszerbe feltöltenek. Az üzenetküldő rendszerben a Felhasználók az egyes funkciókat a saját döntésüktől függően használhatják, amely során bizonyos funkciók (például helymeghatározás) használatához – ilyen módon pedig az üzenetküldő szolgáltatás adott funkciójának igénybevételéhez, valamint ezen funkcióval kapcsolatos szolgáltatás teljesítéséhez – elengedhetetlenül szükséges, hogy a Felhasználó a személyes adatait megadja Társaságnak, amely személyes adatokat a Társaság az Adatvédelmi Tájékoztatójának megfelelő módon kezel.    A Társaság nem felelős az üzenet elérhetetlenné tételéből eredő bármely kárért, amennyiben az a Szabályzat rendelkezéseinek megfelelően történt. A Jófogás Bolt és az Ingatlan kategóriában feladott, előfizetéssel rendelkező Felhasználók hirdetései esetén az üzenetküldés azzal az eltéréssel működik, hogy a hirdetésre jelentkezni kívánó felhasználó első üzenetének elküldésével vállalja, hogy saját regisztrált e-mail címét és telefonszámát, valamint üzenete tartalmát az üzenetküldő rendszer elküldi a hirdető részére, a hirdető e-mail címére. Ezt követően az üzenetváltások az üzenetküldő rendszeren kívül, az érintett felhasználók e-mail fiókján keresztül vagy telefonon történhetnek. Egyebekben az első üzenet elküldésére a jelen bekezdés rendelkezései vonatkoznak. A Jófogás Bolt kategóriában történő hirdetésfeladás során a hirdetők választhatják azt az opciót is, hogy a hirdetésre jelentkezni kívánó felhasználók számára a kapcsolatfelvétel lehetőségét nem a Weboldal üzenetküldő rendszerén keresztül, hanem a hirdetés mellett elhelyezett “Irány a bolt” gomb segítségével a hirdető által üzemeltetett külső weboldalra történő átirányítás útján biztosítják. Ebben az esetben a hirdetésre jelentkező felhasználók és a Jófogás Bolt hirdetője között már az első, és ezt követő valamennyi üzenetváltás is a Weboldal üzenetküldő rendszerén kívül történik, a felhasználók által a Weboldalon megadott személyes adatok, ideértve az e-mail címet és telefonszámot is, nem kerülnek továbbításra. Az „Irány a bolt” gomb megnyomását követően az üzenetváltásra és adatkezelésre a hirdető saját külső weboldalának felhasználási szabályzata és adatkezelési tájékoztatója alkalmazandó.

Az Ingatlan kategóriában feladott egyéb **Díjköteles** Hirdetések és Jármű kategóriában eredetileg a Használtautó Weboldalon egyedi szerződés alapján feladott Hirdetések esetén a Weboldal üzenetküldő rendszere nem használható, ezen hirdetések esetében a hirdető és a hirdetésre jelentkező felhasználó közötti kommunikáció ezen kívül a hirdető által az apróhirdetésben megadott mobiltelefonos elérhetőségen keresztül, szóban vagy sms útján lehetséges.

Az Állás kategóriában történő hirdetésfeladás során az álláshirdetési szolgáltatásra előfizetési szerződéssel rendelkező Felhasználók a hirdetésre jelentkezni kívánó felhasználók számára a kapcsolatfelvétel lehetőségét a következő lehetőségek közül legfeljebb háromnak a hirdetésfeladási felületen keresztül történő kiválasztásával biztosíthatják: telefonos kapcsolatfelvétel, személyes kapcsolatfelvétel a hirdető telephelyén, üzenet továbbítása a hirdető fiókjához tartozó e-mail címére, vagy a hirdető weboldalára mutató link közzététele útján. Az üzenetküldő rendszeren keresztül ebben a kategóriában a kapcsolatfelvétel nem lehetséges.

Az álláshirdetési szolgáltatásra előfizetéssel nem rendelkező Felhasználók hirdetései esetén a fenti kapcsolatfelvételi lehetőségek azzal az eltéréssel biztosíthatók, hogy lehetőség van az üzenetküldő rendszer használatára, a telefonos kapcsolatfelvételre, valamint a hirdető e-mail címének publikussá tételével lehetővé teheti a hirdetésre jelentkező felhasználók részére az e-mailen történő közvetlen kapcsolatfelvételt, a hirdető weboldalára mutató link azonban nem helyezhető el kapcsolatfelvételi lehetőségként, valamint nincs lehetőség üzenet továbbítására a hirdető fiókjához tartozó e-mail címére.

A Társaság által üzemeltetett üzenetküldő rendszer rendeltetésszerű célja, hogy segítse a Felhasználókat abban, hogy biztonságos módon, valós időben tudjanak egymással kommunikálni a feladott hirdetésekkel kapcsolatban. Amennyiben a Társaság észleli, hogy a Felhasználó a Weboldal üzenetküldő rendszerén folytatott kommunikációja nem felel meg a jelen Szabályzatnak, jogellenes, jó erkölcsbe ütközik, vagy egyébként a Társaság megítélése szerint az nem kizárólag a feladott hirdetésekkel kapcsolatos kommunikációra terjed ki, vagy annak hangneme sértő, bántó, illetve uszító, a Társaság jogosult a Felhasználó Weblaphoz való hozzáférését korlátozni, így a Felhasználót a Weblap használatából kizárni és felfüggeszteni (tiltani) a Felhasználó fiókját, illetve az ilyen tartalmú kommunikációt felfüggeszteni, annak a címzett részére történő kézbesítését meghiúsítani. Az „Üzenet jelentése” funkció lehetővé teszi a felhasználók számára, hogy a kéretlen tartalmat, így különösen a sértő, zaklató vagy gyűlöletkeltő üzeneteket, spam vagy reklám célú üzeneteket, illetve az egyéb, a jelen felhasználási feltételekbe ütköző üzeneteket bejelentsék a Társaság felé. Amennyiben a bejelentés megalapozottnak bizonyul, a Társaság a jelen bekezdés szerinti szabályok szerint jár le.

A Felhasználók korlátlan számú hirdetést (kivéve az Álláshirdetések, Üzlet-szolgáltatás hirdetések, Autó és Motor, robogó hirdetések, Ingatlan hirdetések kategóriát, valamint a tűzifa hirdetési alkategóriát amelynek részletes szabályait a lenti 2.2(vi) pont tartalmazza) tölthetnek fel és szerkeszthetnek díjtalanul a Társaság által a Weboldalon biztosított hirdetési helyeken, amelyhez a Társaság keresőrendszert üzemeltet.

Amennyiben a Felhasználó a Weboldalon jármű hirdetést ad fel, akkor ennek feladásakor a Felhasználó az erre vonatkozó mező jelölésével jogosult megrendelni a hirdetése feladását a [www.hasznaltauto.hu](http://www.hasznaltauto.hu/) weboldalon (a továbbiakban: Használtautó Weboldal) is, a Használtautó Weboldalon meglévő fiókjába történő belépési adatainak megadásával. A Használtautó Weboldalon közzétett hirdetésekre nem a jelen Szabályzat, hanem a Használtautó Weboldal Általános Szerződési Feltételei irányadók, amely a jelen mondatban szereplő [Általános Szerződési Feltételek](https://www.hasznaltauto.hu/aszf) szóösszetételre, mint linkre kattintva érhető el.

**2.2. Ellenérték megfizetése mellett igénybe vehető Szolgáltatások**

Az ellenérték megfizetése fejében biztosított Szolgáltatások igénybevételéért fizetendő díjakat, a díjfizetési lehetőségeket és az ellenérték fejében nyújtott Szolgáltatások megvásárlásának menetére vonatkozó részletes szabályokat a hirdetés kezelése oldal, illetve a megrendelés menetébe építve, az egyes termékek leírása tartalmazza.

A díjakat a Felhasználó a Barion Payment Zrt. által nyújtott fizetési szolgáltatások segítségével, a barion.hu oldalon keresztül egyenlítheti ki. A bankkártyával, illetve Barion tárcával történő fizetés során felmerülő esetleges hibákért a Társaság nem vállal felelősséget. A vásárlást megerősítő automatikus visszaigazoló e-mailt a Társaság a Felhasználó által megadott e-mail címre küldi meg. A Barion szolgáltatással történő díjfizetésről a következő linkeken tájékozódhat bővebben: [VÁSÁRLÓI TÁJÉKOZTATÓ A BARION SZOLGÁLTATÁSSAL TÖRTÉNŐ FIZETÉSRŐL](http://www.barion.hu/).

A vásárlás a “Fizetés” gomb megnyomása előtt bármikor, következmények nélkül megszakítható, a Felhasználó elállhat a szerződéstől. A megfizetett díjról a Társaság, elektronikus számlát állít ki, melyet a Felhasználó által megadott e-mail címre küld meg. A Felhasználó visszavonhatatlanul hozzájárul a számla elektronikus formátumban való kiállításához. A Társaság a számviteli bizonylatokat a számvitelről szóló 2000. évi C. törvény 169.§ (2) bekezdése alapján 8 évig köteles őrizni.

Számlamódosítási igényt a Társaság maximum a számlakibocsátás dátumától számított 30 napon belül fogad el.

A jelen pont szerinti Szolgáltatások a 2.1. szerinti Szolgáltatáshoz kapcsolódóan vehetők igénybe, és megszűnnek a rájuk meghatározott időtartam elteltével, továbbá a jelen Szabályzatban meghatározott egyéb, a jogviszonyok megszűnését eredményező esemény bekövetkezte esetén is. A Társaságnak nem felróható okból történő megszűnés esetén a Felhasználó nem jogosult semmilyen megtérítési igényt támasztani a Társaság felé, a jelen Szabályzatban kifejezetten megjelölt esetleges megtérítések kivételével.

Amennyiben a megszűnésre azért kerül sor, mert a jelen pont szerinti Szolgáltatás a Szabályzattal ellentétes hirdetéshez kapcsolódóan került megrendelésre és a hirdetést a Társaság eltávolítja, ez a Felhasználónak felróható okból történő megszüntetésnek minősül, és erre tekintettel a Felhasználó nem jogosult a már megfizetett szolgáltatási ellenérték megtérítésére.

Az ellenérték megfizetése fejében biztosított Szolgáltatások igénybevételéért fizetendő díjakat, a díjfizetési lehetőségeket és az ellenérték fejében nyújtott Szolgáltatások megvásárlásának menetére vonatkozó részletes szabályokat a hirdetés kezelése oldal, illetve a megrendelés menetébe építve, az egyes termékek leírása tartalmazza. A Magánhirdetések és a **Díjköteles** Hirdetések kiegészítő Szolgáltatásainak díja eltérhet egymástól.

A keresési funkcióhoz kapcsolódóan Társaság linkről elérhető formában közzéteszi azt a leírást, amely tartalmazza a találati listában megjelenő hirdetések rangsorolásának általános szempontjait. A hirdetést feladó Felhasználóknak lehetőségük van fizetni a jelen Szabályzat 2.2.1. és 2.2.2. pontjaiban meghatározott Szolgáltatások igénybevételével azért, hogy ezen rangsoroláshoz képest a hirdetésük előresorolva, kiemelve jelenjen meg, amely tényt Üzemeltető a hirdetésen világos kék “K” jelzéssel lát el. Amennyiben a Felhasználó rákattint a “K” jelzésre, akkor megjelenik számára egy pop-up üzenet, ami tájékoztatja a “K” jel pontos jelentéséről Felhasználót.

**2.2.1 A Weblapra ingyenesen feltöltött hirdetések vonatkozásában –**

Álláshirdetések, Üzlet-szolgáltatás hirdetések, Autó és Motor, robogó hirdetések, Ingatlan hirdetések, tűzifa hirdetések kivételével, amelyekre az alábbi 2.2.2 pont vonatkozik **– az alábbi Szolgáltatások vehetők igénybe, ellenérték megfizetése mellett:**

**(i) Kirakat kiemelés**

A jelen pont szerinti Szolgáltatás megvásárlásával a Felhasználó feltöltött hirdetése (a) a Weblapon történő közzétételtől, vagy már közzétett hirdetés esetén a Szolgáltatás aktiválásától számított 7 (hét) napon keresztül a hirdetés régiója és tárgya (kategóriája) szerinti keresési lista találati oldalán, a találati lista első szakaszában kerül közzétételre, az alábbi kiválasztási elvek szerint: keresésben szereplő régiónak (vagy Országos és Multirégiós, valamint Háztól-Házig illetve HDT Szolgáltatás igénybe vételével feladott hirdetés esetén akár más régiónak, kivéve, ha ezt a lehetőséget a a Háztól-Házig, illetve a HDT Szolgáltatás esetében a kereső használója kizárta) és tárgyának megfelelő, Kirakat kiemeléssel feladott hirdetések az aktuálisan legfrissebb hirdetésfeladási dátummal ellátott hirdetéstől indulva, sorban jelennek meg, továbbá (b) a hirdetés kategóriája szerinti lista első helyére sorolódik, a (iv) pontban foglalt szabályok szerint.  Amennyiben nincs a keresésnek megfelelő pontos találat, akkor egyéb kiemeléssel érintett hirdetések jelennek meg a fenti módon.

A Felhasználó a Weblapon található hirdetéseim menüpontban szereplő statisztikában tájékozódhat arról, hogy az adott időpontig hányszor jelent meg a Kirakat kiemelés szolgáltatással feladott hirdetése az első szakaszban, a fenti módon. A Szolgáltatás kizárólag olyan hirdetések vonatkozásában elérhető, amelyek az hirdetés tárgyáról tartalmaznak képet. A Társaság a Szolgáltatás nyújtását a fizetést követően, a sikeres fizetés megerősítésének Társasághoz érkezését követően 24 órán belül megkezdi. A Kirakat kiemelés szolgáltatás az alábbi helyeken vásárolható meg az oldalon bankkártyás fizetés segítségével: hirdetésfeladás oldal, hirdetésfeladást követő visszaigazoló oldalon, a Felhasználó fiókjából. Amennyiben a Felhasználó korábban feladott hirdetése vonatkozásában a jelen pont szerinti Szolgáltatást megrendeli, a 2.1 pontban foglalt ingyenes Szolgáltatásra meghatározott határozott időtartam a jelen pont szerinti Szolgáltatás nyújtásának megkezdésével újraindul.

**(ii) Szalag kiemelés**

A jelen pont szerinti Szolgáltatás megvásárlásával a Felhasználó feltöltött hirdetése a Weblapon történő közzétételtől, vagy már közzétett hirdetés esetén a Szolgáltatás aktiválásától számított 30 (harminc) napon keresztül a Weboldalon, a hirdetés jobb felső sarkában elhelyezett, narancssárga színű szalaggal kerül közzétételre, amelyen a Felhasználó által a Társaság által a Szolgáltatás leírásában előre meghatározott és elérhető alapértelmezett szöveg közül kiválasztott szöveg kerül felvezetésre. Valamint az ilyen hirdetések a Szolgáltatás aktiválásának napján, és az aktiválástól számított 2. (második), 3. (harmadik) és 15. (tizenötödik) napon a hirdetés régiójában közzétett lista Kirakatkiemeléssel feladott hirdetéseit követő szakaszába sorolódik 24 órára. A Szalaggal ellátott hirdetések a Kirakathirdetéseket követően, az Automatikus előresorolással ellátott hirdetésekkel együtt, más kiemeléssel érintett vagy kiemelés nélkül feladott hirdetéseket megelőzik a találati listában.

Az előresorolt  hirdetést az adott  kategórián belül később feladott más hirdetések lejjebb sorolják. A Társaság a Szolgáltatás nyújtását a fizetést követően, a sikeres fizetés megerősítésének Társasághoz érkezését követően 24 órán belül megkezdi. Amennyiben a Felhasználó korábban feladott hirdetése vonatkozásában a jelen pont szerinti Szolgáltatást megrendeli, a 2.1 pontban foglalt ingyenes Szolgáltatásra meghatározott határozott időtartam a jelen pont szerinti Szolgáltatás nyújtásának megkezdésével újraindul.

**(iii) Automatikus előresorolás**

A jelen pont szerinti Szolgáltatás megvásárlásával a Felhasználó a feltöltött hirdetése vonatkozásában megválaszthatja, hogy a hirdetés a) a Weblapon történő közzétételtől, vagy már közzétett hirdetés esetén a Szolgáltatás aktiválásától számított 5 (öt) egymást követő héten keresztül, hetente egy, a Felhasználó által kiválasztott napon és időszakban, vagy b) 5 (öt) egymást követő napon keresztül, naponta egy alkalommal, a Felhasználó által megadott napon és napszakban, a hirdetés régiójában közzétett lista Kirakatkiemeléssel feladott hirdetéseit követő szakaszába sorolódjon 24 órára. Az Automatikus előresorolással ellátott hirdetések a Kirakathirdetéseket követően, a Szalag kiemeléssel ellátott hirdetésekkel együtt, más kiemeléssel érintett vagy kiemelés nélkül feladott hirdetéseket megelőzik a találati listában.

Az Automatikus előresorolással előresorolt hirdetést a hirdetésfeladást követően feladott más hirdetések lejjebb sorolják. A jelen pont szerinti Szolgáltatás megrendelésével a 2.1 pontban foglalt ingyenes Szolgáltatás határozott időtartama az automatikus előresorolás szolgáltatás nyújtásának megkezdésével újraindul. A Társaság a Szolgáltatás nyújtását a fizetést követően, a sikeres fizetés megerősítésének Társasághoz érkezését követően 24 órán belül megkezdi.

**(iv) Azonnali előresorolás / Megújítás**

A jelen pont szerinti Szolgáltatás megvásárlásával a Felhasználó feltöltött hirdetése a Szolgáltatás ellenértékének megfizetését, a sikeres fizetés megerősítésének Társasághoz érkezését követő lehető legrövidebb időn, de maximum 24 órán belül, az adott hirdetés kategóriája szerinti lista Kirakatkiemeléssel, Szalag kiemeléssel vagy Automatikus előresorolással feladott hirdetéseit követő szakaszába sorolódik 24 órára. Az Azonnali előresorolással előresorolt hirdetést az elhelyezést követően feladott más hirdetések lejjebb sorolják. Amennyiben a Felhasználó korábban feladott hirdetése vonatkozásában a jelen pont szerinti Szolgáltatást megrendeli, a 2.1 pontban foglalt ingyenes Szolgáltatásra meghatározott határozott időtartam a jelen pont szerinti Szolgáltatás nyújtásának megkezdésével újraindul.

**(v) Országos és Multirégiós Hirdetés**

A jelen pont szerinti Szolgáltatás megvásárlásával a **Díjköteles** hirdetést feladó Felhasználó a feltöltött hirdetése vonatkozásában megválaszthatja, hogy a hirdetés a Felhasználó által elsődlegesen megválasztott régió mellett a) az egész országra vonatkozó keresés esetén vagy b) több, a Felhasználó által megválasztott további régióra (egy-egy megyére vagy Budapestre, de nem egyes településekre vagy budapesti kerületekre) vonatkozó keresés esetén is bekerüljön a találati listába. Amennyiben a Felhasználó meg kívánja szüntetni a jelen pont szerinti Szolgáltatás alkalmazását, azt az Ügyfélszolgálat segítségével teheti meg (az alábbi 8. pontban szereplő elérhetőségeken).

**2.2.2**

**A Weblapra feltöltött Álláshirdetések, Üzlet-szolgáltatás hirdetések, Autó és Motor, robogó hirdetések, Ingatlan hirdetések, tűzifa hirdetések vonatkozásában az alábbi Szolgáltatások vehetők igénybe, ellenérték megfizetése mellett:**

**(i) Álláshirdetések, Üzlet-szolgáltatás hirdetések, Autó és Motor, robogó hirdetések, Ingatlan hirdetések, tűzifa hirdetések és ezen hirdetések szerkesztése**

A Felhasználó Álláshirdetés, Üzlet-szolgáltatás hirdetés, Autó és Motor, robogó hirdetés és Ingatlan hirdetés kategóriában valamint tűzifa hirdetés alkategóriában díjfizetés ellenében adhat fel Magánhirdetést, **Díjköteles** Hirdetést. Álláshirdetés kategóriában a hirdetésfeladás feltétele a bruttó bér megadása (vagy a Jelentkezés bérigény megjelölésével opció kiválasztása).

A jelen 2.2.2. pontban szereplő valamennyi hirdetés kategóriában a hirdetés leírásának (rövid leírás, bővebb leírás), illetve hirdetés paraméterei, hirdetés adatai (ár, bruttó bér, jelentkezés bérigénnyel), cím (irányítószám) Felhasználó által kezdeményezett módosítása (ide nem értve a képek szerkesztését, valamint a kapcsolat rész adatait, és a kapcsolatfelvételi módok változtatását), a hirdetés első megjelenésétől számított 72 órán túl díjfizetéshez kötött, díjfizetéses módosítás esetén a hirdetés azonnal előresorolódik a találati listában. A hirdetés kategóriájának módosítása új hirdetés feladásának minősül, ezért arra vonatkozóan díjfizetéshez kötött hirdetési kategóriába történő átsorolás esetén az eredeti hirdetés első megjelenésének időpontjától függetlenül az új kategóriára vonatkozó hirdetési díj fizetendő. 

A **Díjköteles** Hirdetésnek minősülő hirdetések vonatkozásában a 2.2. pont szerinti kiegészítő szolgáltatás igénybevétele esetén a hirdetési díjon felül fizetendő a kiegészítő szolgáltatás Díjköteles Hirdetésekre irányadó díja.

Egyebekben a Díjköteles hirdetésekre az ingyenes szolgáltatásokra vonatkozó, 2.1 pontban meghatározott szabályok irányadóak.

**(ii) Kirakat kiemelés**

A jelen pont szerinti Szolgáltatás megvásárlásával a Felhasználó feltöltött ingatlan-, állás- , autó-, üzlet-szolgáltatás- vagy tűzifa hirdetése (a) a Weblapon történő közzétételtől, vagy már közzétett hirdetés esetén a Szolgáltatás aktiválásától számított 7 (hét) napon keresztül az adott hirdetés régiója és tárgya (kategóriája) szerinti keresési lista találati oldalán, a találati lista “Top hirdetést” követő első szakaszában, az egyéb kiemelési kategóriákba tartozó, vagy kiemelés nélkül feladott hirdetéseket megelőzően kerül közzétételre, az alábbi kiválasztási elvek szerint: (a) keresésben szereplő régiónak és tárgyának megfelelő, Kirakat kiemeléssel feladott ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetések az aktuálisan legfrissebb hirdetésfeladási dátummal ellátott hirdetéstől indulva, sorban jelennek meg a fenti módon, továbbá (b) az adott ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetés kategóriája szerinti lista első helyére sorolódik, az (v) pontban foglalt szabályok szerint. Amennyiben nincs a keresésnek megfelelő pontos találat, akkor egyéb kiemeléssel érintett hirdetések jelennek meg a fenti módon.

A „Top hirdetés” az Ingatlan, Autó, Motor és Kishaszongépjármű kategória valamennyi találati listájában a Társaság és a vele egyedi megállapodást kötő ingatlanközvetítők között létrejött megállapodás alapján, az abban meghatározottak szerint, az adott keresésnek megfelelő régióban, de a keresés egyéb feltételeitől függetlenül első helyen megjelenő, kiemelt hirdetés.

A Felhasználó a Weblapon található hirdetéseim menüpontban szereplő statisztikában tájékozódhat arról, hogy az adott időpontig hányszor jelent meg a Kirakat kiemelés szolgáltatással feladott hirdetése az első szakaszban, a fenti módon. A Szolgáltatás kizárólag olyan hirdetések vonatkozásában elérhető, amelyek az hirdetés tárgyáról tartalmaznak képet. A Társaság a Szolgáltatás nyújtását a fizetést követően, a sikeres fizetés megerősítésének Társasághoz érkezését követően 24 órán belül megkezdi. A Kirakat kiemelés szolgáltatás az alábbi helyeken vásárolható meg az oldalon bankkártyás fizetés segítségével: hirdetésfeladás oldal, hirdetésfeladást követő visszaigazoló oldalon, a Felhasználó fiókjából. Amennyiben a Felhasználó korábban feladott hirdetése vonatkozásában a jelen pont szerinti Szolgáltatást megrendeli, a 2.1 pontban foglalt ingyenes Szolgáltatásra meghatározott határozott időtartam a jelen pont szerinti Szolgáltatás nyújtásának megkezdésével újraindul.

**(iii) Szalag kiemelés**

A jelen pont szerinti Szolgáltatás megvásárlásával a Felhasználó feltöltött hirdetése a Weblapon történő közzétételtől, vagy már közzétett hirdetés esetén a Szolgáltatás aktiválásától számított 30 (harminc) napon keresztül a Weboldalon, a hirdetés jobb felső sarkában elhelyezett, narancssárga színű szalaggal kerül közzétételre, amelyen a Felhasználó által a Társaság által a Szolgáltatás leírásában előre meghatározott és elérhető alapértelmezett szöveg közül kiválasztott szöveg kerül felvezetésre. Valamint az ilyen ingatlan-, állás-, autó-, üzlet-szolgáltatás vagy tűzifa hirdetések a Szolgáltatás aktiválásának napján, és az aktiválástól számított 2. (második), 3. (harmadik) és 15. (tizenötödik) napon az ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetés régiójában közzétett lista Top hirdetést, és a Kirakatkiemeléssel feladott hirdetéseit követő szakaszába sorolódik 24 órára. Az előresorolt  ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetést az adott  kategórián belül később feladott más hirdetések lejjebb sorolják. A Szalaggal ellátott hirdetések a Kirakathirdetéseket követően, az Automatikus előresorolással ellátott hirdetésekkel együtt, más kiemeléssel érintett vagy kiemelés nélkül feladott hirdetéseket megelőzik a szűrt találati listában.

A Társaság a Szolgáltatás nyújtását a fizetést követően, a sikeres fizetés megerősítésének Társasághoz érkezését követően 24 órán belül megkezdi. Amennyiben a Felhasználó korábban feladott hirdetése vonatkozásában a jelen pont szerinti Szolgáltatást megrendeli, a 2.1 pontban foglalt ingyenes Szolgáltatásra meghatározott határozott időtartam a jelen pont szerinti Szolgáltatás nyújtásának megkezdésével újraindul.

**(iv) Automatikus előresorolás**

A jelen pont szerinti Szolgáltatás megvásárlásával a Felhasználó a feltöltött ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetése vonatkozásában megválaszthatja, hogy a hirdetés a) a Weblapon történő közzétételtől, vagy már közzétett hirdetés esetén a Szolgáltatás aktiválásától számított 5 (öt) egymást követő héten keresztül, hetente egy, a Felhasználó által kiválasztott napon és időszakban, vagy b) 5 (öt) egymást követő napon keresztül, naponta egy alkalommal, a Felhasználó által megadott napon és napszakban, a hirdetés régiójában közzétett lista Top hirdetést, illetve Kirakatkiemeléssel feladott hirdetéseit követő szakaszába sorolódjon 24 órára. Az Automatikus előresorolással ellátott ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetések a Top hirdetést és a Kirakathirdetéseket követően, a Szalag kiemeléssel ellátott ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetésekkel együtt, más kiemeléssel érintett vagy kiemelés nélkül feladott ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetéseket megelőzik a találati listában.

Az Automatikus előresorolással előresorolt ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetést a hirdetésfeladást követően feladott más hirdetések lejjebb sorolják. A jelen pont szerinti Szolgáltatás megrendelésével a 2.1 pontban foglalt ingyenes Szolgáltatás határozott időtartama az automatikus előresorolás szolgáltatás nyújtásának megkezdésével újraindul. A Társaság a Szolgáltatás nyújtását a fizetést követően, a sikeres fizetés megerősítésének Társasághoz érkezését követően 24 órán belül megkezdi.

**(v) Azonnali előresorolás / Megújítás**

A jelen pont szerinti Szolgáltatás megvásárlásával a Felhasználó feltöltött ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetése a Szolgáltatás ellenértékének megfizetését, a sikeres fizetés megerősítésének Társasághoz érkezését követő lehető legrövidebb időn, de maximum 24 órán belül, az adott hirdetés kategóriája szerinti lista Top hirdetést, illetve Kirakatkiemeléssel, Szalag kiemeléssel vagy Automatikus előresorolással feladott hirdetéseit követő szakaszába sorolódik 24 órára. Az Azonnali előresorolással előresorolt ingatlan-, állás-, autó-, üzlet-szolgáltatás- vagy tűzifa hirdetést az elhelyezést követően feladott más hirdetések lejjebb sorolják. Amennyiben a Felhasználó korábban feladott hirdetése vonatkozásában a jelen pont szerinti Szolgáltatást megrendeli, a 2.1 pontban foglalt ingyenes Szolgáltatásra meghatározott határozott időtartam a jelen pont szerinti Szolgáltatás nyújtásának megkezdésével újraindul.

**(vi) Országos és Multirégiós Hirdetés**

A jelen pont szerinti Szolgáltatás megvásárlásával a **Díjköteles** Hirdetést feladó Felhasználó a feltöltött állás- vagy autóhirdetése vonatkozásában megválaszthatja, hogy a hirdetés a Felhasználó által elsődlegesen megválasztott régió mellett a) az egész országra vonatkozó keresés esetén vagy b) több, a Felhasználó által megválasztott további régióra (egy-egy megyére vagy Budapestre, de nem egyes településekre vagy budapesti kerületekre) vonatkozó keresés esetén is bekerüljön a találati listába. Amennyiben a Felhasználó meg kívánja szüntetni a jelen pont szerinti Szolgáltatás alkalmazását, azt az Ügyfélszolgálat segítségével teheti meg (az alábbi 8. pontban szereplő elérhetőségeken).

**2.3. Az ajánlórendszerek átláthatósága**

A Weboldalon a hirdetések alapértelmezett rendezési sorrendje a hirdetések megjelenésének időpontjától függ. A találati lista elején a legfrissebb hirdetések szerepelnek, amelyet az időrendben később megjelent hirdetések követnek, a találati lista végén pedig a legrégebben megjelent hirdetések láthatók.

A hirdetések alapértelmezett rendezési sorrendjét a 2.2. pont szerinti kiemelt hirdetések és a Jófogás Bolt kategóriában feladott hirdetések befolyásolják. Alapértelmezett rendezési sorrend alkalmazása esetén, amennyiben a találati lista tartalmaz ilyen hirdetéseket, azok az alapértelmezett lista elején– szintén megjelenés szerinti sorrendben – kerülnek feltüntetésre. A Felhasználó a hirdetések alapértelmezett rendezési sorrendjét szabadon módosíthatja és rendezheti ár szerint is a találatokat, amely esetben a kiemelések és a Bolt kategóriában feladott hirdetések nem lesznek hatással a sorrendre.

A kiemelt hirdetéseket K ikon jelöli a hirdetés jobb alsó sarkában, a Bolt kategóriában feladott hirdetéseket pedig a „Bolt” címke jelöli a hirdetésen.

Így változtatják meg a kiemelések a lista sorrendjét alapértelmezett rendezéskor.

Top kiemelés (Top hirdetés)

Ez a kiemelés típus csak az Autó, Motor és Kishaszongépjármű, Állás és Ingatlan kategóriákban jelenhet meg. Ha a szűrési feltételeknek megfelelően elérhető ilyen hirdetés, az a lista elején fog feltűnni.

Kirakat kiemelés

Kirakat kiemeléssel a hirdetések a lista elején jelennek meg a Top kiemeléssel ellátott hirdetések után. A kirakattal kiemelt hirdetések, a kiemelés aktiválásának sorrendjében szerepelnek, és 7 napig jelennek meg ezen a helyen.

Szalag kiemelés

Ezeken a hirdetéseken egy szalag jelenik meg 30 napig, valamint az első, a második, a harmadik és a tizenötödik napon előre sorolódnak. A listában mindig az előre sorolást követő 24 órára kerülnek ebbe a szakaszba, vagyis az első aktiválástól számított 72 órára, majd a tizenötödik napon 24 órára. A szakaszon belül a kiemelés aktiválásának időpontja az alapja a hirdetések sorrendjének.

Előresorolás és Automatikus előresorolás

Minden olyan hirdetés, melyre Automatikus előresorolást vagy sima Előresorolást vásároltak, a találati listában a vásárlástól számított 24 órára a kiemelt szakaszba kerül. A szakaszon belül a kiemelés aktiválásának időpontja az alapja a hirdetések sorrendjének.

Nem kiemelt hirdetések

A kiemelt hirdetéseket követően a megjelenési időpont alapján rangsorolódnak a hirdetések, amennyiben nem vásároltak rájuk kiemelést vagy az már lejárt.

A “Multirégió” vagy “Országos” kiemeléssel ellátott, és a szállítással megrendelhető termékek, a találati lista sorrendjét nem befolyásoló kiemelések, azonban más hirdetésektől eltérően nem csak a feladási helyüknek megfelelő régióban jelennek meg.

“Multirégió” vagy “Országos kiemelések: A kiválasztott megyéknek megfelelően vagy országosan jelennek meg a listázásban.

Szállítással rendelhető termékek: Országosan jelennek meg a listázásban, ezt lehetőség van a Részletes keresőben kikapcsolni.

Ár szerint történő rendezés

A Jófogás látogatói kiválaszthatják, hogy a “Legolcsóbb elöl” vagy a “Legdrágább elöl” kritériumok alapján rendezzék a találatokat, ilyenkor a hirdetésekben szereplő ár alapján történik a listázás, és az alapértelmezett listázásnál szereplő kiemelés alapú rangsorolás nem érvényesül.

Hirdetés ajánlók

A Weboldal főoldalán az alábbi hirdetésajánlók kerülnek megjelenítésre:

* felső, lapozható szekcióban a „Nyitó oldali lapozó” kiemeléssel rendelkező Bolt Üzleti Felhasználók  
    hirdetései, illetve a Jófogás szolgáltatást népszerűsítő reklámok,
* a regisztrált és a felhasználói fiókba bejelentkezett felhasználó számára a “Legutóbb ezeket nézted”  
    szekcióban az általa legutoljára megtekintett hirdetések,
* négy kategóriában a Weboldalra feltöltött legfrissebb hirdetések,
* a Boltok szekcióban véletlenszerűen jelennek meg azok a Boltok és hirdetéseik, melyek legalább 10  
    aktív hirdetéssel rendelkeznek, így listájuk és a megjelenített hirdetéseik minden oldalletöltésre  
    változhat.

A Jófogás Ingatlan és Jófogás Autó nyitóoldalakon a regisztrált és a felhasználói fiókjába  
bejelentkezett felhasználók számára a “Legutóbb ezeket nézted” szekcióban az általa legutoljára  
megtekintett hirdetések, majd az adott kategóriában feladott legfrissebb hirdetésajánlók kerülnek  
megjelenítésre, a nyitóoldal alján pedig a Boltok szekcióban véletlenszerűen jelennek meg azok a  
Boltok, melyek legalább 10 aktív hirdetéssel rendelkeznek. A Jófogás Állás nyitóoldalon a legfrissebb  
feladott hirdetésajánlók láthatók, valamint véletlenszerűen azok az aktív hirdetéssel rendelkező  
kiemelt Üzleti Felhasználók is megjelenítésre kerülnek, akik erre vonatkozó előfizetéssel  
rendelkeznek.

A Weboldal hirdetési aloldalainak alján a hirdetéshez képesti hasonlóság vagy a korábbi böngészési  
előzmények alapján kerülnek megjelenítésre azok az ajánlatok, amelyek még érdekesek lehetnek a  
felhasználó számára, illetve Bolt hirdetés esetén a Bolt hirdető többi hirdetése jelenik meg.  
A hirdetés ajánlók sorrendjét a felhasználó nem módosíthatja.

**2.4. Csomagküldő szolgáltatások**

A Társaság a Felhasználók részére nyújtott Szolgáltatásai keretében lehetővé teszi külső szállítási partnerei szolgáltatásainak igénybevételét a Weboldalon eladásra kínált termékek vevőhöz való eljuttatása érdekében, a külső partnerek által alkalmazott általános szerződési feltételek alapján, melyek a szolgáltatás igénybevételét megelőzően elérhetővé válnak a Felhasználó részére a Weboldalon. A Társaság az alábbi szállítási partnerek szolgáltatásait ajánlja a Weboldalon, mely szolgáltatások alkalmazási köre elkülönül egymástól, vagyis egy adott típusú küldemény adott módon történő feladása az alábbi Szolgáltatások közül csak az annak megfelelő szolgáltató igénybe vétele útján lehetséges. Amennyiben egy termék megfelel mind a Foxpost, mind a Háztól-Házig szolgáltatás (Háztól-Házig XXL szolgáltatásra ez a rendelkezés nem alkalmazandó) igénybevételi feltételeinek, akkor az Eladó választhat a hirdetésfeladási lehetőségnél, hogy melyik szolgáltatást kívánja bekapcsolni. A Háztól-Házig és a Háztól-Házig XXL szolgáltatás igénybevételének feltételeit [ezen a linken](https://docs.jofogas.hu/haztol-hazig/), a Foxpost szolgáltatás esetében pedig a szolgáltatás aktiválásnak feltételeit [ezen a linken](https://foxpost.jofogas.hu/) lehet megtekinteni. A HÁZTÓL-HÁZIG Szolgáltatás, a Háztól-Házig XXL Szolgáltatás vagy a FoxPost Szolgáltatás keretében történő sikeres kézbesítést követően az érintett hirdetés automatikusan eltüntetésre kerül a Weboldalról.

Azon hirdetések, amelyek tekintetében eladó aktiválta a Háztól-Házig, Háztól-Házig XXL és Foxpost  Szolgáltatás igénybevételét, illetőleg, amely hirdetések tekintetében a vevők igényelhetik a Háztól-Házig Szolgáltatás igénybevételét a saját költségükön, országosan jelennek meg az adott szállítási szolgáltatás logójával, függetlenül attól, hogy az aktiválás eredményeként az adott hirdetés vonatkozásában megvalósul a szállítási Szolgáltatás igénybevétele

**2.4.1. Háztól-Házig Szolgáltatás** A **HÁZTÓL-HÁZIG Szolgáltatást** a GLS General Logistics Systems Hungary Kft. (székhely: 2351 Alsónémedi, GLS Európa utca 2.; a továbbiakban: **GLS**) az alábbi [linken](https://gls-group.eu/HU/hu/altalanos-uzleti-feltetelek/) elérhető Általános Szerződés Feltételek (a továbbiakban: **GLS ÁSZF**) szerinti [Business-Parcel](https://gls-group.eu/HU/hu/csomagkuldes/belfoldi-csomagszallitas) (küldemény súlya 0,1 kg -15 kg-ig terjedhet) és [Business Small Parcel](https://gls-group.eu/HU/hu/csomagkuldes/kiscsomag-szallitas) (küldemény súlya 0,1 kg – 5 kg-ig terjedhet) szolgáltatásaival nyújtja. A GLS általános szerződéses feltételei az alábbi **linken** érhetők el. A Háztól-Házig Szolgáltatás a Weboldalon elérhető hirdetések vonatkozásában vehető igénybe, addig, amíg az adott hirdetés szerepel a Weboldalon.

A HÁZTÓL-HÁZIG Szolgáltatás igénybevételével a Felhasználó elfogadja, hogy a HÁZTÓL-HÁZIG Szolgáltatásra, így többek között, de nem kizárólagosan a csomag feladására, szállítására, kézbesítésére, az azzal kapcsolatos felelősségre és panaszkezelésre a GLS ÁSZF, illetve a jelen Szabályzat rendelkezései irányadóak.

A Háztól-Házig szolgáltatás igénylője az Eladó vagy a Vevő is lehet. A Társaság a Felhasználók HÁZTÓL-HÁZIG Szolgáltatásra vonatkozó megrendelését az Eladó általi aktiválás esetén a megrendeléstől, a Vevő általi aktiválás esetén az Eladó által történő megerősítéstől számított 1 órán belül továbbítja a GLS részére. Egyebekben a Társaság a HÁZTÓL-HÁZIG Szolgáltatás nyújtásában nem működik közre, abban nem vesz részt.

A GLS ÁSZF-ben a Megbízó részére előírt kötelezettségek és feltételek teljesítése az alábbiakban meghatározásra kerülő GLS Szolgáltatás vonatkozásában a HÁZTÓL-HÁZIG szolgáltatást igénylő Felhasználót terhelik, míg az ott meghatározott jogok ugyanezt a Felhasználót illetik.

A HÁZTÓL-HÁZIG Szolgáltatás szempontjából a továbbiakban a hirdetést feltöltő Felhasználó a “**Feladó**” vagy “**Eladó**“, a hirdetésre jelentkező Felhasználó pedig a “**Címzett**” vagy “**Vevő**“. Címzett az is, aki az Eladó által feladott GLS csomag címzettje.

A HÁZTÓL-HÁZIG Szolgáltatás kizárólag Magyarország területén történő feladással, és Magyarország területén lévő kézbesítési helyre történő címzéssel vehető igénybe legalább 3.800,- Ft és legfeljebb 499.995,- Ft termékárú árucikk szállításához

A HÁZTÓL-HÁZIG Szolgáltatás kizárólag Magyarország területén történő feladással, és Magyarország területén lévő kézbesítési helyre történő címzéssel vehető igénybe. A HÁZTÓL-HÁZIG Szolgáltatást vagy az Eladó aktiválhatja egy adott hirdetéséhez, vagy – amennyiben Eladó ezzel a lehetőséggel nem élt -, a Háztól-házig szolgáltatást a Vevő is igényelheti, az alábbiak szerint:

Eladó által történő aktiválás:

* Az Eladó a hirdetésfeladás illetve a hirdetés szerkesztése során az adott termék kiszállítására vonatkozóan aktiválhatja a Háztól-házig szolgáltatást a kötelező adatok megadásával, melyet követően egy „Megrendelem ingyenes házhozszállítással” feliratú gomb jelenik meg a Weboldalon az adott hirdetés felületén. A Vevő a „Megrendelem ingyenes házhozszállítással” gomb megnyomásával megrendelheti a terméket, azaz ajánlatot tehet a termék megvásárlására.
* A megrendelés leadását követően az Eladónak 36 óra áll rendelkezésére (vevői igénylés esetén szintén 36 óra), hogy visszaigazolja a megrendelést, vagyis a Vevő vételi ajánlatát elfogadja. Ennek megtörténtét követően indul el a szállítási folyamat. Ha a visszaigazolás bármilyen okból nem történik meg, a szállítási folyamat meghiúsul és a termékre vonatkozó adásvétel nem jön létre.
* Eladó által aktivált Háztól-házig szolgáltatás esetén a szállítási díj az Eladót terheli, amely a termék Weboldalon meghirdetett vételárából (a továbbiakban: Vételár) kerül levonásra. A szállítási díjjal csökkentett Vételárat a GLS utalja át Eladónak, a szolgáltatásról számla pedig az Eladó felé kerül kiállításra.

A Háztól-házig szolgáltatás Eladó általi aktiválása esetén az Eladó tudomásul veszi, hogy a HÁZTÓL-HÁZIG Szolgáltatás díja az Eladó által értékesítésre kínált termékre meghatározott Vételárból kerül levonásra, amely vételár a vevő megrendelésének leadását követően nem módosítható. Az Eladó a HÁZTÓL-HÁZIG Szolgáltatás díjának figyelembe vételével köteles meghatározni a Vételárat. Az Eladó a HÁZTÓL-HÁZIG Szolgáltatás aktuális díjáról a https://docs.jofogas.hu/haztol-hazig/ oldalon tájékozódhat.

Az Eladó a HÁZTÓL-HÁZIG Szolgáltatás aktiválásakor, illetve jóváhagyásakor az alábbi adatokat köteles megadni:

* termék Vételára (amely tartalmazza a HÁZTÓL-HÁZIG Szolgáltatás ellenértékét);
* csomag mérete, súlya;
* az Eladó neve;
* az Eladó telefonszáma;
* az Eladó pontos címe (a csomagfelvétel helye);
* az Eladó bankszámla száma;
* az Eladó számlázási adatai (csak eladói aktiválás esetén);
* az eladó által a futárnak küldött üzenet maximum 30 karakter terjedelemben (pl. kapucsengő);
* az eladásra kínált termék Eladó által megadott rövid leírása.

Vevő által történő aktiválás:

Amennyiben a HÁZTÓL-HÁZIG Szolgáltatásra a Vevő ad le igénylést egy adott hirdetés vonatkozásában, a Háztól-házig szolgáltatás fent meghatározott díja a Vevőt terheli, melyet a megrendelt termék átvételkor köteles a GLS részére megfizetni a termék Vételárán felül.

A Vevő a HÁZTÓL-HÁZIG Szolgáltatás igénylésekor, illetve Eladói aktiválás esetén a termék megrendelésekor az alábbi adatokat köteles megadni:

* a Vevő neve;

* a Vevő telefonszáma;
* a Vevő email címe;

* a Vevő pontos címe (a csomag átvétel helye);

* a Vevő által a futárnak küldött üzenet maximum 30 karakter terjedelemben (pl. kapucsengő);

* a Vevő számlázási adatai (csak vevői igénylés esetén).

A GLS Szolgáltatás az alábbiakat tartalmazza:

A HÁZTÓL-HÁZIG Szolgáltatás részeként a GLS a Szolgáltatást igénybe vevő Felhasználó részére az alábbi szolgáltatásokat nyújtja:

* GLS által történő csomagfelvétel, kézbesítési címre történő kézbesítés, Vételár átvétele a hirdetésre jelentkező címzettől,

* Eladó által történő igényléskor (a továbbiakban: aktiválás) a HÁZTÓL-HÁZIG Szolgáltatás díjával csökkentett Vételár beszedésre és megfizetésre kerül Eladó részére;
* Vevő által történő igényléskor a GLS-nek átadott Vételár megfizetésre kerül Eladó részére (együttesen a továbbiakban: **GLS Szolgáltatás**).

Az érintett csomagot a GLS az Eladó által megadott csomagfelvételi címről a Felhasználó igénylésének a GLS felé történő visszaigazolását követő munkanapon szállításra átveszi az Eladótól, amennyiben a Háztól-házig szolgáltatásra vonatkozó vevői igénylés, vagy az adott termékre vonatkozó vételi ajánlat (megrendelés) Eladó általi jóváhagyása a beérkezés napján 21:00 óráig megérkezik. Amennyiben a tárgynapon a GLS a megrendelést rendszerében rögzítette, a Feladó visszaigazolást kap a felvételt megelőző munkanapon 21.00- ig beérkezett megrendelés feldolgozásáról és egyúttal e-mailen vagy sms-ben tájékoztatásra kerül az Eladó a csomagfelvétel napjáról. A beérkezés napján 21.00 óra után beérkezett, vagy 22:00 óra után visszaigazolt megrendeléseket a GLS a megrendelés napját követő második munkanapon veszi fel az Eladótól.

A GLS a megrendelést annak beérkezését követően haladéktalanul rögzíti és a beérkezést követően haladéktalanul tájékoztatást nyújt a megrendelés csomagazonosító számáról, amely alapján mind az Eladó és a Címzett is képes követni a megrendelés teljesítésének státuszát, továbbá tájékozódni a kiszállítási információkról (IOD);A megrendelésben megadott adatok alapján a GLS csomagcímkét nyomtat ki, amelyet felvételkor a Feladó részére átad, és azt a Feladó ragasztja fel a csomagra, A csomagcímke tartalmát a Feladó köteles ellenőrizni és Feladó felel annak pontosságáért és teljeskörűségéért. Elszállításkor a Feladó által átadott csomagok nyugtázása a GLS által előre átadott átvételi bizonylaton darabszám szerint történik.

A GLS a küldeményeket Eladótól való felvételüket követően feldolgozza, rakodásukat intézi, továbbításukat megszervezi és lebonyolítja az általa igénybe vett fuvarozóval, valamint kézbesíti a Vevő részére.

A csomag szállításra történő felvételének napján a GLS email formájában tájékoztatja a Vevőt a kézbesítés napjáról.

A GLS a csomag kézbesítését belföldön a felvétel napját követő első munkanapon, munkaidőben 17:30-ig, illetve az ún. depóvárosokban 1 munkanapokon 20:00-ig végrehajtja, azaz eddig az időpontig az Eladótól felvett csomagot a Vevő részére átadja, tőle a termék Vételárát, valamint – amennyiben a Vételár azt nem tartalmazza – a Háztól-házig szolgáltatás díját beszedi. A GLS a Címzett részére e-mail értesítést küld a kézbesítés napjának reggelén, ami tartalmazza a kézbesítést végző GLS futár telefonszámát, a GLS ügyfélszolgálat elérhetőségét is, továbbá a kézbesítés várható időpontját egy háromórás időintervallum formájában. Az átvételt a Címzett a GLS fuvarlapján (rollkarte) nyugtázza. A GLS a kiszállítási bizonylatokat két évig megőrzi.

A GLS legkésőbb a csomag sikeres kézbesítése napját követő három munkanapon belül az Eladó részére az általa megadott bankszámlára átutalja Vevői GLS Szolgáltatás-igénylés esetén az értékesített áru teljes Vételárát, Eladói GLS Szolgáltatás-aktiválás esetén a Vételár HÁZTÓL-HÁZIG Szolgáltatás díjával csökkentett összegét. Ezen időszakra Feladó kamatra nem jogosult. A HÁZTÓL-HÁZIG Szolgáltatás díjáról a kézbesítést követően kerül kiállításra aszámla.

A Címzett köteles az átvételkor a küldeményt a GLS futár jelenlétében megvizsgálni és nyilvánvaló, a küldemény külső csomagolásán észlelt, felismerhető károk esetén felbontani és a felismerhető hiányosságokról, vagy egyéb károkról a futárral közösen a sérülés jellegét, mértékét, feltehető okát, valamint a kár részletes leírását tartalmazó jegyzőkönyvet felvenni, továbbá a sérülést és a kárt a GLS-nek haladéktalanul bejelenteni. A Címzett köteles minden ésszerű intézkedést megtenni a kár enyhítése érdekében.

A küldemény részleges elveszését vagy megsérülését – ha az felismerhető – a küldemény kézbesítésekor, illetve a küldemény visszakézbesítésekor a kézbesítési okiraton azonnal jelezni kell. Mind a Címzett, mind a Feladó köteles a GLS-nek lehetővé tenni, hogy személyesen és fizikailag meggyőződjön a kár jellegéről és mértékéről.

A GLS Szolgáltatásból, így a HÁZTÓL-HÁZIG Szolgáltatásból kizárt termékek és áruk:

* bármely, a Szabályzatba ütköző termék vagy áru;
* a Címzett postafiókcímére vagy helyrajzi számára szóló csomagok;
* amelyek körmérete meghaladja a 3 métert (a körméret kiszámítása: a leghosszabb oldal, valamint a két rövidebb oldal kétszeresének összege);
* amelyek maximális hossza meghaladja a 200 centimétert;
* amelyek súlya meghaladja a 15 kg-ot;
* egyéb, láthatóan túlsúlyos vagy túlméretes küldemények;
* amelyek súlyuknak, alakjuknak, tartalmuknak megfelelően nincsenek megfelelően védve, vagy csomagolva, ill. amelyeken a Feladó, vagy a Címzett szükséges adatait nem tüntették fel, vagy a feltüntetett adatok nem pontosak, hiányosak, vagy nem felelnek meg a valóságnak;
* a sérült csomagok, sérült csomagolással ellátott küldemények;
* összepántolt csomagok, faládában feladott csomagok, zsákos, zacskós, ömlesztett csomagolású küldemények;
* hőmérsékletre érzékeny áruk, sugárzó anyagok;
* különlegesen értékes árucikkek, különös tekintettel a kihúzott nyereményszelvényre és hasonlókra, nemesfémekre, valódi ékszerekre, drágakövekre, igazgyöngyökre, antik ékszerekre, műtárgyakra, előszereteti értékkel rendelkező tárgyakra;
* pénz, okiratok, dokumentumok, értékpapírok, hitelkártyák, csekkek vagy telefonkártyák, vagy más hasonló értékek;
* 3.800,- Ft összeget meg nem haladó, illetve 499.995,- Ft összeget meghaladó értékű árucikkek;
* lőfegyvernek minősülő tárgyak, lőszer, lőfegyver, robbanóanyagok, mérgező anyagok, ide értve a levegővel vagy szén-dioxiddal működő fegyvereket, a valódi fegyverrel összetéveszthető utánzatokat;
* okmányok, vagyoni értékű javakat, ill. szolgáltatást megtestesítő tárgyak, személyes papírok, értékes levelek, régiségek, régészeti tárgyak, egyedi és különleges dísztárgyak, régi könyvek és iratok, festmények, pornográfia, tiltott narkotikum, tiltott gyógyszer;
* az olyan csomagok, amelyek tartalma, továbbítása, vagy kialakítása, külső megjelenítése jogszabályban foglalt rendelkezéseket sért;
* fertőző, undort keltő, ill. olyan áruk, amelyeket speciális módon kell védeni, vagy kezelni;
* élő, vagy holt állatok, orvosi vagy biológiai vizsgálati anyagok, egészségügyi hulladékok, emberi vagy állati maradványok, testrészek, szervek, növények;
* élelmiszerek;
* tűzifa
* romlandó áruk (azaz minden olyan dolog, aminek tárolhatósági élettartama korlátozott, és ami különleges bánásmód nélkül rövid időn – néhány napon – belül gyorsan megromlik vagy lebomlik és minden egyéb olyan áru, amelynek károsodásra való hajlamossága feltételezhető, mivel összetételére tekintettel a túlzott meleg vagy hideg hatására megsemmisülhet);
* olyan csomagok, amelyek személyeknek (tulajdonában vagy egészségében) vagy árukban kárt okozhatnak;
* a Címzett postafiókjába kézbesítendő csomagok;
* le nem zárt, illetve megfelelő, a termék állagának fuvarozás során történő megóvásához és épségben tartásához megfelelő csomagolással el nem látott csomagok, illetve egyéb nem megfelelő módon és/ vagy nem a kereskedelemben szokásos módon csomagolt áruk;
* veszélyes áru (különösen az 1979. évi 19. törvényerejű rendelettel kihirdetett a Veszélyes Áruk Nemzetközi Közúti Szállításáról szóló Európai Megállapodásban foglalt áruk);
* a „Medencék, kerti tó” és „Egyéb” alkategóriában – amely alkategóriák az „Otthon, háztartás” főkategórián belül a „Háztartási felszerelések” kategóriában érhetők el – feladott hirdetések.
* egyéb, a GLS Általános Csomagbiztosítási Feltételeibe ütköző, vagy a GLS Általános Szerződési Feltételeiben a Szolgáltatásból kizárt termékek és áruk.

Az Eladó tudomásul veszi, hogy a HÁZTÓL-HÁZIG Szolgáltatásából kizárt termékek vagy áruk Eladó által történő, szállításra történő feladásából eredő valamennyi kárért az Eladó felelős. Amennyiben az észlelhető, hogy az Eladó a HÁZTÓL-HÁZIG Szolgáltatásából kizárt termék vagy áru vonatkozásában kívánja a GLS Szolgáltatást igénybe venni, a GLS a Szolgáltatás nyújtását bármikor megtagadhatja, vagy megszakíthatja, bár erre nem köteles, és a felmerült kárért az Eladó helytállni köteles.

Arra vonatkozóan, hogy valamely feladásra kerülő termék a HÁZTÓL-HÁZIG Szolgáltatásból, így a GLS Szolgáltatásból kizárt terméknek minősül-e, a szolgáltatás nyújtójának nincs ellenőrzési kötelezettsége. A Feladó köteles ellenőrizni a küldeményeknek a GLS részére továbbításra történő átadása előtt, hogy azok nem ütköznek-e a jelen Szabályzat, illetve a GLS Általános Szerződési Feltételeibe. Ezen termékek GLS részére továbbításra történő átadása tilos, és ezen tilalom megszegéséből eredő valamennyi kárért az Eladó felelős. A Társaság kizárja felelősségét azokért a küldeményekért, amelyekről a feladást követően derül ki, hogy a szállításból kizárt küldeménynek minősülnek a GLS ÁSZF, illetve a jelen Szabályzat szerint. A Feladó felel azért is, hogy a feladásra került, lezárt csomagolásban elhelyezett termék megfelel a Weboldalra feltöltött hirdetésében eladásra kínált terméknek, amely vonatkozásában a HÁZTÓL-HÁZIG Szolgáltatást megrendelte, és amely vonatkozásában a Vevőtől vételi ajánlatot kapott.

Az Eladó a jogszabályban előírtak mellett teljes mértékben felelős minden olyan kárért, mely HÁZTÓL-HÁZIG Szolgáltatásából kizárt termékek vagy áruk feladása révén a Társaság, a GLS, vagy alvállalkozója dolgaiban, vagy fuvareszközeiben, vagy más a GLS-nek átadott küldeményben keletkezik, valamint minden ezzel kapcsolatos személyi sérülésért és egyéb költségekért.

A GLS Szolgáltatások teljesítése érdekében a GLS jogosult a küldemények továbbításához szükséges fuvarozási és egyéb szerződések megkötésével alvállalkozók szolgáltatásának igénybevételére, amelyet részben vagy egészben, de változatlan formában továbbértékesít (közvetített szolgáltatás).

Az Eladó köteles gondoskodni a küldemény megfelelő belső és külső csomagolásáról, lezárásáról oly módon, hogy a küldeményt, illetve annak tartalmát a csomagolás – annak jellegzetességeire is figyelemmel – a fuvarozás és a gépi rakodás során is megóvja a kinyílástól és a sérüléstől, illetve, hogy az megfeleljen a GLS Általános Szerződési Feltételeiben foglalt feltételeknek.

A kiszállítást meghiúsultnak kell tekinteni az alábbi esetekben:

* amennyiben a Címzett nem tartózkodik a megadott címen;
* amennyiben a Címzett az átvételt megtagadja;
* amennyiben a GLS gépjárművezetőjének megérkezését követően 10 percen belül nem történik meg a csomag átvétele a Címzett által.

A kiszállítás meghiúsulása esetén a GLS a következőket vállalja:

* Amennyiben a Címzett nem tartózkodik a megadott címen, a GLS egy értesítő-kártyát hagy a helyszínen. Az értesítő-kártyán megtalálható a csomagszám, valamint a GLS telefonszáma. A GLS vállalja, hogy amennyiben a Címzett szabadságon van és a kézbesítést a munkahelyére kérte, a csomagot 10 munkanapon keresztül tárolja, egyéb esetekben pedig 5 munkanapig tárolja, ugyanezen célból. A meghiúsult kézbesítési kísérlet napján a Címzett elektronikus értesítést kap a kézbesítés sikertelenségéről. Az e-mail üzenet tartalmazza a kézbesítési meghiúsulásának okát, az értesítő- kártyán található információkat, valamint direkt hozzáférést a GLS internetes rendelkező felületéhez, ahol a Címzett további kézbesítési lehetőségek közül választhat, illetve új kiszállítási napot egyeztethet a GLS-el. Amennyiben ez nem történik meg a megjelölt tárolási időtartam alatt, úgy a GLS a csomagot visszajuttatja a Feladóhoz.
* Az átvétel megtagadása esetén a GLS a csomagot visszaszállítja a Feladóhoz. Átvétel megtagadásának minősül az is, ha a Címzett nem fizeti meg a GLS részére a beszedendő összeget.
* Időntúli várakozás esetén (ha a GLS gépjárművezetőjének megérkezését követően 10 percen belül nem történik meg a csomag átvétele) a kézbesítési kísérlet a következő munkanapon újra megtörténik, azzal, hogy a tárolási határidőkre és a GLS eljárására az (i) pontban meghatározott feltételek irányadók.
* Helytelen címzés esetén a GLS saját adatbázisából próbálja meg a helyes címet megkeresni. Amennyiben ez a próbálkozás sikertelen, úgy a GLS értesíti a Feladót és rendelkezést kér a csomag további sorsa felől.

Nem a GLS-nek felróható ok, ha a kiszállítás az alábbi okok bármelyike miatt hiúsul meg:

1. adathiányos feladás;
2. nem teljes, vagy hibás cím;
3. a kiszállítás meghiúsulás esetére meghatározott eljárások lefolytatása ellenére a kézbesítés továbbra sem lehetséges a GLS érdekkörén kívül álló okból;
4. a GLS Általános Üzleti Feltételeinek megszegése a Feladó részéről;
5. vis major.

A GLS nem felel a késedelem miatti esetleges károkért, azonban a GLS mindent megtesz annak érdekében, hogy a küldeményt a megadott szállítási idejének megfelelő idő alatt kiszállítsa a Címzettnek.

A Társaság kizárja a felelősségét az alábbi ügykörökben, amelyek vonatkozásában az Eladó vagy a postai szolgáltatásokról szóló 2012. évi CLIX. törvényben (a továbbiakban: Psztv.) meghatározott esetben a Címzett, illetve más harmadik személy közvetlenül a GLS felé fordulhat igényével:

* A küldemény elvesztése, megsemmisülése vagy sérülése esetén a GLS minden esetben megtéríti az eladási árat az igény érvényesítője részére közvetlenül.

* A GLS az átvett áruért a Psztv. irányadó rendelkezései szerint felel.

Amennyiben az arra jogosult nem érvényesíti igényét, a Társaság jogosult az igény érvényesítésére.

A GLS minden küldeményt biztosít. Az automatikus árukár biztosítás feltételeit a GLS Általános Üzleti és Biztosítási Feltételei tartalmazzák, mely jelenleg, belföldi csomagoknál 50.000,- HUF / csomag összegig terjed.

A GLS csak abban az esetben fogad el kárigényt, ha a küldemény a jelen Szabályzat előírásainak, valamint a GLS Általános Üzleti Feltételeinek eleget tesz és az átadáskor a Címzett és a GLS képviselője kárjegyzőkönyvet vesz fel. A GLS nem felel a “belső károkért”, azaz ha a csomagoláson külsérelmi nyom nem található.

A küldeményben bekövetkezett tényleges károkért a GLS felel, mind az Eladó, mind a Címzett a kárigényét a GLS Általános Szerződési Feltételeiben meghatározottak szerinti módon közvetlenül a GLS felé jogosult érvényesíteni.

A Felhasználó a 45/2014. (II.26.) kormányrendelet 29. § (1) a) pontja értelmében kifejezett, előzetes beleegyezését adja a GLS Szolgáltatás igénybevételével ahhoz, hogy a szolgáltatás nyújtója a szolgáltatás egészének teljesítését annak megrendelését követően haladéktalanul, de legkésőbb a megerősítésének a Feladóhoz történő érkezését követően, amennyiben a feltöltött hirdetés megfelel a jelen Szabályzatnak, illetve a GLS ÁSZF-nek az első munkanapon, legfeljebb 1 órán belül megkezdje, és tudomásul veszi, hogy a szolgáltatás egészének teljesítését követően felmondási jogát elveszti.

A GLS Szolgáltatás vonatkozásában a Felhasználók a GLS által működtetett vevőszolgálat igénybevételére jogosultak és kötelesek, a GLS ÁSZF-ben meghatározott feltételek szerint.

Budapest, Rád/Vác, Esztergom, Tatabánya, Sülysáp, Salgótarján, Eger, Miskolc, Debrecen, Nyíregyháza, Törökszentmiklós/Szolnok, Békéscsaba, Kecskemét, Kiskunhalas, Szentes, Szeged, Dunaföldvár, Szekszárd, Kaposvár, Pécs, Székesfehérvár, Veszprém, Siófok, Nagykanizsa, Zalaegerszeg, Győr, Mosonmagyaróvár, Fertőszentmiklós/ Sopron, Szombathely

**2.4.2. Háztól-Házig XXL Szolgáltatás**

A **Háztól-Házig XXL Szolgáltatás** egy belföldön nyújtott (24-72 órás) integrált küldeményszállítási szolgáltatás, melyet a GE Logisztika Korlátolt Felelősségű Társaság (székhely: 1116 Budapest, Hunyadi János út 162; a továbbiakban: **HDT**) nyújt a weboldalán meghatározott általános szerződési feltételek szerint. A HDT általános szerződéses feltételei a [linken](https://www.homedt.hu/documents/aszf.pdf) érhetőek el (a továbbiakban: **HDT ÁSZF**). A Háztól-Házig XXL Szolgáltatás megrendelésével a Felhasználó kötelezőnek fogadja el, hogy a Háztól-Házig XXL Szolgáltatás vonatkozásában közte és a HDT között jön létre szerződés, a HDT ÁSZF feltételei, illetve a HDT által meghatározott, a Weblapon, a hirdetésfeladás menetébe épített tájékoztatásban meghatározott egyedi szerződéses feltételek szerint.

A Társaság a Felhasználók Háztól Házig XXL Szolgáltatásra vonatkozó megrendelését a HDT felé, a hirdetésfeladásnál az Eladó által történő megerősítésétől számított 1 órán belül továbbítja. Egyebekben a HDT Szolgáltatás nyújtásában a Társaság nem működik közre, abban nem vesz részt, azért nem felelős.

A Háztól-Házig XXL Szolgáltatást a hirdetést feladó Felhasználó jogosult igénybe venni a HDT-vel kötött megállapodás alapján a HDT ÁSZF-ben foglalt rendelkezések szerint, azok elfogadása esetén.

A Háztól Házig XXL Szolgáltatást igénybe vevő Felhasználó (a továbbiakban: **Megrendelő**) tudomásul veszi, hogy a Háztól Házig XXL Szolgáltatás megrendelésével a HDT-vel és nem a Társasággal lép szerződéses kapcsolatba a Háztól Házig XXL Szolgáltatás vonatkozásában, továbbá tudomásul veszi, hogy a Társaság nem felel a HDT és a Megrendelő közt kötött szerződés (a továbbiakban: **Szerződés**) létrejöttéért, jogszerűségéért, a Szerződés tartalmáért és a Szerződésben foglaltak szerződésszerű teljesítéséért.

**2.4.3. FoxPost Csomagautomata Szolgáltatás**

A **FoxPost Csomagautomata Szolgáltatás** egy belföldön nyújtott küldeményszállítási szolgáltatás, melyet a **FoxPost Zrt.** (székhely: 3200 Gyöngyös, Batsányi János utca 9.;  továbbiakban: **FoxPost**) nyújt a weboldalán közzétett általános szerződési feltételek szerint, amelyek ezen a  [linken](https://www.foxpost.hu/uploads/documents/hu/foxpost_aszf.pdf) érhetőek el (a továbbiakban: **FoxPost ÁSZF**). A FoxPost Szolgáltatás megrendelésével a Felhasználó magára nézve kötelezőnek ismeri el, hogy a FoxPost Szolgáltatás vonatkozásában közte és a FoxPost között jön létre szerződés, a FoxPost ÁSZF feltételei, illetve a FoxPost által meghatározott, a Weblapon, a hirdetésfeladás menetébe épített tájékoztatásban meghatározott szerződéses feltételek szerint.

A FoxPost Szolgáltatás szempontjából a továbbiakban a hirdetést feltöltő Felhasználó a “**Feladó**” vagy “**Eladó**“, a hirdetésre jelentkező Felhasználó pedig a “**Címzett**” vagy “**Vevő**“. 

A FoxPost Szolgáltatás a legalább 10,- Ft és legfeljebb 150.000,- Ft termékárú árucikkek szállításához vehető igénybe.

A FoxPost Szolgáltatás részeként a FoxPost a Szolgáltatást igénybe vevő Felhasználó részére az alábbi szolgáltatásokat nyújtja:

* FoxPost által történő csomagfelvétel a csomagautomatából, Vevő választása szerinti FoxPost csomagautomatába történő kézbesítés,
* Vételár átvétele a hirdetésre jelentkező címzettől, a FoxPost által a csomagautomatánál megfizetett Vételár megfizetésre kerül Eladó részére (együttesen a továbbiakban: **FoxPost Szolgáltatás**).

Az Eladó a hirdetésfeladás illetve a hirdetés szerkesztése során az adott termék kiszállítására vonatkozóan aktiválhatja a FoxPost Szolgáltatást a kötelező adatok megadásával, melyet követően egy „Megrendelem Foxposttal” feliratú gomb jelenik meg a Weboldalon az adott hirdetés felületén. A Vevő a „Megrendelem Foxposttal” gomb megnyomásával megrendelheti a terméket, azaz ajánlatot tehet a termék megvásárlására.

A megrendelés leadását követően az Eladónak 36 óra áll rendelkezésére, hogy visszaigazolja a megrendelést, vagyis a Vevő vételi ajánlatát elfogadja. Ennek megtörténtét követően a Társaság továbbítja az Eladó és a Vevő adatait a FoxPost részére, amelyet követően létrejön az egyedi szolgáltatási szerződés az Eladó felhasználó és a FoxPost között. 

A Társaság által meghatározott hirdetési kategóriákban és ártartományban a Vevőknek is lehetőségük van aktiválni a FoxPost Szolgáltatást az alábbi feltételekkel:

* Eladó hirdetése megjelenik azokban a kategóriákban, amelyek megfelelnek a FoxPost feltételeknek. Eladónak lehetősége van a hirdetései tekintetében kikapcsolni annak lehetőségét, hogy a hirdetésére vevői FoxPost igénylést kapjon.
* Vevő a hirdetést megnyitva igényelheti a FoxPost Szolgáltatással történő kiszállítását a terméknek. Amennyiben egy hirdetésben szereplő termék egyaránt megfelel a FoxPost Szolgáltatás ([foxpost.jofogas.hu](https://foxpost.jofogas.hu/)) és a GLS Szolgáltatás ([docs.jofogas.hu/haztol-hazig](https://docs.jofogas.hu/haztol-hazig/)) igénybevételi lehetőségének, akkor a Vevőnek lehetősége van a hirdetés megnyitásakor választani a felajánlott két szállítási szolgáltatás közül. 
* Vevői megrendelést kizárólag bejelentkezett Felhasználó indíthat el.
* Vevő megrendelésének leadását követően Eladó e-mailben és push notifikáció formájában megkapja az igénylést. Ezt követően Eladónak 36 óra áll rendelkezésére, hogy visszaigazolja a megrendelést, vagyis a Vevő vételi ajánlatát elfogadja. Eladó a Vevő ajánlatát nem módosíthatja, kizárólag a kiszállításhoz szükséges adatokat adhatja meg. Ennek megtörténtét követően indul el a szállítási folyamat. Ha a visszaigazolás bármilyen okból nem történik meg, a szállítási folyamat meghiúsul, és a termékre vonatkozó adásvétel nem jön létre.
* Vevőnek lehetősége van arra, hogy a megrendelői felületen későbbi felhasználásra megadja az összes kiszállítási adatát.
* A Vevő által leadott FoxPost Szolgáltatással történő szállítási igénylés esetén a szállítás díja Vevőt terheli. Eladónak a szállítás díját ugyan ki kell fizetnie a FoxPost részére amikor leadja az eladott terméket a FoxPost automatába, de a kiszállításkor Foxpost utánvétként beszedi Vevőtől a kiszállítási díj összegével megnövelt vételár összegét, és annak kifizetése történik meg Eladó felé.  

Egyebekben a FoxPost Szolgáltatás nyújtásában a Társaság nem működik közre, abban nem vesz részt, azért nem felelős.

Ha a visszaigazolás bármilyen okból nem történik meg, a szállítási folyamat meghiúsul, és a termékre vonatkozó adásvétel nem jön létre.

A FoxPost szolgáltatásának megrendelése esetén a FoxPost egyedi csomagazonosítót (a továbbiakban: Csomagazonosító) generál, melyet haladéktalanul megküld a felhasználó részére. A csomagazonosító kód  segítségével az Eladó felhasználó (mint feladó) a küldeményt a címzetthez történő eljuttatás céljából az ország meghatározott pontjain elhelyezett és működtetett, kezelőszemélyzet nélküli automata csomagátadó terminálban (a továbbiakban: Csomagterminál) helyezheti el. A Csomagterminál a feladásról bizonylatot nyomtat ki. 

Amennyiben a csomag feladója a csomagazonosító részére történő megküldésétől számított 7 naptári napon belül nem helyezi el a csomagot a Csomagterminálban, az egyedi szolgáltatási szerződés meghiúsul, és a Társaság értesíti a Vevő felhasználót a FoxPost megrendelésének törléséről.

A FoxPost Szolgáltatásért fizetendő, a FoxPost ÁSZF 8.4. pontjában meghatározott kézbesítési és utánvét kezelési díj a Szolgáltatást aktiváló Eladót terheli, amelyet a csomag feladása időpontjában aktuális, a FoxPost által a FoxPost Szolgáltatásért meghatározott díjszabás alapján az Eladó a csomag feladásakor fizet meg bankkártyával a FoxPost automatánál.  Sikertelen kézbesítés esetén a visszakézbesítéssel felmerülő költségek, így különösen a visszakézbesítés FoxPost ÁSZF 3. sz. melléklete szerinti aktuális díja ugyancsak az Eladót terheli. Az Eladó a FoxPost Szolgáltatás díjának figyelembevételével köteles meghatározni a Vételárat. Az Eladó a FoxPost Szolgáltatás aktuális díjáról az alábbi [oldalon](https://foxpost.jofogas.hu/) tájékozódhat.

A címzett a csomagterminálban elhelyezett küldeményt az erről szóló értesítéstől számított 4 napig veheti át a Csomagterminálból.  Ha a címzett 3 napon belül nem veszi át a csomagját a Csomagterminálból, úgy erről a FoxPost a fuvarozót értesíti, aki a küldeményt a saját telephelyére szállítja. 

A küldemény átvételéhez szükséges kódot a FoxPost bocsátja a Címzett rendelkezésére sms útján. A termék Vételárának, valamint az utánvét díjának Vevő általi megfizetése feltétele a küldemény átvételének. Csomagautomatába kért kézbesítés esetén az utánvételi összeg bankkártya útján történő kiegyenlítését követően vehető át a küldemény, ebben az esetben nyílik ki a Csomagterminál megfelelő rekeszének ajtaja.

A Vételárat a FoxPost utalja át az Eladónak, a Szolgáltatásról pedig a FoxPost állít ki számlát a FoxPost felé leadott igény esetén az Eladó felé.

A feladó a megrendeléssel kapcsolatban minden tájékoztatást a FoxPosttól kérhet, illetve felé tehet jognyilatkozatokat. A küldeményt – ha arról a szerződő felek másként nem állapodtak meg – a FoxPost abban az esetben köteles felvenni, ha azt a feladó a tartalom jellegének, természetének és mennyiségének megfelelő csomagolással látta el, és annak tartalmához a csomagolás, illetve a lezárás nyilvánvaló megsértése nélkül hozzáférni nem lehet.

A FoxPost Szolgáltatást a hirdetést feladó Felhasználó jogosult igénybe venni a FoxPosttal kötött megállapodás alapján a FoxPost ÁSZF-ben foglalt rendelkezések szerint, azok elfogadása esetén.

A FoxPost Szolgáltatást igénybe vevő Felhasználó (a továbbiakban: **Megrendelő**) tudomásul veszi, hogy a FoxPost Szolgáltatás megrendelésével a FoxPosttal és nem a Társasággal lép szerződéses kapcsolatba a FoxPost Szolgáltatás vonatkozásában, továbbá tudomásul veszi, hogy a Társaság nem felel a FoxPost és a Megrendelő között fennálló szerződés (a továbbiakban: **Szerződés**) létrejöttéért, jogszerűségéért, a Szerződés tartalmáért és a Szerződésben foglaltak szerződésszerű teljesítéséért

**3\. Hirdetési alapfeltételek**

A Társaság a Weblapon a Felhasználók által közzétett tartalmat, továbbá a hirdetéssel kapcsolatban a Felhasználók közötti kommunikációt jogosult, de nem köteles ellenőrizni, vagy jogellenes tevékenység folytatására utaló tényeket vagy körülményeket keresni.

A Társaság fenntartja a jogot, hogy megítélje a hirdetés valószerűségét, valódiságát és megfelelőségét. A Társaság kizárja a felelősségét minden olyan tartalomért, amelyet harmadik személyek (Felhasználók) töltöttek fel a Társaság rendszerére.

**3.1. A hirdetés típusára vonatkozó alapfeltételek**

**Magánhirdetések:** Magánszemély Felhasználók által a Weblapon feladott hirdetések (a továbbiakban: **Magánhirdetés**).

**Díjköteles hirdetések:** Díjköteles (a továbbiakban: **Díjköteles Hirdetés**) hirdetést Magánszemély Felhasználók és Üzleti Felhasználók adhatnak fel. **Díjköteles** Hirdetésnek minősül minden olyan hirdetés (a) amelyet a Felhasználó Autó kategóriában, Motor, robogó kategóriában, Állásajánlat kategóriában, Üzlet, szolgáltatások kategóriában, Ingatlan kategóriában vagy tűzifa alkategóriában ad fel, , (b) amelyet a Társaság a hirdetés tárgya vagy más ésszerű szempont alapján az (a) pont szerinti kategóriák valamelyikébe tartozónak minősít, vagy (c) az Üzleti Felhasználó által feladott hirdetés. A Társaság fenntartja a jogot annak eldöntésére, hogy egy hirdetés **Díjköteles** Hirdetésnek minősül-e.  

A Jófogás Bolt és az Ingatlan kategóriában feladott **Díjköteles** Hirdetések esetén a 2.1. pontban hivatkozott, a Társaság által üzemeltetett üzenetküldő rendszer az ezen pontban foglalt eltérésekkel használható. Ha a Jófogás Bolt kategóriában feladott hirdetésnél az az opció került kiválasztásra, hogy a hirdető a Weboldal üzenetküldő rendszerét nem veszi igénybe kapcsolatfelvétel biztosítása céljából, ekkor a hirdetésre jelentkező felhasználó a hirdetés mellett elhelyezett “Megrendelem” gomb megnyomásával átirányításra kerül a hirdető külső weboldalára, melyet követően a hirdető és a hirdetésre jelentkező felhasználó között folytatott minden kommunikáció a Weboldalon kívülinek minősül.

Ingatlan, Jófogás Bolt és Állás kategóriában előfizetéssel rendelkező Felhasználók hirdetései és Jármű kategóriában eredetileg a Használtautó Weboldalon feladott **Díjköteles** Hirdetések esetén a 2.1. pontban hivatkozott, a Társaság által üzemeltetett üzenetküldő rendszer nem használható, ezen hirdetések vonatkozásában a hirdető és a hirdetésre jelentkező felhasználó közötti kommunikáció a hirdető által az apróhirdetésben megadott mobiltelefonos elérhetőségen keresztül, szóban vagy sms útján , Ingatlan, Jófogás Bolt és Állás kategóriában előfizetéssel rendelkező Felhasználók hirdetései esetén a hirdetésre jelentkező felhasználó üzenetének a hirdető fiókjához tartozó e-mail címére való továbbítással, Állás és Jófogás Bolt kategóriában előfizetéssel rendelkező Felhasználók hirdetései esetén a hirdető telephelyén történő személyes kapcsolatfelvétel útján, valamint a hirdető weboldalára mutató linkre való átirányítás útján is lehetséges.

**Reklám jellegű hirdetések:** Hirdetést kizárólag reklám-marketing célokra nem lehet használni, konkrét termék, állás vagy szolgáltatás felkínálása nélkül hirdetni tilos.

**3.2. A hirdetés tárgyára vonatkozó alapfeltételek**

**Álláshirdetések:** A [jófogás.hu](http://www.jofogas.hu/) oldalain kizárólag valós, teljes- vagy részmunkaidős, bejelentett álláslehetőségeket engedélyezett hirdetni. Nem engedélyezett szexuális vagy erotikus jellegű munka hirdetése a Weblapon. Az Álláshirdetések feladásának díjára vonatkozó szabályokat a 2.2(vi) pont tartalmazza.

**Állatokra vonatkozó szabályok:** A Weblapon kínált állatoknak meg kell felelniük az adott faj forgalomba hozatalára vonatkozó hatályos jogszabályoknak.

* csak az a kutya/macska nevezhető a hirdetésben fajtatisztának, amely igazolhatóan törzskönyvezett vagy származási lapja van, amely tényt a hirdetésben fel kell tüntetni. A Weblapon kínált állatot a hirdetésben csak akkor lehet fajtatisztaként megjelölni, ha a hirdető feltünteti a hirdetésben, hogy az állat törzskönyvezett vagy származási lappal rendelkezik. Kétség esetén a Társaság a hirdetőtől a származás igazolására szolgáló dokumentációt bekérheti.

* Kutya, macska, vadászgörény esetén az állat 8 hetes kora előtt nem választható el az anyjától, ezért a Weblapon tiltott 8 hetesnél fiatalabb állatot hirdetésben szerepeltetni, kivéve, ha a hirdető megjelöli, hogy az állat csak a 8 hetes kor betöltése után vehető át.

* Kutya tulajdonjogát átruházni kizárólag az állatot azonosító elektronikus transzponderrel (bőr alá ültetett mikrochip) történő megjelölése után lehet a 41/2010. (II.26.) Korm. rendelet rendelkezései szerint. A mikrochip rendelkezésre állását, illetve adott esetben annak tényét, hogy a chipezés költsége a vásárlót terheli, fel kell tüntetni a hirdetésben.  Ennek hiányában a Társaság megtagadhatja a hirdetés megjelentetését.

* Amennyiben jogszabály előírja, illetve amennyiben az állat ilyennel rendelkezik, a hirdetésben fel kell tüntetni a kisállat egészségügyi könyv rendelkezésre állásának tényét. Fel kell tüntetni a hirdetésben továbbá, hogy az állat kapott-e veszettség elleni oltást, valamint az ennek igazolására szolgáló dokumentumok meglétét. A jelen pontban foglalt információk feltüntetése hiányában a Társaság megtagadhatja a hirdetés megjelentetését, illetve kétség esetén a Társaság a hirdetőtől a vonatkozó dokumentációt bekérheti.

* Ha az állatot nem magánszemély, hanem tenyésztő hirdeti, a kennel nevét fel kell tüntetnie a hirdetésben. A Társaság fenntartja a jogot arra, hogy a tenyésztőtől a kennelre vonatkozó dokumentumok közlését kérje. Amennyiben a hirdető a Társaság e kérésének ésszerű határidőn belül nem tesz eleget, a Társaság megtagadhatja a hirdetés megjelentetését.

* Kutya és macska csak ingyenesen, örökbefogadásra hirdethető, kivéve, ha kutya- illetve macskatenyésztő Bolt szerződést köt, és megfelelő dokumentumokkal igazolja, hogy regisztrált tenyésztőként működik. Egyéb kedvtelésből tartott kisállat csak pénzért és örökbefogadásra hirdethető, termékre nem cserélhető.

A Társaság korlátozhatja vagy megtilthatja a fenti szabályokba ütköző hirdetések közzétételét.

A Társaság az általa képviselt etikai és erkölcsi alapelvekkel ellentétesnek tartja olyan állatokra vonatkozó hirdetések közzétételét, amelyek számára nyilvánvalóan nem biztosítják a megfelelő tartási körülményeket, ezért amennyiben ennek gyanúja a Társaság számára rendelkezésre álló adatok, fotók alapján felmerül, a Társaság jogosult megtagadni a hirdetés közzétételét.

**Szolgáltatások:** A felkínált vagy keresett szolgáltatásnak meg kell felelnie a Társaság adott kategóriára vonatkozó előírásainak. A Társaság korlátozhatja vagy megtilthatja egyes szolgáltatások hirdetését.

**Termékek:** A felkínált vagy keresett termék csak a hatályos magyar jogszabályok értelmében forgalomba hozható termék lehet. A Társaság korlátozhatja vagy megtilthatja egyes termékek hirdetését. Amennyiben a hirdetés tárgyaként megjelölt termék vagy szolgáltatás a mindenkor hatályos jogszabályok szerint engedéllyel, bejelentéssel, vagy egyéb, jogszabályban meghatározott feltétellel forgalmazható, hirdethető vagy ruházható át, a Felhasználó a hirdetés feltöltésével kijelenti és szavatolja, hogy ezekkel rendelkezik, így különösen a jogszerű hirdetéshez és átruházáshoz vagy forgalmazásához szükséges jogi követelmények és feltételek fennállnak, a hirdetés közzététele és annak teljesítése jogszabályba nem ütközik, harmadik fél jogát nem sérti. A Felhasználó köteles a termékleírásban szerepeltetni a termékre vonatkozó lényeges információkat, így különösen, de nem kizárólagosan a szavatosságra, lejáratra, garanciára vonatkozó információkat, illetve a magyar nyelvű termékleírást, a jogszabályokban meghatározott egyéb információkat. Engedélyköteles termék esetén a Felhasználó köteles a termékleírásban feltüntetni, hogy ilyen engedély birtokában van és a termék árusítása nem ütközik jogszabályba, hatósági rendelkezésbe. A Weblapon felkínált termékek vagy szolgáltatások különösen nem tartozhatnak a Szabályzat 4. pontjában meghatározott kategóriákba.

**Előzetes minőségvizsgálati vagy megfelelőség-tanúsítási kötelezettség alá tartozó termék vagy szolgáltatás hirdetése**: a hirdetésben megjelenő termékleírásban a Felhasználó köteles nyilatkozni arról, hogy a vizsgálatot elvégezték és az érintett termék vagy szolgáltatás forgalomba hozható.

**Szerzői jogvédelem alatt álló termékek**: A termékleírásban nyilatkozni szükséges arra vonatkozóan, hogy a termék eredeti, nem másolt, illetve a termék felhasználási, értékesítési jogával a Felhasználó rendelkezik.

**3.3. A hirdetés tartalmára vonatkozó alapfeltételek**

**Árak**: A hirdetésben szereplő árak – így a vevő által ténylegesen fizetendő, általános forgalmi adót és egyéb kötelező terheket is tartalmazó ár – jogszabályi rendelkezéseknek megfelelő feltüntetése a hirdetést feladó Felhasználó kötelessége és felelőssége.

**Egyetlen termék hirdetésenként:** Nem megengedett több terméket egy hirdetésben hirdetni, kivéve, ha cseréről van szó (pl. 2 áru 1-ért) illetve, ha a több terméket egyben, egy közös árral szeretné eladni.

**Hirdetés címe:** A hirdetés címének a termék, szolgáltatás vagy állás megnevezését vagy márkáját kell minimálisan tartalmaznia a magyar nyelv helyesírási szabályainak megfelelő módon. Több, azonos Felhasználó által meghirdetett ajánlatnak azonos címe is lehet.

**Hirdetés szövege:** A hirdetés tárgyát rövid leírással szükséges részletesen ismertetni. A hirdetés szövege utalhat a Felhasználó egyéb hirdetéseire is. A hirdetések szövegében az általánosan elfogadott [netikett](http://hu.wikipedia.org/wiki/Netikett) irányelvei követendők.

**Link:** A hirdetésben szereplő linkeknek a hirdetett termékre vagy szolgáltatásra kell vonatkozniuk. Tilos másik aukciós vagy hirdetési oldalra mutató linket elhelyezni a hirdetésben.

**Nyelv:** A Weblapon megjelenő hirdetéseknek magyar nyelvű változatot tartalmazniuk kell.

**Kategória:** A hirdetést abban a kategóriában kell elhelyezni, amelyik a legjobban leírja az adott terméket vagy szolgáltatást. Nem megfelelő kategorizálás esetén a Társaság jogosult a hirdetést a megfelelő kategóriába áthelyezni. Amennyiben a megfelelő kategória ellenérték megfizetése mellett igénybe vehető Szolgáltatás körébe tartozna, a Társaság a hirdetést annak áthelyezése helyett jogosult visszautasítani. Ebben az esetben a Felhasználó a hirdetés ismételt feladását a megfelelő kategória kiválasztásával, díjfizetés ellenében kezdeményezheti.

**Kép:** A hirdetésben szereplő képeknek a hirdetett termékre vagy szolgáltatásra kell vonatkozniuk. Pecséttel, más hirdetési oldalak emblémájával, nevével, vízjelével ellátott képek nem csatolhatók a hirdetéshez. Nem tölthetők fel olyan képek, amelyeken a termék elforgatva szerepel. Masszázs szolgáltatás hirdetése esetén a feltöltött kép önarcképet, illetve a szolgáltatás nyújtásának helyszínét ábrázolhatja kizárólag. A Társaság jogosult a képet logójával ellátni. Más hirdetők képeit beleegyezésük nélkül tilos használni. Ugyanez vonatkozik az Internetről letöltött katalógusképekre is, melyeket csak abban az esetben lehet használni, ha maga a hirdető rendelkezik a szerzői jogokkal.

**Többszörös hirdetés:** Tilos ugyanazt a terméket, szolgáltatást vagy állást több hirdetésben hirdetni. Értelemszerűen, tilos ugyanazt a terméket, szolgáltatást vagy állást különböző kategóriákban vagy régiókban hirdetni (ide nem értve a 2.2.1 (vi) pontban szabályozott multirégiós hirdetést). A hirdetés lejárta, illetve a lejárat előtt a hirdetés felhasználó általi törlésének kezdeményezése esetén az archiválási időtartam, illetve a törlés alatti periódus leteltét követően adható fel ugyanarra a termékre, szolgáltatásra, állásra vonatkozó másik hirdetés. Az archiválási időtartam, illetve a törlés alatti periódus alatt a hirdetés aktiválására van lehetőség a jelen Szabályzat szerint.

**Ingatlan hirdetések:** Ingatlanra vonatkozó hirdetés feladásának feltétele továbbá legalább az érintett ingatlan irányítószámának megadása. A hirdetés termékoldalán a Társaság tájékoztató jelleggel feltünteti az ingatlan megközelítő (de nem pontos) térképi elhelyezkedését a hirdető által megadott címadatok alapján. Az ingatlan teljes címe és tényleges térképi elhelyezkedése nem jelenik meg a hirdetésben.

Kizárólag Jófogás Bolt szerződéssel rendelkező gazdálkodó szervezetek adhatnak fel Vállalati hirdetéseket a „Trágya, műtrágya” alkategóriában – amely az „Otthon, háztartás” főkategóriában a „Növények” kategóriában érhető el -, valamint a „Tisztítószerek” alketgóriában, amely az „Otthon, háztartás” főkategóriában a „Háztartási felszerelések” kategóriában érhető el.

**A Szolgáltatás nyújtásának megtagadása**

A Társaság jogosult a Weblapra feltöltött hirdetést, egyéb tartalmat egyoldalúan, haladéktalanul eltávolítani, a Szolgáltatást megtagadni, végső soron a Felhasználó fiókját felfüggeszteni (tiltani), amennyiben a Weboldalra feltöltött tartalom a jelen Szabályzatba ütközik.

A Társaság haladéktalanul intézkedik minden olyan jogellenes hirdetés, illetve egyéb tartalom eltávolítása iránt, amelyre vonatkozóan az Európai Parlament és a Tanács digitális szolgáltatásokról szóló 2022/2065. rendelete (**DSA**), illetve az elektronikus kereskedelmi szolgáltatások, valamint az információs társadalommal összefüggő szolgáltatások egyes kérdéseiről szóló a 2001. évi CVIII. törvény (**Elkertv.**)13. §-a szerinti bejelentést kap. A Társaság továbbá haladéktalanul, de legkésőbb az online terrorista tartalom terjesztésével szembeni fellépésről szóló, az Európai Parlament és Tanács (EU) 2021/784 rendelete (TCO) szerinti eltávolító végzés kézhezvételétől számított egy órán belül eltávolítja a terrorista tartalmat. A Társaság a vonatkozó és mindenkor hatályos jogszabályi keretek között működik együtt a hatóságokkal a jogsértést elkövető személyek felelősségre vonása érdekében. A Felhasználók a jelen Szabályzatba ütköző hirdetéssel, tartalommal kapcsolatos bejelentéseiket a jelen Szabályzat „Nem megfelelő hirdetés, tartalom bejelentése” cím alatti rendelkezései szerint tehetik meg a Társaság felé. Amennyiben a Felhasználó azt észleli, hogy a szerzői jogi törvény által védett szerzői művén, előadásán, hangfelvételén, műsorán, audiovizuális művén, adatbázisán fennálló jogát, vagy védjegyét a Weblapon megjelenő információ sérti, illetve az a kiskorú (cselekvőképtelen kiskorú jogosult esetében a törvényes képviselője), akinek személyiségi jogát a szolgáltató által hozzáférhetővé tett információ – ide nem értve a hozzáférhetővé tett információ szabványosított címét – a megítélése szerint sérti, úgy az Elkertv. 13. §-a szerinti módon,  teljes bizonyító erejű magánokiratba vagy közokiratba foglalt értesítésével felhívhatja a Társaságot a jogát sértő tartalmú információ eltávolítására, amely esetben a Társaság a továbbiakban a „Nem megfelelő hirdetés, tartalom bejelentése” cím alatti rendelkezések szerint jár el.

A Weboldalra tilos azon tartalom, hirdetés feltöltése, amely

* jogszabályba ütközik, vagy jogellenes tevékenységet hirdet;

* bűncselekményt vagy szabálysértést valósít meg, illetve a TCO 2. cikk 7. pontja szerinti terrorista tartalomnak minősül;

* jóerkölcsbe ütközik vagy sérti a vonatkozó etikai normákat (ideértve különösen a Magyar Reklámetikai Kódex normáit);

* jogellenes, vagy jogellenes magatartással kapcsolatos, vagy jó erkölcsbe ütköző cselekményre hív fel vagy arra buzdít;

* mások jogát vagy jogos érdekét sérti, így különösen, ha védjegyoltalomba ütközik, ha szellemi alkotáshoz fűződő jogok illetve egyéb szerzői és szomszédos, illetve személyhez fűződő jogok sérelmét okozza vagy okozhatja;

* megtévesztő információt tartalmaz;

* a Társaság azt az általa képviselt etikai erkölcsi alapelvekkel ellentétesnek tartja;

* vagy a fentiek gyanúja felmerülhet.

A Weboldalon az alábbi dolgok, termékek, szolgáltatások hirdetése tiltott:

* Közbiztonságra fokozottan vagy különösen veszélyes eszközök, fegyverek, robbanószer, pirotechnikai anyagok, eszközök (tűzijáték), ilyen termékek alkatrésze, ideértve a fegyverviselési engedélyhez nem kötött eszközöket is (műtárgynak minősülő fegyverek, airsoft és paintball fegyverek, légpuska, légpisztoly, vipera, nundzsaku, sokkoló stb.), kivéve a rendeltetésszerűen a szokásos életvitelhez tartozó, háztartási és sport célok körében használt kés eszközöket (pl. konyhakés, búvárkés);

* Fogadással vagy egyéb szerencsejátékkal kapcsolatos rendszer vagy annak használatához kötődő segítség felajánlása;

* Aktiváló kódok, CD-kulcsok, az eredeti szoftvertől különválasztott regisztrációs számok;

* Olyan weboldalak, FTP szerverek, vagy az azokra vezető út megjelölése, amelyek veszélyes, illegális vagy nem birtokolható anyagok létrehozását vagy az ilyen anyagokhoz való hozzáférést elősegítő információkat tartalmaznak;

* On-line szolgáltatáshoz hozzáférést biztosító felhasználónevek, jelszavak, felhasználói fiókok adatai;

* Közösségi oldalak, e-mail szolgáltatások igénybe vételére jogosító meghívók;

* Postai úton történő rendelést lehetővé tevő katalógusok, kivétel ez alól az olyan, gyűjtők részére készült katalógusok, amelyek aktuális hirdetéseket nem tartalmaznak;

* Kitöltetlen garancia-levelek, igazolványok, bizonyítvány- vagy igazolvány-érvényesítő címkék;

* Diplomamunkák, szakdolgozatok, szakdolgozat vagy diplomamunka írását megkönnyítő szolgáltatások, különösen az ilyen dolgozatok megírására, a megírásukban történő segítségre, vagy kapcsolódó anyaggyűjtésre vonatkozóan;

* Hitelkártya és bankkártya, értékpapír, vagy más, nem átruházható fizetési eszköz, kivéve, ha egyértelműen azonosítható módon fizetési funkcióval már nem rendelkezik és gyűjtési céllal kerül eladásra;

* Alkohol, alkoholtartalmú italok;

* Dohánytermékek;

* Telefonszámok;

* Bármilyen tűz-vagy robbanásveszélyes anyag, vegyi anyagok; ideértve különösen, de nem kizárólagosan az ún. robbanóanyag prekurzornak számító anyagokat. Ezek olyan vegyi anyagok, amelyek önmagukban, ipari folyamatokban, de akár a mindennapi életben, háztartási célokra is használatosak, azonban bizonyos mennyiségben, sűrűségben vagy összetételben visszaélésre, így különösen, robbanóanyag előállítására alkalmasak. A robbanóanyag-prekurzorok forgalmazásáról és felhasználásáról, az 1907/2006/EK rendelet módosításáról, valamint a 98/2013/EU rendelet hatályon kívül helyezéséről szóló AZ EURÓPAI PARLAMENT ÉS A TANÁCS (EU) 2019/1148 rendelet alapján egyrészt fogyasztók részére egyáltalán nem bocsátható rendelkezésre, másrészt ezek üzleti célú felhasználásra szánt értékesítése esetén az értékesítő részére kötelezettségeket, így különösen a vevő ellenőrzésére vonatkozó kötelezettség feltételének teljesítésével lehetséges. A Weboldalon semmilyen vegyi anyag, így sem robbanóanyag prekurzornak számító, sem annak nem számító vegyi anyag sem magán, sem üzleti célú hirdetése nem megengedett. Amennyiben ilyen anyagot kíván hirdetni, azt ne a Weboldalon keresztül tegye, és tevékenysége során tartsa be a forgalmazásra vonatkozó kötelezettségeit, különösen a vevő ellenőrzésére és az ehhez legmegfelelőbb nyomtatvány-minta alkalmazására vonatkozó kötelezettséget (LINK: https://mkeh.gov.hu/kereskedelmi/robbanoanyag\_prekurzorok). További információ [ezen a linken](https://ugyfelszolgalat.jofogas.hu/eladas/hirdetesfeladas/robbanoanyag-prekurzorok/) érhető el.

* Gyógyszer, külön engedély, bejelentés birtokában, vagy meghatározott felhasználói kör részére forgalmazható anyagok és készítmények, kábítószer, tudatmódosító anyag, vagy bármely, ezekhez hasonló hatású szer;

* Lopott, vagy egyéb módon bűncselekményből származó, vagy jogellenesen forgalomba hozott termék;

* Hamis, vagy hamisított termék, vagy hamis márka-vagy eredetjelzés feltüntetését segítő termék, továbbá tilos ezen jellegre vagy tulajdonságra vonatkozó információk elhallgatása, visszatartása is;

* Emberi szerv, szövet, állati szerv vagy szövet, anyatej;

* Értékpapír vagy bármely más olyan pénzügyi eszköz, amellyel befektetést vagy pénzkihelyezést lehet végezni;

* Károkozó elemeket tartalmazó szoftver;

* Dekóder kártya vagy illegális dekódolásához felhasználható eszköz;

* Erotikus termékek, szexuális szolgáltatás, erotikus vagy szexuális jellegű masszázs;

* Kontaktlencse;

* Temetkezési helyek;

* Társkeresési szolgáltatás;

* Jóslás, kártyavetés, vagy ezotériához kapcsolódó szolgáltatások, ehhez hasonló szolgáltatások;

* Magánszemély által nyújtandó hitel, egyéb pénzügyi szolgáltatás;

* Meghatározott személy részére szóló, egyedi azonosítóval ellátott jegyek, bérletek;

* Cigaretta töltő berendezés;

* Telefonkészülékek függetlenítésére vonatkozó szolgáltatás;

* Fatermékek, így többek között a tűzifa, faház, fából készült mobil épület, fakerítés, fakorlát, fakapu, egyéb fából készült építőanyag (például fűrészáru) hirdetések, amelyeknek leírásában jogszabályi kötelezettség ellenére a hirdető nem vagy nem a valóságnak megfelelően tünteti fel az erdészeti hatóság által megállapított erdőgazdálkodói kódját vagy az EUTR technikai azonosító számát.  Jelen rendelkezés szempontjából faterméknek minősül az Európai Parlament és a Tanács a fát és fatermékeket piaci forgalomba bocsátó piaci szereplők kötelezettségeinek meghatározásáról szóló 995/2010/EU rendeletének (2010. október 20.) (továbbiakban: EUTR rendelet) 2. cikk a) pontja és melléklete alapján fatermékként definiált termék.
* Az olyan fából vagy fatermékekből előállított fatermékeket, vagy azok alkotórészeit hirdetni, amelyek életciklusukat lezárták, és amelyeket egyébként a hulladékokról szóló, 2008. november 19-i 2008/98/EK európai parlamenti és tanácsi irányelv (8) 3. cikkének 1. pontjában meghatározott hulladékként kezelnének.”
* Klímagázt tartalmazó vagy azzal működtetett, nem hermetikusan zárt alkalmazások hirdetései, amelyekben a hirdető nem nyilatkozik arról, hogy a fluortartalmú üvegházhatású gázokkal és az ózonréteget lebontó anyagokkal kapcsolatos tevékenységek végzésének feltételeiről szóló 14/2015. (II. 10.) Korm. rendelet alapján rendelkezik jogosultsággal klímagázt tartalmazó vagy azzal működtetett alkalmazások értékesítésére, valamint nem adja meg a Telepítési Tanúsítványának érvényes vonalkódját.
* Vetőmag, vetőburgonya hirdetések, amelyek nem felelnek meg a 48/2004 (IV. 21.) FVM rendelet 48/2004 (IV. 21.) FVM rendelet (FVM rendelet) 52. § (1) bekezdés azon előírásának (FVM rendelet) 52. § (1) bekezdés azon előírásának, amely szerint forgalomba hozni csak olyan kiszerelt vetőmagot, vetőburgonyát lehet, amely hivatalosan fémzárolt, az FVM rendelet mellékletekben előírt követelményeknek megfelelő vetőmagtételből származik, a csomagoláson szerepelnek az FVM rendelet 52.§ (4) bekezdés szerinti adatok, egyebek mellett pl. kiszerelő neve és címe, fémzárolási szám, fémzárolás időpontja, fajta stb. A hivatkozott FVM rendeletnek nem megfelelő termékek „vetőburgonya” elnevezéssel vagy vetőburgonyára utaló elnevezéssel (pl. vető méretű burgonya) nem hirdethetők a Weboldalon,
* Étkezési burgonya hirdetések, amelyek nem felelnek meg az étkezési burgonyára vonatkozó minőségi követelményekről szóló 34/2016 (V. 4.) FM rendeletben meghatározott minőségi és jelölési követelményeknek.
* Azok a friss zöldség és gyümölcs hirdetések, így étkezési burgonya hirdetések, amelyeket nem a 68/2007 (VII.26.) FVM-EüM-SZMM együttes rendelet 6.§ (1) bekezdése alapján regisztrált élelmiszer-vállalkozónak minősülő Felhasználó kíván a Weboldalon hirdetni.

A Társaság jogosult a Szolgáltatás nyújtásának megtagadására és a Felhasználót kizárni a Weblapon elérhető Szolgáltatások nyújtásából (a Felhasználó fiókjának illetve hirdetésének törlésével) akkor is, ha a Felhasználó a Társaság megítélése szerint nem rendeltetésszerűen használja a Weboldalt, illetve nem rendeltetésszerűen veszi igénybe a Társaság által nyújtott Szolgáltatást, így különösen, ha:

* a Felhasználó hatósági engedélyköteles tevékenységet kíván az oldal közvetítésével végezni anélkül, hogy erre jogosult volna;

* a Felhasználó kereskedelmi forgalomba nem hozható termékeket kínál eladásra, illetve olyan terméket ajánl fel, melynek forgalmazása bűncselekményt vagy szabálysértést valósít meg, vagy annak hirdetése vagy forgalmazása bejelentéshez vagy engedélyhez kötött;

* a Felhasználó jelen Szabályzat rendelkezéseibe, jogszabályba ütközően vagy tisztességtelen célokra használja a Weblapot vagy a Szolgáltatást;

* a Felhasználó a Weblapon olyan tartalmat tesz közzé vagy olyan tartalomra hivatkozik, illetve olyan módon használja a Weblapot, hogy az a Társaság (vagy cégcsoportjának) jogait, jó hírnevét, üzleti érdekeit, általános megítélését, így különösen termékeinek vagy szolgáltatásainak megítélését sérti vagy sértheti;

* a Felhasználó a Weblapot, illetve a Szolgáltatást olyan módon használja, hogy azzal harmadik személyek jogát, jogos érdekét sérti vagy sértheti;

* a Felhasználó bármilyen olyan alkalmazást vagy programot használ, amellyel a Weblap módosítható, vagy amely egyébéként veszélyezteti a Szolgáltatás rendeltetésszerű működtetését;

* a Felhasználó olyan rendszert vagy megoldást használ, amely a Szolgáltatás jogszabályokba ütköző módon történő felhasználást, vagy a Weblap üzemletetéséhez használt szerverek leállását célozza, teszi lehetővé vagy eredményezi;

* vagy a fentiek gyanúja felmerül.

Tilos a feladott hirdetés olyan módon történő módosítása vagy szerkesztése, amely a hirdetésben szereplő termék vagy szolgáltatás tárgyára tekintettel tartalmilag új hirdetés feladásának minősülne.

A Társaság az adó- és egyéb közterhekkel kapcsolatos nemzetközi közigazgatási együttműködés egyes szabályairól szóló 2013. évi XXXVII. törvény (a továbbiakban: **2013\. évi XXXVII. törvény**) 5. melléklet I/A/4. pontja szerinti platformüzemeltetőként ellenőrzi, hogy adott Felhasználó Értékesítőnek, illetve Jelentendő Értékesítőnek minősül-e, illetve figyelemmel kíséri ennek változását. A Társaság ehhez kapcsolódóan, a 2013. évi XXXVII. törvény szerint adatszolgáltatási kötelezettsége teljesítése érdekében jogosult megtagadni a Szolgáltatás további nyújtását attól a Felhasználótól, aki a Jelentendő Értékesítői minőségét megalapozó hirdetése feladatásakor elmulasztja a Társaság rendelkezésére bocsátani a szükséges adatokat és mindaddig, ameddig a Felhasználó adatszolgáltatási kötelezettségének nem tesz eleget.

A Társaság a DSA szerinti online platform üzemeltetőjeként biztosítja, hogy a DSA szerinti kereskedőnek minősülő Üzleti Felhasználók csak akkor használhassák a Weblapot üzenetek küldésére vagy termékek vagy szolgáltatások kínálására, ha ezen szolgáltatások e célból történő használatát megelőzően az érintett Üzleti Felhasználó a Társaság rendelkezésére bocsátotta a DSA 30. cikk (1) bekezdése szerinti információkat. A Társaság ehhez kapcsolódóan, a DSA szerint azonosítási kötelezettsége teljesítése érdekében jogosult megtagadni a jelen bekezdés szerinti Szolgáltatás további nyújtását attól az Üzleti Felhasználótól, aki elmulasztja a Társaság rendelkezésére bocsátani a szükséges adatokat és mindaddig, ameddig a Felhasználó adatszolgáltatási kötelezettségének nem tesz eleget.

A Társaság fenntartja magának a jogot, hogy a jelen pontban meghatározottak szerinti Szolgáltatás nyújtásának megtagadásával érintett Felhasználó IP címéről érkező ismételt regisztrációt megtagadja, illetve a már létrejött ismételt regisztrációt törölje, felé a Szolgáltatás nyújtását a jövőben megtagadja.

**4\. Nem megfelelő hirdetés, tartalom bejelentése**

Amennyiben a Felhasználó észleli, hogy valamely a Weboldalon elérhető hirdetés nem megfelelően lett feladva, vagy jogellenes, lehetősége van az adott hirdetés bejelentésére a Társaság felé a hirdetésnél erre a célra elhelyezett „Hirdetés jelentése” gomb megnyomásával megjelenő bejelentési formon keresztül.

A Felhasználó a Weboldalon elérhető egyéb, jogellenesnek feltételezett tartalom észlelése esetén a Társaság Ügyfélszolgálata részére tehet bejelentést.

A Felhasználó a nem megfelelő hirdetés, tartalom bejelentése során köteles megadni valós nevét és e-mail címét, kivéve, ha a bejelentés a 2011/93/EU irányelv 3–7. cikkében megjelölt bűncselekményre vonatkozik. A valós név, illetve az e-mail cím megadásának elmaradása a bejelentés mellőzését, azaz kivizsgálásának elmaradását eredményezi. Felhasználó a jogellenes tartalom bejelentése során köteles továbbá a bejelentésében pontosan megjelölni az általa jogellenesnek vélt tartalmat és azt, hogy miért tartja jogellenesnek, valamint nyilatkoznia szükséges arról, hogy jóhiszeműen jár el és a bejelentésében szereplő információk és állítások pontosak és hiánytalanok.

A Társaság a bejelentések kézhezvételét automatikusan visszaigazolja a bejelentéshez kapcsolt esetszám megküldésével. A Társaság a bejelentést annak kézhezvételétől számított 30 napon belül érdemben megvizsgálja és megválaszolja, illetve megteszi a szükséges intézkedéseket a bejelentés megalapozottsága esetén az abban foglaltak orvoslása érdekében.

**Tartalommoderáció**

A Társaság a Weboldalra feltöltött hirdetések és egyéb tartalom kapcsán minden esetben a technikai feltételek fennálltát (pl. karakterszám limit, ismétlődő hirdetések, feltöltött képek mérete, stb.), valamint a jelen Szabályzattal való összhangot ellenőrzi (tartalommoderáció) az alábbiak szerint.

Hirdetések esetében:

A Társaság saját kezdeményezésre, vagy bejelentés, illetve hatósági megkeresés alapján ellenőrzi a hirdetések jelen Szabályzatnak való megfelelését, meg nem felelés esetén pedig intézkedik a hirdetés láthatóságának korlátozása, illetve a hirdető Felhasználó felhasználói fiókjának felfüggesztése (tiltása) iránt (a továbbiakban együttesen: hirdetésmoderáció). 

A Társaság a saját kezdeményezésre végzett hirdetésmoderációt manuálisan (emberi felülvizsgálat útján), illetve részben automatizált eszközökkel végzi, egyrészt a hirdetés feladásakor (előzetes moderáció), másrészt utólag, a hirdetés közzétételét követően (utólagos moderáció). A Felhasználó által feladni kívánt hirdetést annak megjelenése előtt, a hirdetés feladását követő 48 órán belül, a Társaság egyrészt automatizált eszközökkel ellenőrzi annak megállapítása érdekében, hogy az megfelel-e a jelen Szabályzat szerinti formai és tartalmi feltételeknek. Amennyiben igen, a hirdetés automatikusan közzétételre kerül. Amennyiben nem, a hirdetés manuális moderációjára kerül sor. Másrészt a Társaság csalás érzékeny kategóriák/felhasználók hirdetései feladásának észlelése esetén manuális moderációt végez. Emellett a Társaság szúrópróbaszerű ellenőrzést is végez, amely alapján az automatizált ellenőrzés eredményétől függetlenül egyes hirdetések manuális moderációjára kerül sor.

Amennyiben előzetes moderáció során megállapításra kerül, hogy a hirdetés megfelelő, úgy közzétételre kerül, amennyiben azonban a hirdetés jelen Szabályzatba ütközik vagy jogellenes és emiatt nem megfelelő, a hirdetés közététele visszautasításra kerül.

A Weboldalon közzétett hirdetés utólagos moderációjára minden esetben manuálisan kerül sor. Manuális moderációra az automatikusan megjelölt hirdetések közül szúrópróbaszerűen kiválasztott hirdetések, a Felhasználók által bejelentett és a hatósági megkeresések szerinti hirdetések kerülnek.

Utólagos moderáció esetén, ha megállapításra kerül, hogy a hirdetés jelen Felhasználási Feltételekbe ütközik vagy jogellenes, a Társaság az alábbi intézkedésekről dönthet: tartalom teljes eltávolítása (visszautasítása), felhasználói fiók felfüggesztése (tiltása), egyéb esetben intézkedés elvégzésére nem kerül sor.

A Társaság a hirdetés közzétételének visszautasításáról, illetve a hirdetés utólagos moderációja esetén a fenti intézkedésekről és ennek okáról írásban tájékoztatja a hirdető Felhasználót az erre vonatkozó döntése megküldésével. A Társaság emellett, amennyiben valamely Felhasználó bejelentésére járt el, írásban tájékoztatja a bejelentést tevő Felhasználót a döntéséről.

Avatármoderáció

A Felhasználó által a felhasználói fiókban avatárként beállítani kívánt képek (avatár) minden esetben manuális moderációra kerülnek a hirdetések esetében alkalmazott előzetes tartalommoderálás szabályai szerint, illetve utólagos avatár moderációra is sor kerülhet a jelen Szabályzat hirdetés moderációra vonatkozó szabályai szerint. 

Amennyiben a tartalommoderációra az illetékes hatóság végzése alapján azért került sor, mert a tartalom a TCO 2. cikk 7. pontja szerinti terrorista tartalomnak minősül, a Társaság az érintett hirdető Felhasználó kérelmére tájékoztatást nyújt az eltávolítás okairól és a jogorvoslati lehetőségről, vagy a Felhasználó rendelkezésére bocsátja az eltávolítási végzés egy példányát, kivéve, ha az illetékes hatóság ennek a lehetőségét végzésében kizárta.

A Társaság felfüggeszti (tiltja) azon Felhasználók felhasználói fiókját, akik gyakran bocsátanak rendelkezésre nyilvánvalóan jogellenes tartalmat. A Társaság felfüggeszti a jogellenes hirdetésre, tartalomra vonatkozó bejelentések, és a bejelentések alapján hozott döntésekkel szembeni panaszok kivizsgálását azon Felhasználók esetében, akik gyakran tesznek nyilvánvalóan megalapozatlan bejelentéseket vagy tartalommoderáció miatti panaszokat. A Társaság a nyilvánvalóan jogellenes tartalom, illetve nyilvánvalóan megalapozatlan bejelentés, tartalommoderáció miatti panasz észlelésekor, döntésében tájékoztatja a Felhasználót a Szolgáltatás nyújtásának felfüggesztéséről, illetve a bejelentések, tartalommoderáció miatti panaszok kivizsgálásának beszüntetéséről, ennek időtartamáról. Azt, hogy az intézkedés alapjául szolgáló gyakoriság megvalósul-e, a Társaság minden esetben egyedileg mérlegeli és dönt róla, figyelemmel többek között a Felhasználó által közzétett jogellenes tartalom, megalapozatlan bejelentés, tartalommoderáció miatti panasz számosságára, az egyes tartalmak rendelkezésre bocsátása között eltelt időre, a tartalommoderációra vagy panaszra vonatkozó döntés, illetve a bejelentéssel kapcsolatos tájékoztatás kézhezvételét követő és a döntés alapjául szolgáló üggyel azonos, vagy ahhoz nagymértékben hasonló körülmények ismételt felmerülését.

A Társaság tartalommoderálással kapcsolatos döntésével érintett Felhasználó, illetve a bejelentő a döntéssel szemben annak kézhezvételét követő 6 (hat)hónapon belül panaszt nyújthat be a Társaság Ügyfélszolgálatára. A panaszban a Társaság kifogásolt döntésére (hirdetéskód feltüntetésével) szükséges hivatkozni annak megjelölésével, hogy a Társaság döntését milyen okból kifogásolja és határozott kérelmet kell tartalmaznia a kifogásolt döntés felülvizsgálatára. A Társaság csak az ezen feltételeknek megfelelő megkeresést tekinti a jelen pont szerinti panasznak és csak az alapján vizsgálja felül a panaszolt döntést. A Társaság nem tekinti panasznak azokat a megkereséseket, amelyek nem tartalmazzák az említett elemeket és csak a döntés értelmezésével, illetve a jövőben követendő eljárással kapcsolatosak, vagy abban egyet nem értésüket fejezik ki a felülvizsgálati igény jelzése nélkül. A Társaság a panasz kivizsgálása és az arról való döntés meghozatala során a jelen Szabályzat „Fogyasztói panasz, Ügyfélszolgálat” fejezetében foglaltak szerint jár el azzal, hogy a panaszt két héten belül vizsgálja ki és intézi el, ha a sérelmezett tartalom eltávolítására azért került sor, mert az a TCO 2. cikk 7. pontja szerinti terrorista tartalomnak minősül. Amennyiben a panasz megalapozott, a Társaság a panasznak helyt adva megváltoztatja a döntését, amelynek eredményeként a döntés visszavonására, vagy a döntés szerinti korlátozó intézkedés módosítására kerülhet sor. Amennyiben a panasz nem megalapozott, a Társaság a panaszt elutasítva helybenhagyja a korábbi döntését.  A Társaság panasz kapcsán hozott döntésével szemben további panasz benyújtására nincs lehetőség.

Amennyiben a bejelentő, vagy a döntéssel érintett hirdető Felhasználó a Társaság döntését nem tartja kielégítőnek a panaszával kapcsolatban, jogosult az Online Platform Vitarendező Tanácshoz fordulni a döntéssel kapcsolatos jogvita rendezése céljából, amelyet a Társaság részére benyújtott panasz révén nem sikerült rendezni. 

A fenti jogorvoslati lehetőségek mellett az érintett Felhasználó jogosult a hatáskörrel és illetékességgel rendelkező bíróság előtt polgári peres eljárást indítani a jogvitából származó követelése bíróság előtti érvényesítése érdekében, amelyre a Polgári Törvénykönyvről szóló 2013. évi V. törvény, valamint a Polgári Perrendtartásról szóló 2016. évi CXXX. törvény rendelkezései szerint van lehetősége.

**5\. Felelősség**

A Felhasználó által a Weblapon közzé- vagy elérhetővé tett tartalomért, ideértve a tartalom által okozott vagy egyébként azzal összefüggésben felmerült mindennemű vagyoni vagy nem vagyoni kárért kizárólag a Felhasználót terheli felelősség, a Társaság ezen tartalmakért nem felel és semmilyen felelősséget nem vállal. A Felhasználó köteles a Szolgáltatást jogszerűen és a mindenkor hatályos jogszabályok, továbbá a jelen Szabályzat rendelkezéseinek betartásával használni.

A Felhasználó a Szolgáltatások használatának megkezdésével tudomásul veszi, hogy Weblapra általa feltöltött információk, anyagok, szövegek, képek és adatok tartalmáért és jogszerűségéért felelősséggel tartozik. A Társaság a Weblap működtetését a DSA szerint végzi, és amennyiben azt észleli, hogy bármely tartalom a jelen Szabályzatba ütközhet, jogosult és a DSA-ban meghatározott esetekben, illetve szabályok szerint köteles a tartalom haladéktalan eltávolítására, és az érintett Felhasználó további jogellenes tevékenységének megakadályozására. A Társaság kizárja a felelősségét minden abból eredő kárért, ha törölte a Felhasználó jelen Szabályzat rendelkezéseibe ütköző magatartása vagy bármely jogszabály megsértése miatt a Felhasználó által a Weblapra feltöltött bármilyen tartalmat, vagy megakadályozta további Szabályzatba ütköző tevékenységét.

A Társaság nem felelős a Felhasználóknak a Weboldal használatával terjedő vagy elérhetővé váló káros tartalmak, így vírusok, férgek okozta károk és egyéb veszteségek vonatkozásában. A Társaság nem felelős továbbá a Felhasználók által megadott adatok hiányosságáért, vagy a hibásan megadott adatokból eredő bármilyen felmerülő hátrányért.

A Társaság jogosult megtenni a Felhasználó felelősségre vonásához szükséges lépéseket. A Társaság a hatályos jogszabályok szerint együttműködik a hatóságokkal a felelősségre vonás érdekében.

A Társaság nem avatkozik bele a Felhasználók közti jogvitákba, és a Felhasználók közti jogvitákkal kapcsolatos igények tekintetében a Társaság semmilyen felelősségét nem vállal. A Társaság nem vállal felelősséget a Felhasználók által a Weblapra feltöltött felhívásokkal, ajánlatokkal, továbbá az ezek alapján létrejövő szerződésekkel kapcsolatban, így különösen a termékek minőségéért, biztonságáért, jogszabályi megfelelőségéért, a termékek, szolgáltatások ellenőrzéséért, a Felhasználó teljesítéséért. Az eladott termékek kiszállítása, átadása és a pénzügyi teljesítés a Társaságtól függetlenül, a Felhasználók megállapodásának megfelelően zajlik le. A Társaság együttműködő partnerei útján lehetőséget teremthet a Felhasználók közötti ügyletek teljesítésének előmozdítására (pl. átadás, futárszolgáltatás). A Weblapon keresztül elérhető, együttműködő partnerek által biztosított szolgáltatásokat a Felhasználó saját felelősségére, a partnerszolgáltató által nyújtott feltételek szerint veszi igénybe és elismeri, hogy a Társaság nem felel e szolgáltatók által nyújtott szolgáltatásokért.

Amennyiben a Felhasználó magatartása következtében vagy azzal összefüggésben harmadik személy vagy bármely hatóság illetve bíróság a Társasággal szemben bármilyen igényt támaszt illetve eljárást indít, a Felhasználó köteles minden a Társaság által megkövetelt intézkedést megtenni és a Társaságnak megtéríteni minden olyan kárt, vagyoni hátrányt és költséget, amely a Társaságot a Felhasználó bármely jogellenes magatartása miatt vagy azzal összefüggésben éri, továbbá köteles a Társaság helyett közvetlenül helytállni ilyen igény vonatkozásában.

A Felhasználó kijelenti, hogy a Szolgáltatást a jelen Szabályzat rendelkezéseinek megfelelően veszi igénybe, a Szolgáltatás igénybevétele során megadott adatai a valóságnak megfelelnek. A Társaság kizárja felelősségét a Szolgáltatás igénybevétele során megadott téves, hibás vagy hamis adatok vagy e-mail cím megadásából eredő kárért.

A Felhasználó vállalja, hogy minden, a Társaság által megkövetelt intézkedést megtesz a Társaság jó hírnevének megóvása érdekében.

Az Adatvédelmi Tájékoztató szerint a Társaság méri a Felhasználók aktivitását. Felhasználó tudomásul veszi, hogy kizárólag Felhasználó felelőssége az, hogy a Weboldalt honnan, milyen eszközről nyitja meg, használja azt, így Felhasználó saját felelőssége, ha nem anonim ablakban nyitja meg a Weboldalt, amikor nem a saját gépét, eszközét használja a Weboldalra belépéshez, és emiatt a Felhasználó aktivitásáról a Társaság nem pontos adatokat gyűjt össze, ami miatt a Felhasználó számára nem kívánt, sértő tartalom is megjelenik hirdetésként a Felhasználó gépén vagy eszközén, vagy ilyen tartalmú hírlevelet kap. A Társaság kizárja minden felelősségét azokért a károkért, igényekért, amelyek a Felhasználót azáltal érhetik, ha a Weboldalt nem anonim ablakban nyitja meg a Weboldalt, amikor nem a saját gépét, eszközét használja a Weboldalra belépéshez.

**6\. Polgári jog, büntető jog, szerzői, szomszédos és kapcsolódó jogok védelme**

A Szolgáltatás bármely eleme és annak egésze, továbbá a Weblapon (és applikációkon keresztül) a Társaság által elérhetővé tett minden tartalom és elem (beleértve az egyes felhasználói hirdetéseket, a Weblap arculatát, tartalmának, adatbázisának összeválogatását, elrendezését és megjelenítését is, szoftverek, forráskódok, domain nevek, logók, stb) szerzői jogi (ideértve a szerzői jogi törvényben meghatározott szomszédos és kapcsolódó jogokat is, a továbbiakban szerzői jogok alatt ezeket is érteni kell) védelem – és bizonyos esetekben – iparjogvédelem, büntető jogi, illetőleg polgári jogi védelem alatt áll.

A szerzői jogok összessége és az adatbázis előállítójának védelmét megillető jogok, továbbá a fentiek szerint megszerzett felhasználási jog jogosultja a Társaság. A Weboldal bármely elemének engedély nélküli felhasználása, így különösen, de nem kizárólagosan másolása, módosítása vagy újbóli közzététele – a Társaság kifejezetten erre irányuló felhasználási engedélye nélkül – polgári jogi és büntetőjogi felelősséget von maga után. A Weboldalon (és applikációkon keresztül) elérhető adatbázis egésze vagy része nem használható fel és nem hasznosítható a Társaság előzetes írásbeli engedélye nélkül, így különösen tilos a Weboldal (és applikációk), az azon elérhető adatbázis egészét, bármely részét felhasználni, sokszorosítani, másolni, újraközölni, terjeszteni, kivéve, ha a Társaság egyértelműen és kifejezetten ehhez külön hozzájárul. A Társaság a Weboldal tartalmának bármely részét vagy egészét bármikor, előzetes értesítés nélkül módosíthatja vagy visszavonhatja.

A Felhasználó a Szolgáltatás megrendelésével egyben hozzájárul ahhoz, hogy a Társaság a Felhasználó által szolgáltatott, jogvédelem alatt álló elemeket (így különösen a hirdetés szövegét, a csatolt képeket) a Társaság ellenérték fizetése nélkül, a Szolgáltatások nyújtásához szükséges és hasznos mértékben és körben felhasználja, beleértve a másolás, többszörözés, tárolás, közzététel, terjesztés, illetve a szükséges mértékű átdolgozás jogát, valamint harmadik személy részére történő felhasználási jog engedését is időbeli és földrajzi (területi) korlátozás nélkül. A felhasználási jog kizárólagos, a Társaság jogosult azt továbbadni harmadik személy részére. A Felhasználó az előbbiekre való tekintettel csak az általa készített, vagy olyan egyéb anyagokat adhat át, amely kapcsán a kizárólagos felhasználási jogokkal rendelkezik.

**7\. Elállás és felmondás joga**

A jogszabály szerint fogyasztónak minősülő Felhasználó a 45/2014 (II. 26.) Kormányrendelet értelmében, a Kormányrendelet 20.§ szerinti, indokolás nélküli elállási és felmondás joggal rendelkezik a Szolgáltatások vonatkozásában. Az elállás és felmondás joga vonatkozásában a jelen pont szerinti Felhasználón a továbbiakban kizárólag a jogszabályok szerinti fogyasztónak minősülő felhasználó értendő. A Szolgáltatások nyújtására meghatározott, a Szolgáltatás nyújtásának megkezdésére vonatkozó határidőket a Társaság abban az esetben képes betartani, ha a Szolgáltatást igénybe vevő Felhasználó a Szolgáltatás kifizetését megelőzően tett, külön nyilatkozattal kifejezetten igényli és hozzájárul ahhoz, hogy a Társaság az igénybe vett szolgáltatás nyújtását a jelen Szabályzat szerint, a Kormányrendelet 20.§ (2) bekezdés b) pont szerinti időtartam (14 nap) lejártát megelőzően kezdje meg, azzal, hogy az igénybe vett szolgáltatás teljesítésének megkezdését követően a Felhasználót a szolgáltatásra vonatkozó szerződés megkötésének időpontjától számított 14 napon belül elállási jog nem, de indokolás nélküli felmondási jog illeti meg a szerződés fennállása alatt, a Szolgáltatás egészének teljesítéséig, amelyet követően a Felhasználó a felmondási jogát elveszíti.

A fogyasztó Felhasználó az elállási, vagy felmondási jogát a jelen Szabályzat mellékletében meghatározott nyilatkozat-minta felhasználásával, vagy az erre vonatkozó egyértelmű nyilatkozata útján gyakorolhatja, amelyet köteles eljuttatni a Társaság jelen Szabályzat 8. pontjában meghatározott postacímére vagy elektronikus levelezési címére. Az elállási, illetve felmondási jog gyakorlása határidőben történik, ha azt a Felhasználó az előző bekezdésben foglalt határidőben elküldi. A Felhasználót terheli annak bizonyítása, hogy az elállási vagy felmondási jogát a jelen bekezdésben foglaltakkal összhangban gyakorolta.

A Felhasználó jelen pont szerinti jogának gyakorlása esetén a Társaság haladéktalanul, de legkésőbb a nyilatkozat Társaság által történő kézhezvételétől számított 14 napon belül a Társaság visszatéríti a Felhasználó által teljesített valamennyi ellenszolgáltatást, ideértve a teljesítéssel összefüggésben felmerült költségeket is. A visszatérítés során a Társaság az eredeti ügylet során alkalmazott fizetési móddal egyező fizetési módot alkalmaz, kivéve, ha a Felhasználó más fizetési mód igénybevételéhez kifejezett hozzájárulását adja, de a fogyasztót ebből adódóan semmilyen többletdíj nem terhelheti. A Barion szolgáltatás alkalmazásával megfizetett díjakat a Társaság minden esetben Barion szolgáltatással küldi vissza a Felhasználó részére.

A felmondási jog gyakorlása esetén a megszűnés időpontjáig nyújtott szolgáltatásért járó ellenértékre a Társaság jogosult, az ellenérték azon része, amely meghaladja a ténylegesen nyújtott szolgáltatás értékét, a Felhasználó részére visszatérítésre kerül. Az elszámolás során a Felhasználó által arányosan fizetendő összeget az ellenszolgáltatás adóval növelt teljes összege alapján kell kiszámítani. Ha azonban a Felhasználó bizonyítja, hogy a teljes összeg túlzottan magas, az arányos összeget a szerződés megszűnésének időpontjáig teljesített szolgáltatások piaci értéke alapján kell kiszámítani. A piaci érték megállapításánál az azonos tevékenységet végző vállalkozások azonos szolgáltatásának a szerződés megkötésének időpontja szerinti ellenértékét kell figyelembe venni. A Felhasználó a jelen bekezdés szerint köteles megtéríteni a Társaság ésszerű költségeit, ha a Társaság a teljesítést a Felhasználó kifejezett, előzetes kérésére kezdte meg, és a teljesítés megkezdését követően gyakorolja a felmondási jogát.

**8\. Fogyasztói panasz, Ügyfélszolgálat**

**A Társaság ügyfélszolgálata a Szolgáltatással (pl. Weboldal működése, felhasználói regisztráció, felhasználói fiók létrehozása, Weboldal működési beállítások, keresőrendszer használata, hirdetés közzététele, hirdetéskiemelések, Weboldal üzenetküldő rendszerének használata, stb.) kapcsolatos panaszokat, felhasználói megkereséseket fogadja.**

**A fogyasztónak minősülő Magánszemély Felhasználók a Weboldalon valamely Üzleti Felhasználó által meghirdetett termékkel, illetve szolgáltatással kapcsolatos fogyasztói jogaikat a hirdetésben megjelölt Üzleti Felhasználóval, mint vállalkozással szemben érvényesíthetik, fogyasztói panaszaikat kizárólag közvetlenül az Üzleti Felhasználó részére küldhetik meg. A Magánszemély Felhasználók a Weboldalon meghirdetett termékekkel, szolgáltatásokkal kapcsolatos fogyasztói jogaikat a Társasággal szemben nem érvényesíthetik.** 

A Társaság Ügyfélszolgálata a Szolgáltatással kapcsolatos panaszokat, felhasználói megkereséseket magyar és angol nyelven az alábbi címeken fogadja:

* elektronikus levelezési cím: ugyfelszolgalat@jofogas.hu
* telefonszám: +36 1 808 8288

* székhely, postacím:1134 Budapest, Váci út 49. III. emelet

A Társaság a Szolgáltatásokkal kapcsolatos panaszokat elsődlegesen az ugyfelszolgalat@jofogas.hu e-mail címen, vagy postán fogadja, a telefonos ügyfélszolgálat a Szolgáltatás használatára vonatkozó felhasználói kérdések megválaszolására szolgál, a Szolgáltatások könnyebb használata érdekében. A panaszokat, felhasználói kérdéseket az ügyfélszolgálat írásban, a Felhasználó által megadott e-mail címre küldött e-mail útján válaszolja meg. A Társaság a székhelyen személyesen nem fogad panaszokat.

Tájékoztatjuk a Felhasználókat, hogy a +36 1 808 8288 telefonszámon elhangzó beszélgetéseket Társaságunk hangfelvételen rögzíti.  A hangfelvétel rögzítésének és az ügyfélszolgálatra érkező kommunikáció rögzítésének célja a Szolgáltatásra vonatkozó jogviszonyok teljesítése figyelemmel kísérésének biztosítása, és a felek jogos érdekeinek bizonyításához szükséges tények rögzítése, igazolása, a felhasználói észrevételek és kérdések az érintett számára megnyugtató módon kezelése, megválaszolása, kivizsgálása, valamint a Felhasználó jogainak védelme. A Társaság kivizsgálja az összes, a fentiek szerint beérkező panaszt, és mindent megtesz annak érdekében, hogy az a lehető legrövidebb időn belül érdemben orvoslásra vagy megválaszolásra kerüljön.

A panaszkezelésre a fogyasztóvédelemről szóló 1997. évi CLV. törvény rendelkezései irányadók. A Társaság a szóbeli panaszt azonnal megvizsgálja, és szükség szerint orvosolja. Ha a Felhasználó a panasz kezelésével nem ért egyet, vagy a panasz azonnali kivizsgálása nem lehetséges, a Társaság a panaszról és az azzal kapcsolatos álláspontjáról haladéktalanul jegyzőkönyvet vesz fel, és a telefonon vagy egyéb elektronikus hírközlési szolgáltatás felhasználásával közölt szóbeli panasz esetén a Felhasználónak legkésőbb az érdemi válasszal egyidejűleg megküldi, egyebekben pedig az írásbeli panaszra vonatkozóak szerint jár el. A telefonon vagy elektronikus hírközlési szolgáltatás felhasználásával közölt szóbeli panaszt a Társaság egyedi azonosítószámmal látja el.

A jegyzőkönyv tartalmazza a Felhasználó nevét, lakcímét, a panasz előterjesztésének helyét, idejét, módját, a panasz részletes leírását, a Felhasználó által bemutatott iratok, dokumentumok jegyzékét, a Társaság álláspontját (negatív válasz esetén az indokolást), a jegyzőkönyvet felvevő személy nevét és aláírását, a keltezést (hely és idő) és a panasz egyedi azonosítószámát, amennyiben azt szóban vagy elektronikus hírközlési szolgáltatás útján tették.

Az írásbeli panaszt a Társaság annak beérkezését követő harminc napon belül írásban érdemben megválaszolja és intézkedik annak közlése iránt. Ennél rövidebb határidőt jogszabály, hosszabb határidőt törvény állapíthat meg. A panaszt elutasító álláspontját a Társaság megindokolja.

A Társaság a panaszról felvett jegyzőkönyvet és a válasz másolati példányát a fogyasztóvédelmi törvény értemében három évig köteles megőrizni, és azt az ellenőrző hatóságoknak kérésükre bemutatni. A panaszok elbírálására a Társaság és a Felhasználó közötti jogviszony keretén belül, egyedileg kerülhet sor.

A panasz elutasítása esetén a Társaság írásos válasza indoklást tartalmaz, továbbá abban a Társaság tájékoztatást nyújt a jogorvoslati lehetőségekről, így a Társaság a Felhasználót írásban tájékoztatja arról, hogy panaszával – annak jellege szerint – mely hatóság vagy a békéltető testület eljárását kezdeményezheti. A tájékoztatásnak tartalmaznia kell továbbá az illetékes hatóság, illetve a fogyasztó Felhasználó esetén a fogyasztó lakóhelye vagy tartózkodási helye szerinti békéltető testület székhelyét, telefonos és internetes elérhetőségét, valamint levelezési címét, valamint hogy a Társaság fogyasztói jogvita rendezése érdekében igénybe veszi-e a békéltető testületi eljárást.

Amennyiben a Felhasználó a Társaság válaszát nem tartja kielégítőnek, különösen az alábbi szervekhez fordulhat jogorvoslatért:

(i) Fogyasztónak minősülő Felhasználó – ha önálló foglalkozásán és gazdasági tevékenységi körén kívül eső célok érdekében jár el a Szolgáltatások igénybevétele során – a területi gazdasági kamarák mellett működő független, a fogyasztó lakóhelye vagy tartózkodási helye szerinti illetékes, vagy kérelme szerinti **békéltető testület**hez fordulhat, melyek hatáskörébe tartozik a fogyasztó és a Társaság közötti, a termék minőségével, biztonságosságával, a termékfelelősségi szabályok alkalmazásával, a szolgáltatás minőségével, továbbá a felek közötti szerződés megkötésével és teljesítésével kapcsolatos vitás ügy (a továbbiakban: **fogyasztói jogvita**) bírósági eljáráson kívüli rendezése, és e célból egyezség létrehozásának megkísérlése, ennek eredménytelensége esetén pedig az ügyben döntés hozatala a fogyasztói jogok egyszerű, gyors, hatékony és költségkímélő érvényesítésének biztosítása érdekében. Az eljárás részletes szabályait a fogyasztóvédelemről szóló 1997. évi CLV tv. Tartalmazza.

(ii) Fogyasztónak minősülő Felhasználó a fogyasztóvédelmi hatósági jogkört gyakorló, **területileg illetékes kormányhivatalokhoz** fordulhat. Az eljárás részletes szabályait a fogyasztóvédelemről szóló 1997. évi CLV tv., a 2008. évi XLVII tv., valamint a 2004. évi CXL tv. tartalmazza. A Fogyasztó a lakóhelye szerint illetékes kormányhivatalhoz fordulhat az alábbi linken megtalálható elérhetőségeken: [http://www.kormanyhivatal.hu/](http://www.kormanyhivatal.hu/)[.](http://jarasinfo.gov.hu/).

A Társaság a fogyasztói jogvita rendezése érdekében igénybe veszi a békéltető testület előtti eljárást, amelyben együttműködési kötelezettség terheli, illetve ilyen eljárás megindulása esetén együttműködik a kérelmezővel és a békéltető testülettel. A Társaság ugyanakkor tájékoztatja a Felhasználókat, hogy sem a székhelye szerinti békéltető testületnél, sem az MKIK-nál – valamennyi békéltető testületre kiterjedő hatállyal – nem tett az Fgytv. 36/C. §-a szerinti általános alávetési nyilatkozatot. A Társaság a békéltető testület tanácsa döntésének kötelezésként történő elfogadásával kapcsolatban az eljárás megindításáról való tudomásszerzését követően, válasziratában nyilatkozik (eseti alávetés).

(iii) Fogyasztónak minősülő Felhasználó online vitarendezési platformon keresztül kezdeményezheti a közötte és a Társaság között létrejött online adásvételi vagy szolgáltatási szerződéssel kapcsolatos panasza bírósági úton kívüli rendezését.

Fogyasztónak minősülő Felhasználó a fogyasztói jogviták online rendezéséről, valamint a 2006/2004/EK rendelet és a 2009/22/EK irányelv módosításáról (fogyasztói online vitarendezési irányelv) szóló 524/2013/EU rendelet (a továbbiakban: **Rendelet**) szerinti online vitarendezési platformon keresztül kezdeményezheti a közötte és a Társaság, mint online piac- és egyéb szolgáltatást nyújtó szolgáltató között létrejött online szolgáltatási szerződéssel kapcsolatban felmerült fogyasztói jogvita bírósági úton kívüli rendezését. Az online vitarendezési platform az alábbi linken keresztül érhető el: [http://ec.europa.eu/odr](http://ec.europa.eu/odr).

A Fogyasztónak minősülő Felhasználónak javasolt elsőként a Társasággal közvetlenül felvenni a kapcsolatot a jelen pontban foglaltak alapján panasza kivizsgálása és kezelése érdekében, ugyanakkor ez nem előfeltétele az online vitarendezési platform igénybevételének. Emellett a közvetlen kapcsolatfelvételre az online vitarendezési platform is lehetőséget nyújt, amely esetben a megegyezésre 90 nap áll rendelkezésre.

Amennyiben az online adásvételi vagy szolgáltatási szerződést a Fogyasztónak minősülő Felhasználó nem az Társasággal, hanem valamely a Társaság weboldalán hirdető másik kereskedővel, vagy szolgáltatóval kötötte és a panasz ezzel az online szerződéssel kapcsolatban merült fel, úgy az online vitarendezés ezen szerződéses partnerrel, nem pedig a Társasággal szemben kezdeményezhető.

Az online vitarendezési platformmal kapcsolatos részletes tájékoztatás az alábbi linken keresztül érhető el: https://ec.europa.eu/consumers/odr/main/?event=main.home.howitworks Amennyiben azonban az eljárással kapcsolatban további kérdés merülne fel, a tagállami kapcsolattartói pont tanácsadói segítséget nyújtanak. A magyar kapcsolattartói pont elérhetőségei:

Innovációs és Technológiai Minisztérium

Európai Fogyasztói Központ

Levelezési cím: 1440 Budapest, Pf. 1.

E-mail cím: odr@im.gov.hu

Telefon: +3617955233

Weboldal: https://www.magyarefk.hu 

**Melléklet**

**Tájékoztatás a Társaság által nyújtott szolgáltatás fogyasztónak minősülő Felhasználók által történő igénybevétele során a Társaság és a fogyasztónak minősülő Felhasználó, mint távollevők között kötött szerződés vonatkozásában:**

**1.Társaság neve:**

Jófogás Kft.

**2.Társaság székhelye, üzleti tevékenységének helye, postai címe, a Társaság adatai:**

székhelye:1134 Budapest, Váci út 49. III. emelet

telefonszáma: +36 1 808 8288 

elektronikus levelezési címe: ugyfelszolgalat@jofogas.hu

cégbíróság: Fővárosi Törvényszék Cégbírósága

cégjegyzékszám: 01-09-433149

adószáma: HU32606753-2-41

**3.A szerződés szerinti szolgáltatásokért járó ellenszolgáltatás adóval megnövelt teljes összegéről, valamint az esetlegesen ezen felül felmerülő valamennyi további költségről:**

Az árakról és egyéb költségekről az egyes szolgáltatások megrendelése előtt a Weblapon tájékozódhat.

**4.Határozatlan időre szóló szerződés vagy előfizetést magában foglaló szerződés esetében:**

A Társaság által megadott szolgáltatási díj összege a szolgáltatás valamennyi költséget tartalmazza. Ha a teljes ellenértéket, vagy valamennyi költség előre történő kiszámítása nem lehetséges, a Felhasználó az ellenérték összegének kiszámításának módjáról a szerződés megkötését és a szolgáltatás megrendelését megelőzően tájékoztatást kap.

**5.A szerződés megkötéséhez alkalmazott távollévők közötti kommunikációt lehetővé tévő eszköz használatának díja:**

Nincs ilyen.

**6.A teljesítés feltételeiről, így különösen a fizetésről és a teljesítési határidőről, valamint a vállalkozás panaszkezelési módjáról:**

A teljesítés feltételeivel, a fizetési módokkal, teljesítési határidőkkel és a panaszkezeléssel kapcsolatos információk a Szabályzatban elérhetőek (2. és 8. pont).

**7.A fogyasztót megillető elállási és felmondási jog:**

A fogyasztó és a vállalkozás közötti szerződések részletes szabályairól szóló **45/2014. (II. 26.) Korm. rendelet** értelmében Ön 14 napon belül jogosult indokolás nélkül elállni e szerződéstől. Hasonlóképpen, ha a szolgáltatás nyújtására irányuló szerződés esetén a szerződés teljesítése megkezdődött, Ön jogosult 14 napon belül indokolás nélkül felmondani a szerződést.

Az elállási/felmondási határidő a szerződés megkötésének napjától számított 14 nap elteltével jár le.

Ha Ön elállási/felmondási jogával élni kíván, elállási/felmondási szándékát tartalmazó egyértelmű nyilatkozatát köteles eljuttatni (például postán, telefaxon vagy elektronikus úton küldött levél útján) az alábbi címre. 

* Jófogás Kft. 
* 1134 Budapest, Váci út 49.
* elektronikus levelezési címe: ugyfelszolgalat@jofogas.hu

Ebből a célból felhasználhatja az alábbiakban található elállási/felmondási nyilatkozat-mintát is.

A fogyasztó és a vállalkozás közötti szerződések részletes szabályairól szóló **45/2014. (II. 26.) Korm. rendelet** értelmében a távollévők között kötött szerződés esetén a fogyasztót a szolgáltatás nyújtására irányuló szerződés esetében a szerződés megkötésének napjától számított tizennégy napon belül indokolás nélküli elállási jog illeti meg. Olyan távollévők között kötött szerződés esetén, amely szolgáltatás nyújtására irányul, ha az Üzemeltető a teljesítést a fogyasztó kifejezett, előzetes kérésére kezdte meg, és ezen nyilatkozat megtételét követően a teljesítés megkezdődik, a fogyasztót az előzőekben meghatározott határidőn belül indokolás nélküli felmondási jog illeti meg.

A fogyasztó elállási/felmondási jogát az alábbiakban található nyilatkozat-minta felhasználásával vagy az erre vonatkozó egyértelmű nyilatkozat útján gyakorolhatja. Az Üzemeltető internetes honlapján is biztosíthatja a fogyasztó számára az elállási/felmondási jog gyakorlását. Ebben az esetben az Üzemeltető tartós adathordozón haladéktalanul visszaigazolja a fogyasztói nyilatkozat megérkezését. Az elállási/felmondási jogot határidőben érvényesítettnek kell tekinteni, ha a fogyasztó nyilatkozatát a fentiekben meghatározott határidő lejárta előtt elküldi. A fogyasztót terheli annak bizonyítása, hogy az elállási/felmondási jogot a jelen bekezdéssel összhangban gyakorolta. Ha a fogyasztó elállási/felmondási jogával élni kíván, elállási/felmondási szándékát tartalmazó egyértelmű nyilatkozatát vagy a nyilatkozat-mintát köteles eljuttatni (például postán vagy elektronikus úton küldött levél útján) a jelen tájékoztató 4. pontjában meghatározott címre.

Elállási/Felmondási nyilatkozatminta

(csak a szerződéstől való elállási/felmondási szándék esetén töltse ki és juttassa vissza)

Címzett \[ide a vállalkozásnak a nevét, postai címét és elektronikus levelezési címét kell beillesztenie\]:

Címzett:

Alulírott/ak kijelentem/kijelentjük, hogy gyakorlom/gyakoroljuk elállási/felmondási jogomat/jogunkat az alábbi árutermék/ek adásvételére vagy az alábbi szolgáltatás nyújtására irányuló szerződés tekintetében:

Szerződéskötés időpontja /átvétel időpontja:

A fogyasztó(k) neve:

A fogyasztó(k) címe:

A fogyasztó(k) aláírása: (kizárólag papíron tett nyilatkozat esetén)

Kelt:

Ön határidőben gyakorolja elállási/felmondási jogát, ha a fent megjelölt határidő lejárta előtt elküldi elállási/felmondási nyilatkozatát.

A fogyasztót terheli annak bizonyítása, hogy az elállási/felmondási jogot a jelen bekezdéssel összhangban gyakorolta.

**8.Az elállás/felmondás joghatásai**

Ha Ön eláll ettől a szerződéstől, haladéktalanul, de legkésőbb az Ön elállási nyilatkozatának kézhezvételétől számított 14 napon belül visszatérítjük az Ön által teljesített valamennyi ellenszolgáltatást, ideértve a fuvarozási költséget is (kivéve azokat a többletköltségeket, amelyek amiatt merültek fel, hogy Ön az általunk felkínált, legolcsóbb szokásos fuvarozási módtól eltérő fuvarozási módot választott.) A visszatérítés során az eredeti ügylet során alkalmazott fizetési móddal egyező fizetési módot alkalmazunk, kivéve, ha Ön más fizetési mód igénybevételéhez kifejezetten a hozzájárulását adja; e visszatérítési mód alkalmazásából kifolyólag Önt semmilyen többletköltség nem terheli.

Ha a fogyasztó eláll a távollevők között kötött szerződéstől, az Üzemeltető haladéktalanul, de legkésőbb az elállásról való tudomásszerzésétől számított tizennégy napon belül visszatéríti a fogyasztó által ellenszolgáltatásként megfizetett teljes összeget, ideértve a teljesítéssel összefüggésben felmerült költségeket is. A megfelelő elállás vagy felmondás esetén az Üzemeltető a fogyasztónak visszajáró összeget a fogyasztó által igénybe vett fizetési móddal megegyező módon téríti vissza. A fogyasztó kifejezett beleegyezése alapján az Üzemeltető a visszatérítésre más fizetési módot is alkalmazhat, de a fogyasztót ebből adódóan semmilyen többletdíj nem terhelheti.

Ha Ön kérte, hogy a felmondási határidőn belül kezdődjön meg a szolgáltatás teljesítése, felmondása esetén Ön köteles megtéríteni számunkra a szerződés megszűnésének időpontjáig arányosan teljesített szolgáltatásért járó összeget. Hasonlóképpen visszatérítjük az Ön által nyújtott ellenszolgáltatás azon részét, amely meghaladja az általunk nyújtott szolgáltatás ellenértékét.

**9\. A kellékszavatosságra és a termékszavatosságra vonatkozó jogszabályi kötelezettségről:**

**Kellékszavatosság**

Fogyasztó a Társaság hibás teljesítése esetén a Társasággal szemben kellékszavatossági igényt érvényesíthet a Polgári Törvénykönyv szabályai szerint.

A kötelezett hibásan teljesít, ha a szolgáltatás a teljesítés időpontjában nem felel meg a szerződésben vagy jogszabályban megállapított minőségi követelményeknek. Nem teljesít hibásan a kötelezett, ha a jogosult a hibát a szerződéskötés időpontjában ismerte, vagy a hibát a szerződéskötés időpontjában ismernie kellett.

Fogyasztó és a Társaság közötti szerződés esetén az ellenkező bizonyításáig vélelmezni kell, hogy a teljesítést követő hat hónapon belül a fogyasztó által felismert hiba már a teljesítés időpontjában megvolt, kivéve, ha e vélelem a dolog természetével vagy a hiba jellegével összeegyeztethetetlen.

Olyan szerződés alapján, amelyben a felek kölcsönös szolgáltatásokkal tartoznak, a Társaság a hibás teljesítésért kellékszavatossággal tartozik.

Kellékszavatossági igénye alapján a fogyasztó választása szerint: Kérhet kijavítást vagy kicserélést, kivéve, ha az ezek közül a fogyasztó által választott igény teljesítése lehetetlen vagy a Társaság számára más igénye teljesítéséhez képest aránytalan többletköltséggel járna. Ha a kijavítást vagy a kicserélést nem kérte, illetve nem kérhette, úgy igényelheti az ellenszolgáltatás arányos leszállítását vagy a hibát a Társaság költségére fogyasztó is kijavíthatja, illetve mással kijavíttathatja vagy – végső esetben – a szerződéstől is elállhat. Jelentéktelen hiba miatt elállásnak nincs helye.

Fogyasztó a választott kellékszavatossági jogáról másikra térhet át. Az áttéréssel okozott költséget köteles a Társaságnak megfizetni, kivéve, ha az áttérésre a Társaság adott okot, vagy az áttérés egyébként indokolt volt.

Fogyasztó a hiba felfedezése után késedelem nélkül köteles a hibát a Társasággal közölni. Fogyasztó és a Társaság közötti szerződés esetén a hiba felfedezésétől számított két hónapon belül közölt hibát késedelem nélkül közöltnek kell tekinteni. A közlés késedelméből eredő kárért a fogyasztó felelős. Fogyasztó és a Társaság közötti szerződés esetén a fogyasztó kellékszavatossági igénye a teljesítés időpontjától számított két év alatt évül el.

**10\. Az értékesítés utáni ügyfélszolgálati és egyéb szolgáltatások, valamint a jótállás fennállásáról és annak feltételeiről:**

Fogyasztó a szolgáltatásokkal kapcsolatban az adott témakörben a jelen Szabályzatban illetve a Weboldalon megadott e-mail címeken és telefonszámokon is kérhet tájékoztatást. A Társaság jótállást nem vállal.

**11\. Rendelkezésre áll-e a fogyasztókkal szembeni tisztességtelen kereskedelmi gyakorlat tilalmáról szóló törvény szerinti magatartási kódex, illetve a magatartási kódexről történő másolatkérés módjáról:**

Nem áll rendelkezésre, másolatkérésre nincs mód.

**12\. Határozott időre szóló szerződés esetén a szerződés időtartamáról, határozatlan időre szóló szerződés esetén a szerződés megszüntetésének feltételeiről:**

Ellenkező kikötés hiányában a határozott időre szóló szerződések esetén a szerződés időtartama 30 nap. Ellenkező kikötés hiányában a határozatlan időre szóló szerződések esetén a felmondási idő 1 hónap. A szerződések megszüntethetők a szerződő felek közös megegyezésével is. A szerződések időtartamáról és megszüntetésének feltételeiről bővebb tájékoztatás a Szabályzatban, illetve az egyes szolgáltatások megrendelése előtt a Weboldalon található.

**13\. Határozott időre szóló olyan szerződés esetén, amely határozatlan időtartamúvá alakulhat át, az átalakulás feltételeiről, és az így határozatlan időtartamúvá átalakult szerződés megszüntetésének feltételeiről:**

Nincs ilyen eset.

**14\. A fogyasztó kötelezettségeinek szerződés szerinti legrövidebb időtartamáról:**

Ingyenes hirdetések esetében a hirdető a regisztrációját, hirdetését bármikor jogosult törölni. A felhasználói fiók törlésére a törlés kezdeményezésének napjától számított 1 hónap alatt kerül sor, a hirdetések Felhasználó által történő törlés kezdeményezésének hiányában a megjelenítéstől számított legkésőbb 90 napos időtartamot követően archiválásra, az archiválás időtartamát, azaz 179 napot követően, a Szabályzatban meghatározott esetek kivételével, törlésre kerülnek. A hirdetések Felhasználó által kezdeményezett törlésére az erre vonatkozó funkció használatát követő 14 napos törlés alatti periódus leteltét követően a 15. napon törlődnek. Ellenszolgáltatás ellenében nyújtott szolgáltatások esetében a fogyasztó kötelezettségeinek szerződés szerinti legrövidebb időtartama az adott Szolgáltatás tekintetében a Weboldalon található. A fentiektől eltérően a jármű kategóriában eredetileg a Használtautó Weboldalon egyedi szerződés alapján feladott Hirdetések esetében a Társaság addig az időtartamig teszi közzé a hirdetést a Weboldalon, amíg az a Használtautó Weboldalon szerepel.

**15\. A fogyasztó által a vállalkozás kérésére fizetendő vagy biztosítandó letét vagy egyéb pénzügyi biztosíték nyújtásáról és annak feltételeiről:**

Nincs.

**16\. A digitális adattartalom működéséről, valamint az alkalmazandó műszaki védelmi intézkedésről:**

A Társaság az általa üzemeltetett szerverek és egyéb berendezések (ide értve a hálózati kapcsolatot biztosító infrastruktúra) működtetése kapcsán a szükséges védelmi intézkedéseket elvégezte és fenntartja, folyamatosan frissíti. Az eszközök megfelelő elhelyezéséről és fizikai védelméről gondoskodik, illetve a tárolt adatok védelmét a rendelkezésre álló informatikai eszközök révén biztosítja.

**17\. A digitális adattartalom hardverrel és szoftverrel való együttműködési képességéről a vállalkozástól ésszerűen elvárható ismereteknek megfelelően:**

A Weboldal böngészőfüggetlen módon együttműködik valamennyi operációs rendszerrel, illetve megjeleníthető mobilalkalmazásokon (mobiltelefon, táblagép). 

**18\. Arról, hogy az árukat, szolgáltatásokat vagy digitális tartalmat kínáló harmadik fél vállalkozásnak minősül-e, a harmadik fél által az online piac szolgáltatója felé tett nyilatkozat alapján:**

Amennyiben a Weboldalon egy hirdetés hirdetője a hirdetések listázási oldalán Üzleti Felhasználóként, Autó kategóriában Kereskedésként, Ingatlan kategóriában Ingatlanközvetítőként, állás, bolt és minden más, előzőekben nem felsorolt kategóriában Üzleti Felhasználóként jelenik meg, akkor ezen hirdetőket az általuk a Társaság felé tett nyilatkozatuk alapján vállalkozásnak kell tekinteni.

**19\. Arról, hogy ha az árukat, szolgáltatásokat vagy digitális tartalmat kínáló harmadik fél nem vállalkozás, a szerződés nem minősül fogyasztói szerződésnek, és a fogyasztót ez esetben nem illetik meg a fogyasztói jogok:**

Amennyiben a Weboldalon egy hirdetés hirdetője a hirdetések listázási oldalán nem Üzleti Felhasználóként, Autó kategóriában nem Kereskedésként, Ingatlan kategóriában nem Ingatlanközvetítőként, állás, bolt és minden más, előzőekben nem felsorolt kategóriában nem Üzleti Felhasználóként jelenik meg, akkor ezen hirdetők nem minősülnek vállalkozásnak, így a velük megkötött szerződés nem minősül fogyasztói szerződésnek, és a fogyasztót ez esetben nem illetik meg a fogyasztói jogok.

**20\. A jogszabályi előírás vagy a vállalkozás döntése alapján a vállalkozásra nézve kötelező peren kívüli panaszkezelési mód és vitarendezési mechanizmus igénybevételének lehetőségéről, valamint az ehhez való hozzáférés módjáról:**

Fogyasztók a panaszaikkal elsődlegesen írásban fordulhatnak közvetlenül a Társasághoz, amely minden tőle telhetőt megtesz az esetleges jogsértések megszüntetése és orvoslása érdekében. A Társaság és fogyasztó a jogvitáikat elsődlegesen peren kívül, egyeztetés útján rendezik. Ha az egyeztetés nem vezet eredményre, úgy a fogyasztó a hatályos jogszabályok, illetve a Szabályzat szerint hatósághoz, bírósághoz, békéltető testülethez fordulhat.

**21\. A békéltető testülethez fordulás lehetőségéről, a Társaság székhelye szerint illetékes békéltető testület nevéről és székhelyének postai címéről:**

A békéltető testület hatáskörébe tartozik a fogyasztó és a vállalkozás közötti, a termék minőségével, biztonságosságával, a termékfelelősségi szabályok alkalmazásával, a szolgáltatás minőségével, továbbá a felek közötti szerződés megkötésével és teljesítésével kapcsolatos vitás ügy (a továbbiakban: fogyasztói jogvita) bírósági eljáráson kívüli rendezése: e célból egyezség létrehozásának megkísérlése, ennek eredménytelensége esetén pedig az ügyben döntés hozatala a fogyasztói jogok egyszerű, gyors, hatékony és költségkímélő érvényesítésének biztosítása érdekében. A békéltető testület a fogyasztó vagy a vállalkozás kérésére tanácsot ad a fogyasztót megillető jogokkal és a fogyasztót terhelő kötelezettségekkel kapcsolatban. A békéltető testület eljárása megindításának feltétele, hogy a fogyasztó az érintett vállalkozással közvetlenül megkísérelje a vitás ügy rendezését. A békéltető testület eljárása a fogyasztó kérelmére indul. A kérelmet a békéltető testület elnökéhez kell írásban benyújtani. A Társaság székhelye szerint illetékes békéltető testület: Budapesti Békéltető Testület, 1016 Budapest, Krisztina krt. 99. levelezési cím: 1253 Budapest, Pf.: 10.

Jelen tájékoztató a Szabályzat mellékletét képezi.

**Utolsó módosítás időpontja: 2024.11.04.**