![](https://scontent-lhr8-2.xx.fbcdn.net/v/t39.2365-6/255484386_291141566250612_3255458060865995134_n.svg?_nc_cat=1&ccb=1-7&_nc_sid=e280be&_nc_ohc=p_Rl5CAPybUQ7kNvgGSZicg&_nc_oc=AdghuAT08wJo2FcfrNm_isntBg7_sJOwDdR5HwH51DiRuku4eMZ9DykgMvx1fa_zXxI&_nc_zt=14&_nc_ht=scontent-lhr8-2.xx&_nc_gid=A0b6L6tDfsSLyOV-tQCLVhN&oh=00_AYFxJ6BobPfpehsuYpkoJTDOPZmJJCl2_IPDOkDk-rQU_w&oe=67E51444)

[#### Transparency Center](https://transparency.fb.com/)

[Policies](https://transparency.fb.com/policies/)

[###### Community Standards

Policies that outline what is and isn't allowed on our apps.](https://transparency.fb.com/policies/community-standards/)[###### Meta Advertising Standards

Policies for ad content and business assets.](https://transparency.fb.com/policies/ad-standards/)[###### Other policies

Other policies that apply to Meta technologies.](https://transparency.fb.com/policies/other-policies/)[###### How Meta improves

How we update our policies, measure results, work with others, and more.](https://transparency.fb.com/policies/improving/)[###### Age-Appropriate Content

Explore how we help teens have safe, positive experiences on Facebook and Instagram.](https://transparency.fb.com/policies/age-appropriate-content/)

[Enforcement](https://transparency.fb.com/enforcement/)

[###### Detecting violations

How technology and review teams help us detect and review violating content and accounts.](https://transparency.fb.com/enforcement/detecting-violations/)[###### Taking action

Our three-part approach to content enforcement: remove, reduce and inform.](https://transparency.fb.com/enforcement/taking-action/)

[Security](https://transparency.fb.com/metasecurity/)

[###### Threat disruptions

How we take down coordinated adversarial networks to protect people using our services](https://transparency.fb.com/metasecurity/threat-disruptions/)[###### Security threats

Challenges we investigate and counter around the globe](https://transparency.fb.com/metasecurity/security-threats/)[###### Threat reporting

Security research into the adversarial networks we’ve taken down since 2017](https://transparency.fb.com/metasecurity/threat-reporting/)

[Features](https://transparency.fb.com/features/)

[###### Our Approach To Dangerous Organizations and Individuals

How we approach dangerous organizations and individuals.](https://transparency.fb.com/features/dangerous-orgs-and-individuals)[###### Our approach to the opioid epidemic

How we support communities in the face of the opioid epidemic.](https://transparency.fb.com/features/supporting-communities-opioid-epidemic)[###### Our approach to elections

How we help prevent interference, empower people to vote and more.](https://transparency.fb.com/features/approach-to-elections/)[###### Our approach to misinformation

How we work with independent fact-checkers, and more, to identify and take action on misinformation.](https://transparency.fb.com/policies/community-standards/misinformation)[###### Our approach to newsworthy content

How we assess content for newsworthiness.](https://transparency.fb.com/features/approach-to-newsworthy-content/)[###### Our approach to Facebook Feed ranking

How we reduce problematic content in News Feed.](https://transparency.fb.com/features/ranking-and-content/)[###### Our approach to explaining ranking

How we build AI systems.](https://transparency.fb.com/features/explaining-ranking/)

[Governance](https://transparency.fb.com/governance/)

[###### Governance innovation](https://transparency.fb.com/governance/innovation/)[###### Oversight Board overview](https://transparency.fb.com/oversight/overview/)[###### How to appeal to the Oversight Board](https://transparency.fb.com/oversight/appealing-to-oversight-board/)[###### Oversight Board cases](https://transparency.fb.com/oversight/oversight-board-cases/)[###### Oversight Board recommendations](https://transparency.fb.com/oversight/oversight-board-recommendations/)[###### Creating the Oversight Board](https://transparency.fb.com/oversight/creation-of-oversight-board/)[###### Oversight Board: Further asked questions](https://transparency.fb.com/oversight/further-asked-questions/)[###### Meta’s Bi-Annual Updates on the Oversight Board](https://transparency.fb.com/oversight/meta-biannual-updates-on-the-oversight-board/)[###### Tracking the Oversight Board’s Impact](https://transparency.fb.com/governance/tracking-impact/)

[Research tools](https://transparency.fb.com/researchtools/)

[###### Content Library and Content Library API

Comprehensive access to public data from Facebook and Instagram](https://transparency.fb.com/researchtools/meta-content-library/)[###### Ad Library tools

Comprehensive and searchable database of all ads currently running across Meta technologies](https://transparency.fb.com/researchtools/ad-library-tools/)[###### Other research tools and datasets

Additional tools for in-depth research on Meta technologies and programs](https://transparency.fb.com/researchtools/other-datasets/)

[Reports](https://transparency.fb.com/data/)

[###### Community Standards Enforcement Report

Quarterly report on how well we're doing at enforcing our policies on the Facebook app and Instagram.](https://transparency.fb.com/data/community-standards-enforcement/)[###### Intellectual Property

Report on how well we're helping people protect their intellectual property.](https://transparency.fb.com/data/intellectual-property/)[###### Government Requests for User Data

Report on government request for people's data.](https://transparency.fb.com/data/government-data-requests/)[###### Content Restrictions Based on Local Law

Report on when we restrict content that's reported to us as violating local law.](https://transparency.fb.com/data/content-restrictions/)[###### Internet Disruptions

Report on intentional internet restrictions that limit people's ability to access the internet.](https://transparency.fb.com/data/internet-disruptions/)[###### Widely Viewed Content Report

Quarterly report on what people see on Facebook, including the content that receives the widest distribution during the quarter.](https://transparency.fb.com/data/widely-viewed-content-report/)[###### Regulatory and Other Transparency Reports

Download current and past regulatory reports for Facebook and Instagram.](https://transparency.fb.com/data/regulatory-transparency-reports/)

Community Standards
===================

The Community Standards outline what is and isn't allowed on Facebook, Instagram, Messenger and Threads.

### Introduction

Every day, people use Facebook, Instagram, Messenger and Threads to share their experiences, connect with friends and family, and build communities. Our services enable billions of people to freely express themselves across countries and cultures and in dozens of languages.

Meta recognizes how important it is for Facebook, Instagram, Messenger and Threads to be places where people feel empowered to communicate, and we take our role seriously in keeping abuse off the service. That’s why we developed standards for what is and isn’t allowed on these services.

These standards are based on feedback from people and the advice of experts in fields like technology, public safety and human rights. To ensure everyone’s voice is valued, we take great care to create standards that include different views and beliefs, especially from people and communities that might otherwise be overlooked or marginalized.

  

**Please note that the US English version of the Community Standards reflects the most up to date set of the policies and should be used as the primary document.**

### Our commitment to voice

The goal of our Community Standards is to create a place for expression and give people a voice. Meta wants people to be able to talk openly about the issues that matter to them, whether through written comments, photos, music, or other artistic mediums, even if some may disagree or find them objectionable. In some cases, we allow content—which would otherwise go against our standards—if it’s [newsworthy](https://transparency.meta.com/features/approach-to-newsworthy-content) and in the public interest. We do this only after weighing the public interest value against the risk of harm, and we look to international human rights standards to make these judgments. In other cases, we may remove content that uses ambiguous or implicit language when additional context allows us to reasonably understand that the content goes against our standards.

Our commitment to expression is paramount, but we recognize the internet creates new and increased opportunities for abuse. For these reasons, when we limit expression, we do it in service of one or more of the following values:

![](https://lookaside.fbsbx.com/elementpath/media/?media_id=849918942246548&version=1737412765)

AUTHENTICITY
------------

We want to make sure the content people see is authentic. We believe that authenticity creates a better environment for sharing, and that’s why we don’t want people using our services to misrepresent who they are or what they’re doing.

![](https://lookaside.fbsbx.com/elementpath/media/?media_id=2800543873566764&version=1737412765)

SAFETY
------

We’re committed to making Facebook, Instagram, Messenger and Threads safe places. We remove content that could contribute to a risk of harm to the physical security of persons. Content that threatens people has the potential to intimidate, exclude or silence others and isn’t allowed on our services.

![](https://lookaside.fbsbx.com/elementpath/media/?media_id=308994907203174&version=1737412765)

PRIVACY
-------

We’re committed to protecting personal privacy and information. Privacy gives people the freedom to be themselves, choose how and when to share on our services and connect more easily.

![](https://lookaside.fbsbx.com/elementpath/media/?media_id=203760854736655&version=1737412765)

DIGNITY
-------

We believe that all people are equal in dignity and rights. We expect that people will respect the dignity of others and not harass or degrade others.

### Community Standards

Our Community Standards apply to everyone, all around the world, and to all types of content, including AI-generated content.

Each section of our Community Standards starts with a “Policy Rationale” that sets out the aims of the policy followed by specific policy lines that outline:

Content that's not allowed; and

Content that requires additional information or context to enforce on, content that is allowed with a warning screen or content that is allowed but can only be viewed by adults aged 18 and older.

[Coordinating Harm and Promoting Crime](https://transparency.fb.com/policies/community-standards/coordinating-harm-publicizing-crime/)[Dangerous Organizations and Individuals](https://transparency.fb.com/policies/community-standards/dangerous-individuals-organizations/)[Fraud, Scams, and Deceptive Practices](https://transparency.fb.com/policies/community-standards/fraud-scams)[Restricted Goods and Services](https://transparency.fb.com/policies/community-standards/regulated-goods/)[Violence and Incitement](https://transparency.fb.com/policies/community-standards/violence-incitement/)[Adult Sexual Exploitation](https://transparency.fb.com/policies/community-standards/sexual-exploitation-adults/)[Bullying and Harassment](https://transparency.fb.com/policies/community-standards/bullying-harassment/)[Child Sexual Exploitation, Abuse, and Nudity](https://transparency.fb.com/policies/community-standards/child-sexual-exploitation-abuse-nudity/)[Human Exploitation](https://transparency.fb.com/policies/community-standards/human-exploitation/)[Suicide, Self-Injury, and Eating Disorders](https://transparency.fb.com/policies/community-standards/suicide-self-injury/)[Adult Nudity and Sexual Activity](https://transparency.fb.com/policies/community-standards/adult-nudity-sexual-activity/)[Adult Sexual Solicitation and Sexually Explicit Language](https://transparency.fb.com/policies/community-standards/sexual-solicitation/)[Hateful Conduct](https://transparency.fb.com/policies/community-standards/hate-speech/)[Privacy Violations](https://transparency.fb.com/policies/community-standards/privacy-violations-image-privacy-rights/)[Violent and Graphic Content](https://transparency.fb.com/policies/community-standards/violent-graphic-content/)[Account Integrity](https://transparency.fb.com/policies/community-standards/account-integrity) [Authentic Identity Representation](https://transparency.fb.com/policies/community-standards/authentic-identity-representation)[Cybersecurity](https://transparency.fb.com/policies/community-standards/cybersecurity/)[Inauthentic Behavior](https://transparency.fb.com/policies/community-standards/inauthentic-behavior/)[Memorialization](https://transparency.fb.com/policies/community-standards/memorialization/)[Misinformation](https://transparency.fb.com/policies/community-standards/misinformation/)[Spam](https://transparency.fb.com/policies/community-standards/spam/)[Third-Party Intellectual Property Infringement](https://transparency.fb.com/policies/community-standards/intellectual-property/)[Using Meta Intellectual Property and Licenses](https://transparency.fb.com/policies/community-standards/meta-intellectual-property)[Additional Protection of Minors](https://transparency.fb.com/policies/community-standards/additional-protection-minors/)[Locally Illegal Content, Products, or Services](https://transparency.fb.com/policies/community-standards/locally-illegal-products-services)[User Requests](https://transparency.fb.com/policies/community-standards/user-requests/)