**_Termeni și condiții care intră în vigoare la 20 august 2024._**

**1\. Obiectul**
----------------

Comuto SA (în continuare denumit „BlaBlaCar”) a dezvoltat o platformă pentru utilizarea în comun a autovehiculelor, accesibilă pe un site web la adresa www.blablacar.ro sau sub forma unei aplicații pentru mobil și proiectată să pună în contact conducătorii auto care merg spre o anumită destinație cu pasagerii care merg spre aceeași destinație, pentru a le permite să împartă Cursa și, astfel, costurile aferente (în continuare denumită „Platformă”).

Acești termeni și condiții au ca scop să guverneze accesul și condițiile de utilizare a Platformei. Vă rugăm să-i citiți cu atenție. Înțelegeți și sunteți de acord că BlaBlaCar nu este parte a niciunui acord, contract sau raporturi contractuale, de orice natură, încheiate între Membrii Platformei sale.

Făcând clic pe „Conectare cu Facebook” sau „Înregistrare cu o adresă de e-mail”, sunteți de acord că ați citit și ați acceptat toate aceste condiții generale de utilizare.

Dacă vă folosiți Contul pentru a vă conecta la platforma BlaBlaCar din altă țară (de exemplu, www.blablacar.com.br), vă rugăm să rețineți că (i) termenii și condițiile acelei platforme, (ii) politica de confidențialitate a acelei platforme. și (iii) se vor aplica regulile și reglementările statutare ale țării respective. Aceasta înseamnă, de asemenea, că informațiile din Contul dvs., inclusiv datele personale, ar putea trebui transferate către entitatea juridică care operează acea altă platformă. Vă rugăm să rețineți că BlaBlaCar își rezervă dreptul de a restricționa accesul la Platformă pentru utilizatorii identificați în mod rezonabil ca fiind aflați în afara teritoriului Uniunii Europene.

**2\. Definiții**
-----------------

În cadrul acestui document,

„**Anunț**” înseamnă un anunț despre o Cursă postată pe Platformă de un Conducător auto;

„**BlaBlaCar**” are semnificația prevăzută la Articolul 1 de mai sus;

„**T&C**” înseamnă Termeni și condiții;

„**Cont**” înseamnă contul ce trebuie să fie creat pentru a deveni Membru și a accesa anumite servicii oferite de Platformă;

„**Cont de Facebook**” are semnificația prevăzută la Articolul 3.2 de mai jos;

„**Conducător auto**” este Membrul care folosește Platforma și care își oferă locurile disponibile din autovehicul altor persoane în schimbul unei Contribuții la costuri, pentru o Cursă și la un moment definit chiar de Conducătorul auto;

„**Confirmarea rezervării**” are semnificația prevăzută la Articolul 4.2.1 de mai jos;

„**Conținutul de Membru**” are semnificația prevăzută la Articolul 11.2 de mai jos;

„**Membru**” înseamnă orice persoană care și-a creat un Cont pe Platformă;

„**Pasager**” se referă la Membrul care a acceptat oferta de a fi transportat de Conducătorul auto sau, după caz, persoana în numele căreia un Membru a rezervat un Loc;

„**Contribuție la costuri**” înseamnă, pentru o anumită Cursă, suma de bani cerută de Conducătorul auto și acceptată de Pasager pentru contribuția acestuia din urmă la costurile de călătorie;

„**Loc**” reprezintă locul rezervat de un Pasager în autovehiculul Conducătorului auto;

„**Platformă**” are semnificația prevăzută la Articolul 1 de mai sus;

„**Rezervare**” are semnificația prevăzută la Articolul 4.2.1 de mai jos;

„**Servicii**” înseamnă toate serviciile furnizate de BlaBlaCar prin intermediul Platformei;

„**Site web**” reprezintă site-ul web accesibil la adresa www.blablacar.ro;

„**Segment**” are semnificația prevăzută la Articolul 4.1. de mai jos;

„**Cursa**” reprezintă călătoria ce face obiectul Anunțului publicat de un Conducător auto pe Platformă și pentru care acesta acceptă să transporte Pasageri în schimbul Contribuției la costuri;

„**Taxe pentru servicii**” are semnificația prevăzută la articolul 5.2. de mai jos.

**3\. Înregistrarea pe Platformă și crearea unui Cont**
-------------------------------------------------------

### **3.1. Condiții de înregistrare pe Platformă**

Platforma poate fi folosită de persoane cu vârsta de peste 18 ani. Înregistrarea pe platformă de către un minor este strict interzisă. La accesarea, utilizarea sau înregistrarea pe Platformă, declarați și garantați că aveți peste 18 ani.

### **3.2. Crearea unui Cont**

Platforma permite Membrilor să posteze și să vadă Anunțuri și să interacționeze unii cu alții pentru a rezerva un Loc. Puteți vedea Anunțurile chiar dacă nu sunteți înregistrat pe Platformă. Totuși, nu puteți posta un Anunț și nici nu puteți rezerva un Loc fără a crea mai întâi un Cont și a deveni Membru.

Pentru a vă crea un Cont, puteți:

* (i) fie să completați toate câmpurile obligatorii din formularul de înregistrare;
* (ii) fie să vă conectați la contul dvs. de Facebook prin intermediul Platformei noastre (în continuare denumit „Cont de Facebook”). Utilizând această variantă, înțelegeți că BlaBlaCar va avea acces la, va publica pe Platformă și va păstra anumite informații din Contul dvs. de Facebook. Puteți deconecta oricând legătura dintre Contul dvs. și Contul de Facebook prin intermediul secțiunii „Verificări” din cadrul profilului dvs. Dacă doriți să aflați mai multe despre utilizarea datelor dvs. din Contul dvs. de Facebook, citiți [Politica de confidențialitate](https://www.blablacar.ro/about-us/privacy-policy) a noastră și a Facebook.

Pentru a vă înregistra pe Platformă, trebuie mai întâi să citiți și să acceptați acești T&C.

La crearea unui Cont, indiferent de metoda aleasă, sunteți de acord să furnizați informații corecte și adevărate și să le actualizați prin intermediul profilului dvs. sau prin notificare către BlaBlaCar pentru a garanta relevanța și exactitatea lor în toate raporturile dvs. contractuale cu BlaBlaCar.

În cazul înregistrării prin e-mail, sunteți de acord să țineți secretă parola aleasă la crearea Contului dvs. și să nu o transmiteți nimănui. În cazul în care pierdeți sau dezvăluiți parola, vă angajați să anunțați imediat BlaBlaCar. Doar dvs. sunteți singurul responsabil de utilizarea Contului dvs. de terțe persoane, dacă nu ați notificat în mod expres către BlaBlaCar pierderea, utilizarea frauduloasă de o terță persoană sau dezvăluirea parolei dvs. unei terțe persoane.

Sunteți de acord să nu creați și să nu utilizați, sub propria dvs. identitate sau cea a unei terțe persoane, alte Conturi decât cel inițial creat.

### **3.3. Verificare**

În scopul transparenței, îmbunătățirii încrederii și prevenirii sau identificării fraudei, BlaBlaCar poate să instaleze un sistem pentru verificarea unora dintre informațiile pe care le-ați furnizat în profilul dvs. Este cazul mai ales atunci când introduceți numărul dvs. de telefon sau ne furnizați un act de identitate.

Sunteți de acord și acceptați că orice referire de pe Platformă sau Servicii la informații „verificate” (inclusiv „Profilul verificat”), sau alt termen similar, înseamnă doar că un Membru a trecut cu succes de procedura de verificare existentă pe Platformă sau Servicii pentru a vă furniza mai multe informații despre Membrul cu care vă gândiți să călătoriți. BlaBlaCar nu poate garanta veridicitatea, fidelitatea sau valabilitatea informațiilor care fac obiectul procedurii de verificare.

**4\. Utilizarea Serviciilor**
------------------------------

### **4.1. Postarea Anunțurilor**

În calitate de Membru și cu condiția să îndepliniți condițiile de mai jos, puteți crea și posta Anunțuri pe Platformă introducând informații despre Cursa pe care intenționați să o realizați (data/ora și punctele de întâlnire și sosire, numărul de locuri oferite, opțiunile disponibile, valoarea Contribuției la costuri, etc.).

Atunci când postați Anunțul, puteți indica orașele principale în care sunteți de acord să opriți, de unde să luați sau unde să lăsați Pasagerii. Porțiunile Cursei dintre aceste orașe principale sau dintre unul din aceste orașe principale și punctul de întâlnire sau sosire al Cursei reprezintă „Segmente”.

Sunteți autorizat să postați un Anunț doar dacă îndepliniți toate condițiile următoare:

* (i) dețineți un permis de conducere valabil;
* (ii) faceți Anunțuri pentru vehicule pe care le dețineți sau le utilizați cu aprobarea expresă a proprietarului și, în toate cazurile, sunteți autorizat să le folosiți în scopul utilizării în comun;
* (iii) sunteți și rămâneți conducătorul principal al vehiculului care face obiectul Anunțului;
* (iv) vehiculul are asigurare valabilă împotriva prejudiciilor aduse unui terț;
*  (v) nu aveți nicio contraindicație sau incapacitate medicală de a conduce;
* (vi) vehiculul pe care intenționați să îl folosiți pentru Cursă este un autoturism cu 4 roți și maximum 7 locuri, excluzând vehiculele care nu necesită permis de conducere pentru a fi conduse;
* (vii) nu intenționați să postați un alt anunț pentru aceeași Cursă pe Platformă;
* (viii) nu oferiți mai multe Locuri decât numărul disponibil în vehiculul dvs.;
* (ix) toate Locurile oferite au centură de siguranță, chiar dacă vehiculul este omologat cu scaune care nu au centură de siguranță;
* (x) folosiți un vehicul în stare bună de funcționare și care respectă prevederile legale în vigoare și corespunde uzanțelor, și mai ales cu un certificat ITP (Inspecție Tehnică Periodică) la zi;
* (xi) sunteți consumator și nu acționați ca profesionist.

Sunteți de acord că sunteți singurul responsabil de conținutul Anunțului pe care îl postați pe Platformă. În consecință, declarați și garantați exactitatea și veridicitatea tuturor informațiilor din Anunțul dvs. și vă angajați să realizați Cursa în condițiile descrise în Anunțul dvs.

Anunțul dvs. va fi postat pe Platformă și va fi, așadar, vizibil pentru Membrii și toți vizitatorii, chiar ne-membri, care efectuează o căutare pe Platformă sau pe site-ul web al partenerilor BlaBlaCar. BlaBlaCar își rezervă dreptul, la libera sa alegere, să nu posteze sau să elimine, oricând, orice Anunț care nu respectă T&C sau pe care consideră că prejudiciază imaginea sa, a Platformei sau a Serviciilor și/sau să suspende Contul Membrului care postează astfel de Anunțuri în conformitate cu Secțiunea 9 din acești T&C.

Vă informăm că în cazul în care vă prezentați în calitate de consumator prin utilizarea Platformei când în realitate acționați ca profesionist, vă expuneți sancțiunilor prevăzute de legislația aplicabilă.

Sunteți de acord și acceptați că criteriile luate în considerare la clasificarea și ordinea de afișare a Anunțului dvs. printre alte Anunțuri sunt la alegerea exclusivă a BlaBlaCar.

### **4.2. Rezervarea unui Loc**

**4.2.1. Procesul de rezervare**

BlaBlaCar a implementat un sistem de rezervare a Locurilor online („Rezervare”) pentru Curse oferite pe Platformă.

Metodele de rezervare a unui Loc depind de natura Călătoriei avute în vedere.

BlaBlaCar le oferă utilizatorilor de pe Platformă un motor de căutare bazat pe diferite criterii de căutare (Locul de origine, destinația, datele, numărul de călători etc.). Anumite funcționalități suplimentare sunt furnizate în motorul de căutare atunci când utilizatorul se conectează cu Contul său. BlaBlaCar invită utilizatorul, indiferent de procesul de rezervare utilizat, să consulte atent și să utilizeze motorul de căutare pentru a găsi oferta care este cel mai bine adaptată nevoilor sale. Mai multe informații pot fi găsite [aici](https://blog.blablacar.ro/about-us/transparenta-platformelor).

Atunci când un Pasager este interesat de un Anunț care beneficiază de Rezervare, el poate face o cerere de Rezervare online. Această cerere de Rezervare este fie (i) acceptată imediat (dacă Conducătorul auto alege această opțiune la postarea Anunțului său), fie (ii) acceptată manual de Conducătorul auto. La momentul Rezervării, Pasagerul efectuează plata online a Taxelor pentru servicii, după caz. După validarea cererii de Rezervare de către Conducătorul auto, după caz, Pasagerul primește o Confirmare de rezervare („Confirmare de rezervare”).

Dacă sunteți Conducător auto și ați ales, la postarea Anunțului dvs., să aprobați manual cererile de Rezervare, atunci trebuie să răspundeți la orice cerere de Rezervare într-un anumit interval de timp specificat de Pasager în timpul cererii de Rezervare. În caz contrar, cererea de Rezervare va expira automat, iar Pasagerului îi vor fi rambursate toate sumele plătite la momentul cererii de Rezervare, dacă există astfel de sume.

La momentul Confirmării rezervării, BlaBlaCar vă va transmite numărul de telefon al Conducătorului auto (dacă sunteți Pasager) sau al Pasagerului (dacă sunteți Conducător auto), dacă Membrul este de acord să i se afișeze numărul de telefon. În acel moment sunteți singurul responsabil de executarea contractului ce vă leagă de celălalt Membru.

**4.2.2. Rezervarea nominală a Locului și termenii de utilizare a Serviciilor în numele unei terțe persoane**

Orice utilizare a Serviciilor, în calitate de Pasager sau Conducător auto, se referă la un anumit nume. Conducătorul auto și Pasagerul trebuie să corespundă cu identitatea comunicată către BlaBlaCar și celorlalți membri care participă la Cursă.

Totuși, BlaBlaCar permite Membrilor săi să rezerve unul sau mai multe Locuri în numele unei terțe persoane. În acest caz, vă angajați să indicați cu exactitate Conducătorului auto, la momentul Rezervării, prenumele, vârsta și numărul de telefon al persoanei în numele căreia rezervați un Loc. Este strict interzisă rezervarea unui Loc pentru un minor sub 14 ani care călătorește singur. În cazul în care rezervați un loc pentru un minor peste 14 ani care călătorește singur, vă angajați să solicitați acordul prealabil al Conducătorului auto și să îi furnizați aprobarea completată corespunzător și semnată a reprezentanților legali ai acestuia.

În plus, Platforma este concepută pentru rezervarea de Locuri pentru persoane. Este interzisă rezervarea unui Loc pentru transportarea vreunui obiect, pachet, animal care călătorește singur sau pentru articole materiale de orice fel.

De asemenea, este interzisă publicarea unui Anunț pentru alt Conducător auto în afara dvs.

### **4.3. Conținutul de Membru, moderarea și sistemul de evaluare**

**4.3.1. Modul de funcționare al sistemului de evaluare**

BlaBlaCar vă încurajează să lăsați o evaluare despre un Conducător auto (dacă sunteți Pasager) sau despre un Pasager (dacă sunteți Conducător auto) cu care ați împărțit o Cursă sau cu care trebuia să împărțiți o Cursă (și anume, ați rezervat o Cursă). Totuși, nu vi se permite să lăsați o evaluare despre un alt Pasager, dacă ați fost Pasager sau despre un alt Membru cu care nu ați călătorit sau cu care nu trebuia să călătoriți. Aveți opțiunea de a lăsa o evaluare în termen de 14 zile de la data Cursei.

Evaluarea dvs. și evaluarea lăsată de un alt Membru despre dvs., dacă există, sunt vizibile și publicate pe Platformă doar după cea mai scurtă din următoarele perioade: (i) imediat după ce amândoi ați scris o evaluare sau (ii) după o perioadă de 14 zile de la prima evaluare.

Aveți opțiunea să răspundeți la o evalure lăsată de alt Membru pe profilul dvs. într-un termen de 14 zile de la data primirii acesteia. Evaluarea și răspunsul dvs., unde e cazul, vor fi publicate pe profilul dvs.

**4.3.2. Moderare**

**a. Conținutul de Membru**

Sunteți de acord și acceptați că BlaBlaCar poate modera, înainte de publicare, utilizând instrumente automate sau manual, Conținutul de Membru așa cum este definit în Secțiunea 11.2. Dacă BlaBlaCar consideră că Conținutul de Membru încalcă legislația aplicabilă sau acești T&C, își rezervă dreptul să:

* împiedice publicarea sau să șteargă acel Conținut de Membru;
* trimită un avertisment Membrului pentru a-I reaminti de obligația de a respecta legea aplicabilă și acești T&Cs și/sau;
* aplice măsuri restrictive în conformitate cu Secțiunea 9 a acestor T&C.

Utilizarea de către BlaBlaCar a unor astfel de instrumente automate sau a moderării manuale nu trebuie interpretată ca o acțiune de monitorizare sau ca o obligație de a căuta în mod activ activități și sau Conținuturi de Membru ilegale pe Platformă și, în măsura permisă de legea aplicabilă, nu atrage nicio răspundere pentru BlaBlaCar.

**b. Mesaje**

Schimbul de mesaje între Membri prin Platforma noastră („Mesaje”) este exclusiv în scopul schimbului de informații cu privire la Curse.

BlaBlaCar poate, utilizând software automat și algoritmi, să detecteze conținutul Mesajelor pentru a preveni frauda, pentru îmbunătățirea serviciilor, acordarea de asistență pentru clienți și derularea contractelor încheiate cu membrii noștri (cum ar fi acești T&C). În cazul detectării oricărui astfel de conținut într-un Mesaj care are indicii de comportament fraudulos sau ilegal, de ocolire a Platformei sau care contravine în alt mod acestor T&C:

* acest conținut poate să nu fie publicat;
* Membrul care a trimis Mesajul poate primi un avertisment pentru a i se reaminti obligația de a respecta legea aplicabilă și acești T&C; sau
* Contul Membrului poate fi suspendat în conformitate cu Secțiunea 9 din acești T&C.

Utilizarea de către BlaBlaCar a unor astfel de instrumente automate nu trebuie interpretată ca o acțiune de monitorizare sau ca o obligație de a căuta în mod activ activități și sau conținuturi ilegale pe Platformă și, în măsura permisă de legea aplicabilă, nu atrage nicio răspundere pentru BlaBlaCar. 

**4.3.3. Limită**

**În conformitate cu Secțiunea 9 din acești T&C,** BlaBlaCar își rezervă dreptul de a vă suspenda Contul, de a vă limita accesul la Servicii sau de a termina acești T&C în cazul în care (i) ați primit cel puțin trei evaluări și (ii) una dintre evaluări sau media evaluărilor pe care le-ați primit este mai mică sau egală cu 3, în funcție de gravitatea feedbackului lăsat de un Membru în evaluare..

**5\. Condiții financiare**
---------------------------

Accesul la Platformă și înregistrarea pe aceasta, precum și căutarea, vizualizarea și publicarea de Anunțuri sunt gratuite. În schimb, Rezervarea face obiectul unor taxe, în condițiile descrise mai jos.

### **5.1. Contribuția la costuri**

Contribuția la costuri este determinată de dvs., în calitate de Conducător auto, pe propria dvs. răspundere. Este strict interzis să profitați în orice fel de utilizarea Platformei noastre. În consecință, sunteți de acord să limitați Contribuția la costuri pe care o cereți Pasagerilor dvs. să o plătească la nivelul costurilor ocazionate în realitate de realizarea acestei Curse. În caz contrar, veți fi singurul care va suporta riscurile reinterpretării tranzacției finalizate prin intermediul Platformei.

Atunci când postați un Anunț, BlaBlaCar vă sugerează o valoare pentru Contribuția la costuri, care ia în considerare mai ales natura Cursei și distanța parcursă. Această valoare este dată pur și simplu ca o recomandare și depinde de dvs. să o creșteți sau să o reduceți pentru a lua în calcul costurile efectiv ocazionate de Cursă. Pentru a evita abuzul, BlaBlaCar limitează posibilitatea reglării Contribuției la costuri.

### **5.2. Taxele pentru servicii**

În schimbul utilizării Platformei, BlaBlaCar va colecta, la momentul Rezervării, Taxe pentru servicii (denumite în continuare „**Taxele pentru servicii**”). Utilizatorul va fi informat în înainte de aplicarea oricăror Taxe pentru Servicii, atunci când este necesar.

Metodele de calcul pentru Taxele pentru servicii pot fi publicate separat de BlaBlaCar, sunt furnizate exclusiv în scop informativ și nu au valoare contractuală. Taxele pentru servicii pot fi calculate în funcție de diverși factori, în special de durata și prețul Cursei.

BlaBlaCar își rezervă dreptul de a modifica în orice moment metodele de calcul pentru Taxele pentru servicii. Aceste modificări nu vor afecta Taxele pentru servicii acceptate de utilizator înainte de data intrării în vigoare a modificărilor.

În cazul călătoriilor transfrontaliere, vă rugăm să rețineți că metodele pentru calculul cuantumului Taxelor pentru servicii și al TVA-ului aplicabil variază în conformitate cu punctul de  întâlnire și/sau destinația Cursei.

Când se utilizează Platforma pentru Curse transfrontaliere sau în afara României, Taxele pentru servicii pot fi percepute de o societate afiliată BlaBlaCar care operează platforma locală.

### **5.3. Rotunjire**

Sunteți de acord și acceptați că BlaBlaCar poate, la propria alegere, să rotunjească în sus sau în jos Contribuția la costuri sau Taxele pentru servicii.

### **5.4. Plata Contribuției la costuri către Conducătorul auto**

Pasagerul se angajează să plătească Contribuția la costuri Conducătorului auto cel târziu la punctul de sosire.

Conducătorul auto se angajează să nu ceară nicio plată, în întregime sau parțial, a Contribuției la costuri înainte de a avea loc Cursa.

**6\. Scopul necomercial al Serviciilor și al Platformei**
----------------------------------------------------------

Sunteți de acord să utilizați Serviciile și Platforma doar pentru a fi pus în contact, pe bază necomercială, cu persoane ce doresc să împartă o Cursă cu dvs.

În contextul unei Curse bazate pe utilizarea în comun a autovehiculelor, recunoașteți faptul că drepturile consumatorului care decurg din legislația UE privind protecția consumatorului nu se aplică relației dumneavoastră cu alți Membri.

În calitate de Conducător auto, sunteți de acord să nu solicitați o Contribuție la costuri mai mare decât costurile reale ocazionate, și care ar putea să genereze profit, fiind menționat că în contextul suportării în comun a costului dvs. trebuie să suportați, ca și Conducător auto, propria parte din costurile aferente Cursei. Sunteți singurul responsabil de calcularea costurilor pe care le suportați pentru Cursă și de verificarea ca suma cerută Pasagerilor dvs. să nu depășescă costurile suportate efectiv (excluzând cota parte a dvs. din costuri).

BlaBlaCar își rezervă dreptul de a vă suspenda Contul în cazul în care folosiți un autovehicul condus de un șofer , alt autovehicul comercial sau taxi sau o mașină de firmă, și din acest motiv obțineți un profit de pe urma Platformei. Sunteți de acord să furnizați către BlaBlaCar, la simpla cerere, o copie a certificatului de înmatriculare a mașinii dvs. și/sau alt document care să ateste că sunteți autorizat să folosiți acest vehicul pe Platformă și că nu obțineți niciun fel de profit din asta.

De asemenea, în conformitate cu Secțiunea 9 din acești T&C, BlaBlaCar își rezervă dreptul de a vă suspenda Contul, de a vă limita accesul la Servicii sau de a rezilia aceste T&C, în cazul unei activități a dvs. pe Platformă care, datorită naturii Curselor oferite, frecvenței acestora, numărului de Pasageri transportați și Contribuției la costuri solicitate, implică o situație de profit pentru dvs. sau care, din orice motiv, sugerează lui BlaBlaCar că obțineți profit pe Platformă.

**7\. Anularea**
----------------

**7.1 Condiții de rambursare în caz de anulare**

Anularea unui Loc într-o Cursă bazată pe utilizarea în comun a autovehiculelor de către Conducătorul auto sau Pasager după Confirmarea rezervării face obiectul prevederilor de mai jos:

– În cazul anulării de către Conducătorul auto, Pasagerului i se rambursează Taxele pentru servicii. Acest lucru este valabil în special atunci când Conducătorul auto:

* A omis să confirme cererea de Rezervare în termenul prevăzut (dacă această opțiune a fost aleasă de Conducătorul auto);
* Anulează o Cursă bazată pe utilizarea în comun a autovehiculelor sau nu a ajuns la punctul de întâlnire după 15 minute de la momentul convenit;

– În cazul anulării de către Pasager, Taxeler pentru servicii sunt reținute de către BlaBlaCar.

Metoda de returnare a Taxelor pentru servicii este stabilită în funcție de metoda care a fost utilizată pentru plata Taxelor pentru servicii, în timp ce termenul de returnare a Taxelor pentru servicii depinde de momentul la care au loc transferurile efectuate de organizațiile bancare.

Atunci când anularea are loc înainte de plecare și din cauza Pasagerului, Locul (Locurile) anulat(e) de Pasager va (vor) deveni automat disponibil(e) pentru ceilalți Pasageri care pot să îl (le) rezerve online și acestea sunt supuse, în mod corespunzător, condițiilor acestor T&C.

BlaBlaCar apreciază, la discreția sa, pe baza informațiilor disponibile, legitimitatea cererilor de rambursare.

**7.2.** **Dreptul de retragere**

Prin acceptarea acestor T&C, acceptați în mod explicit că contractul dintre dumneavoastră și BlaBlaCar constând în conectarea cu un alt Membru va fi executat înainte de expirarea perioadei de retragere care începe la Confirmarea rezervării și renunțați în mod explicit la dreptul dumneavoastră de retragere, în conformitate cu dispozițiile din Ordonanța 34 / 2014 din.

**8\. Comportamentul utilizatorilor Platformei și Membrilor**
-------------------------------------------------------------

### **8.1. Obligațiile tuturor utilizatorilor Platformei**

Sunteți de acord că sunteți singurul responsabil de respectarea tuturor legilor, reglementărilor și obligațiilor aplicabile atunci când utilizați Platforma.

În plus, atunci când utilizați Platforma și în timpul Curselor, vă angajați:

  (i) să nu folosiți Platforma în scopuri profesionale, comerciale sau pentru obținerea de profit;

  (ii) să nu trimiteți către BlaBlaCar (în special la crearea sau actualizarea Contului dvs.) sau oricărui alt Membru informații false, înșelătoare, rău intenționate sau frauduloase;

  (iii) să nu vorbiți și să nu vă comportați în niciun fel, precum și să nu postați niciun conținut (inclusiv Mesaje) pe Platformă de natură defăimătoare, vătămătoare, obscenă, pornografică, vulgară, ofensatoare, agresivă, necuvenită, violentă, amenințătoare, hărțuitoare, rasistă sau xenofobă sau cu conotații sexuale care să incite la violență, discriminare sau ură, să încurajeze activități cu sau utilizarea de substanțe ilegale sau, mai general, ilegale, contrare legii aplicabile, acestor T&C și scopurilor Platformei care ar putea încălca drepturile BlaBlaCar sau ale unei terțe părți sau ar fi contrare bunelor moravuri;

  (iv) să nu încălcați drepturile și imaginea BlaBlaCar, în special drepturile sale de proprietate intelectuală;

  (v) să nu deschideți mai mult de un Cont pe Platformă și să nu deschideți un Cont în numele unei terțe persoane;

  (vi) să nu încercați să ocoliți sistemul de rezervare online al Platformei, în special încercând să transmiteți unui alt Membru detaliile dvs. de contact pentru a face rezervare în afara Platformei și a nu plăti Taxele pentru servicii;

  (vii) să nu contactați niciun alt Membru, în special prin intermediul Platformei, pentru un alt scop în afara celui definit în termenii privind utilizarea în comun a autovehiculelor;

  (viii) să nu acceptați sau să efectuați plăți în afara Platformei;

  (ix) să respectați acești T&C,  și orice alți termeni și condiții ale Platformei care vi se aplică.

### **8.2. Obligațiile Conducătorilor auto**

De asemenea, atunci când utilizați Platforma în calitate de Conducător auto, vă angajați:

  (i) să respectați toate legile, reglementările și codurile aplicabile pentru conducere și autovehicul, în special să dețineți o asigurare de răspundere civilă valabilă în momentul Cursei și să dețineți un permis de conducere valabil;

  (ii) să verificați ca asigurarea dvs. să acopere utilizarea în comun a autovehiculului și că Pasagerii dvs. sunt considerați terțe părți în vehiculul dvs. și, de aceea, sunt acoperiți de asigurarea dvs. pe durata întregii Curse, chiar și în cazul trecerii graniței;

  (iii) să nu vă asumați niciun risc atunci când conduceți, să nu consumați niciun produs care ar putea să vă diminueze atenția și capacitatea de a conduce cu atenție și în deplină siguranță;

  (iv) să postați Anunțuri care să corespundă doar Curselor efectiv planificate;

  (v) să efectuați Cursa așa cum este descrisă în Anunț (mai ales în ceea ce privește intenția de a circula pe autostradă) și să respectați orele și locurile agreate cu ceilalți Membrii (în special locul de întâlnire și punctul de sosire);

  (vi) să nu luați mai mulți Pasageri decât numărul de Locuri indicat în Anunț;

  (vii) să utilizați un vehicul în stare bună de funcționare și care respectă prevederile legale în vigoare și corespunde uzanțelor, și mai ales cu un certificat ITP (Inspecție Tehnică Periodică) la zi;

  (viii) să prezentați către BlaBlaCar sau oricărui Pasager care solicită acest lucru permisul dvs. de conducere, certificatul de înmatriculare a mașinii dvs., polița de asigurare, certificatul ITP și orice alt document care să demonstreze capacitatea dvs. de a utiliza vehiculul în calitate de Conducător auto pe Platformă;

  (ix) să vă informați Pasagerii imediat în caz de pană sau de schimbare a orei sau a Cursei;

  (x) în cazul unei Curse ce presupune trecerea frontierei, să păstrați și să puneți la dispoziția Pasagerului și oricărei autorități care poate solicita acest lucru orice document care să dovedească identitatea dvs. și dreptul dvs. de a trece granița;

  (xi) să așteptați Pasagerii la locul de întâlnire stabilit cel puțin 15 minute după ora stabilită;

  (xii) să nu postați un Anunț pentru un vehicul al cărui proprietar nu sunteți sau pe care nu sunteți autorizat să îl folosiți în scopul utilizării în comun;

  (xiii) să vă asigurați că puteți fi contactat de Pasagerii dvs. telefonic la numărul înregistrat pe profilul dvs.;

  (xiv) să nu obțineți niciun profit prin intermediul Platformei;

  (xv) să nu aveți nicio contraindicație sau incapacitate medicală de a conduce;

  (xvi) să vă comportați în mod adecvat și responsabil în timpul Cursei și în conformitate cu spiritul sistemului de utilizare în comun a autovehiculelor;

  (xvii) să nu refuzați nicio Rezervare pe bază de de rasă, culoare, etnie, naționalitate, religie, orientare sexuală, stare civilă, invaliditate, aspect fizic, sarcină, vulnerabilitate deosebită din cauza situației economice, nume, localitate de reședință, sănătate, opinie politică, vârstă.

### **8.3. Obligațiile Pasagerilor**

Atunci când utilizați Platforma în calitate de Pasager, vă angajați:

  (i) să adoptați un comportament adecvat în timpul Cursei astfel încât să nu deranjați concentrarea sau conducerea Conducătorului auto sau pacea și liniștea celorlalți Pasageri;

  (ii) să respectați autovehiculul Conducătorului auto și curățenia acestuia;

  (iii) în caz de întârziere, să informați imediat Conducătorul auto;

  (iv) să plătiți Conducătorului auto Contribuția la costuri agreată;

  (v) să așteptați Conducătorul auto la locul de întâlnire cel puțin 15 minute după ora stabilită;

  (vi) să prezentați către BlaBlaCar sau Conducătorului auto care solicită acest lucru cartea dvs. de identitate sau orice document care dovedește identitatea dvs.;

  (vii) să nu aveți în timpul Cursei articole, bunuri, substanțe sau animale care ar putea deranja condusul și concentrarea Conducătorului auto sau natura, deținerea sau transportul cărora este contrar prevederilor legale în vigoare;

  (viii) în cazul unei Curse ce presupune trecerea frontierei, să păstrați și să puneți la dispoziția Conducătorului auto și oricărei autorități care poate solicita acest lucru orice document care să dovedească identitatea dvs. și dreptul dvs. de a trece granița;

  (ix) să vă asigurați că puteți fi contactat de Pasagerii dvs. telefonic la numărul înregistrat pe profilul dvs., inclusiv la locul de întâlnire.

În cazul în care ați făcut o Rezervare pentru unul sau mai multe Locuri în numele unor terțe persoane, în conformitate cu prevederile Articolului 4.2.3 de mai sus, garantați respectarea de către această terță persoană a prevederilor acestui articol și, în general, a acestor T&C. BlaBlaCar își rezervă dreptul de a vă suspenda Contul, de a vă limita accesul la Servicii sau de a rezilia acești T&C în cazul încălcării condițiilor acestora de către terța persoană în numele căreia ați rezervat un Loc în baza acestor T&C, în conformitate cu Secțiunea 9 din acești T&C.

### 8.4. Raportarea conținutului neadecvat sau ilegal (Înștiințare și mecanism de acțiune)

Puteți raporta Conținutul de Membru sau un Mesaj suspect, neadecvat sau ilegal așa cum se descrie [aici](https://support.blablacar.com/s/article/Raportarea-con%C8%9Binutului-ilegal-1729197120608?language=ro).

BlaBlaCar, după ce a fost înștiințată în mod corespunzător, în conformitate cu această Secțiune sau de către autoritățile competente, va elimina prompt orice Conținut de Membru ilegal dacă:

* Conținutul de Membru este în mod evident ilegal sau contrar reglementărilor aplicabile; sau
* BlaBlaCar consideră că acel conținut încalcă acești T&C.

În astfel de cazuri, BlaBlaCar își rezervă dreptul de a elimina Conținutul de Membru care a fost raportat în mod corespunzător, suficient de detaliat și de clar, și/sau să suspende imediat Contul raportat.

Membrul în cauză poate contesta deciziile de mai sus luate de BlaBlaCar așa cum se descrie în Secțiunea 15.1 de mai jos.

**9\. Restricții legate de utilizarea Platformei, suspendarea conturilor, limitarea accesului și rezilierea**
-------------------------------------------------------------------------------------------------------------

Puteți termina raporturile dvs. contractuale cu BlaBlaCar oricând, fără niciun cost și fără niciun motiv. Pentru a face acest lucru, mergeți pur și simplu la fila „Închide contul” pe pagina cu profilul dvs.

În cazul (i) unei încălcări de către dvs. a acestor T&C, inclusiv, dar nelimitându-se la obligațiile dvs. de Membru menționate la Articolele 6 și 8 de mai sus, (ii) depășirii limitei stabilite la Articolul 4.3.3 de mai sus sau (iii) dacă BlaBlaCar are motive serioase să creadă că este necesar să-și protejeze siguranța și integritatea, precum și pe cea a Membrilor sau a terțelor părți, sau în scopul investigațiilor și prevenirii unei fraude, BlaBlaCar își rezervă dreptul:

* (i) să rezilieze T&C care vă leagă de BlaBlaCar imediat și fără o notificare prealabilă; și/sau
* (ii) să împiedice postarea sau să elimine orice Conținut de Membru postat de dvs. pe Platformă; și/sau
* (iii) să vă limiteze accesul și utilizarea Platformei; și/sau
* (iv) să vă suspende, temporar sau permanent, Contul.

Suspendarea contului poate însemna și, după caz, că nu veți primi plățile în curs.

De asemenea, după caz, BlaBlaCar vă poate trimite și un avertisment pentru a vă reaminti că aveți obligația de a respecta legea aplicabilă și/sau acești T&C.

Atunci când acest lucru este necesar, veți fi notificați de stabilirea unei astfel de măsuri pentru a putea să vă confirmați angajamentul de a respecta acești T&C și legea aplicabilă, să dați explicații către BlaBlaCar sau să contestați decizia sa. BlaBlaCar va decide, ținând cont de toate circumstanțele fiecărui caz și de gravitatea încălcării, la libera sa alegere, dacă va ridica sau nu măsurile puse în practică.

**10\. Date cu caracter personal**
----------------------------------

În contextul utilizării Platformei de către dvs., BlaBlaCar va colecta și procesa o parte din datele dvs. Personale așa cum este prevăzut în [Politica de confidențialitate](https://blog.blablacar.ro/about-us/privacy-policy). 

**11\. Proprietatea intelectuală**
----------------------------------

### **11.1. Conținutul publicat de BlaBlaCar**

Cu excepția conținuturilor furnizate de Membrii săi, BlaBlaCar este singurul deținător al drepturilor de proprietate intelectuală privind Serviciile, Platforma, conținutul acesteia (în special texte, imagini, desene, logo-uri, filme, sunete, date, grafice), precum și software-ul și bazele de date care îi asigură funcționarea.

BlaBlaCar vă acordă un drept ne-exclusiv, personal și netransferabil de a utiliza Platforma și Serviciile, pentru folosul dvs. personal și particular, pe o bază ne-comercială și în conformitate cu scopurile Platformei și Serviciilor.

Vi se interzice orice altă utilizare sau exploatare a Platformei și Serviciilor, precum și a conținutului acesteia fără acordul prealabil scris al BlaBlaCar. În special, sunt interzise:

  (i) reproducerea, modificarea, adaptarea, distribuirea, reprezentarea în mod public și diseminarea Platformei, Serviciilor și conținutului, cu excepția aprobării exprese a BlaBlaCar.

  (ii) decompilarea și ingineria inversă a Platformei sau Serviciilor, în afara excepțiilor stipulate de textele în vigoare;

  (iii) extragerea sau încercarea de a extrage (în special folosind roboți de exploatare a datelor sau orice astfel de instrumente similare de colectare a datelor) o parte considerabilă a datelor Platformei,

### **11.2. Conținutul postat de dvs. pe Platformă**

Pentru a permite furnizarea Serviciilor și în conformitate cu scopul Platformei, acordați lui BlaBlaCar o licență neexclusivă de a utiliza conținutul și datele pe care le furnizați în contextul utilizării Serviciilor de dvs., care pot include Cererile dvs. de rezervare, Anunțurile și comentariile din acestea, biografii, fotografii, evaluări și răspunsuri la evaluări (în continuare denumit „Conținutul de Membru”) și Mesaje.

Dumneavoastră purtați răspunderea și dețineți toate drepturile cu privire la Conținutul de Membru și Mesajele pe care le încărcați pe Platformă.

Pentru a permite BlaBlaCar să distribuie prin intermediul rețelei digitale și în conformitate cu orice protocol de comunicare (în special rețeaua de internet și mobil) și să furnizeze conținutul Platformei publicului, dvs. autorizați BlaBlaCar, la nivel global și pe toată durata raporturilor dvs. contractuale cu BlaBlaCar, să reproducă, să reprezinte, să adapteze și să traducă Conținutul de membru, după cum urmează:

  (i) autorizați BlaBlaCar să reproducă, parțial sau în întregime, Conținutul dvs. de Membru pe orice mediu digital prin înregistrare, cunoscut sau încă necunoscut, și, în special, pe orice server, hard disk, card de memorie sau orice alt suport echivalent, în măsura necesară pentru orice operațiune de stocare, backup, transmitere sau descărcare legate de operarea Platformei și de furnizarea Serviciului;

  (ii) autorizați BlaBlaCar să adapteze și să traducă Conținutul dvs. de Membru și să reproducă aceste adaptări pe orice suport digital, actual sau viitor, prevăzut la punctul (i) de mai sus, în scopul de a furniza Serviciile, în special în diferite limbi. Acest drept include mai ales opțiunea de a face modificări formatului Conținutului dvs. de membru, cu respectarea drepturile dvs. morale, în scopul respectării schemei grafice a Platformei și/sau pentru a-l face compatibil din punct de vedere tehnic.

**12\. Rolul lui BlaBlaCar**
----------------------------

Platforma reprezintă o rețea online unde Membrii pot crea și posta Anunțuri pentru Curse în scopul utilizării în comun a autovehiculelor. Aceste Anunțuri pot fi văzute, mai ales, de alți Membrii care caută termenii Cursei și, dacă e cazul, rezervă direct un Loc în autovehiculul respectiv cu membrul care a postat Anunțul pe Platformă.

Folosind Platforma și acceptând aceste T&C, sunteți de acord că BlaBlaCar nu este parte a niciunui contract încheiat între dvs. și ceilalți Membri în scopul împărțirii costurilor aferente unei Curse.

BlaBlaCar nu are niciun control în ceea ce privește comportamentul Membrilor săi și utilizatorilor Platformei. BlaBlaCar nu este proprietarul, nu exploatează, furnizează sau gestionează autovehiculele care fac obiectul Anunțurilor și nu oferă niciun fel de Cursă pe Platformă.

Sunteti de acord și acceptați că BlaBlaCar nu controlează valabilitatea, veridicitatea sau legalitatea Anunțurilor, Locurilor sau Curselor oferite. În calitatea sa de intermediar în procesul de utilizare în comun a autovehiculelor, BlaBlaCar nu furnizează niciun serviciu de transport și nu acționează în calitate de transportator; rolul lui BlaBlaCar este limitat la a facilita accesul la Platformă.  
Membrii (Conducători auto sau Pasageri) acționează pe propria lor răspundere.

În calitatea sa de intermediar, BlaBlaCar nu poate fi considerat responsabil pentru evenimentele aferente unei călătorii și, mai ales, referitor la:

* (i) informații incorecte comunicate de Conducătorul auto în Anunțul său sau prin orice alte mijloace, în ceea ce privește Cursa și termenii acesteia;
*  (ii) anularea sau modificarea unei Curse de un Membru;
*  (iii) neplata Contribuției de către Pasager;
* (iv) comportamentului Membrilor săi în timpul, înaintea sau după Cursă.

**13\. Operarea, disponibilitatea și funcțiile Platformei**
-----------------------------------------------------------

BlaBlaCar încearcă, pe cât posibil, să mențină Platforma accesibilă 7 zile pe săptămână și 24 de ore pe zi. Cu toate acestea, accesul la Platformă poate fi temporar suspendat, fără o notificare prealabilă, din cauza întreținerii tehnice, migrației sau operațiunilor de update, sau din cauza unor întreruperi sau constrângeri legate de funcționarea rețelei.

În plus, BlaBlaCar își rezervă dreptul de a modifica Platforma sau părți ale acesteia pentru ca unii utilizatori să testeze noi funcții și să ofere o experiență mai bună utilizatorului, precum și să modifice sau să suspende accesul total sau parțial la Platformă sau la funcționalitățile acesteia, la discreția sa. , temporar sau permanent.

**14\. Modificarea T&C**
------------------------

Acești T&C și documentele integrate prin referințe reprezintă întregul acord între dvs. și BlaBlaCar cu privire la utilizarea Serviciilor. Orice alt document, mai ales orice mențiune la Platformă (FAQ, etc.) are doar rol de ghidare.

BlaBlaCar poate modifica acești T&C pentru a se adapta mediului său tehnic și comercial și pentru a respecta legislația în vigoare. Orice modificare a acestor T&C va fi publicată pe Platformă cu menționarea datei de intrare în vigoare, iar dvs. veți fi notificați de BlaBlaCar înainte ca acestea să producă efecte.

**15\. Legea aplicabilă și litigii**
------------------------------------

Acești T&C sunt în limba română și fac obiectul legii din România.

**15.1 Sistemul intern de gestionare a reclamațiilor**

Puteți contesta deciziile pe care le putem lua cu privire la:

* Conținutul de Membru: de exemplu, am eliminat, am restricționat vizibilitatea sau am refuzat să eliminăm orice Conținut de Membru pe care îl furnizați când utilizați Platforma, sau
* Contul dvs.: v-am suspendat accesul la Platformă,

În cazul în care am luat astfel de decizii deoarece Conținutul de Membru constituie conținut ilegal sau este incompatibil cu acești  T&C. Procedura de contestare este descrisă [aici](https://support.blablacar.com/s/article/Cum-se-poate-contesta-eliminarea-con%C8%9Binutului-sau-suspendarea-contului-1729197123040?language=ro).

**15.2 Soluționarea litigiilor în afara instanțelor de judecată**

De asemenea, dacă este necesar, puteți să vă prezentați plângerile privind Platforma sau Serviciile noastre pe platforma privind rezolvarea disputelor, plasată online de Comisia Europeana, accesibilă [aici](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage). Comisia Europeană va transmite plângerea dvs. instanței naționale competente. În conformitate cu regulile aplicabile medierii, sunteți obligat, ca înainte de orice cerere de mediere, să notificați BlaBlaCar în scris în legătura cu orice dispută, pentru a obține o soluție amiabilă .

De asemenea, puteți să trimiteți o solicitare unui organism de soluționare a litigiilor din țara dvs. (găsiți lista [aici)](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show).

**16\. Prevederi legale**
-------------------------

Platforma este publicată de Comuto SA, o companie cu răspundere limitată, având un capital social de 155,880.999 EUR, înregistrată la Oficiul Registrului Comerțului sub numărul 491.904.546 (cod înregistrare de TVA intra-UE: FR76491904546), cu sediul social în Avenue de la République nr. 84, 75011 Paris (Franța), reprezentă de Directorul său executiv, Nicolas Brusson, Director responsabil cu publicarea pe site.

Acest site web este găzduit pe serverele Google Cloud din Țările de Jos.

Comuto SA este înregistrată în registrul operatorilor de servicii de transport și cazare cu următorul număr: IM075180037.

Garanția financiară este oferită de: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Paris – Franța.

Asigurarea de răspundere civilă profesională este subscrisă la: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paris – Franța.

Comuto SA este înregistrată în registrul intermediarilor de asigurări, servicii bancare și financiare sub numărul de înregistrare (Orias) 15003890.

Pentru orice întrebare, puteți contacta Comuto SA folosind acest [formular de contact](https://support.blablacar.com/s/contactsupport?language=ro).

**17\. Digital Services Act (****Regulamentul privind serviciile digitale)**
----------------------------------------------------------------------------

**17.1.  Informații privind numărul mediu lunar de beneficiari activi ai serviciului în Uniunea Europeană**

În conformitate cu articolul 24, alineatul 2 din Regulamentul Parlamentului European și al Consiliului privind o piață unică pentru serviciile digitale și de modificare a Directivei 2000/31/CE („DSA”), furnizorii de platforme online au obligația de a publica informații despre numărul mediu lunar de beneficiari activi ai serviciului prestat de aceștia în Uniunea Europeană, calculat ca medie în ultimele șase luni. Scopul publicării acestor informații este de a stabili dacă o platformă online îndeplinește criteriul de „platformă online foarte mare” în temeiul DSA, și anume, depășește pragul de 45 milioane de beneficiari activi, în medie și lunar, în Uniune.

La 17 august 2024 numărul mediu lunar de beneficiari activi ai BlaBlaCar în perioada cuprinsă între februarie și iulie 2024, calculat ținând cont de Preambulul 77 și articolul 3 din DSA (numărarea utilizatorilor expuși la conținutul Platformei o singură dată în perioada de 6 luni), înainte de adoptarea unui act delegat specific, a fost de 3,28 milioane în UE.

Aceste informații sunt publicate exclusiv în scopul respectării prevederilor DSA și nu ar trebuie utilizate ca bază în alte scopuri. Acestea vor fi actualizate cel puțin o dată la șase luni. Abordarea noastră pentru realizarea acestui calcul poate evolua și poate necesita modificări în timp, de exemplu, datorită unor modificări ale produsului sau unor noi tehnologii.

**17.2. Punct de contact pentru autorități**

În conformitate cu articolul 11 din DSA, dacă sunteți membru al autorităților UE competente, al Comisiei Europene sau al Comitetului European pentru Servicii Digitale, ne puteți contacta în legătură cu chestiuni referitoare la DSA prin e-mail, la [\[email protected\]](https://blog.blablacar.ro/cdn-cgi/l/email-protection).

Ne puteți contacta în limbile engleză și franceză.

Vă rugăm să rețineți că această adresă de e-mail nu este utilizată pentru comunicarea cu membrii. Dacă aveți întrebări despre utilizarea BlaBlaCar, ne puteți contacta în calitate de Membru prin acest [formular de contact](https://support.blablacar.com/s/contactsupport?language=ro).

* * *

**_Termeni și condiții care intră în vigoare la 19 aprilie 2024._**

**1\. Obiectul**
----------------

Comuto SA (în continuare denumit „BlaBlaCar”) a dezvoltat o platformă pentru utilizarea în comun a autovehiculelor, accesibilă pe un site web la adresa www.blablacar.ro sau sub forma unei aplicații pentru mobil și proiectată să pună în contact conducătorii auto care merg spre o anumită destinație cu pasagerii care merg spre aceeași destinație, pentru a le permite să împartă Cursa și, astfel, costurile aferente (în continuare denumită „Platformă”).

Acești termeni și condiții au ca scop să guverneze accesul și condițiile de utilizare a Platformei. Vă rugăm să-i citiți cu atenție. Înțelegeți și sunteți de acord că BlaBlaCar nu este parte a niciunui acord, contract sau raporturi contractuale, de orice natură, încheiate între Membrii Platformei sale.

Făcând clic pe „Conectare cu Facebook” sau „Înregistrare cu o adresă de e-mail”, sunteți de acord că ați citit și ați acceptat toate aceste condiții generale de utilizare.

**2\. Definiții**
-----------------

În cadrul acestui document,

„**Anunț**” înseamnă un anunț despre o Cursă postată pe Platformă de un Conducător auto;

“**Anunț autobuz**” înseamnă un anunț privind o călătorie cu autobuzul a unui operator de transport cu autobuzul postat pe platformă;

„**BlaBlaCar**” are semnificația prevăzută la Articolul 1 de mai sus;

„**T&C**” înseamnă Termeni și condiții;

„**Cont**” înseamnă contul ce trebuie să fie creat pentru a deveni Membru și a accesa anumite servicii oferite de Platformă;

„**Cont de Facebook**” are semnificația prevăzută la Articolul 3.2 de mai jos;

„**Conducător auto**” este Membrul care folosește Platforma și care își oferă locurile disponibile din autovehicul altor persoane în schimbul unei Contribuții la costuri, pentru o Cursă și la un moment definit chiar de Conducătorul auto;

„**Confirmarea rezervării**” are semnificația prevăzută la Articolul 4.2.1 de mai jos;

„**Conținutul de Membru**” are semnificația prevăzută la Articolul 11.2 de mai jos;

„**Membru**” înseamnă orice persoană care și-a creat un Cont pe Platformă;

„**Pasager**” se referă la Membrul care a acceptat oferta de a fi transportat de Conducătorul auto sau, după caz, persoana în numele căreia un Membru a rezervat un Loc;

„**Contribuție la costuri**” înseamnă, pentru o anumită Cursă, suma de bani cerută de Conducătorul auto și acceptată de Pasager pentru contribuția acestuia din urmă la costurile de călătorie;

„**Loc**” reprezintă locul rezervat de un Pasager în autovehiculul Conducătorului auto;

“**Operator de autobuz**” se referă la o companie care transportă în mod profesionist pasageri și ale cărei bilete pentru călătoriile sale sunt distribuite pe Platformă de către BlaBlaCar;

„**Platformă**” are semnificația prevăzută la Articolul 1 de mai sus;

„**Rezervare**” are semnificația prevăzută la Articolul 4.2.1 de mai jos;

„**Servicii**” înseamnă toate serviciile furnizate de BlaBlaCar prin intermediul Platformei;

„**Site web**” reprezintă site-ul web accesibil la adresa www.blablacar.ro;

„**Segment**” are semnificația prevăzută la Articolul 4.1. de mai jos;

„**Cursa**” reprezintă călătoria ce face obiectul Anunțului publicat de un Conducător auto pe Platformă și pentru care acesta acceptă să transporte Pasageri în schimbul Contribuției la costuri;

„**Taxe pentru servicii**” are semnificația prevăzută la articolul 5.2. de mai jos.

**3\. Înregistrarea pe Platformă și crearea unui Cont**
-------------------------------------------------------

### **3.1. Condiții de înregistrare pe Platformă**

Platforma poate fi folosită de persoane cu vârsta de peste 18 ani. Înregistrarea pe platformă de către un minor este strict interzisă. La accesarea, utilizarea sau înregistrarea pe Platformă, declarați și garantați că aveți peste 18 ani.

### **3.2. Crearea unui Cont**

Platforma permite Membrilor să posteze și să vadă Anunțuri și să interacționeze unii cu alții pentru a rezerva un Loc. Puteți vedea Anunțurile chiar dacă nu sunteți înregistrat pe Platformă. Totuși, nu puteți posta un Anunț și nici nu puteți rezerva un Loc fără a crea mai întâi un Cont și a deveni Membru.

Pentru a vă crea un Cont, puteți:

* (i) fie să completați toate câmpurile obligatorii din formularul de înregistrare;
* (ii) fie să vă conectați la contul dvs. de Facebook prin intermediul Platformei noastre (în continuare denumit „Cont de Facebook”). Utilizând această variantă, înțelegeți că BlaBlaCar va avea acces la, va publica pe Platformă și va păstra anumite informații din Contul dvs. de Facebook. Puteți deconecta oricând legătura dintre Contul dvs. și Contul de Facebook prin intermediul secțiunii „Verificări” din cadrul profilului dvs. Dacă doriți să aflați mai multe despre utilizarea datelor dvs. din Contul dvs. de Facebook, citiți [Politica de confidențialitate](https://www.blablacar.ro/about-us/privacy-policy) a noastră și a Facebook.

Pentru a vă înregistra pe Platformă, trebuie mai întâi să citiți și să acceptați acești T&C.

La crearea unui Cont, indiferent de metoda aleasă, sunteți de acord să furnizați informații corecte și adevărate și să le actualizați prin intermediul profilului dvs. sau prin notificare către BlaBlaCar pentru a garanta relevanța și exactitatea lor în toate raporturile dvs. contractuale cu BlaBlaCar.

În cazul înregistrării prin e-mail, sunteți de acord să țineți secretă parola aleasă la crearea Contului dvs. și să nu o transmiteți nimănui. În cazul în care pierdeți sau dezvăluiți parola, vă angajați să anunțați imediat BlaBlaCar. Doar dvs. sunteți singurul responsabil de utilizarea Contului dvs. de terțe persoane, dacă nu ați notificat în mod expres către BlaBlaCar pierderea, utilizarea frauduloasă de o terță persoană sau dezvăluirea parolei dvs. unei terțe persoane.

Sunteți de acord să nu creați și să nu utilizați, sub propria dvs. identitate sau cea a unei terțe persoane, alte Conturi decât cel inițial creat.

### **3.3. Verificare**

În scopul transparenței, îmbunătățirii încrederii și prevenirii sau identificării fraudei, BlaBlaCar poate să instaleze un sistem pentru verificarea unora dintre informațiile pe care le-ați furnizat în profilul dvs. Este cazul mai ales atunci când introduceți numărul dvs. de telefon sau ne furnizați un act de identitate.

Sunteți de acord și acceptați că orice referire de pe Platformă sau Servicii la informații „verificate” (inclusiv „Profilul verificat”), sau alt termen similar, înseamnă doar că un Membru a trecut cu succes de procedura de verificare existentă pe Platformă sau Servicii pentru a vă furniza mai multe informații despre Membrul cu care vă gândiți să călătoriți. BlaBlaCar nu poate garanta veridicitatea, fidelitatea sau valabilitatea informațiilor care fac obiectul procedurii de verificare.

**4\. Utilizarea Serviciilor**
------------------------------

### **4.1. Postarea Anunțurilor**

În calitate de Membru și cu condiția să îndepliniți condițiile de mai jos, puteți crea și posta Anunțuri pe Platformă introducând informații despre Cursa pe care intenționați să o realizați (data/ora și punctele de întâlnire și sosire, numărul de locuri oferite, opțiunile disponibile, valoarea Contribuției la costuri, etc.).

Atunci când postați Anunțul, puteți indica orașele principale în care sunteți de acord să opriți, de unde să luați sau unde să lăsați Pasagerii. Porțiunile Cursei dintre aceste orașe principale sau dintre unul din aceste orașe principale și punctul de întâlnire sau sosire al Cursei reprezintă „Segmente”.

Sunteți autorizat să postați un Anunț doar dacă îndepliniți toate condițiile următoare:

* (i) dețineți un permis de conducere valabil;
* (ii) faceți Anunțuri pentru vehicule pe care le dețineți sau le utilizați cu aprobarea expresă a proprietarului și, în toate cazurile, sunteți autorizat să le folosiți în scopul utilizării în comun;
* (iii) sunteți și rămâneți conducătorul principal al vehiculului care face obiectul Anunțului;
* (iv) vehiculul are asigurare valabilă împotriva prejudiciilor aduse unui terț;
*  (v) nu aveți nicio contraindicație sau incapacitate medicală de a conduce;
* (vi) vehiculul pe care intenționați să îl folosiți pentru Cursă este un autoturism cu 4 roți și maximum 7 locuri, excluzând vehiculele care nu necesită permis de conducere pentru a fi conduse;
* (vii) nu intenționați să postați un alt anunț pentru aceeași Cursă pe Platformă;
* (viii) nu oferiți mai multe Locuri decât numărul disponibil în vehiculul dvs.;
* (ix) toate Locurile oferite au centură de siguranță, chiar dacă vehiculul este omologat cu scaune care nu au centură de siguranță;
* (x) folosiți un vehicul în stare bună de funcționare și care respectă prevederile legale în vigoare și corespunde uzanțelor, și mai ales cu un certificat ITP (Inspecție Tehnică Periodică) la zi;
* (xi) sunteți consumator și nu acționați ca profesionist.

Sunteți de acord că sunteți singurul responsabil de conținutul Anunțului pe care îl postați pe Platformă. În consecință, declarați și garantați exactitatea și veridicitatea tuturor informațiilor din Anunțul dvs. și vă angajați să realizați Cursa în condițiile descrise în Anunțul dvs.

Anunțul dvs. va fi postat pe Platformă și va fi, așadar, vizibil pentru Membrii și toți vizitatorii, chiar ne-membri, care efectuează o căutare pe Platformă sau pe site-ul web al partenerilor BlaBlaCar. BlaBlaCar își rezervă dreptul, la libera sa alegere, să nu posteze sau să elimine, oricând, orice Anunț care nu respectă T&C sau pe care consideră că prejudiciază imaginea sa, a Platformei sau a Serviciilor și/sau să suspende Contul Membrului care postează astfel de Anunțuri în conformitate cu Secțiunea 9 din acești T&C.

Vă informăm că în cazul în care vă prezentați în calitate de consumator prin utilizarea Platformei când în realitate acționați ca profesionist, vă expuneți sancțiunilor prevăzute de legislația aplicabilă.

Sunteți de acord și acceptați că criteriile luate în considerare la clasificarea și ordinea de afișare a Anunțului dvs. printre alte Anunțuri sunt la alegerea exclusivă a BlaBlaCar.

### **4.2. Rezervarea unui Loc**

**4.2.1. Procesul de rezervare**

BlaBlaCar a implementat un sistem de rezervare a Locurilor online („Rezervare”) pentru Curse oferite pe Platformă.

Metodele de rezervare a unui Loc depind de natura Călătoriei avute în vedere.

BlaBlaCar le oferă utilizatorilor de pe Platformă un motor de căutare bazat pe diferite criterii de căutare (Locul de origine, destinația, datele, numărul de călători etc.). Anumite funcționalități suplimentare sunt furnizate în motorul de căutare atunci când utilizatorul se conectează cu Contul său. BlaBlaCar invită utilizatorul, indiferent de procesul de rezervare utilizat, să consulte atent și să utilizeze motorul de căutare pentru a găsi oferta care este cel mai bine adaptată nevoilor sale. Mai multe informații pot fi găsite [aici](https://blog.blablacar.ro/about-us/transparenta-platformelor).

Atunci când un Pasager este interesat de un Anunț care beneficiază de Rezervare, el poate face o cerere de Rezervare online. Această cerere de Rezervare este fie (i) acceptată imediat (dacă Conducătorul auto alege această opțiune la postarea Anunțului său), fie (ii) acceptată manual de Conducătorul auto. La momentul Rezervării, Pasagerul efectuează plata online a Taxelor pentru servicii, după caz. După validarea cererii de Rezervare de către Conducătorul auto, după caz, Pasagerul primește o Confirmare de rezervare („Confirmare de rezervare”).

Dacă sunteți Conducător auto și ați ales, la postarea Anunțului dvs., să aprobați manual cererile de Rezervare, atunci trebuie să răspundeți la orice cerere de Rezervare într-un anumit interval de timp specificat de Pasager în timpul cererii de Rezervare. În caz contrar, cererea de Rezervare va expira automat, iar Pasagerului îi vor fi rambursate toate sumele plătite la momentul cererii de Rezervare, dacă există astfel de sume.

La momentul Confirmării rezervării, BlaBlaCar vă va transmite numărul de telefon al Conducătorului auto (dacă sunteți Pasager) sau al Pasagerului (dacă sunteți Conducător auto), dacă Membrul este de acord să i se afișeze numărul de telefon. În acel moment sunteți singurul responsabil de executarea contractului ce vă leagă de celălalt Membru.

**4.2.2. Rezervarea nominală a Locului și termenii de utilizare a Serviciilor în numele unei terțe persoane**

Orice utilizare a Serviciilor, în calitate de Pasager sau Conducător auto, se referă la un anumit nume. Conducătorul auto și Pasagerul trebuie să corespundă cu identitatea comunicată către BlaBlaCar și celorlalți membri care participă la Cursă.

Totuși, BlaBlaCar permite Membrilor săi să rezerve unul sau mai multe Locuri în numele unei terțe persoane. În acest caz, vă angajați să indicați cu exactitate Conducătorului auto, la momentul Rezervării, prenumele, vârsta și numărul de telefon al persoanei în numele căreia rezervați un Loc. Este strict interzisă rezervarea unui Loc pentru un minor sub 14 ani care călătorește singur. În cazul în care rezervați un loc pentru un minor peste 14 ani care călătorește singur, vă angajați să solicitați acordul prealabil al Conducătorului auto și să îi furnizați aprobarea completată corespunzător și semnată a reprezentanților legali ai acestuia.

În plus, Platforma este concepută pentru rezervarea de Locuri pentru persoane. Este interzisă rezervarea unui Loc pentru transportarea vreunui obiect, pachet, animal care călătorește singur sau pentru articole materiale de orice fel.

De asemenea, este interzisă publicarea unui Anunț pentru alt Conducător auto în afara dvs.

### **4.3. Conținutul de Membru, moderarea și sistemul de evaluare**

**4.3.1. Modul de funcționare al sistemului de evaluare**

BlaBlaCar vă încurajează să lăsați o evaluare despre un Conducător auto (dacă sunteți Pasager) sau despre un Pasager (dacă sunteți Conducător auto) cu care ați împărțit o Cursă sau cu care trebuia să împărțiți o Cursă (și anume, ați rezervat o Cursă). Totuși, nu vi se permite să lăsați o evaluare despre un alt Pasager, dacă ați fost Pasager sau despre un alt Membru cu care nu ați călătorit sau cu care nu trebuia să călătoriți. Aveți opțiunea de a lăsa o evaluare în termen de 14 zile de la data Cursei.

Evaluarea dvs. și evaluarea lăsată de un alt Membru despre dvs., dacă există, sunt vizibile și publicate pe Platformă doar după cea mai scurtă din următoarele perioade: (i) imediat după ce amândoi ați scris o evaluare sau (ii) după o perioadă de 14 zile de la prima evaluare.

Aveți opțiunea să răspundeți la o evalure lăsată de alt Membru pe profilul dvs. într-un termen de 14 zile de la data primirii acesteia. Evaluarea și răspunsul dvs., unde e cazul, vor fi publicate pe profilul dvs.

**4.3.2. Moderare**

**a. Conținutul de Membru**

Sunteți de acord și acceptați că BlaBlaCar poate modera, înainte de publicare, utilizând instrumente automate sau manual, Conținutul de Membru așa cum este definit în Secțiunea 11.2. Dacă BlaBlaCar consideră că Conținutul de Membru încalcă legislația aplicabilă sau acești T&C, își rezervă dreptul să:

* împiedice publicarea sau să șteargă acel Conținut de Membru;
* trimită un avertisment Membrului pentru a-I reaminti de obligația de a respecta legea aplicabilă și acești T&Cs și/sau;
* aplice măsuri restrictive în conformitate cu Secțiunea 9 a acestor T&C.

Utilizarea de către BlaBlaCar a unor astfel de instrumente automate sau a moderării manuale nu trebuie interpretată ca o acțiune de monitorizare sau ca o obligație de a căuta în mod activ activități și sau Conținuturi de Membru ilegale pe Platformă și, în măsura permisă de legea aplicabilă, nu atrage nicio răspundere pentru BlaBlaCar.

**b. Mesaje**

Schimbul de mesaje între Membri prin Platforma noastră („Mesaje”) este exclusiv în scopul schimbului de informații cu privire la Curse.

BlaBlaCar poate, utilizând software automat și algoritmi, să detecteze conținutul Mesajelor pentru a preveni frauda, pentru îmbunătățirea serviciilor, acordarea de asistență pentru clienți și derularea contractelor încheiate cu membrii noștri (cum ar fi acești T&C). În cazul detectării oricărui astfel de conținut într-un Mesaj care are indicii de comportament fraudulos sau ilegal, de ocolire a Platformei sau care contravine în alt mod acestor T&C:

* acest conținut poate să nu fie publicat;
* Membrul care a trimis Mesajul poate primi un avertisment pentru a i se reaminti obligația de a respecta legea aplicabilă și acești T&C; sau
* Contul Membrului poate fi suspendat în conformitate cu Secțiunea 9 din acești T&C.

Utilizarea de către BlaBlaCar a unor astfel de instrumente automate nu trebuie interpretată ca o acțiune de monitorizare sau ca o obligație de a căuta în mod activ activități și sau conținuturi ilegale pe Platformă și, în măsura permisă de legea aplicabilă, nu atrage nicio răspundere pentru BlaBlaCar. 

**4.3.3. Limită**

**În conformitate cu Secțiunea 9 din acești T&C,** BlaBlaCar își rezervă dreptul de a vă suspenda Contul, de a vă limita accesul la Servicii sau de a termina acești T&C în cazul în care (i) ați primit cel puțin trei evaluări și (ii) una dintre evaluări sau media evaluărilor pe care le-ați primit este mai mică sau egală cu 3, în funcție de gravitatea feedbackului lăsat de un Membru în evaluare..

**5\. Condiții financiare**
---------------------------

Accesul la Platformă și înregistrarea pe aceasta, precum și căutarea, vizualizarea și publicarea de Anunțuri sunt gratuite. În schimb, Rezervarea face obiectul unor taxe, în condițiile descrise mai jos.

### **5.1. Contribuția la costuri**

Contribuția la costuri este determinată de dvs., în calitate de Conducător auto, pe propria dvs. răspundere. Este strict interzis să profitați în orice fel de utilizarea Platformei noastre. În consecință, sunteți de acord să limitați Contribuția la costuri pe care o cereți Pasagerilor dvs. să o plătească la nivelul costurilor ocazionate în realitate de realizarea acestei Curse. În caz contrar, veți fi singurul care va suporta riscurile reinterpretării tranzacției finalizate prin intermediul Platformei.

Atunci când postați un Anunț, BlaBlaCar vă sugerează o valoare pentru Contribuția la costuri, care ia în considerare mai ales natura Cursei și distanța parcursă. Această valoare este dată pur și simplu ca o recomandare și depinde de dvs. să o creșteți sau să o reduceți pentru a lua în calcul costurile efectiv ocazionate de Cursă. Pentru a evita abuzul, BlaBlaCar limitează posibilitatea reglării Contribuției la costuri.

### **5.2. Taxele pentru servicii**

În schimbul utilizării Platformei, BlaBlaCar va colecta, la momentul Rezervării, Taxe pentru servicii (denumite în continuare „**Taxele pentru servicii**”). Utilizatorul va fi informat în înainte de aplicarea oricăror Taxe pentru Servicii, atunci când este necesar.

Metodele de calcul pentru Taxele pentru servicii pot fi publicate separat de BlaBlaCar, sunt furnizate exclusiv în scop informativ și nu au valoare contractuală. Taxele pentru servicii pot fi calculate în funcție de diverși factori, în special de durata și prețul Cursei.

BlaBlaCar își rezervă dreptul de a modifica în orice moment metodele de calcul pentru Taxele pentru servicii. Aceste modificări nu vor afecta Taxele pentru servicii acceptate de utilizator înainte de data intrării în vigoare a modificărilor.

În cazul călătoriilor transfrontaliere, vă rugăm să rețineți că metodele pentru calculul cuantumului Taxelor pentru servicii și al TVA-ului aplicabil variază în conformitate cu punctul de  întâlnire și/sau destinația Cursei.

Când se utilizează Platforma pentru Curse transfrontaliere sau în afara României, Taxele pentru servicii pot fi percepute de o societate afiliată BlaBlaCar care operează platforma locală.

### **5.3. Rotunjire**

Sunteți de acord și acceptați că BlaBlaCar poate, la propria alegere, să rotunjească în sus sau în jos Contribuția la costuri sau Taxele pentru servicii.

### **5.4. Plata Contribuției la costuri către Conducătorul auto**

Pasagerul se angajează să plătească Contribuția la costuri Conducătorului auto cel târziu la punctul de sosire.

Conducătorul auto se angajează să nu ceară nicio plată, în întregime sau parțial, a Contribuției la costuri înainte de a avea loc Cursa.

**6\. Scopul necomercial al Serviciilor și al Platformei**
----------------------------------------------------------

Sunteți de acord să utilizați Serviciile și Platforma doar pentru a fi pus în contact, pe bază necomercială, cu persoane ce doresc să împartă o Cursă cu dvs.

În contextul unei Curse bazate pe utilizarea în comun a autovehiculelor, recunoașteți faptul că drepturile consumatorului care decurg din legislația UE privind protecția consumatorului nu se aplică relației dumneavoastră cu alți Membri.

În calitate de Conducător auto, sunteți de acord să nu solicitați o Contribuție la costuri mai mare decât costurile reale ocazionate, și care ar putea să genereze profit, fiind menționat că în contextul suportării în comun a costului dvs. trebuie să suportați, ca și Conducător auto, propria parte din costurile aferente Cursei. Sunteți singurul responsabil de calcularea costurilor pe care le suportați pentru Cursă și de verificarea ca suma cerută Pasagerilor dvs. să nu depășescă costurile suportate efectiv (excluzând cota parte a dvs. din costuri).

BlaBlaCar își rezervă dreptul de a vă suspenda Contul în cazul în care folosiți un autovehicul condus de un șofer , alt autovehicul comercial sau taxi sau o mașină de firmă, și din acest motiv obțineți un profit de pe urma Platformei. Sunteți de acord să furnizați către BlaBlaCar, la simpla cerere, o copie a certificatului de înmatriculare a mașinii dvs. și/sau alt document care să ateste că sunteți autorizat să folosiți acest vehicul pe Platformă și că nu obțineți niciun fel de profit din asta.

De asemenea, în conformitate cu Secțiunea 9 din acești T&C, BlaBlaCar își rezervă dreptul de a vă suspenda Contul, de a vă limita accesul la Servicii sau de a rezilia aceste T&C, în cazul unei activități a dvs. pe Platformă care, datorită naturii Curselor oferite, frecvenței acestora, numărului de Pasageri transportați și Contribuției la costuri solicitate, implică o situație de profit pentru dvs. sau care, din orice motiv, sugerează lui BlaBlaCar că obțineți profit pe Platformă.

**7\. Anularea**
----------------

**7.1 Condiții de rambursare în caz de anulare**

Anularea unui Loc într-o Cursă bazată pe utilizarea în comun a autovehiculelor de către Conducătorul auto sau Pasager după Confirmarea rezervării face obiectul prevederilor de mai jos:

– În cazul anulării de către Conducătorul auto, Pasagerului i se rambursează Taxele pentru servicii. Acest lucru este valabil în special atunci când Conducătorul auto:

* A omis să confirme cererea de Rezervare în termenul prevăzut (dacă această opțiune a fost aleasă de Conducătorul auto);
* Anulează o Cursă bazată pe utilizarea în comun a autovehiculelor sau nu a ajuns la punctul de întâlnire după 15 minute de la momentul convenit;

– În cazul anulării de către Pasager, Taxeler pentru servicii sunt reținute de către BlaBlaCar.

Metoda de returnare a Taxelor pentru servicii este stabilită în funcție de metoda care a fost utilizată pentru plata Taxelor pentru servicii, în timp ce termenul de returnare a Taxelor pentru servicii depinde de momentul la care au loc transferurile efectuate de organizațiile bancare.

Atunci când anularea are loc înainte de plecare și din cauza Pasagerului, Locul (Locurile) anulat(e) de Pasager va (vor) deveni automat disponibil(e) pentru ceilalți Pasageri care pot să îl (le) rezerve online și acestea sunt supuse, în mod corespunzător, condițiilor acestor T&C.

BlaBlaCar apreciază, la discreția sa, pe baza informațiilor disponibile, legitimitatea cererilor de rambursare.

**7.2.** **Dreptul de retragere**

Prin acceptarea acestor T&C, acceptați în mod explicit că contractul dintre dumneavoastră și BlaBlaCar constând în conectarea cu un alt Membru va fi executat înainte de expirarea perioadei de retragere care începe la Confirmarea rezervării și renunțați în mod explicit la dreptul dumneavoastră de retragere, în conformitate cu dispozițiile din Ordonanța 34 / 2014 din.

**8\. Comportamentul utilizatorilor Platformei și Membrilor**
-------------------------------------------------------------

### **8.1. Obligațiile tuturor utilizatorilor Platformei**

Sunteți de acord că sunteți singurul responsabil de respectarea tuturor legilor, reglementărilor și obligațiilor aplicabile atunci când utilizați Platforma.

În plus, atunci când utilizați Platforma și în timpul Curselor, vă angajați:

  (i) să nu folosiți Platforma în scopuri profesionale, comerciale sau pentru obținerea de profit;

  (ii) să nu trimiteți către BlaBlaCar (în special la crearea sau actualizarea Contului dvs.) sau oricărui alt Membru informații false, înșelătoare, rău intenționate sau frauduloase;

  (iii) să nu vorbiți și să nu vă comportați în niciun fel, precum și să nu postați niciun conținut (inclusiv Mesaje) pe Platformă de natură defăimătoare, vătămătoare, obscenă, pornografică, vulgară, ofensatoare, agresivă, necuvenită, violentă, amenințătoare, hărțuitoare, rasistă sau xenofobă sau cu conotații sexuale care să incite la violență, discriminare sau ură, să încurajeze activități cu sau utilizarea de substanțe ilegale sau, mai general, ilegale, contrare legii aplicabile, acestor T&C și scopurilor Platformei care ar putea încălca drepturile BlaBlaCar sau ale unei terțe părți sau ar fi contrare bunelor moravuri;

  (iv) să nu încălcați drepturile și imaginea BlaBlaCar, în special drepturile sale de proprietate intelectuală;

  (v) să nu deschideți mai mult de un Cont pe Platformă și să nu deschideți un Cont în numele unei terțe persoane;

  (vi) să nu încercați să ocoliți sistemul de rezervare online al Platformei, în special încercând să transmiteți unui alt Membru detaliile dvs. de contact pentru a face rezervare în afara Platformei și a nu plăti Taxele pentru servicii;

  (vii) să nu contactați niciun alt Membru, în special prin intermediul Platformei, pentru un alt scop în afara celui definit în termenii privind utilizarea în comun a autovehiculelor;

  (viii) să nu acceptați sau să efectuați plăți în afara Platformei;

  (ix) să respectați acești T&C,  și orice alți termeni și condiții ale Platformei care vi se aplică.

### **8.2. Obligațiile Conducătorilor auto**

De asemenea, atunci când utilizați Platforma în calitate de Conducător auto, vă angajați:

  (i) să respectați toate legile, reglementările și codurile aplicabile pentru conducere și autovehicul, în special să dețineți o asigurare de răspundere civilă valabilă în momentul Cursei și să dețineți un permis de conducere valabil;

  (ii) să verificați ca asigurarea dvs. să acopere utilizarea în comun a autovehiculului și că Pasagerii dvs. sunt considerați terțe părți în vehiculul dvs. și, de aceea, sunt acoperiți de asigurarea dvs. pe durata întregii Curse, chiar și în cazul trecerii graniței;

  (iii) să nu vă asumați niciun risc atunci când conduceți, să nu consumați niciun produs care ar putea să vă diminueze atenția și capacitatea de a conduce cu atenție și în deplină siguranță;

  (iv) să postați Anunțuri care să corespundă doar Curselor efectiv planificate;

  (v) să efectuați Cursa așa cum este descrisă în Anunț (mai ales în ceea ce privește intenția de a circula pe autostradă) și să respectați orele și locurile agreate cu ceilalți Membrii (în special locul de întâlnire și punctul de sosire);

  (vi) să nu luați mai mulți Pasageri decât numărul de Locuri indicat în Anunț;

  (vii) să utilizați un vehicul în stare bună de funcționare și care respectă prevederile legale în vigoare și corespunde uzanțelor, și mai ales cu un certificat ITP (Inspecție Tehnică Periodică) la zi;

  (viii) să prezentați către BlaBlaCar sau oricărui Pasager care solicită acest lucru permisul dvs. de conducere, certificatul de înmatriculare a mașinii dvs., polița de asigurare, certificatul ITP și orice alt document care să demonstreze capacitatea dvs. de a utiliza vehiculul în calitate de Conducător auto pe Platformă;

  (ix) să vă informați Pasagerii imediat în caz de pană sau de schimbare a orei sau a Cursei;

  (x) în cazul unei Curse ce presupune trecerea frontierei, să păstrați și să puneți la dispoziția Pasagerului și oricărei autorități care poate solicita acest lucru orice document care să dovedească identitatea dvs. și dreptul dvs. de a trece granița;

  (xi) să așteptați Pasagerii la locul de întâlnire stabilit cel puțin 15 minute după ora stabilită;

  (xii) să nu postați un Anunț pentru un vehicul al cărui proprietar nu sunteți sau pe care nu sunteți autorizat să îl folosiți în scopul utilizării în comun;

  (xiii) să vă asigurați că puteți fi contactat de Pasagerii dvs. telefonic la numărul înregistrat pe profilul dvs.;

  (xiv) să nu obțineți niciun profit prin intermediul Platformei;

  (xv) să nu aveți nicio contraindicație sau incapacitate medicală de a conduce;

  (xvi) să vă comportați în mod adecvat și responsabil în timpul Cursei și în conformitate cu spiritul sistemului de utilizare în comun a autovehiculelor;

  (xvii) să nu refuzați nicio Rezervare pe bază de de rasă, culoare, etnie, naționalitate, religie, orientare sexuală, stare civilă, invaliditate, aspect fizic, sarcină, vulnerabilitate deosebită din cauza situației economice, nume, localitate de reședință, sănătate, opinie politică, vârstă.

### **8.3. Obligațiile Pasagerilor**

Atunci când utilizați Platforma în calitate de Pasager, vă angajați:

  (i) să adoptați un comportament adecvat în timpul Cursei astfel încât să nu deranjați concentrarea sau conducerea Conducătorului auto sau pacea și liniștea celorlalți Pasageri;

  (ii) să respectați autovehiculul Conducătorului auto și curățenia acestuia;

  (iii) în caz de întârziere, să informați imediat Conducătorul auto;

  (iv) să plătiți Conducătorului auto Contribuția la costuri agreată;

  (v) să așteptați Conducătorul auto la locul de întâlnire cel puțin 15 minute după ora stabilită;

  (vi) să prezentați către BlaBlaCar sau Conducătorului auto care solicită acest lucru cartea dvs. de identitate sau orice document care dovedește identitatea dvs.;

  (vii) să nu aveți în timpul Cursei articole, bunuri, substanțe sau animale care ar putea deranja condusul și concentrarea Conducătorului auto sau natura, deținerea sau transportul cărora este contrar prevederilor legale în vigoare;

  (viii) în cazul unei Curse ce presupune trecerea frontierei, să păstrați și să puneți la dispoziția Conducătorului auto și oricărei autorități care poate solicita acest lucru orice document care să dovedească identitatea dvs. și dreptul dvs. de a trece granița;

  (ix) să vă asigurați că puteți fi contactat de Pasagerii dvs. telefonic la numărul înregistrat pe profilul dvs., inclusiv la locul de întâlnire.

În cazul în care ați făcut o Rezervare pentru unul sau mai multe Locuri în numele unor terțe persoane, în conformitate cu prevederile Articolului 4.2.3 de mai sus, garantați respectarea de către această terță persoană a prevederilor acestui articol și, în general, a acestor T&C. BlaBlaCar își rezervă dreptul de a vă suspenda Contul, de a vă limita accesul la Servicii sau de a rezilia acești T&C în cazul încălcării condițiilor acestora de către terța persoană în numele căreia ați rezervat un Loc în baza acestor T&C, în conformitate cu Secțiunea 9 din acești T&C.

**8.4. Raportarea conținutului neadecvat sau ilegal (Înștiințare și mecanism de acțiune)**

Puteți raporta Conținutul de Membru sau un Mesaj suspect, neadecvat sau ilegal așa cum se descrie [aici](https://support.blablacar.com/s/article/Raportarea-con%C8%9Binutului-ilegal-1729197120608?language=ro).

BlaBlaCar, după ce a fost înștiințată în mod corespunzător, în conformitate cu această Secțiune sau de către autoritățile competente, va elimina prompt orice Conținut de Membru ilegal dacă:

* Conținutul de Membru este în mod evident ilegal sau contrar reglementărilor aplicabile; sau
* BlaBlaCar consideră că acel conținut încalcă acești T&C.

În astfel de cazuri, BlaBlaCar își rezervă dreptul de a elimina Conținutul de Membru care a fost raportat în mod corespunzător, suficient de detaliat și de clar, și/sau să suspende imediat Contul raportat.

Membrul în cauză poate contesta deciziile de mai sus luate de BlaBlaCar așa cum se descrie în Secțiunea 15.1 de mai jos.

**9\. Restricții legate de utilizarea Platformei, suspendarea conturilor, limitarea accesului și rezilierea**
-------------------------------------------------------------------------------------------------------------

Puteți termina raporturile dvs. contractuale cu BlaBlaCar oricând, fără niciun cost și fără niciun motiv. Pentru a face acest lucru, mergeți pur și simplu la fila „Închide contul” pe pagina cu profilul dvs.

În cazul (i) unei încălcări de către dvs. a acestor T&C, inclusiv, dar nelimitându-se la obligațiile dvs. de Membru menționate la Articolele 6 și 8 de mai sus, (ii) depășirii limitei stabilite la Articolul 4.3.3 de mai sus sau (iii) dacă BlaBlaCar are motive serioase să creadă că este necesar să-și protejeze siguranța și integritatea, precum și pe cea a Membrilor sau a terțelor părți, sau în scopul investigațiilor și prevenirii unei fraude, BlaBlaCar își rezervă dreptul:

* (i) să rezilieze T&C care vă leagă de BlaBlaCar imediat și fără o notificare prealabilă; și/sau
* (ii) să împiedice postarea sau să elimine orice Conținut de Membru postat de dvs. pe Platformă; și/sau
* (iii) să vă limiteze accesul și utilizarea Platformei; și/sau
* (iv) să vă suspende, temporar sau permanent, Contul.

Suspendarea contului poate însemna și, după caz, că nu veți primi plățile în curs.

De asemenea, după caz, BlaBlaCar vă poate trimite și un avertisment pentru a vă reaminti că aveți obligația de a respecta legea aplicabilă și/sau acești T&C.

Atunci când acest lucru este necesar, veți fi notificați de stabilirea unei astfel de măsuri pentru a putea să vă confirmați angajamentul de a respecta acești T&C și legea aplicabilă, să dați explicații către BlaBlaCar sau să contestați decizia sa. BlaBlaCar va decide, ținând cont de toate circumstanțele fiecărui caz și de gravitatea încălcării, la libera sa alegere, dacă va ridica sau nu măsurile puse în practică.

**10\. Date cu caracter personal**
----------------------------------

În contextul utilizării Platformei de către dvs., BlaBlaCar va colecta și procesa o parte din datele dvs. Personale așa cum este prevăzut în [Politica de confidențialitate](https://blog.blablacar.ro/about-us/privacy-policy). 

**11\. Proprietatea intelectuală**
----------------------------------

### **11.1. Conținutul publicat de BlaBlaCar**

Cu excepția conținuturilor furnizate de Membrii săi, BlaBlaCar este singurul deținător al drepturilor de proprietate intelectuală privind Serviciile, Platforma, conținutul acesteia (în special texte, imagini, desene, logo-uri, filme, sunete, date, grafice), precum și software-ul și bazele de date care îi asigură funcționarea.

BlaBlaCar vă acordă un drept ne-exclusiv, personal și netransferabil de a utiliza Platforma și Serviciile, pentru folosul dvs. personal și particular, pe o bază ne-comercială și în conformitate cu scopurile Platformei și Serviciilor.

Vi se interzice orice altă utilizare sau exploatare a Platformei și Serviciilor, precum și a conținutului acesteia fără acordul prealabil scris al BlaBlaCar. În special, sunt interzise:

  (i) reproducerea, modificarea, adaptarea, distribuirea, reprezentarea în mod public și diseminarea Platformei, Serviciilor și conținutului, cu excepția aprobării exprese a BlaBlaCar.

  (ii) decompilarea și ingineria inversă a Platformei sau Serviciilor, în afara excepțiilor stipulate de textele în vigoare;

  (iii) extragerea sau încercarea de a extrage (în special folosind roboți de exploatare a datelor sau orice astfel de instrumente similare de colectare a datelor) o parte considerabilă a datelor Platformei,

### **11.2. Conținutul postat de dvs. pe Platformă**

Pentru a permite furnizarea Serviciilor și în conformitate cu scopul Platformei, acordați lui BlaBlaCar o licență neexclusivă de a utiliza conținutul și datele pe care le furnizați în contextul utilizării Serviciilor de dvs., care pot include Cererile dvs. de rezervare, Anunțurile și comentariile din acestea, biografii, fotografii, evaluări și răspunsuri la evaluări (în continuare denumit „Conținutul de Membru”) și Mesaje.

Dumneavoastră purtați răspunderea și dețineți toate drepturile cu privire la Conținutul de Membru și Mesajele pe care le încărcați pe Platformă.

. Pentru a permite BlaBlaCar să distribuie prin intermediul rețelei digitale și în conformitate cu orice protocol de comunicare (în special rețeaua de internet și mobil) și să furnizeze conținutul Platformei publicului, dvs. autorizați BlaBlaCar, la nivel global și pe toată durata raporturilor dvs. contractuale cu BlaBlaCar, să reproducă, să reprezinte, să adapteze și să traducă Conținutul de membru, după cum urmează:

  (i) autorizați BlaBlaCar să reproducă, parțial sau în întregime, Conținutul dvs. de Membru pe orice mediu digital prin înregistrare, cunoscut sau încă necunoscut, și, în special, pe orice server, hard disk, card de memorie sau orice alt suport echivalent, în măsura necesară pentru orice operațiune de stocare, backup, transmitere sau descărcare legate de operarea Platformei și de furnizarea Serviciului;

  (ii) autorizați BlaBlaCar să adapteze și să traducă Conținutul dvs. de Membru și să reproducă aceste adaptări pe orice suport digital, actual sau viitor, prevăzut la punctul (i) de mai sus, în scopul de a furniza Serviciile, în special în diferite limbi. Acest drept include mai ales opțiunea de a face modificări formatului Conținutului dvs. de membru, cu respectarea drepturile dvs. morale, în scopul respectării schemei grafice a Platformei și/sau pentru a-l face compatibil din punct de vedere tehnic.

**12\. Rolul lui BlaBlaCar**
----------------------------

Platforma reprezintă o rețea online unde Membrii pot crea și posta Anunțuri pentru Curse în scopul utilizării în comun a autovehiculelor. Aceste Anunțuri pot fi văzute, mai ales, de alți Membrii care caută termenii Cursei și, dacă e cazul, rezervă direct un Loc în autovehiculul respectiv cu membrul care a postat Anunțul pe Platformă.

Folosind Platforma și acceptând aceste T&C, sunteți de acord că BlaBlaCar nu este parte a niciunui contract încheiat între dvs. și ceilalți Membri în scopul împărțirii costurilor aferente unei Curse.

BlaBlaCar nu are niciun control în ceea ce privește comportamentul Membrilor săi și utilizatorilor Platformei. BlaBlaCar nu este proprietarul, nu exploatează, furnizează sau gestionează autovehiculele care fac obiectul Anunțurilor și nu oferă niciun fel de Cursă pe Platformă.

Sunteti de acord și acceptați că BlaBlaCar nu controlează valabilitatea, veridicitatea sau legalitatea Anunțurilor, Locurilor sau Curselor oferite. În calitatea sa de intermediar în procesul de utilizare în comun a autovehiculelor, BlaBlaCar nu furnizează niciun serviciu de transport și nu acționează în calitate de transportator; rolul lui BlaBlaCar este limitat la a facilita accesul la Platformă.  
Membrii (Conducători auto sau Pasageri) acționează pe propria lor răspundere.

În calitatea sa de intermediar, BlaBlaCar nu poate fi considerat responsabil pentru evenimentele aferente unei călătorii și, mai ales, referitor la:

* (i) informații incorecte comunicate de Conducătorul auto în Anunțul său sau prin orice alte mijloace, în ceea ce privește Cursa și termenii acesteia;
*  (ii) anularea sau modificarea unei Curse de un Membru;
*  (iii) neplata Contribuției de către Pasager;
* (iv) comportamentului Membrilor săi în timpul, înaintea sau după Cursă.

**13\. Operarea, disponibilitatea și funcțiile Platformei**
-----------------------------------------------------------

BlaBlaCar încearcă, pe cât posibil, să mențină Platforma accesibilă 7 zile pe săptămână și 24 de ore pe zi. Cu toate acestea, accesul la Platformă poate fi temporar suspendat, fără o notificare prealabilă, din cauza întreținerii tehnice, migrației sau operațiunilor de update, sau din cauza unor întreruperi sau constrângeri legate de funcționarea rețelei.

De asemenea, BlaBlaCar își rezervă dreptul de a modifica sau suspenda, parțial sau în întregime, accesul la Platformă și funcțiile sale, la libera alegere, temporar sau permanent.

**14\. Modificarea T&C**
------------------------

Acești T&C și documentele integrate prin referințe reprezintă întregul acord între dvs. și BlaBlaCar cu privire la utilizarea Serviciilor. Orice alt document, mai ales orice mențiune la Platformă (FAQ, etc.) are doar rol de ghidare.

BlaBlaCar poate modifica acești T&C pentru a se adapta mediului său tehnic și comercial și pentru a respecta legislația în vigoare. Orice modificare a acestor T&C va fi publicată pe Platformă cu menționarea datei de intrare în vigoare, iar dvs. veți fi notificați de BlaBlaCar înainte ca acestea să producă efecte.

**15\. Legea aplicabilă și litigii**
------------------------------------

Acești T&C sunt în limba română și fac obiectul legii din România.

**15.1 Sistemul intern de gestionare a reclamațiilor**

Puteți contesta deciziile pe care le putem lua cu privire la:

* Conținutul de Membru: de exemplu, am eliminat, am restricționat vizibilitatea sau am refuzat să eliminăm orice Conținut de Membru pe care îl furnizați când utilizați Platforma, sau
* Contul dvs.: v-am suspendat accesul la Platformă,

În cazul în care am luat astfel de decizii deoarece Conținutul de Membru constituie conținut ilegal sau este incompatibil cu acești  T&C. Procedura de contestare este descrisă [aici](https://support.blablacar.com/s/article/Cum-se-poate-contesta-eliminarea-con%C8%9Binutului-sau-suspendarea-contului-1729197123040?language=ro).

**15.2 Soluționarea litigiilor în afara instanțelor de judecată**

De asemenea, dacă este necesar, puteți să vă prezentați plângerile privind Platforma sau Serviciile noastre pe platforma privind rezolvarea disputelor, plasată online de Comisia Europeana, accesibilă [aici](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage). Comisia Europeană va transmite plângerea dvs. instanței naționale competente. În conformitate cu regulile aplicabile medierii, sunteți obligat, ca înainte de orice cerere de mediere, să notificați BlaBlaCar în scris în legătura cu orice dispută, pentru a obține o soluție amiabilă .

De asemenea, puteți să trimiteți o solicitare unui organism de soluționare a litigiilor din țara dvs. (găsiți lista [aici)](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show).

**16\. Prevederi legale**
-------------------------

Platforma este publicată de Comuto SA, o companie cu răspundere limitată, având un capital social de 155,880.999 EUR, înregistrată la Oficiul Registrului Comerțului sub numărul 491.904.546 (cod înregistrare de TVA intra-UE: FR76491904546), cu sediul social în Avenue de la République nr. 84, 75011 Paris (Franța), reprezentă de Directorul său executiv, Nicolas Brusson, Director responsabil cu publicarea pe site.

Acest site web este găzduit pe serverele Google Cloud din Țările de Jos.

Comuto SA este înregistrată în registrul operatorilor de servicii de transport și cazare cu următorul număr: IM075180037.

Garanția financiară este oferită de: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Paris – Franța.

Asigurarea de răspundere civilă profesională este subscrisă la: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paris – Franța.

Comuto SA este înregistrată în registrul intermediarilor de asigurări, servicii bancare și financiare sub numărul de înregistrare (Orias) 15003890.

Pentru orice întrebare, puteți contacta Comuto SA folosind acest [formular de contact](https://support.blablacar.com/s/contactsupport?language=ro).

**17\. Digital Services Act (****Regulamentul privind serviciile digitale)**
----------------------------------------------------------------------------

**17.1.  Informații privind numărul mediu lunar de beneficiari activi ai serviciului în Uniunea Europeană**

În conformitate cu articolul 24, alineatul 2 din Regulamentul Parlamentului European și al Consiliului privind o piață unică pentru serviciile digitale și de modificare a Directivei 2000/31/CE („DSA”), furnizorii de platforme online au obligația de a publica informații despre numărul mediu lunar de beneficiari activi ai serviciului prestat de aceștia în Uniunea Europeană, calculat ca medie în ultimele șase luni. Scopul publicării acestor informații este de a stabili dacă o platformă online îndeplinește criteriul de „platformă online foarte mare” în temeiul DSA, și anume, depășește pragul de 45 milioane de beneficiari activi, în medie și lunar, în Uniune.

La 31 ianuarie 2024 numărul mediu lunar de beneficiari activi ai BlaBlaCar în perioada cuprinsă între august 2023 și ianuarie 2024, calculat ținând cont de Preambulul 77 și articolul 3 din DSA, înainte de adoptarea unui act delegat specific, a fost de 4,82 milioane în UE.

Aceste informații sunt publicate exclusiv în scopul respectării prevederilor DSA și nu ar trebuie utilizate ca bază în alte scopuri. Acestea vor fi actualizate cel puțin o dată la șase luni. Abordarea noastră pentru realizarea acestui calcul poate evolua și poate necesita modificări în timp, de exemplu, datorită unor modificări ale produsului sau unor noi tehnologii.

**17.2. Punct de contact pentru autorități**

În conformitate cu articolul 11 din DSA, dacă sunteți membru al autorităților UE competente, al Comisiei Europene sau al Comitetului European pentru Servicii Digitale, ne puteți contacta în legătură cu chestiuni referitoare la DSA prin e-mail, la [\[email protected\]](https://blog.blablacar.ro/cdn-cgi/l/email-protection).

Ne puteți contacta în limbile engleză și franceză.

Vă rugăm să rețineți că această adresă de e-mail nu este utilizată pentru comunicarea cu membrii. Dacă aveți întrebări despre utilizarea BlaBlaCar, ne puteți contacta în calitate de Membru prin acest [formular de contact](https://support.blablacar.com/s/contactsupport?language=ro).