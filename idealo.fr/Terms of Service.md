Mentions légales / Conditions Générales
---------------------------------------

Nous attirons votre attention
-----------------------------

Sur le fait qu'idealo.fr n'est pas une boutique en ligne, mais exclusivement un comparateur de prix. C'est pourquoi vous ne pourrez sur idealo.fr

* **pas** commander ni réserver des produits ou des services,
* **pas** vous informer sur le statut de votre commande ou de votre réservation, ni sur le suivi de votre dossier,
* **pas** trouver d'informations sur les prestations d'un marchand ou d'un fournisseur.

Pour toutes ces requêtes, nous vous invitons à vous adresser directement au marchand ou au prestataire de service en question.

En cas de litige avec un marchand, les utilisateurs peuvent utiliser [la plateforme de règlement en ligne des litiges (RLL)](http://ec.europa.eu/consumers/odr/).

Identité
--------

idealo internet GmbH  
Zimmerstraße 50  
10888 Berlin, Allemagne  
fon +33 9 75 17 88 31 (pour vos questions à propos d'une commande, merci de prendre contact directement avec le marchand concerné)  
fax +33 9 75 18 37 78  
[contact@idealo.fr](javascript:void(0))

Directeurs : Maxim Nohroudi, Jörn Rehse, Martin Sinner  
TVA : DE813070905  
HRB 76749 – Tribunal de première instance Berlin-Charlottenburg

Référencement d'une boutique en ligne
-------------------------------------

Si vous souhaitez inscrire une boutique en ligne sur idealo.fr en vue d'un référencement sur notre comparateur de prix, rendez-vous sur [**cette**](https://business.idealo.com/) page.

Point de contact et service clientele
-------------------------------------

Notre point de contact unique est joignable via e-mail ou notre formulaire de contact:

E-Mail: contact@idealo.fr

Formulaire de contact:  [https://kundenservice.idealo.com/hc/fr/requests/new](https://kundenservice.idealo.com/hc/fr/requests/new)

Notre centre d’aide: [kundenservice.idealo.com/hc/fr](https://kundenservice.idealo.com/hc/fr)

Presse
------

Votre interlocutrice pour toute question de presse est Anna Perret [presse.france@idealo.fr](javascript:void(0))

Responsable de la publication
-----------------------------

Jörn Rehse  
Zimmerstraße 50  
10888 Berlin, Allemagne

Protection des mineurs
----------------------

John Strassburg [privacy@idealo.fr](javascript:void(0))

Protection des données
----------------------

[privacy@idealo.fr](javascript:void(0))  
Nous attirons expressément votre attention sur le fait que si vous utilisez cette adresse électronique, les contenus ne seront pas exclusivement portés à la connaissance de notre délégué à la protection des données. Si vous souhaitez échanger des informations confidentielles, nous vous prions donc de nous contacter d'abord directement via cette adresse e-mail.

Technique
---------

Tel +33 9 75 17 88 31  
[support.technique@idealo.fr](javascript:void(0))

Publicité sur idealo
--------------------

[publicite@idealo.fr](javascript:void(0))

Règlement sur les services numériques (Digital Services Act)
------------------------------------------------------------

Point de contact central pour la communication avec les autorités des États membres de l'UE, la Commission européenne et l'organe européen pour les services numériques, conformément à l'article 11 de la loi sur les services numériques : dsa-authorities@idealo.de. La communication est possible en allemand et en anglais. 

Veuillez noter que ce point de contact est exclusivement destiné à la communication avec ces organismes en ce qui concerne l'application de la loi sur les services numériques. Les demandes d'autres personnes ou entités ne peuvent pas être traitées via ce point de contact.

Informations sur la moyenne mensuelle des destinataires actifs de nos comparateurs de prix dans l’Union europénne vous trouverez [ici](https://www.idealo.de/unternehmen/digital-services-act).

Informations sur le fonctionnement et l’efficacité du système interne de traitement des plaintes conformément à l’article 11 du Règlement (UE) 2019/1150
--------------------------------------------------------------------------------------------------------------------------------------------------------

Entre le 1er janvier 2024 et le 31 décembre 2024, idealo internet GmbH a reçu un total de 56 plaintes d'entreprises utilisatrices inscrites chez idealo. Celles-ci concernaient majoritairement le traitement des paiements ainsi que les sanctions ou mesures restrictives imposées par idealo (par exemple les résiliations). Au terme de la période de référence, un (1) cas était encore en cours de traitement. En règle générale, les plaintes ont pu être résolues en huit (8) jours ouvrables.

Sécurité
--------

idealo prend la sécurité de l'information très au sérieux. Si tu as des observations sur la sécurité technique, n'hésite pas à contacter security@idealo.de. Lors de la transmission d'informations sensibles, nous recommandons de crypter le message avec notre Public Key.

\-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF87aQsBEAD0mO8FIrW16mfX5D1uC6KFAz1ztGbYSYV/xIfq0cI7EGySCgG/ 2z8TDB7dD4yNQCJad7II4EsdimGYFYFxoOZariZyjZQv8wNLmRvBL6MXjkf7DiN7 K/sIKlSfu+XC+fzPKW2/z597rDHlzUdLPfwrCeAFhU+B6I/BKQ9ydLyo6fxn29xZ uvK4+DdohPwdj4AYFbluEnJjNXUJb+hb8l3zP8uG7Xm5FCoKqJLc3TGj4oAWvwWl KDHWCGn1UKQ6u0Mp6/oxr7ojc3sKPdPmP9ZhC4kr2WZp7wBLBusn3S8ln6X6SByc qoyykaMs8G31/ZHP8hjuNKyT7HwY78z65SLrPjVTPeAlJjfaDxeg/yptVrPmszt8 oSiV6CkXDQtnIKFo3vZpzPsxax3V8TYw8KikWJzu2rNmBrtC0w+QYJ9sWfiQYM5B yjERwAN1UMadWJ1QoRZQIdkzimQtynHGMNDlwRCNfds5OObFN+7qz0ruE8XBnTBe az0vFJSlU3e5VvKLAraTGAHcBB9T5UvGa17w5yACf/cxsZ19GcR1TF4pftJtfM1h ldXElkonlmf5sMzD/6J/A5Je5mbgqJuKouEGx4l6v0kwIFHRSqUaARDE4VByafAY k0l1/Dg+74jShLg7CNz4Mdj3UscnQOvopHsVdTUAFASq3Bul9dUuMHn1PwARAQAB tClpZGVhbG8gU2VjdXJpdHkgVGVhbSA8c2VjdXJpdHlAaWRlYWxvLmRlPokCVAQT AQgAPhYhBBIfTVJe0FkyGFvwe4UAZW4AbE5YBQJfO2kLAhsDBQkJZgGABQsJCAcC BhUKCQgLAgQWAgMBAh4BAheAAAoJEIUAZW4AbE5YXfQP/RJXAk0fE+3bcM7DYBCR fQGnlh+HKw5+tvfI86sL1qFcHohWqMN5jRV43sUijqZ3GQ1Km2YShgdGqRXOw7bN Dq7r8nEIDi2TyCQFUmuk+8fu5h7hG7s9Pl3MhSxACgjIzM0Pv2cmUSzQsnF4lu8G I7Tb5fciKD90Iwu0CFMwBsosub0sj0r/xbCqsa4cq5aoYtbJSxQPsTFmYc0ojjnV wO4jYaABQOUgB3xkWkZX7xASqvTVLJ7QxVmw2AIGvutr4Clo8GICJu8Z+SVryql1 yQbLciZDWi+lcH1fBfDpmhx2QqE3qUJ/YR3Nhu66eojx41FraSXAQ52ebSzdYcd0 KsYqRRfuwK4LluGdlmJA/zIZv7CvoT/iZjC5iuGtuwAw3MRfIxtuY4WUDEP2iSy9 YkZbs1pE37cK/i5+PEViRVIqmGcnalT6ktQVSjMm2DN5aXwfFnRDsyoe2w5xKs9+ ZdiUzMvmtfT/nPNSDbfQqtNO81RpMt2WZRk++mhsp/3hdpTS+xhaffY8fO6MJ+qT 8UHEAsfJ9IWOKDLq9CkS+OSe0jIihKYXS4BC9vQL01w9Xf/Nu50XHvNu59yZr1Sl dUxUtmFbLofn2uRTS8CMg79XiaBVJLTQZsiAu8VelcepC7+I1VOK8VJHlObYGh88 w1KMVkHIU74UFAJhOxo4X/HguQINBF87aQsBEADAZlw+/XnqFhTVtCsEJhRqj2xY UOz66BKCSDi244uXEmBWnYZMJnk3y++7nfOsMU1fyhM0aF8WqsbLexLGZg056Gkl kYso2RLeIikcsObAcKMm83Z4aZVLFe97L8ffN4RGpE+d6+eJfAQdYDluSm4S9gUU 5O8DtBP44QC4BIMmWSITtiLTqM3N5HbWXtBjKMVTpM2W8elOhpFvi3r/lbG3h7KL kpBILmuImOlcmsPeA+YeXcu27mYPPhtFv+MUS+VctERg/YpVZOOFnvq6YLoADcf7 9iucd4cVZfXdGJU7pWrCKMLjLrgCmIw+JjJCW/qezG5RpYWMHYh3oY0iV0gYJDSc iOkMhmezNExqLTQZk2+ZEY6ewFj4gD8qr1LYBsqGnwqQAsqAcErrIoDeCWk1y9/n xa6jopahzpdKtD4n0RDuz53IXOsy+wkMQ+oyRbnjqqD44r0G+rJdHNw/rTk2iDqM Itkai/t4wCfZGQ64OnORrdrUJepT0RGKOQ1WMczQwlTg+0IyXUKq9CYh8+kpEs20 ZOr3qAUz10Y6ANk7pifDDSSqkVPyCjlhxQrSVcz23jDcNOU6UbmFOvz+2Mjjhs4L tn2j6A78TRZifmXFv5P0YYBhtQd0ilXF0K+xZihcTDXvBlPIkY7XnAtOUWKdYpe6 087c5+w1EYDpT/Mj1QARAQABiQI8BBgBCAAmFiEEEh9NUl7QWTIYW/B7hQBlbgBs TlgFAl87aQsCGwwFCQlmAYAACgkQhQBlbgBsTlg9Uw//bZM9i02Hj/4+l49jHWvC 64JJE8usS/PST1BeK34/RDVGjWCsuJ6VWbPFWr1hUyrArcOgwVhEa9HKC6Tqv8TH QwiRgWQ+H0qDUKSsoKw3XrQBsg+YY7+AJCYmgomEqG4BK66/nX3h6UahU4RStayy /MVW5dzX5HA+RnctK9hA1u0fQZzwZx6VCYkTE4AlGnCc4FS6p67dmOhjug5RJuOg Qax89GJz3srU5gXU+OPrq2BaG7+UhjCXj8OQge+XGdt3qbkHtN3iGeH2R3Oq7jjz sESgL6J/teRP89IkP6YktAxKZhg/WzgqYEHVT19cL+OynGWzCvsD4EbB1CtsIuUw XEUjh2DDVZ28PCRREK8p6RoEIZBLn/2p1R7cw5ujzQrsvSl3QtZJC8uF8IJhzRZv WXpYfXiVR6MLBUJemcB/DQnB55v6ot8hR+1Wq9cwfGS70q023AOVKRhogw/zyGJf svE2KTqmvSK8uhbgJCJ52KZpS1Ppvt39pGb+bhu2M+5/dws+1eWB8h5/r+XY/b8v KUwNn9hh7vWgrpnKqrYGY5fLEThLbjOd1HQ8IJsLWIzPhsxxQjHs4QSgZ/CI+voY x4evFRJWTXIEfVE0M4V/+vagAMdjgrADWYcH31WiXK9ZEQNTx5riDC2zA2JVAMJ9 Xy+KOsLD0XFjnPZeFHNyEus= =Ids7

\-----END PGP PUBLIC KEY BLOCK-----

Confidentialité et protection des données
-----------------------------------------

La déclaration d’idealo sur la confidentialité et la protection des données peut être consultée [ici](https://www.idealo.fr/protection-donnees.html).

Conditions générales d'utilisation des portails de comparaison de prix d'idealo
-------------------------------------------------------------------------------

idealo internet GmbH, Zimmerstraße 50, 10888 Berlin (ci-après « **idealo »**) gère différents portails de comparaison de prix (ci-après « **portails de comparaison** »). Cela est réalisé par différents sites web et applications mobiles.

### **1\. Champ d'application, modifications** 

1.1 Les conditions générales d'utilisation suivantes (ci-après « CGU ») s'appliquent à l'utilisation des services fournis par idealo via 

* les domaines idealo.fr ainsi que  
    
* les applications mobiles correspondantes 
    

Cela inclut l'utilisation des services qui y sont proposés (ci-après dénommés collectivement « **services d’idealo** »). 

1.2 Les portails de comparaison fournis par idealo comprennent : 

* « idealo Comparateur de prix » et 
    
* « idealo Vols » 
    

1.3 En plus des présentes CGU, les conditions générales de vente d'idealo (ci-après « CGV d’idealo »), qui peuvent être consultés, imprimés et/ou stockés via [idealo Business](https://business.idealo.com/fr), s'appliquent à la soumission d'offres de produits ou de services sur idealo par les entreprises utilisatrices.    

1.4 Les CGU s'appliquent dans la version qui est intégrée au contrat respectif avec idealo lors de la conclusion du contrat. En particulier, lors de la conclusion d'un contrat d'utilisation au moment de l'enregistrement pour un compte client d'idealo (cf. paragraphe 4) ou la soumission d'une évaluation (cf. paragraphe 6).  

1.5 idealo est en droit de modifier les CGU. Cependant, un contrat existant avec les personnes enregistrées peut seulement être modifié si 

* les personnes concernées ne sont pas défavorisées par les modifications, ou 
    
* les modifications sont nécessaires pour combler des lacunes réglementaires apparues après la conclusion du contrat, que ce soit en raison des changes du régime juridique ou de la jurisprudence des hauts tribunaux ou du développement des services d’idealo, ou 
    
* les modifications n'affectent pas les dispositions essentielles du contrat et si elles sont nécessaires ou appropriées pour s'adapter aux développements futurs, notamment aux évolutions techniques, juridiques ou réglementaires, aux changements des conditions du marché ou pour des raisons équivalentes.   
    

Les dispositions essentielles de la relation contractuelle sont par exemple le paragraphe 5 (alerte prix), le paragraphe 6.5 (cession de droits) et le paragraphe 9 (Responsabilité, limitation de la responsabilité) des CGU. 

1.6 Les modifications des CGU en cours d’un contrat sont notifiées aux personnes enregistrées par courriel ou lors de leur prochaine connexion. Elles sont considérées comme convenues lorsque 

* la personne concernée ne s'y oppose pas dans un délai de quatre (4) semaines à compter de la réception de la notification de modification, et que  
    
* idealo a attiré l'attention sur cette conséquence juridique dans la communication.  
    

L'objection nécessite la forme écrite, c'est-à-dire au moins un courriel. En cas d'objection écrite d'une personne, le contrat d'utilisation est maintenu aux conditions antérieures sans les modifications. idealo se réserve dans ce cas le droit de résilier le contrat d'utilisation conformément au paragraphe 4.8 des CGU. 

### **2. Comparaison des prix et recherche de produits** 

2.1 Les portails de comparaison s'adressent à des personnes physiques (ci-après « **utilisateurs** »). idealo ne propose ses services qu'à des utilisateurs adultes et à capacité juridique illimitée.  

2.2 Sur les portails de comparaison, les utilisateurs ont la possibilité de rechercher, sans engagement, des marchandises ou des services (ci-après dénommés collectivement « **produits** ») et de s'informer sur leurs prix, leurs sources d'approvisionnement et leurs autres caractéristiques. En outre, idealo propose d'autres services dans le domaine de la comparaison de produits et de prix.   

2.3 L'utilisation des portails de comparaison est gratuite. 

2.4 Les produits affichés sur les portails de comparaison sont fournis par des tiers. Les tiers sont notamment des commerçants en ligne et autres fournisseurs de produits sur l’Internet (ci-après « **fournisseurs** »).  

2.5 La recherche d'informations sur les portails de comparaison est majoritairement automatisée. Elle repose en grande partie sur les informations qu'idealo reçoit de ses partenaires. Les partenaires d'idealo sont notamment des fournisseurs et fabricants de produits, des places de marché et des services d'intermédiation. Normalement les offres de produits affichés sont automatiquement actualisées plusieurs fois par jour. Les informations sur les portails de comparaison et sur les portails des fournisseurs peuvent différer dans certains cas. Cela peut notamment concerner les caractéristiques suivantes : 

* le prix (et donc le classement des offres),  
    
* la disponibilité, 
    
* les frais de livraison ou 
    
* le délai de livraison des produits. 
    

2.6 idealo utilise différents systèmes de recommandation afin d'aider les utilisateurs dans leur recherche d'offres de produits. idealo affiche les recommandations qui, selon l'avis d'idealo, pourraient intéresser les utilisateurs. Afin de permettre la fourniture des recommandations aussi précises que possible, idealo génère les recommandations sur la base de divers paramètres. Plus de détails sur les systèmes de recommandation, les principaux paramètres et leur pondération relative peuvent être consultés [ici](http://idealo.fr/meilleures-offres/ranking).  

2.7 Les portails de comparaison ne considèrent pas tous les fournisseurs et toutes les offres disponibles sur le marché. Les informations affichées sur les produits doivent servir d'aperçu des offres des fournisseurs coopérant avec idealo. Elles ne prétendent pas à l'exhaustivité. idealo est en droit de retirer des produits à tout moment et sans préavis. 

2.8 idealo est en droit de modifier, de complémenter, de supprimer ou de suspendre des parties des services, les services en totalité et/ou certaines fonctionnalités.  

C'est possible par exemple  

* pour appliquer une décision de justice et/ou une décision administrative,  
    
* en cas d’amendements législatifs,  
    
* pour éliminer des vulnérabilités ou 
    
* pour élargir ou limiter l'offre des services d’idealo. 
    

Cela peut également être implémenté sans notification individuelle. Le paragraphe 1.5 n'en est pas affecté.

### **3. Relations contractuelles**

3.1 En cas d'intérêt pour un achat, les utilisateurs sont généralement dirigés vers le portail du fournisseur de l'offre concernée. Il incombe aux utilisateurs de vérifier toutes les informations pertinentes avant de commander un produit. Y compris l'actualité des informations, notamment des prix. idealo vous avise que : les utilisateurs qui effectuent des prestations préalables aux fournisseurs (par ex. paiement anticipé) assument en général le risque de défaillance.  

3.2 Les contrats d'achat de produits sont conclus exclusivement entre les utilisateurs et les fournisseurs. En conséquence, les conditions générales de vente des fournisseurs peuvent s'appliquer à ces contrats. idealo, comme portail de comparaison, n'offre pas lui-même des produits à l'achat. Aucun contrat d'achat, de service ou autre n'est donc conclu avec idealo pour l'acquisition de produits affichés. 

3.3 idealo ne garantit pas que les produits affichés sur les portails de comparaison puissent véritablement être commandés ou achetés auprès des fournisseurs aux conditions indiquées. idealo n'a pas non plus l'obligation de veiller à l'exécution de ces contrats. Toutes les obligations contractuelles découlant de ces contrats et en rapport avec ceux-ci naissent uniquement entre les utilisateurs et les fournisseurs.  

3.4 idealo et les fournisseurs proposent leurs services respectifs indépendamment les uns des autres. idealo n'a aucun contrôle sur le contenu ou l'accessibilité des services tiers.  

### **4. Enregistrement au compte client (« Mon idealo »)** 

4.1 En principe, il est possible d'utiliser les portails de comparaison sans s'enregistrer.  

4.2 Les utilisateurs ont la possibilité de créer leur propre compte client (« Mon idealo »). Ce compte client gratuit offre aux utilisateurs des fonctions et des services supplémentaires. En font partie   

* « l'alerte prix » (voir paragraphe 5),  
    
* « Ma sélection » (les produits qui intéressent l’utilisateur et leur évolution de prix peuvent être sauvegardez sur une liste par un simple clic),  
    
* les « alertes bons plans » (notifications et courriels personnalisés sur les changements de prix actuels pour les produits auxquels l’utilisateur s’intéresse sur le portail de comparaison).  
    

4.3 La fourniture de l'option d'enregistrement du compte client ne constitue pas l'obligation de l'accepter. Elle constitue une invitation à soumettre une offre de création d'un compte client. Pour s'enregistrer, les utilisateurs doivent indiquer leur adresse électronique, leur mot de passe et, le cas échéant, d'autres informations. idealo envoie un courriel à l'adresse électronique indiquée après l'enregistrement. Ce courriel contient un lien sur lequel il faut cliquer pour confirmer l'enregistrement (Méthode de double opt-in). La confirmation de l'enregistrement et la fourniture subséquente d'un compte client sont effectuées sous réserve de vérification par idealo. Les fonctions et services inclus dans le compte client sont gratuits pour les utilisateurs. Leur fourniture par idealo n'est pas obligatoire.  

4.4 Les utilisateurs sont tenus de n'utiliser l'option d'enregistrement que pour eux-mêmes. L'utilisation d'identités de tiers est interdite explicitement. Cela vaut également pour l'utilisation non autorisée de données de tiers. 

4.5 Les utilisateurs sont seuls responsables de la sécurité de leurs données d'accès. Ils ont notamment l'obligation de garder leur mot de passe secret et de ne pas le transmettre à des tiers. idealo ne transmettra pas non plus les mots de passe à des tiers. Les utilisateurs doivent choisir un mot de passe suffisamment sécure et le changer régulièrement. Cela serve d'assurer la sécurité et de prévenir toute utilisation abusive du compte client. 

4.6 idealo est en droit de rendre inaccessible temporairement ou permanentement l'accès aux comptes clients. Cela peut résulter s’il y a des indices que les comptes clients sont utilisés 

* de manière destinée ou susceptible de compromettre le fonctionnement des services, ou  
    
* pour commettre ou encourager des infractions.  
    

idealo décidera en toute équité de l'existence d'indices correspondants. idealo informera les utilisateurs concernés d'un blocage et leur donnera la possibilité de prendre position. 

4.7 Les utilisateurs peuvent à tout moment demander à idealo la suppression de leur compte client. Le contrat d'utilisation correspondant avec idealo est alors résilié. Les utilisateurs peuvent adresser leur demande de suppression par courriel à : supprimermoncompte@idealo.fr. La suppression doit en principe être demandée à partir de l'adresse électronique avec laquelle les utilisateurs se sont initialement enregistrés pour le compte.  

4.8 idealo est en droit de résilier les contrats d'utilisation à tout moment en envoyant un message correspondant sous forme de texte par courriel.

### **5.** **Alerte prix (seule pour les utilisateurs enregistrés et idealo Comparateur de prix)**

5.1 idealo offre aux utilisateurs, sur les portails d’idealo comparateur de prix et sans l’option facultative de configurer un alerte prix pour des produits sélectionnés. Cette option n'est offerte qu'aux utilisateurs enregistrés et aux utilisateurs de l’application idealo : comparateur de prix. Les utilisateurs peuvent définir un prix souhaité pour les produits sélectionnés à l'aide de l’alerte prix. Si ce prix souhaité est atteint ou non atteint par de nouvelles offres, les utilisateurs peuvent être notifiés par idealo.  La notification est gratuite et sans obligation d’achat. Les utilisateurs qui ont installé l’application idealo : comparateur de prix sont informés par une notification push. Les utilisateurs enregistrés sont également informés par courriel. 

5.2 idealo ne garantit pas que les utilisateurs soient notifiés à temps des changements de prix. idealo ne garantit pas non plus, en cas de notification, qu'un produit sera fourni à un prix donné pendant une période donnée. L'alerte de prix sert uniquement comme aide informative sans obligation. En aucun cas, une notification ne constitue une déclaration de volonté visant à la conclusion d'un contrat. Ceci vaut aussi bien pour idealo que pour le fournisseur concerné. Il incombe aux utilisateurs de vérifier l'actualité des prix avant de passer une commande.   

5.3 Si un prix souhaité n'est pas atteint dans un délai trop long, idealo peut désactiver l’alerte prix sans préavis pour le produit concerné.  Les utilisateurs ont alors la possibilité de configurer à nouveau l’alerte prix pour ce produit. Les utilisateurs peuvent également modifier eux-mêmes à tout moment les paramètres de leur alerte prix.  

5.4 Si les utilisateurs ne souhaitent plus être notifiés sur un produit, ils peuvent supprimer l'alerte prix réglée via leur compte client ou l'application idealo : comparateur de prix. 

### **6. Contributions des utilisateurs** 

6.1 idealo propose parfois aux utilisateurs des portails de comparaison de s'exprimer sur les produits listés ou leurs fournisseurs. Cela peut être fait au moyen de contributions textuelles et d'étoiles (ci-après dénommées « évaluations »). Une étoile représente la note la plus basse et cinq étoiles la plus élevée.  Les évaluations sont visibles par tous les visiteurs du portail de comparaison concerné. Elles sont également visibles par les fournisseurs. Les fournisseurs ont la possibilité de réagir aux évaluations en rédigeant un commentaire d'évaluation (ci-après « commentaire »). Le commentaire apparaît en dessous de l'évaluation.    

6.2 Les utilisateurs ne peuvent donner des évaluations que s'ils sont ou ont été clients du fournisseur évalué ou s'ils utilisent ou ont utilisé effectivement les produits évalués. Ils ne peuvent pas donner d'évaluations s'ils sont propriétaires du fournisseur évalué, des membres de leur famille ou des employés du fournisseur, ou s'ils ont reçu ou se sont vu promettre une contrepartie pour les évaluations par le fournisseur ou un représentant du fournisseur. Cela inclus, entre autres, des rabais, des bons, des prestations en espèces, des cadeaux, des points de fidélité et des primes de recommandation.    

6.3 idealo ne vérifie pas si les évaluations proviennent de consommateurs qui ont effectivement utilisé ou acheté le bien ou le service évalué, à moins que l'évaluation en question ne soit marquée comme « avis vérifié ». Pour plus d'informations sur ce processus de vérification, consultez les pages détaillées des produits concernés et le site concernant [avis cimenio sur idealo](https://www.idealo.fr/meilleures-offres/reviews). 

6.4 Les utilisateurs sont tenus d'indiquer leur numéro de client ou de commande lorsqu'ils évaluent un fournisseur. Celui-ci est ensuite transmis au fournisseur. Le fournisseur a ainsi la possibilité d'établir un lien entre l'évaluation et le processus de commande correspondant et d'identifier l'utilisateur. Le numéro de client ou de commande n'est pas publié dans l'évaluation. Les fournisseurs peuvent adresser leurs objections justifiées aux évaluations à : contact@idealo.fr. 

6.5 Lors de l'envoi d'évaluations, les utilisateurs sont tenus de respecter les dispositions légales, les présentes CGU ainsi que la directive d'évaluation d'idealo localement applicable. La même obligation s'applique aux fournisseurs lors de l’envoi de commentaires. La directive d'évaluation d’idealo peut être consultée [ici](https://kundenservice.idealo.com/hc/fr/articles/17043859697170-Directive-d-%C3%A9valuation). Conformément à la législation applicable, les évaluations et les commentaires doivent être véridiques, pour autant que des faits y soient allégués ou qu'ils impliquent un noyau de faits. Ils ne doivent pas contenir d'insultes ou de critiques injurieuses, ni être illicites pour d'autres raisons. idealo se réserve le droit de demander à ses utilisateurs et fournisseurs de prouver qu'ils respectent ces dispositions et de stipuler à tout moment des dispositions supplémentaires pour la soumission des évaluations et des commentaires. La légalité des évaluations soumises avant l'entrée en vigueur des nouvelles dispositions n'est pas affectée. 

6.6 idealo se réserve le droit de rendre inaccessible ou de retirer les évaluations et les commentaires qui, selon l'appréciation d'idealo, enfreignent les dispositions du paragraphe 6.5 des présentes CGU ou la directive d'évaluation d'idealo. idealo informera les utilisateurs si leur évaluation est concernée par une telle décision de modération. De même, idealo informera les fournisseurs si leur commentaire est concerné par une telle décision de modération. La notification aux utilisateurs sera envoyée par courriel à l'adresse électronique indiquée lors de la soumission de l'évaluation. Pour plus d'informations sur la modération des évaluations et autres contenus par idealo, consultez paragraphe 7 des présentes conditions d'utilisation. Les utilisateurs et les fournisseurs concernés par une décision de modération ont accès au système interne de traitement des réclamations d'idealo. De plus amples informations se trouvent en paragraphe 8 des présentes CGU.    

6.7 Si des utilisateurs fréquemment soumettent des évaluations manifestement illicites, idealo suspendra l'utilisation du service d'évaluation pour ces utilisateurs pendant une période appropriée, après les avoir avertis au préalable. La décision sur la suspension et, si applicable, sur sa durée, considérera tous les faits et circonstances connus par idealo. Il s'agit notamment (i) du nombre absolu d'évaluations manifestement illicites soumises au cours d'une période, (ii) de leur part relative par rapport au nombre total d'évaluations soumises au cours de cette période, (iii) de la nature des contenus illicites et de la gravité des reproches qui y sont liés, y compris leurs conséquences pour les personnes concernées par l'évaluation, et (iv) les intentions motivant l'évaluation, si elles sont perceptibles.   

6.8 En soumettant des évaluations, y compris des textes, les utilisateurs accordent à idealo le droit non exclusif, gratuit et non limité temporairement ou territorialement d'utiliser ces évaluations en ligne, hors ligne, dans tous les médias et par tous les moyens de diffusion. Cela inclus également leur     

* copie,  
    
* publication,     
    
* affichage et diffusion,     
    
* stockage,     
    
* archivage,     
    
* mise à disposition de tiers,     
    
* mise à disposition du public et la reproduction, et    
    
* intégration dans d'autres œuvres.     
    

Cela inclut également les droits d'adaptation et de traduction des évaluations. Cette disposition ne s'applique que si l'édition et l'adaptation préservent le caractère intellectuel de l'œuvre.   

6.9 La concession de droits susmentionnée autorise notamment idealo à publier des évaluations sur les portails de comparaison. idealo peut également utiliser les évaluations comme publicité pour les portails de comparaison, les produits listés et les fournisseurs.   En outre, idealo peut autoriser les fournisseurs à utiliser les évaluations pour leur publicité.  La portée et la teneur des droits accordés se limitent à ce qui est raisonnable 

* pour gérer les portails de comparaison idealo et en faire la publicité, et     
    
* pour faire de la publicité pour les produits et/ou les fournisseurs évalués. 
    

6.10 Il n'existe aucun droit à l'utilisation du système d'évaluation ou à la publication d'évaluations. idealo est habilité à tout moment, dans ses relations avec les utilisateurs, à rendre inaccessible les évaluations qu'il considère, selon son appréciation, comme non pertinentes pour ses visiteurs. Cela s'applique notamment aux évaluations qui ne sont plus à jour, qui se réfèrent à des produits qui ne sont plus listés sur le portail de comparaison ou si le fournisseur évalué a changé son nom d'entreprise après l'envoi de l'évaluation. idealo est en plus habilité à tout moment à modifier, complémenter et/ou suspendre des parties ou le système d'évaluation en total. Une annonce correspondante n'est pas nécessaire et n'est généralement pas effectuée. 

### **7. Modération des contenus et système de notification** 

7.1 Les contenus publiés par les fournisseurs et les utilisateurs sur les portails de comparaison ne doivent pas enfreindre les dispositions légales, les droits de tiers et, si cela a été convenu au cas par cas, les présentes CGU, les CGV d'idealo et les directives d'idealo (ci-après dénommées collectivement « conditions contractuelles »). Cela concerne en particulier les offres de produits et les évaluations de fournisseurs de produits.  

7.2 idealo n'a aucune obligation générale de surveiller les contenus soumis par les fournisseurs et les utilisateurs et stockés en leur nom, ou de rechercher activement des circonstances indiquant une activité illicite ou l'incompatibilité de ces contenus avec les dispositions légales, les droits de tiers ou les conditions contractuelles applicables.  

7.3 idealo se réserve le droit de vérifier volontairement les contenus à son initiative et selon son bon jugement, afin de détecter, identifier et retirer ou rendre inaccessible tout contenu illicite ou incompatible avec les conditions contractuelles applicables, et de prendre toutes les mesures nécessaires conformément aux conditions contractuelles applicables. 

7.4 Les particuliers et les entités ont également la possibilité de notifié à idealo des contenus publiés sur les portails de comparaison qu'elles considèrent comme illicites (ci-après les « notifications »). Cela s'applique en particulier aux offres de produits publiées sur les portails de comparaison par les fournisseurs, ainsi qu'aux évaluations sur fournisseurs fournis par les utilisateurs sur les portails de comparaison. idealo a établi un mécanisme de notification pour permettre aux particuliers et entités de notifier à idealo, au moyen d'un formulaire électronique, les contenus qu'ils considèrent illicites. De plus amples informations sur ce mécanisme peuvent être consultées [ici](https://kundenservice.idealo.com/hc/fr/articles/17019773847698-Notification-des-contenus-illicites).  

7.5 Pour les enquêtes volontaires de sa propre initiative et la vérification des notifications de contenu, idealo utilise diverses procédures et outils pour détecter, vérifier et modérer le contenu. Il peut s'agir d'une vérification manuelle, d'une vérification automatisée ou d'une combinaison des deux. De plus amples informations sur les procédures et les outils utilisés par idealo pour modérer les contenus sont disponibles dans la directive sur la modération des contenus. Elle est accessible [ici](https://kundenservice.idealo.com/hc/fr/articles/17043850193426-Directive-sur-la-mod%C3%A9ration-des-contenus). 

7.6 idealo est en droit de suspendre le traitement des notifications à l'égard de certaines particuliers ou entités pendant une période raisonnable, après avertissement préalable, dans le cas où celles-ci soumettent fréquemment des notifications manifestement infondées. Pour décider sur la suspension et, si applicable, sur sa durée, idealo considère tous les faits et circonstances disponibles. Il s'agit notamment (i) du nombre absolu de notifications manifestement infondées soumises au cours d'une certaine période, (ii) de leur part relative par rapport au nombre total de notifications soumises au cours de cette période, (iii) de la nature des notifications et de la gravité des allégations associées, y compris leur impact sur les autres utilisateurs, et (iv) des intentions du particulier ou de l'entité qui soumet la notification, si elles sont perceptibles. 

**8. Système interne de traitement des réclamations** 

8.1 idealo a établi un système interne de traitement des réclamations, électronique et gratuit, (ci-après « SITR ») pour les réclamations des utilisateurs contre certaines mesures de modération imposées par idealo. 

8.2 L'accès à l'SITR est fourni aux particuliers et aux entités qui ont soumis une notification de contenu présumé illicite au moyen du mécanisme de notification électronique (voir paragraphe 7 des présentes CGU) et qui contestent la décision prise concernant le contenu signalé.  Les fournisseurs et les utilisateurs qui sont affectés par une décision de modération de contenu, notamment au sens de paragraphe 6.6 des présentes CGU, ont également accès à l'SITR.  

8.3 Il est possible de soumettre une réclamation pendant une période de six mois à compter de la décision. De plus amples informations sur la procédure de réclamation sont disponibles dans les règles de procédure pour le système interne de traitement des réclamations d'idealo. Les règles applicables peuvent être consultées [ici](https://kundenservice.idealo.com/hc/fr/articles/17033300378130-Syst%C3%A8me-interne-de-traitement-des-r%C3%A9clamations).  

8.4 idealo se réserve le droit de suspendre, pour une durée raisonnable et après avoir émis un avertissement préalable, le traitement des réclamations soumises par des particuliers et des entités si ceux-ci soumettent fréquemment des réclamations manifestement infondées. idealo considérera tous les faits et circonstances disponibles lorsqu'il décidera sur la suspension et, si applicable, sur sa durée. Il s'agit notamment (i) du nombre absolu de réclamations manifestement infondées soumises au cours d'une certaine période, (ii) de leur part relative par rapport au nombre total de réclamations soumises au cours de cette période, (iii) de la nature des réclamations et de la gravité des allégations associées, y compris leur impact sur d'autres utilisateurs ou des tiers, et (iv) des intentions du réclamant, si elles sont perceptibles. 

**9. Responsabilité, limitation de la responsabilité** 

9.1 idealo s'efforce de présenter toutes les informations fournis sur les portails de comparaison de manière actuelle et sans erreur. Toutefois, en raison de la quantité d'informations à traiter, des erreurs dans la présentation ne peuvent être exclues. Celles-ci peuvent notamment être dues à des informations erronées de la part des partenaires. idealo ne peut donc pas garantir l'exactitude, l'actualité, l'exhaustivité et la qualité des informations fournies par les partenaires.  

9.2 En ce qui concerne la responsabilité d'idealo pour des dommages, quel qu'en soit le motif juridique, les exclusions et limitations de responsabilité suivantes s'appliquent, sans préjudice des autres conditions légales de réclamation :  

a) idealo n'est responsable d'une négligence ordinaire qu'en cas de violation d'une obligation dont il est essentiel qu'elle soit remplie pour la bonne exécution du contrat et sur le respect de laquelle les utilisateurs peuvent régulièrement compter (Kardinalpflicht).  S’idealo est responsable d'une négligence ordinaire, sa responsabilité est limitée aux dommages auxquels il fallait typiquement s'attendre au vu des circonstances connues lors de la conclusion du contrat.  

b) Par ailleurs, la responsabilité d'idealo est exclue pour les dommages de toute nature résultant d'une violation des obligations par négligence légère.  

9.3 Les dispositions ci-dessus s'appliquent également aux demandes de remboursement de dépenses. 

9.4 Les dispositions ci-dessus s'appliquent également aux collaborateurs, aux auxiliaires d'exécution et à d'autres tiers auxquels idealo fait appel pour l'exécution du contrat.  

9.5 Les dispositions ci-dessus ne s'appliquent toutefois pas aux cas de responsabilité légale obligatoire, en particulier 

* selon la loi allemande sur la responsabilité du fait des produits (Produkthaftungsgesetz),  
    
* en cas de prise en charge d'une garantie et 
    
* en cas d'atteinte fautive à la vie, à l'intégrité physique ou à la santé. 
    

Dans ces cas, idealo est également responsable en cas de négligence ordinaires.

### **10. Mentions relatives aux droits de propriété intellectuelle** 

10.1 Les portails de comparaison et les données, résultats de recherche, textes, graphiques et autres informations qu'ils contiennent sont en partie protégés par des droits d'auteur. Il en va de même pour le logiciel et la base de données utilisés pour la fourniture des services. La mise à disposition des portails de comparaison pour une utilisation aux termes des présentes CGU ne constitue pas une renonciation aux droits d'auteur ou une concession de droits d'utilisation relevant du droit d'auteur.  

10.2 Les utilisateurs ne peuvent utiliser les portails de comparaison qu'à des fins individuelles et conformes de comparaison des informations et des offres.  En particulier, les utilisateurs ne peuvent consulter la base de données sous-jacente qu'au moyen de la possibilité d'accès mise à disposition par idealo et n'utiliser les données que pour leur propre usage personnel. La lecture ou l'espionnage de la base de données au moyen d'autres logiciels n'est pas autorisé. En particulier, les utilisateurs ne sont pas autorisés à accéder aux portails de comparaison de manière automatisée. Cela comprend notamment le scraping.   

10.3 Les utilisateurs ne sont autorisés à effectuer des impressions individuelles, des copies ou d'autres reproductions des portails de comparaison et des informations qu'ils contiennent ou de parties de ceux-ci que pour leur propre usage personnel. Toute autre reproduction, transformation, présentation, mise à disposition du public, diffusion, émission ou autre exploitation de contenus non négligeables des portails de comparaison, y compris de leur base de données, nécessite l'accord écrit d'idealo. Cela s'applique également à l'intégration des contenus des portails de comparaison dans un « frame » HTML. 

10.4 Les noms d'entreprises, logos et/ou désignations de produits mentionnés sur les portails de comparaison sont des marques protégées ou des marques déposées de leurs propriétaires respectifs. 

10.5 idealo utilise en partie sur ses portails de comparaison des données qui proviennent des bases de données des projets suivants : 

* Geonames [(](http://www.geonames.org/)http://www.geonames.org) 
    
* Dbpedia [(](http://www.dbpedia.org/)http://www.dbpedia.org) 
    
* OpenStreetMap (https://www.openstreetmap.org)  
    

Geonames et Dbpedia sont sous licence CC-BY-SA 3.0. OpenStreetMap est sous la licence OdbL- et CC-BY-SA 2.0.  L'utilisation de ces données est soumise aux conditions de licence des projets respectifs.

### **11. Langue du contrat, texte du contrat, communication**

11\. La langue du contrat est le français. 

11.2 La communication entre idealo et les utilisateurs se déroule en principe par des moyens de communication électroniques à distance. Cette communication se déroule également en langue française.   

11.3 Les utilisateurs peuvent consulter les CGU actuelles sur les portails de comparaison et les enregistrer au format PDF ou les imprimer. Les CGU ne sont pas enregistrées après la conclusion d'un contrat par idealo.

### **12. Note sur le règlement des litiges**

La Commission européenne met à disposition une [plateforme de règlement des litiges](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage). idealo n'est pas tenu de participer aux procédures de règlement des litiges devant les organes d'arbitrage pour la protection des consommateurs. idealo ne propose pas non plus volontairement aux utilisateurs de participer à ces procédures. 

### **13. Droit applicable** 

Le droit allemand s'applique. Dans la mesure où les utilisateurs sont des consommateurs et ont leur lieu de résidence habituel dans un autre État membre de l'UE que l'Allemagne, les dispositions obligatoires de protection des consommateurs de ce pays ne sont pas affectées. Cela concerne en particulier la conclusion du contrat. 

### **14. Lieu de juridiction**  

Si l'utilisateur est un commerçant, une entité juridique de droit public ou un fonds spécial constitué en tant qu'entité de droit public, le lieu de juridiction pour les litiges résultant de la relation contractuelle entre l'utilisateur et idealo est le siège social d'idealo. idealo est toutefois également autorisé à poursuivre ces utilisateurs devant les tribunaux de leur lieu d'activité. Si les utilisateurs sont des consommateurs et ont leur domicile ou leur lieu de résidence habituel en dehors de l'Allemagne, le tribunal compétent est celui du siège social d'idealo. Dans tous les autres cas, les dispositions légales s'appliquent.

Mise à jour : 17 février 2024