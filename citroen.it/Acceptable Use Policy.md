**INFORMATIVA PRIVACY**

**A partire dal 30/06/2023 abbiamo modificato la gestione dei dati dei clienti.**   

Sai che da gennaio 2021, dalla fusione di PSA Automobiles SA e Fiat Chrysler Automobiles N.V, è nata Stellantis N.V che copre i seguenti marchi automobilistici: Abarth, Alfa Romeo, Citroën, DS Automobiles, Fiat, Fiat Professional, Jeep, Lancia, Opel, Peugeot, Vauxhall, Spoticar, Mopar.  
Con la presente ti informiamo che, a seguito di una riorganizzazione interna del Gruppo Stellantis, volta a centralizzare il trattamento dei dati personali dei Clienti, a partire dal 30/06/2023, tutte le attività di Customer Relationship Management (CRM) delle Società europee del Gruppo saranno gestite dalla società Stellantis Europe S.p.A., con sede legale in C.so G. Agnelli 200, 10135 - Torino, Italia, che agirà in qualità di autonomo Titolare del trattamento.  
  
Tale riorganizzazione interna comporterà il trasferimento alla società Stellantis Europe S.p.A. della gestione dei siti web dei marchi Stellantis sopra indicati, delle App, nonché di tutti i dati personali dei Clienti che hanno aderito alle iniziative commerciali dei suddetti marchi.   
  
**Privacy Policy generale sulla privacy di Stellantis Europe (in vigore dal 30/06/2023)**   
Se stai leggendo questo documento ("**Privacy Policy**"), è perché stai visitando il Nostro Sito Web e/o la nostra Applicazione, o perché hai partecipato a uno dei Nostri Eventi.   
La presente Privacy Policy è redatta ai sensi dell'art. 13 del Regolamento UE 679/2016 (di seguito "**GDPR**") e ti fornisce alcuni esempi di come trattiamo i tuoi Dati Personali, nonché le Definizioni rimandando a spiegazioni più dettagliate (in calce alla presente Privacy Policy) per i termini in maiuscolo qui riportati. Per qualsiasi chiarimento in merito alla presente Privacy Policy o alle modalità di trattamento dei tuoi Dati, invia la tua richiesta a: [dataprotectionofficer@stellantis.com](mailto:dataprotectionofficer@stellantis.com)  
  

  
**1\. Chi siamo**

Stellantis Europe S.p.A. con sede legale in Corso Agnelli 200, 10135 Torino, Italia (di seguito, "Stellantis Europe"; "noi" o "ci") è il Titolare del Trattamento dei tuoi Dati Personali.  
  

  
**2\. Quali Dati raccogliamo e trattiamo**

Raccogliamo Dati dal Nostro Sito Web (inclusa la tua area personale sul Nostro Sito Web) e dall'Applicazione, nonché durante i Nostri Eventi. I Dati raccolti e le relative finalità di trattamento dipendono dalle modalità di utilizzo dei nostri Servizi e dalla gestione delle impostazioni del Browser, del Dispositivo e dell'Applicazione in uso. Le finalità per la raccolta dei tuoi Dati Personali sono indicate alla sezione "Perché raccogliamo e trattiamo i tuoi Dati".

  
**a) Dati forniti dall'utente**  
Quando utilizzi i nostri Servizi, puoi fornirci Dati Personali quali nome, numero/i di telefono/cellulare, indirizzo e-mail, luogo di residenza o dati di terzi, nonché le tue preferenze (ad esempio, su determinati modelli di veicoli o servizi disponibili presso il tuo rivenditore locale). È il caso, ad esempio, di quando richiedi un test drive, di quando cerchi il rivenditore più vicino a te, di quando partecipi a uno dei Nostri Eventi o di quando ci poni domande, fai richieste o interagisci con i nostri Servizi di assistenza (ad esempio, quando ci contatti per chiedere informazioni, per presentare un reclamo o per darci un feedback o nuove idee). In questi casi, puoi chiamarci al nostro numero di assistenza clienti o compilare un modulo online o contattarci via chat, se disponibile.  
Puoi anche scegliere di fornirci informazioni sulla tua posizione se, ad esempio, vuoi cercare un concessionario/rivenditore/officina della Nostra Rete nella zona di tuo interesse (ad esempio Torino) utilizzando Il Nostro Sito Web e la nostra Applicazione.  
Se ci fornisci i dati di terzi, sarai ritenuto responsabile di aver condiviso tali informazioni. Devi essere legalmente autorizzato a condividerle (cioè autorizzato da terzi a condividere le loro informazioni, o la condivisione deve essere necessaria e giustificata da unmotivo legittimo). Devi manlevarci da qualsiasi responsabilità in caso di eventuali reclami, pretese o richieste di risarcimento danni che potrebbero derivare dal trattamento di Dati Personali di terzi in violazione della legge applicabile sulla protezione dei dati.

**b) Dati raccolti dal Browser, dal Dispositivo e dall'Applicazione**

Quando utilizzi il Nostro Sito Web e la nostra Applicazione, raccogliamo informazioni sul Browser, sul Dispositivo e sull'Applicazione che stai utilizzando. Queste informazioni includono il tuo Indirizzo IP, la data, l'ora e l'URL richiesto, gli Identificatori Unici e altre informazioni come il tipo di Browser o Dispositivo. Le informazioni relative al Browser o al Dispositivo possono includere il sistema operativo, la lingua, le impostazioni di rete, l'operatore telefonico o il provider Internet, le applicazioni di terzi installate e gli elenchi di plug-in.

Alcune di queste informazioni vengono raccolte utilizzando Cookie e Altre Tecnologie di Tracciamento presenti nel Browser o nel Dispositivo dell'utente. Questo ci aiuta, ad esempio, a evitare malfunzionamenti durante la fornitura dei Servizi e ci permette di fornire Contenuti che potrebbero essere utili all’Utente. Ulteriori informazioni sui Cookie sono disponibili nella nostra Cookie Policy.

  
**c) Dati desunti dalla tua attività**

Raccogliamo informazioni basate sulle interazioni dell'utente con i nostri Servizi al fine di migliorarli (ad esempio, se vediamo che l'utente è interessato a un particolare modello di Veicolo, ai Nostri Eventi o ai check-up periodici dei veicoli in una determinata area geografica, ci concentreremo sulla fornitura di tali contenuti) e di comprendere i Contenuti che potrebbero essere utili all'Utente. In altri casi, se ci contatti via e-mail, posta, telefono o altro in merito ai Veicoli o richiedi altre informazioni, raccogliamo e conserviamo un registro dei tuoi Dati di contatto, delle comunicazioni e delle nostre risposte. Se ci contatti per telefono, ti verranno fornite ulteriori informazioni durante la telefonata.

  
**d) Informazioni sulla tua posizione**

Raccogliamo informazioni sulla tua posizione per permetterti di visualizzare il concessionario/rivenditore/officina della Nostra Rete più vicino a te, come parte dei nostri Servizi, e per fornirti Contenuti che potrebbero essere utili all’Utente. La tua posizione puoi essere determinata attraverso:  
  
• l'inserimento manuale di un indirizzo, una città o un codice postale;  
• i Sensori del Dispositivo;  
• l'Indirizzo IP dell'utente, raccolto grazie all'autorizzazione del Browser o del Dispositivo.  
  
La posizione dell'utente viene determinata in modo più o meno accurato e coerente a seconda che venga raccolta dal Browser o dal Dispositivo e dalle impostazioni sulla privacy impostate dall'utente. Ci impegniamo al massimo per garantire che le informazioni sulla tua posizione non vengano utilizzate per raccogliere i tuoi Dati Sensibili. Puoi limitare la raccolta della tua posizione modificando le impostazioni del tuo Browser o Dispositivo, come indicato nella successiva sezione "Come controllare i tuoi dati e gestire le tue scelte".  

  
**3\. Fonte dei Dati Personali  
**  

**a) Dati raccolti dai siti web e dalle app dei nostri Partner**

Raccogliamo inoltre informazioni sull'utente dai siti web e dalle app dei nostri Partner. I nostri Partner possono comunicarci i tuoi Dati Personali solo dopo averci assicurato contrattualmente di aver ottenuto il tuo consenso o di avere un'altra base giuridica che legittimi la loro comunicazione/condivisione di tali Dati con noi (ad esempio, se chiedi a uno dei nostri Partner di prenotare un test drive, quando acquisti e quando richiedi di ricevere comunicazioni commerciali). Questa pratica sarà indicata di seguito come "Raccolta Indiretta". A questo proposito, desideriamo sottolineare che facciamo ogni sforzo possibile per verificare la conformità dei Dati che riceviamo prima che vengano utilizzati. Inoltre, chiediamo loro di non fornirci i tuoi Dati Sensibili.  
  

**b) Dati raccolti da fonti pubbliche o pubblicamente accessibili**

Possiamo raccogliere o arricchire i tuoi Dati Personali con informazioni ottenute da fonti pubbliche accessibili nei limiti della legge a noi applicabile. Tali fonti possono includere registri pubblici, giornali online, liste o elenchi pubblici. Si prega di notare che viene sempre effettuata una verifica preliminare sulla possibilità di utilizzare tali informazioni, secondo le migliori prassi stabilite dalla rispettiva autorità competente a cui siamo soggetti (attualmente l'Autorità Garante italiana - Autorità Garante per la protezione dei Dati Personali).  
  

**4\. Perché raccogliamo e trattiamo i tuoi Dati e base giuridica**

I tuoi Dati servono per le seguenti finalità:  
  

**a) Facilitare la raccolta e la correzione dei tuoi Dati**

Nella misura consentita dalla legge applicabile in materia di protezione dei dati, utilizziamo i tuoi Dati, in particolare quelli da te forniti alla Nostra Rete, per aggiornare le informazioni che abbiamo su di te come proprietario di uno dei nostri Veicoli o come persona interessata ai marchi Stellantis. Questo scopo include anche la condivisione con la Nostra Rete e con i Costruttori di Autoveicoli per assicurarci che i tuoi Dati Personali siano corretti e aggiornati.

Questo trattamento si basa sul legittimo interesse di Stellantis Europe, della Nostra Rete e dei Costruttori di Autoveicoli a mantenere aggiornata la qualità dei Dati Personali relativi a proprietari e clienti.  
  

**b) Fornire i nostri Servizi e il relativo supporto**

Utilizziamo i tuoi Dati per offrirti i nostri Servizi, compresa la prenotazione di test drive sul Nostro Sito Web e sull'Applicazione; per organizzare i Nostri Eventi a cui partecipi; per rispondere alle tue richieste/suggerimenti/segnalazioni.

Questo trattamento si basa sull'esecuzione di un obbligo contrattuale o di misure precontrattuali adottate su tua richiesta.  

  
**c) Invio di comunicazioni promozionali**

Possiamo utilizzare i tuoi Dati di contatto (e-mail, telefono, SMS, indirizzo postale e/o qualsiasi altro mezzo disponibile) per l'invio di comunicazioni promozionali o per ricerche di mercato e sondaggi tra i consumatori che includono contenuti di marketing. Queste comunicazioni riguardano tutti i marchi attuali e futuri di Stellantis Europe (ad esempio, Fiat, Fiat Professional, Alfa Romeo, Lancia, Abarth, Peugeot, Citroên, DS automobiles, Opel, Jeep, Mopar, Vauxhall) nonché i marchi delle società del gruppo Stellantis, quali ad esempio Stellantis Financial Services S.A., Stellantis Financial Services Italia S.p.A., Stellantis Renting S.p.A., Stellantis Insurance Limited, Stellantis Life Insurance Limited, Stellantis Insurance Europe Limited e Stellantis Life Insurance Europe Limited. In alcuni casi, le comunicazioni possono includere promozioni di prodotti o servizi di Partner selezionati. Quando inviamo questo tipo di comunicazioni, possiamo agire in qualità di Contitolare del Trattamento con il Partner in questione. Prima dell'invio di qualsiasi comunicazione all'utente, saranno stipulati accordi e informazioni specifiche con tali Partner. A questo proposito, precisiamo che nessuna comunicazione ti sarà inviata senza il tuo preventivo consenso, che potrai fornire attraverso apposite _caselle di spunta_.  
  

**d) Rilevare le anomalie e migliorare i nostri Servizi**

Utilizziamo i Dati forniti dall'utente, i Dati raccolti dal Browser, dal Dispositivo e dall'Applicazione, i Dati desunti dalle attività dell'utente e le Informazioni Aggregate al fine di evitare anomalie nei nostri Servizi. Ad esempio, possiamo rilevare anomalie quando l'utente apre una sezione del Nostro Sito Web e della nostra Applicazione, accede a un link o quando è presente un bug nel nostro sistema. Il trattamento si basa sulla nostra necessità di garantire i migliori Servizi e sul nostro legittimo interesse a evitare eventuali interruzioni del servizio.  
  

**e) Escludere l'utente da comunicazioni promozionali non pertinenti.**

Trattiamo i tuoi Dati per escluderti dalle comunicazioni promozionali, nel caso in cui tali comunicazioni non siano coerenti con il tuo profilo (ad esempio, se risiedi in Italia, non condivideremo promozioni relative alla Francia, ecc.)

Questo trattamento si basa sul nostro legittimo interesse a ridurre o utilizzare efficacemente il nostro budget di marketing e sul tuo legittimo interesse a non ricevere comunicazioni non pertinenti.  
  

**f) Analizzare le preferenze e i comportamenti dell'utente al fine di personalizzare i nostri Servizi e le nostre comunicazioni, compresi i Contenuti che potrebbero essere utili all'Utente**

Utilizziamo i tuoi Dati, in particolare i Dati desunti dalle tue attività, i Dati del Veicolo, le Informazioni sulla tua posizione (se condivise con noi) e i Dati raccolti dal Browser, dal Dispositivo e dall'Applicazione, per migliorare i nostri Servizi (ad esempio, il Nostro Sito Web e l'Applicazione, i Nostri Eventi, le comunicazioni promozionali) e per mostrarti Contenuti che potrebbero essere utili all’Utente, anche su piattaforme di social media o attraverso piattaforme di Pubblicità Programmatica, solo nella misura in cui ci hai autorizzato a caricarli su queste piattaforme.

I Servizi e/o le comunicazioni e/o i Contenuti che possono essere utili all'Utente si basano sul tuo comportamento, i tuoi interessi, le tue esigenze, le tue preferenze e il tuo profilo; tali finalità possono essere raggiunte anche sulla base dei Dati Personali raccolti attraverso l'uso di Cookie o di Altre Tecnologie di Tracciamento per analizzare e prevedere le preferenze del cliente e fornirgli offerte su misura.

Contenuti che potrebbero essere utili all’Utente:

  
• non vengono creati utilizzando Dati Sensibili, come quelli che possono derivare dalle informazioni sulla tua posizione;  
• possono essere visibili anche su siti web e applicazioni mobili diversi dai nostri una volta caricati sulle piattaforme di Pubblicità Programmatica solo nella misura in cui ci hai autorizzato a caricarli su tali piattaforme.  
  

Questo trattamento si basa sul tuo consenso preventivo.

Quando ti indirizziamo su piattaforme di social media o attraverso piattaforme di Pubblicità Programmatica, possiamo agire in qualità di Contitolare del Trattamento con il relativo fornitore della piattaforma. Prima dell'invio di qualsiasi comunicazione all'utente, saranno stipulati accordi e informazioni specifiche con il fornitore della piattaforma. Se non desideri Servizi o Contenuti che potrebbero essere utili all’Utente, puoi modificare le tue preferenze, come spiegato nella sezione "Come controllare i tuoi Dati e gestire le tue scelte" di seguito.  

  
**g) Analizzare e migliorare i nostri Servizi e creare nuovi Servizi e funzionalità**

Utilizziamo i tuoi Dati e le Informazioni Aggregate per misurare le prestazioni dei nostri Servizi o per crearne di nuovi. Ciò puoi avvenire, ad esempio, attraverso l'analisi delle tue interazioni con la Nostra Rete, i Nostri Eventi, la nostra newsletter e/o le nostre comunicazioni promozionali (se richieste).

Per quanto possibile, utilizziamo Dati anonimizzati o pseudonimizzati per questi scopi. Solo in casi eccezionali può essere possibile un riferimento personale. In questi casi, si applica quanto segue: ad eccezione del tuo consenso alla personalizzazione dei nostri Servizi, la misurazione dell'efficacia dei nostri Servizi e la creazione di nuovi Servizi si basano sul nostro legittimo interesse a creare e mantenere Servizi realmente utili per i nostri utenti.  
  

**h) Condividere i Dati con i Partner per le loro finalità di marketing**

Condividiamo i tuoi Dati di contatto con Partner terzi selezionati per i loro scopi di marketing autonomi. I Partner ti contatteranno solo con mezzi automatizzati (ad esempio, e-mail, SMS, telefonate con registratore) e saranno obbligati a fornire la loro Privacy Policy. Questo trattamento si basa sul tuo consenso preventivo. L'elenco completo o le categorie di Partner con cui abbiamo condiviso direttamente i tuoi Dati sono disponibili all'indirizzo: .  
  

**i) Adempiere agli obblighi legali e fiscali**

Potremmo utilizzare i tuoi Dati per adempiere agli obblighi di legge e agli ordini a cui siamo soggetti, che costituiscono la base giuridica per il trattamento dei tuoi Dati.

Alcune legislazioni potrebbero richiederci di condividere i tuoi Dati con le autorità pubbliche (ad esempio, campagne di richiamo). Se questa condivisione non fosse richiesta dalla legge del tuo paese, potremmo inviare ugualmente i tuoi Dati, come spiegato più dettagliatamente nella successiva sezione "Protezione dei nostri interessi e dei tuoi interessi".  
  

**j) Invio di comunicazioni aziendali e istituzionali**

Nei limiti consentiti dalla legge sulla protezione dei Dati, condividiamo i tuoi Dati di contatto per inviare sondaggi aziendali e comunicazioni istituzionali riguardanti l’intero Gruppo Stellantis. Si tratta di comunicazioni non promozionali inviate da noi per conto o in sostituzione dei Costruttori di Autoveicoli, sulla base del legittimo interesse a fornire informazioni coerenti all’utente.  
  

**k) Tutela dei nostri interessi e dei tuoi interessi**

Nella misura consentita dalla legge applicabile in materia di protezione dei Dati, potremmo dover utilizzare i tuoi Dati per individuare, prevenire e rispondere a comportamenti fraudolenti e illegali o attività che potrebbero compromettere la sicurezza dei nostri Servizi e del Nostro Sito web e Applicazione. Ciò potrebbe verificarsi quando l'utente utilizza la nostra Applicazione in modi diversi da quelli consentiti, al fine di verificare la cosiddetta Raccolta Indiretta, o in caso di comportamento inappropriato ai Nostri Eventi. Tali finalità comprendono anche verifiche e valutazioni delle nostre operazioni commerciali, dei controlli di sicurezza, dei controlli finanziari, dei registri e del programma di gestione delle informazioni, e in ogni altro modo relativo all'amministrazione delle nostre attività generali, della contabilità, della tenuta dei registri e delle funzioni legali.

Tali finalità si basano sul nostro legittimo interesse a salvaguardare i nostri interessi e a proteggere i nostri clienti, compreso l'utente.  

  
**5\. Come utilizziamo i tuoi Dati (modalità di trattamento)**

I Dati raccolti per le finalità sopra indicate sono trattati sia manualmente che in modo automatizzato, ossia attraverso programmi o algoritmi che analizzano i Dati desunti dalle attività dell'utente, le informazioni sulla tua posizione e i Dati raccolti dal Browser, dal Dispositivo e dall'Applicazione.

I tuoi Dati possono anche essere soggetti a Combinazione e/o Incrocio, nella misura in cui ciò è consentito dalla legge applicabile in materia di protezione dei Dati. Questo ci permette di capire, ad esempio, se un singolo utente utilizza i nostri Servizi con lo stesso Indirizzo IP o gli stessi Identificatori Univoci del Browser e del Dispositivo; o se le comunicazioni promozionali o i Contenuti che potrebbero essere utili all'Utente sono strettamente correlati alle Informazioni sulla tua posizione o ai Dati forniti attraverso le tue attività o ai Dati raccolti dal Browser, dal Dispositivo e dall'Applicazione. La Combinazione e/o Incrocio dei tuoi Dati per le finalità per cui li trattiamo (ad esempio, la personalizzazione dei Servizi) può essere attivata o disattivata come spiegato nella successiva sezione "Come controllare i tuoi Dati e gestire le tue scelte".  
  

**6\. Come possiamo divulgare i tuoi Dati**

Possiamo divulgare i tuoi Dati ai seguenti destinatari e/o categorie di destinatari ("Destinatari"):

**• Persona da noi autorizzate** a svolgere una qualsiasi delle attività relative ai Dati descritte in questo documento: i nostri dipendenti e collaboratori che si sono assunti un obbligo di riservatezza e si attengono a regole specifiche per il trattamento dei tuoi Dati;  
  
**• I nostri Responsabili del trattamento:** soggetti esterni ai quali deleghiamo alcune attività di trattamento. Ad esempio, fornitori di sistemi di sicurezza, consulenti contabili e di altro tipo, fornitori di hosting di dati, banche, assicurazioni, ecc. Abbiamo stipulato accordi con ciascuno dei nostri Responsabili del trattamento per garantire che i tuoi Dati siano trattati con adeguate garanzie e solo in base alle nostre istruzioni;  
  
**• Amministratori di sistema:** i nostri dipendenti o quelli dei Responsabili del trattamento ai quali abbiamo delegato la gestione dei nostri sistemi informatici e che sono quindi in grado di accedere, modificare, sospendere o limitare il trattamento dei tuoi Dati. Questi soggetti sono stati selezionati, adeguatamente formati e le loro attività sono tracciate da sistemi che non possono modificare, come previsto dalle disposizioni della nostra competente Autorità di controllo;  
  
**• La nostra Rete e i Costruttori di Autoveicoli di riferimento:** la Nostra Rete nel caso in cui abbiate richiesto un servizio da loro svolto (ad esempio, la richiesta di un test drive vicino a te) o se hai richiesto la loro assistenza o quella fornita dai Costruttori di Autoveicoli;  
  
**• I nostri Partner selezionati:** quando hai acconsentito alla comunicazione dei tuoi Dati Personali per le loro finalità di marketing e/o profilazione e che agiscono come autonomi Titolari del Trattamento.  
  
**• Autorità di polizia o qualsiasi altra autorità le cui disposizioni siano vincolanti per noi:** questo è il caso in cui dobbiamo ottemperare a un ordine giudiziario o alla legge o difenderci in un procedimento legale.  
  

**7\. Dove si trovano i tuoi Dati**

Siamo un'azienda globale e i nostri servizi sono disponibili in diverse giurisdizioni del mondo. Ciò significa che i tuoi Dati possono essere archiviati, consultati, utilizzati, elaborati e divulgati al di fuori della tua giurisdizione, anche all'interno dell'Unione Europea, degli Stati Uniti d'America o di qualsiasi altro paese in cui si trovano i nostri Responsabili del trattamento e i nostri sub-Responsabili del Trattamento, o dove possono essere ospitati i loro server o le loro infrastrutture di cloud computing. Ci adoperiamo per garantire che il trattamento dei tuoi Dati da parte dei nostri destinatari sia conforme alle leggi applicabili in materia di protezione dei dati, compresa la legislazione dell'UE a cui siamo soggetti. Laddove richiesto dalla legge sulla protezione dei dati dell'UE, i trasferimenti dei tuoi Dati a destinatari al di fuori dell'UE saranno soggetti a garanzie adeguate (come le clausole contrattuali standard dell'UE per i trasferimenti di dati tra paesi dell'UE e paesi terzi), e/o ad altre basi legali in conformità alla legislazione dell'UE. Per ulteriori informazioni sulle garanzie da noi implementate per proteggere i Dati trasferiti a paesi terzi al di fuori dell'UE, puoi scriverci a: [dataprotectionofficer@stellantis.com](mailto:dataprotectionofficer@stellantis.com)

**8\. Per quanto tempo conserviamo i tuoi Dati**

I Dati trattati per le finalità sopra indicate saranno conservati per il periodo ritenuto strettamente necessario all'adempimento di tali finalità. Tuttavia, i Dati potrebbero essere conservati per un periodo più lungo in caso di potenziali e/o effettivi reclami e conseguenti responsabilità e/o in caso di altri requisiti legali obbligatori di conservazione e/o obblighi di conservazione.

• I Dati dei clienti trattati per finalità di marketing e profilazione saranno conservati dal Titolare del trattamento dal momento in cui il cliente fornisce il consenso fino al momento in cui il cliente ritira il consenso. Una volta revocato il consenso, i Dati non saranno più trattati per finalità di marketing e profilazione, anche se potrebbero essere conservati dal Titolare del trattamento per gestire potenziali reclami e/o azioni legali. La conservazione dei Dati in caso di marketing e profilazione è conforme alla legge locale e alle decisioni dell'Autorità per la protezione dei dati.  

• I Dati dei clienti trattati per adempiere agli obblighi di legge saranno conservati per il periodo previsto dalle leggi e dai regolamenti.  

• I Dati del Cliente per il miglioramento del prodotto e dei Servizi potrebbero essere conservati per il periodo ritenuto strettamente necessario per adempiere a tali finalità e non oltre i tre anni.  
  

Una volta scaduto il relativo periodo di conservazione, i tuoi Dati saranno cancellati in base alla nostra politica di conservazione. Puoi richiederci ulteriori informazioni sui nostri criteri e sulla nostra politica di conservazione scrivendoci qui: [dataprotectionofficer@stellantis.com](mailto:dataprotectionofficer@stellantis.com)

  
**9\. Come controllare i tuoi Dati e gestire le tue scelte**

In qualsiasi momento, è possibile chiedere di:

**• Accedere ai tuoi Dati (diritto di accesso):** in base all'uso che fai dei nostri Servizi, ti forniremo i tuoi Dati, come il tuo nome, l'età, l'Indirizzo IP, gli Identificatori Unici, le e-mail e le preferenze espresse, insieme alla Privacy Policy che hai ricevuto quando li hai forniti e alla fonte dei Dati (se, ad esempio, ci sono stati forniti da uno dei nostri Partner);  
  
**• Esercitare il diritto alla portabilità dei tuoi Dati Personali (diritto alla portabilità dei Dati):** in base al tuo utilizzo dei nostri Servizi, ti forniremo un file interoperabile contenente i Dati che ti riguardano.  
**• Correggere i tuoi Dati (diritto di rettifica):** ad esempio, puoi chiederci di modificare il tuo indirizzo e-mail o il tuo numero di telefono se non sono corretti;  
**• Limitare il trattamento dei tuoi Dati (diritto alla limitazione del trattamento):** ad esempio, quando ritieni che il trattamento dei tuoi Dati sia illegale o che il trattamento basato sul nostro legittimo interesse non sia appropriato;  
**• Cancellare i tuoi Dati (diritto alla cancellazione):** ad esempio, quando non vuoi utilizzare i nostri Servizi e non vuoi che i tuoi Dati vengano conservati;  
**• Opporti alle attività di trattamento (diritto di opposizione);  
• Revocare i tuoi consensi (diritto di revoca del consenso).**  

Puoi esercitare i diritti di cui sopra o esprimere qualsiasi preoccupazione o reclamo in merito all'utilizzo dei tuoi Dati da parte nostra direttamente all'indirizzo: [https://privacyportal.stellantis.com](https://privacyportal.stellantis.com/).  
  

In qualsiasi momento, puoi anche:  
• contattare il nostro Responsabile della Protezione dei dati (DPO), qui: [dataprotectionofficer@stellantis.com](mailto:dataprotectionofficer@stellantis.com);  
• contattare l'Autorità di Garante, qui è possibile trovare l'elenco di tutte le Autorità di Garanti per paese: [https://edpb.europa.eu/about-edpb/board/members\_en](https://edpb.europa.eu/about-edpb/board/members_en)  

  
**10\. Come proteggiamo i tuoi Dati**

Adottiamo ragionevoli precauzioni dal punto di vista fisico, tecnologico e organizzativo per prevenire la perdita, l’uso improprio o la modifica dei Dati sotto il nostro controllo. Ad esempio:

• Ci assicuriamo che i tuoi Dati siano accessibili e utilizzati solo da, trasferiti o divulgati ai Destinatari che devono avere accesso a tali Dati.  
• Limitiamo inoltre la quantità di Dati accessibili, trasferiti o divulgati ai Destinatari solo a quanto necessario per adempiere alle finalità o ai compiti specifici svolti dal Destinatario.  
• I computer e i server in cui sono archiviati i tuoi Dati sono tenuti in un ambiente sicuro, sono controllati da password con accesso limitato e hanno installato firewall e software antivirus standard del settore.  
• Anche le copie cartacee dei documenti contenenti i tuoi Dati (se presenti) sono conservate in un ambiente sicuro.  
• Distruggiamo le copie cartacee dei documenti contenenti i tuoi Dati che non sono più necessari.  
• Quando distruggiamo i Dati registrati e archiviati sotto forma di file elettronici che non sono più necessari, ci assicuriamo che un metodo tecnico (ad esempio, un formato di basso livello) garantisca che i record non possano essere riprodotti.  
• I computer portatili, le chiavi USB, i telefoni cellulari e altri dispositivi elettronici senza fili utilizzati dai nostri dipendenti che hanno accesso ai tuoi Dati sono protetti. Invitiamo i dipendenti a non memorizzare i tuoi Dati su tali dispositivi, a meno che non sia ragionevolmente necessario per svolgere un compito specifico come indicato nella presente Privacy Policy.  
• Formiamo i nostri dipendenti al rispetto della presente Privacy Policy e conduciamo attività di monitoraggio per garantire la continua conformità e per determinare l'efficacia delle nostre pratiche di gestione della privacy.  
• Qualsiasi Responsabile del Trattamento da noi utilizzato è tenuto per contratto a conservare e proteggere i tuoi Dati utilizzando misure sostanzialmente simili a quelle indicate nella presente Privacy Policy o richieste dalla legge applicabile in materia di protezione dei dati.  
• Nei casi previsti dalla normativa applicabile, qualora si verifichi una violazione della sicurezza che comporti la distruzione accidentale o illecita, la perdita, l'alterazione, la divulgazione non autorizzata o l'accesso ai Dati trasmessi, conservati o altrimenti trattati, sarà data comunicazione all'utente e all'autorità competente per la protezione dei Dati, come richiesto (ad esempio, a meno che i Dati non siano incomprensibili per qualsiasi persona o sia improbabile che la violazione comporti un rischio per i diritti e le libertà dell'utente e di altri).  
  

**11\. Cosa non copre la presente Privacy Policy**

La presente Privacy Policy illustra e copre il trattamento che effettuiamo in qualità di Titolare del Trattamento all'interno del nostro sito web e della nostra Applicazione.

La presente Privacy Policy non copre i trattamenti effettuati da soggetti diversi da noi.

In questi casi, non siamo responsabili di alcun trattamento dei tuoi Dati che non sia coperto dalla presente Privacy Policy.  
  

**12\. Utilizzo dei Dati per altri scopi**

Qualora dovessimo trattare i tuoi Dati in modo diverso o per finalità diverse da quelle indicate nel presente documento, riceverai una specifica comunicazione prima dell'inizio di tale trattamento.  
  

**13\. Modifiche alla Privacy Policy**

Ci riserviamo il diritto di adattare e/o modificare la presente Privacy Policy in qualsiasi momento. Ti informeremo di ogni adattamento/modifica rilevante.  
  

**14\. Licenza**

Le icone illustrate in questa Privacy Policy sono "Icone della protezione dei dati" dell'Università di Maastricht, European Centre on Privacy and Cybersecurity (ECPC) CC BY 4.0.  
  

**15\. Definizioni**

**Altre Tecnologie di Tracciamento:** pixel tag (tracciatori utilizzati con i Cookie e incorporati nelle immagini delle pagine web o dell'Applicazione per tracciare determinate attività, come la visualizzazione di Contenuti che potrebbero essere utili all'Utente o per verificare se un'e-mail è stata letta) o identificatori univoci incorporati nei link alle comunicazioni commerciali che ci inviano informazioni quando vengono cliccati.

**Applicazione:** indica questa Applicazione, se applicabile.  

**Browser:** si riferisce ai programmi utilizzati per accedere a Internet (ad es. Safari, Chrome, Firefox, ecc.).

**Combinazione e/o Incrocio:** si tratta dell'insieme di operazioni completamente automatizzate e non automatizzate che combiniamo con le Informazioni sulla tua posizione, i Dati desunti dalla tua attività, i Dati raccolti dal Browser, dal Dispositivo e dall'Applicazione, i Dati da te forniti e quelli raccolti dai Siti Web e dalle Applicazioni dei nostri Partner, utilizzati per fornire i Servizi, analizzare e migliorare i nostri Servizi e creare nuovi servizi e funzionalità, nonché per offrire Contenuti che potrebbero essere utili all’Utente. Possiamo anche combinare e/o incrociare informazioni provenienti da fonti diverse, come le informazioni raccolte dal Nostro Sito Web e dall'Applicazione, dai siti web e dalle applicazioni dei nostri Partner e/o i Dati raccolti da fonti pubbliche o pubblicamente accessibili.

**Contenuti che potrebbero essere utili all'Utente:** ad esempio, se l'utente cerca il modello "Citroên", potremmo visualizzare altri contenuti relativi a questo modello sul nostro Sito Web e Applicazione o attraverso la Pubblicità Programmatica. La personalizzazione dei contenuti può avvenire attraverso la Combinazione e/o Incrocio dei Dati.  

**Cookie:** si tratta di un piccolo testo inviato al tuo Browser dai Nostri Siti o dai nostri Partner o rivenditori. Permette al sito di memorizzare informazioni come il fatto di aver visitato il sito, la tua lingua e altre informazioni. I Cookie vengono utilizzati per diversi scopi, ad esempio per registrare le preferenze dell'utente in merito all'utilizzo dei Cookie (Cookie tecnici), per analizzare e migliorare i nostri Servizi e per creare nuovi Servizi e funzionalità o per personalizzare i nostri Servizi, compresi i Contenuti che possono essere utili all'Utente. Le informazioni trasmesse dai Cookie sono soggette a Combinazione e/o Incrocio con una delle Altre Tecnologie di Tracciamento, ove applicabile.  

**Costruttori di Autoveicoli:** singolarmente o collettivamente si riferisce alle seguenti entità che agiscono come produttori di veicoli: Stellantis Europe S.p.A., Corso Agnelli 200, 10135 - Torino, Italia; PSA Automobiles S.A. (Stellantis Auto S.A.S.), 2-10 Boulevard de l'Europe, F-78300 Poissy, Francia; Opel Automobile GmbH, Bahnhofsplatz, D-65423 Rüsselsheim am Main, Germania.  

**Dati del Veicolo:** indica tutti i dati tecnici, diagnostici e reali che è possibile raccogliere tramite il Dispositivo del Veicolo installato sul Veicolo (ad esempio, posizione, velocità e distanze, tempo di funzionamento del motore e tempo di spegnimento; se il cavo della batteria è tagliato, diagnostica della batteria, movimenti con la chiave estratta, presunta collisione, nonché dati diagnostici quali, a titolo esemplificativo ma non esaustivo, livelli dell'olio e del carburante, pressione degli pneumatici e stato del motore).

**Dati Personali:** qualsiasi informazione relativa a una persona fisica identificata o identificabile, direttamente o indirettamente, nonché qualsiasi informazione collegata o ragionevolmente collegabile a un particolare individuo o nucleo familiare. Ad esempio, un indirizzo e-mail (se si riferisce a uno o più aspetti di un individuo), gli Indirizzi IP e gli Identificatori Unici sono considerati Dati Personali. Per comodità, indicheremo collettivamente tutti i Dati Personali citati anche come "Dati".

**Dati Sensibili:** si intendono i Dati Personali che rivelano l'origine razziale o etnica, le opinioni politiche, le convinzioni religiose o filosofiche, l'appartenenza sindacale e il trattamento di dati genetici, dati biometrici intesi a identificare in modo univoco una persona fisica, dati relativi alla salute o dati relativi alla vita sessuale o all'orientamento sessuale di una persona fisica.  

**Dispositivo:** si riferisce al Dispositivo elettronico (ad es. iPhone) attraverso il quale visitate Il Nostro Sito Web e la nostra Applicazione e/o i Siti Web e le applicazioni dei nostri Partner.

**Identificatori Unici:** informazioni che possono identificare in modo univoco l'utente attraverso il Browser, il Dispositivo e/o l'Applicazione. Nel Browser, l'Indirizzo IP e i Cookie sono considerati Identificatori Unici. Sul Dispositivo, gli identificatori pubblicitari forniti dai produttori, come l'IDFA di Apple e l'AAIG di Android, che utilizziamo per analizzare e migliorare i nostri Servizi e per creare nuovi Servizi e funzionalità, compresi i Contenuti che possono essere utili all'Utente, sono considerati Identificatori Unici. Si prega di notare che per questi scopi e in linea con i pareri delle autorità di vigilanza europee, non utilizziamo altri identificatori univoci come gli indirizzi MAC e gli IMEI in quanto non sono reimpostabili dall'utente. Per l'Applicazione, invece, gli Identificatori Unici sono considerati il codice che identifica l'Applicazione installata.

**Indirizzo IP:** è un numero univoco utilizzato dal Browser, dal Dispositivo e dall'Applicazione per connettersi a Internet. Il fornitore di servizi Internet fornisce questo numero che consente di identificare il fornitore e/o l'area approssimativa in cui si trova l'utente. Senza questi Dati, l'utente non puoi connettersi a Internet e utilizzare i nostri Servizi o i Contenuti che potrebbero essergli utili.  

**Informazioni Aggregate:** si riferiscono a informazioni statistiche sull'utente che non contengono i tuoi Dati Personali. Utilizziamo queste informazioni per analizzare e migliorare i Nostri Servizi e creare nuovi Servizi e funzionalità e per creare rapporti statistici per i nostri Partner e la Nostra Rete. Ad esempio, possiamo raccogliere informazioni sulla tua posizione o sui contenuti che ti possono essere utili e che hai visualizzato. Desideriamo sottolineare che non condividiamo i tuoi Dati in questi rapporti.

**Nostri Eventi:** sono eventi/showroom organizzati da Stellantis o in collaborazione con altri marchi con cui Stellantis Europe ha siglato accordi di partnership.

**Nostro Sito Web:** comprende il presente Sito Web e le pagine dei nostri social network in cui è presente la presente Privacy Policy.  

**Nostra Rete**: si tratta di rivenditori e/o concessionari e/o officine con i quali Stellantis Europe e i Produttori di Autoveicoli hanno sottoscritto accordi commerciali per la vendita dei Veicoli e/o per la fornitura di servizi/prodotti di assistenza. Partner: indica entità terze che possono comunicarci i tuoi Dati Personali solo dopo averci assicurato contrattualmente di aver ottenuto il tuo consenso o di avere un'altra base giuridica che legittima la loro comunicazione/condivisione di tali Dati con noi (ad esempio, se chiedi a uno dei nostri Partner di prenotare un test drive, quando acquisti e quando richiedi di ricevere comunicazioni commerciali). Questa definizione include anche i Partner selezionati con cui possiamo condividere i tuoi Dati. I Partner possono appartenere ai seguenti settori merceologici: attività manifatturiere, commercio all'ingrosso e al dettaglio, servizi finanziari, bancari, di trasporto e magazzinaggio, servizi di informazione e comunicazione, attività professionali, scientifiche e tecniche, agenzie di viaggio, servizi di supporto alle imprese, attività artistiche, sportive, di intrattenimento e divertimento, attività di organizzazioni associative, servizi di centri di benessere fisico, fornitori di energia elettrica e gas, società di noleggio, di mobilità elettrica e di assicurazione.  

**Pubblicità Programmatica:** si tratta di piattaforme che condividono le informazioni raccolte sull'utente, come l'Indirizzo IP e i Dati raccolti da Cookie e altre Tecnologie di Tracciamento, con entità che hanno interesse a mostrargli Contenuti che potrebbero essere utili all’Utente. Nel nostro caso, se visualizzate il modello "Citroên" sul nostro Sito Web e sulla nostra Applicazione, chiederemo ai partecipanti alla Pubblicità Programmatica di concederci uno spazio pubblicitario su uno dei Siti Web che visitate al fine di mostrare Contenuti che potrebbero essere utili all’Utente. A questo proposito, desideriamo ribadire che la comunicazione dei tuoi Dati ai partecipanti alla Pubblicità Programmatica si basa sul tuo previo e specifico consenso fornito sul banner al momento della prima visita al Nostro Sito Web e all'Applicazione.

**Raccolta Indiretta:** è uno dei Servizi che forniamo sui siti web e sulle applicazioni dei nostri Partner. In questi casi, è il Partner che ci assicura di aver ricevuto il tuo consenso o di avere un'altra base giuridica che legittima la comunicazione/condivisione dei tuoi Dati Personali. A questo proposito, precisiamo che, prima di essere utilizzati, verifichiamo le modalità con cui i Partner raccolgono e ci trasferiscono i Dati al fine di rispettare le tue preferenze.  

**Responsabile del Trattamento:** si riferisce a un'entità da noi incaricata di trattare i tuoi Dati Personali esclusivamente per conto di Stellantis Europe S.p.A. e in base alle tue istruzioni scritte.

**Sensori del Dispositivo:** a seconda del Dispositivo, si tratta di sensori quali accelerometri, giroscopi, Bluetooth, Wi-fi e GPS che, in un modo o nell'altro, condividono le informazioni raccolte attraverso il Dispositivo e quindi l'Applicazione. Se abilitati dalle impostazioni del Dispositivo, ci permettono di ottenere informazioni sulla tua posizione.  

**Servizi:** collettivamente, si intendono tutti i Servizi disponibili sul Nostro Sito Web e sull'Applicazione, quali "configura e ordina", "trova Rivenditori", "acquista o noleggia", prenotazioni di test drive, la newsletter istituzionale, il servizio clienti e I Nostri Eventi. Titolare del Trattamento: si riferisce alla persona giuridica, all'autorità pubblica, al servizio o ad altra entità che, individualmente o congiuntamente, determina le finalità e i mezzi per il trattamento dei tuoi Dati Personali. Questa definizione si riferisce tipicamente a Stellantis Europe S.p.A. In altri casi, è preceduta dalla parola "Indipendente" (ad esempio, "Titolare indipendente del trattamento") per indicare che i tuoi Dati Personali sono trattati da un soggetto diverso da Stellantis Europe S.p.A.  

**Veicolo:** si riferisce a un Veicolo di uno dei marchi del gruppo Stellantis.