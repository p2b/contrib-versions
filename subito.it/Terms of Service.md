**1\. Contenuto del Servizio**
------------------------------

Subito.it S.r.l. società di servizi informatici e telematici con sede legale in Via B. Crespi 19 (MI) Italia, mette a disposizione degli utenti che intendano avvalersene un servizio (da ora in poi il "Servizio Base" e/o "Servizio") web based che consente di pubblicare e consultare annunci e inserzioni di soggetti privati o professionali che intendano alienare o acquistare beni o prestare e ricevere servizi. Il Servizio consente altresì agli utenti inserzionisti ed agli utenti interessati a quanto pubblicato di entrare in contatto tra di loro.

**2\. Titolarità della piattaforma**
------------------------------------

Subito.it S.r.l., è l'unica titolare della piattaforma web per il tramite della quale viene gestito il Servizio nonchè di tutti i relativi diritti inerenti e conseguenti allo sfruttamento della piattaforma medesima.

**3\. Applicabilità delle condizioni**
--------------------------------------

Le presenti Condizioni Generali del Servizio si applicano sia agli utenti che utilizzino il Servizio in consultazione degli annunci pubblicati sia agli utenti inserzionisti privati o professionali (d‘ora in poi collettivamente “utente/i”).

**4\. Termini per l'uso del Servizio**
--------------------------------------

L’utilizzo del Servizio è consentito solo ad utenti maggiorenni secondo la legge italiana (maggiori di anni 18).

L'utilizzo del Servizio Base consente la libera consultazione degli annunci e la pubblicazione di inserzioni e la creazione di utenze finalizzate all'utilizzo del servizio medesimo. Talune funzionalità specifiche del Servizio, l'inserzione in specifiche categorie merceologiche e le inserzioni ulteriori rispetto alle soglie fissate per talune categorie potranno essere messe a disposizione solamente a pagamento così come da specifiche condizioni di contratto relative ai servizi medesimi.

Le relazioni intrattenute tra gli utenti del Servizio, incluso l'acquisto, lo scambio di informazioni, anche per il tramite del form di risposta all’annuncio, la consegna o il pagamento di beni o servizi, avvengono esclusivamente tra utenti senza che Subito.it S.r.l. sia parte della relazione. L'utenza si impegna, altresì, a non fare utilizzo improprio dei contatti presenti a qualunque titolo sulla piattaforma di Subito.it. A titolo esemplificativo ma non esaustivo è vietato l'invio di pubblicità, materiale promozionale, o qualsiasi altra forma di sollecitazione non autorizzata o non richiesta tramite e-mail o con qualsiasi altro metodo di contatto.

Gli utenti manterranno la titolarità di tutti diritti di proprietà relativi a video, fotografie, fermo immagini, audio o altro materiale (di seguito, le “Riproduzioni”) che Subito.it ha consentito agli stessi di pubblicare sul proprio sito. Fermo restando quanto precede, l’utente concede a Subito.it una licenza non esclusiva, gratuita, perpetua, trasferibile, irrevocabile e senza limiti di territorio, per utilizzare (anche in combinazione con altri materiali audio e/o video e/o fotografici) e, dunque, fissare, all’interno del sito e/o su ogni altro mezzo (sia esso digitale, elettronico o cartaceo), riprodurre, modificare, adattare, tradurre, distribuire, pubblicare, visualizzare, riprodurre in pubblico le Riproduzioni dell’utente, impegnandosi peraltro a non opporsi alla pubblicazione, utilizzo, modifica, cancellazione e sfruttamento delle Riproduzioni da parte di Subito.it.

Con l’utilizzo del Servizio, gli utenti si assumono l’esclusiva responsabilità circa il diritto di utilizzo delle Riproduzioni pubblicate dagli stessi sul sito, manlevando e tenendo indenne Subito.it da qualsiasi pretesa e/o richiesta di risarcimento formulata da soggetti terzi a fronte della violazione dei diritti di quest’ultimi.

Al momento della registrazione sul sito da parte dell’utente, quest’ultimo sarà tenuto a fornire i propri dati anagrafici reali senza ricorrere all'utilizzo di indirizzi email temporanei o alias (ovvero indirizzi associati al proprio indirizzo email dal quale però non è possibile inviare email ma solo riceverle). Resta espressamente inteso che, in caso di mancata osservanza delle disposizioni della presente clausola da parte dell’utente, Subito.it sarà considerata liberata dell’onere di fornire taluni servizi di assistenza allo stesso, essendo questa impossibilitata alla verifica della corrispondenza utente-email.

**5\. Responsabilità dell'utente**
----------------------------------

L'utente è totalmente ed esclusivamente responsabile dell'uso del Servizio (da intendersi espressamente con riguardo alle funzioni di pubblicazione, di consultazione, di gestione delle inserzioni e di contatto tra utenti) ed è pertanto l'unico garante e responsabile dei beni e dei servizi offerti per il tramite del Servizio nonchè della correttezza, completezza e liceità delle inserzioni e del proprio comportamento nell'ambito del contatto tra utenti.

L'utente garantisce la disponibilità e/o la titolarità del bene/servizio oggetto delle inserzioni medesime. L'utente garantisce altresì che i propri annunci non violano alcun diritto d'autore né diritto di proprietà industriale né altro diritto di terzi. In caso di contestazione da parte di terzi riguardo a qualsiasi annuncio o condotta ad esso legata, l'utente se ne assume la piena responsabilità e si impegna a tenere manlevata e indenne Subito.it S.r.l. da qualsiasi danno, perdita o spesa.

L'utente si impegna ad utilizzare il form di risposta all’annuncio al solo scopo di prendere contatto e scambiare informazioni con gli altri utenti relativamente agli annunci, utilizzando un linguaggio consono, nel rispetto della legge, dell’etica e della netiquette. In ogni caso, l'Utente si impegna a rispettare con qualsiasi interlocutore le norme di buona educazione e l'utilizzo di un linguaggio consono, sia _online_ sia _offline_. Subito.it S.r.l. si riserva in ogni momento di adire le competenti autorità, qualora dovesse ravvisare, nelle comunicazioni nei confronti del proprio personale, contenuti illeciti, ingiuriosi, minacce o calunnie.

L'utente, inoltre, si assume ogni responsabilità per eventuali danni che possano derivare al suo sistema informatico dall'uso del Servizio o a quello di terzi. Resta infine inteso che ogni eventuale utilizzo di robot, spider, scraper e/o ulteriori  strumenti automatici per accedere al sito e/o per estrapolare i relativi dati, contenuti, informazioni è espressamente vietato.

**6\. Limitazione di responsabilità**
-------------------------------------

Subito.it S.r.l. non presta alcuna garanzia circa il contenuto, la completezza e la correttezza delle inserzioni pubblicate nè con riguardo ai dati pubblicati, né relativamente alle informazioni successivamente fornite dall’utente, nè con riferimento al numero o alla qualità dei risultati ottenuti tramite il Servizio. . In ogni caso Subito.it si riserva, in qualsiasi momento, il diritto di valutare, approvare, eliminare o impedire l'inserzione ovvero il diritto di inibire la consultazione o il contatto per il tramite del form di risposta all’annuncio nel caso in cui l'uso del Servizio da parte del cliente si riferisca a particolari sezioni merceologiche o possa considerarsi lesivo di diritti o delle prerogative di Subito.it o di terzi. In tali ipotesi, in caso di utente commerciale, ai sensi dell’art. 4, 1° comma del Regolamento (UE) n. 1150/2019, verrà effettuata un’apposita comunicazione in merito alla sospensione con cui saranno indicate le motivazione della sospensione del servizio, salvo in caso di esclusioni espressamente previste dal Regolamento (UE) n. 1150/2019 e qui richiamate [https://www.subito.it/aziende/procedure-aziende](https://info.subito.it/policies/procedure-aziende.htm). Eventuali reclami contro il provvedimento di sospensione potranno essere presentati tramite il meccanismo interno di risoluzione delle controversie secondo le modalità ivi indicate reperibili sul Sito. Resta inteso che anche in caso di valutazione ed approvazione delle inserzioni (siano esse automatiche o manuali) Subito.it S.r.l. non presta alcuna garanzia circa il contenuto, la completezza e la correttezza delle stesse. Subito.it S.r.l. è altresì estranea alle trattative eventualmente nascenti dall'uso del Servizio e pertanto non garantisce nè la bontà nè l'esito delle stesse, di conseguenza nessuna richiesta di restituzione, compensazione, riparazione e/o risarcimento a qualunque titolo potrà essere indirizzata nei confronti di Subito.it S.r.l.

Il Servizio è offerto per il tramite del sito [www.subito.it](http://www.subito.it/), del m-site m.subito.it e delle applicazioni mobile che possono contenere banner/link ad altri siti Internet o applicazioni che non sono sotto il controllo di Subito.it S.r.l.; la pubblicazione dei predetti banner/link non comporta l’approvazione o l’avallo da parte di Subito.it S.r.l. dei relativi siti e dei loro contenuti, né implica alcuna forma di garanzia da parte di quest’ultima che pertanto non si assume alcuna responsabilità. L'utente riconosce, quindi, che Subito.it S.r.l. non è responsabile, a titolo meramente esemplificativo, della veridicità, correttezza, completezza, del rispetto dei diritti di proprietà intellettuale e/o industriale, né risponde della loro eventuale contrarietà all’ordine pubblico, al buon costume e/o alla morale.

**7\. Limitazioni nell'erogazione del Servizio**
------------------------------------------------

 Nell’evento in cui il cliente violi le regole editoriali di Subito.it (https://assistenza.subito.it/hc/it/sections/360000209545-Quali-regole-devo-seguire), quest’ultima si riserva il diritto di modificare, sospendere o interrompere, in tutto o in parte, il Servizio in qualsiasi momento anche senza preavviso, inoltrando al’utente le motivazioni sottese a tale decisione come da normativa vigente. In via esemplificativa e non esaustiva, Subito.it sospenderà il Servizio laddove (i) l’utente venda un articolo non realmente posseduto, (ii) l’utente venda un articolo contraffatto ovvero non originale (e.g. capi di abbigliamento, console modificate, etc.), (ii) i contenuti inseriti dall’utente  risultino contrari alle regole all’ordine pubblico o al buon costume (e.g. contenuti che incitano all’odio, ingiuriosi o atti a porre in pericolo la vita o la salute delle persone), etc..

I dettagli di tale procedura, così come il numero ed i dettagli relativi ai reclami ricevuti da Subito.it,  sono visionabili al seguente link: [https://www.subito.it/aziende/procedure-aziende](https://info.subito.it/policies/procedure-aziende.htm) (trasparenza - segnalazione reclamo). In ogni caso sarà inviata apposita comunicazione, anche tramite gli strumenti forniti dalla piattaforma, relativa al provvedimento adottato ed il cliente avrà facoltà di presentare chiarimenti o osservazioni tramite la procedura sopra indicata. 

Subito.it si riserva il diritto alla rubricazione degli annunci in una delle categorie merceologiche previste all’interno della propria piattaforma internet, anche in difformità dalle richieste del cliente, offrendone successivamente comunicazione anche solo mediante messaggio di posta elettronica. L’eventuale modifica alla rubricazione apportata da Subito.it nel caso di Servizi a Pagamento non darà diritto al cliente di recedere dal Servizio o alla richiesta di rimborso.

Subito.it si riserva altresì il diritto di indicare alla propria utenza quando gli annunci sono oggetto di spontanea segnalazione da parte degli utenti e/o dell’autorità giudiziarie che lamentino la violazione delle regole e/o delle Condizioni del Servizio e/o della normativa di riferimento. Resta inteso che la predetta segnalazione è eventuale e non vuole sostituirsi al giudizio del cliente che consulta le inserzioni, né garantisce la bontà o meno delle stesse. Ogni valutazione riguardo alla possibile integrazione di una fattispecie illecita da parte dell’annuncio/inserzionista segnalato è rimessa all’autorità competente.

Subito.it ha implementato un sistema antispam unitamente al form di risposta all’annuncio, che respinge le richieste di contatto attraverso il form in caso di violazione delle norme applicabili, delle regole e/o delle presenti condizioni (quali a mero titolo esemplificativo, presenza di insulti, epiteti razzisti, attività di spam o phishing, ecc.). Qualora il cliente  indichi nella casella di testo del form, link o indirizzi email, il sistema antispam, nel caso di annunci non provenienti da Servizi a Pagamento di Subito.it, oscurerà i predetti elementi dandone comunicazione al destinatario. Il sistema antispam in ogni caso non garantisce né l’autenticità del prodotto/servizio in vendita né la veridicità del contenuto del contatto. Subito.it non potrà quindi in nessun caso essere ritenuta parte o responsabile dello scambio di comunicazioni tra gli utenti e delle trattative da esse nascenti. Parimenti, Subito.it ha implementato un sistema c.d. “antifrode”, atto ad individuare automaticamente comportamenti messi in atto da potenziali utenti che adottano comportamenti non in linea con le regole di pubblicazione o con le presenti condizioni di servizio, bloccando il Servizio fornito ai medesimi.  

Subito.it non potrà parimenti essere ritenuta responsabile dei danni conseguenti alla mancata prestazione del Servizio oggetto del presente Contratto a causa dell'errato o mancato funzionamento dei mezzi elettronici di comunicazione per cause estranee alla sfera del proprio prevedibile controllo o per interruzioni dei servizi per lavori di manutenzione della piattaforma: a titolo esemplificativo, ma non esaustivo, il malfunzionamento dei server e di altri dispositivi elettronici, anche non facenti parte integrante della rete Internet, malfunzionamento dei software installati, virus informatici, azioni di hacker o di altri utenti aventi accesso alla rete.

Subito.it si riserva la facoltà di non pubblicare annunci contrari alla politica editoriale, di procedere in qualsiasi momento alla loro cancellazione e, nei casi più gravi, all'oscuramento degli annunci, nonché alla sospensione del servizio o alla sua interruzione  anche tramite il recesso da parte di Subito.it dal presente Contratto e senza che sia dovuto alcun corrispettivo per tale recesso, con facoltà di Subito.it di richiedere il risarcimento degli eventuali danni. In tali ipotesi, in caso di utente commerciale, ai sensi dell’art. 4 del Regolamento (UE) n. 1150/2019, verrà effettuata un’apposita comunicazione in merito alla sospensione dell’annuncio con cui saranno indicate le motivazione del provvedimento. Eventuali reclami potranno essere presentati tramite il meccanismo interno di risoluzione delle controversie, e secondo le modalità ivi indicate, descritto sul sito internet. Qualora si tratti di cessazione dei servizi, ed al di fuori delle ipotesi in cui la cessazione derivi dall’adempimento da parte di Subito.it di un obbligo normativo o regolamentare o dalla ripetuta violazione da parte dell’utente dei termini e condizioni stabiliti dal presente Contratto, verrà fornita comunicazione del recesso dai servizi con un preavviso di almeno 30 (trenta) giorni con indicazione delle motivazioni dello stesso. Nelle altre ipotesi la sospensione del servizio o il recesso saranno comunicati senza indebito ritardo.

Resta espressamente inteso che, con riferimento ai Servizi a Pagamento che necessitano della registrazione nell’Area Riservata, il Servizio non potrà più essere utilizzato qualora il cliente cancelli spontaneamente la propria Area Riservata. Il cliente nulla avrà a che pretendere da Subito.it per la mancata fruizione del medesimo.

**8\. Pubblicazione seriale di annunci e/o per conto terzi**
------------------------------------------------------------

E’ espressamente vietato, salvo esplicita autorizzazione da parte di Subito.it S.r.l.:  
\- l'utilizzo di sistemi automatici di caricamento annunci, salvo quelli espressamente autorizzati da Subito.it S.r.l.;  
\- la pubblicazione seriale e/o la gestione di annunci per conto terzi con ogni mezzo o modalità;  
\- rivendere a terzi i servizi di Subito.it S.r.l. (da intendersi per tali anche, a titolo esemplificativo, le opzioni Vendilo Subito, prodotti Impresa +, ecc.).

Al raggiungimento di soglie predeterminate di inserimento degli annunci del Servizio Base, variabili in base alla categoria di inserimento e alle direttive Subito.it S.r.l., l'utente non potrà inserire ulteriori annunci e verrà avvisato mediante una comunicazione di Subito.it.

Si riportano di seguito le soglie di pubblicazione degli annunci afferenti al servizio non a pagamento:

Motori: auto 1, caravan e camper 2, veicoli commerciali 3, moto e scooter 3, nautica 4, accessori auto e moto 5; Immobili: 1, casa vacanze 4; Lavoro: offerte di lavoro 1; candidati in cerca di lavoro 20; Elettronica 20; Casa e persona: elettrodomestici 20, arredamento e casalinghi 30, tutto per bambini 45, giardino e fai da te 45, abbigliamento e accessori 45. Sport e hobby: biciclette, sport e strumenti musicali 10, musica e film 20, animali 45; libri e riviste 130, collezionismo 250; Altri: 5.

**9\. Limitazioni al contenuto delle pubblicazioni ed informazioni connesse al profilo dell'utente**
----------------------------------------------------------------------------------------------------

L'utente si impegna a non falsificare la propria identità ed a rispettare tutte le disposizioni di legge vigenti nonché le [Regole](http://www.subito.it/info/regole.htm) di Subito.it S.r.l., che a tal fine l’utente accetta e riconosce di dover visionare prima dell’inserimento di ogni annuncio o di prendere contatto con un altro utente per il tramite del form di risposta o la funzione Messaggi.  

_A mero titolo esemplificativo, per quanto concerne gli annunci di animali, per il caso specifico degli animali di razza dei quali è consentita la commercializzazione, ai sensi dell’art. 5 del Decreto Legislativo 30 dicembre 1992, n. 529, il proprietario/detentore che ha pubblicato l’annuncio è tenuto ad indicare nello stesso il certificato genealogico rilasciato dalla relativa associazione._  
_Inoltre, ai sensi dell'art. 11 del D.Lgs n. 135/2022 il proprietario/detentore di un animale del quale è consentita la commercializzazione è tenuto ad indicare nell'annuncio l'identificativo previsto dalla normativa in vigore e ad avere la certificazione medico veterinaria attestante le condizioni sanitarie dell'animale._  
_Il proprietario/detentore dell’animale, pertanto, pubblicando l'annuncio sulla piattaforma [www.subito.it](http://www.subito.it/) di Subito.it accetta e dichiara di essere in possesso del certificato genealogico degli animali di razza e in ogni caso dell’identificativo e della certificazione medico veterinaria degli animali oggetto dei relativi annunci pubblicati, sollevando Subito.it da ogni e qualsiasi responsabilità in merito._

Quando l’utente è online ed è disponibile ad entrare in contatto con altri utenti, tale informazione viene resa accessibile tramite un pallino verde, visualizzabile in corrispondenza del profilo dell’utente e del dettaglio annuncio. L’utente può, in qualsiasi momento, decidere di cambiare tale impostazione e scegliere di apparire offline. Nel momento in cui l’utente risulta offline e non disponibile ad entrare in contatto con altri utenti, viene quantificato, in modo indicativo, il tempo trascorso dall'ultimo collegamento e/o visualizzazione online e tale informazione è resa disponibile in corrispondenza del profilo dell’utente e del dettaglio annuncio.

L'utente si impegna altresì, a non utilizzare il Servizio per la pubblicazione, trasmissione, scambio di materiale illecito, volgare, osceno, calunnioso, diffamatorio, offensivo della morale corrente, o, comunque, lesivo dei diritti altrui o di messaggi incitanti all'odio ed alla discriminazione razziale o religioso. Inoltre, l'utente si impegna a non utilizzare il Servizio in maniera tale da violare diritti di proprietà intellettuale o industriale di Subito.it S.r.l. o di terzi.

L’utente prende atto ed accetta che il Servizio richiederà, per finalità di revisione e sicurezza, il completamento di una specifica procedura di identificazione prima di procedere alla pubblicazione degli annunci o per accedere al Servizio e/o continuare ad usufruire dello stesso.

A tal proposito l'utente prende atto e accetta che ogni annuncio inserito riporterà obbligatoriamente in chiaro alcune delle informazioni di cui sopra, ed in particolare il numero di annunci online facenti capo all'inserzionista ed una stima del tempo di utilizzo/frequentazione dello stesso del Servizio. Dette informazioni devono ritenersi frutto di calcoli automatici approssimativi e non devono interferire/pregiudicare/influenzare le trattative eventualmente nascenti dall'uso del Servizio per le quali Subito.it S.r.l. resta estranea.

**10\. Pubblicazioni ulteriori**
--------------------------------

L'utente riconosce ed accetta la possibilità offerta da Subito.it S.r.l. di far pubblicare gli annunci anche su altri siti internet o sulle testate off-line facenti parte di Adevinta Group (cui appartiene Subito.it S.r.l.) o, comunque, appartenenti a soggetti terzi interessati a promuovere e/o pubblicare gli annunci dell'utente. L'utente acconsente inoltre all'utilizzo del contenuto dell'annuncio (foto, testo, ecc.) da parte di Subito.it S.r.l. al fine della pubblicazione dello stesso su siti terzi, riviste on line o cartacee o altri media, impegnandosi altresì a tenere indenne e manlevare Subito.it S.r.l. da qualsiasi responsabilità e/o richiesta al riguardo.

Subito.it S.r.l. vieta l’utilizzo da parte di software/applicazioni/siti cd. aggregatori e/o terzi, non espressamente e previamente autorizzati, di qualunque contenuto afferente al Servizio. Ogni violazione al predetto divieto potrà essere perseguito a norma di legge.

**11\. Valutazione dell'utente** 
---------------------------------

Ciascun utente, al termine di una transazione conclusa tramite il servizio “Tuttosubito” ([https://info.subito.it/tuttosubito/#tuttosubito](https://info.subito.it/tuttosubito/#tuttosubito)) oppure in caso di autonoma cancellazione del proprio annuncio a valle di una vendita raggiunta attraverso la piattaforma messa a disposizione da Subito, può recensire un altro utente e lasciare un proprio commento. La valutazione sull’utente è finalizzata ad esprimere un giudizio sulle modalità con cui è avvenuta la transazione e a segnalare l'esito positivo o negativo della stessa.

In considerazione di quanto sopra, non è possibile lasciare la propria valutazione nei confronti di utenti con cui l’utente non ha concluso alcuna transazione.

L’utente che acquista è libero di raccomandare o meno l’utente inserzionista dopo ogni transazione e viceversa. La valutazione sarà pubblicata solo nel momento in cui entrambe le parti avranno concluso la propria valutazione o, in alternativa, allo scadere di un periodo di 14 giorni dalla prima notifica di richiesta di valutazione dell’utente.

La pubblicazione delle valutazioni degli utenti avviene entro un massimo di 15 giorni dalla conclusione della transazione.

Il numero e la media delle valutazioni nonché gli eventuali commenti lasciati dagli utenti, saranno pubblicati in ordine cronologico e mostrati in corrispondenza al profilo dell’utente e ai rispettivi annunci.

In corrispondenza del profilo utente sarà inoltre visualizzabile il numero di compravendite completate tramite la piattaforma tramite un apposito contatore finalizzato a mostrare il numero di transazioni concluse attraverso il sito.

Nel caso in cui l’utente dovesse cancellare il proprio account, il nome di tale utente non sarà più visibile nelle valutazioni, ma apparirà l’indicazione che la valutazione proviene da un utente cancellato. Ogni eventuale commento di tale utente verrà rimosso definitivamente e non sarà pertanto più disponibile.

Subito potrebbe controllare o moderare contenuti palesemente offensivi afferenti alle valutazioni prima della loro pubblicazione. L’utente resta in ogni caso responsabile dei contenuti inseriti e pubblicati e ha l’obbligo di conformarsi alle presenti Condizioni di Servizio nonché al rispetto della netiquette. 

Subito può, in ogni caso, intervenire sulla base delle segnalazioni effettuate dagli utenti e rimuovere i contenuti offensivi, illeciti e non veritieri. Parimenti, Subito ha il diritto di intervenire su recensioni e contenuti ritenuti, ad insindacabile giudizio di quest’ultima, non pertinenti con la transazione avvenuta ovvero discordanti con quanto effettivamente emerso e/o riportato. 

E’ espressamente vietato qualsiasi compenso in cambio di apprezzamenti e/o valutazioni positive a favore di altri utenti.

**12\. Obblighi di legge di Subito a seguito della normativa europea DAC7**
---------------------------------------------------------------------------

A seguito dell’introduzione nel 2023 della Normativa Europea DAC 7, tutte le piattaforme online inclusa Subito sono obbligate a comunicare alle autorità fiscali locali le informazioni riguardanti le vendite e i guadagni degli utenti che, utilizzando il servizio TuttoSubito, superino nell’anno civile la soglia di 29 transazioni o un guadagno di 2.000€.

Solo in caso di superamento di queste soglie, Subito richiederà all’utente tramite email di  fornire le informazioni fiscali e confermare i dettagli anagrafici compilando il formulario DAC7.

Qualora l’utente non compilasse il modulo DAC7, Subito potrà limitare la sua attività in piattaforma, ad esempio sospendendo la possibilità di fare transazioni o disattivando temporaneamente l’account.

**13\. Reclami. Procedura per gli utenti.**
-------------------------------------------

Laddove un utente ritenga illeciti taluni contenuti pubblicati all’interno della sezione annunci, lo stesso potrà usufruire della procedura di segnalazione messa a disposizione dalla piattaforma, attraverso la quale i contenuti considerati illeciti potranno essere segnalati direttamente a [Subito.it](http://subito.it/).  
L’utente dovrà motivare la segnalazione con sufficiente precisione e adeguatezza ed, in particolare, fornire gli elementi richiesti da [Subito.it](http://subito.it/) nel corso del processo di segnalazione, dichiarando peraltro che tale azione viene effettuata in buona fede. Se la segnalazione contiene i dati di contatto elettronici dell’utente, [Subito.it](http://subito.it/) provvederà ad inviare la relativa conferma di ricezione. L'identità dell’utente segnalante verrà divulgata solo in caso di assoluta necessità. Resta inteso che sono consentiti altri canali di segnalazione anonima solamente nelle ipotesi previste dalle normative vigenti. [Subito.it](http://subito.it/) procederà a verificare la segnalazione, decidendo sulla medesima tempestivamente e con imparzialità. In seguito a tale  verifica, [Subito.it](http://subito.it/) provvederà ad informare l’utente segnalato nonché l’utente segnalante circa (i) la propria decisione e (ii) i mezzi a disposizione per effettuare un eventuale ricorso nei confronti della stessa, restando inteso che tale diritto potrà essere esercitato entro e non oltre 6 (sei) mesi dalla notifica della decisione da parte di [Subito.it](http://subito.it/).

**14\. Consultazione listing. Annunci Pubblicitari. Dynamic Pricing.**
----------------------------------------------------------------------

A valle di una specifica ricerca da parte dell’Utente, la piattaforma -  una volta individuati gli annunci attinenti alla stessa - li organizza ordinandoli secondo i seguenti criteri:

\- per rilevanza: quando si ricerca in una categoria specifica (tranne per le categorie di Immobili e Motori), come impostazione predefinita vengono mostrati gli annunci più rilevanti. Tale rilevanza è attribuita agli annunci dall’algoritmo di Subito che mette principalmente in relazione le parole cercate al testo dell’annuncio contenuto nel titolo e nella descrizione;  
\- per data di pubblicazione dell'annuncio: questo tipo di ordinamento, quando selezionato, mostra i risultati in ordine di data di pubblicazione dell’annuncio dal più recente a quello meno recente;  
\- per prezzo crescente o decrescente: puoi scegliere di visualizzare i risultati con un ordinamento per prezzo, alternativamente dal prezzo più alto (decrescente) o dal prezzo più basso (crescente).

Gli annunci pubblicitari sono visualizzati dall’utente in base ad uno o più dei seguenti criteri:  
\- criteri di ricerca utilizzati dall'utente;  
\- precedenti attività svolte sulla piattaforma di [Subito.it](http://subito.it/);  
\- preferenze relative alla condivisione dei propri dati ovvero alla pubblicità personalizzata.

Da ultimo, resta inteso che, con specifico riferimento alle c.d. "Opzioni di Visibilità" offerte da Subito.it ai propri utenti non professionali, taluni prezzi di listino riportati sulla piattaforma sono automatizzati e pertanto potrebbero variare di volta in volta in base a specifiche caratteristiche dell'annuncio.

**15\. Giurisdizione, legge applicabile e foro competente**
-----------------------------------------------------------

I rapporti tra Subito.it S.r.l.e gli utenti sono regolati dalla legge e dalla giurisdizione italiana, in base alla quale anche le presenti Condizioni Generali dovranno essere interpretate. Salvo quanto disposto da norme di legge non derogabili, il Tribunale di Milano sarà competente in via esclusiva a dirimere ogni controversia riguardante le presenti Condizioni Generali ed i rapporti dalle stesse regolati.

**16\. Validità delle presenti Condizioni Generali**
----------------------------------------------------

Le presenti condizioni Generali di Servizio si considereranno applicabili, ove compatibili, anche in caso di ulteriori, diversi e specifici accordi relativi ai servizi a pagamento (ed in particolare con riferimento ai servizi "Premium", "Pro" e "Promuovi"). Subito.it S.r.l. potrà comunque apportare unilateralmente in qualsiasi momento modifiche alle presenti Condizioni Generali dandone comunicazione sulla propria piattaforma web.

**17\. Modifiche**
------------------

Le presenti condizioni potrebbero essere soggette a modifiche. In caso di sostanziali modifiche, Subito.it S.r.l. avviserà l’utente pubblicandole con la massima evidenza sulle proprie pagine o tramite email o altro mezzo di comunicazione.