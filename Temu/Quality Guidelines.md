Review guidelines
=================

* English
_|_* Gaeilge

We encourage high quality reviews like:

![](https://aimg.kwcdn.com/upload_aimg/web/198bd873-1d61-4978-9c2f-64b6ac5d79f8.png.slim.png?imageView2/2/w/800/q/70/format/webp)

More than 30 characters

![](https://aimg.kwcdn.com/upload_aimg/web/198bd873-1d61-4978-9c2f-64b6ac5d79f8.png.slim.png?imageView2/2/w/800/q/70/format/webp)

With pictures or videos

![](https://aimg.kwcdn.com/upload_aimg/web/70eadd3b-5528-4d6f-908b-7624a325ffac.png.slim.png?imageView2/2/w/800/q/70/format/webp)

Real feedback experience

For example:

D\*\*\*he

![](https://aimg.kwcdn.com/upload_aimg/web/51bc9561-1837-4fcf-bd49-e89366f005be.png.slim.png?imageView2/2/w/800/q/70/format/webp)![](https://aimg.kwcdn.com/upload_aimg/web/51bc9561-1837-4fcf-bd49-e89366f005be.png.slim.png?imageView2/2/w/800/q/70/format/webp)![](https://aimg.kwcdn.com/upload_aimg/web/51bc9561-1837-4fcf-bd49-e89366f005be.png.slim.png?imageView2/2/w/800/q/70/format/webp)![](https://aimg.kwcdn.com/upload_aimg/web/51bc9561-1837-4fcf-bd49-e89366f005be.png.slim.png?imageView2/2/w/800/q/70/format/webp)![](https://aimg.kwcdn.com/upload_aimg/web/51bc9561-1837-4fcf-bd49-e89366f005be.png.slim.png?imageView2/2/w/800/q/70/format/webp)

![](https://aimg.kwcdn.com/upload_aimg/web/3293d821-c9da-4713-8471-25bde0fcb2bd.png.slim.png?imageView2/2/w/800/q/70/format/webp)![](https://aimg.kwcdn.com/upload_aimg/web/2787ab62-5086-4c5f-bbbf-83b2cbf83346.png.slim.png?imageView2/2/w/800/q/70/format/webp)![](https://aimg.kwcdn.com/upload_aimg/web/7cae290e-4e2f-40e6-9cac-d20335f1eead.png.slim.png?imageView2/2/w/800/q/70/format/webp)

I have ordered my precious baby girl close to 20 outfits and shoes, colurs, hats and I can honestly say so far I am 100% satisfied with everything!!!!! I am so glad I found this app!! everything is cheap priced but great products and not cheap made!!! I was so excited to get her things!!! and I love my things I've purchased for me also!!!!

How we keep reviews trustworthy and useful?

To ensure only real customers post reviews, we have submission requirements. Customers who have purchased items on Temu are eligible to submit ratings and reviews for the items they have purchased.

Before posting a review, we check if it meets our [Community Guidelines](https://www.temu.com/community-guidelines.html).

Our automated and human checks stop millions of suspicious reviews before customers ever see them. We also take legal action against groups that pay customers to post fake reviews.

We listen to our customers. If you think a review violates our guidelines, you can report the review and we'll investigate it.

How we calculate overall star ratings?

The overall star rating reflects the average of all valid ratings submitted by customers. A star rating may be excluded from the average if its review violates Temu's Review guidelines.

What are our review guidelines?

When you write reviews about Temu or items sold on Temu, you should:

1\. Be truthful.

Your review, including words, photos, and other materials, should be about your own experience on Temu or about a product sold on Temu. Your review should not mislead viewers about any feature, function, or quality of a product.

2\. Be respectful.

Your review should not contain anything that is obscene, libelous, hateful, violent, offensive, illegal, sexually explicit, or otherwise inappropriate.

3\. Respect intellectual property rights.

Your review should not contain any material that you do not have permission to use or infringes on any intellectual property of any third party.

4\. Use product as intended.

Your review should be about using a product as intended by the manufacturer. Your review should not depict a product being used in an unintended manner, especially in any unsafe or illegal manner.

5\. Be mindful of privacy.

Your review should not contain personal information of your own or a third party, including phone numbers, mailing addresses, email addresses, etc.

6\. Do not mention competitors.

Your review should not compare products or services of other competitors by name.

7\. Do not mention promotional codes.

Your review should not contain any information about promotional codes or invitation codes.

For more details, please visit our [Community Guidelines](https://www.temu.com/community-guidelines.html).

How long are reviews displayed?

As long as the review meets the review guidelines and [Community Guidelines](https://www.temu.com/community-guidelines.html), and the item is currently available, the review will be displayed.

Are sponsored or influenced reviews allowed on Temu?

Traders are not allowed to sponsor or influence reviews posted on Temu.