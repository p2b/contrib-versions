[What Information Do We Collect](#information_collect)

[How and Why We Process Your Information and Our Legal Bases for Processing](#information_process_and_legal_bases)

[How and Why We Share Your Information](#information_share)

[Your Rights and Choices](#controls_choices)

[Our Global Operations and Data Transfers](#global_opetations)

[Children](#children)

[Data Security and Retention](#data_security)

[Changes to this Privacy Policy](#change_privacy)

[Contact Us](#contact_us)

Temu | Privacy Policy
=====================

Effective Date: January 1, 2025

This Privacy Policy describes how Whaleco Technology Limited, an Irish registered company (“Temu”, “we", “us” or “our”) handles Personal Data relating to persons located in the European Union (EU), the European Economic Area (EEA), and Switzerland that we collect through our digital properties that link to this Privacy Policy, including our website (www.temu.com), Temu's mobile application (collectively, the “Service”), and other activities as described in this Privacy Policy. At Temu, we care deeply about privacy. This Privacy Policy explains how we collect, use, share, and otherwise process the Personal Data of our registered account holders, those who purchase items on Temu without an account, and those who browse the Service (“users”). In this Privacy Policy, we also explain the rights individuals who are affected by our data handling have with regard to their Personal Data, including the right to object to certain types of processing we carry out. For more information, see the “[Your Rights and Choices](#controls_choices)” section below.

For the purpose of this Privacy Policy, “Personal Data” has the meaning given in the General Data Protection Regulation (“GDPR”), i.e. meaning any information that relates to an identified or identifiable natural person (the “Data Subject”).

Data Controller: If you are located in the EU, EEA, or Switzerland, Whaleco Technology Limited, an Irish registered company, is the data controller responsible for your Personal Data. Contact details for Whaleco Technology Limited are set out in the “[Contact Us](#contact_us)” section below.

What Information Do We Collect
------------------------------

In the course of providing and improving our products and services, we collect your Personal Data for the purposes described in this Privacy Policy. The following are the types of Personal Data that we collect:

### **Information that you provide**

When you create an account, browse the Service, place an order at checkout, contact us directly, or otherwise use the Service, you provide some or all of the following information:

> ##### **Account and profile**
> 
> We collect your mobile phone number and/or email address as the login credentials for your account and assign a user identification number (“UID”) to your account. If you choose to register or sign in via external third-party services, such as Facebook or Google, we will collect your profile photo, username, and/or email address associated with the relevant third-party service provider. We also collect your profile information, account settings, and preferences.

> ##### **Purchases**
> 
> We collect data related to your orders on the Service (e.g., transaction ID, transaction history), payment information required to complete the transaction (e.g., payment card number or other third-party payment information required for payment, billing address), information provided by you for product sizing purposes, your confirmation that you satisfy the age threshold for certain products, your address, designated package collection point (e.g., city, state, country of residence, postal code), and recipient contact information (e.g., name, mobile phone number). We also collect your region on a country/region basis.

> ##### **Customer support activity**
> 
> When you communicate with our customer service team through our customer support functions in the mobile application/on the website, either with a customer service agent or with our virtual assistant (via the chatbot or hotline), through social media, or any other means, we will collect your communication history with us which includes any text, images, video, audio, or supporting documents exchanged between us.

> ##### **Information when you contact us**
> 
> When you contact us, we collect the information you send us, such as reports, feedback, or inquiries about your use of the Service or information about possible violations of our [Terms of Use](https://www.temu.com/terms-of-use.html) or other policies.

> ##### **Chats with merchandise partners**
> 
> When you chat with merchandise partners on Temu, we collect your chat communications and other related information.

> ##### **User-generated content**
> 
> When you provide product reviews and ratings on the Service, we collect this information, including any accompanying images, videos or text, as well as associated metadata.

> ##### **Promotion and event participation**
> 
> We may collect certain information when you participate in a contest, event, promotion, survey, giveaway or sweepstake as part of the participation process, such as your desired prize selection. Such sweepstakes and contests are voluntary. We recommend that you read the rules and other relevant information for each sweepstake and contest that you enter. In addition, we collect information about your preferences for receiving marketing communications from us, as well as your interactions with them.

> ##### **Other data**
> 
> We will collect other data that you provide for purposes as described in this Privacy Policy, or for any other purpose disclosed to you at the time we collect your information.

### **Information collected automatically**

To enhance your experience with the Service and support the other purposes for which we collect Personal Data as outlined in this Privacy Policy, we automatically process information about you, your computer or mobile device, your interactions with the Service, and our communications over time, such as:

> ##### **Device data**
> 
> We collect Personal Data about the device you use to access the Service, such as device model, operating system information, language settings, unique identifiers (including identifiers used for advertising purposes where we have a legal basis for doing so).

> ##### **Service usage information**
> 
> We collect Personal Data about your interactions with the Service, including the items in your shopping cart, your order pages you view, your duration on a page, the source from which you arrived at a page, your interactions with a page, your searched text and images, your browsing history, whether you opened our emails, and whether you clicked the links within our emails. We also collect service-related, diagnostic, and performance information, including crash reports and performance logs.  

> ##### **General location data**
> 
> We collect your approximate location based on your technical information (e.g., IP address).  

> ##### **Cookies data**
> 
> We collect information from cookies and similar technologies stored on your device. These cookies are either necessary and required to provide our Service (for example, the cookies we use to display products relevant to your language and location) or optional (for example, the cookies used to track and measure the advertisements shown to you). To learn more, including how to disable certain cookies, please read our [Cookies and Similar Technologies Policy](https://www.temu.com/cookie-and-similar-technologies-policy.html).

### **Information from third-party sources**

To the extent permitted by applicable law, we receive and collect Personal Data from other third-party sources, such as:

> ##### **Data providers**
> 
> We receive and collect Personal Data, such as identity verification information and fraud prevention risk-scoring, from information services to help us detect fraud regarding payments, prevent unauthorised transactions, and safeguard the Service.

> ##### **Marketing and advertising partners**
> 
> We receive and collect Personal Data from our marketing and advertising partners, such as business partners with whom we collaborate on marketing events and promotion of the Service. Specifically, we receive from these partners information including, for example, the specific advertisements you clicked on, attribution information via cookies and similar technologies to help us determine the origin of traffic to our Service, device data if you clicked on certain advertisements, responses to marketing emails, advertisements and offers, and audience information from marketing partners where they have the authority to share that information with us.

> ##### **Public authorities **in the EEA and Switzerland**, public sources, and rights holders**
> 
> We obtain Personal Data which may include username, address, email, mobile phone number, purchase and/or payment information from third party sources, for example, as necessary to comply with our obligations, prevent, investigate and detect an alleged claim or crime, or for a party to assert their legal rights.

> ##### **Our payment processors**
> 
> We obtain Personal Data from our third-party payment processors who help us process transactions, provide our Service, and prevent fraudulent or illegal activity. The Personal Data our payment processors pass to us includes your name, email address, billing address, and phone number.

> ##### **Other third-party services**
> 
> We obtain your Personal Data from other third-party services, such as:
> 
> * **Social media service providers**, from whom we collect your username, profile picture, and/or email address associated with the relevant third-party service provider, if you choose to register or sign in on the Service using said third-party service.
>     
> * **Logistics service providers**. To effectively complete order fulfilment, we will obtain your logistics information from logistics providers, such as delivery progress and delivery address, and make this information available to you within your account.
>     
> * **Third-party mapping platforms**. To easily direct you to the most appropriate package collection point for deliveries in the countries where we enable this functionality, when you provide your address to us, we receive, collect and locate the geographical coordinates of the address you have provided to us from third-party mapping platforms, such as Google maps.
>     

### **Declining to provide information**

We need to collect Personal Data to provide certain services. If you do not provide the information we require to provide the Service, we cannot provide the Service. Other information is optional, but without it, the quality of your experience of the Service may be affected.

**How and Why We Process Your Information and Our Legal Bases for Processing**
------------------------------------------------------------------------------

We use the Personal Data that we collect for various purposes, including to develop, improve, support and provide the Service, allowing you to use its features while fulfilling and enforcing our [Terms of Use](https://www.temu.com/terms-of-use.html).

In the EEA and Switzerland, we require a legal basis to process your Personal Data. We rely on different legal bases depending on why we are processing your Personal Data.

For each legal basis we rely on, we explain in this section the purposes of our processing (why we use your Personal Data) and the specific processing operations we perform (how we handle your Personal Data to achieve those purposes). We also set out the categories of Personal Data involved in each case.

You have rights available to you depending on which legal basis we use. For more information on how to exercise your rights, see the “[Your Rights and Choices](#controls_choices)” section below.

### **Contractual Necessity**

When you register for, access or use the Service, we process Personal Data as necessary to perform the contracts you enter into with us (such as our [Terms of Use](https://www.temu.com/terms-of-use.html)) if you have the legal capacity to enter into an enforceable contract.

When we process Personal Data you provide to us which is necessary to perform a contract with you, you have the right to port it to another data controller. To exercise your rights, please visit the “[Your Rights and Choices](#controls_choices)” section of this Privacy Policy.

> ##### **Account creation and maintenance**
> 
> If you create a user account on the Service, we create and maintain your account, using your account and profile information, device data, and general location data (e.g., IP address), in accordance with your settings and preferences, and enable account security features (e.g., sending security codes via email or SMS).
> 
> We will collect information from social media services if you choose to register or sign in on the Service using said third-party service.

> ##### **Orders and delivery of products and services**
> 
> We facilitate adding items to your shopping cart and enable provision of customised products using your service usage information (including customised order information you provide), allow you to chat in relation to your orders using chats with merchandise partners, receive and process your orders using account and profile and purchases information, deliver products to you using information from logistics service providers and third-party mapping platforms, and process your payments using information from our payment processors. We facilitate order disputes, refunds and/or returns of your orders using information categories including customer support activity, purchases, general location data (e.g., IP address), information from logistics service providers, and information from our payment processors.

> ##### **Provide search results and listings for non-personalised products you may wish to buy where strictly necessary to provide the Service**
> 
> For example, when you search for a particular item, we will present a selection of items matching your search criteria using information categories, including general location data (e.g., IP address) and service usage information.

> ##### **Contest, event, promotion, survey, giveaway or sweepstake**
> 
> If you have agreed to terms as part of a contest, event, promotion, survey, giveaway or sweepstake, we will facilitate your participation using promotion and event participation information and general location data (e.g., use your IP address to help decide if a contest, event, promotion, survey, giveaway or sweepstake applies to you), verify your identity using your account and profile information, inform you about the result using service usage information, and deliver the prize using purchases data, in line with the relevant terms and rules.
> 
> Such sweepstakes and contests are voluntary. We recommend that you read the rules and other relevant information for each sweepstake and contest that you enter.

> ##### **Customer support**
> 
> We provide customer support for your requests, including your data subject rights requests, questions, disputes, complaints and feedback. For example, we provide online, hotline and after-sale services.
> 
> For this purpose, we may use information categories including account and profile, chats with merchandise partners, customer support activity, device data, purchases, and service usage information.

> ##### **To communicate with you**
> 
> To the extent strictly necessary to provide the Service in line with the [Terms of Use](https://www.temu.com/terms-of-use.html), we use your account and profile and device data to communicate with you (e.g., announcements, updates, security alerts, support, and administrative messages), which also involves the use of customer support activity and service usage information.

> ##### **Maintain permissions for sending messages**
> 
> To the extent strictly necessary to provide the Service in line with the [Terms of Use](https://www.temu.com/terms-of-use.html). This involves the use of your account and profile and device data.

> ##### **Platform Safety and Security**
> 
> To the extent strictly necessary to provide the Service in line with the [Terms of Use](https://www.temu.com/terms-of-use.html), we prevent, detect, investigate, and respond to possible unauthorised access, fraudulent transactions, and harmful, unauthorised, unethical, or illegal activities, including cyberattacks and identity theft.
> 
> For this purpose, we may use information categories including account and profile, chats with merchandise partners, customer support activity, device data, general location data (e.g., IP address), information from data providers, information from our payment processors, purchases, service usage information and user-generated content.

> ##### **Publication of user-generated content**
> 
> In accordance with your settings, we make available your user-generated content. For example, when you leave a review, we process your username, profile picture and the content of your review to make the review available on the relevant product page and your user profile.
> 
> For this purpose, we may use information categories including user-generated content, service usage information, and account and profile.

> ##### **Interaction with other users**
> 
> If you choose to interact with other users on or through the Service, we facilitate your interaction with those other users. For example, when you click the “Helpful” button on other users’ item reviews, we will notify them of your interaction.
> 
> For this purpose, we may use information categories including user-generated content, service usage information, and account and profile.

### **Legitimate Interests**

Where there is no legally enforceable contract in place, or where the processing is not strictly necessary, we can rely on legitimate interests to process your Personal Data for the purposes described above.

We rely on legitimate interests (ours, yours or those of another party) where they are not outweighed by your interests or fundamental rights and freedoms.

You have the right to object to, and seek restriction of, processing of your Personal Data based on legitimate interests. To exercise your rights, please visit the “[Your Rights and Choices](#controls_choices)” section of this Privacy Policy.

> ##### **Recommend content and products, (such as product listings and search queries) that you might be interested in, where not strictly necessary to provide the Service to you.**
> 
> To recommend products and content that may be of interest to you and others, we use the following categories of your Personal Data: account and profile, general location data (e.g., IP address), purchases (including transaction history), service usage information, and user-generated content.
> 
> You can find more information on the various criteria we use for recommending content and products to you and how you can modify or influence those recommendations [here](https://www.temu.com/digital-services-act-help.html).
> 
> Legitimate interest relied upon:
> 
> * It is in our interests to be able to provide a personalised experience for users of the Service.
>     
> * Users have an interest in discovering products/notifications/searches that may be interesting and meaningful to them.
>     
> * Merchandise partners have an interest in reaching a more relevant audience, which may increase revenues.
>     

> ##### **Promotion activities and events, which may be personalised**
> 
> Where not strictly necessary to provide a competition you enter, we process your Personal Data to facilitate sweepstakes, contests, giveaways, and other promotions and events (e.g., to notify you of wins, to verify your identity, or to send you prizes). Depending on your profile settings, we will use your user profile picture and account information in promotions, which may be shared with other users, and facilitate your invitations to friends who you want to invite to join the Service.
> 
> Information categories used for this purpose may include promotion and event participation, general location data (e.g., IP address), service usage information, account and profile, and purchases (including transaction history).
> 
> Legitimate interest relied upon:
> 
> * It is in our commercial interests to provide a competition, sweepstake or other promotion that is appealing to actual and potential users of the Service.
>     
> * It is in the interests of actual and potential users who are interested in participating in a competition, sweepstake or other promotion.
>     
> * It is in our interests to be able to provide a personalised experience for users of the Service.
>     
> * Users have an interest in discovering promotions that may be interesting and meaningful to them.
>     

> ##### **Send Marketing Messages**
> 
> We collect and use your Personal Data for marketing purposes in accordance with your stated preferences and applicable law, which may be personalised. Depending on your communication preferences, we may send you direct marketing communications, using your account and profile information, device data, service usage information, and information from marketing partners.
> 
> Legitimate interest relied upon:
> 
> * It is in our commercial interests to provide messages to you that might promote the Service.
>     
> * It is in the interests of users who have indicated a wish to receive marketing materials (and not since indicated otherwise).
>     
> * It is in our interests to be able to provide a personalised experience for users of the Service.
>     
> * Users have an interest in marketing messages that may be interesting and meaningful to them.
>     

> ##### **Provide off-platform interest-based advertising, which are personalised**
> 
> We, our service providers, and our third-party advertising partners process your Personal Data for interest-based advertising purposes off the platform, and for analytics. In providing interest-based advertising, we follow the Self-Regulatory Principles for Online Behavioural Advertising, as set forth by the European Interactive Digital Advertising Alliance (EDAA), which is an advocacy organisation for responsible digital marketing and consumer privacy. You can learn more about interest-based advertising and your opt-out choices of third-party advertising networks [here](https://youronlinechoices.eu/).
> 
> Information categories used for this purpose may include account and profile, cookies data, device data, general location data (e.g., IP address), purchases, and service usage information.
> 
> See our [Cookies and Similar Technologies Policy](https://www.temu.com/cookie-and-similar-technologies-policy.html) for more information on our advertising cookies.
> 
> Legitimate interest relied upon:
> 
> * It is in our commercial interests to promote the Service and measure the effectiveness of advertising.
>     
> * It is in the interests of persons who would be happy to learn or learn more about the Service and discover relevant products that they are interested in, including advertising that is appropriate and relevant to their location and language.
>     
> * It is in the interests of merchandise partners operating on the Service to promote their products to potential customers.
>     

> ##### **Ensure Security of the Platform**
> 
> We prevent, identify, investigate, report and deter fraudulent, harmful, unauthorised, unethical or illegal activities, including fraud, unauthorised access to or use of the Service, violations of our policies, cyberattacks, identity theft or other misconduct, where not strictly necessary to provide the Service in line with the [Terms of Use](https://www.temu.com/terms-of-use.html) and/or where there is no legal obligation to do so.
> 
> Information categories used for this purpose may include account and profile, chats with merchandise partners, customer support activity, device data, general location data (e.g., IP address), information from data providers, our payment processors, public authorities in the EEA and Switzerland, public sources and rights holders, information when you contact us, purchases, service usage information, and user-generated content.
> 
> Legitimate interest relied upon:
> 
> * We benefit by taking steps to: (i) ensure that any harmful, unauthorised, unethical and/or unlawful activity on or in connection with the Service is detected, investigated, reported and prevented accordingly; and (ii) ensure that the Service can be provided in a manner that is safe and compliant with applicable laws.
>     
> * Law enforcement agencies benefit because the information provided by us can assist with the detection, investigation and prevention of criminal offences.
>     
> * Users of the Service benefit because those seeking to use the Service in a harmful, unauthorised, unethical and/or unlawful manner to cause harm to other users can be identified and removed from the platform.
>     
> * Third-party financial institutions benefit because perpetrators of financial crimes can be detected, investigated and penalised accordingly.
>     
> * Victims of unlawful activity benefit because the relevant perpetrator(s) can be detected, investigated and penalised accordingly.
>     
> * The wider public benefits because criminal offences can be detected, investigated and prevented.
>     

> ##### **Enable security features within the Temu **mobile application** and website which enable you to safeguard your Temu account**
> 
> We process your Personal Data to enable security features of the Service, such as by sending you security codes via email or SMS using your account and profile information, and remembering devices from which you have previously signed in using device data and general location data (e.g., IP address).
> 
> For this purpose, we may use information categories including account and profile information, purchases, service usage information, device data and general location data (e.g., IP address).
> 
> Legitimate interest relied upon:
> 
> * We benefit by taking steps to ensure that any unauthorised and/or unlawful activity or access on or in connection with the Service is prevented and detected.
>     
> * Users of the Service benefit because those seeking to use the Service in a harmful, unauthorised, unethical and/or unlawful manner to cause harm to other users can be prevented from accessing the Service and, where not prevented, can be identified and removed from the platform.
>     
> * Third-party financial institutions benefit because perpetrators of financial crimes will find it more difficult to compromise Temu accounts and cardholder information linked to those accounts.
>     

> ##### **Engage with valid requests from legal authorities, regulators and rights holders**
> 
> We take necessary action to protect rights, privacy, safety or property (including the institution and defence of legal claims), and to enforce the [Terms of Use](https://www.temu.com/terms-of-use.html).
> 
> Information categories used for this purpose may include account and profile, customer support activity, device data, general location data (e.g., IP address), information from public authorities in the EEA and Switzerland, public sources and rights holders, purchases, service usage information, and user-generated content.
> 
> Legitimate interest relied upon:
> 
> * It is in our interests to take steps to ensure that: (i) any harmful, unauthorised, unethical and/or unlawful activity on or in connection with the Service is detected, investigated and prevented; and (ii) the Service can be provided in a manner that is safe and compliant with applicable laws.
>     
> * Law enforcement agencies benefit because the information provided by us can assist with the detection, investigation and prevention of criminal offences.
>     
> * Users of the Service benefit because those seeking to use the Service in a harmful, unauthorised, unethical and/or unlawful manner to cause harm to users can be identified and removed from the platform.
>     
> * Third-party financial institutions benefit because perpetrators of financial crimes can be detected, investigated and penalised accordingly.
>     
> * Victims of unlawful activity benefit because the relevant perpetrator(s) can be detected, investigated and penalised accordingly.
>     
> * The wider public benefits because criminal offences can be detected, investigated and prevented.
>     

> ##### **Improve and optimise services, features, analyse performance metrics, and improve the Service and our business**
> 
> We improve the Service, analyse performance, and maintain and improve our business. As part of these activities, we may create aggregated or otherwise deidentified data. We fix errors and ensure the Service works properly outside what is strictly contractually necessary to provide you with the Service. We may use your customer support activity and chats with merchandise partners data to optimise our customer service support activity, for example, to improve the responses provided by our virtual assistant.
> 
> Other information categories used for this purpose may include account and profile, device data, general location data (e.g., IP address), information when you contact us, purchases, service usage information, and user-generated content.
> 
> Legitimate interest relied upon:
> 
> * It is in our commercial interests to provide an effective Service that performs well and is updated regularly.
>     
> * It is in the interests of users to receive a regularly updated service that performs well.
>     

> ##### **Retain certain user information for legal reasons (e.g., where necessary to defend against legal claims or to comply with regulatory compliance requirements).**
> 
> Information categories used for this purpose may include account and profile, customer support activity, device data, general location data (e.g., IP address), information from public authorities in the EEA and Switzerland, public sources and rights holders, and purchases.
> 
> Legitimate interest relied upon:
> 
> * It is in our interests to take steps to ensure that: (i) any harmful, unauthorised, unethical and/or unlawful activity on or in connection with the Service is detected, investigated and prevented; and (ii) the Service can be provided in a manner that is safe and compliant with applicable laws.
>     
> * Law enforcement agencies benefits because the information provided by us can assist with the detection, investigation and prevention of criminal offences.
>     
> * Users of the Service benefit because those seeking to use the Service in a harmful, unauthorised, unethical and/or unlawful manner to cause harm to other users can be identified and removed from the platform.
>     
> * Third-party financial institutions benefit because perpetrators of financial crimes can be detected, investigated and penalised accordingly.
>     
> * Victims of unlawful activity benefit because the relevant perpetrator(s) can be detected, investigated and penalised accordingly.
>     
> * The wider public benefits because criminal offences can be detected, investigated and prevented.
>     

> ##### **A****ddress user complaints or data subject rights requests**
> 
> We disclose information to facilitate user complaints and data subject rights requests as described in the “[Your Rights and Choices](#controls_choices)” section below.
> 
> Information categories used for this purpose may include account and profile, customer support activity, device data, and purchases.
> 
> Legitimate interest relied upon:
> 
> * It is in our commercial interests to provide a Service that meets the expectations of our users with respect to the exercise of their rights.
>     
> * It is in the interests of users to receive a service that meets their expectations with respect to the exercise of their rights.
>     

> ##### **For litigation matters**
> 
> Where applicable in defence or in pursuit of legal claims, we may process the following categories of your Personal Data: account and profile, customer support activity, device data, general location data (e.g., IP address), information from public authorities in the EEA and Switzerland, public sources and rights holders, and purchases.
> 
> Legitimate interest relied upon:
> 
> * It is in our commercial interests to protect our legal rights and interests.
>     
> * It is in the interests of third parties to protect their legal rights and interests.
>     

### **Your Consent**

We process Personal Data for the purposes described below when you have given us your prior consent. You have the right to withdraw consent to processing based on your prior consent at any time without affecting the lawfulness of processing based on such consent before the consent is withdrawn. To exercise your rights, visit the “[Your Rights and Choices](#controls_choices)” section of this Privacy Policy.

> ##### **Where your prior consent is obtained**
> 
> In some cases, we will specifically ask for your prior consent to collect, use or share your Personal Data to the extent permitted by applicable law. For example, we will seek your prior consent when we place certain cookies or similar technologies on your device.
> 
> Depending on the circumstances, information categories used may include account and profile, device data, general location data (e.g., IP address), information from cookies and similar technologies, and information from marketing partners.

### **Compliance with a Legal Obligation**

We may use your Personal Data where it is necessary for us to comply with a legal obligation.

> ##### **Compliance and legal obligations**
> 
> We can use your Personal Data for compliance purposes and to comply with applicable law, lawful requests and legal processes (e.g., responding to subpoenas or requests from government or regulatory authorities, or where there is a mandatory duty to report criminal offences); to protect our, your and other users' rights, privacy, safety or property (including the institution and defence of legal claims); audit internal processes to ensure compliance with legal and contractual requirements and our internal policies; enforce the [Terms of Use](https://www.temu.com/terms-of-use.html); and prevent, identify, investigate and deter fraudulent, harmful, unauthorised, unethical or illegal activities, including cyberattacks and identity theft.
> 
> Information categories used for this purpose may include account and profile, chats with merchandise partners, customer support activity, device data, general location data (e.g., IP address), information from public authorities in the EEA and Switzerland, public sources and rights holders, purchases, service usage information, user-generated content, and other data.
> 
> Examples of Irish and EU laws enforceable in Ireland that could give rise to a legal obligation requiring us to process information we hold about you are the Irish Data Protection Act 2018, the GDPR, and the Digital Services Act.

How and Why We Share Your Information
-------------------------------------

At Temu, we care deeply about privacy. In certain circumstances described in this Privacy Policy, we share your Personal Data with the following parties for the purposes outlined below.

### **Affiliates**

As a global company, our Service is supported by other entities within our corporate group, in addition to Whaleco Technology Limited. We share some of your Personal Data with Temu subsidiaries and affiliates as necessary to provide certain functions of the Service, including for the purposes of order fulfilment. Such Personal Data includes shipping address, and contact information. For more details about how we ensure adequate protection when transferring your information, please see the “[Our Global Operations and Data Transfers](#global_opetations)” section below.

### **Service providers**

We share your Personal Data with third parties who provide services on our behalf or help us operate the Service. The services these third parties provide include hosting, information technology, customer support, content delivery, email delivery, order fulfilment and delivery, marketing, and website analytics. We generally require these service providers to use Personal Data only as necessary to perform the services or comply with applicable legal obligations.

### **Payment processors**

We share your Personal Data with our payment processors as necessary to complete payments, process disputes, and/or process refunds.

### **Advertising and analytics partners**

We share your Personal Data with third-party advertising, marketing, and analytics companies for the interest-based advertising and analytics purposes described in our [Cookies and Similar Technologies Policy](https://www.temu.com/cookie-and-similar-technologies-policy.html).

### **Third parties designated by you**

We share your Personal Data with third parties where you have instructed us or provided your consent to do so and where we have a legal basis for doing so. We share the Personal Data required for the services you request with third parties designated by you. For example, where you choose to share a link to a Temu product listing via a third-party social media service. Please be aware that when you use third-party sites or services, their own terms and privacy policies will govern your use of those sites or services.

### **Business and marketing partners**

We share your Personal Data with specialist third parties with whom we collaborate in order to offer or promote the Service. For example, depending on your communication preferences, we may share your Personal Data with third-party service providers with whom we have partnered in order to send you marketing SMS and email messages.

### **Professional advisors,** **public** **authorities,** **institutions,** **and regulators**

We may share your Personal Data with our professional advisors (e.g., lawyers, auditors, bankers and insurers), and public authorities, such as law enforcement authorities in response to legal processes (such as those issued by courts or authorities in your country of residence); vetted researchers, where required by applicable law, and with other parties (including financial institutions) in order to enforce our agreements or policies, protect the rights, property and safety of Temu, users and others, and to detect, prevent and address actual or suspected fraud, violations of the [Terms of Use](https://www.temu.com/terms-of-use.html), other illegal activities, security issues, or when it is required by law.

### **Business transferees**

In the rare event of a business transaction such as a merger, acquisition, or reorganisation, we may share some of your Personal Data with relevant parties (such as a buyer or successor) in order to facilitate such a transaction. If we intend to transfer information about you, we will inform you either by email or by posting a notice on the Temu mobile application and on www.temu.com.

### **Merchandise Partners**

We share with our merchandise partners the product reviews you leave, the reasons you provide to request a return or refund, and customisation information you provide for customised/personalised items. When necessary to fulfil your order, we share with a merchandise partner your name, shipping address, contact information, communications with our customer support functions, and your order information. For example, to address a query or complaint or when a merchandise partner will connect directly with logistics service providers. A merchandise partner will not receive your payment information or device data.

### **Other users**

In accordance with your settings and preferences, your Personal Data may be visible to other users. For example, if you have posted a product review of a product on Temu, other users may see your review when they use the Service.

However, if you would like to hide your profile image and profile name sharing in relation to these activities (e.g., product review) from **other users**, you can do so by editing your reviews and/or adjusting your settings and preferences in the “[Notifications](https://www.temu.com/bgp_preferences.html)” menu.

Your Rights and Choices
-----------------------

You have the following rights and choices:

### **Right to Access**

You have the right to obtain from us confirmation as to whether or not Personal Data concerning you is being processed, and, where that is the case, access to the Personal Data and certain information about the processing. Click [here](https://www.temu.com/bgp-additional-privacy-options.html) to submit a data request.

### **Right to Erasure**

You have the right to request that we delete Personal Data we maintain about you without undue delay if and to the extent that the Personal Data is no longer necessary in relation to the purposes for which it was processed, you have withdrawn your consent on which the processing is based, and where there is no other legal basis for the processing. In addition, we will erase your Personal Data if you object to the processing and there are no overriding legitimate grounds for the processing, the Personal Data have been unlawfully processed, or the Personal Data must be erased to comply with a legal obligation in the EU or Member State to which we are subject.

You can permanently delete your Temu account at any time should you no longer want to use the Service. For further details, please see [here](https://www.temu.com/bgp_close_account.html).

### **Right to Rectification**

You have the right to request that we correct inaccurate Personal Data we maintain about you. You can edit your profile information at any time through your account settings.

### **Right to Restriction of Processing**

You have the right to request that we restrict the processing of your Personal Data in any of the following circumstances: (i) if your Personal Data is inaccurate, its processing can be restricted for the period of time it takes for us to verify its accuracy; (ii) if the processing is unlawful and you object to your Personal Data being deleted, you can instead request that its processing be restricted; (iii) if we no longer need your Personal Data but you require us to retain it in order for you to bring, or defend against, a legal claim; (iv) if you have objected to our processing of your Personal Data but we rely on a legitimate interest to process it and you are awaiting the conclusion of our legitimate interest assessment.

### **Right to Data Portability**

You have the right to request that we provide the Personal Data concerning you, which you have provided to us, in a structured, commonly used and machine-readable format. This applies where the processing is based on consent or contractual necessity and the processing is carried out by automated means. Also, you have the right to have the Personal Data transmitted directly from one controller to another, where technically feasible.

### **Right to Object**

You have the right to object, on grounds relating to your particular situation, at any time to our processing of Personal Data concerning you, including profiling, which is based on a task carried out by us in the public interest or on a legitimate interest. We will no longer process the Personal Data in case of such objection unless we can demonstrate compelling legitimate grounds for the processing which override your interests, rights and freedoms or for the establishment, exercise or defence of legal claims.

### **Right to Object (Direct Marketing)**

When we process Personal Data for the purpose of direct marketing, including profiling related to such direct marketing, you have the right to object at any time to the processing of your Personal Data for this purpose. See the “[Opt-out from Marketing Communications](#Opt-out_from_Marketing_Communications)” section below for further information.

### **Right to Withdraw Consent**

You have the right to withdraw your consent to the processing of your Personal Data at any time, where processing is based on your consent. The withdrawal of consent will not affect the lawfulness of processing based on consent before its withdrawal.

### **Right to object to/opt-out of automated individual decision making**

You have the right not to be subject to a decision based solely on automated processing (i.e. an operation that is performed without any human intervention), if it produces legal effects (i.e. impacts your legal rights) or similarly significantly affects you (e.g., significantly affects your financial circumstances or ability to access essential goods or services), or to opt out of the processing of your Personal Data based on automated decision making. Temu does not make decisions based solely on automated processing that produce a legal effect or that similarly significantly affect individuals.

### **Right to Lodge a Complaint with a Supervisory Authority**

You have the right to lodge a complaint with our lead supervisory authority, the Irish Data Protection Commission or your local supervisory authority.

These rights can be limited, for example, if fulfilling your data subject rights request would reveal Personal Data about another person, if the exercise of these rights would infringe the rights of a third party (including our rights), or if your request relates to information which we are required by law or for certain reasons of public interest to keep. Relevant exemptions are included in both the GDPR and relevant local implementing legislation.

You can exercise any of these rights by submitting a request via our [web form](https://www.temu.com/bgp-additional-privacy-options.html) dedicated to data subject requests or by contacting us using the information provided below. We will not discriminate against you for exercising any of these rights. In certain circumstances, we will need to collect additional information from you to verify your identity before providing a substantive response to the request. You can also designate an authorised agent to make requests on your behalf to exercise your rights. Before accepting such a request from an agent, we will require that the agent provide proof you have authorised them to act on your behalf, and we may need you to verify your identity directly with us.

### **Opt-out from Marketing Communications**

To manage your preferences or opt-out of marketing communications, you can take any of the following actions:

* **Email Promotional Offers.** If you do not want to receive emails from us regarding special promotions or offers, you can follow the unsubscribe options at the bottom of emails to stop receiving direct marketing emails. You can also opt out of receiving promotional messages via email in the “[Notifications](https://www.temu.com/bgp_preferences.html)” menu.
    
* **Mobile Promotional Offers.** When you provide us with your mobile phone number and consent to receiving messages for marketing purposes, we will send you certain marketing alerts via text message. Standard data and messaging rates apply. If you no longer wish to receive mobile marketing alerts from us, you can follow the instructions provided in these messages. You can also opt out of receiving promotional messages via SMS in the “[Notifications](https://www.temu.com/bgp_preferences.html)” menu. You can continue to use Temu even if you stop authorising mobile promotional offers.
    
* **Push notifications.** You can receive push notifications when you use the Temu mobile application. If you wish to adjust push notification settings, including turning them off, you can do so in your mobile device’s notification settings. You can also opt out of receiving promotional messages via push notification in the “[Notifications](https://www.temu.com/bgp_preferences.html)” menu.
    

### **Change settings for cookies and similar technologies**

Most browsers let you remove or reject cookies. To do this, follow the instructions in your browser settings. Many browsers accept cookies by default until you change your settings. Please note that if you set your browser to disable cookies, the Service may not function properly. For more information about cookies and similar technologies, including how to see what cookies and similar technologies have been set on your browser and how to manage and delete them, visit [http://www.allaboutcookies.org](http://www.allaboutcookies.org/). You can also configure your device to prevent images from loading to prevent web beacons from functioning.

Our Global Operations and Data Transfers
----------------------------------------

To support our global operations:

* We store the information described in the “[What Information Do We Collect](#information_collect)” section in servers located in the EEA.
    
* Certain of our subsidiaries and affiliates, located outside the EU, EEA, and Switzerland, are given limited remote access to your Personal Data. See the “[How and Why We Share Your Information](#information_share)” section above for more information.
    
* We share your Personal Data with service providers, merchandise partners, and other parties described in the “[How and Why We Share Your Information](#information_share)” section above, which can be located outside the EU, EEA, and Switzerland. Our service providers are based in Africa, the Asia-Pacific region, Europe and North and South America.
    

When we transfer your information outside of the EU, EEA, and Switzerland, we ensure it benefits from an adequate level of data protection by relying on:

* **Adequacy decisions.** These are decisions from the European Commission under Article 45 of the GDPR (or equivalent decisions under other laws) which recognise that a country offers an adequate level of data protection. We can transfer your information detailed under the “[What Information Do We Collect](#information_collect)” section, as described in the “[How and Why We Share Your Information](#information_share)” section, to some countries with adequacy decisions, such as the countries listed [here](https://commission.europa.eu/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en?_x_sessn_id=jfed3lz68i&refer_page_name=privacy-and-cookie-policy&refer_page_id=17875_1724161992024_t7r9p3i8vk&refer_page_sn=17875); or
    
* **Standard contractual clauses.** The European Commission has approved contractual clauses under Article 46 of the GDPR that allows companies in the EEA to transfer data outside the EEA. These contractual clauses (and their approved equivalent for Switzerland) are called standard contractual clauses. We rely on standard contractual clauses to transfer information detailed under the “[What Information Do We Collect](#information_collect)” section, as described in the “[How and Why We Share Your Information](#information_share)” section, to certain affiliates and third parties in countries without adequacy decisions.
    

In certain situations, we rely on derogations provided for under applicable law to transfer information to a third country.

Click the following links to learn more about [Adequacy decisions](https://commission.europa.eu/law/law-topic/data-protection/international-dimension-data-protection/adequacy-decisions_en?_x_sessn_id=jfed3lz68i&refer_page_name=privacy-and-cookie-policy&refer_page_id=17875_1724161992024_t7r9p3i8vk&refer_page_sn=17875) (or equivalent links under applicable laws) or [Standard contractual clauses](https://commission.europa.eu/law/law-topic/data-protection/international-dimension-data-protection/standard-contractual-clauses-scc_en?_x_sessn_id=jfed3lz68i&refer_page_name=privacy-and-cookie-policy&refer_page_id=17875_1724161992024_t7r9p3i8vk&refer_pag) (or equivalent links under applicable laws).

Children
--------

The Service is not intended for use by anyone who is under the age of 18 or a minor (as defined by applicable law). If you are a parent or guardian of a child about whom you believe we have collected personal information, please contact us directly or through our [online reporting form](https://www.temu.com/bgp_delete_request.html). If we learn that we have collected personal information through the Service from a child or without the knowledge of a child’s parent or guardian as required by law, we will comply with applicable legal requirements to delete the information.

Data Security and Retention
---------------------------

The security of your Personal Data is important to us. We use technical and organisational measures to help protect your Personal Data from loss, theft, misuse, unauthorised access, disclosure, alteration, and/or destruction. We also follow the Payment Card Industry Data Security Standard (“PCI-DSS”) in handling your credit card information. However, security risk is inherent in all internet and information technologies.

We retain Personal Data for as long as necessary to fulfil the purposes described in this Privacy Policy, including to provide the Service, to comply with legal obligations, or to protect our or other’s interests. We decide how long we need information on a case-by-case basis. To determine the appropriate retention period for Personal Data, we consider factors such as the amount, nature, and sensitivity of the Personal Data, the potential risk of harm from unauthorised use or disclosure of your Personal Data, the purposes for which we process your Personal Data, and whether we can achieve those purposes through other means, and the applicable legal requirements. When we no longer require the Personal Data we have collected about you for those purposes, we delete it, or anonymise it.

The length of time we will retain your Personal Data is generally determined by how long we require it to provide you with the Service and customer support.

For example:

As described above in the “[How and Why We Process Your Information and Our Legal Bases for Processing](#information_process_and_legal_bases)” section:

* We require certain of your Account and Profile information to create and manage your Temu account. We need to keep this information for the lifetime of your account in order to maintain your account.  
    
* We retain your account preferences, including your communication preferences, which you are free to amend at any time, for the lifetime of your account.  
    
* We retain your report relating to products (including complaint records and supporting documentation) for 18 months after the complaint in question has been resolved.  
    
* If we decide to terminate the provision of the Service to you, we retain your appeal records and supporting documentation for no longer than one year after the appeal has been resolved.
    
* We retain any communication you have had with our customer support team for the lifetime of your account.
    

In certain circumstances, we need to retain your Personal Data for longer periods in order to comply with legal obligations, as described further above in the “[How and Why We Process Your Information and Our Legal Bases for Processing](#information_process_and_legal_bases)” section above. This will, in certain circumstances, involve the retention of certain of your Personal Data after your account has been deleted.

We retain your Personal Data to:

* respond to a lawful request or legal process. For example, to respond to subpoenas or requests from government authorities we may need to preserve certain of your Personal Data after your account has been deleted.
    
* address requests and complaints. For example, if there is an ongoing dispute in relation to your account, we will retain certain of your Personal Data where it is required to address the dispute or complaint.
    
* investigate issues relating to the security and safety of our services and to protect our rights, property and customers. For example, we may retain certain of your Personal Data where it is necessary to investigate, actual or potential misuse of the Service.
    

Changes to this Privacy Policy
------------------------------

We reserve the right to modify this Privacy Policy. We will notify you of any material changes to this Privacy Policy by publishing a notice on the Service or by other means. We recommend that you regularly review this Privacy Policy to stay informed of our privacy practices.

Contact Us
----------

If you have any questions or comments about our Privacy Policy or the terms mentioned, you can:

* Send an email to Temu's Data Protection Officer at privacy@eur.temu.com; or
    
* Send mail by post to Temu’s Data Protection Officer or the data controller, Whaleco Technology Limited, at the following address: Whaleco Technology Limited, First Floor, 25 St Stephens Green, Dublin 2, Ireland.
    

You also have the right to lodge a complaint with Temu’s lead supervisory authority, the Irish Data Protection Commission, or your local supervisory authority.