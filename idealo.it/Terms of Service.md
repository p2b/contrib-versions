Contatti/Termini
----------------

Si prega di notare che
----------------------

idealo.it non è un negozio online, ma un servizio di comparazione prezzi. Questo significa che da noi non è possibile:

* ordinare, prenotare o richiedere prodotti;
* informarsi sullo stato del proprio ordine, della propria prenotazione o del proprio contratto;
* informarsi sui servizi offerti da negozi o rivenditori.

Per tutte queste informazioni è necessario rivolgersi direttamente ai rispettivi negozianti.

In caso di controversia con un negozio, il consumatore può rivolgersi alla [piattaforma di risoluzione delle controversie online (piattaforma ODR)](http://ec.europa.eu/consumers/odr/).

Registrazione di un negozio
---------------------------

Per registrare un negozio sul nostro sito di comparazione prezzi, si prega di andare alla [pagina di registrazione](https://business.idealo.com/).

idealo internet GmbH  
Zimmerstraße 50  
10888 Berlino, Germania  
Tel.: +39 06 622 072 45 (Per informazioni su prodotti offerti o ordini effettuati, si prega di [contattare direttamente i negozi](https://www.idealo.it/negozi.html) che vendono il prodotto o presso i quali è stato realizzato l'acquisto.)  
Fax: +39 06 948 050 56  
[contatto@idealo.it](javascript:void(0))

Direttori amministrativi: Maxim Nohroudi, Jörn Rehse, Martin Sinner  
USt-ID (codice tedesco di identificazione IVA): DE813070905  
Registro delle imprese: HRB 76749 – Pretura di Berlino-Charlottenburg

Servizio clienti e punto di contatto unico
------------------------------------------

idealo.de è un portale di comparazione prezzi dove puoi trovare prezzi vantaggiosi, risultati di test e altre recensioni di prodotti durante la ricerca. Puoi ordinare i prodotti sul sito web del rispettivo rivenditore. 

Se hai domande sull'ordine, contatta direttamente il rivenditore.

Se hai domande sulla nostra piattaforma, puoi contattare direttamente il nostro punto di contatto unico:

**E-mail:** [contatto@idelao.it](javascript:void(location.href='mailto:'+String.fromCharCode(99,111,110,116,97,116,116,111,64,105,100,101,97,108,111,46,105,116)))

**Modulo di contatto:** [https://kundenservice.idealo.com/hc/it/requests/new](https://kundenservice.idealo.com/hc/it/requests/new)

**Al centro assistenza idealo:** [https://kundenservice.idealo.com/hc/it](https://kundenservice.idealo.com/hc/it) 

Ufficio stampa e supporto redazionale
-------------------------------------

Invio comunicati stampa a: [redazione@idealo.it](javascript:void(0))

Contatti per la stampa: [stampa@idealo.it](javascript:void(0))

Responsabile
------------

Jörn Rehse  
Zimmerstraße 50  
10888 Berlino, Germania

Responsabile tutela dei minori
------------------------------

John Strassburg [privacy@idealo.it](javascript:void(0))

Responsabile privacy
--------------------

[privacy@idealo.it](javascript:void(0))

Segnaliamo espressamente che se utilizzate questo indirizzo e-mail, il contenuto non sarà esclusivamente annotato dal nostro responsabile della protezione dei dati. Se desiderate scambiare informazioni confidenziali, vi chiediamo quindi di contattarci prima direttamente tramite questo indirizzo e-mail.

Supporto tecnico
----------------

Tel.: +39 06 622 072 45  
[supporto.tecnico@idealo.it](javascript:void(0))

Promozione e pubblicità su idealo.it
------------------------------------

[commerciale@idealo.it](javascript:void(0))

Regolamento sui Servizi Digital (DSA)
-------------------------------------

Punto di contatto unico per le comunicazioni con le autorità degli Stati membri, la Commissione e il Comitato Europeo per i Servizi Digitali ai sensi dell'articolo 11 del Regolamento sui Servizi Digitali: dsa-authorities@idealo.it. La comunicazione è possibile in inglese e tedesco. 

Si prega di tenere in considerazione che questo punto di contatto unico è destinato esclusivamente alla comunicazione con le suddette organizzazioni in relazione all'applicazione del Regolamento sui Servizi Digitali. Le richieste di altre persone o organizzazioni non possono essere trattate tramite questo punto di contatto. 

Informazioni sulla media dei destinatari attivi mensili dei nostri servizi portale di comparazione prezzi nell'Unione europea sono disponibili [qui](https://www.idealo.de/unternehmen/digital-services-act).

Informazioni sul funzionamento e l’efficacia del sistema interno di gestione dei reclami ai sensi dell'Art. 11 del Regolamento (UE) 2019/1150
---------------------------------------------------------------------------------------------------------------------------------------------

Nel periodo dal 1° gennaio 2024 al 31 dicembre 2024, idealo internet GmbH ha ricevuto complessivamente 56 reclami da parte degli utenti commerciali registrati presso idealo. Questi riguardavano principalmente l'elaborazione dei pagamenti e le sanzioni o le misure restrittive imposte da idealo (ad es. cancellazione del rapporto contrattuale). Alla fine del periodo di riferimento, un (1) caso era ancora in fase di elaborazione. Di norma, i reclami sono stati risolti in otto (8) giornate lavorative.

Sicurezza
---------

idealo prende molto sul serio la sicurezza delle informazioni. Per qualsiasi osservazione sulla sicurezza tecnica, contatta security@idealo.de. Quando si trasmettono informazioni sensibili, consigliamo di crittografare il messaggio con la nostra Public Key.

\-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBF87aQsBEAD0mO8FIrW16mfX5D1uC6KFAz1ztGbYSYV/xIfq0cI7EGySCgG/ 2z8TDB7dD4yNQCJad7II4EsdimGYFYFxoOZariZyjZQv8wNLmRvBL6MXjkf7DiN7 K/sIKlSfu+XC+fzPKW2/z597rDHlzUdLPfwrCeAFhU+B6I/BKQ9ydLyo6fxn29xZ uvK4+DdohPwdj4AYFbluEnJjNXUJb+hb8l3zP8uG7Xm5FCoKqJLc3TGj4oAWvwWl KDHWCGn1UKQ6u0Mp6/oxr7ojc3sKPdPmP9ZhC4kr2WZp7wBLBusn3S8ln6X6SByc qoyykaMs8G31/ZHP8hjuNKyT7HwY78z65SLrPjVTPeAlJjfaDxeg/yptVrPmszt8 oSiV6CkXDQtnIKFo3vZpzPsxax3V8TYw8KikWJzu2rNmBrtC0w+QYJ9sWfiQYM5B yjERwAN1UMadWJ1QoRZQIdkzimQtynHGMNDlwRCNfds5OObFN+7qz0ruE8XBnTBe az0vFJSlU3e5VvKLAraTGAHcBB9T5UvGa17w5yACf/cxsZ19GcR1TF4pftJtfM1h ldXElkonlmf5sMzD/6J/A5Je5mbgqJuKouEGx4l6v0kwIFHRSqUaARDE4VByafAY k0l1/Dg+74jShLg7CNz4Mdj3UscnQOvopHsVdTUAFASq3Bul9dUuMHn1PwARAQAB tClpZGVhbG8gU2VjdXJpdHkgVGVhbSA8c2VjdXJpdHlAaWRlYWxvLmRlPokCVAQT AQgAPhYhBBIfTVJe0FkyGFvwe4UAZW4AbE5YBQJfO2kLAhsDBQkJZgGABQsJCAcC BhUKCQgLAgQWAgMBAh4BAheAAAoJEIUAZW4AbE5YXfQP/RJXAk0fE+3bcM7DYBCR fQGnlh+HKw5+tvfI86sL1qFcHohWqMN5jRV43sUijqZ3GQ1Km2YShgdGqRXOw7bN Dq7r8nEIDi2TyCQFUmuk+8fu5h7hG7s9Pl3MhSxACgjIzM0Pv2cmUSzQsnF4lu8G I7Tb5fciKD90Iwu0CFMwBsosub0sj0r/xbCqsa4cq5aoYtbJSxQPsTFmYc0ojjnV wO4jYaABQOUgB3xkWkZX7xASqvTVLJ7QxVmw2AIGvutr4Clo8GICJu8Z+SVryql1 yQbLciZDWi+lcH1fBfDpmhx2QqE3qUJ/YR3Nhu66eojx41FraSXAQ52ebSzdYcd0 KsYqRRfuwK4LluGdlmJA/zIZv7CvoT/iZjC5iuGtuwAw3MRfIxtuY4WUDEP2iSy9 YkZbs1pE37cK/i5+PEViRVIqmGcnalT6ktQVSjMm2DN5aXwfFnRDsyoe2w5xKs9+ ZdiUzMvmtfT/nPNSDbfQqtNO81RpMt2WZRk++mhsp/3hdpTS+xhaffY8fO6MJ+qT 8UHEAsfJ9IWOKDLq9CkS+OSe0jIihKYXS4BC9vQL01w9Xf/Nu50XHvNu59yZr1Sl dUxUtmFbLofn2uRTS8CMg79XiaBVJLTQZsiAu8VelcepC7+I1VOK8VJHlObYGh88 w1KMVkHIU74UFAJhOxo4X/HguQINBF87aQsBEADAZlw+/XnqFhTVtCsEJhRqj2xY UOz66BKCSDi244uXEmBWnYZMJnk3y++7nfOsMU1fyhM0aF8WqsbLexLGZg056Gkl kYso2RLeIikcsObAcKMm83Z4aZVLFe97L8ffN4RGpE+d6+eJfAQdYDluSm4S9gUU 5O8DtBP44QC4BIMmWSITtiLTqM3N5HbWXtBjKMVTpM2W8elOhpFvi3r/lbG3h7KL kpBILmuImOlcmsPeA+YeXcu27mYPPhtFv+MUS+VctERg/YpVZOOFnvq6YLoADcf7 9iucd4cVZfXdGJU7pWrCKMLjLrgCmIw+JjJCW/qezG5RpYWMHYh3oY0iV0gYJDSc iOkMhmezNExqLTQZk2+ZEY6ewFj4gD8qr1LYBsqGnwqQAsqAcErrIoDeCWk1y9/n xa6jopahzpdKtD4n0RDuz53IXOsy+wkMQ+oyRbnjqqD44r0G+rJdHNw/rTk2iDqM Itkai/t4wCfZGQ64OnORrdrUJepT0RGKOQ1WMczQwlTg+0IyXUKq9CYh8+kpEs20 ZOr3qAUz10Y6ANk7pifDDSSqkVPyCjlhxQrSVcz23jDcNOU6UbmFOvz+2Mjjhs4L tn2j6A78TRZifmXFv5P0YYBhtQd0ilXF0K+xZihcTDXvBlPIkY7XnAtOUWKdYpe6 087c5+w1EYDpT/Mj1QARAQABiQI8BBgBCAAmFiEEEh9NUl7QWTIYW/B7hQBlbgBs TlgFAl87aQsCGwwFCQlmAYAACgkQhQBlbgBsTlg9Uw//bZM9i02Hj/4+l49jHWvC 64JJE8usS/PST1BeK34/RDVGjWCsuJ6VWbPFWr1hUyrArcOgwVhEa9HKC6Tqv8TH QwiRgWQ+H0qDUKSsoKw3XrQBsg+YY7+AJCYmgomEqG4BK66/nX3h6UahU4RStayy /MVW5dzX5HA+RnctK9hA1u0fQZzwZx6VCYkTE4AlGnCc4FS6p67dmOhjug5RJuOg Qax89GJz3srU5gXU+OPrq2BaG7+UhjCXj8OQge+XGdt3qbkHtN3iGeH2R3Oq7jjz sESgL6J/teRP89IkP6YktAxKZhg/WzgqYEHVT19cL+OynGWzCvsD4EbB1CtsIuUw XEUjh2DDVZ28PCRREK8p6RoEIZBLn/2p1R7cw5ujzQrsvSl3QtZJC8uF8IJhzRZv WXpYfXiVR6MLBUJemcB/DQnB55v6ot8hR+1Wq9cwfGS70q023AOVKRhogw/zyGJf svE2KTqmvSK8uhbgJCJ52KZpS1Ppvt39pGb+bhu2M+5/dws+1eWB8h5/r+XY/b8v KUwNn9hh7vWgrpnKqrYGY5fLEThLbjOd1HQ8IJsLWIzPhsxxQjHs4QSgZ/CI+voY x4evFRJWTXIEfVE0M4V/+vagAMdjgrADWYcH31WiXK9ZEQNTx5riDC2zA2JVAMJ9 Xy+KOsLD0XFjnPZeFHNyEus= =Ids7

\-----END PGP PUBLIC KEY BLOCK-----

Protezione dei dati personali
-----------------------------

L'informativa sulla privacy di idealo è disponibile [qui](https://www.idealo.it/informativa-privacy.html).

**Condizioni d’uso dei portali di comparazione prezzi di idealo** 
------------------------------------------------------------------

idealo internet GmbH, Zimmerstraße 50, 10888 Berlino (di seguito "idealo") gestisce diversi portali di comparazione prezzi (di seguito "portali di comparazione").  Ciò avviene tramite diversi siti web e applicazioni mobili. 

### **1\. Ambito di applicazione, modifiche** 

1.1 Le seguenti condizioni d'uso (di seguito denominate "Condizioni d’uso") si applicano all'utilizzo dei servizi offerti da idealo via 

* i domini idealo.it 
    
* applicazioni mobili associate. 
    

Questo include l'uso dei servizi offerti (di seguito collettivamente "idealo servizi"). 

1.2 I portali di comparazione offerti da idealo includono: 

* "idealo Shopping" e 
    
* "idealo Voli". 
    

1.3 Oltre alle presenti Condizioni d'uso, per la pubblicazione di offerte di prodotti o servizi su idealo da parte di utenti commerciali valgono le Condizioni Generali di idealo (di seguito "CG di idealo"), che possono essere consultate, stampate e/o salvate tramite idealo Business. 

1.4 Le condizioni d'uso si applicano nella versione inclusa nel rispettivo contratto con idealo al momento della stipula del contratto. Ciò comprende, in particolare, la stipula di un contratto di utilizzo al momento della registrazione del conto cliente idealo (cfr. punto 4) o l'invio di una recensione (cfr. punto 6). 

1.5 idealo ha il diritto di modificare le condizioni d'uso. Tuttavia, le modifiche del rapporto contrattuale in corso con una persona registrata possono avvenire soltanto 

* nella misura in cui la persona affetta non si trovino in una posizione peggiore a seguito delle modifiche, o 
    
* nella misura in cui ciò sia necessario per eliminare le lacune normative emerse dopo la stipula del contratto, sia a causa di una mutata situazione giuridica o di sentenze di tribunali supremi, sia per l'ulteriore sviluppo dei servizi offerti da idealo, oppure 
    
* nella misura in cui le disposizioni essenziali del rapporto contrattuale non siano influenzate dalle modifiche e nella misura in cui queste siano necessarie o opportune per adattarsi agli sviluppi, in particolare alle modifiche tecniche, legali o normative, ai cambiamenti delle circostanze sul mercato o per ragioni equivalenti. 
    

Le disposizioni rilevanti del rapporto contrattuale sono, ad esempio, la clausola 5 (Avviso di prezzo), la clausola 6.8 (contributi degli utenti) e la clausola 9 (responsabilità, limitazione di responsabilità) delle condizioni d'uso. 

1.6 Le modifiche alle condizioni d'uso nel rapporto contrattuale in corso saranno comunicate alle persone registrate via e-mail o al successivo login. Esse si considerano accettate in ogni caso se 

* l'interessato non si oppone entro quattro (4) settimane dal ricevimento dell'avviso di modifica e 
    
* idealo ha evidenziato questa conseguenza legale nella notifica. 
    

L'obiezione deve essere formulata in forma testuale, vale a dire almeno per e-mail. Se una persona si oppone, il contratto d'utilizzo continua a valere alle condizioni precedenti senza le modifiche. In questo caso idealo si riserva il diritto di recedere dal contratto d'utilizzo secondo quanto previsto al punto 4.8 delle condizioni d’uso. 

### **2\. Comparazione dei prezzi e ricerca dei prodotti** 

2.1 I portali di comparazione si rivolgono a persone fisiche (di seguito denominate "utenti"). idealo offre i propri servizi solo a utenti maggiorenni e con capacità giuridica illimitata. 

2.2 Sui portali di comparazione gli utenti hanno la possibilità di ricercare senza impegno beni o servizi (di seguito denominati collettivamente "prodotti") e di ottenere informazioni sui prezzi, sulle fonti di approvvigionamento e su altre caratteristiche. Inoltre, idealo offre ulteriori servizi nell'ambito della comparazione di prodotti e prezzi. 

2.3 L'utilizzo dei portali di comparazione è gratuito. 

2.4 I prodotti presentati sui portali di comparazione sono offerti da terzi. I terzi sono in particolare rivenditori online e altri fornitori di prodotti su Internet (di seguito "fornitori"). 

2.5 La ricerca di informazioni sui portali di comparazione è in gran parte automatizzata. Si basa in gran parte sulle informazioni che idealo riceve dai partner. I partner di idealo sono principalmente fornitori e produttori di prodotti, marketplaces e servizi di intermediazione. Le offerte di prodotti presentante vengono aggiornate automaticamente e di solito più volte al giorno. Le informazioni sui portali di comparazione e sui portali dei fornitori possono differire a seconda dei casi. Ciò può riguardare in particolare le seguenti caratteristiche: 

* il prezzo (e quindi anche la classifica delle offerte), 
    
* la disponibilità, 
    
* i costi di consegna o 
    
* i tempi di consegna dei prodotti. 
    

2.6 idealo utilizza diversi sistemi di raccomandazione per supportare gli utenti nella ricerca di offerte di prodotti. In questo modo, idealo mostra le raccomandazioni che ritiene possano essere di interesse per gli utenti. Per fornire raccomandazioni il più possibile personalizzate, idealo genera raccomandazioni basate su diversi parametri. I dettagli sui sistemi di raccomandazione, i parametri principali dei sistemi di raccomandazione e la loro ponderazione relativa sono disponibili [qui](https://www.idealo.it/sconti/ranking). 

2.7 Nei portali di comparazione non vengono presi in considerazione tutti i fornitori e le offerte presenti sul mercato. Le informazioni presentante sui prodotti hanno lo scopo di fornire una panoramica delle offerte dei fornitori che collaborano con idealo. Non hanno la pretesa di essere complete. idealo ha il diritto di rimuovere i prodotti dai suoi utenti in qualsiasi momento e senza preavviso. 

2.8 idealo è autorizzata a modificare, integrare, cancellare o interrompere parti dei servizi, l'insieme dei servizi e/o singole funzionalità. 

Questo può accadere, ad esempio, 

* per attuare una sentenza del tribunale e/o una decisione amministrativa, 
    
* in caso di modifiche legali, 
    
* per colmare le lacune nella sicurezza, o 
    
* per ampliare o limitare l'offerta. 
    

Ciò può avvenire anche senza preavviso. La clausola 1.5 rimane inalterata. 

### **3\. Relazioni contrattuali** 

3.1 Gli utenti interessati all'acquisto di un prodotto vengono solitamente indirizzati al portale del fornitore da cui proviene l'offerta in questione. Gli utenti sono tenuti a verificare tutte le informazioni rilevanti prima di ordinare un prodotto. Ciò comprende anche l'attualità delle informazioni, in particolare dei prezzi. idealo segnala quanto segue: gli utenti che effettuano pagamenti anticipati ai fornitori (ad es. pagamento anticipato) si assumono generalmente il rischio di insolvenza. 

3.2 I contratti per l'acquisto di prodotti vengono stipulati esclusivamente tra utenti e fornitori. A tali contratti possono quindi applicarsi le condizioni generali di contratto dei fornitori. In qualità di portale di confronto, idealo non offre alcun prodotto da acquistare. Pertanto, con idealo non vengono stipulati contratti di acquisto, di servizio o di altro tipo per l'acquisto di prodotti esposti. 

3.3 idealo non garantisce che i prodotti presentati sui portali di comparazione possano essere effettivamente ordinati o acquistati dai fornitori alle condizioni indicate. idealo non è inoltre tenuta a garantire l'adempimento di tali contratti. Tutti gli obblighi contrattuali derivanti da e in relazione a tali contratti intercorrono esclusivamente tra utenti e fornitori. 

3.4 idealo e i fornitori gestiscono i rispettivi servizi in modo indipendente l'uno dall'altro. idealo non ha alcun controllo sul contenuto o sull'accessibilità dei servizi di terzi. 

### **4\. Registrazione dell'account cliente ("Il mio idealo")** 

4.1 In linea di principio, l'utilizzo dei portali di comparazione è possibile senza registrazione. 

4.2 Gli utenti hanno la possibilità di aprire un proprio conto cliente ("Il mio idealo"). Questo conto cliente gratuito offre agli utenti ulteriori funzioni e servizi. Tra questi vi sono: 

* l'Avviso di prezzo (cfr. punto 5), 
    
* preferiti personali (i prodotti a cui gli utenti sono interessati e il loro andamento del prezzo possono essere "annotati" con un semplice clic e visualizzati come elenco), 
    
* raccomandazioni personalizzate, occasioni e informazioni sulle attuali variazioni di prezzo dei prodotti per i quali gli utenti hanno mostrato interesse sul portale di confronto. 
    

4.3 La fornitura dell'opzione di registrazione del conto cliente non costituisce un obbligo di accettazione da parte di idealo. Si tratta di un invito a presentare un'offerta per l'apertura di un conto cliente. Per registrarsi, l'utente deve indicare il proprio indirizzo e-mail, la password ed eventualmente ulteriori informazioni. Dopo la registrazione, idealo invia una e-mail all'indirizzo e-mail indicato. Questa contiene un link che deve essere cliccato per confermare la registrazione (la cosiddetta procedura double opt-in). L'accettazione della registrazione con la creazione di un conto cliente è soggetta a verifica da parte di idealo. Le funzioni e i servizi inclusi nel conto cliente sono gratuiti per gli utenti. Non sono vincolanti per idealo. 

4.4 Gli utenti sono tenuti a utilizzare l'opzione di registrazione solo per sé stessi. Soprattutto non è consentito l'utilizzo di identità altrui. Ciò vale anche per l'uso non autorizzato di dati di terzi. 

4.5 Gli utenti sono gli unici responsabili della sicurezza dei loro dati di accesso. In particolare, è tenuto a mantenere segreta la propria password e a non trasmetterla a terzi. Anche idealo non trasmetterà le password a terzi. Gli utenti devono scegliere una password sufficientemente sicura e cambiarla regolarmente. Ciò serve a garantire la sicurezza e ad evitare un uso improprio del conto cliente. 

4\. 6 idealo ha il diritto di disabilitare temporaneamente o definitivamente l'accesso ai conti dei clienti. Ciò può accadere se vi sono indicazioni che i conti dei clienti vengono utilizzati in modo tale che 

* appaia destinato o idoneo a compromettere la funzionalità dei servizi o 
    
* commettere o promuovere reati. 
    

idealo decide a sua ragionevole discrezione in merito all'esistenza di indicazioni corrispondenti. idealo informerà gli utenti interessati di una disabilitazione e darà loro la possibilità di rispondere. 

4.7 Gli utenti possono chiedere a idealo di cancellare il proprio account cliente in qualsiasi momento. In questo modo si estingue il relativo contratto d'utilizzo con idealo. La richiesta di cancellazione può essere inviata via e-mail a: cancellaaccount@idealo.it. La cancellazione deve essere richiesta sempre dall'indirizzo e-mail con il quale l'utente ha registrato il proprio account. 

4.8 idealo ha il diritto di rescindere i contratti d'utilizzo in qualsiasi momento inviando una comunicazione in forma di testo via e-mail. 

### **5\. Avviso di prezzo (solo per utenti registrati e idealo Shopping)** 

5.1 Nell'ambito di idealo Shopping, idealo offre agli utenti la possibilità di impostare senza impegno un cosiddetto avviso di prezzo per determinati prodotti. Questa opzione è disponibile solo per gli utenti registrati e per gli utenti dell'app idealo Shopping. Con l'aiuto dell'avviso di prezzo gli utenti possono stabilire un prezzo desiderato per i prodotti selezionati. Se il prezzo desiderato viene raggiunto o superato da nuove offerte, gli utenti possono essere avvisati da idealo.  La notifica è gratuita e non vincolante. Gli utenti che hanno installato l'idealo Shopping App vengono avvisati tramite notifica push. Gli utenti registrati vengono avvisati anche via e-mail. 

5.2 idealo non garantisce che gli utenti vengano informati tempestivamente delle variazioni di prezzo. idealo non garantisce nemmeno che un prodotto venga effettivamente offerto a un determinato prezzo per un determinato periodo di tempo. L'avviso di prezzo serve solo come supporto informativo non vincolante. In nessun caso una notifica costituisce una dichiarazione di volontà di stipulare un contratto. Questo vale sia per idealo che per il rispettivo fornitore. È responsabilità dell'utente verificare che i prezzi siano aggiornati prima di effettuare un ordine. 

5.3 Se il prezzo desiderato non viene raggiunto entro un periodo di tempo più lungo, idealo può disattivare senza preavviso l’avviso di prezzo per il prodotto in questione.  L'utente ha quindi la possibilità di impostare nuovamente l’avviso di prezzo per il prodotto in questione. L'utente può inoltre modificare in qualsiasi momento le impostazioni del proprio avviso di prezzo. 

5.4 Se gli utenti non desiderano più essere avvisati su un prodotto, possono cancellare l'avviso di prezzo tramite il proprio account cliente o l'app idealo shopping. 

### **6\. Contributi degli utenti** 

6.1 In alcuni casi, idealo offre agli utenti dei portali di comparazione la possibilità di commentare i fornitori elencati. Ciò può avvenire tramite contributi testuali e stelle (di seguito denominate collettivamente "recensioni"). Una stella rappresenta la valutazione più bassa e cinque stelle quella più alta.  Le recensioni possono essere visualizzate da tutti i visitatori del rispettivo portale di comparazione. Possono essere visualizzate anche dai fornitori. I fornitori hanno la possibilità di rispondere alle recensioni scrivendo un commento (di seguito "commento"). Il commento appare sotto la recensione. 

6.2 Gli utenti possono inviare recensioni solo se sono o sono stati effettivamente clienti del fornitore da recensire. In particolare, non possono inviare recensioni se sono proprietari del fornitore oggetto della recensione, familiari o dipendenti del fornitore, o se hanno ricevuto o gli è stato promesso un compenso per una recensione dal fornitore o da un rappresentante autorizzato del fornitore. Ciò include sconti, buoni, benefici in denaro, regali, punti fedeltà e bonus di segnalazione. 

6.3 idealo non controlla se le recensioni provengono da consumatori che hanno effettivamente utilizzato o acquistato i beni o i servizi valutati, a meno che la recensione non sia contrassegnata come "opinione verificata". Ulteriori informazioni su questo processo di verifica sono disponibili nelle pagine di dettaglio dei prodotti e sul sito  [Opinioni di Cimenio su idealo](https://www.idealo.it/sconti/reviews). 

6.4 Gli utenti sono tenuti a fornire il numero di cliente o di ordine autentico quando valutano un fornitore. Il numero viene poi trasmesso al fornitore. In questo modo il fornitore ha la possibilità di collegare la recensione al rispettivo processo d'ordine e di identificare l'utente. Il numero del cliente o dell'ordine non viene pubblicato all'interno della recensione. I fornitori possono inviare le loro obiezioni giustificate alle recensioni a contatto@idealo.it. 

6.5 Gli utenti sono tenuti a rispettare le disposizioni di legge, le presenti condizioni d'uso e le linee guida sulla valutazione di idealo. Queste ultime possono essere consultate [qui](https://kundenservice.idealo.com/hc/it/articles/17043859697170). Lo stesso obbligo vale per i fornitori quando inviano commenti. In conformità con le leggi vigenti, le recensioni e i commenti devono essere dimostrabilmente veritieri, laddove riportano fatti o contengono un nucleo di fatti. Non possono contenere insulti o critiche offensive o essere illegali per altri motivi. idealo si riserva il diritto di richiedere agli utenti e ai fornitori la prova del rispetto di questi requisiti e di stabilire in qualsiasi momento ulteriori condizioni per l'invio di recensioni e commenti. La legalità delle recensioni e dei commenti inviati prima dell'entrata in vigore dei nuovi requisiti rimane inalterata. 

6.6 idealo si riserva il diritto di limitare la visualizzazione di recensioni e commenti o di rimuovere recensioni e commenti che, a ragionevole giudizio di idealo, violino i requisiti della sezione 6.5 o le linee guida sulla valutazione di idealo. idealo informerà gli utenti e i fornitori se la loro recensione o il loro commento è interessato da tale decisione di moderazione.  Gli utenti saranno informati via e-mail all'indirizzo di posta elettronica fornito al momento dell'invio della recensione. Ulteriori informazioni sulla moderazione dei contenuti sono disponibili nella sezione 7 delle presenti Condizioni d'uso. Gli utenti e i fornitori direttamente interessati da una decisione di moderazione hanno accesso al sistema interno di gestione dei reclami di idealo. Ulteriori informazioni sul sistema interno di gestione dei reclami di idealo sono disponibili nella sezione 8 delle presenti Condizioni d'uso. 

6.7 Se gli utenti creano recensioni frequenti e manifestamente illegali, idealo sospenderà l'uso del servizio di recensione per questi utenti per un periodo di tempo ragionevole, previo avviso. Nel decidere la sospensione e, se del caso, la durata del blocco, idealo terrà conto di tutti i fatti e le circostanze di cui idealo è a conoscenza. Questi includono in particolare (i) il numero assoluto di recensioni manifestamente illegali inviate in un determinato periodo di tempo, (ii) la loro quota relativa rispetto al numero totale di recensioni inviate durante tale periodo, (iii) il tipo di contenuto illegale e la gravità delle accuse associate, comprese le conseguenze per coloro che sono stati colpiti dalla recensione, e (iv) le intenzioni perseguite con la recensione in ciascun caso, se riconoscibili. 

6.8 Pubblicando recensioni, compresi i contributi testuali, gli utenti concedono a idealo il diritto non esclusivo, gratuito e senza limiti spaziali e temporali di utilizzarle online, offline, su tutti i media e attraverso tutti i canali di distribuzione. Ciò include anche 

* copia, 
    
* pubblicazione, 
    
* visualizzazione e distribuzione, 
    
* salvataggio, 
    
* archiviazione, 
    
* messa a disposizione di terzi, 
    

* messa a disposizione del pubblico e riproduzione, nonché 
    
* integrazione in altre opere. 
    

Ciò include anche i diritti di modificare e tradurre le recensioni.  Ciò vale solo nella misura in cui l'adattamento o la modifica preservino il carattere intellettuale dell'opera. 

6.9 La concessione dei diritti di cui sopra autorizza idealo, in particolare, a pubblicare recensioni sui portali di comparazione. idealo può anche utilizzare le recensioni per pubblicizzare i portali di comparazione, i prodotti presentati o i fornitori. Inoltre, idealo può autorizzare i fornitori a utilizzare le recensioni che li riguardano a fini pubblicitari. La portata e il contenuto dei diritti d'uso concessi sono limitati in ogni caso a quanto necessario 

* per il funzionamento e/o la pubblicità dei portali di comparazione di idealo oppure 
    
* la pubblicità dei prodotti e/o dei fornitori valutati. 
    

6.10 Non esiste alcun diritto all'utilizzo del sistema di recensione o di pubblicazione delle recensioni. In relazione agli utenti, idealo ha il diritto di limitare in qualsiasi momento la visualizzazione delle recensioni che idealo, a sua discrezione, ritiene irrilevanti per gli utenti. Questo vale in particolare per le recensioni che non sono più aggiornate, che si riferiscono a prodotti che non sono più presenti nel portale di comparazione o se il fornitore valutato ha cambiato la propria ragione sociale dopo aver inviato la recensione. idealo ha inoltre il diritto di modificare, integrare e/o interrompere in qualsiasi momento parti o l'intero sistema de recensione. Il rispettivo avviso non è necessario e di regola non viene effettuato. 

### **7\. Moderazione dei contenuti e meccanismo di segnalazione** 

7.1 I contenuti pubblicati da fornitori e utenti sui portali di comparazione non devono violare le norme di legge, i diritti di terzi e, se concordato in singoli casi, le presenti Condizioni d'uso, le Condizioni Generali di idealo e le linee guida di idealo (di seguito denominate congiuntamente "Condizioni contrattuali"). Ciò vale in particolare per le offerte di prodotti e le recensioni dei fornitori di prodotti. 

7.2 idealo non ha l'obbligo generale di monitorare i contenuti trasmessi da fornitori e utenti e memorizzati per loro conto, né di ricercare attivamente circostanze che indichino attività illegali o l'incompatibilità di tali contenuti con le norme di legge, i diritti di terzi o le condizioni contrattuali applicabili. 

7.3 Tuttavia, idealo si riserva il diritto di esaminare volontariamente i contenuti di propria iniziativa e a propria discrezione al fine di identificare, determinare e rimuovere o disabilitare l'accesso a contenuti illegali o che violano i termini e le condizioni contrattuali applicabili, nonché di adottare tutte le misure necessarie in conformità con i termini e le condizioni contrattuali applicabili. 

7.4 Le persone e gli enti hanno inoltre la possibilità di segnalare i contenuti pubblicati sui portali di comparazione che ritengono illegali. Questo vale in particolare per le offerte di prodotti pubblicate dai fornitori sui portali di comparazione e per le recensioni pubblicate dagli utenti sui portali di comparazione. idealo ha predisposto un meccanismo di segnalazione elettronica per la segnalazione di presunti contenuti illegali. Ulteriori informazioni sulla segnalazione di contenuti illegali sono contenute nelle linee guida di idealo sulla procedura di segnalazione. Queste possono essere consultate [qui](https://kundenservice.idealo.com/hc/it/articles/17019773847698). 

7.5 Nell'ambito delle indagini volontarie di propria iniziativa e dell'esame delle segnalazioni di contenuti, idealo utilizza diverse procedure e strumenti per riconoscere, esaminare e moderare i contenuti. Tali procedure possono includere la revisione manuale, la revisione automatica o una combinazione di entrambe. Ulteriori informazioni sui processi e gli strumenti utilizzati per moderare i contenuti sono disponibili nelle nostre linee guida per la moderazione dei contenuti. Puoi consultarla qui. 

7.6 idealo è autorizzata a sospendere il trattamento delle segnalazioni nei confronti di singole persone o entità per un periodo di tempo ragionevole, previo avvertimento, se queste presentano frequentemente segnalazioni manifestamente infondate. Nel decidere la sospensione e, se del caso, la durata del blocco, idealo terrà conto di tutti i fatti e le circostanze di cui idealo è a conoscenza. In particolare (i) il numero assoluto di segnalazioni manifestamente infondate inviate in un determinato periodo, (ii) la loro quota relativa rispetto al numero totale di segnalazioni inviate durante tale periodo, (iii) la natura delle segnalazioni e la gravità delle accuse associate, comprese le conseguenze per gli altri utenti, e (iv) le intenzioni perseguite dalla persona o dall'ente segnalante, se riconoscibili. 

### **8\. Sistema interno di gestione dei reclami** 

8.1 idealo ha istituito un sistema elettronico e gratuito di gestione dei reclami interni (di seguito "SIGR") per i reclami degli utenti contro determinate misure adottate da idealo. 

8.2 Possono accedere all'SIGR le persone e gli enti che hanno segnalato un presunto contenuto illegale tramite il meccanismo di segnalazione elettronica (vedi Sezione 7 delle presenti Condizioni d'uso) e che non sono d'accordo con la decisione relativa al contenuto segnalato. Hanno accesso all'SIGR anche i fornitori e gli utenti che sono direttamente interessati da una decisione di moderazione, in particolare ai sensi della sezione 6.6 delle presenti Condizioni d'uso. 

8.3 La possibilità di presentare un reclamo esiste per un periodo di sei mesi dalla data della decisione. Ulteriori informazioni sulla procedura di reclamo sono contenute nelle linee guida sulla procedura per il sistema interno di gestione dei reclami (SIGR) di idealo. Queste possono essere consultate [qui](https://kundenservice.idealo.com/hc/it/articles/17033300378130). 

8.4 idealo ha il diritto di sospendere il trattamento dei reclami nei confronti di singole persone o ente per un periodo di tempo ragionevole, previo avvertimento, se queste presentano con frequenza reclami manifestamente infondati. Nel decidere la sospensione e, se del caso, la durata della sospensione, idealo prende in considerazione tutti i fatti e le circostanze di cui idealo è a conoscenza. Ciò include in particolare (i) il numero assoluto di reclami manifestamente infondati presentati in un determinato periodo di tempo, (ii) la loro quota relativa rispetto al numero totale di reclami presentati durante tale periodo, (iii) la natura dei reclami e la gravità delle relative accuse, comprese le conseguenze per altri utenti o altri terzi, e (iv) le intenzioni perseguite dalla persona o dall'ente segnalante, se riconoscibili. 

### **9\. Responsabilità, limitazione di responsabilità** 

9.1 idealo si impegna a presentare tutte le informazioni fornite nell'ambito dei portali di comparazione in modo aggiornato e privo di errori. Tuttavia, a causa della quantità di informazioni da elaborare, non si possono escludere errori nella presentazione. idealo non si assume pertanto alcuna responsabilità per la correttezza, l'attualità, la completezza e la qualità delle informazioni fornite dai partner. 

9.2 Per quanto riguarda la responsabilità di idealo per i danni arrecati, indipendentemente dai motivi giuridici, valgono le seguenti esclusioni e limitazioni di responsabilità, fatti salvi gli altri presupposti giuridici per le richieste di risarcimento: 

a. idealo risponde per semplice negligenza secondo il diritto tedesco solo in caso di violazione di un obbligo il cui adempimento è essenziale per la corretta esecuzione del contratto e sulla cui osservanza gli utenti possono regolarmente fare affidamento (cosiddetto obbligo cardinale \[“Kardinalpflicht”\]).  Nel caso in cui idealo risponda per semplice negligenza, la responsabilità è limitata al danno che si può tipicamente prevedere che si verifichi in base alle circostanze note al momento della stipula del contratto. 

b. Per il resto, idealo non è responsabile per i danni di alcun tipo che derivano da una violazione degli obblighi per semplice negligenza. 

9.3 Le disposizioni di cui sopra si applicano anche alle richieste di rimborso delle spese. 

9.4 Le disposizioni di cui sopra valgono anche per i dipendenti, gli ausiliari e gli altri terzi di cui idealo si avvale per l'esecuzione del contratto. 

9.5 Tuttavia, le disposizioni di cui sopra non si applicano in caso di responsabilità legale obbligatoria, in particolare 

* in conformità con la legge tedesca sulla responsabilità del prodotto \[“Produkthaftungsgesetz”\], 
    
* in caso di assunzione di una garanzia e 
    
* in caso di lesioni colpose alla vita, all'integrità fisica o alla salute. 
    

In questi casi, idealo è responsabile anche per semplice negligenza. 

### **10\. Riferimenti ai diritti di proprietà intellettuale** 

10.1 I portali di comparazione e i dati, i risultati della ricerca, i testi, i grafici e le altre informazioni in essi contenuti sono in parte protetti da diritto d'autore. Lo stesso vale per il software e la banca dati utilizzati per la fornitura dei servizi. La messa a disposizione dei portali di comparazione per l'utilizzo nell'ambito delle presenti condizioni d'uso non costituisce una rinuncia ai diritti d'autore o una concessione di diritti di utilizzo ai sensi della legge tedesca sul diritto d'autore.  

10.2 Gli utenti possono utilizzare i portali di comparazione solo per un confronto mirato e individuale di informazioni e offerte.  In particolare, gli utenti possono accedere alla banca dati sottostante solo tramite l'opzione di accesso fornita da idealo e possono utilizzare i dati solo per uso personale. Non è consentito leggere o spiare la banca dati con altri software. In particolare, gli utenti non possono accedere automaticamente ai portali di comparazione. Ciò include in particolare lo scraping. 

10.3 Gli utenti possono stampare, copiare o riprodurre in altro modo i portali di comparazione e le informazioni in essi contenute o parti di essi solo per uso personale. Qualsiasi altra riproduzione, rielaborazione, presentazione, messa a disposizione del pubblico, diffusione, radiodiffusione e altro sfruttamento di contenuti non trascurabili dei portali di comparazione, compresa la loro banca dati, richiede il consenso scritto di idealo. Ciò vale anche per l'inserimento di contenuti dei portali di comparazione nella cornice. 

10.4 I nomi delle aziende, i loghi e/o i nomi dei prodotti citati nei portali di comparazione sono marchi protetti o marchi di fabbrica dei rispettivi proprietari. 

10.5 idealo utilizza in parte i dati dei portali di comparazione che provengono dalle banche dati dei seguenti progetti: 

* Geonimi ([http://www.geonames.org](http://www.geonames.org/)) 
    
* Dbpedia ([http://www.dbpedia.org](http://www.dbpedia.org/)) 
    
* OpenStreetMap ([https://www.openstreetmap.org](https://www.openstreetmap.org/)) 
    

Geonames e Dbpedia sono concessi in licenza CC-BY-SA 3.0. OpenStreetMap è concesso in licenza con le licenze OdbL- e CC-BY-SA 2.0.  All'uso di questi dati si applicano solo le condizioni di licenza dei rispettivi progetti. 

### **11\. Linguaggio contrattuale, testo contrattuale, comunicazione** 

11.1 La lingua del contratto è l'italiano. 

11.2 La comunicazione tra idealo e gli utenti avviene generalmente tramite mezzi elettronici di comunicazione a distanza.  Questa comunicazione avviene anche su in lingua italiano o inglese. 

11.3 Gli utenti possono richiamare le condizioni d'uso attuali attraverso i portali di comparazione e salvarle in formato PDF o stamparle. Le rispettive condizioni d'uso non vengono salvate da idealo dopo la stipula del contratto. 

### **12\. Nota sulla risoluzione delle controversie** 

La Commissione europea mette a disposizione una piattaforma di risoluzione delle controversie ([piattaforma ODR](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage)). idealo non è obbligata a partecipare a procedure di risoluzione delle controversie davanti a collegi arbitrali dei consumatori. idealo non offre inoltre volontariamente agli utenti la partecipazione a tali procedure. 

### **13\. Legge applicabile** 

Si applica il diritto tedesco. Se gli utenti sono consumatori e hanno la loro residenza abituale in uno Stato membro dell'UE diverso dalla Germania, le disposizioni obbligatorie in materia di tutela dei consumatori di tale Stato rimangono inalterate. Ciò vale in particolare per la conclusione del contratto. 

### **14\. Luogo di giurisdizione** 

Se l'utente è un commerciante, il foro competente è la sede di idealo. idealo è tuttavia autorizzata a citare in giudizio questi utenti anche presso il tribunale del loro luogo di residenza. Se l'utente è un consumatore e ha il proprio domicilio o la propria residenza abituale al di fuori della Germania, il foro competente è quello della sede di idealo. In tutti gli altri casi valgono le norme di legge. 

Stato: 17 febbraio 2024