Algemene voorwaarden van kracht vanaf 20 augustus 2024

**1.    Onderwerp**
-------------------

Comuto SA (hierna “**BlaBlaCar**” genoemd) heeft een rideshareplatform ontwikkeld dat op de website [www.blablacar.nl](http://www.blablacar.nl/) of in de vorm van een mobiele app toegankelijk is en dat bedoeld is (i) om autobestuurders die naar een bepaalde bestemming rijden, in contact te brengen met passagiers die in dezelfde richting reizen, zodat ze de rit en de bijbehorende kosten kunnen delen, en (ii) om kaartjes te boeken voor busritten die door Busmaatschappijen worden uitgevoerd (hierna het “**Platform**” genoemd).

Deze algemene voorwaarden hebben ten doel om de toegang tot en de voorwaarden voor het gebruik van het Platform te regelen. Lees deze algemene voorwaarden aandachtig. U begrijpt en erkent dat BlaBlaCar geen partij vormt in een overeenkomst, contract of contractuele verhoudingen van welke aard dan ook tussen de Leden van het Platform of tussen u en de Busmaatschappij.

Als u op “Inloggen met Facebook” of “Registreren met een mailadres” klikt, erkent u al deze algemene voorwaarden te hebben gelezen en te aanvaarden.

Merk op dat, als u uw profiel gebruikt om in te loggen op het BlaBlaCar-platform van een ander land (bijvoorbeeld www.blablacar.com.br), (i) de algemene voorwaarden van dat platform, (ii) het privacybeleid van dat platform en (iii) de statutaire regels en verordeningen van dat land van toepassing zullen zijn. Dit betekent ook dat de informatie van uw Profiel, inclusief persoonsgegevens, mogelijk moet worden overgedragen naar de juridische entiteit die dat andere platform beheert. BlaBlaCar behoudt zich het recht voor om de toegang tot het platform te beperken voor gebruikers van wie redelijkerwijs kan worden vastgesteld dat ze zich buiten het grondgebied van de Europese Unie bevinden.

**2.    Definities**
--------------------

In onderhavig document hebben de volgende termen de volgende betekenis:

“**Algemene Verkoopvoorwaarden**” betekent de [Algemene Verkoopvoorwaarden](https://blog.nl.blablacar.be/about-us/algemene-verkoopvoorwaarden) van de betreffende Busmaatschappij afhankelijk van de Busrit geselecteerd door de Klant, en de bijzondere voorwaarden, beschikbaar op de Website, en die de Klant heeft gelezen vóór zijn bestelling.

“**AV’s**” betekent onderhavige Algemene Voorwaarden;

“**Bijdrage in de Kosten**” betekent voor een bepaalde Carpoolrit de som die door de Bestuurder wordt gevraagd en die door de Passagier wordt aanvaard als bijdrage van de reiskosten;

“**BlaBlaCar**” heeft de betekenis die eraan wordt gegeven in artikel 1 hierboven;

“**Bestelling**“: de handeling waarbij de klant Diensten bij BlaBlaCar Bus reserveert, ongeacht de gebruikte reserveringsmethode, met als enige uitzondering de aankoop van Tickets rechtstreeks bij een Verkooppunt, en die voor de Passagier of eventueel een Agent de verplichting met zich meebrengt om de prijs voor de desbetreffende Diensten te betalen;

“**Bestuurder**” betekent het Lid dat gebruikmaakt van het Platform om een andere persoon vervoer aan te bieden in ruil voor een Bijdrage aan de Kosten voor een Rit en op een tijdstip die alleen door de Bestuurder worden gedefinieerd;

“**Bevestiging van boeking**” heeft de betekenis die eraan wordt gegeven in Artikel 4.2.1 hieronder;

“**Boeking**” heeft de betekenis die eraan is toegekend in Artikel 4.2.1. hieronder;

“**Busmaatschappij**” verwijst naar een maatschappij die passagiers professioneel vervoert en tickets voor wier reizen door BlaBlaCar op het Platform worden verspreid;

“**Buspublicatie**” betekent een publicatie over een busrit van een een Busmaatschappij die op het Platform wordt gepost;

“**Busrit**” betekent het reisonderwerp van een Buspublicatie op het Platform en waarvoor de Busmaatschappij zitplaatsen in de busvoertuigen voorstelt in ruil voor de Prijs;

“**Carpoolpublicatie**” betekent een publicatie betreffende een Rit die door een Bestuurder op het Platform is geplaatst;

“**Carpoolrit**” betekent de verplaatsing die het voorwerp uitmaakt van een Carpoolpublicatie die door een Bestuurder op het Platform werd gepubliceerd en waarvoor hij zich akkoord verklaart om Passagiers te vervoeren in ruil voor de Bijdrage in de Kosten;

“**Content voor Leden**” heeft de betekenis die eraan wordt gegeven in Artikel 11.2 hieronder;

“**Diensten**” betekent alle diensten die via het Platform door BlaBlaCar worden verleend;

“**Deel van de rit**” heeft de betekenis die eraan wordt gegeven in Artikel 4.1 hieronder;

“**Facebook-Profiel**” heeft de betekenis die eraan wordt gegeven in Artikel 3.2 hieronder;

“**Hyperwallet-betaalmethode**” heeft de betekenis die er in onderstaand artikel 5.4.2.a aan wordt toegekend;

“**Klant**” betekent elke fysieke persoon (Lid of geen lid) die, hetzij voor zichzelf, hetzij namens iemand anders die de Passagier zal zijn, een Busticket koopt via het Platform om een Rit te maken die wordt uitgevoerd door de Busmaatschappij;

“**Lid**” betekent een persoon die een Profiel heeft aangemaakt op het Platform;

“**Passagier**” betekent het Lid dat het aanbod van de Bestuurder om te worden vervoerd, heeft aanvaard of, indien dit van toepassing is, de persoon in wiens naam een Lid een Zitplaats heeft geboekt;

“**Platform**” heeft de betekenis die eraan wordt gegeven in Artikel 1 hierboven;

“**Prijs**” verwijst, voor een bepaalde Busrit, naar de prijs inclusief alle belastingen, vergoedingen en kosten van relevante diensten inbegrepen, betaald door de Klant op het Platform, op het moment van de validatie van de Bestelling, voor een Zitplaats op een bepaalde Busrit;

“**Profiel**” betekent het profiel dat moet worden aangemaakt om Lid te worden en om toegang te krijgen tot bepaalde diensten die door het Platform worden aangeboden;

“**Provider Hyperwallet-betaalmlethode**” betekent PayPal (Europe) S.à r.l. et Cie, S.C.A.;

“**Publicatie**” betekent een publicatie over een Rit die door een Bestuurder op het Platform wordt gepost;

“**Rit**” verwijst naar een Busrit of een Carpoolrit;

“**Ticket**“: het nominatiefve geldigee vervoersbewijs dat aan de consument wordt verstrekt na het boeken van een busrit, als bewijs van de bestaande vervoersovereenkomst tussen de passagier en de Busmaatschappij, waarop de algemene verkoopvoorwaarden van toepassing zijn, onverminderd eventuele aanvullende bijzondere voorwaarden die tussen de passagier en de Busmaatschappij zijn bedongen en waarnaar op het Vervoerbewijs wordt verwezen; 

“**Verkooppunt**” verwijst naar de fysieke loketten en terminals vermeld op de Website waar de Tickets te koop worden aangeboden;

“**Vergoeding voor de Service**” heeft de betekenis die eraan wordt gegeven in Artikel 5.2 hieronder;

“**Vervoerdiensten**” zijn de vervoerdiensten waarop een Busreiziger zich heeft ingeschreven en die door de Busmaatschappij worden verleend;

“**Website**” betekent de website die toegankelijk is op het adres [www.blablacar.nl](http://www.blablacar.nl/);

“**Zitplaats**” betekent de plaats die door een Passagier in het voertuig van de Bestuurder of de Bus van de Busmaatschappij wordt geboekt.

**3.    Registratie op het Platform en aanmaken van een Profiel**
-----------------------------------------------------------------

### **3.1.    Voorwaarden voor registratie op het Platform**

Het Platform mag worden gebruikt voor personen van 18 jaar of ouder. Registratie op het platform door een minderjarige is ten strengste verboden. Door het Platform te bezoeken of te gebruiken of door u erop te registreren, verklaart en garandeert u dat u minstens 18 bent.

### **3.2.    Aanmaken van een Profiel**

Het Platform biedt Leden de mogelijkheid om Carpoolpublicaties te posten en te bekijken en om interactie te voeren met anderen met de bedoeling om een Zitplaats te boeken. U kunt de Publicaties bekijken als u niet op het Platform geregistreerd bent. Maar u kunt pas een Carpoolpublicatie plaatsen of een Zitplaats boeken nadat u een Profiel hebt aangemaakt en nadat u Lid bent geworden.

U kunt op de volgende manieren een Profiel aanmaken:

* (i) alle verplichte velden van het registratieformulier in te vullen;
* (ii) of via ons Platform inloggen op uw Facebook-account (hierna uw “**Facebook-profiel**” genoemd). Door deze functionaliteit te gebruiken, begrijpt u dat BlaBlaCar toegang zal hebben tot bepaalde informatie op uw Facebook-profiel en dat ze deze op het Platform zal delen en zal bijhouden. U kunt de link tussen uw Profiel en uw Facebook-profiel op eender welk moment wissen via de rubriek “Verificatie” van uw profiel. Als u meer wilt weten over het gebruik van uw gegevens van uw Facebook-profiel, kunt u ons [Privacybeleid](https://www.blablacar.nl/about-us/privacy-policy) en dat van Facebook lezen.

Als u zich op het Platform wilt registreren, moet u deze AV’s hebben gelezen en begrepen.

Wanneer u uw Profiel – via welke methode dan ook – hebt aangemaakt, stemt u ermee in om juiste en ware informatie te bezorgen en om die via uw profiel of door contact op te nemen met BlaBlaCar te updaten, zodat de relevantie en de juistheid van die gegevens tijdens de volledige geldigheidsduur van uw contractuele relaties met BlaBlaCar gegarandeerd zijn.

Indien u zich per mail registreert, stemt u ermee in om het wachtwoord dat u bij het aanmaken van uw Profiel hebt ingevoerd, geheim te houden en het aan niemand anders mee te delen. Als u uw wachtwoord verliest of bekend maakt, moet u BlaBlaCar daarvan onmiddellijk op de hoogte brengen. Alleen u bent verantwoordelijk voor het gebruik van uw Profiel door derden, tenzij u BlaBlaCar vooraf uitdrukkelijk in kennis hebt gesteld van het verlies, het bedrieglijke gebruik door een derde of de bekendmaking van uw wachtwoord aan een derde.

U stemt ermee in om geen andere Profielen dan het Profiel dat u aanvankelijk zelf hebt aangemaakt, aan te maken of te gebruiken onder uw eigen identiteit of de identiteit van een derde.

### **3.3.    Verificatie**

BlaBlaCar kan met het oog op de transparantie, de verbetering van het vertrouwen en de preventie of de detectie van fraude een systeem invoeren waarmee ze een deel van de informatie die u op uw profiel geeft, kan verifiëren. Dit geldt meer bepaald wanneer u uw telefoonnummer invoert of ons een identiteitsbewijs verstrekt.

U erkent en aanvaardt dat iedere verwijzing op het Platform of de Diensten naar “gecontroleerde” informatie of een vergelijkbare term alleen betekent dat een Lid de verificatieprocedure op het Platform of de Diensten met succes heeft doorlopen met de bedoeling om u meer informatie te bezorgen over het Lid met wie u overweegt een rit te maken. BlaBlaCar kan de waarheidsgetrouwheid, de betrouwbaarheid of de geldigheid van de informatie die het onderwerp is van de verificatieprocedure, niet garanderen.

**4.    Gebruik van de Diensten**
---------------------------------

### **4.1.    Publicaties posten**

Als Lid en op voorwaarde dat u aan de voorwaarden hieronder voldoet, kunt u Carpoolpublicaties aanmaken en op het Platform posten door informatie in te voeren over de Carpoolrit die u van plan bent om te maken (datums/ tijdstippen en oppikpunten en aankomst, aantal Zitplaatsen aangeboden, beschikbare opties, bedrag van de Bijdrage in de Kosten enz.).

Wanneer u een Carpoolpublicatie plaatst, kunt u de belangrijkste steden vermelden waar u wilt stoppen of waar u Passagiers wilt oppikken of afzetten. De delen van de Carpoolrit tussen die belangrijke steden of tussen een van die belangrijke steden en het vertrekpunt of de bestemming van de Carpoolrit worden “**Delen van de rit**” genoemd.

U mag een Carpoolpublicatie alleen posten als u aan alle ondergenoemde voorwaarden voldoet:

  (i) u bent in het bezit van een geldig rijbewijs;

  (ii) u biedt alleen Carpoolpublicaties aan voor voertuigen waarvan u zelf de eigenaar bent of die u met de uitdrukkelijke toestemming van de eigenaar mag gebruiken en in ieder geval moet u het voertuig voor ridesharedoeleinden mogen gebruiken;

  (iii) u bent en u blijft de hoofdbestuurder van het voertuig dat het voorwerp uitmaakt van de Carpoolpublicatie;

  (iv) er is voor het voertuig een geldige burgerlijke aansprakelijkheidsverzekering afgesloten;

  (v) er gelden voor u geen medische contra-indicaties en u bent ook niet om medische redenen onbevoegd verklaard om met de auto te rijden;

  (vi) het voertuig dat u van plan bent om te gebruiken voor de Rit, is een personenauto met 4 wielen en maximaal 7 zitplaatsen, met uitsluiting van auto’s waar geen rijbewijs voor nodig is om erin te rijden.

  (vii) u bent niet van plan om voor dezelfde Rit een andere Carpoolpublicatie op het Platform te posten;

  (vii) u bent niet van plan om voor dezelfde Rit een andere Publicatie op het Platform te posten;

  (viii) u biedt niet meer Zitplaatsen aan dan het aantal beschikbare Zitplaatsen in uw voertuig;

  (ix) alle Zitplaatsen die worden aangeboden, zijn uitgerust met een veiligheidsgordel – zelfs als het voertuig goedgekeurd is met Zitplaatsen zonder veiligheidsgordel;

  (x) u maakt gebruik van een voertuig dat zich in een goede staat bevindt en dat beantwoordt aan de toepasselijke wettelijke voorschriften en gewoonten, in het bijzonder met een geldig APK-keuringsrapport;

  (xi) u bent een consument en handelt niet als professional..

U erkent dat u als enige verantwoordelijk bent voor de content van de Publicatie die u op het Platform post. U verklaart en garandeert dan ook dat alle informatie in uw Publicatie juist en waarheidsgetrouw is en dat u van plan bent om de Rit te maken onder de voorwaarden die in uw Publicatie worden beschreven.

Uw Carpoolpublicatie wordt op het Platform gepost en is daardoor zichtbaar voor Leden en alle bezoekers (zelfs niet-leden) die een zoekopdracht uitvoeren op het Platform of op de website van de partners van BlaBlaCar. BlaBlaCar behoudt zich het recht voor om naar eigen goeddunken een Carpoolpublicatie die niet aan de AV’s beantwoordt of die het schadelijk is voor haar imago, het imago van het Platform of het imago van de Diensten, niet te plaatsen of te verwijderen en/of het Profiel van het Lid dat dergelijke Carpoolpublicaties post, op te schorten conform hoofdstuk 9 van deze AV’s.

U wordt ervan op de hoogte gebracht dat als u zich voordoet als een consument door gebruik te maken van het Platform terwijl u in feite handelt als professional, u blootgesteld bent aan sancties volgens de toepasselijke wetgeving.

### **4.2.**    Een Zitplaats boeken

BlaBlaCar heeft een systeem opgezet voor het online reserveren van Zitplaatsen (“Boeking”) voor Ritten die op het Platform worden aangeboden. De methodes voor het reserveren van een Zitplaats hangen af van de aard van de geplande Busrit.

BlaBlaCar biedt gebruikers op het Platform een zoekmachine aan op basis van verschillende zoekcriteria (Plaats van herkomst, bestemming, datums, aantal reizigers, enz.). Bepaalde extra functionaliteiten worden op de zoekmachine aangeboden wanneer de gebruiker ingelogd is op zijn account. BlaBlaCar nodigt de gebruiker uit om, ongeacht het gebruikte boekingsproces, de zoekmachine zorgvuldig te raadplegen en te gebruiken om te bepalen wat het best past bij zijn behoeften. Meer informatie vindt u [hier](https://blog.nl.blablacar.be/about-us/platformtransparantie).  De Klant die een Busrit boekt in een Verkooppunt kan ook de Busmaatschappij of de medewerker aan de balie vragen de zoekopdracht uit te voeren.

**4.2.1 Carpoolrit**

Wanneer een Passagier geïnteresseerd is in een Carpoolpublicatie die gereserveerd kan worden, kan hij online een aanvraag voor de Boeking maken. Deze aanvraag wordt ofwel (i) automatisch aanvaard (indien de Bestuurder deze optie selecteerde wanneer hij zijn Publicatie plaatste) of (ii) handmatig door de Bestuurder goedgekeurd. Op het moment van de boeking betaalt de Passagier online de Bijdrage in de Kosten en de bijbehorende Vergoeding voor de Diensten waar dit van toepassing is. Na ontvangst van de betaling door BlaBlaCar en de validering van de aanvraag van de Boeking door de Bestuurder, waar dit van toepassing is, ontvangt de Passagier een Bevestiging van Boeking (de “**Bevestiging van Boeking**”).

Als u een Bestuurder bent en als u bij het plaatsen van uw Carpoolpublicatie heeft beslist om de aanvragen voor boekingen manueel te verwerken, moet u op ieder verzoek voor Boeking reageren binnen de gegeven termijn, gespecificeerd door de Passagier tijdens de Boeking. Anders vervalt de aanvraag van Boeking automatisch en zullen aan de Passagier alle sommen die hij eventueel op het tijdstip van de aanvraag van boeking heeft betaald, worden terugbetaald.

Op het moment van de Bevestiging van Boeking deelt BlaBlaCar het telefoonnummer van de bestuurder met u (als u de Passagier bent) of van de Passagier (als u de Bestuurder bent), als het Lid ermee heeft ingestemd om zijn/ haar telefoonnummer weer te geven. U bent vanaf dat moment als enige verantwoordelijk voor de uitvoering van de overeenkomst die u met het andere Lid bindt.

**4.2.2 Busrit** 

Voor Busritten maakt BlaBlaCar het mogelijk om Buskaartjes voor een bepaalde Busrit via het Platform te boeken. 

De Vervoersdiensten zijn onderhevig aan de Algemene Verkoopvoorwaarden van de desbetreffende Busmaatschappij, afhankelijk van de door de Klant gekozen Rit, die door de Klant moeten worden aanvaard alvorens de bestelling te plaatsen. BlaBlaCar biedt geen vervoersdiensten aan met betrekking tot Busritten, de Busmaatschappijen zijn de enige partij in de Algemene Verkoopvoorwaarden. U erkent dat de boeking van Zitplaatsen voor een bepaalde Busrit onderhevig is aan de Algemene Verkoopvoorwaarden van de desbetreffende Busmaatschappij. 

BlaBlaCar vestigt uw aandacht op het feit dat bepaalde Vervoersdiensten die door de Busmaatschappij worden aangeboden en op de Website worden vermeld, kunnen worden geannuleerd, met name door weersomstandigheden, seizoensgebonden redenen of in geval van overmacht.     

Ieder gebruik van de Diensten in de hoedanigheid van Passagier of Bestuurder is verbonden met een specifieke naam. De Bestuurder en de Passagier moeten overeenstemmen met de identiteit die aan BlaBlaCar en aan de andere Leden die de Carpoolrit meemaken of de Busmaatschappij, werd vermeld, indien van toepassing.

BlaBlaCar laat zijn Leden wel toe om één of meerdere Zitplaatsen in naam van een derde te boeken. In dat geval moet u op het moment van de boeking, de voornamen, de leeftijd en het telefoonnummer vermelden van de persoon in wiens naam u een Zitplaats boekt. Het is ten strengste verboden om een Zitplaats te boeken voor een minderjarige jonger dan 13 jaar die alleen reist. Indien u een Zitplaats boekt voor een minderjarige ouder dan 13 jaar, moet u vooraf de toestemming van de Bestuurder vragen en hem een behoorlijk ingevulde en ondertekende toestemming van de wettelijke vertegenwoordigers van de minderjarige bezorgen.

Daarnaast is het Platform bedoeld voor het boeken van Zitplaatsen voor personen. Het is verboden om een Zitplaats te boeken om welk voorwerp, pakket, dieren die alleen reizen of materiële artikelen dan ook te vervoeren.

Voorts is het verboden om een Publicatie te publiceren voor een andere Bestuurder dan uzelf.

### **4.3.**    Content voor Leden, moderatie en evaluatiesysteem

**4.3.1.    Werking**

BlaBlaCar moedigt u aan om een beoordeling over een Bestuurder (als u een Passagier bent) of een Passagier (als u een Bestuurder bent) met wie u een Rit hebt gedeeld of met wie u van plan was een rit te delen (d.w.z. een rit te boeken), te plaatsen. U mag geen beoordeling over een andere Passagier plaatsen als u zelf een Passagier was of een beoordeling over een Lid met wie u geen Rit hebt gemaakt of niet van plan was een Rit te maken.

Uw beoordeling en de beoordeling door een ander Lid over u, zijn alleen zichtbaar en worden alleen op het Platform gepubliceerd nadat: (i) u beiden een evaluatie hebt gepubliceerd of (ii) na een periode van 14 dagen na de eerste beoordeling.

U kunt wel nog reageren op een beoordeling die een ander Lid op uw profiel heeft achtergelaten. De beoordeling en uw reactie, indien van toepassing, zullen op uw profiel worden gepubliceerd.

**4.3.2.    Moderatie**

**a. Content voor Leden**

U erkent en aanvaardt dat BlaBlaCar voorafgaand aan publicatie door middel van geautomatiseerde tools of handmatig Content voor Leden zoals gedefinieerd in Hoofdstuk 11.12 kan modereren. Als BlaBlaCar van mening is dat dergelijke Content voor Leden inbreuk maakt op toepasselijke wetgeving of deze AV’s behoudt zij zich het recht voor:

* de Publicatie te voorkomen of dergelijke Content voor Leden te verwijderen,
* het Lid een waarschuwing te sturen met de herinnering aan de verplichting tot naleving van toepasselijke wetgeving of deze AV’s, en/of
* Restrictieve maatregelen te treffen conform Hoofdstuk 9 van deze AV’s.

BlaBlaCar’s gebruik van dergelijke geautomatiseerde tools of handmatige moderatie mag niet worden geïnterpreteerd als een maatregel tot het monitoren van en een verplichting tot het actief zoeken naar onwettige activiteiten en/of Content voor Leden die gepubliceerd wordt op het Platform, en voor zover wettelijk toegestaan leidt dit niet tot enige aansprakelijkheid van BlaBlaCar.

**b. Berichten**

Het uitwisselen van berichten tussen Leden via ons Platform (“Berichten”) vindt uitsluitend plaats met als doel het uitwisselen van informatie over Carpoolritten.

BlaBlaCar kan geautomatiseerde software en algoritmes gebruiken om de content van Berichten te detecteren om fraude te voorkomen, de service te verbeteren, ten behoeve van klantenondersteuning en het uitvoeren van contracten die met onze Leden zijn aangegaan (zoals deze AV’s). In geval dergelijke content wordt gedetecteerd in een Bericht dat tekenen vertoont van fraude of illegaal gedrag, ontduiking van het Platform of gedrag dat anderszins is in strijd is met deze AV’s

* Dergelijke Content mag niet worden gepubliceerd
* Het Lid dat de Bericht verstuurt kan een waarschuwing ontvangen waarin wordt herinnerd aan de verplichting om de toepasselijke wetgeving en deze AV’s na te leven
* Het Profiel van het Lid kan worden opgeschort conform Hoofdstuk 9 van deze AV’s

Het gebruik door BlaBlaCar van dergelijke geautomatiseerde software mag niet geïnterpreteerd als een maatregel tot het monitoren van en een verplichting tot het actief zoeken naar onwettige activiteiten en/of Content voor Leden die gepubliceerd wordt op het Platform, en voor zover wettelijk toegestaan leidt dit niet tot enige aansprakelijkheid van BlaBlaCar.

**4.3.3.    Limiet**

Conform Hoofdstuk 9  van deze AV’s behoudt BlaBlaCar zich het recht voor om uw Profiel op te schorten, uw toegang tot de Diensten te beperken of deze AV’s op te zeggen indien (i) u minstens drie beoordelingen hebt ontvangen en (ii) een of de gemiddelde beoordeling die u heeft ontvangen, hoogstens gelijk is aan 3, afhankelijk van de ernst van de feedback van een Lid in de beoordeling..

**5.    Financiële voorwaarden**
--------------------------------

De toegang tot en de registratie op het Platform en het zoeken, bekijken en posten van Publicaties zijn gratis. Voor de boeking worden kosten aangerekend in overeenstemming met de voorwaarden die hieronder worden beschreven.

### **5.1.**    Bijdrage in de Kosten

5.1.1. Voor een Carpool wordt de bijdrage in de Kosten onder uw exclusieve verantwoordelijkheid door u als Bestuurder bepaald. Het is ten strengste verboden om op welke manier dan ook winst te boeken door het gebruik van ons Platform. U stemt er dan ook mee in om de Bijdrage in de Kosten die u aan uw Passagiers vraagt, te beperken tot de kosten die u werkelijk maakt om de Carpoolrit te maken. Anders zult u als enige de risico’s dragen van een herclassificiatie van de transactie die via het Platform werd uitgevoerd.

Wanneer u een Carpoolpublicatie plaatst, stelt BlaBlaCar een bedrag voor de Bijdrage in de Kosten voor, waarbij onder meer rekening wordt gehouden met de aard van de Carpoolrit en de afstand die wordt afgelegd. Dit bedrag wordt louter als richtlijn gegeven; u kunt zelf beslissen om dat bedrag te verhogen of te verlagen om rekening te houden met de reële kosten van de Carpoolrit. Om misbruik te voorkomen, limiteert BlaBlaCar de mogelijkheid om de Bijdrage in de Kosten aan te passen.

5.1.2 Wat de Busrit betreft, wordt de Prijs per Zitplaats bepaald naar goeddunken van de Busmaatschappij. De Klant wordt verzocht de relevante Algemene Verkoopvoorwaarden te raadplegen zodat hij de geldende modaliteiten met betrekking tot het plaatsen van een bestelling voor een ticket en de betaling begrijpt.  

### **5.2.    Vergoeding voor de Diensten**

BlaBlaCar kan, in ruil voor het gebruik van het Platform, op het moment van de boeking van een Rit of buskaartje een Vergoeding voor de Diensten (hierna de “**Vergoeding voor de Diensten**” genoemd) vragen. De gebruiker vóór de bevestiging wordt op de hoogte gebracht van de Vergoeding voor Diensten, indien vereist.

De manier waarop de geldende Vergoeding voor de Diensten wordt berekend, vindt u [hier](https://blog.nl.blablacar.be/blablalife/lp/nieuwe-reserveringskosten-tabel) en is louter ter informatie en heeft geen contractuele waarde. BlaBlaCar behoudt zich het recht voor om de berekeningsmethode te allen tijde te veranderen. Deze wijzigingen hebben geen invloed op de Vergoeding voor de Diensten en worden niet retroactief toegepast op reeds geboekte Ritten en buskaartjes.

Houd in het geval van internationale ritten rekening met het feit dat de berekeningsmethoden van het bedrag van de Vergoeding voor de Diensten en de btw die daarop van toepassing is, kan verschillen afhankelijk van de verblijfplaats van de Passagier.

Wanneer u het Platform gebruikt voor grensoverschrijdende ritten of voor ritten buiten Nederland, is het mogelijk dat de Vergoeding voor de Diensten wordt aangerekend door een filiaal van BlaBlaCar dat het plaatselijke platform runt.

### **5.3.    Afronden**

U erkent en aanvaardt dat BlaBlaCar naar eigen goeddunken de Vergoeding voor de Diensten en de Bijdrage in de Kosten naar boven of naar beneden kan afronden op gehele getallen of decimalen.

### **5.4.    Methoden van betaling en terugbetaling van de Bijdrage in de Kosten aan de Bestuurder**

**5.4.1.    Instructie Bestuurder (machtiging)**

Door het Platform als Bestuurder voor Carpoolritten te gebruiken, bevestigt u dat u de [algemene voorwaarden van de provider van de Hyperwallet-betaalmlethode](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) hebt gelezen en aanvaard – dit is onze betaalpartner die de overdrachten van kostenbijdragen aan de Bestuurders verwerkt via de Hyperwallet-betaalmethode, beschreven onder 5.4.2.a. Deze algemene voorwaarden hierna de “AV’s van de Hyperwallet” genoemd.

Merk op dat uitsluitingen op EU-richtlijn 2015/2366 van het Europese Parlement en de Raad van 25 november 2015 betreffende betaaldiensten op de interne markt, vermeld in artikel 11.4(i) en (ii) in de algemene voorwaarden van de Hyperwallet niet van toepassing zijn op de leden van het Platform die geen professionals zijn.

Als Bestuurder geeft u instructies aan

* BlaBlaCar om de Provider van de Hyperwallet-betaalmethode opdracht te geven om de overdracht voor de Bijdrage in de Kosten uit te voeren en
* de Provider van de Hyperwallet-betaalmethode om de overdracht van de Bijdrage in de Kosten over te maken naar uw bank of PayPal-account in uw naam en in uw opdracht, overeenkomstig de AV’s van de Hyperwallet.

In de context van een Carpoolrit en na handmatige of automatische goedkeuring van de Boeking wordt de volledige som die door de Passagier werd betaald (Vergoeding voor de Diensten en Bijdrage in de Kosten) op een geblokkeerde rekening geplaatst die de Provider van de Hyperwallet-betaalmethode wordt beheerd.

**5.4.2.    Betaling van de Bijdrage in de Kosten aan de Bestuurder**

Na het einde van de Carpoolrit zullen de passagiers een periode van 24 uur hebben om een riten-claim aan BlaBlaCar te vragen. In afwezigheid van enige passagiers claim binnen deze periode, zal BlaBlaCar de rit als uitgevoerd beschouwen.

Vanaf het tijdstip van deze uitdrukkelijke of stilzwijgende bevestiging krijgt u als Bestuurder een krediet op uw Profiel. Dit krediet stemt overeen met het volledige bedrag dat door de Passagier werd betaald op het moment van de bevestiging van de Boeking, verminderd met de Vergoeding voor de Diensten, d.w.z. de Bijdrage in de Kosten die door de Passagier wordt betaald.

**a. Hyperwallet-betaalmethode**

De Hyperwallet-betaalmethode is een betaaldienst die door de Provider van de Hyperwallet-betaalmethode wordt verstrekt. Voor alle duidelijkheid: BlaBlaCar biedt geen betaalverwerkingsdiensten voor Leden en neemt de gelden van Leden niet in ontvangst.

Wanneer de Carpoolrit door de Passagier bevestigd is, hebt u als Bestuurder de mogelijkheid om uw Bijdrage in de Kosten op uw bankrekening of op uw PayPal-account te ontvangen.

Hiertoe kunt u de Hyperwallet-betaalmethode gebruiken om rechtstreeks met de Provider van de Hyperwallet-betaalmethode een overeenkomst aan te gaan en de AV’s van de Hyperwallet te aanvaarden.

Om de eerste overschrijving van de kostenbijdrage te ontvangen, wordt u gevraagd een betaalmethode te selecteren, d.w.z. uw bank- of PayPal-rekeninggegevens op te geven, evenals uw postadres en geboortedatum. Indien u dit niet doet, of als de informatie die u verstrekt, niet correct, onvolledig of niet actueel is, dan zal de Bijdrage in de Kosten niet worden uitbetaald.

De overdracht zal uitgevoerd worden door de Provider van de Hyperwallet-betaalmethode.

Aan het einde van de periode van 10 jaar, wordt elk bedrag aan Kostenbijdrage dat niet is geclaimd vanaf de datum dat het door u als Bestuurder via de Hyperwallet-betaalmethode op uw Account verscheen, geacht toe te behoren aan BlaBlaCar.

**b. KYC-controles**

Bij het gebruik van Hyperwallet-betaalmethode aanvaardt u dat u mogelijk onderworpen bent aan regelgevingsprocedures die door de Provider van de Hyperwallet-betaalmethode worden toegepast, met inachtneming van de [AV’s van de Hyperwallet](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml).

Deze procedures kunnen identiteitscontroles en andere vereisten van de “Know Your Customer” (KYC)-procedure omvatten, op basis van de criteria die zijn vastgesteld door de Provider van de Hyperwallet-betaalmethode. Deze criteria kunnen financiële drempels omvatten, afhankelijk van het totale bedrag aan uitbetalingen van de Bijdrage in de Kosten die u doet.

Voor KYC-controledoeleinden kan u worden gevraagd om aanvullende informatie te verstrekken zoals gevraagd door de Provider van de Hyperwallet-betaalmethode. In sommige gevallen kan BlaBlaCar contact met u opnemen om een KYC-verzoek van de Provider van de Hyperwallet-betaalmethode door te geven. Als u de vereiste documenten en informatie niet verstrekt, kunt u de kostenbijdrage pas ontvangen nadat u de KYC-procedure heeft voltooid.

Er kunnen zich situaties voordoen waarin de Provider van de Hyperwallet-betaalmethode, in overeenstemming met de toepasselijke wetgeving, de overdracht van de Bijdrage in de Kosten of de toegang tot de Hyperwallet-betaalmethode moet opschorten. BlaBlaCar heeft geen invloed op dergelijke acties van de Provider van de Hyperwallet-betaalmethode.

De KYC-controles die worden uitgevoerd door de Provider van de Hyperwallet-betaalmethode zijn verschillend en onafhankelijk van de verificaties die BlaBlaCar kan uitvoeren met betrekking tot het gebruik van het Platform in overeenstemming met deze Voorwaarden en het [Privacybeleid](https://www.blablacar.nl/about-us/privacy-policy).

**5.4.3. Betaling van de Prijs**

De betaling voor elke Bestelling gemaakt via het Platform gebeurt aan de hand van een van de hieronder vermelde toegestane betaalmiddelen. De Klant heeft de mogelijkheid om de informatie met betrekking tot een of meer kredietkaarten in zijn Klantaccount op te slaan, zodat hij deze informatie niet systematisch moet invoeren bij latere betalingen.

De betalingswijze kan variëren naargelang het BlaBlaCar-platform dat u gebruikt. De toegestane betaalmiddelen zijn:

* Bankkaart (de aanvaarde merken worden op het platform vermeld. Indien u een “co-branded” kaart hebt kunt u een specifiek merk kiezen in het betaalstadium)
* PayPal
* Voucher
* iDEAL
* Apple Pay, Google Pay

Er zal geen bestelbevestiging plaatsvinden vóór de volledige en effectieve betaling van de prijs van de Vervoersdiensten gekozen door de Klant. Indien de betaling onregelmatig of onvolledig is of niet wordt uitgevoerd om een reden die toe te schrijven is aan de Klant, zal de Bestelling onmiddellijk worden geannuleerd.

De Bestelling wordt bevestigd door een e-mail aan de Klant met de gegevens van de Bestelling.

De Passagier wordt verzocht de instellingen voor de inbox van zijn e-mailadres te controleren en zich er met name van te vergewissen dat de bevestigingsmail niet rechtstreeks in de Spam-mailbox terechtkomt.

De bevestiging van de bestelling is definitief. Bijgevolg zullen alle wijzigingen resulteren in een omruiling of annulering onder de voorwaarden van de toepasselijke Algemene Voorwaarden. Het is de verantwoordelijkheid van de Klant om ervoor te zorgen dat de Vervoersdiensten worden gekozen in overeenstemming met de behoeften en verwachtingen van de Passagier. De klant is verantwoordelijk voor de juistheid van de door hem ingevoerde persoonlijke informatie, behalve wanneer BlaBlaCar aantoonbaar nalaat deze persoonlijke informatie te verzamelen, op te slaan of te beschermen.

**6.    Niet-commerciële en niet-bedrijfsdoeleinden van de Diensten en van het Platform**
-----------------------------------------------------------------------------------------

U stemt ermee in om de Diensten en het Platform alleen te gebruiken om voor niet-commerciële en niet-bedrijfsdoeleinden in contact te worden gebracht met mensen die een Carpoolrit met u willen delen of om een Zitplaats te boeken in het kader van een Busrit.

In het kader van een Carpoolrit erkent u dat de consumentenrechten die voortvloeien uit de consumentenbeschermingswetgeving van de Unie niet van toepassing zijn op uw relatie met andere Leden.

Als Bestuurder stemt u ermee in om geen Bijdrage in de Kosten te vragen die hoger ligt dan de kosten die u in werkelijkheid maakt en die een winst zou kunnen opleveren, met dien verstande dat u in de context van kostendeling als Bestuurder uw eigen deel van de kosten voor de Carpoolrit moet dragen. U bent als enige verantwoordelijk om de kosten die u maakt voor de Carpoolrit, te berekenen en om te controleren of het bedrag dat wordt gevraagd, niet hoger ligt dan de kosten die u werkelijk maakt (met uitsluitsel van uw deel van de kosten),  door onder meer te verwijzen naar de toepasselijke belastingschaal \[aan te passen\].

BlaBlaCar behoudt zich het recht voor om uw Profiel op te schorten wanneer u gebruikmaakt van een door een chauffeur bestuurd of ander bedrijfsvoertuig of taxi of bedrijfsvoertuig en wanneer u daardoor winst boekt door gebruik te maken van het Platform. U stemt ermee in om BlaBlaCar op haar eenvoudige verzoek een kopie te bezorgen van het inschrijvingsbewijs van uw auto en ieder ander document dat aantoont dat u dit voertuig op het Platform mag gebruiken en dat u op basis daarvan geen winst boekt.

Conform Hoofdstuk 9 van deze AV’s behoudt BlaBlaCar zich ook het recht voor om uw Profiel op te schorten, om uw toegang tot de Diensten te beperken of om deze AV’s op te zeggen indien u op het Platform activiteiten uitvoert die wegens de aard of de frequentie van de aangeboden Ritten, het aantal vervoerde Passagiers en de Bijdrage in de Kosten die wordt gevraagd, tot een situatie leiden waarin u winst boekt of die om welke reden dan ook die bij BlaBlaCar de indruk wekken dat u winst maakt door het gebruik van het Platform.

**7.    Annuleringsbeleid**
---------------------------

### **7.1.**    Annulering van een Rit

Dit annuleringsbeleid is enkel van toepassing op Carpoolritten en BlaBlaCar biedt geen enkele garantie in geval van een annulering om welke reden dan ook. In het kader van de busritten is het annuleringsbeleid onderworpen aan de algemene verkoopvoorwaarden van de busmaatschappijen. De klant wordt verzocht de betreffende Algemene Voorwaarden te raadplegen om kennis te nemen van de voorwaarden voor omwisseling en annulering van zijn/haar Busrit.

Voor de annulering van een Zitplaats voor een Carpoolrit door de Bestuurder of de Passagier na Bevestiging van Boeking gelden de volgende bepalingen:

–    In het geval van een annulering die aan de Bestuurder toe te schrijven is, krijgt de Passagier het volledige betaalde bedrag terug (d.w.z. de Bijdrage in de Kosten en de bijbehorende Vergoeding voor de Diensten). Dit is het geval wanneer de Bestuurder een Rit annuleert of 15 minuten na het afgesproken tijdstip niet op de plaats van afspraak verschenen is ;

–    In het geval van een annulering die aan de Passagier toe te schrijven is:

* Als de Passagier meer dan 24 uur vóór het tijdstip van vertrek (vermeld in de Carpoolpublicatie) annuleert, krijgt de Passagier alleen de Bijdrage in de Kosten terugbetaald. De Vergoeding voor de Diensten blijven bij BlaBlaCar en de Bestuurder ontvangt geen enkel bedrag van welke aard dan ook;
* Als de Passagier 24 uur of minder vóór het tijdstip van vertrek (vermeld in de Carpoolpublicatie) en meer dan dertig minuten na de Bevestiging van Boeking annuleert, krijgt de Pasagier de helft van de Bijdrage in de Kosten die op het moment van de boeking werden betaald, terugbetaald. De Vergoeding voor de Diensten blijven bij BlaBlaCar en de Bestuurder ontvangt 50 % van de Bijdrage in de Kosten via de Hyperwallet-betaalmethode;
* Als de Passagier 24 uur of minder vóór het geplande tijdstip van vertrek (vermeld in de Carpoolpublicatie) en dertig minuten of minder na de Bevestiging van Boeking annuleert, wordt de Passagier de volledige Bijdrage in de Kosten terugbetaald via de Hyperwallet-betaalmethode. De Vergoeding voor de Diensten blijft bij BlaBlaCar en de Bestuurder ontvangt geen enkele som van welke aard dan ook;
* Als de Passagier annuleert na het geplande tijdstip van vertrek (vermeld in de Carpoolpublicatie), of als de Passagier 15 minuten na het overeengekomen tijdstip niet op de plaats van afspraak verschenen is, wordt geen enkel bedrag terugbetaald. In dat geval wordt de Bestuurder vergoed via de Hyperwallet-betaalmethode voor het volledige bedrag van de Bijdrage in de Kosten en de Vergoeding voor de Diensten blijft bij BlaBlaCar.

Wanneer de annulering plaatsvindt vóór het vertrek en ten gevolge van de Passagier, wordt de Zitplaats of worden de Zitplaatsen die door de Passagier werd(en) geannuleerd, automatisch beschikbaar gesteld voor andere Passagiers, die die Zitplaatsen online kunnen boeken. Voor hen gelden dan deze voorwaarden van de AV’s.  
BlaBlaCar oordeelt naar eigen goeddunken op basis van de beschikbare informatie over de legitimiteit van de aanvragen voor terugbetaling.

### **7.2.    Recht op herroeping**

In overeenstemming met Artikel 6:230p sub e van het Nederlandse Burgerlijk Wetboek hebt u geen recht op herroeping vanaf het tijdstip van de Bevestiging van Boeking indien het Contract tussen u en BlaBlaCar, dat erin bestaat om u in contact te brengen met een ander Lid, volledig werd uitgevoerd.

**8.    Gedrag van gebruikers van het Platform en Leden**
---------------------------------------------------------

### **8.1.    Verbintenissen van alle gebruikers op het Platform**

U erkent als enige verantwoordelijk te zijn voor de naleving van alle wetten, voorschriften en verplichtingen die van toepassing zijn op uw gebruik van het Platform.

Voorts belooft u bij het gebruik van het Platform en tijdens de Ritten om:

* (i) het Platform niet te gebruiken voor professionele of commerciële doeleinden of om andere doeleinden om winst te maken;
* (ii) geen valse, misleidende, kwaadwillige of frauduleuze informatie te sturen naar BlaBlaCar (en dan meer bepaald bij het aanmaken of het updaten van uw Profiel);
* (iii) in uw gesprekken of uw gedrag of in uw publicaties (inclusief Berichten) geen lasterlijke, beledigende, obscene, pornografische, vulgaire, kwetsende, ongepaste, gewelddadige, bedreigende, irriterende, racistische of xenofobe uitingen te geven of uitingen met seksuele connotaties, uitingen die tot geweld, discriminatie of haat oproepen, activiteiten of die het gebruik van illegale stoffen aanmoedigen of meer algemeen die indruisen tegen de toepasselijke wetgeving, deze AV’s en de doelstellingen van het Platform, die de rechten van BlaBlaCar of een derde zouden kunnen aantasten of die indruisen tegen de goede zeden;
* (iv) de rechten en het imago van BlaBlaCar – en dan meer bepaald haar intellectuele eigendomsrechten – niet te schaden;
* (v) niet meer dan één Profiel op het Platform te openen en geen Profiel te openen in naam van een derde;
* (vi) niet te proberen om het online boekingssysteem van het Platform te omzeilen, en dan meer bepaald door te proberen om een ander Lid of een Busmaatschappij uw contactgegevens te bezorgen, zodat de boeking buiten het Platform kan worden gemaakt om zo de Vergoeding voor de Diensten te omzeilen;
* (vii) geen contact op te nemen met een ander Lid, en dan meer bepaald via het Platform, voor een ander doeleinde dan de voorwaarden voor de ridesharing af te spreken;
* (viii) geen betalingen buiten het Platform of de Hyperwallet-betaalmethode te aanvaarden of uit te voeren;
* (ix) deze AV’s na te leven; en
* (x) de algemene voorwaarden van de Busmaatschappij die het buskaartje dat u hebt geboekt aanbiedt, na te leven.

### **8.2.    Verbintenissen van de Bestuurder**

Voorts belooft u bij het gebruik van het Platform als Bestuurder:

* (i) alle wetten, voorschriften en richtlijnen na te leven die van toepassing zijn op het autorijden en het voertuig, en dan meer bepaald om op het moment van de Carpoolingrit een burgerlijke aansprakelijkheidspolis te hebben onderschreven en in het bezit te zijn van een geldig rijbewijs;
* (ii) te controleren of uw verzekering ridesharing dekt en of uw Passagiers worden beschouwd als derden in uw voertuig en daardoor door uw verzekering gedekt zijn;
* (iii) geen risico’s te nemen tijdens het rijden en geen producten te nemen die uw aandacht en uw vermogen om op een oplettende en volledig veilige manier het voertuig te besturen, zouden kunnen aantasten;
* (iv) alleen Carpoolpublicaties te plaatsen over Ritten die echt zijn gepland;
* (v) de Carpoolrit te maken zoals die in de Publicatie wordt beschreven (en dan meer bepaald met betrekking tot het al dan niet gebruiken van de snelweg) en de tijdstippen en de plaatsen te respecteren die met de andere Leden werden overeengekomen (en dan meer bepaald met betrekking tot de ontmoetingsplaats en het afzetpunt);
* (vi) niet meer Passagiers mee te nemen dan het aantal Zitplaatsen dat in de Carpoolpublicatie wordt vermeld;
* (vii) een voertuig te gebruiken dat zich in een goede staat bevindt en die in lijn is met de toepasselijke wettelijke bepalingen en gewoonten, in het bijzonder met een geldig APK-keuringsrapport;
* (viii) aan BlaBlaCar of iedere Passagier die dat vraagt, uw rijbewijs, het inschrijvingsbewijs van uw auto, uw verzekeringspolis, uw APK-keuringsrapport en ieder ander document voor te leggen dat uw bevoegdheid aantoont om het voertuig als Bestuurder op het Platform te gebruiken;
* (ix) uw Passagiers onmiddellijk op de hoogte te brengen van een eventuele vertraging of verandering van tijdstip of Carpoolrrit;
* (x) bij een grensoverschrijdende Carpoolrit alle documenten bij te houden en beschikbaar te stellen voor de Passagier en iedere andere instantie, die uw identiteit en uw recht om de grens over te steken, aantonen;
* (xi) tot minstens 15 minuten na het afgesproken tijdstip te wachten op de Passagiers;
* (xii) geen Publicatie te plaatsen met betrekking tot een voertuig waarvan u niet de eigenaar bent of dat u niet mag gebruiken om ritten te delen;
* (xiii) ervoor te zorgen dat uw Passagiers contact met u kunnen opnemen op het telefoonnummer dat in uw profiel vermeld is;
* (xiv) geen winst te boeken door gebruik te maken van het Platform;
* (xv) dat voor u geen medische tegenindicatie van toepassing is of dat u niet om medische redenen ongeschikt bent om met de auto te rijden;
* (xvi) om ervoor te zorgen dat uw Bestuurder telefonisch contact met u kan opnemen op het nummer dat in uw profiel is geregistreerd, ook op het ontmoetingspunt.

### **8.3.    Verbintenissen van de Passagiers**

**8.3.1 Voor Carpoolrit**

Wanneer u het Carpoolplatform als Passagier gebruikt, belooft u om:

* (i) u op een gepaste manier te gedragen tijdens de Carpoolrit, zodat de concentratie of het rijgedrag van de Bestuurder of de rust van de andere Passagiers niet wordt verstoord;
* (ii) het voertuig en de netheid van het voertuig van de Bestuurder te respecteren;
* (iii) de Bestuurder onmiddellijk op de hoogte te brengen als u niet op tijd op de plaats van afspraak zult zijn;
* (iv) op de plaats van afspraak tot minstens 15 minuten na het afgesproken tijdstip op de Bestuurder te wachten;
* (v) aan BlaBlaCar of een Bestuurder die dat vraagt, uw identiteitskaart of een ander document dat uw identiteit aantoont, voor te leggen;
* (vi) tijdens een Carpoolrit geen artikel, goederen, product of dier mee te nemen dat het rijgedrag en de concentratie van de Bestuurder zou kunnen verstoren of waarvan de aard, het eigendom of het vervoer in strijd is met de geldende wettelijke voorschriften;
* (vii) bij een grensoverschrijdende Carpoolrit ieder document waarmee uw identiteit en uw recht om de grens over te steken, kan worden aangetoond, bij te houden en beschikbaar te houden voor de Bestuurder en iedere instantie die daarom vraagt;
* (viii) ervoor te zorgen dat uw andere Passagiers contact met u kunnen opnemen via het telefoonnummer dat op uw profiel ingeschreven is – ook op de plaats van afspraak.

Indien u een Boeking hebt uitgevoerd voor één of meer Zitplaatsen in naam van derden, garandeert u – in overeenstemming met de bepalingen van Artikel 4.2.3 hierboven – dat deze derde de bepalingen van dit artikel en algemeen gesteld onderhavige AV’s zal naleven. BlaBlaCar behoudt zich het recht voor om uw Profiel op te schorten, uw toegang tot de Diensten te beperken of onderhavige AV’s op te zeggen in het geval van een inbreuk door de derde in wiens naam u een Zitplaats conform onderhavige AV’s hebt geboekt.

**8.3.2 Voor een Busrit**

In het kader van een busrit verbindt de passagier zich ertoe de Algemene Verkoopvoorwaarden van de betrokken Busmaatschappij na te leven.

**8.4 Melden van ongepaste of illegale content (Kennisgeving en actie mechanisme).**

U kunt verdachte, ongepaste of illegale Content van Leden of Berichten melden zoals [hier beschreven](https://support.blablacar.com/s/article/Illegale-inhoud-melden-1729197120603?language=nl_NL).

BlaBlaCar zal na passend te zijn geïnformeerd conform dit Hoofdstuk of door de bevoegde autoriteiten, onmiddellijk alle onwettige Content voor Leden verwijderen als:

* De Content voor Leden duidelijk onwettig of in strijd is met toepasselijke regelgeving; of
* BlaBlaCar van mening is dat dergelijke Content inbreuk maakt op deze AV’s.

In dergelijke gevallen behoudt BlaBlaCar zich het recht voor de Content voor Leden te verwijderen nadat deze correct en op een voldoende gedetailleerde en duidelijke manier is gemeld en/of het gemelde Profiel onmiddellijk verwijderen.

Het betreffende Lid kan bezwaar maken tegen de bovenstaande beslissing van BlaBlaCar zoals beschreven in Hoofdstuk 15.1 hieronder.

**9.    Beperkingen in verband met het gebruik van de Platforms, opschorting van profielen, beperking van toegang en opzegging**
--------------------------------------------------------------------------------------------------------------------------------

U kunt uw contractuele relaties met BlaBlaCar kosteloos en zonder opgave van reden op eender welk moment opzeggen. Ga daarvoor naar de tab “Profiel afsluiten” op uw Profielpagina.

Bij een (i) inbreuk door u van onderhavige AV’s, met inbegrip van maar niet beperkt tot uw verplichtingen als Lid die in de Artikelen 6 en 8 hierboven worden vermeld, (ii) overschrijding van de grenswaarde die in Artikel 4.3.3 hierboven wordt uiteengezet of (iii) als BlaBlaCar gegronde redenen heeft om aan te nemen dat dit nodig is om haar eigen veiligheid en integriteit of de veiligheid en integriteit van haar Leden of derden te vrijwaren of om fraude of onderzoeken te vermijden, behoudt BlaBlaCar zich het recht voor om:

* (i) onmiddellijk en zonder voorafgaand bericht onderhavige AV’s die u met BlaBlaCar verbinden, op te zeggen en/of
* (ii) het plaatsen van Content van Leden die door u op het Platform werd gepost, te blokkeren of te verwijderen en/of
* (iii) uw toegang tot en uw gebruik van het Platform te beperken en/of
* (iv) uw Profiel tijdelijk of permanent op te schorten.

Opschorting van het Profiel kan in voorkomende gevallen ook betekenen dat u geen lopende uitbetalingen ontvangt.

Indien van toepassing kan BlaBlaCar u ook een herinnering sturen aan de verplichting om te voldoen aan toepasselijke wetgeving en deze AV’s.

Wanneer dit nodig is, zult u in kennis worden gesteld van zo’n maatregel, zodat u kunt bevestigingen dat u toepasselijke wetgeving en deze AV’s zult naleven, u de kans krijgt om uitleg te geven aan BlaBlaCar of bezwaar kunt maken tegen de beslissing van BlaBlaCar. BlaBlaCar zal rekening houdend met de omstandigheden van elk afzonderlijk geval en de ernst van de schending, naar eigen goeddunken beslissen om de ingevoerde maatregelen al dan niet op te heffen.

**10.    Persoonsgegevens**
---------------------------

In de context van uw gebruik van het Platform zal BlaBlaCar enkele persoonsgegevens verzamelen en verwerken zoals beschreven in het [Privacybeleid.](https://blog.blablacar.nl/about-us/privacy-policy)

**11.    Intellectuele eigendom**

### **11.1.    Content die door BlaBlaCar werd gepubliceerd**

Rekening houdend met de content die door haar Leden beschikbaar gesteld wordt, is BlaBlaCar de enige houder van alle intellectuele eigendomsrechten met betrekking tot de Diensten, het Platform en de content ervan (en dan meer bepaald teksten, beelden, designs, logo’s, video’s, geluiden, gegevens en grafische elementen) en de software en de databases die de werking ervan garanderen.

BlaBlaCar kent u een niet-exclusief, persoonlijk en niet-overdraagbaar recht toe om van het Platform en de Diensten gebruik te maken voor uw persoonlijke en private gebruik op een niet-commerciële basis en in overeenstemming met de doelstellingen van het Platform en de Services.  
Het is verboden om het Platform en de Diensten en hun content op enige andere manier te gebruiken of te exploiteren, zonder dat BlaBlaCar daarvoor vooraf haar schriftelijke toestemming heeft verleend. Het is meer bepaald verboden om:

* (i) het Platform, de Diensten en de content te reproduceren, te wijzigen, aan te passen, te verdelen, in het openbaar te vertegenwoordigen en te verspreiden, met uitzondering van wat uitdrukkelijk wordt toegestaan door BlaBlaCar;
* (ii) het Platform of de Diensten te ontleden en er ‘reverse engineering’ op toe te passen, behalve in de uitzonderlijke gevallen die door de geldende teksten worden bepaald;
* (iii) een substantieel deel van de gegevens van het Platform te extraheren of te proberen te extraheren (meer bepaald door het gebruik van ‘data mining robots’ of andere vergelijkbare verzameltools).

### **11.2. Content die door u op het Platform werd gepost**

Om de Verlening van de Diensten mogelijk te maken in overeenstemming met de doelstelling van het Platform, kent u BlaBlaCar een niet-exclusieve toelating toe om de content en de gegevens die u bezorgt in de context van uw gebruik van de Diensten, waaronder uw reserveringsverzoeken, Carpoolpublicaties en commentaren daarin, biografieën, foto’s, beoordelingen en antwoorden op beoordelingen(hierna de “**Content voor Leden**” genoemd) en Berichten, te gebruiken. Om BlaBlaCar toe te laten om via het digitale netwerk en in overeenstemming met het communicatieprotocol (en dan meer bepaalde het internet en het mobiele netwerk) en om de content van het Platform aan het publiek beschikbaar te stellen, laat u BlaBlaCar toe om uw Content voor Leden voor de hele wereld en tijdens de volledige duur van uw contractuele relaties met BlaBlaCar als volgt te reproduceren, weer te geven, aan te passen en te vertalen:

(i) u staat BlaBlaCar toe om uw Content voor Leden geheel of gedeeltelijk te reproduceren op eender welk bekend of nog niet bekend mediamiddel, en dan meer bepaald op iedere server, geheugenkaart of ieder ander gelijkwaardig medium, in eender welk formaat en door eender welk bekend of nog niet bekend proces, in de mate die nodig is om iedere opslag, back-up, verzending of download in verband met de werking van het Platform en de verlening van de Diensten mogelijk te maken;

(ii) u staat BlaBlaCar toe om uw Content voor Leden aan te passen en te vertalen en om die aanpassingen te reproduceren op eender welk bestaand of toekomstig medium, zoals bepaald in punt (i) hierboven, met de bedoeling om de Diensten meer bepaald in verschillende talen aan te bieden. Dit recht omvat meer bepaald de optie om wijzigingen door te voeren aan de formattering van uw Content voor Leden, met inachtneming van uw morele recht met de bedoeling om het grafische charter van het Platform te respecteren en/of om het technisch compatibel te maken met oog op de publicatie ervan via het Platform.

U blijft verantwoordelijk voor en hebt alle rechten voor Content voor Leden en Berichten die u upload op ons Platform. 

**12\. Rol van BlaBlaCar**
--------------------------

Het Platform vormt een online netwerkplatform waarop de Leden Carpoolpublicaties voor Carpoolritten kunnen aanmaken en plaatsen met het oog op ridesharing, en buskaartjes van Busmaatschappijen kunnen boeken. Deze Carpoolpublicaties kunnen meer bepaald worden bekeken door de andere Leden, die op die manier de voorwaarden van de Rit kunnen bekijken en in voorkomend geval rechtstreeks een Zitplaats kunnen boeken in het voertuig in kwestie met het Lid dat de Publicatie op het Platform heeft gepost. Het Platform biedt ook de mogelijkheid om Zitplaatsen voor Busreizen te boeken.

Door het Platform te gebruiken en onderhavige AV’s te aanvaarden, erkent u dat BlaBlaCar geen partij vormt in eender welke overeenkomst tussen u en de andere Leden met het oog op het delen van de kosten die verband houden met een Rit bij carpooling, noch in eender welke overeenkomst tussen u en de Busmaatschappij voor het vervoer van passagiers per bus.

BlaBlaCar heeft geen controle over het gedrag van haar Leden, de Busmaatschappijen en hun vertegenwoordigers, en de gebruikers van het Platform. BlaBlaCar is geen eigenaar, exploitant, leverancier of beheerder van de voertuigen die het voorwerp vormen van de Publicaties en ze biedt ook geen Ritten aan op het Platform.

U erkent en aanvaardt dat BlaBlaCar de geldigheid, waarheidsgetrouwheid of wettelijkheid van de Publicaties en aangeboden Zitplaatsen en Ritten controleert. In haar hoedanigheid van tussenpersoon voor ridesharing biedt BlaBlaCar geen transportdiensten aan en treedt ze ook niet op als vervoerbedrijf; de rol van BlaBlaCar blijft beperkt tot het vergemakkelijken van de toegang tot het Platform.

In het kader van Carpoolritten handelen de Leden (Bestuurders of Passagiers) op hun eigen exclusieve en volledige verantwoordelijkheid.

In haar hoedanigheid van tussenpersoon kan BlaBlaCar niet aansprakelijk worden gesteld voor het plaatsvinden van een Rit, meer bepaald ten gevolge van:

  (i) onjuiste informatie die door de Bestuurder of de Busmaatschappij werd gemeld in zijn/haar Publicatie of via enig ander communicatiemiddel met betrekking tot een Rit;

  (ii) annulering of wijziging van een Rit door een Lid of een Busmaatschappij;

  (iii) het gedrag van haar Leden tijdens, vóór of na de Rit of een verplaatsing per bus.

**13.    Werking, beschikbaarheid en functionaliteiten van het Platform**
-------------------------------------------------------------------------

BlaBlaCar probeert in de mate van het mogelijke het Platform 7 dagen per week en 24 uur per dag toegankelijk te houden. Het is evenwel mogelijk dat de toegang tot het Platform zonder voorafgaand bericht tijdelijk wordt opgeschort wegens technisch onderhoud, migratie- of update-operaties of storingen of beperkingen die verband houden met de werking van het netwerk.

BlaBlaCar behoudt zich bovendien het recht voor om het Platform of delen daarvan te wijzigen voor bepaalde gebruikers om nieuwe functies te testen en een betere gebruikerservaring te bieden, evenals om de toegang tot het Platform of de functionaliteiten daarvan geheel of gedeeltelijk, naar eigen goeddunken, tijdelijk of permanent, te wijzigen of op te schorten.

**14.    Wijziging van de AV’s**
--------------------------------

Onderhavige AV’s en de documenten die er door verwijzing in opgenomen zijn, vormen de volledige overeenkomst tussen u en BlaBlaCar met betrekking tot uw gebruik van de Diensten. Ieder ander document, en dan meer bepaald iedere vermelding op het Platform (FAQ enz.) is uitsluitend als richtlijn bedoeld.

BlaBlaCar mag onderhavige AV’s wijzigen om ze aan te passen aan de technologische en commerciële omgeving en om ze in overeenstemming te brengen met de geldende wetgeving. Iedere wijziging aan onderhavige AV’s zal op het Platform worden gepubliceerd met vermelding van de datum waarop ze van kracht wordt en u zult daarvan door BlaBlaCar op de hoogte worden gebracht voordat de wijziging van kracht wordt.

**15.    Toepasselijk recht en geschillen**
-------------------------------------------

Onderhavige AV’s zijn in het Nederlands opgesteld en zijn onderworpen aan het Nederlandse recht.

**15.1 Intern klachtenverwerkingssysteem**

U kunt bezwaar maken tegen beslissingen die wij nemen betreffende:

* Content van Leden: bijvoorbeeld wij hebben Content van Leden die u hebt gepubliceerd bij het gebruik van het Platform verwijderd, de zichtbaarheid ervan beperkt of geweigerd deze content te verwijderen, of
* Uw Profiel: we hebben uw toegang tot het Platform opgeschort,

in geval wij een dergelijke beslissing hebben genomen op basis van het feit dat de Content van Leden illegale content vormt of in strijd is met deze AV’s. De bezwaarprocedure wordt beschreven in [deze link](https://support.blablacar.com/s/article/Bezwaar-maken-tegen-verwijdering-van-inhoud-of-accountschorsing-1729197123035?language=nl_NL).

**15.2 Buitengerechtelijke geschiloplossing**

Indien nodig kunt u uw klachten met betrekking tot ons Platform of onze Diensten voorleggen op het platform voor de oplossing van geschillen dat door de Europese Commissie online werd geplaatst en dat [hier](https://ec.europa.eu/consumers/odr/main/?event=main.home2.show) bereikbaar is. De Europese Commissie zal dan uw klacht naar de bevoegde nationale ombudsman doorsturen. In overeenstemming met de regels die gelden voor bemiddeling, dient u, voordat u een aanvraag voor bemiddeling indient, BlaBlaCar schriftelijk op de hoogte te brengen van het geschil met de bedoeling om een oplossing in der minne te verkrijgen.

U kunt ook een verzoek indienen bij een instantie voor geschiloplossing in uw land (de lijst is [hier te vinden)](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show).

**16.    Wettelijke berichten**
-------------------------------

Het Platform wordt gepubliceerd door Comuto SA, een vennootschap met beperkte aansprakelijkheid met een aandelenkapitaal van € 166,552.163, die ingeschreven is in het handelsregister van Parijs onder nummer 491.904.546 (btw-nummer: FR76491904546) en waarvan het hoofdkantoor gevestigd is in 84, avenue de la République, 75011 Parijs (Frankrijk), vertegenwoordigd door haar Chief Executive Officer Nicolas Brusso, de publicatiedirecteur van de website.

De Website wordt gehost op de servers van Google Ireland Limited, Gordon House, Barrow Street, Dublin 4 (Ierland).

Comuto SA is ingeschreven in het register van reis- en verblijforganisatoren onder het volgende nummer: IM075180037

De financiële garantie wordt verstrekt door: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Parijs – Frankrijk.

De beroepsaansprakelijkheidsverzekering is onderschreven bij: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Parijs – Frankrijk.

Comuto SA is ingeschreven in het register van verzekeringstussenpersonen, banken en financiën met registratienummer (Orias) 15003890.

Met dit [contactformulier](https://support.blablacar.com/s/contactsupport?language=nl_NL) kunt u met al uw vragen bij Comuto SA terecht.

17\. Digital Services Act (Digitaledienstenverordening)  

----------------------------------------------------------

**17.1.  Informatie over gemiddelde actieve afnemers van de dienst in de EU**

Conform artikel 24(2) van de verordening van het Europese Parlement en de Raad betreffende de interne markt voor digitale diensten ter wijziging van Richtlijn 2000/31/EG (“DSA”), moeten online platform providers informatie publiceren over het maandelijkse gemiddelde van actieve afnemers van hun diensten in de EU berekend als een gemiddelde van de afgelopen zes maanden. Het doel is te bepalen of de dienstverleners voldoen aan het criterium voor “zeer grote online platform dienstverleners” onder de DSA, d.w.z. dienstverleners met een maandelijks gemiddelde van meer dan 45 miljoen actieve afnemers in de EU.

Vanaf 17 februari 2025 was het maandelijkse gemiddelde van actieve afnemers van BlaBlaCar voor de periode van augustus tot en met januari 2025 berekend rekening houdend met Recital 77 en artikel 3 van de DSA (inclusief gebruikers die slechts één keer in een periode van 6 maanden aan de inhoud van het platform worden blootgesteld), voordat een specifieke verordening werd aangenomen, ongeveer 3,75 miljoen in de EU.

Deze informatie wordt uitsluitend gepubliceerd om te voldoen aan de vereisten van de DSA en moet niet worden gebruikt voor andere doeleinden. De informatie wordt minstens elke zes maanden bijgewerkt. De manier waarop wij deze informatie berekenen kan evolueren of kan wijzigingen vereisen, bijvoorbeeld vanwege productwijzigingen of nieuwe technologieën.

**17.2. Contactpunt voor autoriteiten**

Conform artikel 11 van de DSA, kunt u als u lid bent van een bevoegde EU-autoriteit, de Europese Commissie of de Europese Raad voor digitale diensten contact met ons opnemen betreffende DSA-aangelegenheden via het e-mailadres [\[email protected\]](https://blog.nl.blablacar.be/cdn-cgi/l/email-protection).

U kunt in het Engels en Frans contact met ons opnemen.

Wij wijzen erop dat dit e-mailadres niet bedoeld is door communicatie met Leden. Met vragen over het gebruik van BlaBlaCar kunt u als Lid contact opnemen via dit [contactformulier](https://support.blablacar.com/s/contactsupport?language=nl_NL).

**18\. Gebruik van BlaBlaBono Energético (alleen voor Spanje) – van toepassing vanaf 17 januari 2025**
------------------------------------------------------------------------------------------------------

Passagiers die in aanmerking komen voor het BlaBlaBono Energético-aanbod op het platform  [www.blablacar.es](http://www.blablacar.es/), hebben de optie om gebruik te maken van hun BlaBlaBono-bonus door een carpoolrit in Spanje te boeken via [www.blablacar.be](http://www.blablacar.be/) of via de app in Nederland, onderhevig aan de gebruiksvoorwaarden van deze bonus die [hier](https://blog.blablacar.nl/about-us/terms-and-conditions?_gl=1*ku9iqf*_gcl_au*MTA4MTk1MTk1OS4xNzMyNzg4NjE5) te vinden zijn.

* * *

Algemene voorwaarden van kracht vanaf 17 februari 2024

**1.    Onderwerp**
-------------------

Comuto SA (hierna “**BlaBlaCar**” genoemd) heeft een rideshareplatform ontwikkeld dat op de website [www.blablacar.nl](http://www.blablacar.nl/) of in de vorm van een mobiele app toegankelijk is en dat bedoeld is (i) om autobestuurders die naar een bepaalde bestemming rijden, in contact te brengen met passagiers die in dezelfde richting reizen, zodat ze de rit en de bijbehorende kosten kunnen delen, en (ii) om kaartjes te boeken voor busritten die door Busmaatschappijen worden uitgevoerd (hierna het “**Platform**” genoemd).

Deze algemene voorwaarden hebben ten doel om de toegang tot en de voorwaarden voor het gebruik van het Platform te regelen. Lees deze algemene voorwaarden aandachtig. U begrijpt en erkent dat BlaBlaCar geen partij vormt in een overeenkomst, contract of contractuele verhoudingen van welke aard dan ook tussen de Leden van het Platform of tussen u en de Busmaatschappij.

Als u op “Inloggen met Facebook” of “Registreren met een mailadres” klikt, erkent u al deze algemene voorwaarden te hebben gelezen en te aanvaarden.

Merk op dat, als u uw profiel gebruikt om in te loggen op het BlaBlaCar-platform van een ander land (bijvoorbeeld www.blablacar.com.br), (i) de algemene voorwaarden van dat platform, (ii) het privacybeleid van dat platform en (iii) de statutaire regels en verordeningen van dat land van toepassing zullen zijn. Dit betekent ook dat de informatie van uw Profiel, inclusief persoonsgegevens, mogelijk moet worden overgedragen naar de juridische entiteit die dat andere platform beheert. BlaBlaCar behoudt zich het recht voor om de toegang tot het platform te beperken voor gebruikers van wie redelijkerwijs kan worden vastgesteld dat ze zich buiten het grondgebied van de Europese Unie bevinden.

**2.    Definities**
--------------------

In onderhavig document hebben de volgende termen de volgende betekenis:

“**Algemene Verkoopvoorwaarden**” betekent de [Algemene Verkoopvoorwaarden](https://blog.nl.blablacar.be/about-us/algemene-verkoopvoorwaarden) van de betreffende Busmaatschappij afhankelijk van de Busrit geselecteerd door de Klant, en de bijzondere voorwaarden, beschikbaar op de Website, en die de Klant heeft gelezen vóór zijn bestelling.

“**AV’s**” betekent onderhavige Algemene Voorwaarden;

“**Bijdrage in de Kosten**” betekent voor een bepaalde Carpoolrit de som die door de Bestuurder wordt gevraagd en die door de Passagier wordt aanvaard als bijdrage van de reiskosten;

“**BlaBlaCar**” heeft de betekenis die eraan wordt gegeven in artikel 1 hierboven;

“**Bestelling**“: de handeling waarbij de klant Diensten bij BlaBlaCar Bus reserveert, ongeacht de gebruikte reserveringsmethode, met als enige uitzondering de aankoop van Tickets rechtstreeks bij een Verkooppunt, en die voor de Passagier of eventueel een Agent de verplichting met zich meebrengt om de prijs voor de desbetreffende Diensten te betalen;

“**Bestuurder**” betekent het Lid dat gebruikmaakt van het Platform om een andere persoon vervoer aan te bieden in ruil voor een Bijdrage aan de Kosten voor een Rit en op een tijdstip die alleen door de Bestuurder worden gedefinieerd;

“**Bevestiging van boeking**” heeft de betekenis die eraan wordt gegeven in Artikel 4.2.1 hieronder;

“**Boeking**” heeft de betekenis die eraan is toegekend in Artikel 4.2.1. hieronder;

“**Busmaatschappij**” verwijst naar een maatschappij die passagiers professioneel vervoert en tickets voor wier reizen door BlaBlaCar op het Platform worden verspreid;

“**Buspublicatie**” betekent een publicatie over een busrit van een een Busmaatschappij die op het Platform wordt gepost;

“**Busrit**” betekent het reisonderwerp van een Buspublicatie op het Platform en waarvoor de Busmaatschappij zitplaatsen in de busvoertuigen voorstelt in ruil voor de Prijs;

“**Carpoolpublicatie**” betekent een publicatie betreffende een Rit die door een Bestuurder op het Platform is geplaatst;

“**Carpoolrit**” betekent de verplaatsing die het voorwerp uitmaakt van een Carpoolpublicatie die door een Bestuurder op het Platform werd gepubliceerd en waarvoor hij zich akkoord verklaart om Passagiers te vervoeren in ruil voor de Bijdrage in de Kosten;

“**Content voor Leden**” heeft de betekenis die eraan wordt gegeven in Artikel 11.2 hieronder;

“**Diensten**” betekent alle diensten die via het Platform door BlaBlaCar worden verleend;

“**Deel van de rit**” heeft de betekenis die eraan wordt gegeven in Artikel 4.1 hieronder;

“**Facebook-Profiel**” heeft de betekenis die eraan wordt gegeven in Artikel 3.2 hieronder;

“**Hyperwallet-betaalmethode**” heeft de betekenis die er in onderstaand artikel 5.4.2.a aan wordt toegekend;

“**Klant**” betekent elke fysieke persoon (Lid of geen lid) die, hetzij voor zichzelf, hetzij namens iemand anders die de Passagier zal zijn, een Busticket koopt via het Platform om een Rit te maken die wordt uitgevoerd door de Busmaatschappij;

“**Lid**” betekent een persoon die een Profiel heeft aangemaakt op het Platform;

“**Passagier**” betekent het Lid dat het aanbod van de Bestuurder om te worden vervoerd, heeft aanvaard of, indien dit van toepassing is, de persoon in wiens naam een Lid een Zitplaats heeft geboekt;

“**Platform**” heeft de betekenis die eraan wordt gegeven in Artikel 1 hierboven;

“**Prijs**” verwijst, voor een bepaalde Busrit, naar de prijs inclusief alle belastingen, vergoedingen en kosten van relevante diensten inbegrepen, betaald door de Klant op het Platform, op het moment van de validatie van de Bestelling, voor een Zitplaats op een bepaalde Busrit;

“**Profiel**” betekent het profiel dat moet worden aangemaakt om Lid te worden en om toegang te krijgen tot bepaalde diensten die door het Platform worden aangeboden;

“**Provider Hyperwallet-betaalmlethode**” betekent PayPal (Europe) S.à r.l. et Cie, S.C.A.;

“**Publicatie**” betekent een publicatie over een Rit die door een Bestuurder op het Platform wordt gepost;

“**Rit**” verwijst naar een Busrit of een Carpoolrit;

“**Ticket**“: het nominatiefve geldigee vervoersbewijs dat aan de consument wordt verstrekt na het boeken van een busrit, als bewijs van de bestaande vervoersovereenkomst tussen de passagier en de Busmaatschappij, waarop de algemene verkoopvoorwaarden van toepassing zijn, onverminderd eventuele aanvullende bijzondere voorwaarden die tussen de passagier en de Busmaatschappij zijn bedongen en waarnaar op het Vervoerbewijs wordt verwezen; 

“**Verkooppunt**” verwijst naar de fysieke loketten en terminals vermeld op de Website waar de Tickets te koop worden aangeboden;

“**Vergoeding voor de Service**” heeft de betekenis die eraan wordt gegeven in Artikel 5.2 hieronder;

“**Vervoerdiensten**” zijn de vervoerdiensten waarop een Busreiziger zich heeft ingeschreven en die door de Busmaatschappij worden verleend;

“**Website**” betekent de website die toegankelijk is op het adres [www.blablacar.nl](http://www.blablacar.nl/);

“**Zitplaats**” betekent de plaats die door een Passagier in het voertuig van de Bestuurder of de Bus van de Busmaatschappij wordt geboekt.

**3.    Registratie op het Platform en aanmaken van een Profiel**
-----------------------------------------------------------------

### **3.1.    Voorwaarden voor registratie op het Platform**

Het Platform mag worden gebruikt voor personen van 18 jaar of ouder. Registratie op het platform door een minderjarige is ten strengste verboden. Door het Platform te bezoeken of te gebruiken of door u erop te registreren, verklaart en garandeert u dat u minstens 18 bent.

### **3.2.    Aanmaken van een Profiel**

Het Platform biedt Leden de mogelijkheid om Carpoolpublicaties te posten en te bekijken en om interactie te voeren met anderen met de bedoeling om een Zitplaats te boeken. U kunt de Publicaties bekijken als u niet op het Platform geregistreerd bent. Maar u kunt pas een Carpoolpublicatie plaatsen of een Zitplaats boeken nadat u een Profiel hebt aangemaakt en nadat u Lid bent geworden.

U kunt op de volgende manieren een Profiel aanmaken:

* (i) alle verplichte velden van het registratieformulier in te vullen;
* (ii) of via ons Platform inloggen op uw Facebook-account (hierna uw “**Facebook-profiel**” genoemd). Door deze functionaliteit te gebruiken, begrijpt u dat BlaBlaCar toegang zal hebben tot bepaalde informatie op uw Facebook-profiel en dat ze deze op het Platform zal delen en zal bijhouden. U kunt de link tussen uw Profiel en uw Facebook-profiel op eender welk moment wissen via de rubriek “Verificatie” van uw profiel. Als u meer wilt weten over het gebruik van uw gegevens van uw Facebook-profiel, kunt u ons [Privacybeleid](https://www.blablacar.nl/about-us/privacy-policy) en dat van Facebook lezen.

Als u zich op het Platform wilt registreren, moet u deze AV’s hebben gelezen en begrepen.

Wanneer u uw Profiel – via welke methode dan ook – hebt aangemaakt, stemt u ermee in om juiste en ware informatie te bezorgen en om die via uw profiel of door contact op te nemen met BlaBlaCar te updaten, zodat de relevantie en de juistheid van die gegevens tijdens de volledige geldigheidsduur van uw contractuele relaties met BlaBlaCar gegarandeerd zijn.

Indien u zich per mail registreert, stemt u ermee in om het wachtwoord dat u bij het aanmaken van uw Profiel hebt ingevoerd, geheim te houden en het aan niemand anders mee te delen. Als u uw wachtwoord verliest of bekend maakt, moet u BlaBlaCar daarvan onmiddellijk op de hoogte brengen. Alleen u bent verantwoordelijk voor het gebruik van uw Profiel door derden, tenzij u BlaBlaCar vooraf uitdrukkelijk in kennis hebt gesteld van het verlies, het bedrieglijke gebruik door een derde of de bekendmaking van uw wachtwoord aan een derde.

U stemt ermee in om geen andere Profielen dan het Profiel dat u aanvankelijk zelf hebt aangemaakt, aan te maken of te gebruiken onder uw eigen identiteit of de identiteit van een derde.

### **3.3.    Verificatie**

BlaBlaCar kan met het oog op de transparantie, de verbetering van het vertrouwen en de preventie of de detectie van fraude een systeem invoeren waarmee ze een deel van de informatie die u op uw profiel geeft, kan verifiëren. Dit geldt meer bepaald wanneer u uw telefoonnummer invoert of ons een identiteitsbewijs verstrekt.

U erkent en aanvaardt dat iedere verwijzing op het Platform of de Diensten naar “gecontroleerde” informatie of een vergelijkbare term alleen betekent dat een Lid de verificatieprocedure op het Platform of de Diensten met succes heeft doorlopen met de bedoeling om u meer informatie te bezorgen over het Lid met wie u overweegt een rit te maken. BlaBlaCar kan de waarheidsgetrouwheid, de betrouwbaarheid of de geldigheid van de informatie die het onderwerp is van de verificatieprocedure, niet garanderen.

**4.    Gebruik van de Diensten**
---------------------------------

### **4.1.    Publicaties posten**

Als Lid en op voorwaarde dat u aan de voorwaarden hieronder voldoet, kunt u Carpoolpublicaties aanmaken en op het Platform posten door informatie in te voeren over de Carpoolrit die u van plan bent om te maken (datums/ tijdstippen en oppikpunten en aankomst, aantal Zitplaatsen aangeboden, beschikbare opties, bedrag van de Bijdrage in de Kosten enz.).

Wanneer u een Carpoolpublicatie plaatst, kunt u de belangrijkste steden vermelden waar u wilt stoppen of waar u Passagiers wilt oppikken of afzetten. De delen van de Carpoolrit tussen die belangrijke steden of tussen een van die belangrijke steden en het vertrekpunt of de bestemming van de Carpoolrit worden “**Delen van de rit**” genoemd.

U mag een Carpoolpublicatie alleen posten als u aan alle ondergenoemde voorwaarden voldoet:

  (i) u bent in het bezit van een geldig rijbewijs;

  (ii) u biedt alleen Carpoolpublicaties aan voor voertuigen waarvan u zelf de eigenaar bent of die u met de uitdrukkelijke toestemming van de eigenaar mag gebruiken en in ieder geval moet u het voertuig voor ridesharedoeleinden mogen gebruiken;

  (iii) u bent en u blijft de hoofdbestuurder van het voertuig dat het voorwerp uitmaakt van de Carpoolpublicatie;

  (iv) er is voor het voertuig een geldige burgerlijke aansprakelijkheidsverzekering afgesloten;

  (v) er gelden voor u geen medische contra-indicaties en u bent ook niet om medische redenen onbevoegd verklaard om met de auto te rijden;

  (vi) het voertuig dat u van plan bent om te gebruiken voor de Rit, is een personenauto met 4 wielen en maximaal 7 zitplaatsen, met uitsluiting van auto’s waar geen rijbewijs voor nodig is om erin te rijden.

  (vii) u bent niet van plan om voor dezelfde Rit een andere Carpoolpublicatie op het Platform te posten;

  (vii) u bent niet van plan om voor dezelfde Rit een andere Publicatie op het Platform te posten;

  (viii) u biedt niet meer Zitplaatsen aan dan het aantal beschikbare Zitplaatsen in uw voertuig;

  (ix) alle Zitplaatsen die worden aangeboden, zijn uitgerust met een veiligheidsgordel – zelfs als het voertuig goedgekeurd is met Zitplaatsen zonder veiligheidsgordel;

  (x) u maakt gebruik van een voertuig dat zich in een goede staat bevindt en dat beantwoordt aan de toepasselijke wettelijke voorschriften en gewoonten, in het bijzonder met een geldig APK-keuringsrapport;

  (xi) u bent een consument en handelt niet als professional..

U erkent dat u als enige verantwoordelijk bent voor de content van de Publicatie die u op het Platform post. U verklaart en garandeert dan ook dat alle informatie in uw Publicatie juist en waarheidsgetrouw is en dat u van plan bent om de Rit te maken onder de voorwaarden die in uw Publicatie worden beschreven.

Uw Carpoolpublicatie wordt op het Platform gepost en is daardoor zichtbaar voor Leden en alle bezoekers (zelfs niet-leden) die een zoekopdracht uitvoeren op het Platform of op de website van de partners van BlaBlaCar. BlaBlaCar behoudt zich het recht voor om naar eigen goeddunken een Carpoolpublicatie die niet aan de AV’s beantwoordt of die het schadelijk is voor haar imago, het imago van het Platform of het imago van de Diensten, niet te plaatsen of te verwijderen en/of het Profiel van het Lid dat dergelijke Carpoolpublicaties post, op te schorten conform hoofdstuk 9 van deze AV’s.

U wordt ervan op de hoogte gebracht dat als u zich voordoet als een consument door gebruik te maken van het Platform terwijl u in feite handelt als professional, u blootgesteld bent aan sancties volgens de toepasselijke wetgeving.

### **4.2.**    Een Zitplaats boeken

BlaBlaCar heeft een systeem opgezet voor het online reserveren van Zitplaatsen (“Boeking”) voor Ritten die op het Platform worden aangeboden. De methodes voor het reserveren van een Zitplaats hangen af van de aard van de geplande Busrit.

BlaBlaCar biedt gebruikers op het Platform een zoekmachine aan op basis van verschillende zoekcriteria (Plaats van herkomst, bestemming, datums, aantal reizigers, enz.). Bepaalde extra functionaliteiten worden op de zoekmachine aangeboden wanneer de gebruiker ingelogd is op zijn account. BlaBlaCar nodigt de gebruiker uit om, ongeacht het gebruikte boekingsproces, de zoekmachine zorgvuldig te raadplegen en te gebruiken om te bepalen wat het best past bij zijn behoeften. Meer informatie vindt u [hier](https://blog.nl.blablacar.be/about-us/platformtransparantie).  De Klant die een Busrit boekt in een Verkooppunt kan ook de Busmaatschappij of de medewerker aan de balie vragen de zoekopdracht uit te voeren.

**4.2.1 Carpoolrit**

Wanneer een Passagier geïnteresseerd is in een Carpoolpublicatie die gereserveerd kan worden, kan hij online een aanvraag voor de Boeking maken. Deze aanvraag wordt ofwel (i) automatisch aanvaard (indien de Bestuurder deze optie selecteerde wanneer hij zijn Publicatie plaatste) of (ii) handmatig door de Bestuurder goedgekeurd. Op het moment van de boeking betaalt de Passagier online de Bijdrage in de Kosten en de bijbehorende Vergoeding voor de Diensten waar dit van toepassing is. Na ontvangst van de betaling door BlaBlaCar en de validering van de aanvraag van de Boeking door de Bestuurder, waar dit van toepassing is, ontvangt de Passagier een Bevestiging van Boeking (de “**Bevestiging van Boeking**”).

Als u een Bestuurder bent en als u bij het plaatsen van uw Carpoolpublicatie heeft beslist om de aanvragen voor boekingen manueel te verwerken, moet u op ieder verzoek voor Boeking reageren binnen de gegeven termijn, gespecificeerd door de Passagier tijdens de Boeking. Anders vervalt de aanvraag van Boeking automatisch en zullen aan de Passagier alle sommen die hij eventueel op het tijdstip van de aanvraag van boeking heeft betaald, worden terugbetaald.

Op het moment van de Bevestiging van Boeking deelt BlaBlaCar het telefoonnummer van de bestuurder met u (als u de Passagier bent) of van de Passagier (als u de Bestuurder bent), als het Lid ermee heeft ingestemd om zijn/ haar telefoonnummer weer te geven. U bent vanaf dat moment als enige verantwoordelijk voor de uitvoering van de overeenkomst die u met het andere Lid bindt.

**4.2.2 Busrit** 

Voor Busritten maakt BlaBlaCar het mogelijk om Buskaartjes voor een bepaalde Busrit via het Platform te boeken. 

De Vervoersdiensten zijn onderhevig aan de Algemene Verkoopvoorwaarden van de desbetreffende Busmaatschappij, afhankelijk van de door de Klant gekozen Rit, die door de Klant moeten worden aanvaard alvorens de bestelling te plaatsen. BlaBlaCar biedt geen vervoersdiensten aan met betrekking tot Busritten, de Busmaatschappijen zijn de enige partij in de Algemene Verkoopvoorwaarden. U erkent dat de boeking van Zitplaatsen voor een bepaalde Busrit onderhevig is aan de Algemene Verkoopvoorwaarden van de desbetreffende Busmaatschappij. 

BlaBlaCar vestigt uw aandacht op het feit dat bepaalde Vervoersdiensten die door de Busmaatschappij worden aangeboden en op de Website worden vermeld, kunnen worden geannuleerd, met name door weersomstandigheden, seizoensgebonden redenen of in geval van overmacht.     

Ieder gebruik van de Diensten in de hoedanigheid van Passagier of Bestuurder is verbonden met een specifieke naam. De Bestuurder en de Passagier moeten overeenstemmen met de identiteit die aan BlaBlaCar en aan de andere Leden die de Carpoolrit meemaken of de Busmaatschappij, werd vermeld, indien van toepassing.

BlaBlaCar laat zijn Leden wel toe om één of meerdere Zitplaatsen in naam van een derde te boeken. In dat geval moet u op het moment van de boeking, de voornamen, de leeftijd en het telefoonnummer vermelden van de persoon in wiens naam u een Zitplaats boekt. Het is ten strengste verboden om een Zitplaats te boeken voor een minderjarige jonger dan 13 jaar die alleen reist. Indien u een Zitplaats boekt voor een minderjarige ouder dan 13 jaar, moet u vooraf de toestemming van de Bestuurder vragen en hem een behoorlijk ingevulde en ondertekende toestemming van de wettelijke vertegenwoordigers van de minderjarige bezorgen.

Daarnaast is het Platform bedoeld voor het boeken van Zitplaatsen voor personen. Het is verboden om een Zitplaats te boeken om welk voorwerp, pakket, dieren die alleen reizen of materiële artikelen dan ook te vervoeren.

Voorts is het verboden om een Publicatie te publiceren voor een andere Bestuurder dan uzelf.

### **4.3.**    Content voor Leden, moderatie en evaluatiesysteem

**4.3.1.    Werking**

BlaBlaCar moedigt u aan om een beoordeling over een Bestuurder (als u een Passagier bent) of een Passagier (als u een Bestuurder bent) met wie u een Rit hebt gedeeld of met wie u van plan was een rit te delen (d.w.z. een rit te boeken), te plaatsen. U mag geen beoordeling over een andere Passagier plaatsen als u zelf een Passagier was of een beoordeling over een Lid met wie u geen Rit hebt gemaakt of niet van plan was een Rit te maken.

Uw beoordeling en de beoordeling door een ander Lid over u, zijn alleen zichtbaar en worden alleen op het Platform gepubliceerd nadat: (i) u beiden een evaluatie hebt gepubliceerd of (ii) na een periode van 14 dagen na de eerste beoordeling.

U kunt wel nog reageren op een beoordeling die een ander Lid op uw profiel heeft achtergelaten. De beoordeling en uw reactie, indien van toepassing, zullen op uw profiel worden gepubliceerd.

**4.3.2.    Moderatie**

**a. Content voor Leden**

U erkent en aanvaardt dat BlaBlaCar voorafgaand aan publicatie door middel van geautomatiseerde tools of handmatig Content voor Leden zoals gedefinieerd in Hoofdstuk 11.12 kan modereren. Als BlaBlaCar van mening is dat dergelijke Content voor Leden inbreuk maakt op toepasselijke wetgeving of deze AV’s behoudt zij zich het recht voor:

* de Publicatie te voorkomen of dergelijke Content voor Leden te verwijderen,
* het Lid een waarschuwing te sturen met de herinnering aan de verplichting tot naleving van toepasselijke wetgeving of deze AV’s, en/of
* Restrictieve maatregelen te treffen conform Hoofdstuk 9 van deze AV’s.

BlaBlaCar’s gebruik van dergelijke geautomatiseerde tools of handmatige moderatie mag niet worden geïnterpreteerd als een maatregel tot het monitoren van en een verplichting tot het actief zoeken naar onwettige activiteiten en/of Content voor Leden die gepubliceerd wordt op het Platform, en voor zover wettelijk toegestaan leidt dit niet tot enige aansprakelijkheid van BlaBlaCar.

**b. Berichten**

Het uitwisselen van berichten tussen Leden via ons Platform (“Berichten”) vindt uitsluitend plaats met als doel het uitwisselen van informatie over Carpoolritten.

BlaBlaCar kan geautomatiseerde software en algoritmes gebruiken om de content van Berichten te detecteren om fraude te voorkomen, de service te verbeteren, ten behoeve van klantenondersteuning en het uitvoeren van contracten die met onze Leden zijn aangegaan (zoals deze AV’s). In geval dergelijke content wordt gedetecteerd in een Bericht dat tekenen vertoont van fraude of illegaal gedrag, ontduiking van het Platform of gedrag dat anderszins is in strijd is met deze AV’s

* Dergelijke Content mag niet worden gepubliceerd
* Het Lid dat de Bericht verstuurt kan een waarschuwing ontvangen waarin wordt herinnerd aan de verplichting om de toepasselijke wetgeving en deze AV’s na te leven
* Het Profiel van het Lid kan worden opgeschort conform Hoofdstuk 9 van deze AV’s

Het gebruik door BlaBlaCar van dergelijke geautomatiseerde software mag niet geïnterpreteerd als een maatregel tot het monitoren van en een verplichting tot het actief zoeken naar onwettige activiteiten en/of Content voor Leden die gepubliceerd wordt op het Platform, en voor zover wettelijk toegestaan leidt dit niet tot enige aansprakelijkheid van BlaBlaCar.

**4.3.3.    Limiet**

Conform Hoofdstuk 9  van deze AV’s behoudt BlaBlaCar zich het recht voor om uw Profiel op te schorten, uw toegang tot de Diensten te beperken of deze AV’s op te zeggen indien (i) u minstens drie beoordelingen hebt ontvangen en (ii) een of de gemiddelde beoordeling die u heeft ontvangen, hoogstens gelijk is aan 3, afhankelijk van de ernst van de feedback van een Lid in de beoordeling..

**5.    Financiële voorwaarden**
--------------------------------

De toegang tot en de registratie op het Platform en het zoeken, bekijken en posten van Publicaties zijn gratis. Voor de boeking worden kosten aangerekend in overeenstemming met de voorwaarden die hieronder worden beschreven.

### **5.1.**    Bijdrage in de Kosten

5.1.1. Voor een Carpool wordt de bijdrage in de Kosten onder uw exclusieve verantwoordelijkheid door u als Bestuurder bepaald. Het is ten strengste verboden om op welke manier dan ook winst te boeken door het gebruik van ons Platform. U stemt er dan ook mee in om de Bijdrage in de Kosten die u aan uw Passagiers vraagt, te beperken tot de kosten die u werkelijk maakt om de Carpoolrit te maken. Anders zult u als enige de risico’s dragen van een herclassificiatie van de transactie die via het Platform werd uitgevoerd.

Wanneer u een Carpoolpublicatie plaatst, stelt BlaBlaCar een bedrag voor de Bijdrage in de Kosten voor, waarbij onder meer rekening wordt gehouden met de aard van de Carpoolrit en de afstand die wordt afgelegd. Dit bedrag wordt louter als richtlijn gegeven; u kunt zelf beslissen om dat bedrag te verhogen of te verlagen om rekening te houden met de reële kosten van de Carpoolrit. Om misbruik te voorkomen, limiteert BlaBlaCar de mogelijkheid om de Bijdrage in de Kosten aan te passen.

5.1.2 Wat de Busrit betreft, wordt de Prijs per Zitplaats bepaald naar goeddunken van de Busmaatschappij. De Klant wordt verzocht de relevante Algemene Verkoopvoorwaarden te raadplegen zodat hij de geldende modaliteiten met betrekking tot het plaatsen van een bestelling voor een ticket en de betaling begrijpt.  

### **5.2.    Vergoeding voor de Diensten**

BlaBlaCar kan, in ruil voor het gebruik van het Platform, op het moment van de boeking van een Rit of buskaartje een Vergoeding voor de Diensten (hierna de “**Vergoeding voor de Diensten**” genoemd) vragen. De gebruiker vóór de bevestiging wordt op de hoogte gebracht van de Vergoeding voor Diensten, indien vereist.

De manier waarop de geldende Vergoeding voor de Diensten wordt berekend, vindt u [hier](https://blog.nl.blablacar.be/blablalife/lp/nieuwe-reserveringskosten-tabel) en is louter ter informatie en heeft geen contractuele waarde. BlaBlaCar behoudt zich het recht voor om de berekeningsmethode te allen tijde te veranderen. Deze wijzigingen hebben geen invloed op de Vergoeding voor de Diensten en worden niet retroactief toegepast op reeds geboekte Ritten en buskaartjes.

Houd in het geval van internationale ritten rekening met het feit dat de berekeningsmethoden van het bedrag van de Vergoeding voor de Diensten en de btw die daarop van toepassing is, kan verschillen afhankelijk van de verblijfplaats van de Passagier.

Wanneer u het Platform gebruikt voor grensoverschrijdende ritten of voor ritten buiten Nederland, is het mogelijk dat de Vergoeding voor de Diensten wordt aangerekend door een filiaal van BlaBlaCar dat het plaatselijke platform runt.

### **5.3.    Afronden**

U erkent en aanvaardt dat BlaBlaCar naar eigen goeddunken de Vergoeding voor de Diensten en de Bijdrage in de Kosten naar boven of naar beneden kan afronden op gehele getallen of decimalen.

### **5.4.    Methoden van betaling en terugbetaling van de Bijdrage in de Kosten aan de Bestuurder**

**5.4.1.    Instructie Bestuurder (machtiging)**

Door het Platform als Bestuurder voor Carpoolritten te gebruiken, bevestigt u dat u de [algemene voorwaarden van de provider van de Hyperwallet-betaalmlethode](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) hebt gelezen en aanvaard – dit is onze betaalpartner die de overdrachten van kostenbijdragen aan de Bestuurders verwerkt via de Hyperwallet-betaalmethode, beschreven onder 5.4.2.a. Deze algemene voorwaarden hierna de “AV’s van de Hyperwallet” genoemd.

Merk op dat uitsluitingen op EU-richtlijn 2015/2366 van het Europese Parlement en de Raad van 25 november 2015 betreffende betaaldiensten op de interne markt, vermeld in artikel 11.4(i) en (ii) in de algemene voorwaarden van de Hyperwallet niet van toepassing zijn op de leden van het Platform die geen professionals zijn.

Als Bestuurder geeft u instructies aan

* BlaBlaCar om de Provider van de Hyperwallet-betaalmethode opdracht te geven om de overdracht voor de Bijdrage in de Kosten uit te voeren en
* de Provider van de Hyperwallet-betaalmethode om de overdracht van de Bijdrage in de Kosten over te maken naar uw bank of PayPal-account in uw naam en in uw opdracht, overeenkomstig de AV’s van de Hyperwallet.

In de context van een Carpoolrit en na handmatige of automatische goedkeuring van de Boeking wordt de volledige som die door de Passagier werd betaald (Vergoeding voor de Diensten en Bijdrage in de Kosten) op een geblokkeerde rekening geplaatst die de Provider van de Hyperwallet-betaalmethode wordt beheerd.

**5.4.2.    Betaling van de Bijdrage in de Kosten aan de Bestuurder**

Na het einde van de Carpoolrit zullen de passagiers een periode van 24 uur hebben om een riten-claim aan BlaBlaCar te vragen. In afwezigheid van enige passagiers claim binnen deze periode, zal BlaBlaCar de rit als uitgevoerd beschouwen.

Vanaf het tijdstip van deze uitdrukkelijke of stilzwijgende bevestiging krijgt u als Bestuurder een krediet op uw Profiel. Dit krediet stemt overeen met het volledige bedrag dat door de Passagier werd betaald op het moment van de bevestiging van de Boeking, verminderd met de Vergoeding voor de Diensten, d.w.z. de Bijdrage in de Kosten die door de Passagier wordt betaald.

**a. Hyperwallet-betaalmethode**

De Hyperwallet-betaalmethode is een betaaldienst die door de Provider van de Hyperwallet-betaalmethode wordt verstrekt. Voor alle duidelijkheid: BlaBlaCar biedt geen betaalverwerkingsdiensten voor Leden en neemt de gelden van Leden niet in ontvangst.

Wanneer de Carpoolrit door de Passagier bevestigd is, hebt u als Bestuurder de mogelijkheid om uw Bijdrage in de Kosten op uw bankrekening of op uw PayPal-account te ontvangen.

Hiertoe kunt u de Hyperwallet-betaalmethode gebruiken om rechtstreeks met de Provider van de Hyperwallet-betaalmethode een overeenkomst aan te gaan en de AV’s van de Hyperwallet te aanvaarden.

Om de eerste overdracht voor de Bijdrage in de Kosten te ontvangen, zal u gevraagd worden een betaalmethode te kiezen en uw bankrekeninggegevens, postadres en geboortedatum te verstrekken of uw PayPal-account te verbinden. Indien u dit niet doet, of als de informatie die u verstrekt, niet correct, onvolledig of niet actueel is, dan zal de Bijdrage in de Kosten niet worden uitbetaald.

De overdracht zal uitgevoerd worden door de Provider van de Hyperwallet-betaalmethode.

Aan het einde van de toepasselijke verjaringstermijnen zoals gedefinieerd in de toepasselijke wetten, wordt elk bedrag voor Bijdragen in de Kosten die door u als Bestuurder via de Hyperwallet-betaalmethode is geclaimd, geacht eigendom te zijn van BlaBlaCar.

**b. KYC-controles**

Bij het gebruik van Hyperwallet-betaalmethode aanvaardt u dat u mogelijk onderworpen bent aan regelgevingsprocedures die door de Provider van de Hyperwallet-betaalmethode worden toegepast, met inachtneming van de [AV’s van de Hyperwallet](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml).

Deze procedures kunnen identiteitscontroles en andere vereisten van de “Know Your Customer” (KYC)-procedure omvatten, op basis van de criteria die zijn vastgesteld door de Provider van de Hyperwallet-betaalmethode. Deze criteria kunnen financiële drempels omvatten, afhankelijk van het totale bedrag aan uitbetalingen van de Bijdrage in de Kosten die u doet.

Voor KYC-controledoeleinden kan u worden gevraagd om aanvullende informatie te verstrekken zoals gevraagd door de Provider van de Hyperwallet-betaalmethode. In sommige gevallen kan BlaBlaCar contact met u opnemen om een KYC-verzoek van de Provider van de Hyperwallet-betaalmethode door te geven. Als u de vereiste documenten en informatie niet verstrekt, kunt u de kostenbijdrage pas ontvangen nadat u de KYC-procedure heeft voltooid.

Er kunnen zich situaties voordoen waarin de Provider van de Hyperwallet-betaalmethode, in overeenstemming met de toepasselijke wetgeving, de overdracht van de Bijdrage in de Kosten of de toegang tot de Hyperwallet-betaalmethode moet opschorten. BlaBlaCar heeft geen invloed op dergelijke acties van de Provider van de Hyperwallet-betaalmethode.

De KYC-controles die worden uitgevoerd door de Provider van de Hyperwallet-betaalmethode zijn verschillend en onafhankelijk van de verificaties die BlaBlaCar kan uitvoeren met betrekking tot het gebruik van het Platform in overeenstemming met deze Voorwaarden en het [Privacybeleid](https://www.blablacar.nl/about-us/privacy-policy).

**5.4.3. Betaling van de Prijs**

De betaling voor elke Bestelling gemaakt via het Platform gebeurt aan de hand van een van de hieronder vermelde toegestane betaalmiddelen. De Klant heeft de mogelijkheid om de informatie met betrekking tot een of meer kredietkaarten in zijn Klantaccount op te slaan, zodat hij deze informatie niet systematisch moet invoeren bij latere betalingen.

De betalingswijze kan variëren naargelang het BlaBlaCar-platform dat u gebruikt. De toegestane betaalmiddelen zijn:

* Bankkaart (de aanvaarde merken worden op het platform vermeld. Indien u een “co-branded” kaart hebt kunt u een specifiek merk kiezen in het betaalstadium)
* PayPal
* Voucher
* iDEAL
* Apple Pay, Google Pay

Er zal geen bestelbevestiging plaatsvinden vóór de volledige en effectieve betaling van de prijs van de Vervoersdiensten gekozen door de Klant. Indien de betaling onregelmatig of onvolledig is of niet wordt uitgevoerd om een reden die toe te schrijven is aan de Klant, zal de Bestelling onmiddellijk worden geannuleerd.

De Bestelling wordt bevestigd door een e-mail aan de Klant met de gegevens van de Bestelling.

De Passagier wordt verzocht de instellingen voor de inbox van zijn e-mailadres te controleren en zich er met name van te vergewissen dat de bevestigingsmail niet rechtstreeks in de Spam-mailbox terechtkomt.

De bevestiging van de bestelling is definitief. Bijgevolg zullen alle wijzigingen resulteren in een omruiling of annulering onder de voorwaarden van de toepasselijke Algemene Voorwaarden. Het is de verantwoordelijkheid van de Klant om ervoor te zorgen dat de Vervoersdiensten worden gekozen in overeenstemming met de behoeften en verwachtingen van de Passagier. De klant is verantwoordelijk voor de juistheid van de door hem ingevoerde persoonlijke informatie, behalve wanneer BlaBlaCar aantoonbaar nalaat deze persoonlijke informatie te verzamelen, op te slaan of te beschermen.

**6.    Niet-commerciële en niet-bedrijfsdoeleinden van de Diensten en van het Platform**
-----------------------------------------------------------------------------------------

U stemt ermee in om de Diensten en het Platform alleen te gebruiken om voor niet-commerciële en niet-bedrijfsdoeleinden in contact te worden gebracht met mensen die een Carpoolrit met u willen delen of om een Zitplaats te boeken in het kader van een Busrit.

In het kader van een Carpoolrit erkent u dat de consumentenrechten die voortvloeien uit de consumentenbeschermingswetgeving van de Unie niet van toepassing zijn op uw relatie met andere Leden.

Als Bestuurder stemt u ermee in om geen Bijdrage in de Kosten te vragen die hoger ligt dan de kosten die u in werkelijkheid maakt en die een winst zou kunnen opleveren, met dien verstande dat u in de context van kostendeling als Bestuurder uw eigen deel van de kosten voor de Carpoolrit moet dragen. U bent als enige verantwoordelijk om de kosten die u maakt voor de Carpoolrit, te berekenen en om te controleren of het bedrag dat wordt gevraagd, niet hoger ligt dan de kosten die u werkelijk maakt (met uitsluitsel van uw deel van de kosten),  door onder meer te verwijzen naar de toepasselijke belastingschaal \[aan te passen\].

BlaBlaCar behoudt zich het recht voor om uw Profiel op te schorten wanneer u gebruikmaakt van een door een chauffeur bestuurd of ander bedrijfsvoertuig of taxi of bedrijfsvoertuig en wanneer u daardoor winst boekt door gebruik te maken van het Platform. U stemt ermee in om BlaBlaCar op haar eenvoudige verzoek een kopie te bezorgen van het inschrijvingsbewijs van uw auto en ieder ander document dat aantoont dat u dit voertuig op het Platform mag gebruiken en dat u op basis daarvan geen winst boekt.

Conform Hoofdstuk 9 van deze AV’s behoudt BlaBlaCar zich ook het recht voor om uw Profiel op te schorten, om uw toegang tot de Diensten te beperken of om deze AV’s op te zeggen indien u op het Platform activiteiten uitvoert die wegens de aard of de frequentie van de aangeboden Ritten, het aantal vervoerde Passagiers en de Bijdrage in de Kosten die wordt gevraagd, tot een situatie leiden waarin u winst boekt of die om welke reden dan ook die bij BlaBlaCar de indruk wekken dat u winst maakt door het gebruik van het Platform.

**7.    Annuleringsbeleid**
---------------------------

### **7.1.**    Annulering van een Rit

Dit annuleringsbeleid is enkel van toepassing op Carpoolritten en BlaBlaCar biedt geen enkele garantie in geval van een annulering om welke reden dan ook. In het kader van de busritten is het annuleringsbeleid onderworpen aan de algemene verkoopvoorwaarden van de busmaatschappijen. De klant wordt verzocht de betreffende Algemene Voorwaarden te raadplegen om kennis te nemen van de voorwaarden voor omwisseling en annulering van zijn/haar Busrit.

Voor de annulering van een Zitplaats voor een Carpoolrit door de Bestuurder of de Passagier na Bevestiging van Boeking gelden de volgende bepalingen:

–    In het geval van een annulering die aan de Bestuurder toe te schrijven is, krijgt de Passagier het volledige betaalde bedrag terug (d.w.z. de Bijdrage in de Kosten en de bijbehorende Vergoeding voor de Diensten). Dit is het geval wanneer de Bestuurder een Rit annuleert of 15 minuten na het afgesproken tijdstip niet op de plaats van afspraak verschenen is ;

–    In het geval van een annulering die aan de Passagier toe te schrijven is:

* Als de Passagier meer dan 24 uur vóór het tijdstip van vertrek (vermeld in de Carpoolpublicatie) annuleert, krijgt de Passagier alleen de Bijdrage in de Kosten terugbetaald. De Vergoeding voor de Diensten blijven bij BlaBlaCar en de Bestuurder ontvangt geen enkel bedrag van welke aard dan ook;
* Als de Passagier 24 uur of minder vóór het tijdstip van vertrek (vermeld in de Carpoolpublicatie) en meer dan dertig minuten na de Bevestiging van Boeking annuleert, krijgt de Pasagier de helft van de Bijdrage in de Kosten die op het moment van de boeking werden betaald, terugbetaald. De Vergoeding voor de Diensten blijven bij BlaBlaCar en de Bestuurder ontvangt 50 % van de Bijdrage in de Kosten via de Hyperwallet-betaalmethode;
* Als de Passagier 24 uur of minder vóór het geplande tijdstip van vertrek (vermeld in de Carpoolpublicatie) en dertig minuten of minder na de Bevestiging van Boeking annuleert, wordt de Passagier de volledige Bijdrage in de Kosten terugbetaald via de Hyperwallet-betaalmethode. De Vergoeding voor de Diensten blijft bij BlaBlaCar en de Bestuurder ontvangt geen enkele som van welke aard dan ook;
* Als de Passagier annuleert na het geplande tijdstip van vertrek (vermeld in de Carpoolpublicatie), of als de Passagier 15 minuten na het overeengekomen tijdstip niet op de plaats van afspraak verschenen is, wordt geen enkel bedrag terugbetaald. In dat geval wordt de Bestuurder vergoed via de Hyperwallet-betaalmethode voor het volledige bedrag van de Bijdrage in de Kosten en de Vergoeding voor de Diensten blijft bij BlaBlaCar.

Wanneer de annulering plaatsvindt vóór het vertrek en ten gevolge van de Passagier, wordt de Zitplaats of worden de Zitplaatsen die door de Passagier werd(en) geannuleerd, automatisch beschikbaar gesteld voor andere Passagiers, die die Zitplaatsen online kunnen boeken. Voor hen gelden dan deze voorwaarden van de AV’s.  
BlaBlaCar oordeelt naar eigen goeddunken op basis van de beschikbare informatie over de legitimiteit van de aanvragen voor terugbetaling.

### **7.2.    Recht op herroeping**

In overeenstemming met Artikel 6:230p sub e van het Nederlandse Burgerlijk Wetboek hebt u geen recht op herroeping vanaf het tijdstip van de Bevestiging van Boeking indien het Contract tussen u en BlaBlaCar, dat erin bestaat om u in contact te brengen met een ander Lid, volledig werd uitgevoerd.

**8.    Gedrag van gebruikers van het Platform en Leden**
---------------------------------------------------------

### **8.1.    Verbintenissen van alle gebruikers op het Platform**

U erkent als enige verantwoordelijk te zijn voor de naleving van alle wetten, voorschriften en verplichtingen die van toepassing zijn op uw gebruik van het Platform.

Voorts belooft u bij het gebruik van het Platform en tijdens de Ritten om:

* (i) het Platform niet te gebruiken voor professionele of commerciële doeleinden of om andere doeleinden om winst te maken;
* (ii) geen valse, misleidende, kwaadwillige of frauduleuze informatie te sturen naar BlaBlaCar (en dan meer bepaald bij het aanmaken of het updaten van uw Profiel);
* (iii) in uw gesprekken of uw gedrag of in uw publicaties (inclusief Berichten) geen lasterlijke, beledigende, obscene, pornografische, vulgaire, kwetsende, ongepaste, gewelddadige, bedreigende, irriterende, racistische of xenofobe uitingen te geven of uitingen met seksuele connotaties, uitingen die tot geweld, discriminatie of haat oproepen, activiteiten of die het gebruik van illegale stoffen aanmoedigen of meer algemeen die indruisen tegen de toepasselijke wetgeving, deze AV’s en de doelstellingen van het Platform, die de rechten van BlaBlaCar of een derde zouden kunnen aantasten of die indruisen tegen de goede zeden;
* (iv) de rechten en het imago van BlaBlaCar – en dan meer bepaald haar intellectuele eigendomsrechten – niet te schaden;
* (v) niet meer dan één Profiel op het Platform te openen en geen Profiel te openen in naam van een derde;
* (vi) niet te proberen om het online boekingssysteem van het Platform te omzeilen, en dan meer bepaald door te proberen om een ander Lid of een Busmaatschappij uw contactgegevens te bezorgen, zodat de boeking buiten het Platform kan worden gemaakt om zo de Vergoeding voor de Diensten te omzeilen;
* (vii) geen contact op te nemen met een ander Lid, en dan meer bepaald via het Platform, voor een ander doeleinde dan de voorwaarden voor de ridesharing af te spreken;
* (viii) geen betalingen buiten het Platform of de Hyperwallet-betaalmethode te aanvaarden of uit te voeren;
* (ix) deze AV’s na te leven; en
* (x) de algemene voorwaarden van de Busmaatschappij die het buskaartje dat u hebt geboekt aanbiedt, na te leven.

### **8.2.    Verbintenissen van de Bestuurder**

Voorts belooft u bij het gebruik van het Platform als Bestuurder:

* (i) alle wetten, voorschriften en richtlijnen na te leven die van toepassing zijn op het autorijden en het voertuig, en dan meer bepaald om op het moment van de Carpoolingrit een burgerlijke aansprakelijkheidspolis te hebben onderschreven en in het bezit te zijn van een geldig rijbewijs;
* (ii) te controleren of uw verzekering ridesharing dekt en of uw Passagiers worden beschouwd als derden in uw voertuig en daardoor door uw verzekering gedekt zijn;
* (iii) geen risico’s te nemen tijdens het rijden en geen producten te nemen die uw aandacht en uw vermogen om op een oplettende en volledig veilige manier het voertuig te besturen, zouden kunnen aantasten;
* (iv) alleen Carpoolpublicaties te plaatsen over Ritten die echt zijn gepland;
* (v) de Carpoolrit te maken zoals die in de Publicatie wordt beschreven (en dan meer bepaald met betrekking tot het al dan niet gebruiken van de snelweg) en de tijdstippen en de plaatsen te respecteren die met de andere Leden werden overeengekomen (en dan meer bepaald met betrekking tot de ontmoetingsplaats en het afzetpunt);
* (vi) niet meer Passagiers mee te nemen dan het aantal Zitplaatsen dat in de Carpoolpublicatie wordt vermeld;
* (vii) een voertuig te gebruiken dat zich in een goede staat bevindt en die in lijn is met de toepasselijke wettelijke bepalingen en gewoonten, in het bijzonder met een geldig APK-keuringsrapport;
* (viii) aan BlaBlaCar of iedere Passagier die dat vraagt, uw rijbewijs, het inschrijvingsbewijs van uw auto, uw verzekeringspolis, uw APK-keuringsrapport en ieder ander document voor te leggen dat uw bevoegdheid aantoont om het voertuig als Bestuurder op het Platform te gebruiken;
* (ix) uw Passagiers onmiddellijk op de hoogte te brengen van een eventuele vertraging of verandering van tijdstip of Carpoolrrit;
* (x) bij een grensoverschrijdende Carpoolrit alle documenten bij te houden en beschikbaar te stellen voor de Passagier en iedere andere instantie, die uw identiteit en uw recht om de grens over te steken, aantonen;
* (xi) tot minstens 15 minuten na het afgesproken tijdstip te wachten op de Passagiers;
* (xii) geen Publicatie te plaatsen met betrekking tot een voertuig waarvan u niet de eigenaar bent of dat u niet mag gebruiken om ritten te delen;
* (xiii) ervoor te zorgen dat uw Passagiers contact met u kunnen opnemen op het telefoonnummer dat in uw profiel vermeld is;
* (xiv) geen winst te boeken door gebruik te maken van het Platform;
* (xv) dat voor u geen medische tegenindicatie van toepassing is of dat u niet om medische redenen ongeschikt bent om met de auto te rijden;
* (xvi) om ervoor te zorgen dat uw Bestuurder telefonisch contact met u kan opnemen op het nummer dat in uw profiel is geregistreerd, ook op het ontmoetingspunt.

### **8.3.    Verbintenissen van de Passagiers**

**8.3.1 Voor Carpoolrit**

Wanneer u het Carpoolplatform als Passagier gebruikt, belooft u om:

* (i) u op een gepaste manier te gedragen tijdens de Carpoolrit, zodat de concentratie of het rijgedrag van de Bestuurder of de rust van de andere Passagiers niet wordt verstoord;
* (ii) het voertuig en de netheid van het voertuig van de Bestuurder te respecteren;
* (iii) de Bestuurder onmiddellijk op de hoogte te brengen als u niet op tijd op de plaats van afspraak zult zijn;
* (iv) op de plaats van afspraak tot minstens 15 minuten na het afgesproken tijdstip op de Bestuurder te wachten;
* (v) aan BlaBlaCar of een Bestuurder die dat vraagt, uw identiteitskaart of een ander document dat uw identiteit aantoont, voor te leggen;
* (vi) tijdens een Carpoolrit geen artikel, goederen, product of dier mee te nemen dat het rijgedrag en de concentratie van de Bestuurder zou kunnen verstoren of waarvan de aard, het eigendom of het vervoer in strijd is met de geldende wettelijke voorschriften;
* (vii) bij een grensoverschrijdende Carpoolrit ieder document waarmee uw identiteit en uw recht om de grens over te steken, kan worden aangetoond, bij te houden en beschikbaar te houden voor de Bestuurder en iedere instantie die daarom vraagt;
* (viii) ervoor te zorgen dat uw andere Passagiers contact met u kunnen opnemen via het telefoonnummer dat op uw profiel ingeschreven is – ook op de plaats van afspraak.

Indien u een Boeking hebt uitgevoerd voor één of meer Zitplaatsen in naam van derden, garandeert u – in overeenstemming met de bepalingen van Artikel 4.2.3 hierboven – dat deze derde de bepalingen van dit artikel en algemeen gesteld onderhavige AV’s zal naleven. BlaBlaCar behoudt zich het recht voor om uw Profiel op te schorten, uw toegang tot de Diensten te beperken of onderhavige AV’s op te zeggen in het geval van een inbreuk door de derde in wiens naam u een Zitplaats conform onderhavige AV’s hebt geboekt.

**8.3.2 Voor een Busrit**

In het kader van een busrit verbindt de passagier zich ertoe de Algemene Verkoopvoorwaarden van de betrokken Busmaatschappij na te leven.

**8.4 Melden van ongepaste of illegale content (Kennisgeving en actie mechanisme).**

U kunt verdachte, ongepaste of illegale Content van Leden of Berichten melden zoals [hier beschreven](https://support.blablacar.com/s/article/Illegale-inhoud-melden-1729197120603?language=nl_NL).

BlaBlaCar zal na passend te zijn geïnformeerd conform dit Hoofdstuk of door de bevoegde autoriteiten, onmiddellijk alle onwettige Content voor Leden verwijderen als:

* De Content voor Leden duidelijk onwettig of in strijd is met toepasselijke regelgeving; of
* BlaBlaCar van mening is dat dergelijke Content inbreuk maakt op deze AV’s.

In dergelijke gevallen behoudt BlaBlaCar zich het recht voor de Content voor Leden te verwijderen nadat deze correct en op een voldoende gedetailleerde en duidelijke manier is gemeld en/of het gemelde Profiel onmiddellijk verwijderen.

Het betreffende Lid kan bezwaar maken tegen de bovenstaande beslissing van BlaBlaCar zoals beschreven in Hoofdstuk 15.1 hieronder.

**9.    Beperkingen in verband met het gebruik van de Platforms, opschorting van profielen, beperking van toegang en opzegging**
--------------------------------------------------------------------------------------------------------------------------------

U kunt uw contractuele relaties met BlaBlaCar kosteloos en zonder opgave van reden op eender welk moment opzeggen. Ga daarvoor naar de tab “Profiel afsluiten” op uw Profielpagina.

Bij een (i) inbreuk door u van onderhavige AV’s, met inbegrip van maar niet beperkt tot uw verplichtingen als Lid die in de Artikelen 6 en 8 hierboven worden vermeld, (ii) overschrijding van de grenswaarde die in Artikel 4.3.3 hierboven wordt uiteengezet of (iii) als BlaBlaCar gegronde redenen heeft om aan te nemen dat dit nodig is om haar eigen veiligheid en integriteit of de veiligheid en integriteit van haar Leden of derden te vrijwaren of om fraude of onderzoeken te vermijden, behoudt BlaBlaCar zich het recht voor om:

* (i) onmiddellijk en zonder voorafgaand bericht onderhavige AV’s die u met BlaBlaCar verbinden, op te zeggen en/of
* (ii) het plaatsen van Content van Leden die door u op het Platform werd gepost, te blokkeren of te verwijderen en/of
* (iii) uw toegang tot en uw gebruik van het Platform te beperken en/of
* (iv) uw Profiel tijdelijk of permanent op te schorten.

Opschorting van het Profiel kan in voorkomende gevallen ook betekenen dat u geen lopende uitbetalingen ontvangt.

Indien van toepassing kan BlaBlaCar u ook een herinnering sturen aan de verplichting om te voldoen aan toepasselijke wetgeving en deze AV’s.

Wanneer dit nodig is, zult u in kennis worden gesteld van zo’n maatregel, zodat u kunt bevestigingen dat u toepasselijke wetgeving en deze AV’s zult naleven, u de kans krijgt om uitleg te geven aan BlaBlaCar of bezwaar kunt maken tegen de beslissing van BlaBlaCar. BlaBlaCar zal rekening houdend met de omstandigheden van elk afzonderlijk geval en de ernst van de schending, naar eigen goeddunken beslissen om de ingevoerde maatregelen al dan niet op te heffen.

**10.    Persoonsgegevens**
---------------------------

In de context van uw gebruik van het Platform zal BlaBlaCar enkele persoonsgegevens verzamelen en verwerken zoals beschreven in het [Privacybeleid.](https://blog.blablacar.nl/about-us/privacy-policy)

**11.    Intellectuele eigendom**

### **11.1.    Content die door BlaBlaCar werd gepubliceerd**

Rekening houdend met de content die door haar Leden beschikbaar gesteld wordt, is BlaBlaCar de enige houder van alle intellectuele eigendomsrechten met betrekking tot de Diensten, het Platform en de content ervan (en dan meer bepaald teksten, beelden, designs, logo’s, video’s, geluiden, gegevens en grafische elementen) en de software en de databases die de werking ervan garanderen.

BlaBlaCar kent u een niet-exclusief, persoonlijk en niet-overdraagbaar recht toe om van het Platform en de Diensten gebruik te maken voor uw persoonlijke en private gebruik op een niet-commerciële basis en in overeenstemming met de doelstellingen van het Platform en de Services.  
Het is verboden om het Platform en de Diensten en hun content op enige andere manier te gebruiken of te exploiteren, zonder dat BlaBlaCar daarvoor vooraf haar schriftelijke toestemming heeft verleend. Het is meer bepaald verboden om:

* (i) het Platform, de Diensten en de content te reproduceren, te wijzigen, aan te passen, te verdelen, in het openbaar te vertegenwoordigen en te verspreiden, met uitzondering van wat uitdrukkelijk wordt toegestaan door BlaBlaCar;
* (ii) het Platform of de Diensten te ontleden en er ‘reverse engineering’ op toe te passen, behalve in de uitzonderlijke gevallen die door de geldende teksten worden bepaald;
* (iii) een substantieel deel van de gegevens van het Platform te extraheren of te proberen te extraheren (meer bepaald door het gebruik van ‘data mining robots’ of andere vergelijkbare verzameltools).

### **11.2. Content die door u op het Platform werd gepost**

Om de Verlening van de Diensten mogelijk te maken in overeenstemming met de doelstelling van het Platform, kent u BlaBlaCar een niet-exclusieve toelating toe om de content en de gegevens die u bezorgt in de context van uw gebruik van de Diensten, waaronder uw reserveringsverzoeken, Carpoolpublicaties en commentaren daarin, biografieën, foto’s, beoordelingen en antwoorden op beoordelingen(hierna de “**Content voor Leden**” genoemd) en Berichten, te gebruiken. Om BlaBlaCar toe te laten om via het digitale netwerk en in overeenstemming met het communicatieprotocol (en dan meer bepaalde het internet en het mobiele netwerk) en om de content van het Platform aan het publiek beschikbaar te stellen, laat u BlaBlaCar toe om uw Content voor Leden voor de hele wereld en tijdens de volledige duur van uw contractuele relaties met BlaBlaCar als volgt te reproduceren, weer te geven, aan te passen en te vertalen:

(i) u staat BlaBlaCar toe om uw Content voor Leden geheel of gedeeltelijk te reproduceren op eender welk bekend of nog niet bekend mediamiddel, en dan meer bepaald op iedere server, geheugenkaart of ieder ander gelijkwaardig medium, in eender welk formaat en door eender welk bekend of nog niet bekend proces, in de mate die nodig is om iedere opslag, back-up, verzending of download in verband met de werking van het Platform en de verlening van de Diensten mogelijk te maken;

(ii) u staat BlaBlaCar toe om uw Content voor Leden aan te passen en te vertalen en om die aanpassingen te reproduceren op eender welk bestaand of toekomstig medium, zoals bepaald in punt (i) hierboven, met de bedoeling om de Diensten meer bepaald in verschillende talen aan te bieden. Dit recht omvat meer bepaald de optie om wijzigingen door te voeren aan de formattering van uw Content voor Leden, met inachtneming van uw morele recht met de bedoeling om het grafische charter van het Platform te respecteren en/of om het technisch compatibel te maken met oog op de publicatie ervan via het Platform.

U blijft verantwoordelijk voor en hebt alle rechten voor Content voor Leden en Berichten die u upload op ons Platform. 

**12\. Rol van BlaBlaCar**
--------------------------

Het Platform vormt een online netwerkplatform waarop de Leden Carpoolpublicaties voor Carpoolritten kunnen aanmaken en plaatsen met het oog op ridesharing, en buskaartjes van Busmaatschappijen kunnen boeken. Deze Carpoolpublicaties kunnen meer bepaald worden bekeken door de andere Leden, die op die manier de voorwaarden van de Rit kunnen bekijken en in voorkomend geval rechtstreeks een Zitplaats kunnen boeken in het voertuig in kwestie met het Lid dat de Publicatie op het Platform heeft gepost. Het Platform biedt ook de mogelijkheid om Zitplaatsen voor Busreizen te boeken.

Door het Platform te gebruiken en onderhavige AV’s te aanvaarden, erkent u dat BlaBlaCar geen partij vormt in eender welke overeenkomst tussen u en de andere Leden met het oog op het delen van de kosten die verband houden met een Rit bij carpooling, noch in eender welke overeenkomst tussen u en de Busmaatschappij voor het vervoer van passagiers per bus.

BlaBlaCar heeft geen controle over het gedrag van haar Leden, de Busmaatschappijen en hun vertegenwoordigers, en de gebruikers van het Platform. BlaBlaCar is geen eigenaar, exploitant, leverancier of beheerder van de voertuigen die het voorwerp vormen van de Publicaties en ze biedt ook geen Ritten aan op het Platform.

U erkent en aanvaardt dat BlaBlaCar de geldigheid, waarheidsgetrouwheid of wettelijkheid van de Publicaties en aangeboden Zitplaatsen en Ritten controleert. In haar hoedanigheid van tussenpersoon voor ridesharing biedt BlaBlaCar geen transportdiensten aan en treedt ze ook niet op als vervoerbedrijf; de rol van BlaBlaCar blijft beperkt tot het vergemakkelijken van de toegang tot het Platform.

In het kader van Carpoolritten handelen de Leden (Bestuurders of Passagiers) op hun eigen exclusieve en volledige verantwoordelijkheid.

In haar hoedanigheid van tussenpersoon kan BlaBlaCar niet aansprakelijk worden gesteld voor het plaatsvinden van een Rit, meer bepaald ten gevolge van:

  (i) onjuiste informatie die door de Bestuurder of de Busmaatschappij werd gemeld in zijn/haar Publicatie of via enig ander communicatiemiddel met betrekking tot een Rit;

  (ii) annulering of wijziging van een Rit door een Lid of een Busmaatschappij;

  (iii) het gedrag van haar Leden tijdens, vóór of na de Rit of een verplaatsing per bus.

**13.    Werking, beschikbaarheid en functionaliteiten van het Platform**
-------------------------------------------------------------------------

BlaBlaCar probeert in de mate van het mogelijke het Platform 7 dagen per week en 24 uur per dag toegankelijk te houden. Het is evenwel mogelijk dat de toegang tot het Platform zonder voorafgaand bericht tijdelijk wordt opgeschort wegens technisch onderhoud, migratie- of update-operaties of storingen of beperkingen die verband houden met de werking van het netwerk.

Voorts behoudt BlaBlaCar zich het recht voor om naar eigen goeddunken de toegang tot het Platform of zijn functionaliteiten volledig of gedeeltelijk en tijdelijk of permanent te wijzigen of op te schorten.

**14.    Wijziging van de AV’s**
--------------------------------

Onderhavige AV’s en de documenten die er door verwijzing in opgenomen zijn, vormen de volledige overeenkomst tussen u en BlaBlaCar met betrekking tot uw gebruik van de Diensten. Ieder ander document, en dan meer bepaald iedere vermelding op het Platform (FAQ enz.) is uitsluitend als richtlijn bedoeld.

BlaBlaCar mag onderhavige AV’s wijzigen om ze aan te passen aan de technologische en commerciële omgeving en om ze in overeenstemming te brengen met de geldende wetgeving. Iedere wijziging aan onderhavige AV’s zal op het Platform worden gepubliceerd met vermelding van de datum waarop ze van kracht wordt en u zult daarvan door BlaBlaCar op de hoogte worden gebracht voordat de wijziging van kracht wordt.

**15.    Toepasselijk recht en geschillen**
-------------------------------------------

Onderhavige AV’s zijn in het Nederlands opgesteld en zijn onderworpen aan het Nederlandse recht.

**15.1 Intern klachtenverwerkingssysteem**

U kunt bezwaar maken tegen beslissingen die wij nemen betreffende:

* Content van Leden: bijvoorbeeld wij hebben Content van Leden die u hebt gepubliceerd bij het gebruik van het Platform verwijderd, de zichtbaarheid ervan beperkt of geweigerd deze content te verwijderen, of
* Uw Profiel: we hebben uw toegang tot het Platform opgeschort,

in geval wij een dergelijke beslissing hebben genomen op basis van het feit dat de Content van Leden illegale content vormt of in strijd is met deze AV’s. De bezwaarprocedure wordt beschreven in [deze link](https://support.blablacar.com/s/article/Bezwaar-maken-tegen-verwijdering-van-inhoud-of-accountschorsing-1729197123035?language=nl_NL).

**15.2 Buitengerechtelijke geschiloplossing**

Indien nodig kunt u uw klachten met betrekking tot ons Platform of onze Diensten voorleggen op het platform voor de oplossing van geschillen dat door de Europese Commissie online werd geplaatst en dat [hier](https://ec.europa.eu/consumers/odr/main/?event=main.home2.show) bereikbaar is. De Europese Commissie zal dan uw klacht naar de bevoegde nationale ombudsman doorsturen. In overeenstemming met de regels die gelden voor bemiddeling, dient u, voordat u een aanvraag voor bemiddeling indient, BlaBlaCar schriftelijk op de hoogte te brengen van het geschil met de bedoeling om een oplossing in der minne te verkrijgen.

U kunt ook een verzoek indienen bij een instantie voor geschiloplossing in uw land (de lijst is [hier te vinden)](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show).

**16.    Wettelijke berichten**
-------------------------------

Het Platform wordt gepubliceerd door Comuto SA, een vennootschap met beperkte aansprakelijkheid met een aandelenkapitaal van € 166,552.163, die ingeschreven is in het handelsregister van Parijs onder nummer 491.904.546 (btw-nummer: FR76491904546) en waarvan het hoofdkantoor gevestigd is in 84, avenue de la République, 75011 Parijs (Frankrijk), vertegenwoordigd door haar Chief Executive Officer Nicolas Brusso, de publicatiedirecteur van de website.

De Website wordt gehost op de servers van Google Ireland Limited, Gordon House, Barrow Street, Dublin 4 (Ierland).

Comuto SA is ingeschreven in het register van reis- en verblijforganisatoren onder het volgende nummer: IM075180037

De financiële garantie wordt verstrekt door: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Parijs – Frankrijk.

De beroepsaansprakelijkheidsverzekering is onderschreven bij: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Parijs – Frankrijk.

Comuto SA is ingeschreven in het register van verzekeringstussenpersonen, banken en financiën met registratienummer (Orias) 15003890.

Met dit [contactformulier](https://support.blablacar.com/s/contactsupport?language=nl_NL) kunt u met al uw vragen bij Comuto SA terecht.

17\. Digital Services Act (Digitaledienstenverordening)  

----------------------------------------------------------

**17.1.  Informatie over gemiddelde actieve afnemers van de dienst in de EU**

Conform artikel 24(2) van de verordening van het Europese Parlement en de Raad betreffende de interne markt voor digitale diensten ter wijziging van Richtlijn 2000/31/EG (“DSA”), moeten online platform providers informatie publiceren over het maandelijkse gemiddelde van actieve afnemers van hun diensten in de EU berekend als een gemiddelde van de afgelopen zes maanden. Het doel is te bepalen of de dienstverleners voldoen aan het criterium voor “zeer grote online platform dienstverleners” onder de DSA, d.w.z. dienstverleners met een maandelijks gemiddelde van meer dan 45 miljoen actieve afnemers in de EU.

Vanaf 31 januari 2024 was het maandelijkse gemiddelde van actieve afnemers van BlaBlaCar voor de periode van augustus 2023 tot januari 2024 berekend rekening houdend met Recital 77 en artikel 3 van de DSA, voordat een specifieke verordening werd aangenomen, ongeveer 4,82 miljoen in de EU.

Deze informatie wordt uitsluitend gepubliceerd om te voldoen aan de vereisten van de DSA en moet niet worden gebruikt voor andere doeleinden. De informatie wordt minstens elke zes maanden bijgewerkt. De manier waarop wij deze informatie berekenen kan evolueren of kan wijzigingen vereisen, bijvoorbeeld vanwege productwijzigingen of nieuwe technologieën.

**17.2. Contactpunt voor autoriteiten**

Conform artikel 11 van de DSA, kunt u als u lid bent van een bevoegde EU-autoriteit, de Europese Commissie of de Europese Raad voor digitale diensten contact met ons opnemen betreffende DSA-aangelegenheden via het e-mailadres [\[email protected\]](https://blog.nl.blablacar.be/cdn-cgi/l/email-protection).

U kunt in het Engels en Frans contact met ons opnemen.

Wij wijzen erop dat dit e-mailadres niet bedoeld is door communicatie met Leden. Met vragen over het gebruik van BlaBlaCar kunt u als Lid contact opnemen via dit [contactformulier](https://support.blablacar.com/s/contactsupport?language=nl_NL).