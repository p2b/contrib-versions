[Condições de utilização - Portal SAPO](https://ajuda.sapo.pt/condicoes-de-utilizacao-portal-sapo-60556)
--------------------------------------------------------------------------------------------------------

* 10.08.16

**O Portal SAPO é uma página /sítio na Internet propriedade da MEO, que funciona como ponto de acesso centralizado a informação disponível na Internet, bem como a canais e serviços prestados pela MEO (todos em conjunto de ora em diante designados por “Portal”).**

**  
PARA PODER UTILIZAR E TIRAR O MELHOR PARTIDO DO PORTAL E DE CADA UM DOS SEUS CANAIS E SERVIÇOS, TENHA EM ATENÇÃO AS SEGUINTES REGRAS:**

* **  
    Não aceda, nem tente aceder, ao Portal através de outros meios que não os disponibilizados pela MEO, nem use formas modificadas de software com o objetivo de, designadamente, obter acesso não autorizado a qualquer parte do Portal.**
* **Não envie qualquer conteúdo ilegal, ameaçador, maldoso, abusivo, assediante, difamatório, injurioso, ordinário, obsceno, indecente, invasivo da privacidade, odioso, racial, ética ou moralmente condenável, prejudicial ou atentatório da dignidade das pessoas, especialmente no caso de menores de idade, ou ainda que possa afetar negativamente a imagem da MEO.**
* **Não disponibilize, envie, utilize ou partilhe qualquer conteúdo ou material que não tenha o direito ou a autorização para utilizar ou disponibilizar ao abrigo de qualquer lei, nomeadamente quando isso viole direitos de propriedade intelectual da MEO ou de terceiros, (por exemplo canções que tenha descarregado ilegalmente e partilha de ficheiros dos quais não possua os respetivos direitos para partilhar), ou ainda deveres de confidencialidade (por exemplo, informações confidenciais obtidas por causa do seu posto de trabalho ou derivadas de acordos com terceiros).**
* **Em geral, não disponibilize ou transmita conteúdos que violem direitos de terceiros, quaisquer que sejam, nomeadamente direitos de personalidade de terceiros, como sejam o direito à imagem e à reserva da vida privada (por exemplo, fotografias de amigos seus que eles não tenham autorizado a circular na Internet);**
* **Quando disponibilizar, enviar ou utilizar, por qualquer forma, software, conteúdos ou materiais que não sejam seus (incluindo, nomeadamente, o seu desenho, configuração e forma de apresentação), deve obter dos seus autores/titulares dos direitos de propriedade intelectual e/ou de direitos de personalidade, como imagem e voz, as devidas autorizações por escrito antes de proceder àqueles atos e, caso necessário, proceder aos pagamentos devidos para o efeito. Não disponibilize conteúdos ou materiais sobre os quais existem reclamações ou processos relativos à titularidade dos mesmos.**
* **Não promova ou disponibilize material instrutivo sobre atividades ilegais, nem material que promova qualquer agressão (física, emocional, etc.) sobre qualquer grupo ou indivíduo, ou que promova qualquer ato de crueldade sobre animais; isto inclui, mas não se limita a: disponibilizar informações sobre construção de bombas, granadas, fabrico de venenos, etc.;**
* **Não disponibilize ou transmita, propositada ou negligentemente, qualquer tipo de material que contenha ou possa conter vírus, worms, defeitos, cavalos de Tróia ou outro item ou códigos informáticos, ficheiros ou programas que sejam suscetíveis de interromper, destruir ou limitar a funcionalidade de qualquer equipamento ou sistema informático (hardware ou software) ou equipamento de telecomunicações;**
* **Não utilize nem explore o Portal para fins comerciais ou que sejam por qualquer forma pagos, incluindo os seus conteúdos, materiais, funcionalidades ou recursos, exceto quanto for expressamente autorizado pela MEO. Nomeadamente, não promova, difunda ou publicite qualquer marca, produto ou serviço, ainda que gratuitamente, sem a prévia autorização escrita da MEO, incluindo, designadamente: ofertas de venda, pedidos de patrocínios e promoção de concursos, mensagens comerciais (banners) de qualquer tipo, incluindo as geradas por serviços de troca de banners. Não disponibilize também quaisquer conteúdos (gráficos ou textuais) que promovam serviços que oferecem dinheiro ou qualquer outro tipo de contrapartidas pelo facto de colocar links para tais páginas através dos serviços e páginas do Portal.**
* **Não envie materiais que expressem ou indiquem falsamente que são patrocinados ou apoiados pela MEO;**
* **Não disponibilize nem envie informação que saiba ou desconfie ser falsa ou enganosa;**
* **Não obtenha informações, mensagens, gráficos, desenhos, arquivos de som e/ou imagens, fotografias, gravações, vídeos e software e, em geral, qualquer tipo de material ou conteúdo disponível no Portal, utilizando meios ou procedimentos distintos daqueles que tenham sido postos à sua disposição ou dos que se usam habitualmente na Internet para tal efeito; não podendo, em qualquer caso, utilizar um método que afete as funcionalidades do Portal ou de qualquer aplicação.**
* **Não utilize o Portal, ou qualquer um dos seus canais ou serviços, para fins não permitidos nas presentes Condições de Utilização, nem sublicencie o software que a MEO lhe coloca à disposição.**
* **Não personifique alguém ou alguma entidade, incluindo mas não limitado a um responsável da MEO, não guie nem receba alguém como se tratasse de um anfitrião, nem testemunhe falsamente parentescos ou ligações com alguém;**
* **Não oculte a sua identidade, salvo no caso de pseudónimos (nicks, alcunhas), em serviços ou semelhantes ou em serviços de proteção da identidade (anonimato);**
* **Não forje cabeçalhos ou qualquer forma de identificação no sentido de disfarçar e/ou camuflar quaisquer conteúdos transmitidos pelos serviços;**
* **Não disponibilize ou transmita qualquer conteúdo não solicitado ou não autorizado como, nomeadamente: e-mails de natureza publicitária, SPAM, junk mail, chain letters, pyramid schemes**
* **Não recolha, armazene, disponibilize, transmita, explore ou reproduza informações sobre outros utilizadores (incluindo nomes de utilizadores e/ou endereços de e-mail) para fins não autorizados;**
* **Não remova qualquer aviso de direitos de autor, marca comercial ou avisos de direitos de propriedade;**
* **Não use qualquer tipo de robô, spider, aplicativos de pesquisa/recuperação de website, ou outro dispositivo para indexar qualquer parte dos canais e serviços do Portal;**
* **Não utilize os canais e serviços do Portal para fins de partilha de ficheiros protegidos por copyright , e**
* **Não falsifique (introduzir, modificar, suprimir ou apagar, no todo ou em parte) dados, após a sua produção, com intenção de iludir e induzir em erro os recetores dos mesmos, incluindo, nomeadamente: alteração de endereços IP (IP Spoofing) e alteração da identificação de mensagens de e-mail.**

 **DEVE AINDA TER EM ATENÇÃO QUE NÃO SÃO PERMITIDOS QUAISQUER ACTOS QUE POSSAM CAUSAR DANOS OU COLOCAR EM RISCO A INTEGRIDADE, CONTINUIDADE OU QUALIDADE DO PORTAL, NOMEADAMENTE:**

* **Violação de sistemas de autenticação ou segurança que proteja contas de acesso, servidores, serviços ou redes;**
* **Acesso ou obtenção não autorizadas de quaisquer dados alheios, pessoais ou não, ficando proibido qualquer tipo de prática de Phishing;**
* **Pesquisa não autorizada de vulnerabilidades ou deficiências em contas de acesso, servidores, serviços, redes, em especial a deteção sistemática de resposta a serviços (Scan);**
* **Entrada em sistemas informáticos, serviços ou redes sem a autorização expressa dos responsáveis ou titulares (Break In);**
* **Ações de sobrecarga que visem sabotar ou sabotem o funcionamento de serviços da MEO ou de terceiros (Denial of Service);**
* **Envio em massa de pacotes (Flooding);**
* **Intercetação e/ou interferência ilegal ou indevida (e tentativa de) em quaisquer dados, sistemas ou equipamentos não estando autorizado para tal;**
* **Utilização de computadores remotos para o encaminhamento de tráfego;**
* **Modificação, adaptação, tradução ou descompilação (reverse engineering) de qualquer parte do software propriedade da MEO; e**
* **Violação ou tentativa de violação das regras de proteção das medidas de carácter tecnológico e das informações para gestão eletrónica dos direitos previstos no Código de Direitos de Autor e dos Direitos Conexos.**

**Para além das presentes Condições de Utilização, leia ainda as Condições de Utilização específicas do canal ou do serviço em causa, disponíveis em** [**http://ajuda.sapo.pt/pt-pt/security/politica-de-privacidade**](http://ajuda.sapo.pt/pt-pt/security/politica-de-privacidade) **. É muito importante que leia estes documentos, pois eles contêm regras de utilização e informação adicional que podem ajudar a utilizar o canal ou o serviço pretendido da melhor forma.**

**SE TIVER MENOS DE 16 ANOS, PEÇA AJUDA AOS SEUS PAIS PARA LHE EXPLICAREM ESTAS CONDIÇÕES DE UTILIZAÇÃO. CASO SINTA QUE É NECESSÁRIO, DEVE APENAS UTILIZAR O PORTAL, OU QUALQUER UM DOS SEUS CANAIS E SERVIÇOS, COM A SUPERVISÃO DOS SEUS PAIS OU ENCARREGADOS DE EDUCAÇÃO.**

**Leia ainda a seguinte informação sobre o Portal. Se tiver alguma dúvida, pode falar connosco através dos contactos disponibilizados em https://ajuda.sapo.pt/contactos-8745.**

1. **De quem é o Portal?**

* **O Portal, cada um dos seus canais e serviços nele disponíveis, a sua estrutura, a seleção, organização e apresentação do seu conteúdo, incluindo as suas funcionalidades e o software utilizado no mesmo, bem como as marcas e logótipos nele apresentados, são propriedade exclusiva da MEO ou das entidades ou pessoas que autorizaram a esta a respetiva utilização.**
* **O Portal foi desenvolvido a pensar nos interesses dos nossos utilizadores. No entanto, não podemos garantir que o Portal, ou qualquer canal, serviço ou funcionalidade disponível no mesmo, vá de encontro a quaisquer necessidades e expectativas que tenha ou que sirva os seus fins específicos.**
* **Tenha ainda em atenção que cabe à MEO gerir o design, layout e disposição de toda a informação, conteúdos e materiais no Portal, pelo que podemos, a qualquer altura, atualizar, modificar ou eliminar quaisquer conteúdos, serviços, opções ou funcionalidades, bem como modificar a sua apresentação e configuração e alterar os respetivos URLs.**

2. **Tenho de fazer alguma coisa para utilizar o Portal?**

* **Para navegar nos canais e serviços SAPO, não precisa, em princípio, de se registar. No entanto, alguns canais e serviços exigem que abra uma Conta Pessoal para poder aceder aos mesmos (doravante designados por “Serviços SAPO”).**
* **Está sujeito a estas Condições de Utilização independentemente de se tratar de um utilizador esporádico, frequente ou registado, e da finalidade do acesso ou do tipo de utilização.**

3. **Como é que eu me posso registar nos canais e serviços que exigem registo?**

**É muito simples abrir uma Conta Pessoal nos Serviços SAPO. Basta aceder à página de registo que lhe é disponibilizada em cada canal ou serviço em que se pretende registar e preencher a informação que lhe pedimos. Para saber tudo sobre a criação de contas em serviços da MEO, leia as Condições Gerais do SAPO ID disponíveis em https://ajuda.sapo.pt/condicoes-de-utilizacao-sapo-id-61045.**

**SE TIVER MENOS DE 16 ANOS, PEÇA AJUDA AOS SEUS PAIS OU ENCARREGADO DE EDUCAÇÃO PARA O AJUDAREM E O ACOMPANHAREM DURANTE O PROCESSO DE REGISTO. NÃO SE REGISTE SEM ANTES FALAR COM OS SEUS PAIS, E OBTER DELES A RESPECTIVA AUTORIZAÇÃO. SE TIVER MENOS DE 14 ANOS, SIGA AS REGRAS DE REGISTO ESPECÍFICAS PARA MENORES DE 14 ANOS QUE ESTÃO NAS CONDIÇÕES GERAIS DO REGISTO SAPO.**

**Em caso de registo em serviços destinados a menores (como sucede com o MAIL KIDS), consulte as regras de registo constantes das respetivas Condições de Utilização.**

4. **Como é que posso aceder ao Portal de cada vez que o queira utilizar?**

* **Se o canal ou serviço em causa exigir registo, basta preencher, nos espaços indicados na página de login do serviço em causa, as suas credenciais de acesso; ou no caso de usar um serviço externo para autenticação, autorizar o Login SAPO a aceder aos seus dados.**
* **Pode aceder à página de acesso do canal ou do serviço por todas as formas e/ou plataformas autorizadas pela MEO, e nomeadamente através do Portal SAPO, ou apenas escrevendo o respetivo endereço de URL na sua página inicial da Internet.**

5. **Que cuidados devo ter ao utilizar o Portal?**

* **Para utilizar o Portal, deve não só respeitar as presentes condições, como todas as regras imperativas que constam da lei, nomeadamente do Código dos Direitos de Autor e Direitos Conexos, do Código da Propriedade Industrial, da Lei da Criminalidade Informática, da Lei da Proteção de Dados Pessoais e do Código da Publicidade.**
* **Deve utilizar o Portal de forma responsável, prudente e cuidadosa, não devendo perturbar ou degradar a continuidade, integridade e qualidade dos recursos e funcionalidades do Portal.**
* **A utilização que faça do Portal é por sua conta e risco, sendo o único responsável por qualquer dano causado ao seu sistema e/ou equipamento informático ou por outros danos ou prejuízos, incluindo perda ou danificação de dados próprios ou de terceiros, que resultem da utilização dos materiais, conteúdos ou informações obtidas, por qualquer forma, através do Portal.**

6. **E se eu não respeitar as condições de utilização do Portal?**

* **Se não respeitar as condições de utilização do Portal, podemos suspender ou encerrar a sua Conta Pessoal no canal ou serviço em causa, ou mesmo suspender ou encerrar a sua Conta Pessoal para todos os canais e serviços nos quais esteja registado.**
* **Podemos ainda remover, sem necessidade de aviso prévio, todo e qualquer conteúdo ou material que tenha disponibilizado através do Portal ou que tenha armazenado nos nossos servidores, quando esses conteúdos ou materiais forem manifestamente ilícitos ou quando tal for determinado por uma entidade competente nos termos legais.**
* **A MEO pode ainda pedir-lhe para apagar e/ou remover imediatamente qualquer conteúdo que viole ou seja suscetível de violar direitos da MEO ou de terceiros, ou que infrinja regras legais.**

7. **A MEO controla os conteúdos disponibilizados através do Portal?**

* **A MEO não está obrigada a pré-visionar, visionar, controlar ou editar quaisquer conteúdos ou materiais disponibilizados através do Portal ou armazenados nos seus servidores, assumindo a MEO que os utilizadores obtiveram todas as autorizações necessárias antes da sua utilização e/ou disponibilização.**
* **Os conteúdos que disponibilize através do Portal, incluindo opiniões, comentários e sugestões, são da sua exclusiva responsabilidade. Deve por isso ter atenção para não utilizar ou disponibilizar conteúdos ilegais ou cuja utilização ou disponibilização não foi autorizada pelos respetivos autores/titulares dos direitos de propriedade intelectual e/ou de direitos de personalidade, como imagem e voz.**
* **A MEO não é responsável pela informação, opiniões ou conteúdos de qualquer tipo que sejam inseridos por si ou por outros utilizadores no Portal, nem por qualquer informação, produto ou serviço que possa ser associado ou anunciado no Portal, incluindo em “banners” ou qualquer forma de publicidade.**
* **Adicionalmente, a MEO não poderá ser responsabilizada, por qualquer forma, pela utilização que terceiros possam dar aos conteúdos, quaisquer que sejam, que tenha disponibilizado ou tornado acessível através do Portal. A MEO informa que, em regra, não limitará a possibilidade de download ou de edição de tais conteúdos.**

**A MEO também não garante que:**

* **Os resultados obtidos através da utilização do Portal sejam corretos, verdadeiros, próprios ou confiáveis;**
* **Qualquer conselho, recomendação ou informação, de qualquer tipo, apresentadas ou disponibilizadas no Portal, ou obtidos através da sua utilização, sejam atuais, rigorosos, completos ou estejam isentos de erros; a MEO informa que não assume qualquer dever jurídico ou responsabilidade nesta matéria;**
* **Qualquer material ou outro tipo de conteúdo disponibilizado por terceiros através do Portal seja seguro, legal ou adequado;**
* **As qualidades, funcionalidades ou características dos produtos, serviços, informações ou outros materiais ou conteúdos adquiridos ou acedidos através do Portal preencham qualquer expectativa dos utilizadores.**

8. **E os Links?**

* **A MEO também não está obrigada a pré-visionar, visionar ou controlar os conteúdos, materiais e serviços disponibilizados em páginas da Internet para onde remetam os links disponibilizados através do Portal, pelo que não se responsabiliza pela legalidade, fidedignidade ou qualidade de qualquer conteúdo aí disponibilizado, nem pelo cumprimento das regras legais aplicáveis em relação aos conteúdos ali disponíveis.**
* **O estabelecimento de links não implica, em caso algum, a existência de relações entre a MEO e o proprietário ou gestor da página web para a qual o link remeta, nem a aceitação ou aprovação pela MEO de qualquer dos conteúdos, serviços ou materiais ali disponibilizados.  
    Sempre que quiser colocar links no Portal, deverá respeitar as seguintes condições:**
* **O link permitirá unicamente o acesso às páginas da Internet mas não as poderá reproduzir por qualquer forma;**
* **Não se poderá indicar ou dar a entender que a MEO autorizou o link ou que supervisionou ou assumiu, por qualquer forma, os conteúdos ou serviços oferecidos ou disponibilizados na página linkada;**
* **À exceção daqueles sinais que façam parte do mesmo link, a página web em que se inclua o link não conterá nenhuma marca, nome comercial, rótulo de estabelecimento, denominação, logótipo, slogan ou outros sinais distintivos pertencentes à MEO;**
* **A página web linkada não deverá conter informações, dados ou conteúdos ilícitos, contrários à moral e aos bons costumes, ilegais ou, de outra forma lesivos de direitos de terceiros.**
* **Os links que coloque são da sua exclusiva responsabilidade.**

9. **O que é que a MEO pode fazer com os meus conteúdos?**

* **Exceto quando acordado expressamente em contrário, ao alojar, nos servidores da MEO, através do Portal, conteúdos e materiais, quaisquer que sejam, protegidos por direitos de propriedade intelectual, está a autorizar, de forma automática, a MEO a, de forma gratuita, divulgar, publicar, utilizar ou explorar tais conteúdos e materiais com o objetivo de divulgá-los em qualquer espaço ou área do Portal ou em qualquer outro espaço gerido pela MEO ou por uma empresa do Grupo PT Portugal. \[Esta autorização cessa no momento em que a sua Conta Pessoal para todos os canais e serviços do Portal for cancelada.\]**
* **Ao disponibilizar no Portal conteúdos ou materiais, está a autorizar que estes possam ser publicitados e incluídos no serviço RSS Feeds do Portal.**

10. **O que é que devo fazer se souber de alguém que esteja a violar as condições de utilização do Portal?**

**Se souber de alguém que esteja a violar as regras de utilização do Portal, nomeadamente disponibilizando conteúdos que pense serem ilegais ou considere ofensivos, deve informa imediatamente a MEO através dos contactos disponibilizados em https://ajuda.sapo.pt/condicoes-de-utilizacao-sapo-id-61045.**

**11.O Portal estará sempre a funcionar e é seguro?**

* **Fazemos todos os esforços para garantir que o Portal e cada um dos seus canais e serviços estão disponíveis sempre que queira utilizá-los. No entanto, não podemos garantir que o Portal e cada um dos seus canais e serviços funcionem de forma ininterrupta, seja isento de erros ou falhas ou que esteja disponível de forma contínua.**
* **Tentamos garantir que o Portal e cada um dos seus canais e serviços não contêm qualquer tipo de vírus ou de outros elementos igualmente perigosos para o seu computador. No entanto, uma vez que não conseguimos controlar integralmente a circulação de informação através da Internet, não conseguimos garantir que os mesmos não contêm qualquer tipo de vírus ou outros elementos que possam danificar o seu computador. Da mesma forma, não podemos garantir que os conteúdos trocados ou enviados através do Portal e de cada um dos seus canais e serviços não possam ser visionados por terceiros aos quais os mesmos não se destinavam.**
* **Para garantir a segurança do Portal, a MEO poderá, a qualquer momento e sem necessidade de aviso prévio, tomar as providências necessárias para garantir a integridade, segurança, continuidade ou qualidade do Portal, incluindo restrições ou limitações de acesso.**
* **Cabe também a si adotar as medidas necessárias para proteger o seu computador. Recomendamos assim que instale um software antivírus e proceda à sua atualização periódica. Deve também informar a MEO sempre que ache que existe uma situação de violação de segurança ou de riscos para a segurança do Portal e/ou de cada um dos seus canais e serviços.**

12. **E se houver falhas de segurança?**

* **Neste caso, a MEO envidará todos os esforços para repor o funcionamento seguro do Portal.**
* **No entanto, a MEO não se responsabiliza pelos danos ou prejuízos que possam resultar, nomeadamente, de:**
* **Utilização ou impossibilidade de utilização do Portal, incluindo, designadamente, atrasos, interrupções, erros, interferências e suspensão de comunicações, omissões, vírus, bugs, e ainda avarias e/ou problemas de funcionamento do sistema eletrónico, informático ou de telecomunicações;**
* **Atrasos ou bloqueios na utilização causados por deficiências ou sobrecargas de Internet ou em outros sistemas eletrónicos, designadamente, falhas no acesso a qualquer parte do Portal;**
* **Suspensão, não funcionamento ou utilização não autorizada dos servidores nos quais o Portal se encontra alojado e/ou de toda a informação e dados ali alojados;**
* **Atuações ilegítimas de terceiros, incluindo o acesso ou a modificação de bases de dados pessoais;**
* **Utilização do nome de utilizador e da palavra-chave por terceiros não autorizados; e**
* **Possíveis erros ou deficiências de segurança que possam ocorrer na utilização de um browser desatualizado ou inseguro, assim como pela ativação dos dispositivos de conservação de palavras-chave ou códigos de identificação do utilizador no browser, ou pelos danos, erros ou inexatidões que possam resultar do mau funcionamento do mesmo.**

13. **O Portal pode fechar?**

* **Sim. A MEO pode, em qualquer altura e sem avisar os utilizadores do Portal:**
* **Suspender, parcial ou totalmente, o acesso a qualquer parte do Portal, em especial nas operações de gestão, manutenção, reparação, alteração ou modernização das mesmas. Quando se trate de operações programadas, e tal seja possível, a MEO divulgará online um aviso sobre a data e duração da intervenção;**
* **Encerrar, definitiva ou temporariamente, parcial ou totalmente, qualquer parte do Portal.**
* **A MEO pode ainda restringir, de acordo com o seu critério, o acesso a qualquer parte do Portal ou sujeitá-lo a condicionalismos estratégicos ou comerciais.**
* **A suspensão ou encerramento do Portal ou de cada um dos seus canais e/ou serviços, por qualquer motivo, não dá direito aos utilizadores a receber qualquer indemnização ou compensação, a que título for, por parte da MEO.**

14. **Informações finais**

* **Ao utilizar o Portal e/ou qualquer um dos seus canais ou serviços está a aceitar o conhecimento e cumprimento das presentes Condições de Utilização.**
* **Recomendamos a leitura regular deste documento, disponível em** [**http://ajuda.sapo.pt/pt-pt/security/condicoes-de-utilizacao/condicoes-de-utilizacao-portal-sapo**](http://ajuda.sapo.pt/pt-pt/security/condicoes-de-utilizacao/condicoes-de-utilizacao-portal-sapo)**, uma vez que a MEO pode alterar as presentes condições em qualquer momento e sem aviso prévio. As novas condições de utilização produzirão os seus efeitos assim que forem inseridas neste documento e publicadas em** [**http://ajuda.sapo.pt/pt-pt/security/condicoes-de-utilizacao/condicoes-de-utilizacao-portal-sapo**](http://ajuda.sapo.pt/pt-pt/security/condicoes-de-utilizacao/condicoes-de-utilizacao-portal-sapo)**. Caso não concorde com alguma das condições de utilização, não deve utilizar o Portal ou qualquer um dos seus canais e serviços.**
* **Estas Condições de Utilização são regidas pela lei portuguesa e, em caso de litígio na interpretação ou aplicação das mesmas, é competente o foro da comarca de Lisboa, com expressa renúncia a qualquer outro.**
* **As referências à MEO neste documento consideram-se aplicáveis, com as necessárias adaptações, às outras empresas gestoras dos Serviços SAPO.**
* **Em caso de contradição entre as presentes Condições de Utilização e os Condições específicas de cada canal ou serviço do Portal, prevalecem estas últimas.**

**SUMÁRIO**

* **O Portal SAPO (“Portal”) é uma página que permite aceder a informação disponível na Internet, bem como a canais e serviços da MEO.**
* **Não aceda, nem tente aceder, ao Portal através de outros meios que não os disponibilizados pela MEO.**
* **Não envie, disponibilize ou utilize conteúdos ilegais, ofensivos, ou que violem direitos de propriedade intelectual de terceiros ou direitos de personalidade (como imagem e privacidade)**
* **Não disponibilize ou transmita conteúdos com vírus ou outros ficheiros/códigos suscetíveis de interromper, destruir ou limitar a funcionalidade de qualquer equipamento ou sistema;**
* **Não utilize nem explore de forma comercial, o Portal, nem promova, difunda ou publicite qualquer marca, produto ou serviço, ainda que gratuitamente.**
* **Não envie materiais que expressem ou indiquem falsamente que tais materiais são patrocinados ou apoiados pela MEO;**
* **Não disponibilize nem envie informações que saiba serem falsas ou enganosas;**
* **Não personifique alguém ou alguma entidade;**
* **Não oculte a sua identidade, salvo no caso de pseudónimos (nicks, alcunhas), em serviços ou semelhantes ou em serviços de proteção da identidade (anonimato);**
* **Não forje cabeçalhos ou qualquer forma de identificação no sentido de disfarçar e/ou camuflar quaisquer conteúdos transmitidos pelos serviços;**
* **Não disponibilize ou transmita qualquer conteúdo não solicitado ou não autorizado como, nomeadamente: e-mails de natureza publicitária, SPAM, junk mail, chain letters, pyramid schemes**
* **Não recolha nem utilize informações sobre outros utilizadores para fins não autorizados;**
* **Não use qualquer tipo dispositivo para indexar qualquer parte dos canais e serviços do Portal;**
* **Não remova qualquer aviso de direitos de autor, marca comercial ou avisos de direitos de propriedade;**
* **Não falsifique dados, incluindo, nomeadamente: alteração de endereços IP (IP Spoofing) e alteração da identificação de mensagens de e-mail.**
* **Não pratique nenhum dos seguintes atos: • violação de sistemas de autenticação ou segurança, • Phishing, • Scan, • Break In, • Ações de sobrecarga que levem a Denial of Service, • envio em massa de pacotes (Flooding), • Intercetação e/ou interferência ilegal ou indevida em quaisquer dados, sistemas ou equipamentos, • utilização de computadores remotos para o encaminhamento de tráfego, • modificação, adaptação, tradução ou descompilação de software, • Violação ou tentativa de violação das regras de proteção das medidas de carácter tecnológico e das informações para gestão eletrónica dos direitos (DRM).**
* **Leia ainda as Condições de Utilização específicas do canal ou do serviço em causa, e a Política de Privacidade, disponíveis em ajuda.sapo.pt.**
* **SE TIVERES MENOS DE 16 ANOS, APENAS DEVES UTILIZAR O PORTAL, OU QUALQUER UM DOS SEUS CANAIS E SERVIÇOS, COM A SUPERVISÃO DOS TEUS PAIS OU ENCARREGADOS DE EDUCAÇÃO.**
* **O Portal e os seus conteúdos são propriedade exclusiva da MEO ou das entidades ou pessoas que autorizaram a esta a respetiva utilização.**
* **Não garantimos que o Portal vá de encontro a quaisquer necessidades e expectativas que tenha ou que sirva os seus fins específicos.**
* **O Portal, os seus conteúdos e serviços, podem ser a qualquer altura modificados ou removidos.**
* **Para navegar no Portal, não precisa, em princípio, de se registar. No entanto, alguns canais e serviços exigem que abra uma Conta Pessoal para poder aceder aos mesmos (“Serviços SAPO”). Para saber como se registar, leia as Condições Gerais do SAPO ID ou as condições de registo aplicáveis aos serviços destinados a menores (como o MAIL KIDS), disponíveis nas respetivas Condições de Utilização.**
* **A utilização que faça do Portal é por sua conta e risco, sendo responsável por qualquer perda ou danificação de dados.**
* **Se não respeitar as regras de utilização do Portal, podemos ter de suspender ou encerrar a sua Conta Pessoal no canal ou serviço em causa, ou suspender ou encerrar a sua Conta Pessoal para todos os canais e serviços nos quais esteja registado.**
* **Podemos ainda remover todo e qualquer conteúdo ou material que tenha disponibilizado através do Portal quando esses conteúdos ou materiais forem manifestamente ilícitos ou quando tal for determinado por uma entidade competente nos termos legais.**
* **Se lhe pedirmos, deve remover os conteúdos que lhe sejam solicitados pela MEO, no prazo máximo de 5 (cinco) dias.**
* **A MEO não está obrigada a pré-visionar, visionar, controlar ou editar quaisquer conteúdos ou materiais disponibilizados através do Portal.**
* **Os conteúdos que disponibilize através do Portal são da sua exclusiva responsabilidade.**
* **A MEO não é responsável pela utilização que terceiros possam dar aos seus conteúdos. A MEO informa que, em regra, não limitará a possibilidade de download ou de edição de tais conteúdos.**
* **A MEO não garante que qualquer conselho, recomendação ou informação, apresentados no Portal, sejam atuais, rigorosos, completos ou estejam isentos de erros, ou que qualquer material ou conteúdo seja seguro ou legal.**
* **A MEO não é responsável pelos conteúdos disponibilizados em páginas linkadas.**
* **Ao colocar links no Portal, não poderá indicar ou dar a entender que a MEO autorizou o link. Os links que coloque são da sua exclusiva responsabilidade.**
* **Ao colocar conteúdos no Portal, está a autorizar a MEO a, de forma gratuita, utilizar tais conteúdos em qualquer espaço ou área do Portal ou em qualquer outro espaço gerido pela MEO ou por uma empresa do Grupo PT Portugal, bem como a incluí-los no serviço RSS Feeds.**
* **Se souber de alguém que esteja a violar as regras de utilização do Portal, deve contactar-nos imediatamente para este e-mail.**
* **Nós fazemos todos os esforços para garantir que o Portal está disponível sempre que queira utilizá-lo. No entanto, não podemos garantir que o Portal funcione de forma ininterrupta, seja isento de erros ou falhas ou que esteja disponível de forma contínua.**
* **Da mesma forma, não podemos garantir que os conteúdos trocados ou enviados através do Portal e de cada um dos seus canais e serviços não possam ser visionados por terceiros aos quais os mesmos não se destinavam.**
* **A MEO não se responsabiliza pelos danos ou prejuízos que possam resultar de, nomeadamente, utilização ou impossibilidade de utilização do Portal que resulte de problemas com os sistemas eletrónico, informático ou de telecomunicações, incluindo os servidores, ou que não resultem de atos da MEO.**
* **A MEO pode, em qualquer altura e sem o avisar antes, suspender ou encerrar o Portal ou qualquer parte dele.**
* **A suspensão ou cessação do Portal ou cada um dos seus canais e/ou serviços, por qualquer motivo, não lhe dá direito a receber uma indemnização ou compensação da MEO.**
* **Ao usar o Portal, está a aceitar estas Condições de Utilização e deve por isso respeitá-las.**
* **Podemos alterar estas Condições de Utilização a qualquer altura e sem aviso prévio. Deve ler regularmente estas Condições de Utilização e, caso não concorde com alguma das regras de utilização, não deve utilizar o Portal.**
* **Em tudo o que não estiver previsto nas presentes Condições de Utilização, será aplicável a lei portuguesa.**

**Sem prejuízo do recurso aos tribunais judiciais ou arbitrais e às entidades responsáveis pela defesa e promoção dos direitos dos consumidores, designadamente a Direção-Geral do**

**Consumidor, o Utilizador pode submeter quaisquer conflitos contratuais aos mecanismos de**

**arbitragem e mediação que se encontrem ou venham a ser legalmente constituídos, bem como**

**reclamar junto da MEO de atos e omissões que violem as disposições legais aplicáveis à prestação do Serviço.**

**Para questões emergentes do Serviço, serão competentes as seguintes entidades de Resolução Alternativa de Litígios de Consumo: CNIACC - Centro Nacional de Informação e Arbitragem de Conflitos de Consumo (**[**https://www.cniacc.pt/pt/**](https://www.cniacc.pt/pt/)**), Centro de Arbitragem da Universidade Autónoma de Lisboa (**[**https://arbitragem.autonoma.pt/**](https://arbitragem.autonoma.pt/)**), CIMAAL - Centro de Informação, Mediação e Arbitragem do Algarve (**[**https://www.consumidoronline.pt/pt/**](https://www.consumidoronline.pt/pt/)**), Centro de Arbitragem de Conflitos de Consumo da Região de Coimbra (**[**https://cacrc.pt/**](https://cacrc.pt/)**), Centro de Arbitragem de Conflitos de Consumo de Lisboa (**[**http://www.centroarbitragemlisboa.pt/**](http://www.centroarbitragemlisboa.pt/)**), Centro de Arbitragem de Conflitos de Consumo da Região Autónoma da Madeira (**[**https://www.madeira.gov.pt/cacc/**](https://www.madeira.gov.pt/cacc/)**), Centro de Informação de**

**Consumo e Arbitragem do Porto (**[**https://www.cicap.pt/**](https://www.cicap.pt/)**), Centro de Arbitragem de Conflitos de Consumo do Ave, Tâmega e Sousa (**[**https://www.triave.pt/**](https://www.triave.pt/)**) e Centro de Informação, Mediação e Arbitragem de Consumo (Tribunal Arbitral de Consumo) (**[**https://www.ciab.pt/pt/**](https://www.ciab.pt/pt/)**) e cujos contactos podem ser consultados em meo.pt ou em** [**www.consumidor.pt**](http://www.consumidor.pt/)**.**

**Última atualização: 28/11/2024**

* [link do post](https://ajuda.sapo.pt/condicoes-de-utilizacao-portal-sapo-60556)
* [favorito](https://blogs.sapo.pt/tools/memadd.bml?journal=ajudasapo&itemid=60556&cw=1)
    
      
    

* [](#)
* [](#)