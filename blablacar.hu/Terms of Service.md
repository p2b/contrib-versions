**_Felhasználási feltételek 2024. augusztus 20-től hatályos_**

**1\. Tárgy**
-------------

A Comuto SA vállalat (a továbbiakban: „**BlaBlaCar**”) kifejlesztett egy telekocsi platformot – amely a(z) [www.blablacar.hu](https://www.blablacar.hu/) címen található weboldalon keresztül, valamint mobilalkalmazás formájában érhető el – azzal a céllal, hogy összekösse egymással az azonos irányba utazó autósokat és utasokat, akik így közösen osztozhatnak a gépjárművön és az utazással kapcsolatos költségeken valamint (ii) hogy az Autóbusz üzemeltetők által működtetett buszos utakra jegyet foglalhassanak (erre a telekocsi platformra a továbbiakban „**Platform**”-ként hivatkozunk).

A jelen Felhasználási Feltételek célja a Platform eléréséhez és használatához kapcsolódó feltételek szabályozása. Kérjük, hogy a jelen dokumentumot figyelmesen olvasd el. Tudomásul veszed, hogy a BlaBlaCar semmilyen módon nem vesz részt szerződéses félként a Platform Tagjai között létrejött megállapodásokban, szerződésekben és/vagy szerződéses jogviszonyokban, függetlenül azok jellegétől.

A „Facebook csatlakozás” vagy a „Feliratkozás e-mail címmel” gombra kattintva elismered, hogy elolvastad és elfogadod a jelen dokumentumban meghatározott általános felhasználási feltételeket.

Ha fiókjával bejelentkezik egy másik ország BlaBlaCar platformjára (például www.blablacar.com.br), kérjük, vegye figyelembe, hogy (i) az adott platform feltételei és feltételei, (ii) az adott platform adatvédelmi szabályzata és (iii) az adott ország törvényi szabályai és előírásai érvényesek. Ez azt is jelenti, hogy a Fiókjával kapcsolatos információkat, beleértve a személyes adatokat is, át kell adni a másik platformot üzemeltető jogi személynek. Felhívjuk figyelmét, hogy a BlaBlaCar fenntartja a jogot, hogy korlátozza a Platformhoz való hozzáférést azon felhasználók számára, akikről ésszerűen azonosítható, hogy az Európai Unió területén kívül tartózkodnak.

**2\. Fogalommeghatározások**
-----------------------------

A jelen dokumentum vonatkozásában a következő kifejezések az alábbiakban meghatározott jelentéssel bírnak:

„**Általásnos Értékesítési Feltételek**”: Felhasználó által kiválasztott Buszos útra vonatkozó és az értinett Autóbusz üzemeltető Általános Értékesítési Feltételei valamint a Weboldalon közzétett különös szerződési feltételek, melyek elolvasását a Felhasználó a megrendelés előtt elismeri;

„**Autóbusz üzemeltető**”: olyan utasokat hivatásszerűen szállító társaság, amelynek jegyeit a BlaBlaCar terjeszti a Platformon;

„**Autós**”: az a Tag, aki a Platform segítségével egy Utazás keretében személyszállítási szolgáltatást kínál más személyek számára a Költség-hozzájárulás ellenében a kizárólag az Autós által meghatározott időpontban;

„**BlaBlaCar**”: a fenti 1. szakaszban meghatározott jelentéssel bír;

„**Busz Hirdetés**”: egy Autóbusz üzemeltető Buszos útjára vonatkozó, a Platformon közzétett hirdetés;

„**Buszos út**” a Platformon közzétett Busz Hirdetés tárgyát képező olyan út, melyre az Autóbusz üzemeltető a buszban viteldíj ellenében Üléseket kínál;

„**Értékesítési Pont**”: a Weboldalon felsorolt pultok és terminálok ahol a Jegyeket eladásra kínálják;

„**Facebook-fiók**”: a lenti 3.2. szakaszban meghatározott jelentéssel bír;

„**Felhasználási Feltételek**”: a jelen felhasználási feltételek;

„**Felhasználó**”: bármely természetes személy (Tagságra tekintet nélkül), aki saját vagy más Utas részére, az Autóbusz üzemeltető által biztosított Utazás céljából Buszjegyet vásárol a Platformon keresztül;

„**Fiók**”: olyan fiók, amelyet kötelező létrehozni mindazok számára, akik szeretnének Taggá válni, és szeretnék a Platformon keresztül kínált bizonyos szolgáltatásokat igénybe venni;

„**Foglalás**”: a lenti 4.2. szakaszban meghatározott jelentéssel bír;

„**Foglalás-visszaigazolás**”: a lenti 4.2.1. szakaszban meghatározott jelentéssel bír;

„**Hirdetés**”: egy Autós által a Platformon közzétett Utazással kapcsolatos hirdetés;

„**Jegy**”: a Felhasználó nevére szóló és részére a Buszos Utazás Foglalását követően kiállított érvényes utazási jogosultság, amely igazolja az Utasok és az Autóbusz üzemeltető között a Felhasználási Feltételek alapján létrejött szerződést, tekintet nélkül az Utas és az Autóbusz üzemeltető között külön kikötött és a Jegyen feltüntetett feltételekre;

„**Költség-hozzájárulás**”: egy adott Utazás vonatkozásában az Autós által kért, és az Utas által elfogadott azon összeg, amelynek a segítségével az Utas hozzájárul az utazási költségekhez;

„**Platform**”: a fenti 1. szakaszban meghatározott jelentéssel bír;

„**Rendelés**”: az a művelet, amellyel a Felhasználó, bármilyen módon lefoglalja a Szolgáltatásokat a BlaBlaCar Busznál, kivéve a Jegyek közvetlenül az Értékesítési Ponton történő megvásárlását, és amellyel az Utas vagy megbízottja kötelezettséget vállal a megrendelt Szolgáltatások árának megfizetésére.

„**Szállítási Szolgáltatások**”: az Autóbusz üzemeltető áltál kínált és a Buszos út Utasa által igénybevett szállítási szogáltatások;

„**Szolgáltatási Díjak”** az alábbi 5.2 pontban meghatározott jelentéssel bír(nak).

„**Szolgáltatások**”: a BlaBlaCar által a Platformon keresztül kínált összes szolgáltatás;

„**Tag**”: minden olyan természetes személy, aki a Platformon keresztül létrehozott egy Fiókot;

„**Tagi Tartalom**”: a lenti 11.2. szakaszban meghatározott jelentéssel bír;

„**Telekocsi Hirdetés**”: egy Autós által a Platformon közzétett Utazásmegolsztással kapcsolatos hirdetés;

„**Telekocsi utazás**”: a Platformon az Autós által közzétett Telekocsi Hirdetés tárgyát képező olyan út, melyen az Utasokat Költség hozzájárulás fejében szállítja;

„**Ülés**”: az Utas által a Autós járművében lefoglalt ülés;

„**Utas**”: olyan Tag, aki elfogadta az Autós személyszállítási szolgáltatásra tett ajánlatát, vagy adott esetben olyan személy, akinek a nevében és megbízásából egy Tag lefoglalt egy Ülést;

Az „**Utazás**”: elválaszthatatlanul buszos utazásra vagy telekocsi utazásra utal;

„**Útszakasz**”: a lenti 4.1. szakaszban meghatározott jelentéssel bír;

„**Viteldíj**”: egy adott Buszos út ára, amely tartalmaz minden adót, djíjat  és a vonatkozó szolgáltatások költségeit, melyet a Felhasználó a Platformon keresztül egy Buszos útra szóló Ülés foglalására vonatkozó Rendelés érvényesítése során megfizet;

„**Weboldal**”: a(z) [www.BlaBlaCar.hu](https://www.blablacar.hu/) címen elérhető weboldal.

**3\. A Platformon történő regisztráció, valamint Fiók létrehozása**
--------------------------------------------------------------------

### **3.1. A Platformon történő regisztráció feltételei**

A Platformot kizárólag a 18. életévüket betöltött természetes személyek használhatják. A Platformra szigorúan tilos kiskorúaknak regisztrálni. A Platform elérésével, használatával, és/vagy a Platformon történő regisztrációval kijelented és szavatolod, hogy a 18. életévedet betöltötted.

### **3.2. Fiók létrehozása**

A Platform lehetővé teszi a Tagok számára, hogy Telekocsi Hirdetéseket tehessenek közzé, Hirdetéseket tekinthessenek meg, valamint hogy egymással kapcsolatba léphessenek egy-egy Ülés lefoglalásával kapcsolatban. A közzétett Hirdetéseket a Platformra nem regisztrált személyek is megtekinthetik. Azonban egy Hirdetés közzétételének, valamint egy Ülés lefoglalásának az előfeltétele a Fiók létrehozása, és ezáltal a Taggá válás.

BlaBlaCar Fiókot a következőképpen hozhatsz létre:

(i) a regisztrációs űrlapon szereplő összes kötelező mező kitöltésével;

(ii) vagy a Platformon keresztül a Facebook-fiókodba történő bejelentkezéssel (a továbbiakban: „**Facebook-fiók**”). Ezen funkció használatával tudomásul veszed, hogy a BlaBlaCar hozzáférhet a Facebook-fiókodból származó bizonyos információkhoz, valamint azokat a Platformon közzéteheti, és saját magának megőrizheti. A BlaBlaCar Fiókod és a Facebook-fiókod közötti összerendelést bármikor törölheted a profilod „Ellenőrzés” című szakaszában. Ha szeretnél bővebb információkat kapni arról, hogy a Facebook-fiókodból származó adatokat miként használjuk fel, kérjük, olvasd el az [Adatkezelési Tájékoztatónkat](https://www.blablacar.hu/about-us/privacy-policy), valamint a Facebook adatvédelmi szabályzatát.

A Platformra csak abban az esetben regisztrálhatsz, ha előtte elolvastad és elfogadtad a jelen Felhasználási Feltételeket.

Vállalod, hogy a Fiók létrehozása során (függetlenül a létrehozás módjától) pontos és valós adatokat szolgáltatsz magadról, valamint hogy a Fiók létrehozása során szolgáltatott adatokat naprakészen tartod a profilodon keresztül vagy a BlaBlaCar felé küldött értesítés segítségével, így garantálva a szolgáltatott adatok pontosságát és aktualitását a közted és a BlaBlaCar között létrejött szerződéses jogviszony teljes fennállása során.

E-mail címmel történő regisztráció esetén vállalod, hogy a Fiók létrehozása során megadott jelszót titkosan kezeled, és azt nem hozod mások tudomására. Vállalod, hogy a jelszó esetleges elveszítése vagy mások tudomására hozatala esetén azonnal értesíted a BlaBlaCar-t erről. Kizárólagos felelősséget vállalsz a Fiókod harmadik felek általi esetleges használatából eredő következményekért, kivéve, ha kifejezetten értesítetted a BlaBlaCar-t a jelszavad elveszítéséről, harmadik fél általi illetéktelen felhasználásáról; vagy harmadik fél tudomására hozataláról.

Vállalod, hogy a kezdetben létrehozott Fiókodon kívül semmilyen más Fiókot nem hozol létre és/vagy nem használsz sem a saját nevedben, sem bármely harmadik fél nevében.

### **3.3. Ellenőrzés**

A BlaBlaCar az átláthatóság biztosítása, a bizalom erősítése, valamint az esetleges csalási kísérletek megelőzése és/vagy észlelése érdekében jogosult egy olyan rendszert létrehozni, amelynek a segítségével ellenőrizheti a profilodban meghatározott bizonyos információkat. Ez különösen igaz arra az esetre, ha megadod a telefonszámodat, vagy a rendelkezésünkre bocsátod a személyazonosításra alkalmas valamely igazolványodnak a másolatát.

Tudomásul veszed és elfogadod, hogy a Platformon és/vagy a Szolgáltatásokban található, az „ellenőrzött” információk (ideértve az „Igazolt Profilra”) vagy ahhoz hasonló egyéb kifejezésre vonatkozó hivatkozás kizárólag annyit jelent, hogy az adott Tag sikeresen keresztülment a – Platformon és/vagy a Szolgáltatásokban üzemeltetett – ellenőrzési eljáráson annak érdekében, hogy a rendszer bővebb információkat nyújthasson neked arról a Tagról, akivel közös telekocsi utat tervezel. A BlaBlaCar mindazonáltal nem garantálhatja az ellenőrzési eljárás során ellenőrzött adatok valódiságát, megbízhatóságát és/vagy érvényességét.

**4\. A Szolgáltatások használata**
-----------------------------------

### **4.1. Hirdetések közzététele**

Ha Taggá váltál, és az összes alábbi feltételt sikeresen teljesíted, akkor jogosult vagy Telekocsi Hirdetéseket létrehozni és közzétenni a Platformon oly módon, hogy beírod a tervezett Utazásoddal kapcsolatos információkat (például az indulási és az érkezési dátumot és időpontot, a beszállási pontokat, a kínált Ülések számát, az elérhető opciókat, a Költség-hozzájárulás összegét stb.).

A közzétenni kívánt Telekocsi Hirdetés beállítása során feltüntetheted, hogy mely köztes városokban tervezel megállni, valamint hogy az Utasok mely köztes városokban szállhatnak be és/vagy ki. Az Utazásnak a köztes városok közötti szakaszait, valamint egy köztes város és a beszállási pont vagy célállomás közötti szakaszait a jelen dokumentum vonatkozásában „Útszakaszok”-nak hívjuk.

Kizárólag a következő feltételek maradéktalan teljesülése esetén vagy jogosult Telekocsi Hirdetést közzétenni:

(i) érvényes gépjármű-vezetői engedéllyel rendelkezel;

(ii) kizárólag olyan járművekre vonatkozóan teszel közzé Hirdetést vagy Hirdetéseket, (i) amelyeknek te vagy a tulajdonosa, valamint amelyek használatára kifejezett engedéllyel rendelkezel; és (ii) amelyeket jogosult vagy telekocsi célokra használni;

(iii) a Hirdetéshez vagy Hirdetésekhez kapcsolódó járművet elsősorban te vezeted;

(iv) az adott jármű érvényes kötelező gépjármű-felelősségbiztosítással rendelkezik;

(v) orvosilag vagy más módon nem tiltottak el a gépjárművezetéstől, illetve arra vonatkozóan ellenjavallatot nem kaptál;

(vi) az Utazáshoz használni kívánt jármű olyan utazó célú személygépjármű, amely 4 kerékkel és legfeljebb 7 üléssel rendelkezik;

(vii) nem szándékozol a Platformon ugyanarra az Utazásra vonatkozóan egy másik Hirdetést is közzétenni;

(viii) nem kínálsz több Ülést annál, mint ahány üléssel a járműved rendelkezik;

(ix) minden felkínált Üléshez kapcsolódik biztonsági öv, még olyan esetben is, ha a jármű biztonsági öv nélküli üléssel vagy ülésekkel is megkapta a forgalomba helyezési engedélyt;

(x) műszakilag jó állapotú járművet használsz, amely megfelel a vonatkozó jogszabályi rendelkezéseknek és előírásoknak, különösen ideértve az érvényes műszaki vizsgát és zöldkártyát (környezetvédelmi engedélyt)

(xi) fogyasztó vagy, és nem minősülsz hivatásos, szakmai célú felhasználónak.

Tudomásul veszed, hogy a Platformon általad közzétett Telekocsi Hirdetések tartalmáért kizárólag te vagy felelős. Ebből kifolyólag kijelented és szavatolod, hogy az általad közzétett Telekocsi Hirdetésben szereplő összes információ pontos és valós, valamint vállalod, hogy az Utazást az általad közzétett Telekocsi Hirdetésben szereplő feltételekkel teljesíted.

A Telekocsi Hirdetésedet közzétesszük, így az látható lesz nemcsak a Tagok számára, hanem mindazon személyek számára is, akik a Platformon vagy a BlaBlaCar partnereinek a weboldalán Utazást keresnek. A BlaBlaCar a jogosult a saját kizárólagos hatáskörében, megtagadni egy közzétenni kívánt Telekocsi Hirdetés közzétételét és/vagy egy korábban közzétett Telekocsi Hirdetést bármikor eltávolítani a rendszerből, ha az adott Hirdetés nem felel meg a jelen Felhasználási Feltételekben foglalt előírásoknak, vagy ha a BlaBlaCar úgy véli, hogy az adott Hirdetés sérti a BlaBlaCar, a Platform és/vagy a Szolgáltatások jó hírnevét, és/vagy törölni az ilyen Hirdetést közzétevő Tag Fiókját a jelen Felhasználási Feltételek 9. pontjában foglaltak szerint.

Tájékoztatunk, hogy abban az esetben, ha a Platform használatakor hivatásos, szakmai célú felhsználóként jársz el, miközben egyszerű fogyasztóként tünteted fel magad, a vonatkozó jogszabályok által előírt szankcióknak leszel kitéve.

### **4.2. Ülés foglalása**

A BlaBlaCar létrehozott egy rendszert az ülőhelyek online foglalására („Foglalás”) a Platformon kínált utazásokhoz.

Az ülőhelyfoglalás módja a tervezett utazás jellegétől függ.

A BlaBlaCar a Platformon a felhasználók számára különböző keresési kritériumok (származási hely, célállomás, időpontok, utazók száma stb.) alapján keresőmotort biztosít. A keresőmotor bizonyos további funkciókat biztosít, ha a felhasználó a Fiókján keresztül csatlakozik. A BlaBlaCar felkéri a felhasználót, hogy az alkalmazott foglalási eljárástól függetlenül, gondosan tekintse át és használja a keresőmotort, hogy meghatározza az igényeinek leginkább megfelelő ajánlatot. További információk itt érhetők [el](https://blog.blablacar.hu/about-us/a-platformok-atlathatosaga). Az Értékesítési Pontokon Buszos utat foglaló Felhasználó a keresés elvégzésére az Autóbusz üzemeltetőt vagy a pultos ügyintézőt is megkérheti.

**4.2.1.Telekocsi utazás**

Ha egy Utas egy Telekocsi Hirdetés után érdeklődik, akkor az Utas adott esetben online is elküldheti a Foglalási kérelmét. A Foglalási kérelem kétféleképpen hagyható jóvá: (i) automatikusan, ha az Autós a közzétett Hirdetés beállítása során ezt a lehetőséget választotta; vagy (ii) kézileg, az Autós által. A foglaláskor az Utas online fizeti meg a Szolgáltatási Díjakat, amennyiben szükséges. A fizetés BlaBlaCar általi kézhezvételéről valamint a Foglalási kérelem Autós általi jóváhagyásáról az Utas adott esetben foglalás-visszaigazolást (a továbbiakban: „**Foglalás-visszaigazolás**”) kap.

Ha Autósként a Foglalási kérelmek kézi jóváhagyásának vagy elutasításának a lehetőségét választottad a közzétett Telekocsi Hirdetés beállítása során, akkor köteles vagy az Utasok által küldött Foglalási kérelmekre az adott Utasok által a Foglalási kérelmeik beállítása során meghatározott határidőn belül válaszolni. Ellenkező esetben a Foglalási kérelem automatikusan elavul és az Utasnak vissza kell téríteni a foglalási kérelem benyújtásakor befizetett összegeket, ha voltak ilyenek.

A BlaBlaCar a Foglalás-visszaigazolás részeként előtt, vagy annak részeként elküldheti neked az Autós telefonszámát (amennyiben Utas vagy), illetve az Utas telefonszámát (amennyiben Autós vagy), ha a Tag beleegyezik a telefonszámának feltüntetésébe. Ezt követően kizárólagos felelősséggel tartozol azért, hogy a másik Taggal létrejött kötelező érvényű megállapodásában foglaltaknak eleget tegyél.

**4.2.2 Buszos út**

Buszos utak esetén a BlaBlaCar lehetővé teszi adott Buszos útra vonatkozó Buszjegyek Foglalását a Platformon keresztül.

A Szállítási Szolgáltatásokra, a Felhasználó által kiválasztott Utat kínáló Autóbusz üzemeletető Általános Értékesítési Feltételei vonatkoznak, melyet a Felhasználónak a rendelés megerősítése előtt el kell fogadnia. A BlaBlaCar nem kínál Buszos utakra vonaktozó szállítási szolgáltatásokat, ami azt jelenti, hogy az Általános Értékesítési Feltételek tekintetében kizárólag az Autóbusz üzemeltetők szerepelnek szerződéses félként. Elismerekd, hogy egy adott Buszos útra történő Ülésfoglalás feltétele az érintett Autóbusz üzemeltető az Általános Értékesítési Feltételeinek elfogadása.

A BlaBlaCar felhívja a figyelmet arra, hogy a Busz üzemeltető az  általa a Weboldalon közzétett egyes Szállítási Szolgáltatásokat visszavonhatja, különösen időjárási okok, szezonális változások vagy vis major esteétn.

**4.2.3. Az Ülések név szerinti Foglalása, valamint a Szolgáltatások harmadik felek nevében és megbízásában történő használatára vonatkozó feltételek**

A Szolgáltatások Utas vagy Autós minőségben történő igénybe vétele névhez kötött. Az Autós és az Utas kizárólag azok a személyek lehetnek, akiknek a személyazonosságát a BlaBlaCar és az Utazásban részt vevő többi Tag felé kommunikálták.

Ugyanakkor a BlaBlaCar lehetővé teszi a Tagjai számára, hogy harmadik felek nevében és megbízásából is foglaljanak egy vagy több Ülést. Ilyen esetben vállalod, hogy az Autós felé pontosan jelzed a Foglalás leadásakor annak a személynek a keresztnevét, életkorát és telefonszámát, akinek a nevében és megbízásából az adott Ülést szeretnéd lefoglalni. Szigorúan tilos 13 év alatti kiskorú személy részére Ülést foglalni, ha vele nem utazik kísérő. Ha 13. életévét betöltött, egyedül utazó kiskorú személy számára szeretnél Ülést foglalni, akkor válllalod, hogy ezzel kapcsolatban beszerzed az Autós előzetes hozzájárulását, valamint hogy az Autós rendelkezésére bocsátod a hiánytalanul kitöltött és az adott személy törvényes képviselője vagy törvényes képviselői által aláírt meghatalmazást.

A Platformon keresztül kizárólag természetes személyek számára foglalhatók Ülések. Tilos Ülést foglalni tárgyak, csomagok, kísérő nélkül utazó állatok és/vagy anyagi javak szállítása céljából.

Továbbá tilos más Autós nevében és megbízásából Hirdetést közzétenni.

### **4.3. Tagi tartalom, moderálás és értékelési rendszer**

**4.3.1. Az értékelési rendszer működése**

A BlaBlaCar arra biztat, hogy írjál értékelést arról az Autósról (ha Utas vagy), illetve arról az Utasról (ha Autós vagy), akivel egy Utazás keretében együtt utaztál vagy együtt terveztél utazni (vagyis akivel együtt foglaltál Utazást). Ugyanakkor nem vagy jogosult egy másik Utasról értékelést készíteni, ha te is Utasként vettél részt az adott Utazáson, illetve olyan Tagról, akivel egy Utazás keretében nem utazotál együtt vagy nem terveztél együtt utazni. Lehetőséged van arra, hogy az Utazást követő 14 napon belül értékelést hagyj.

Az általad írt értékelés, valamint egy másik Tag által rólad készített értékelés a következő időpontok közül a korábban bekövetkező időpontban jelenik meg a Platformon: (i) közvetlenül azt követően, hogy mind a ketten elkészítettétek az értékeléseteket; vagy (ii) az elsőként elkészült értékelés időpontját követő 14 nap elteltével.

Ha egy másik Tag értékelést ír rólad, akkor az értékelésnek a profilodon történő fogadásától számított 14 nap áll a rendelkezésedre, hogy válaszolj az értékelésre. A kapott értékelés és adott esetben az adott értékelésre küldött válaszod megjelenik a profilodban.

**4.3.2. Moderálás**

**a. Tagi tartalom**

Tudomásul veszed és elfogadod, hogy a BlaBlaCar a 11.2. pontban meghatározott Tagi tartalmat annak közzététele előtt moderálhatja akár automatizált eszközök révén, akár manuálisan. Amennyiben a BlaBlaCar véleménye szerint az ilyen Tagi tartalom sérti a vonatkozó jogszabályokat vagy a jelen Felhasználási Feltételeket, fenntartja a jogot arra, hogy:

* megakadályozza az ilyen tagi tartalom közzétételét vagy törölje azt;
* figyelmeztetést küldjön az adott Tagnak, emlékeztetve őt arra, hogy kötelessége betartani a vonatkozó jogszabályokat és a jelen Felhasználási Feltételeket; és/vagy
* foganatosítsa a jelen Felhasználási Feltételek 9. pontja szerinti korlátozó intézkedéseket.

A BlaBlaCar automatizált eszközök révén vagy manuálisan végzett moderálása nem tekintendő annak vállalásaként vagy azon kötelezettségként, hogy mindenkor monitorozza a Platformon közzétett Tagi tartalmakat vagy aktívan pásztázza a Platformot illegális tevékenységek után kutatva, és a vonatkozón jogszabályok által megengedett mértékig nem keletkezik ezzel kapcsolatos felelőssége.

**b. Üzenetek**

A Tagok Platformon keresztüli üzenetváltása („Üzenetek”) kizárólag az Utazásokkal kapcsolatos információk cseréjét szolgálhatja.

A BlaBlaCar automatizált szoftverek vagy algoritmusok révén kiolvashatja az Üzenetek tartalmát a csalás megelőzése, a szolgáltatások javítása, ügyfélszolgálati célok és a Tagokkal között szerződések (például a jelen Felhasználási Feltételek) érvényesítése érdekében. Ha egy Üzenetben csalárd vagy törvénytelen magatartásra, illetőleg a Platform megkerülésére utaló jeleket talál, vagy az egyéb módon ellent mond a jelen Felhasználási Feltételeknek:

* előfordulhat, hogy az ilyen tartalom nem kerül közzétételre;
* az adott Üzenetet küldő Tag figyelmeztetést kaphat, melyben emlékeztetjük arra, hogy kötelessége betartani a vonatkozó jogszabályokat és a jelen Felhasználási Feltételeket; vagy
* a Tag Fiókja a jelen Felhasználási Feltételek 9. pontja szerint felfüggesztésre kerülhet.

Az ilyen automatizált szoftverek BlaBlaCar általi használata nem tekintendő annak vállalásaként vagy azon kötelezettségként, hogy mindenkor monitorozza a Platformon közzétett tartalmakat és/vagy aktívan pásztázza a Platformot illegális tevékenységek után kutatva, és a vonatkozón jogszabályok által megengedett mértékig nem keletkezik ezzel kapcsolatos felelőssége.

**4.3.3. Korlátozás**

A jelen Felhasználási Feltételek 9. pontja értelmében a BlaBlaCar fenntartja a jogot arra, hogy a Fiókodat felfüggessze, és a Szolgáltatásokhoz történő hozzáférésedet korlátozza, vagy hogy a jelen Felhasználási Feltételeket megszüntesse, amennyiben (i) legalább három darab értékelést kaptál; és (ii) az általad kapott egyetlen értékelés vagy az értékelések átlagértéke 3 vagy annál kevesebb – attól függően, hogy az értékelés keretében egy másik Tag milyen súlyos dolgot fogalmazott meg.

**5\. Pénzügyi feltételek**
---------------------------

A Platformhoz való hozzáférés és a platformon való regisztráció, valamint a keresés, a hirdetések megtekintése és feladása ingyenes. A Foglalás azonban az alábbiakban ismertetett feltételek szerint díjköteles.

### **5.1. Költség-hozzájárulás és Viteldíj**

5.1.1. Telekocsi út esetén a Költség-hozzájárulás összegének a meghatározása kizárólag az Autós felelőssége. Szigorúan tilos nyereséget realizálni a Platform használatából. Ebből kifolyólag vállalod, hogy az Utasoktól összesítve nem kérsz több Költség-hozzájárulást annál, mint amennyi pénzbe valójában és ténylegesen kerül neked az Utazás teljesítése. Ellenkező esetben kizárólagosan köteles vagy vállalni annak a kockázatát, hogy a Platformon keresztül végrehajtott tranzakciót utólag esetlegesen átminősítik.

A BlaBlaCar azt javasolja, hogy egy-egy Telekocsi Hirdetés beállítása során a tervezett Utazás jellegének és a megtenni tervezett távolságnak a figyelembe vételével határozd meg a Költség-hozzájárulás összegét. Ez az összeg azonban csak irányadó összeg; saját belátásod szerint dönthetsz arról, hogy csökkented-e vagy növeled-e ezt az összeget annak függvényében, hogy valójában és ténylegesen neked mennyi pénzedbe kerül az Utazás teljesítése. A BlaBlaCar a visszaélések megakadályozása érdekében korlátozza a Költséghozzájárulás-módosítási lehetőségeknek a számát.

5.1.2 Buszos út esetén, egy Ülésre vonaktozó Viteldíj meghatározása az Autóbusz üzemeltető kizárólagos hatáskörébe tartozik. Felhívjuk a  Felhasználó figyelmét arra, hogy a vonatkozó Általános Értékesítési Feltételeket olvassa el, hogy megértse a Jegy rendelésre és fizetési feltételekre vonatkozó feltételeket.

### **5.2. Szolgáltatási díjak**

A BlaBlaCar a Platform használatáért cserébe, a foglalás időpontjában szolgáltatási díjakat (a továbbiakban: „**Szolgáltatási díjak**”) szed. A felhasználót a Szolgáltatási Díjak alkalmazása előtt tájékoztatjuk, amennyiben szükséges. 

A hatályos Szolgáltatási Díjak számítási módjait a BlaBlaCar külön is közzéteheti, kizárólag tájékoztató jellegűek, és nem rendelkeznek szerződéses értékkel. A Szolgáltatási Díjak kiszámítása különböző tényezők, különösen az Utazás hossza és ára alapján történhet. A BlaBlaCar fenntartja a jogot, hogy bármikor módosítsa a Szolgáltatási Díjak számítási módszereit. Ezek a módosítások nem érintik a felhasználó által a módosítások hatályba lépése előtt elfogadott Szolgáltatási Díjakat.

Kérjük, vedd figyelembe, hogy a határokon átnyúló utazások esetében a Szolgáltatási Díjak összegének és az alkalmazandó ÁFA-nak a kiszámítási módszerei az utazás beszállási pontjától és/vagy célállomásától függően változnak.

A Platform határokon átnyúló utazásokhoz vagy Magyarországon kívüli utazásokhoz történő használatakor a Szolgáltatási Díjakat a BlaBlaCar helyi platformot üzemeltető leányvállalata számíthatja fel.

### **5.3. Kerekítés**

Tudomásul veszed és elfogadod, hogy a BlaBlaCar a saját kizárólagos hatáskörében lefelé vagy felfelé kerekítheti a Költség-hozzájárulásként meghatározott összeget vagy a Szolgáltatási Díjakat.

### **5.4. A Költség-hozzájárulás Autós, vagy az Autóbusz üzemeltető részére történő kifizetéséneke és visszafizetésének módjai**

**5.4.1 A Költség-hozzájárulás kifizetése az Autós részére**

Az Utas vállalja, hogy a Költség-hozzájárulás összegét legkésőbb a kiszállási ponton kifizeti az Autós felé.

Az Autós vállalja, hogy az Utazás megkezdése előtt nem kéri kifizetni a Költség-hozzájárulást sem részben, sem egészben.

**5.4.2 A Viteldíj kifizetése**

A Platformon keresztüli Rendelések kifizetése az alábbiakban engedélyezett módokon történik. A Felhasználó egy vagy több hitelkártyára vonatkozó információt  elmenthet a Felhasználói Fiókjába, így későbbi fizetések alkalmával ezeket az adatokat nem kell újra megadnia. 

A fizetési módok a használt BlaBlaCar platformtól függően változhatnak. Az engedélyezett fizetései módok a következők:

* Bankkártya (az elfogadott márkák a platformon fel vannak tüntetve. Ha társ márkájú kártyával rendelkezel, a fizetési szakaszban kiválaszthatsz egy adott márkát)
* Paypal
* Utalványok, 
* Apple Pay, Google Pay.

A Rendelés nem igazolható vissza a Felhasználó által kiválasztott Szállítási Szolgáltatások teljes és tényleges kifizetése előtt. Ha a fizetés szabálytalan, hiányos vagy a Felhasználünak tulajdonítható bármilyen okból nem történt meg, a Rendelést haladéktalanul törölni kell.

A Rendelés viszaigazolása a Felhasználónak küldött e-mailben történik, amely tartalmazza a Rendelés részleteit.

Javasoljuk, hogy az Utas ellenőrizze az e-mail címére beérkező levelek beállításait annak biztosítása érdekében, hogy a visszaigazoló e-mail ne kerüljön közvetlenül a levélszemét mappába.

A Rendelés visszaigazolás végleges. Következésképpen, a vonatkozó Általános Értékesítési Feltételek szerint, minden változtatás cserét vagy törélést von maga után. A Felhasználó felelőssége, hogy a Szállítási Szolgáltatásokat az Utas igényeinek és elvárásainak megfelelően válasszák ki. A Felhasználó felel az általa megadott Személyes adatok pontosságáért, kivéve, ha bizonyítja, hogy a BlaBlaCar e Személyes adatok gyűjtését, tárolását vagy védelmét elmulasztotta.

**6\. A Szolgáltatások és a Platform nem kereskedelmi és/vagy nem üzleti jellege**
----------------------------------------------------------------------------------

Elfogadod, hogy a Szolgáltatásokat és a Platformot kizárólag nem kereskedelmi és/vagy nem üzleti jellegű céllal veszed igénybe, azért hogy kapcsolatba léphess olyan személyekkel, akik szeretnének egy Utazás keretében Telekocsizniegyütt telekocsizni veled, vagy Buszos útra Ülést foglalni.

A telekocsi keretében elismerted, hogy az uniós fogyasztóvédelmi jogból eredő fogyasztói jogok nem vonatkoznak a többi taggal való kapcsolatodra.

Autósként vállalod, hogy nem kérsz olyan összegű Költség-hozzájárulást, amely meghaladná részedről az adott Utazással kapcsolatban ténylegesen felmerült költségeket, és amely számodra nyereséget realizálna, figyelembe véve azt is, hogy a költségmegosztás vonatkozásában Autósként köteles vagy az Utazással kapcsolatban felmerülő költségekből rád eső részt kifizetni. Kizárólagos felelősséggel tartozol az adott Utazással kapcsolatban felmerülő költségek kiszámításáért, valamint annak ellenőrzéséért, hogy az Utasoktól általad kért Költség-hozzájárulások összege nem haladja meg részedről az adott Utazással kapcsolatban ténylegesen felmerülő költségeknek a te saját részeddel csökkentett összegét.

A BlaBlaCar fenntartja a jogot arra, hogy felfüggessze a Fiókodat olyan esetben, ha sofőr által vezetett járművet, taxiként funkcionáló járművet, céges autót vagy egyéb vállalati és/vagy kereskedelmi célú járművet használsz, és ezáltal nyereséget realizálsz a Platform használatából. Vállalod, hogy a BlaBlaCar első felszólítására bemutatod a BlaBlaCar részére a járműved forgalmi engedélyét és/vagy egyéb olyan dokumentumát, amely igazolja, hogy jogosult vagy az adott járművet a Platformon használni, és hogy az adott jármű Platformon történő használatából nem realizálsz pénzügyi nyereséget.

A jelen Felhasználási Feltételek 9. pontja értelmében a BlaBlaCar fenntartja a jogot arra is, hogy felfüggessze Fiókodat, korlátozza a Szolgáltatásokhoz történő hozzáférésedet, vagy hogy megszüntesse a jelen Felhasználási Feltételeket: ha olyan tevékenységet végezel a Platformon, amely a kínált Utazások jellege és gyakorisága, a szállított Utasok száma és/vagy a kért Költség-hozzájárulások összeg alapján arra enged következtetni, hogy nyereséget realizálsz ezen tevékenységből; vagy bármely olyan okból, amely azt sugallja a BlaBlaCar felé, hogy nyereséget realizálsz a Platform használatából.

**7\. Visszamondás**
--------------------

### **7.1. A visszatérítés feltételei lemondás esetén**

Ezek a feltételek csak Telekocsi utakra vonatkoznak. A BlaBlaCar nem vállal semmiféle garanciát bármilyen okból történő lemondás esetén. Buszos út esetén a lemondási feltételekre az Autóbusz üzemeltető Általános Értékesítési Feltételei irányadók. Javasoljuk, hogy a felhasználó olvassa át a vonatkozó Általános Értékesítei Feltételeket, hogy megértse a Buszos utat érintő változtatásokra és lemondásra vonatkozó feltételeket.

A telekocsi utazáson való helynek az Autós vagy az Utas által a foglalás visszaigazolását követően történő lemondására az alábbi rendelkezések vonatkoznak: 

– Az Autósnak köszönhető lemondás esetén az Utasnak visszatérítik a Szolgáltatási díjat. Ez az eset különösen akkor áll fenn, ha az Autós:

* nem erősítette meg a foglalási kérelmet a megadott határidőn belül (ha az Autós ilyen lehetőséget választott);
* a telekocsi-utazást, vagy nem érkezik meg a találkozási pontra 15 perccel a megbeszélt időpont után;

–Az Utasnak köszönhető lemondás esetén a szolgáltatási díjakat a BlaBlaCar visszatartja.

A Szolgáltatási Díj visszatérítésének módja a Szolgáltatási Díj befizetésére használt módtól függően kerül meghatározásra, míg a Szolgáltatási Díj visszatérítésének határideje a banki szervezetek általi átutalások időzítésétől függ.

Ha az Utas az Utazás megkezdése előtt visszamondja a lefoglalt Ülést vagy Üléseket, akkor ezek az Ülések automatikusan felszabadulnak, így azokat más Utasok online lefoglalhatják, és így a jelen Felhasználási Feltételekben foglaltak az új Utasokra is érvényesek lesznek.

A BlaBlaCar saját belátása szerint, a rendelkezésre álló információk alapján nagyra értékeli a visszatérítési kérelmek jogosságát.

### **7.2. Visszalépési jog**

A jelen Felhasználási Feltételek elfogadásával kifejezetten elfogadod, hogy a Te és a BlaBlaCar közötti, egy másik Taggal való kapcsolatból álló szerződés a foglalási visszaigazolás óta eltelt elállási határidő lejárta előtt teljesül, így kifejezetten lemondasz az elállási jogodról, a  45/2014. (II. 26.) Korm. rendelet a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól 9.20.(2). cikkének rendelkezéseivel összhangban.

**8\. A Platform felhasználóinak és a Tagoknak a viselkedése**
--------------------------------------------------------------

### **8.1. A Platform összes felhasználójára vonatkozó kötelezettségek**

Tudomásul veszed, hogy kizárólagos felelősséggel tartozol a Platform használatára vonatkozó összes törvény, jogszabály és/vagy kötelezettség betartásáért és/vagy teljesítéséért.

Továbbá a Platform használata során, valamint az Utazások során vállalod, hogy:

(i) a Platformot nem használod semmilyen szakmai, kereskedelmi és/vagy nyereségszerzési célra;

(ii) a BlaBlaCar felé (különösen a Fiók létrehozása és/vagy esetleges aktualizálása során) vagy más Tagok felé nem küldesz semmilyen hamis, megtévesztő, rosszindulatú és/vagy csalárd információt;

(iii) nem beszélsz, nem viselkedsz és/vagy nem teszel közzé tartalmat (köztük Üzeneteket) a Platformon olyan módon, amely bármilyen formában (i) becsületsértő, becsmérlő, obszcén, pornográf, vulgáris, sértő, agresszív, kéretlen, erőszakos, fenyegető, zaklató, rasszista és/vagy idegengyűlölő jellegű; és/vagy (ii) szexuális utalást tartalmaz; és/vagy (iii) erőszakot, diszkriminációt (hátrányos megkülönböztetést) és/vagy gyűlöletet gerjeszt; és/vagy (iv) illegális szerek használatára és/vagy illegális szerekkel kapcsolatos tevékenység végzésére bátorít; és/vagy (v) általánosabb értelemben véve illegális, a vonatkozó jogszabályoknak, a jelen Felhasználási Feltételeknek vagy a Platform eredeti rendeltetési céljának ellentmond; és amely a BlaBlaCar vagy bármely harmadik fél bármely jogát sérti, vagy a közerkölcsbe ütközik;

(iv) nem sérted meg a BlaBlaCar semmilyen jogát (különösen ideértve a BlaBlaCar-t megillető szellemi tulajdonjogokat) és/vagy nem rontod a BlaBlaCar imázsát és/vagy hírnevét;

(v) a Platformon egynél több Fiókot nem hozol létre saját magad részére, illetve hogy harmadik fél vagy harmadik felek nevében egyetlen Fiókot sem hozol létre a Platformon;

(vi) nem próbálod megkerülni a Platformon kínált online Foglalási rendszert (különösen ideértve azt az esetet, hogy ha azért próbálod meg elküldeni a saját elérhetőségi adataidat egy másik Tag felé, hogy a Foglalást a Platformon kívül bonyolítsd le és így ne kelljen megfizetned a szolgáltatási díjakat);

(vii) nem lépsz kapcsolatba másik Taggal (különösen ideértve a Platformon keresztüli kapcsolatfelvételt) a telekocsi szolgáltatás feltételeinek egyeztetésétől eltérő célból;

(viii) nem fogadsz el és nem teljesítesz fizetést a Platformon kívül;

(ix) betartod a jelen Felhasználási Feltételekben, az Adatkezelési Tájékoztatóban foglaltakat és a Platform rád vonatkozó egyéb feltételeit.

### **8.2. Az Autósokra vonatkozó kötelezettségek**

Továbbá, ha Autósként veszed igénybe a Platformot, akkor vállalod és/vagy kijelented, hogy:

(i) tiszteletben tartasz minden, a vezetésre és a járműre vonatkozó törvényt, jogszabályt és kódexet, különösen ideértve az alábbiakat: gondoskodsz róla, hogy az Utazás ideje alatt rendelkezz felelősségbiztosítással és gépjármű-vezetői engedéllyel;

(ii) utánanézel, hogy biztosításod lefedi-e a telekocsi szolgáltatást, illetve harmadik félnek minősülő Utasaidra határátlépés esetén is a teljes Utazás időtartama alatt van-e biztosítási fedezet;

(iii) vezetés közben nem vállalsz semmilyen kockázatot és nem használsz olyan árucikket, amely elvonhatja a figyelmedet, illetve az éber és teljesen biztonságos vezetés kárára mehet;

(iv) csak a ténylegesen tervezett Utazásokra vonatkozó Telekocsi Hirdetéseket teszel közzé;

(v) az Utazást a Telekocsi Hirdetésben leírtak szerint teljesíted (különös tekintettel az autópálya használatára vonatkozóan), illetve az időpontokra és egyéb helyekre (különös tekintettel a találkozási helyekre és a kiszállási pontokra) vonatkozóan a többi Taggal történt megegyezés szerint jársz el;

(vi) a Telekocsi Hirdetésben megjelölt Ülések számánál több Utast nem szállítasz;

(vii) műszakilag jó állapotú járművet használsz, amely megfelel a vonatkozó jogszabályi rendelkezéseknek és előírásoknak, különösen ideértve az érvényes műszaki vizsgát és zöldkártyát (környezetvédelmi engedélyt);

(viii) hogy a BlaBlaCar vagy bármely Utas felszólítására bemutatod az gépjármű-vezetői engedélyedet, a járműved forgalmi engedélyét, a biztosításáról szóló igazolást, a zöldkártyát (környezetvédelmi engedélyt), és minden olyan dokumentumot, ami igazolja, hogy Autósként veheted igénybe a Platformot;

(ix) forgalmi akadály miatti késés vagy a Telekocsi Utazás időpontjának megváltozása esetén azonnal értesíted az Utasaidat;

(x) nemzetközi Telekocsi Utazás esetén magadnál tartod, illetve az Utas és a hatóságok számára felszólítás esetén elérhetővé teszed a személyazonosságodat és a határátlépésre jogosító dokumentumokat;

(xi) az Utasokra a megbeszélt találkozási helyen a megbeszélt időpontban, illetve azon túlmenően legalább 15 percig vársz;

(xii) csak olyan járműre vonatkozó Telekocsi Hirdetést teszel közzé, ami te tulajdonodban van, illetve amit jogosult vagy telekocsi célokra használni;

(xiii) biztosítod, hogy Utasaid felvehetik veled a kapcsolatot a profilodon regisztrált telefonszámon;

(xiv) nem realizálsz nyereséget a Platform használatából;

(xv) orvosilag vagy más módon nem tiltottak el a gépjárművezetéstől, illetve arra vonatkozóan ellenjavallatot nem kaptál;

(xvi) a Telekocsi Utazás során megfelelően és felelősségteljesen, a telekocsi szolgáltatás alapelveit betartva viselkedsz;

(xvii) nem utasítasz vissza senkitől Foglalást faji, etnikai, nemzetiségi, vallási okokból vagy szexuális irányultság, családi állapot, fogyatékosság, fizikai megjelenés, házassági állapot, terhesség miatt, illetve gazdasági helyzet, név, tartózkodási hely, egészségügyi állapot vagy politikai vélemény okán különösen kiszolgáltatott helyzet miatt;

(xviii) biztosítod, hogy Utasaid felvehetik veled a kapcsolatot a profilodon regisztrált telefonszámon, beleértve a találkozási helyet is.

### **8.3. Az Utasokra vonatkozó kötelezettségek**

**8.3.1 Telekocsi Útra vonatkozóan**

Ha Utasként veszed igénybe a Platformot, vállalod, hogy:

(i) a Telekocsi Utazás során megfelelő magatartást tanúsítasz annak érdekében, hogy ne zavard az Autóst a koncentrálásban vagy a vezetésben, illetve nem zavarod meg más Utasok csendjét és nyugalmát;

(ii) az Autós járművével tisztelettel bánsz és óvod tisztaságát;

(iii) forgalmi akadály miatti késés esetén azonnal értesíted az Autóst;

(iv) az Autósnak a megbeszélt Költség-hozzájárulási összeget fizeted ki;

(v) az Autósra a találkozási helyen a megbeszélt időpontban, illetve azon túlmenően legalább 15 percig vársz;

(vi) hogy a BlaBlaCar vagy bármely Autós felszólítására bemutatod a személyi igazolványodat vagy bármely, személyazonosságot igazoló dokumentumodat;

(vii) nem viszel a Telekocsi Utazásra olyan használati cikket, árucikket, anyagot vagy állatot, ami megzavarhatja az Autóst a vezetésben és a koncentrálásban, vagy aminek természete, birtoklása vagy szállítása ellentétes a hatályos jogszabályok rendelkezéseivel;

(viii) nemzetközi Telekocsi Utazás esetén magánál tartod, illetve az Autós és a hatóságok számára felszólítás esetén elérhetővé teszed a személyazonosságodat és a határátlépésre jogosító dokumentumokat;

(ix) annak biztosítása érdekében, hogy sofőrje telefonon felvehesse Önnel a kapcsolatot a profiljában regisztrált számon, beleértve a találkozási pontot is.

Abban az esetben, ha egy vagy több Ülést harmadik felek nevében foglaltál le a fenti 4.2.3 szakaszban meghatározottak szerint, akkor garantálod, hogy az érintett harmadik fél betartja a jelen szakasz előírásait, illetve általánosságban a jelen Felhasználási Feltételeket. A jelen Felhasználási Feltételek 9. pontja értelmében a BlaBlaCar fenntartja a jogot arra is, hogy felfüggessze a Fiókodat, korlátozza a Szolgáltatásokhoz történő hozzáférésedet vagy, hogy a jelen Felhasználási Feltételek 9. szakasza alapján   megszüntesse a jelen Felhasználási Feltételeket abban az esetben, ha a harmadik fél, akinek nevében a jelen Felhasználási Feltételek szerint Ülést foglaltál, megsérti az előírásokat.

**8.3.2 Buszos Utak**

Az Utas Buszos Utak tekintetében az Utas vállalja, hogy betartja az érintett Autóbusz üzemeltető Általános Értékesítési Feltételeiben foglaltakat.

**8.4. Helytelen vagy illegális tartalom jelentése (értesítési és intézkedési mechanizmus)**

A gyanús, helytelen vagy illegális Tagi tartalmat vagy Üzenetet az [itt](https://support.blablacar.com/s/article/Illeg%C3%A1lis-tartalom-jelent%C3%A9se-1729197120599?language=hu) leírtak szerint jelentheted.

A BlaBlaCar a jelen pontban foglaltak szerint kapott vagy az illetékes hatóságok részéről érkező szabályszerű értesítést követően haladéktalanul eltávolít minden törvénytelen Tagi tartalmat, ha:

* a Tagi tartalom egyértelműen törvénytelen vagy ellent mond a vonatkozó jogszabályoknak; vagy
* a BlaBlaCar az adott tartalmat a jelen Felhasználási Feltételeknek ellent mondónak ítéli.

Ilyen esetben a BlaBlaCar fenntartja a jogot arra, hogy eltávolítsa a szabályszerűen, megfelelő részletességgel és érthető módon jelentett Tagi tartalmat és/vagy azonnali hatállyal felfüggessze a jelentett Fiókot.

Az érintett Tag az alábbi 15.1. pontban foglaltak szerint fellebezhet a BlaBlaCar fentiek szerinti döntése ellen.

**9\. A Platform használatával kapcsolatos korlátozások, Fiókok felfüggesztése, hozzáférés korlátozása és megszüntetés**
------------------------------------------------------------------------------------------------------------------------

A közted és a BlaBlaCar között létrejött szerződéses jogviszonyt bármikor díjmentesen és minden ok nélkül megszüntetheted. Ehhez mindössze a Profiloldalon lévő „Fiók megszüntetése” fülre kell lépned.

Amennyiben (i) megsérted a jelen Felhasználási Feltételeket, beleértve, de nem kizárólagosan a fenti 6. és 8. szakaszban meghatározott, Tagként vállalt kötelezettségeidet (ii) megszeged a fenti 4.3.3 szakaszban meghatározott korlátozást vagy (iii), ha a BlaBlaCar-nak valódi oka van azt feltételezni, hogy szükségessé vált saját, a Tagok, illetve harmadik felek biztonságának és tisztességének védelme, különböző vizsgálatokat vagy a csalás, megelőzése érdekében, akkor a BlaBlaCar fenntartja a jogot arra, hogy:

(i) azonnal és értesítés nélkül megszüntesse a közted és a BlaBlaCar között létrejött kötelező érvényű Felhasználási Feltételeket; és/vagy

(ii) megakadályozza, hogy közzétegyél vagy eltávolítsa az általad a Platformon közzétett bármely Tagi tartalmat; és/vagy

(iii) korlátozza hozzáférésedet és a Platform használatát; és/vagy

(iv) átmenetileg vagy végleg felfüggessze Fiókodat.

A Fiók felfüggesztése adott esetben azt is jelentheti, hogy nem kapod meg a függőben lévő kifizetéseket.

Esettől függően a BlaBlaCar figyelmeztetést küldhet neked, emlékeztetve arra, hogy kötelességed betartani a vonatkozó jogszabályokat és/vagy a jelen Felhasználási Feltételeket. 

Amennyiben ilyen jellegű intézkedésre van szükség, erről értesítést fogsz kapni annak érdekében, hogy lehetőséged legyen megerősíteni a jelen Felhasználási Feltételek és a vonatkozó jogszabályok betartása melletti elköteleződéseket, tisztázni magad a BlaBlaCar előtt vagy fellebezni a döntés ellen. A BlaBlaCar az egyes esetek körülményeit és a szabálysértés súlyosságát figyelembe véve saját kizárólagos hatáskörében dönt majd az intézkedések életbe léptetéséről.

**10\. Személyes adatok**
-------------------------

A Platform használatának vonatkozásában a BlaBlaCar az [Adatkezelési Tájékoztatóban](https://blog.blablacar.hu/about-us/privacy-policy)  foglaltak szerint össze fog gyűjteni és fel fog dolgozni bizonyos személyes adatokat.

**11\. Szellemi tulajdonjog**
-----------------------------

**11.1. A BlaBlaCar által közzétett tartalom**

A Tagok által rendelkezésre bocsátott tartalmakat tekintve minden szellemi tulajdonjog kizárólag a BlaBlaCar-t illeti meg a Szolgáltatás, a Platform, a tartalmak (különösen ideértve a szövegeket, képeket, kivitelezéseket, logókat, videókat, hangokat, adatokat, grafikákat), a működést biztosító szoftver és adatbázis vonatkozásában.

A BlaBlaCar nem kizárólagos, személyre szóló és nem átruházható jogot biztosít számodra a Platform és a Szolgáltatások személyes és magáncélú, nem kereskedelmi alapon, illetve a Platform és a Szolgáltatások céljaival összhangban történő használatára vonatkozóan.

A Platform, a Szolgáltatások és azok tartalmának egyéb jellegű használata és hasznosítása tilos a BlaBlaCar előzetes írásos engedélye nélkül. Különösen tilos:

(i) A Platform, a Szolgáltatások és a tartalom reprodukálása, módosítása, átdolgozása, terjesztése, nyilvános képviselete és népszerűsítése, kivéve, ha az a BlaBlaCar által kifejezetten jóváhagyásra kerül.

(ii) a Platform és a Szolgáltatások visszafejtése és ellentétes irányú tervezése a hatályos jogszabályszövegekben meghatározott kivételeket figyelembe véve;

(iii) a Platformadatok jelentős részének kigyűjtése vagy kigyűjtésének megkísérlése (különös tekintettel az adatbányász robotok vagy más hasonló adatgyűjtő eszközök használatára).

**11.2. A Platformon általad közzétett tartalom**

A Szolgáltatásnyújtás lehetővé tétele érdekében és a Platform céljával összhangban a BlaBlaCar egy nem kizárólagos felhasználási engedélyt biztosít számodra a Szolgáltatások igénybevételével (többek között a Foglalási kérelmekkel, Hirdetésekkel, hozzászólásokkal, bemutatkozásokkal, fotókkal, értékelésekkel és értékelésekre adott válaszokkal) összefüggésben általad rendelkezésre bocsájtott tartalom és adatok, valamint Üzenetek használatára vonatkozóan (a továbbiakban „**Tagi tartalom**”). Annak érdekében, hogy a BlaBlaCar a digitális hálózaton keresztül és bármely kommunikációs (különös tekintettel az internetes és mobil hálózati) protokoll előírásainak megfelelően terjeszkedjen, és a Platform tartalmát rendelkezésre bocsájthassa a nyilvánosság számára, felhatalmazod a BlaBlaCar-t, hogy az egész világon, illetve a közted és a BlaBlaCar között létrejött szerződéses jogviszony fennállása alatt reproduklája, feltüntesse, átdolgozza és lefordítsa a Tagi tartalmat az alábbiak szerint:

(i) Felhatalmazod a BlaBlaCar-t, hogy a Tagi tartalom minden részét reprodukálja bármely, akár ismert akár még nem ismert digitális adathordozón – különös tekintettel bármely kiszolgálóra, merevlemezre, memóriakártyára vagy más ezekkel egyenértékű adathordozóra – bármely, akár ismert vagy akár nem ismert formátumban és eljárással olyan mértékben, ami a Platform működtetése és a Szolgáltatásnyújtás tekintetében a tárolási, biztonsági mentési, továbbítási vagy letöltési műveletekhez szükséges;

(ii) Felhatalmazza a BlaBlaCar-t, hogy átdolgozza és lefordítsa a Tagi tartalmat , illetve reprodukálja ezeket az átdolgozásokat bármilyen jelenlegi vagy jövőbeni digitális adathordozón a fenti (i) pontban meghatározottak szerint Szolgáltatásnyújtás céljából, különös tekintettel a különböző nyelveken történő Szolgáltatásnyújtásra. Ez a jogosultság különösen vonatkozik a Tagi tartalom formátumának módosítási lehetőségére az erkölcsi jogaidnak tiszteletben tartása mellett, a Platform grafikai irányelvének betartása céljából és/vagy annak érdekében, hogy technikailag alkalmas legyen a Platformon történő közzétételre.

A Platformra feltöltött Tagi tartalomért és Üzenetekért te felelsz, és azokhoz fűződően minden jog téged illet meg.

**12\. A BlaBlaCar szerepköre**
-------------------------------

A Platform egy online hálózati felület, amin a Tagok telekocsi célokra tervezett Telekocsi Utazásokat bejelentő Telekocsi Hirdetéseket hozhatnak létre és tehetnek közzé. Ezeket a Telekocsi Hirdetéseket főként a többi Tag tekintheti meg, így tájékozódhat az Utazás feltételeiről, és adott esetben közvetlenül Ülést foglalhat a Platformon a Hirdetést közzétevő Tag érintett járműjében. A Plaform a Buszos utakra törétnő Ülésfoglalást szintén lehetővé teszi.

A Platform használatával és a jelen Felhasználási Feltételek elfogadásával tudomásul veszed, hogy a BlaBlaCar semmilyen módon nem vesz részt szerződéses félként (i) a közted és más Tagok között létrejött megállapodásokban az Utazásokkal kapcsolatos költségek megosztására irányuló szándékkal, (ii) vagy .a közted és azt Autóbusz üzemeltető között létrejött szerződésben.

A BlaBlaCar nem befolyásolhatja a Tagok , az Autóbusz üzemeltetők és követítőik, valamint a Platform felhasználóinak magatartását. A Hirdetésekben szereplő járműveknek nem tulajdonosa, azokat nem hasznosítja, nem szereli fel és nem kezeli, illetve nem kínál semmilyen Utazást a Platformon.

Tudomásul veszed és elfogadod, hogy a BlaBlaCar nem szabályozhatja a kínált Hirdetések, Ülések és Utazások érvényességét, hitelességét vagy jogszerűségét. Utazásmegosztást közvetítőként a BlaBlaCar nem nyújt szállítási szolgáltatást, és nem jár el fuvarozóként; a BlaBlaCar szerepköre a Platformhoz való hozzáférés lehetővé tételére korlátozódik.

A Telekocsi Utak tekintetében a Tagok (Autósok vagy Utasok) kizárólagos és teljes felelősségük tudatában járnak el.

Közvetítőként a BlaBlaCar nem vonható felelősségre az Utazások hatékony lebonyolítása miatt, különösen az alábbiak miatt:

(i) az Autós vagy az Autóbusz üzemeltető téves információkat közölt Hirdetésében vagy bármely más módon az Utazással és annak feltételeivel kapcsolatban;

(ii) a Tag vagy az Autóbusz üzemeltető törli vagy módosítja az Utazást;

(iii) az Utas nem fizeti ki a Költség-hozzájárulási összegét;

(iv) a Tagok Utazás közbeni, előtti vagy utáni magatartása miatt.

**13\. A Platform működése, elérhetősége és funkciói**
------------------------------------------------------

A BlaBlaCar lehetőség szerint köteles megtenni mindent azért, hogy a Platform napi 24 órában, a hét minden napján elérhető legyen. Mindemellett a Platformhoz való hozzáférés átmenetileg előzetes értesítés nélkül műszaki karbantartás, áttelepítés, frissítési műveletek vagy leállás, illetve a hálózat működésével kapcsolatos korlátozások miatt szünetelhet.

Ezenkívül a BlaBlaCar fenntartja a jogot, hogy egyes felhasználók számára a Platformot vagy annak egyes részeit módosítsa új funkciók tesztelése és jobb felhasználói élmény biztosítása érdekében, valamint a Platformhoz vagy annak funkcióihoz való teljes vagy részleges hozzáférés módosítása vagy felfüggesztése, saját belátása szerint. , ideiglenesen vagy véglegesen.

**14\. A Felhasználási Feltételek módosítása**
----------------------------------------------

A jelen Felhasználási Feltételek és a jelen Felhasználási Feltételeknek hivatkozás útján a részét képező egyéb dokumentumok képezik közted és a BlaBlaCar között a Szolgáltatások igénybevétele tárgyában létrejött teljes megállapodást. Minden más dokumentum, különös tekintettel a Platformon megjelenő dokumentumokra (GYIK, stb.), csak tájékoztatási célt szolgál.

A BlaBlaCar módosíthatja jelen Felhasználási Feltételeket annak érdekében, hogy azt átdolgozza saját műszaki és kereskedelmi környezetéhez illően, illetve, hogy megfeleljen a hatályos jogszabályok rendelkezéseinek. A Felhasználási Feltételek bárminemű módosítása közzétételre kerül a Platformon a hatályba lépés dátumának megjelölésével. A módosításról a BlaBlaCar még a hatályba lépés előtt küld értesítést.

**15\. Alkalmazandó jog és jogviták**
-------------------------------------

Jelen Felhasználási Feltételek magyar nyelven íródtak és a magyar jog hatálya alá tartoznak.

**15.1. Belső panaszkezelési rendszer**

Lehetőséged van fellebbezni az alábbiakat érintő döntéseinkkel szemben:

* Tagi tartalom: ha például eltávolítottunk egy általad a Platform használata közben közzétett Tagi tartalmat, korlátoztuk annak láthatóságát vagy elutasítottuk az eltávolítás iránti kérelmet; vagy
* Fiókod: felfüggesztettük a platformhoz való hozzáférésedet.

Ez a lehetőség azonban csak akkor él, ha a döntést azért hoztuk meg, mert a Tagi tartalom illegálisnak vagy a jelen Felhasználási Feltételeknek ellent mondónak bizonyult. A fellebbezési eljárásról részletesebben [itt olvashatsz](https://support.blablacar.com/s/article/Hogyan-fellebbezhetsz-tartalom-elt%C3%A1vol%C3%ADt%C3%A1sa-vagy-fi%C3%B3kt%C3%B6rl%C3%A9s-ellen-1729197123031?language=hu).

**15.2. Bíróságon kívüli jogvitarendezés**

A Platformmal és a Szolgáltatásokkal kapcsolatos panaszaidat szükség esetén az Európai Bizottság által online módon biztosított vitarendezési felületen is jelezheted. A felületet [itt](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage) érheted el. Az Európai Bizottság panaszodat az illetékes nemzeti ombudsmannak továbbítja. A közvetítésre vonatkozó szabályozások szerint minden közvetítésre irányuló kérelem benyújtása előtt köteles vagy írásban értesíteni a BlaBlaCar-t bármely vita kapcsán békés megoldás keresése érdekében.

Az országodban illetékes békéltető testülethez is nyújthatsz be kérelmet (a listát [itt](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show) találod).

**16\. Jogi közlemények**
-------------------------

A Platform üzemeltetője a Comuto SA vállalat (alaptőkéje: 155 880 999 euró; a Párizsi Cégjegyzékbe 491.904.546 számon van bejegyezve; közösségen belüli HÉA-szám: FR76491904546); székhelye: 84, avenue de la République, 75011 Párizs (Franciaország); képviselője: Nicolas Brusson vezérigazgató és weboldal-publikálási igazgató.

A Weboldalnak a Google Cloud szerverei biztosítanak helyet Hollandiában.

A Comuto SA a következő számon szerepel az utazásszervezők nyilvántartásában: IM075180037.

A pénzügyi garanciát a Groupama Assurance-Crédit & Caution nyújtja, 8-10 rue d’Astorg, 75008 Párizs – Franciaország.

A szakmai felelősségbiztosítást a következő társaság köti: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Párizs – Franciaország.

A Comuto SA be van jegyezve a biztosításközvetítői, banki és pénzügyi nyilvántartásba az (Orias) 15003890 nyilvántartási számon.

Kérdés esetén felveheti a kapcsolatot a Comuto SA vállalattal [ezen a kapcsolati űrlapon](https://support.blablacar.com/s/contactsupport?language=hu) keresztül.

**17\.** **Digital Services Act (digitális szolgáltatásokról szóló rendelet)**
------------------------------------------------------------------------------

**17.1. Az EU-ban nyújtott szolgáltatások átlagos havi aktív felhasználóira vonatkozó információk**

Az Európai Parlament és Tanács belső piacon nyújtott digitalis szolgáltatásokról szóló, 2000/31/EK irányelvet módosító rendelete (Digital Services Act, „DSA”) 24. cikkének (2) bekezdése értelmében az online platformok szolgáltatóinak közzé kell tenniük a Unióban nyújtott szolgáltatás havi átlagos aktív felhasználóira vonatkozó információkat, az elmúlt hat hónap átlagaként kiszámítva. Ez a követelmény annak megállapítására szolgál, hogy az online platform úgynevezett óriásplatform-e, amely – a DSA meghatározása szerint – havonta legalább 45 millió aktív felhasználóval rendelkezik az EU-ban.

2024\. augusztus 17-én a BlaBlaCar aktív címzettjeinek átlagos havi száma a 2024. február és július közötti időszakban, a DSA 77. preambulumbekezdésének és 3. cikkének figyelembevételével számítva (a 6 hónapos időszak során csak egyszer számítva a platformtartalomnak kitett felhasználókat) , egy konkrét felhatalmazáson alapuló jogi aktus elfogadása előtt körülbelül 3,28 millió volt az EU-ban.

Ezen információ közzététele kizárólag a DSA követelményeinek való megfelelést szolgálja, arra egyéb célokból támaszkodni nem megengedett. A számadatot legalább hathavonta frissítjük. A számítás módja idővel fejlettebb lehet vagy módosulhat például a termékek megváltozása vagy új technológiák miatt.

**17.2. Kapcsolattartó a hatóságok számára**

A DSA 11. cikke értelmében amennyiben valamely EU-beli hatóság, az Európai Bizottság vagy a Digitális Szolgáltatások Európai Testületének képviselőjeként jársz el, a DSA-val kapcsolatos ügyekben a [\[email protected\]](https://blog.blablacar.hu/cdn-cgi/l/email-protection) e-mail-címen veheted fel velünk a kapcsolatot.

Ezen az e‑mail‑címen angol és francia nyelvű kommunikációra van lehetőség.

Felhívjuk figyelmedet, hogy ez az e-mail-cím nem tagokkal való kommunikációra használatos. A BlaBlaCar használatával kapcsolatos kérdésekkel Tagként a [kapcsolatfelvételi űrlapon](https://support.blablacar.com/s/contactsupport?language=hu) veheted fel velünk a kapcsolatot.

* * *

**_2024\. április 19-én hatályba lépő Felhasználási Feltételek_**

**1\. Tárgy**
-------------

A Comuto SA vállalat (a továbbiakban: „**BlaBlaCar**”) kifejlesztett egy telekocsi platformot – amely a(z) [www.blablacar.hu](https://www.blablacar.hu/) címen található weboldalon keresztül, valamint mobilalkalmazás formájában érhető el – azzal a céllal, hogy összekösse egymással az azonos irányba utazó autósokat és utasokat, akik így közösen osztozhatnak a gépjárművön és az utazással kapcsolatos költségeken (erre a telekocsi platformra a továbbiakban „**Platform**”-ként hivatkozunk).

A jelen Felhasználási Feltételek célja a Platform eléréséhez és használatához kapcsolódó feltételek szabályozása. Kérjük, hogy a jelen dokumentumot figyelmesen olvasd el. Tudomásul veszed, hogy a BlaBlaCar semmilyen módon nem vesz részt szerződéses félként a Platform Tagjai között létrejött megállapodásokban, szerződésekben és/vagy szerződéses jogviszonyokban, függetlenül azok jellegétől.

A „Facebook csatlakozás” vagy a „Feliratkozás e-mail címmel” gombra kattintva elismered, hogy elolvastad és elfogadod a jelen dokumentumban meghatározott általános felhasználási feltételeket.

**2\. Fogalommeghatározások**
-----------------------------

A jelen dokumentum vonatkozásában a következő kifejezések az alábbiakban meghatározott jelentéssel bírnak:

„**Hirdetés**”: egy Autós által a Platformon közzétett Utazással kapcsolatos hirdetés;

„**BlaBlaCar**”: a fenti 1. szakaszban meghatározott jelentéssel bír;

„**Felhasználási Feltételek**”: a jelen felhasználási feltételek;

„**Fiók**”: olyan fiók, amelyet kötelező létrehozni mindazok számára, akik szeretnének Taggá válni, és szeretnék a Platformon keresztül kínált bizonyos szolgáltatásokat igénybe venni;

„**Facebook-fiók**”: a lenti 3.2. szakaszban meghatározott jelentéssel bír;

„**Autós**”: az a Tag, aki a Platform segítségével egy Utazás keretében személyszállítási szolgáltatást kínál más személyek számára a Költség-hozzájárulás ellenében a kizárólag az Autós által meghatározott időpontban;

„**Foglalás-visszaigazolás**”: a lenti 4.2.1. szakaszban meghatározott jelentéssel bír;

„**Tagi Tartalom**”: a lenti 11.2. szakaszban meghatározott jelentéssel bír;

„**Tag**”: minden olyan természetes személy, aki a Platformon keresztül létrehozott egy Fiókot;

„**Utas**”: olyan Tag, aki elfogadta az Autós személyszállítási szolgáltatásra tett ajánlatát, vagy adott esetben olyan személy, akinek a nevében és megbízásából egy Tag lefoglalt egy Ülést;

„**Költség-hozzájárulás**”: egy adott Utazás vonatkozásában az Autós által kért, és az Utas által elfogadott azon összeg, amelynek a segítségével az Utas hozzájárul az utazási költségekhez;

„**Ülés**”: az Utas által a Autós járművében lefoglalt ülés;

„**Platform**”: a fenti 1. szakaszban meghatározott jelentéssel bír;

„**Foglalás**”: a lenti 4.2.1. szakaszban meghatározott jelentéssel bír;

„**Szolgáltatások**”: a BlaBlaCar által a Platformon keresztül kínált összes szolgáltatás;

„**Weboldal**”: a(z) [www.BlaBlaCar.hu](https://www.blablacar.hu/) címen elérhető weboldal;

„**Útszakasz**”: a lenti 4.1. szakaszban meghatározott jelentéssel bír;

„**Utazás**”: az Autós által a Platformon közzétett Hirdetéshez kapcsolódó olyan utazás, amelyre az Autós vállalta, hogy a Költség-hozzájárulás ellenében elviszi magával az Utasokat;

„**Szolgáltatási Díjak”** az alábbi 5.2 pontban meghatározott jelentéssel bír(nak).

“**Buszhirdetés**“: az autóbusz-üzemeltető által a Platformon közzétett, autóbuszos utazásra vonatkozó hirdetés;

“**Autóbusz-üzemeltető**“: olyan vállalkozás, amely hivatásszerűen szállít utasokat, és amelynek utazására szóló jegyeket a BlaBlaCar a Platformon terjeszti;

**3\. A Platformon történő regisztráció, valamint Fiók létrehozása**
--------------------------------------------------------------------

### **3.1. A Platformon történő regisztráció feltételei**

A Platformot kizárólag a 18. életévüket betöltött természetes személyek használhatják. A Platformra szigorúan tilos kiskorúaknak regisztrálni. A Platform elérésével, használatával, és/vagy a Platformon történő regisztrációval kijelented és szavatolod, hogy a 18. életévedet betöltötted.

### **3.2. Fiók létrehozása**

A Platform lehetővé teszi a Tagok számára, hogy Hirdetéseket tehessenek közzé és tekinthessenek meg, valamint hogy egymással kapcsolatba léphessenek egy-egy Ülés lefoglalásával kapcsolatban. A közzétett Hirdetéseket a Platformra nem regisztrált személyek is megtekinthetik. Azonban egy Hirdetés közzétételének, valamint egy Ülés lefoglalásának az előfeltétele a Fiók létrehozása, és ezáltal a Taggá válás.

BlaBlaCar Fiókot a következőképpen hozhatsz létre:

(i) a regisztrációs űrlapon szereplő összes kötelező mező kitöltésével;

(ii) vagy a Platformon keresztül a Facebook-fiókodba történő bejelentkezéssel (a továbbiakban: „**Facebook-fiók**”). Ezen funkció használatával tudomásul veszed, hogy a BlaBlaCar hozzáférhet a Facebook-fiókodból származó bizonyos információkhoz, valamint azokat a Platformon közzéteheti, és saját magának megőrizheti. A BlaBlaCar Fiókod és a Facebook-fiókod közötti összerendelést bármikor törölheted a profilod „Ellenőrzés” című szakaszában. Ha szeretnél bővebb információkat kapni arról, hogy a Facebook-fiókodból származó adatokat miként használjuk fel, kérjük, olvasd el az [Adatkezelési Tájékoztatónkat](https://www.blablacar.hu/about-us/privacy-policy), valamint a Facebook adatvédelmi szabályzatát.

A Platformra csak abban az esetben regisztrálhatsz, ha előtte elolvastad és elfogadtad a jelen Felhasználási Feltételeket.

Vállalod, hogy a Fiók létrehozása során (függetlenül a létrehozás módjától) pontos és valós adatokat szolgáltatsz magadról, valamint hogy a Fiók létrehozása során szolgáltatott adatokat naprakészen tartod a profilodon keresztül vagy a BlaBlaCar felé küldött értesítés segítségével, így garantálva a szolgáltatott adatok pontosságát és aktualitását a közted és a BlaBlaCar között létrejött szerződéses jogviszony teljes fennállása során.

E-mail címmel történő regisztráció esetén vállalod, hogy a Fiók létrehozása során megadott jelszót titkosan kezeled, és azt nem hozod mások tudomására. Vállalod, hogy a jelszó esetleges elveszítése vagy mások tudomására hozatala esetén azonnal értesíted a BlaBlaCar-t erről. Kizárólagos felelősséget vállalsz a Fiókod harmadik felek általi esetleges használatából eredő következményekért, kivéve, ha kifejezetten értesítetted a BlaBlaCar-t a jelszavad (i) elveszítéséről; (ii) harmadik fél általi illetéktelen felhasználásáról; vagy (iii) harmadik fél tudomására hozataláról.

Vállalod, hogy a kezdetben létrehozott Fiókodon kívül semmilyen más Fiókot nem hozol létre és/vagy nem használsz sem a saját nevedben, sem bármely harmadik fél nevében.

### **3.3. Ellenőrzés**

A BlaBlaCar az átláthatóság biztosítása, a bizalom erősítése, valamint az esetleges csalási kísérletek megelőzése és/vagy észlelése érdekében jogosult egy olyan rendszert létrehozni, amelynek a segítségével ellenőrizheti a profilodban meghatározott bizonyos információkat. Ez különösen igaz arra az esetre, ha megadod a telefonszámodat, vagy a rendelkezésünkre bocsátod a személyazonosításra alkalmas valamely igazolványodnak a másolatát.

Tudomásul veszed és elfogadod, hogy a Platformon és/vagy a Szolgáltatásokban található, az „ellenőrzött” információk (ideértve az „Igazolt Profilra”) vagy ahhoz hasonló egyéb kifejezésre vonatkozó hivatkozás kizárólag annyit jelent, hogy az adott Tag sikeresen keresztülment a – Platformon és/vagy a Szolgáltatásokban üzemeltetett – ellenőrzési eljáráson annak érdekében, hogy a rendszer bővebb információkat nyújthasson neked arról a Tagról, akivel közös telekocsi utat tervezel. A BlaBlaCar mindazonáltal nem garantálhatja az ellenőrzési eljárás során ellenőrzött adatok valódiságát, megbízhatóságát és/vagy érvényességét.

**4\. A Szolgáltatások használata**
-----------------------------------

### **4.1. Hirdetések közzététele**

Ha Taggá váltál, és az összes alábbi feltételt sikeresen teljesíted, akkor jogosult vagy Hirdetéseket létrehozni és közzétenni a Platformon oly módon, hogy beírod a tervezett Utazásoddal kapcsolatos információkat (például az indulási és az érkezési dátumot és időpontot, a beszállási pontokat, a kínált Ülések számát, az elérhető opciókat, a Költség-hozzájárulás összegét stb.).

A közzétenni kívánt Hirdetés beállítása során feltüntetheted, hogy mely köztes városokban tervezel megállni, valamint hogy az Utasok mely köztes városokban szállhatnak be és/vagy ki. Az Utazásnak a köztes városok közötti szakaszait, valamint egy köztes város és a beszállási pont vagy célállomás közötti szakaszait a jelen dokumentum vonatkozásában „Útszakaszok”-nak hívjuk.

Kizárólag a következő feltételek maradéktalan teljesülése esetén vagy jogosult Hirdetést közzétenni:

(i) érvényes gépjármű-vezetői engedéllyel rendelkezel;

(ii) kizárólag olyan járművekre vonatkozóan teszel közzé Hirdetést vagy Hirdetéseket, (i) amelyeknek te vagy a tulajdonosa, valamint amelyek használatára kifejezett engedéllyel rendelkezel; és (ii) amelyeket jogosult vagy telekocsi célokra használni;

(iii) a Hirdetéshez vagy Hirdetésekhez kapcsolódó járművet elsősorban te vezeted;

(iv) az adott jármű érvényes kötelező gépjármű-felelősségbiztosítással rendelkezik;

(v) orvosilag vagy más módon nem tiltottak el a gépjárművezetéstől, illetve arra vonatkozóan ellenjavallatot nem kaptál;

(vi) az Utazáshoz használni kívánt jármű olyan utazó célú személygépjármű, amely 4 kerékkel és legfeljebb 7 üléssel rendelkezik;

(vii) nem szándékozol a Platformon ugyanarra az Utazásra vonatkozóan egy másik Hirdetést is közzétenni;

(viii) nem kínálsz több Ülést annál, mint ahány üléssel a járműved rendelkezik;

(ix) minden felkínált Üléshez kapcsolódik biztonsági öv, még olyan esetben is, ha a jármű biztonsági öv nélküli üléssel vagy ülésekkel is megkapta a forgalomba helyezési engedélyt;

(x) műszakilag jó állapotú járművet használsz, amely megfelel a vonatkozó jogszabályi rendelkezéseknek és előírásoknak, különösen ideértve az érvényes műszaki vizsgát és zöldkártyát (környezetvédelmi engedélyt)

(xi) fogyasztó vagy, és nem minősülsz hivatásos, szakmai célú felhasználónak.

Tudomásul veszed, hogy a Platformon általad közzétett Hirdetések tartalmáért kizárólag te vagy felelős. Ebből kifolyólag kijelented és szavatolod, hogy az általad közzétett Hirdetésben szereplő összes információ pontos és valós, valamint vállalod, hogy az Utazást az általad közzétett Hirdetésben szereplő feltételekkel teljesíted.

A Hirdetésedet közzétesszük, így az látható lesz nemcsak a Tagok számára, hanem mindazon személyek számára is, akik a Platformon vagy a BlaBlaCar partnereinek a weboldalán Utazást keresnek. A BlaBlaCar a jogosult a saját kizárólagos hatáskörében, megtagadni egy közzétenni kívánt Hirdetés közzétételét és/vagy egy korábban közzétett Hirdetést bármikor eltávolítani a rendszerből, ha az adott Hirdetés nem felel meg a jelen Felhasználási Feltételekben foglalt előírásoknak, vagy ha a BlaBlaCar úgy véli, hogy az adott Hirdetés sérti a BlaBlaCar, a Platform és/vagy a Szolgáltatások jó hírnevét, és/vagy törölni az ilyen Hirdetést közzétevő Tag Fiókját a jelen Felhasználási Feltételek 9. pontjában foglaltak szerint.

Tájékoztatunk, hogy abban az esetben, ha a Platform használatakor hivatásos, szakmai célú felhsználóként jársz el, miközben egyszerű fogyasztóként tünteted fel magad, a vonatkozó jogszabályok által előírt szankcióknak leszel kitéve.

Tudomásul veszed és elfogadod, hogy a BlaBlaCar jogosult a saját kizárólagos hatáskörében meghatározni az általad közzétenni kívánt Hirdetés besorolására és a többi Hirdetés között történő megjelenítési sorrendjére vonatkozóan figyelembe vett követelményeket.

### **4.2. Ülés foglalása**

**4.2.1.Telekocsi utazás**

A BlaBlaCar létrehozott egy rendszert az ülőhelyek online foglalására („Foglalás”) a Platformon kínált utazásokhoz.

A BlaBlaCar a Platformon a felhasználók számára különböző keresési kritériumok (származási hely, célállomás, időpontok, utazók száma stb.) alapján keresőmotort biztosít. A keresőmotor bizonyos további funkciókat biztosít, ha a felhasználó a Fiókján keresztül csatlakozik. A BlaBlaCar felkéri a felhasználót, hogy az alkalmazott foglalási eljárástól függetlenül, gondosan tekintse át és használja a keresőmotort, hogy meghatározza az igényeinek leginkább megfelelő ajánlatot. További információk itt érhetők [el](https://blog.blablacar.hu/about-us/a-platformok-atlathatosaga). 

Ha egy Utas egy Hirdetés után érdeklődik, akkor az Utas adott esetben online is elküldheti a Foglalási kérelmét. A Foglalási kérelem kétféleképpen hagyható jóvá: (i) automatikusan, ha az Autós a közzétett Hirdetés beállítása során ezt a lehetőséget választotta; vagy (ii) kézileg, az Autós által. A foglaláskor az Utas online fizeti meg a Szolgáltatási Díjakat, amennyiben szükséges. A Foglalási kérelem Autós általi jóváhagyásáról az Utas adott esetben foglalás-visszaigazolást (a továbbiakban: „**Foglalás-visszaigazolás**”) kap.

Ha Autósként a Foglalási kérelmek kézi jóváhagyásának vagy elutasításának a lehetőségét választottad a közzétett Hirdetés beállítása során, akkor köteles vagy az Utasok által küldött Foglalási kérelmekre az adott Utasok által a Foglalási kérelmeik beállítása során meghatározott határidőn belül válaszolni. Ellenkező esetben a Foglalási kérelem automatikusan elavul és az Utasnak vissza kell téríteni a foglalási kérelem benyújtásakor befizetett összegeket, ha voltak ilyenek.

A BlaBlaCar a Foglalás-visszaigazolás részeként elküldi neked az Autós telefonszámát (amennyiben Utas vagy), illetve az Utas telefonszámát (amennyiben Autós vagy), ha a Tag beleegyezik a telefonszámának feltüntetésébe. Ezt követően kizárólagos felelősséggel tartozol azért, hogy a másik Taggal létrejött kötelező érvényű megállapodásában foglaltaknak eleget tegyél.

**4.2.2 Buszos út**

Buszos utak esetén a BlaBlaCar lehetővé teszi adott Buszos útra vonatkozó Buszjegyek Foglalását a Platformon keresztül.

A Szállítási Szolgáltatásokra, a Felhasználó által kiválasztott Utat kínáló Autóbusz üzemeletető Általános Értékesítési Feltételei vonatkoznak, melyet a Felhasználónak a rendelés megerősítése előtt el kell fogadnia. A BlaBlaCar nem kínál Buszos utakra vonaktozó szállítási szolgáltatásokat, ami azt jelenti, hogy az Általános Értékesítési Feltételek tekintetében kizárólag az Autóbusz üzemeltetők szerepelnek szerződéses félként. Elismerekd, hogy egy adott Buszos útra történő Ülésfoglalás feltétele az érintett Autóbusz üzemeltető az Általános Értékesítési Feltételeinek elfogadása.

A BlaBlaCar felhívja a figyelmet arra, hogy a Busz üzemeltető az  általa a Weboldalon közzétett egyes Szállítási Szolgáltatásokat visszavonhatja, különösen időjárási okok, szezonális változások vagy vis major esteétn.

**4.2.3. Az Ülések név szerinti Foglalása, valamint a Szolgáltatások harmadik felek nevében és megbízásában történő használatára vonatkozó feltételek**

A Szolgáltatások Utas vagy Autós minőségben történő igénybe vétele névhez kötött. Az Autós és az Utas kizárólag azok a személyek lehetnek, akiknek a személyazonosságát a BlaBlaCar és az Utazásban részt vevő többi Tag felé kommunikálták.

Ugyanakkor a BlaBlaCar lehetővé teszi a Tagjai számára, hogy harmadik felek nevében és megbízásából is foglaljanak egy vagy több Ülést. Ilyen esetben vállalod, hogy az Autós felé pontosan jelzed a Foglalás leadásakor annak a személynek a keresztnevét, életkorát és telefonszámát, akinek a nevében és megbízásából az adott Ülést szeretnéd lefoglalni. Szigorúan tilos 13 év alatti kiskorú személy részére Ülést foglalni, ha vele nem utazik kísérő. Ha 13. életévét betöltött, egyedül utazó kiskorú személy számára szeretnél Ülést foglalni, akkor válllalod, hogy ezzel kapcsolatban beszerzed az Autós előzetes hozzájárulását, valamint hogy az Autós rendelkezésére bocsátod a hiánytalanul kitöltött és az adott személy törvényes képviselője vagy törvényes képviselői által aláírt meghatalmazást.

A Platformon keresztül kizárólag természetes személyek számára foglalhatók Ülések. Tilos Ülést foglalni tárgyak, csomagok, kísérő nélkül utazó állatok és/vagy anyagi javak szállítása céljából.

Továbbá tilos más Autós nevében és megbízásából Hirdetést közzétenni.

### **4.3. Tagi tartalom, moderálás és értékelési rendszer**

**4.3.1. Az értékelési rendszer működése**

A BlaBlaCar arra biztat, hogy írjál értékelést arról az Autósról (ha Utas vagy), illetve arról az Utasról (ha Autós vagy), akivel egy Utazás keretében együtt utaztál vagy együtt terveztél utazni (vagyis akivel együtt foglaltál Utazást). Ugyanakkor nem vagy jogosult egy másik Utasról értékelést készíteni, ha te is Utasként vettél részt az adott Utazáson, illetve olyan Tagról, akivel egy Utazás keretében nem utazotál együtt vagy nem terveztél együtt utazni. Lehetőséged van arra, hogy az Utazást követő 14 napon belül értékelést hagyj.

Az általad írt értékelés, valamint egy másik Tag által rólad készített értékelés a következő időpontok közül a korábban bekövetkező időpontban jelenik meg a Platformon: (i) közvetlenül azt követően, hogy mind a ketten elkészítettétek az értékeléseteket; vagy (ii) az elsőként elkészült értékelés időpontját követő 14 nap elteltével.

Ha egy másik Tag értékelést ír rólad, akkor az értékelésnek a profilodon történő fogadásától számított 14 nap áll a rendelkezésedre, hogy válaszolj az értékelésre. A kapott értékelés és adott esetben az adott értékelésre küldött válaszod megjelenik a profilodban.

**4.3.2. Moderálás**

**a. Tagi tartalom**

Tudomásul veszed és elfogadod, hogy a BlaBlaCar a 11.2. pontban meghatározott Tagi tartalmat annak közzététele előtt moderálhatja akár automatizált eszközök révén, akár manuálisan. Amennyiben a BlaBlaCar véleménye szerint az ilyen Tagi tartalom sérti a vonatkozó jogszabályokat vagy a jelen Felhasználási Feltételeket, fenntartja a jogot arra, hogy:

* megakadályozza az ilyen tagi tartalom közzétételét vagy törölje azt;
* figyelmeztetést küldjön az adott Tagnak, emlékeztetve őt arra, hogy kötelessége betartani a vonatkozó jogszabályokat és a jelen Felhasználási Feltételeket; és/vagy
* foganatosítsa a jelen Felhasználási Feltételek 9. pontja szerinti korlátozó intézkedéseket.

A BlaBlaCar automatizált eszközök révén vagy manuálisan végzett moderálása nem tekintendő annak vállalásaként vagy azon kötelezettségként, hogy mindenkor monitorozza a Platformon közzétett Tagi tartalmakat vagy aktívan pásztázza a Platformot illegális tevékenységek után kutatva, és a vonatkozón jogszabályok által megengedett mértékig nem keletkezik ezzel kapcsolatos felelőssége.

**b. Üzenetek**

A Tagok Platformon keresztüli üzenetváltása („Üzenetek”) kizárólag az Utazásokkal kapcsolatos információk cseréjét szolgálhatja.

A BlaBlaCar automatizált szoftverek vagy algoritmusok révén kiolvashatja az Üzenetek tartalmát a csalás megelőzése, a szolgáltatások javítása, ügyfélszolgálati célok és a Tagokkal között szerződések (például a jelen Felhasználási Feltételek) érvényesítése érdekében. Ha egy Üzenetben csalárd vagy törvénytelen magatartásra, illetőleg a Platform megkerülésére utaló jeleket talál, vagy az egyéb módon ellent mond a jelen Felhasználási Feltételeknek:

* előfordulhat, hogy az ilyen tartalom nem kerül közzétételre;
* az adott Üzenetet küldő Tag figyelmeztetést kaphat, melyben emlékeztetjük arra, hogy kötelessége betartani a vonatkozó jogszabályokat és a jelen Felhasználási Feltételeket; vagy
* a Tag Fiókja a jelen Felhasználási Feltételek 9. pontja szerint felfüggesztésre kerülhet.

Az ilyen automatizált szoftverek BlaBlaCar általi használata nem tekintendő annak vállalásaként vagy azon kötelezettségként, hogy mindenkor monitorozza a Platformon közzétett tartalmakat és/vagy aktívan pásztázza a Platformot illegális tevékenységek után kutatva, és a vonatkozón jogszabályok által megengedett mértékig nem keletkezik ezzel kapcsolatos felelőssége.

**4.3.3. Korlátozás**

A jelen Felhasználási Feltételek 9. pontja értelmében a BlaBlaCar fenntartja a jogot arra, hogy a Fiókodat felfüggessze, és a Szolgáltatásokhoz történő hozzáférésedet korlátozza, vagy hogy a jelen Felhasználási Feltételeket megszüntesse, amennyiben (i) legalább három darab értékelést kaptál; és (ii) az általad kapott egyetlen értékelés vagy az értékelések átlagértéke 3 vagy annál kevesebb – attól függően, hogy az értékelés keretében egy másik Tag milyen súlyos dolgot fogalmazott meg.

**5\. Pénzügyi feltételek**
---------------------------

A Platformhoz való hozzáférés és a platformon való regisztráció, valamint a keresés, a hirdetések megtekintése és feladása ingyenes. A Foglalás azonban az alábbiakban ismertetett feltételek szerint díjköteles.

### **5.1. Költség-hozzájárulás**

A Költség-hozzájárulás összegének a meghatározása kizárólag az Autós felelőssége. Szigorúan tilos nyereséget realizálni a Platform használatából. Ebből kifolyólag vállalod, hogy az Utasoktól összesítve nem kérsz több Költség-hozzájárulást annál, mint amennyi pénzbe valójában és ténylegesen kerül neked az Utazás teljesítése. Ellenkező esetben kizárólagosan köteles vagy vállalni annak a kockázatát, hogy a Platformon keresztül végrehajtott tranzakciót utólag esetlegesen átminősítik.

A BlaBlaCar azt javasolja, hogy egy-egy Hirdetés beállítása során a tervezett Utazás jellegének és a megtenni tervezett távolságnak a figyelembe vételével határozd meg a Költség-hozzájárulás összegét. Ez az összeg azonban csak irányadó összeg; saját belátásod szerint dönthetsz arról, hogy csökkented-e vagy növeled-e ezt az összeget annak függvényében, hogy valójában és ténylegesen neked mennyi pénzedbe kerül az Utazás teljesítése. A BlaBlaCar a visszaélések megakadályozása érdekében korlátozza a Költséghozzájárulás-módosítási lehetőségeknek a számát.

### **5.2. Szolgáltatási díjak**

A BlaBlaCar a Platform használatáért cserébe, a foglalás időpontjában szolgáltatási díjakat (a továbbiakban: „**Szolgáltatási díjak**”) szed. A felhasználót a Szolgáltatási Díjak alkalmazása előtt tájékoztatjuk, amennyiben szükséges. 

A hatályos Szolgáltatási Díjak számítási módjait a BlaBlaCar külön is közzéteheti, kizárólag tájékoztató jellegűek, és nem rendelkeznek szerződéses értékkel. A Szolgáltatási Díjak kiszámítása különböző tényezők, különösen az Utazás hossza és ára alapján történhet. A BlaBlaCar fenntartja a jogot, hogy bármikor módosítsa a Szolgáltatási Díjak számítási módszereit. Ezek a módosítások nem érintik a felhasználó által a módosítások hatályba lépése előtt elfogadott Szolgáltatási Díjakat.

Kérjük, vedd figyelembe, hogy a határokon átnyúló utazások esetében a Szolgáltatási Díjak összegének és az alkalmazandó ÁFA-nak a kiszámítási módszerei az utazás beszállási pontjától és/vagy célállomásától függően változnak.

A Platform határokon átnyúló utazásokhoz vagy Magyarországon kívüli utazásokhoz történő használatakor a Szolgáltatási Díjakat a BlaBlaCar helyi platformot üzemeltető leányvállalata számíthatja fel.

### **5.3. Kerekítés**

Tudomásul veszed és elfogadod, hogy a BlaBlaCar a saját kizárólagos hatáskörében lefelé vagy felfelé kerekítheti a Költség-hozzájárulásként meghatározott összeget vagy a Szolgáltatási Díjakat.

### **5.4. A Költség-hozzájárulás kifizetése az Autós felé**

Az Utas vállalja, hogy a Költség-hozzájárulás összegét legkésőbb a kiszállási ponton kifizeti az Autós felé.

Az Autós vállalja, hogy az Utazás megkezdése előtt nem kéri kifizetni a Költség-hozzájárulást sem részben, sem egészben.

**6\. A Szolgáltatások és a Platform nem kereskedelmi és/vagy nem üzleti jellege**
----------------------------------------------------------------------------------

Elfogadod, hogy a Szolgáltatásokat és a Platformot kizárólag nem kereskedelmi és/vagy nem üzleti jellegű céllal veszed igénybe, azért hogy kapcsolatba léphess olyan személyekkel, akik szeretnének egy Utazás keretében együtt telekocsizni veled.

A telekocsi keretében elismerted, hogy az uniós fogyasztóvédelmi jogból eredő fogyasztói jogok nem vonatkoznak a többi taggal való kapcsolatodra.

Autósként vállalod, hogy nem kérsz olyan összegű Költség-hozzájárulást, amely meghaladná részedről az adott Utazással kapcsolatban ténylegesen felmerült költségeket, és amely számodra nyereséget realizálna, figyelembe véve azt is, hogy a költségmegosztás vonatkozásában Autósként köteles vagy az Utazással kapcsolatban felmerülő költségekből rád eső részt kifizetni. Kizárólagos felelősséggel tartozol az adott Utazással kapcsolatban felmerülő költségek kiszámításáért, valamint annak ellenőrzéséért, hogy az Utasoktól általad kért Költség-hozzájárulások összege nem haladja meg részedről az adott Utazással kapcsolatban ténylegesen felmerülő költségeknek a te saját részeddel csökkentett összegét.

A BlaBlaCar fenntartja a jogot arra, hogy felfüggessze a Fiókodat olyan esetben, ha sofőr által vezetett járművet, taxiként funkcionáló járművet, céges autót vagy egyéb vállalati és/vagy kereskedelmi célú járművet használsz, és ezáltal nyereséget realizálsz a Platform használatából. Vállalod, hogy a BlaBlaCar első felszólítására bemutatod a BlaBlaCar részére a járműved forgalmi engedélyét és/vagy egyéb olyan dokumentumát, amely igazolja, hogy jogosult vagy az adott járművet a Platformon használni, és hogy az adott jármű Platformon történő használatából nem realizálsz pénzügyi nyereséget.

A jelen Felhasználási Feltételek 9. pontja értelmében a BlaBlaCar fenntartja a jogot arra is, hogy felfüggessze Fiókodat, korlátozza a Szolgáltatásokhoz történő hozzáférésedet, vagy hogy megszüntesse a jelen Felhasználási Feltételeket: ha olyan tevékenységet végezel a Platformon, amely a kínált Utazások jellege és gyakorisága, a szállított Utasok száma és/vagy a kért Költség-hozzájárulások összeg alapján arra enged következtetni, hogy nyereséget realizálsz ezen tevékenységből; vagy bármely olyan okból, amely azt sugallja a BlaBlaCar felé, hogy nyereséget realizálsz a Platform használatából.

**7\. Visszamondás**
--------------------

### **7.1. A visszatérítés feltételei lemondás esetén**

A telekocsi utazáson való helynek az Autós vagy az Utas által a foglalás visszaigazolását követően történő lemondására az alábbi rendelkezések vonatkoznak: 

– Az Autósnak köszönhető lemondás esetén az Utasnak visszatérítik a Szolgáltatási díjat. Ez az eset különösen akkor áll fenn, ha az Autós:

* nem erősítette meg a foglalási kérelmet a megadott határidőn belül (ha az Autós ilyen lehetőséget választott);
* a telekocsi-utazást, vagy nem érkezik meg a találkozási pontra 15 perccel a megbeszélt időpont után;

–Az Utasnak köszönhető lemondás esetén a szolgáltatási díjakat a BlaBlaCar visszatartja.

A Szolgáltatási Díj visszatérítésének módja a Szolgáltatási Díj befizetésére használt módtól függően kerül meghatározásra, míg a Szolgáltatási Díj visszatérítésének határideje a banki szervezetek általi átutalások időzítésétől függ.

Ha az Utas az Utazás megkezdése előtt visszamondja a lefoglalt Ülést vagy Üléseket, akkor ezek az Ülések automatikusan felszabadulnak, így azokat más Utasok online lefoglalhatják, és így a jelen Felhasználási Feltételekben foglaltak az új Utasokra is érvényesek lesznek.

A BlaBlaCar saját belátása szerint, a rendelkezésre álló információk alapján nagyra értékeli a visszatérítési kérelmek jogosságát.

### **7.2. Visszalépési jog**

A jelen Felhasználási Feltételek elfogadásával kifejezetten elfogadod, hogy a Te és a BlaBlaCar közötti, egy másik Taggal való kapcsolatból álló szerződés a foglalási visszaigazolás óta eltelt elállási határidő lejárta előtt teljesül, így kifejezetten lemondasz az elállási jogodról, a  45/2014. (II. 26.) Korm. rendelet a fogyasztó és a vállalkozás közötti szerződések részletes szabályairól 9.20.(2). cikkének rendelkezéseivel összhangban.

**8\. A Platform felhasználóinak és a Tagoknak a viselkedése**
--------------------------------------------------------------

### **8.1. A Platform összes felhasználójára vonatkozó kötelezettségek**

Tudomásul veszed, hogy kizárólagos felelősséggel tartozol a Platform használatára vonatkozó összes törvény, jogszabály és/vagy kötelezettség betartásáért és/vagy teljesítéséért.

Továbbá a Platform használata során, valamint az Utazások során vállalod, hogy:

(i) a Platformot nem használod semmilyen szakmai, kereskedelmi és/vagy nyereségszerzési célra;

(ii) a BlaBlaCar felé (különösen a Fiók létrehozása és/vagy esetleges aktualizálása során) vagy más Tagok felé nem küldesz semmilyen hamis, megtévesztő, rosszindulatú és/vagy csalárd információt;

(iii) nem beszélsz, nem viselkedsz és/vagy nem teszel közzé tartalmat (köztük Üzeneteket) a Platformon olyan módon, amely bármilyen formában (i) becsületsértő, becsmérlő, obszcén, pornográf, vulgáris, sértő, agresszív, kéretlen, erőszakos, fenyegető, zaklató, rasszista és/vagy idegengyűlölő jellegű; és/vagy (ii) szexuális utalást tartalmaz; és/vagy (iii) erőszakot, diszkriminációt (hátrányos megkülönböztetést) és/vagy gyűlöletet gerjeszt; és/vagy (iv) illegális szerek használatára és/vagy illegális szerekkel kapcsolatos tevékenység végzésére bátorít; és/vagy (v) általánosabb értelemben véve illegális, a vonatkozó jogszabályoknak, a jelen Felhasználási Feltételeknek vagy a Platform eredeti rendeltetési céljának ellentmond; és amely a BlaBlaCar vagy bármely harmadik fél bármely jogát sérti, vagy a közerkölcsbe ütközik;

(iv) nem sérted meg a BlaBlaCar semmilyen jogát (különösen ideértve a BlaBlaCar-t megillető szellemi tulajdonjogokat) és/vagy nem rontod a BlaBlaCar imázsát és/vagy hírnevét;

(v) a Platformon egynél több Fiókot nem hozol létre saját magad részére, illetve hogy harmadik fél vagy harmadik felek nevében egyetlen Fiókot sem hozol létre a Platformon;

(vi) nem próbálod megkerülni a Platformon kínált online Foglalási rendszert (különösen ideértve azt az esetet, hogy ha azért próbálod meg elküldeni a saját elérhetőségi adataidat egy másik Tag felé, hogy a Foglalást a Platformon kívül bonyolítsd le és így ne kelljen megfizetned a szolgáltatási díjakat);

(vii) nem lépsz kapcsolatba másik Taggal (különösen ideértve a Platformon keresztüli kapcsolatfelvételt) a telekocsi szolgáltatás feltételeinek egyeztetésétől eltérő célból;

(viii) nem fogadsz el és nem teljesítesz fizetést a Platformon kívül;

(ix) betartod a jelen Felhasználási Feltételekben, az Adatkezelési Tájékoztatóban foglaltakat és a Platform rád vonatkozó egyéb feltételeit.

### **8.2. Az Autósokra vonatkozó kötelezettségek**

Továbbá, ha Autósként veszed igénybe a Platformot, akkor vállalod és/vagy kijelented, hogy:

(i) tiszteletben tartasz minden, a vezetésre és a járműre vonatkozó törvényt, jogszabályt és kódexet, különösen ideértve az alábbiakat: gondoskodsz róla, hogy az Utazás ideje alatt rendelkezz felelősségbiztosítással és gépjármű-vezetői engedéllyel;

(ii) utánanézel, hogy biztosításod lefedi-e a telekocsi szolgáltatást, illetve harmadik félnek minősülő Utasaidra határátlépés esetén is a teljes Utazás időtartama alatt van-e biztosítási fedezet;

(iii) vezetés közben nem vállalsz semmilyen kockázatot és nem használsz olyan árucikket, amely elvonhatja a figyelmedet, illetve az éber és teljesen biztonságos vezetés kárára mehet;

(iv) csak a ténylegesen tervezett Utazásokra vonatkozó Hirdetéseket teszel közzé;

(v) az Utazást a Hirdetésben leírtak szerint teljesíted (különös tekintettel az autópálya használatára vonatkozóan), illetve az időpontokra és egyéb helyekre (különös tekintettel a találkozási helyekre és a kiszállási pontokra) vonatkozóan a többi Taggal történt megegyezés szerint jársz el;

(vi) a Hirdetésben megjelölt Ülések számánál több Utast nem szállítasz;

(vii) műszakilag jó állapotú járművet használsz, amely megfelel a vonatkozó jogszabályi rendelkezéseknek és előírásoknak, különösen ideértve az érvényes műszaki vizsgát és zöldkártyát (környezetvédelmi engedélyt);

(viii) hogy a BlaBlaCar vagy bármely Utas felszólítására bemutatod az gépjármű-vezetői engedélyedet, a járműved forgalmi engedélyét, a biztosításáról szóló igazolást, a zöldkártyát (környezetvédelmi engedélyt), és minden olyan dokumentumot, ami igazolja, hogy Autósként veheted igénybe a Platformot;

(ix) forgalmi akadály miatti késés vagy az Utazás időpontjának megváltozása esetén azonnal értesíted az Utasaidat;

(x) nemzetközi Utazás esetén magadnál tartod, illetve az Utas és a hatóságok számára felszólítás esetén elérhetővé teszed a személyazonosságodat és a határátlépésre jogosító dokumentumokat;

(xi) az Utasokra a megbeszélt találkozási helyen a megbeszélt időpontban, illetve azon túlmenően legalább 15 percig vársz;

(xii) csak olyan járműre vonatkozó Hirdetést teszel közzé, ami te tulajdonodban van, illetve amit jogosult vagy telekocsi célokra használni;

(xiii) biztosítod, hogy Utasaid felvehetik veled a kapcsolatot a profilodon regisztrált telefonszámon;

(xiv) nem realizálsz nyereséget a Platform használatából;

(xv) orvosilag vagy más módon nem tiltottak el a gépjárművezetéstől, illetve arra vonatkozóan ellenjavallatot nem kaptál;

(xvi) az Utazás során megfelelően és felelősségteljesen, a telekocsi szolgáltatás alapelveit betartva viselkedsz;

(xvii) nem utasítasz vissza senkitől Foglalást faji, etnikai, nemzetiségi, vallási okokból vagy szexuális irányultság, családi állapot, fogyatékosság, fizikai megjelenés, házassági állapot, terhesség miatt, illetve gazdasági helyzet, név, tartózkodási hely, egészségügyi állapot vagy politikai vélemény okán különösen kiszolgáltatott helyzet miatt;

(xviii) biztosítod, hogy Utasaid felvehetik veled a kapcsolatot a profilodon regisztrált telefonszámon, beleértve a találkozási helyet is.

### **8.3. Az Utasokra vonatkozó kötelezettségek**

**8.3.1 Telekocsi Útra vonatkozóan**

Ha Utasként veszed igénybe a Platformot, vállalod, hogy:

(i) a Telekocsi Utazás során megfelelő magatartást tanúsítasz annak érdekében, hogy ne zavard az Autóst a koncentrálásban vagy a vezetésben, illetve nem zavarod meg más Utasok csendjét és nyugalmát;

(ii) az Autós járművével tisztelettel bánsz és óvod tisztaságát;

(iii) forgalmi akadály miatti késés esetén azonnal értesíted az Autóst;

(iv) az Autósnak a megbeszélt Költség-hozzájárulási összeget fizeted ki;

(v) az Autósra a találkozási helyen a megbeszélt időpontban, illetve azon túlmenően legalább 15 percig vársz;

(vi) hogy a BlaBlaCar vagy bármely Autós felszólítására bemutatod a személyi igazolványodat vagy bármely, személyazonosságot igazoló dokumentumodat;

(vii) nem viszel a Telekocsi Utazásra olyan használati cikket, árucikket, anyagot vagy állatot, ami megzavarhatja az Autóst a vezetésben és a koncentrálásban, vagy aminek természete, birtoklása vagy szállítása ellentétes a hatályos jogszabályok rendelkezéseivel;

(viii) nemzetközi Telekocsi Utazás esetén magánál tartod, illetve az Autós és a hatóságok számára felszólítás esetén elérhetővé teszed a személyazonosságodat és a határátlépésre jogosító dokumentumokat;

(ix) annak biztosítása érdekében, hogy sofőrje telefonon felvehesse Önnel a kapcsolatot a profiljában regisztrált számon, beleértve a találkozási pontot is.

Abban az esetben, ha egy vagy több Ülést harmadik felek nevében foglaltál le a fenti 4.2.3 szakaszban meghatározottak szerint, akkor garantálod, hogy az érintett harmadik fél betartja a jelen szakasz előírásait, illetve általánosságban a jelen Felhasználási Feltételeket. A jelen Felhasználási Feltételek 9. pontja értelmében a BlaBlaCar fenntartja a jogot arra is, hogy felfüggessze a Fiókodat, korlátozza a Szolgáltatásokhoz történő hozzáférésedet vagy, hogy a jelen Felhasználási Feltételek 9. szakasza alapján   megszüntesse a jelen Felhasználási Feltételeket abban az esetben, ha a harmadik fél, akinek nevében a jelen Felhasználási Feltételek szerint Ülést foglaltál, megsérti az előírásokat.

**8.3.2 Buszos Utak**

Az Utas Buszos Utak tekintetében az Utas vállalja, hogy betartja az érintett Autóbusz üzemeltető Általános Értékesítési Feltételeiben foglaltakat.

**8.4. Helytelen vagy illegális tartalom jelentése (értesítési és intézkedési mechanizmus)**

A gyanús, helytelen vagy illegális Tagi tartalmat vagy Üzenetet az [itt](https://support.blablacar.com/s/article/Illeg%C3%A1lis-tartalom-jelent%C3%A9se-1729197120599?language=hu) leírtak szerint jelentheted.

A BlaBlaCar a jelen pontban foglaltak szerint kapott vagy az illetékes hatóságok részéről érkező szabályszerű értesítést követően haladéktalanul eltávolít minden törvénytelen Tagi tartalmat, ha:

* a Tagi tartalom egyértelműen törvénytelen vagy ellent mond a vonatkozó jogszabályoknak; vagy
* a BlaBlaCar az adott tartalmat a jelen Felhasználási Feltételeknek ellent mondónak ítéli.

Ilyen esetben a BlaBlaCar fenntartja a jogot arra, hogy eltávolítsa a szabályszerűen, megfelelő részletességgel és érthető módon jelentett Tagi tartalmat és/vagy azonnali hatállyal felfüggessze a jelentett Fiókot.

Az érintett Tag az alábbi 15.1. pontban foglaltak szerint fellebezhet a BlaBlaCar fentiek szerinti döntése ellen.

**9\. A Platform használatával kapcsolatos korlátozások, Fiókok felfüggesztése, hozzáférés korlátozása és megszüntetés**
------------------------------------------------------------------------------------------------------------------------

A közted és a BlaBlaCar között létrejött szerződéses jogviszonyt bármikor díjmentesen és minden ok nélkül megszüntetheted. Ehhez mindössze a Profiloldalon lévő „Fiók megszüntetése” fülre kell lépned.

Amennyiben (i) megsérted a jelen Felhasználási Feltételeket, beleértve, de nem kizárólagosan a fenti 6. és 8. szakaszban meghatározott, Tagként vállalt kötelezettségeidet (ii) megszeged a fenti 4.3.3 szakaszban meghatározott korlátozást vagy (iii), ha a BlaBlaCar-nak valódi oka van azt feltételezni, hogy szükségessé vált saját, a Tagok, illetve harmadik felek biztonságának és tisztességének védelme, különböző vizsgálatokat vagy a csalás, megelőzése érdekében, akkor a BlaBlaCar fenntartja a jogot arra, hogy:

(i) azonnal és értesítés nélkül megszüntesse a közted és a BlaBlaCar között létrejött kötelező érvényű Felhasználási Feltételeket; és/vagy

(ii) megakadályozza, hogy közzétegyél vagy eltávolítsa az általad a Platformon közzétett bármely Tagi tartalmat; és/vagy

(iii) korlátozza hozzáférésedet és a Platform használatát; és/vagy

(iv) átmenetileg vagy végleg felfüggessze Fiókodat.

A Fiók felfüggesztése adott esetben azt is jelentheti, hogy nem kapod meg a függőben lévő kifizetéseket.

Esettől függően a BlaBlaCar figyelmeztetést küldhet neked, emlékeztetve arra, hogy kötelességed betartani a vonatkozó jogszabályokat és/vagy a jelen Felhasználási Feltételeket. 

Amennyiben ilyen jellegű intézkedésre van szükség, erről értesítést fogsz kapni annak érdekében, hogy lehetőséged legyen megerősíteni a jelen Felhasználási Feltételek és a vonatkozó jogszabályok betartása melletti elköteleződéseket, tisztázni magad a BlaBlaCar előtt vagy fellebezni a döntés ellen. A BlaBlaCar az egyes esetek körülményeit és a szabálysértés súlyosságát figyelembe véve saját kizárólagos hatáskörében dönt majd az intézkedések életbe léptetéséről.

**10\. Személyes adatok**
-------------------------

A Platform használatának vonatkozásában a BlaBlaCar az [Adatkezelési Tájékoztatóban](https://blog.blablacar.hu/about-us/privacy-policy)  foglaltak szerint össze fog gyűjteni és fel fog dolgozni bizonyos személyes adatokat.

**11\. Szellemi tulajdonjog**
-----------------------------

**11.1. A BlaBlaCar által közzétett tartalom**

A Tagok által rendelkezésre bocsátott tartalmakat tekintve minden szellemi tulajdonjog kizárólag a BlaBlaCar-t illeti meg a Szolgáltatás, a Platform, a tartalmak (különösen ideértve a szövegeket, képeket, kivitelezéseket, logókat, videókat, hangokat, adatokat, grafikákat), a működést biztosító szoftver és adatbázis vonatkozásában.

A BlaBlaCar nem kizárólagos, személyre szóló és nem átruházható jogot biztosít számodra a Platform és a Szolgáltatások személyes és magáncélú, nem kereskedelmi alapon, illetve a Platform és a Szolgáltatások céljaival összhangban történő használatára vonatkozóan.

A Platform, a Szolgáltatások és azok tartalmának egyéb jellegű használata és hasznosítása tilos a BlaBlaCar előzetes írásos engedélye nélkül. Különösen tilos:

(i) A Platform, a Szolgáltatások és a tartalom reprodukálása, módosítása, átdolgozása, terjesztése, nyilvános képviselete és népszerűsítése, kivéve, ha az a BlaBlaCar által kifejezetten jóváhagyásra kerül.

(ii) a Platform és a Szolgáltatások visszafejtése és ellentétes irányú tervezése a hatályos jogszabályszövegekben meghatározott kivételeket figyelembe véve;

(iii) a Platformadatok jelentős részének kigyűjtése vagy kigyűjtésének megkísérlése (különös tekintettel az adatbányász robotok vagy más hasonló adatgyűjtő eszközök használatára).

**11.2. A Platformon általad közzétett tartalom**

A Szolgáltatásnyújtás lehetővé tétele érdekében és a Platform céljával összhangban a BlaBlaCar egy nem kizárólagos felhasználási engedélyt biztosít számodra a Szolgáltatások igénybevételével (többek között a Foglalási kérelmekkel, Hirdetésekkel, hozzászólásokkal, bemutatkozásokkal, fotókkal, értékelésekkel és értékelésekre adott válaszokkal) összefüggésben általad rendelkezésre bocsájtott tartalom és adatok, valamint Üzenetek használatára vonatkozóan (a továbbiakban „**Tagi tartalom**”). Annak érdekében, hogy a BlaBlaCar a digitális hálózaton keresztül és bármely kommunikációs (különös tekintettel az internetes és mobil hálózati) protokoll előírásainak megfelelően terjeszkedjen, és a Platform tartalmát rendelkezésre bocsájthassa a nyilvánosság számára, felhatalmazod a BlaBlaCar-t, hogy az egész világon, illetve a közted és a BlaBlaCar között létrejött szerződéses jogviszony fennállása alatt reproduklája, feltüntesse, átdolgozza és lefordítsa a Tagi tartalmat az alábbiak szerint:

(i) Felhatalmazod a BlaBlaCar-t, hogy a Tagi tartalom minden részét reprodukálja bármely, akár ismert akár még nem ismert digitális adathordozón – különös tekintettel bármely kiszolgálóra, merevlemezre, memóriakártyára vagy más ezekkel egyenértékű adathordozóra – bármely, akár ismert vagy akár nem ismert formátumban és eljárással olyan mértékben, ami a Platform működtetése és a Szolgáltatásnyújtás tekintetében a tárolási, biztonsági mentési, továbbítási vagy letöltési műveletekhez szükséges;

(ii) Felhatalmazza a BlaBlaCar-t, hogy átdolgozza és lefordítsa a Tagi tartalmat , illetve reprodukálja ezeket az átdolgozásokat bármilyen jelenlegi vagy jövőbeni digitális adathordozón a fenti (i) pontban meghatározottak szerint Szolgáltatásnyújtás céljából, különös tekintettel a különböző nyelveken történő Szolgáltatásnyújtásra. Ez a jogosultság különösen vonatkozik a Tagi tartalom formátumának módosítási lehetőségére az erkölcsi jogaidnak tiszteletben tartása mellett, a Platform grafikai irányelvének betartása céljából és/vagy annak érdekében, hogy technikailag alkalmas legyen a Platformon történő közzétételre.

A Platformra feltöltött Tagi tartalomért és Üzenetekért te felelsz, és azokhoz fűződően minden jog téged illet meg.

**12\. A BlaBlaCar szerepköre**
-------------------------------

A Platform egy online hálózati felület, amin a Tagok telekocsi célokra tervezett Utazásokat bejelentő Hirdetéseket hozhatnak létre és tehetnek közzé. Ezeket a Hirdetéseket főként a többi Tag tekintheti meg, így tájékozódhat az Utazás feltételeiről, és adott esetben közvetlenül Ülést foglalhat a Platformon a Hirdetést közzétevő Tag érintett járműjében.

A Platform használatával és a jelen Felhasználási Feltételek elfogadásával tudomásul veszed, hogy a BlaBlaCar semmilyen módon nem vesz részt szerződéses félként közted és más Tagok között létrejött megállapodásokban az Utazásokkal kapcsolatos költségek megosztására irányuló szándékkal.

A BlaBlaCar nem befolyásolhatja a Tagok és a Platform felhasználóinak magatartását. A Hirdetésekben szereplő járműveknek nem tulajdonosa, azokat nem hasznosítja, nem szereli fel és nem kezeli, illetve nem kínál semmilyen Utazást a Platformon.

Tudomásul veszed és elfogadod, hogy a BlaBlaCar nem szabályozhatja a kínált Hirdetések, Ülések és Utazások érvényességét, hitelességét vagy jogszerűségét. Telekocsi közvetítőként a BlaBlaCar nem nyújt szállítási szolgáltatást, és nem jár el fuvarozóként; a BlaBlaCar szerepköre a Platformhoz való hozzáférés lehetővé tételére korlátozódik.

A Tagok (Autósok vagy Utasok) kizárólagos és teljes felelősségük tudatában járnak el.

Közvetítőként a BlaBlaCar nem vonható felelősségre az Utazások hatékony lebonyolítása miatt, különösen az alábbiak miatt:

(i) az Autós téves információkat közölt Hirdetésében vagy bármely más módon az Utazással és annak feltételeivel kapcsolatban;

(ii) a Tag törli vagy módosítja az Utazást;

(iii) az Utas nem fizeti ki a Költség-hozzájárulási összegét;

(iv) a Tagok Utazás közbeni, előtti vagy utáni magatartása miatt.

**13\. A Platform működése, elérhetősége és funkciói**
------------------------------------------------------

A BlaBlaCar lehetőség szerint köteles megtenni mindent azért, hogy a Platform napi 24 órában, a hét minden napján elérhető legyen. Mindemellett a Platformhoz való hozzáférés átmenetileg előzetes értesítés nélkül műszaki karbantartás, áttelepítés, frissítési műveletek vagy leállás, illetve a hálózat működésével kapcsolatos korlátozások miatt szünetelhet.

Ezenfelül a BlaBlaCar fenntartja a jogot, hogy saját kizárólagos hatáskörében átmenetileg vagy végleg, teljesen vagy részlegesen megváltoztassa, illetve felfüggessze a Platformhoz való hozzáférést vagy annak funkcióit.

**14\. A Felhasználási Feltételek módosítása**
----------------------------------------------

A jelen Felhasználási Feltételek és a jelen Felhasználási Feltételeknek hivatkozás útján a részét képező egyéb dokumentumok képezik közted és a BlaBlaCar között a Szolgáltatások igénybevétele tárgyában létrejött teljes megállapodást. Minden más dokumentum, különös tekintettel a Platformon megjelenő dokumentumokra (GYIK, stb.), csak tájékoztatási célt szolgál.

A BlaBlaCar módosíthatja jelen Felhasználási Feltételeket annak érdekében, hogy azt átdolgozza saját műszaki és kereskedelmi környezetéhez illően, illetve, hogy megfeleljen a hatályos jogszabályok rendelkezéseinek. A Felhasználási Feltételek bárminemű módosítása közzétételre kerül a Platformon a hatályba lépés dátumának megjelölésével. A módosításról a BlaBlaCar még a hatályba lépés előtt küld értesítést.

**15\. Alkalmazandó jog és jogviták**
-------------------------------------

Jelen Felhasználási Feltételek magyar nyelven íródtak és a magyar jog hatálya alá tartoznak.

**15.1. Belső panaszkezelési rendszer**

Lehetőséged van fellebbezni az alábbiakat érintő döntéseinkkel szemben:

* Tagi tartalom: ha például eltávolítottunk egy általad a Platform használata közben közzétett Tagi tartalmat, korlátoztuk annak láthatóságát vagy elutasítottuk az eltávolítás iránti kérelmet; vagy
* Fiókod: felfüggesztettük a platformhoz való hozzáférésedet.

Ez a lehetőség azonban csak akkor él, ha a döntést azért hoztuk meg, mert a Tagi tartalom illegálisnak vagy a jelen Felhasználási Feltételeknek ellent mondónak bizonyult. A fellebbezési eljárásról részletesebben [itt olvashatsz](https://support.blablacar.com/s/article/Hogyan-fellebbezhetsz-tartalom-elt%C3%A1vol%C3%ADt%C3%A1sa-vagy-fi%C3%B3kt%C3%B6rl%C3%A9s-ellen-1729197123031?language=hu).

**15.2. Bíróságon kívüli jogvitarendezés**

A Platformmal és a Szolgáltatásokkal kapcsolatos panaszaidat szükség esetén az Európai Bizottság által online módon biztosított vitarendezési felületen is jelezheted. A felületet [itt](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.chooseLanguage) érheted el. Az Európai Bizottság panaszodat az illetékes nemzeti ombudsmannak továbbítja. A közvetítésre vonatkozó szabályozások szerint minden közvetítésre irányuló kérelem benyújtása előtt köteles vagy írásban értesíteni a BlaBlaCar-t bármely vita kapcsán békés megoldás keresése érdekében.

Az országodban illetékes békéltető testülethez is nyújthatsz be kérelmet (a listát [itt](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show) találod).

**16\. Jogi közlemények**
-------------------------

A Platform üzemeltetője a Comuto SA vállalat (alaptőkéje: 155 880 999 euró; a Párizsi Cégjegyzékbe 491.904.546 számon van bejegyezve; közösségen belüli HÉA-szám: FR76491904546); székhelye: 84, avenue de la République, 75011 Párizs (Franciaország); képviselője: Nicolas Brusson vezérigazgató és weboldal-publikálási igazgató.

A Weboldalnak a Google Cloud szerverei biztosítanak helyet Hollandiában.

A Comuto SA a következő számon szerepel az utazásszervezők nyilvántartásában: IM075180037.

A pénzügyi garanciát a Groupama Assurance-Crédit & Caution nyújtja, 8-10 rue d’Astorg, 75008 Párizs – Franciaország.

A szakmai felelősségbiztosítást a következő társaság köti: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Párizs – Franciaország.

A Comuto SA be van jegyezve a biztosításközvetítői, banki és pénzügyi nyilvántartásba az (Orias) 15003890 nyilvántartási számon.

Kérdés esetén felveheti a kapcsolatot a Comuto SA vállalattal [ezen a kapcsolati űrlapon](https://support.blablacar.com/s/contactsupport?language=hu) keresztül.

**17\.** **Digital Services Act (digitális szolgáltatásokról szóló rendelet)**
------------------------------------------------------------------------------

**17.1. Az EU-ban nyújtott szolgáltatások átlagos havi aktív felhasználóira vonatkozó információk**

Az Európai Parlament és Tanács belső piacon nyújtott digitalis szolgáltatásokról szóló, 2000/31/EK irányelvet módosító rendelete (Digital Services Act, „DSA”) 24. cikkének (2) bekezdése értelmében az online platformok szolgáltatóinak közzé kell tenniük a Unióban nyújtott szolgáltatás havi átlagos aktív felhasználóira vonatkozó információkat, az elmúlt hat hónap átlagaként kiszámítva. Ez a követelmény annak megállapítására szolgál, hogy az online platform úgynevezett óriásplatform-e, amely – a DSA meghatározása szerint – havonta legalább 45 millió aktív felhasználóval rendelkezik az EU-ban.

A DSA (felhatalmazáson alapuló jogi aktus bevezetését megelőzően hatályos változatának) 3. cikkében és a (77) preambulumbekezdésben foglalt iránymutatással összhangban végzett számítások szerint 2024. január 31-ig, a 2023. augusztus és 2024. január közötti időszakban a BlaBlaCar aktív felhasználóinak havi átlagos száma 4,82 millió az EU-ban.

Ezen információ közzététele kizárólag a DSA követelményeinek való megfelelést szolgálja, arra egyéb célokból támaszkodni nem megengedett. A számadatot legalább hathavonta frissítjük. A számítás módja idővel fejlettebb lehet vagy módosulhat például a termékek megváltozása vagy új technológiák miatt.

**17.2. Kapcsolattartó a hatóságok számára**

A DSA 11. cikke értelmében amennyiben valamely EU-beli hatóság, az Európai Bizottság vagy a Digitális Szolgáltatások Európai Testületének képviselőjeként jársz el, a DSA-val kapcsolatos ügyekben a [\[email protected\]](https://blog.blablacar.hu/cdn-cgi/l/email-protection) e-mail-címen veheted fel velünk a kapcsolatot.

Ezen az e‑mail‑címen angol és francia nyelvű kommunikációra van lehetőség.

Felhívjuk figyelmedet, hogy ez az e-mail-cím nem tagokkal való kommunikációra használatos. A BlaBlaCar használatával kapcsolatos kérdésekkel Tagként a [kapcsolatfelvételi űrlapon](https://support.blablacar.com/s/contactsupport?language=hu) veheted fel velünk a kapcsolatot.

* * *