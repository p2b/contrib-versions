Conditions générales d'utilisation du site internet Tripadvisor et notifications
================================================================================

Conditions générales d'utilisation de Tripadvisor

Dernière mise à jour : **16 février 2024**

**Conditions générales d'utilisation**

[UTILISATION DES SERVICES](#_USE_OF_THE)

[PRODUITS SUPPLÉMENTAIRES](#_ADDITIONAL_PRODUCTS)

[ACTIVITÉS INTERDITES](#_PROHIBITED_ACTIVITIES)

[POLITIQUE DE CONFIDENTIALITÉ ET COMMUNICATIONS D'INFORMATIONS](#_PRIVACY_POLICY_AND)

[AVIS, COMMENTAIRES ET UTILISATION DES AUTRES ZONES INTERACTIVES ; OCTROI DE LICENCE](#_REVIEWS,_COMMENTS_AND)

* [Limitation des droits de licence de Tripadvisor](#_Restricting_Tripadvisor%E2%80%99s_Licence)

[RÉSERVATION AUPRÈS DE PRESTATAIRES TIERS PAR L'INTERMÉDIAIRE DE Tripadvisor](#_BOOKING_WITH_THIRD-PARTY)

* [Utilisation des services de réservation de Tripadvisor](#_Use_of_Tripadvisor)
* [Prestataires tiers](#_Third-Party_Suppliers._The)
* [Réservation de locations de vacances, de restaurants et d'expériences auprès de prestataires tiers répertoriés sur les sites Web de partenaires affiliés](#_Booking_Holiday_Rentals,)

[DESTINATIONS DE VOYAGE](#_TRAVEL_DESTINATIONS)

* [Voyage à l'international](#_International_Travel._When)

[LIMITATION DE RESPONSABILITÉ](#_LIABILITY_DISCLAIMER_1)

[INDEMNISATION](#_Indemnification)

[LIENS VERS DES SITES INTERNET TIERS](#_LINKS_TO_THIRD-PARTY)

[UTILISATION DE LOGICIELS EN TANT QU'ÉLÉMENTS DE SERVICE ; LICENCES SUPPLÉMENTAIRES POUR APPAREILS MOBILES](#_SOFTWARE_AS_PART)

[AVIS DE DROITS D'AUTEUR ET DE MARQUE DÉPOSÉE](#_COPYRIGHT_AND_TRADEMARK)

* [Politique de notification et de retrait en cas de contenu illicite](#_Notice_and_Take-Down)

[MODIFICATIONS APPORTÉES AU PRÉSENT CONTRAT ET AUX SERVICES ; MISES À JOUR ; RÉSILIATION ET RETRAIT](#_MODIFICATIONS_TO_THE)

[LOI APPLICABLE ET JURIDICTION COMPÉTENTE](#_JURISDICTION_AND_GOVERNING)

[CONVERTISSEUR DE DEVISES](#_CURRENCY_CONVERTER)

[DISPOSITIONS GÉNÉRALES](#_GENERAL_PROVISIONS)

[SERVICE D'ASSISTANCE](#_SERVICE_HELP)

Bienvenue sur les sites Web et les plateformes mobiles de Tripadvisor, accessibles à l'adresse https://fr.tripadvisor.be/ et sur les domaines nationaux de premier niveau (y compris les sous-domaines qui leur sont associés), les applications logicielles connexes (parfois appelées « applis »), les données, les SMS, les API, les e-mails, les discussions instantanées, les échanges téléphoniques, les boutons, les widgets et les publicités concernés (collectivement, tous ces éléments sont dénommés ci-après les « Services » ; de manière plus générale, les sites Web et les plateformes mobiles de Tripadvisor sont désignés ci-après par l'expression « sites Web »).

Les Services sont fournis par Tripadvisor LLC, une société à responsabilité limitée établie dans le Delaware, aux États-Unis (« **Tripadvisor** ») et dont le siège social est situé au \[Tripadvisor LLC, 400 1st Avenue, Needham, MA 02494, USA\]. Les termes « nous », « notre » et « nos » doivent être interprétés en conséquence.

En créant un compte Tripadvisor, vous acceptez d'être lié par les conditions générales et les notifications énoncées ci-dessous (collectivement, le « Contrat »). Le présent Contrat ne s'applique pas si vous naviguez simplement sur les sites Web ou utilisez les Services sans créer de compte Tripadvisor.  Veuillez lire attentivement le présent Contrat dans la mesure où il contient des informations concernant vos droits et les restrictions y afférentes, ainsi qu'une section concernant la législation applicable et le règlement des litiges. Si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni, vous avez légalement le droit de vous rétracter du présent Contrat dans les 14 jours suivant sa conclusion. Pour ce faire, ou pour résilier le présent Contrat à tout autre moment, clôturez votre compte (en nous contactant ou en accédant aux « Informations sur le compte » après connexion, puis en sélectionnant l'option « Fermer votre compte ») et renoncez à accéder aux Services ainsi qu'à leur utilisation.

Si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni, vous bénéficiez de certains droits obligatoires. Si vous êtes un consommateur résidant ailleurs, vous pouvez également bénéficier de droits en vertu des lois applicables de votre territoire. Aucune disposition du présent Contrat n'affecte vos droits de consommateur obligatoires.

Le terme « Contenu » désigne les informations, données, textes, liens, graphiques, contenus photo, audio ou vidéo, codes ou autres documents ou ensembles de documents que vous pouvez consulter, auxquels vous pouvez accéder ou avec lesquels vous pouvez interagir par l'intermédiaire des Services. Les « Services » tels qu'ils sont définis ci-dessus font référence aux services fournis par Tripadvisor ou ses partenaires affiliés (Tripadvisor et lesdites sociétés, lorsqu'il est fait référence à une ou plusieurs de ces entités, sont collectivement définies comme les « Sociétés Tripadvisor »). Afin que nul doute ne subsiste, les sites Web sont tous détenus et contrôlés par Tripadvisor.  Cependant, certains Services spécifiques mis à disposition par l'intermédiaire des sites Web peuvent être détenus et contrôlés par les partenaires affiliés de Tripadvisor, par exemple les Services facilitant [**la Réservation de locations de vacances, de restaurants et d**'**Expériences avec des prestataires tiers**](https://tripadvisor.mediaroom.com/FR-terms-of-use#OLE_LINK11) (voir ci-dessous). Dans le cadre de nos Services, nous pouvons vous écrire pour vous faire part d'offres spéciales, de produits ou de services susceptibles de vous intéresser et proposés par notre entreprise, nos sociétés affiliées ou nos partenaires. Nous vous enverrons généralement ces informations sous la forme de newsletters ou de communications marketing dans le but de mieux connaître vos goûts et préférences concernant l'ensemble de nos Services et ceux de nos sociétés affiliées. Celles-ci nous permettent d'adapter ces Services à vos préférences.

Le terme « vous » ou « utilisateur » désigne la personne, la société, l'organisation commerciale ou toute autre entité juridique qui a créé un compte Tripadvisor et utilise les Services et/ou y publie du Contenu. Le Contenu auquel vous contribuez, que vous soumettez, transmettez et/ou publiez sur ou par l'intermédiaire des Services peut être désigné de différentes manières telles que « votre Contenu » ou « le Contenu que vous soumettez ».

Les Services sont fournis de manière continue et exclusivement aux fins suivantes :

1. Aider les utilisateurs à rassembler des informations de voyage, à publier du Contenu, à rechercher et à réserver des services de voyage, à effectuer des réservations ; et
2. Aider les entreprises des secteurs du voyage, du tourisme et de l'hôtellerie à établir des relations avec les utilisateurs et les clients potentiels par le biais de services gratuits ou payants proposés par les sociétés Tripadvisor ou par leur entremise.

#### Les produits, les services, les données et le Contenu associés à nos Services sont régulièrement mis à jour (par exemple, avec les informations et les offres actuelles). Ainsi, dans le cas de services tiers, de nouveaux services de transport, d'hébergement, de restauration, de visites, d'activités ou d'expériences peuvent être disponibles à la réservation, tandis que d'autres peuvent ne plus l'être.

Nous sommes susceptibles de modifier d'une quelconque autre manière le présent Contrat conformément aux présentes Conditions. Vous comprenez et acceptez que la poursuite de votre accès aux Services et de leur utilisation après lesdites modifications signifie que vous vous engagez à respecter le Contrat modifié ou actualisé. Nous indiquerons la date des dernières modifications du présent Contrat au bas de celui-ci, lesquelles prendront effet dès leur publication. Nous vous informerons des changements importants apportés aux présentes conditions générales en vous envoyant une notification à l'adresse e-mail associée à votre profil ou en la publiant sur nos sites Web. Nous vous recommandons de consulter régulièrement cette page afin de prendre connaissance de la version la plus récente du présent Contrat. Si vous ne souhaitez pas accepter les modifications apportées au présent Contrat, vous pouvez clôturer votre compte et cesser d'accéder aux Services (les instructions pour ce faire sont décrites [ici](https://www.tripadvisorsupport.com/fr-FR/hc/traveler/articles/510)).

**UTILISATION DES SERVICES**

Comme condition préalable à votre utilisation des Services, vous reconnaissez et acceptez (i) de fournir des informations authentiques, exactes, actuelles et complètes aux Sociétés Tripadvisor par le biais des Services, (ii) de protéger les informations de votre compte, de le superviser et d'assumer l'entière responsabilité en cas d'utilisation par une tierce personne, (iii) d'être âgé d'au moins 13 ans. Les Sociétés Tripadvisor ne recueillent pas intentionnellement les informations personnelles de tout enfant âgé de moins de 13 ans. Si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni, nous pouvons vous refuser l'accès aux Services en cas de violation du présent Contrat, ou pour tout autre motif raisonnable. Si vous n'êtes pas un consommateur résident au sein de l'UE ou du Royaume-Uni, nous conservons le droit, à notre seule discrétion, de vous refuser l'accès aux Services, à tout moment et pour quelque raison que ce soit, y compris, mais sans s'y limiter, en cas de violation du présent Contrat. En utilisant les Services, y compris les produits ou services qui facilitent le partage de Contenu vers ou à partir de sites Web tiers, vous comprenez que vous êtes seul responsable de toute information que vous partagez avec les Sociétés Tripadvisor. Vous ne pouvez accéder aux Services qu'aux fins prévues par le biais des fonctionnalités fournies par les Services et que dans les limites permises par le présent Contrat.

La copie, la transmission, la reproduction, la réplication, la publication ou la redistribution (a) du Contenu ou de toute partie de celui-ci ; (b) des Services de manière plus générale, sont strictement interdits sans l'autorisation écrite préalable des Sociétés Tripadvisor. Pour en faire la demande, veuillez vous adresser à :

Director, Partnerships and Business Development

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, États-Unis

Pour accéder à certaines fonctionnalités des Services, vous devrez créer un compte. Lors de la création d'un compte, vous devez fournir des informations complètes et exactes. Vous êtes seul responsable des activités qui surviennent sur votre compte, y compris de vos interactions et communications avec autrui, et devez assurer sa protection. Dans cette optique, vous vous engagez à maintenir vos coordonnées à jour. 

Si vous créez un compte Tripadvisor à des fins commerciales et acceptez le présent Contrat au nom d'une société, d'une organisation ou de toute autre entité juridique, vous convenez que vous êtes autorisé à le faire et que vous avez le pouvoir de lier ladite entité au présent Contrat, auquel cas les mots « vous » et « votre » utilisés dans le présent Contrat font référence à l'entité, et la personne agissant au nom de la société est considérée comme un « Représentant de l'établissement ».

Dans le cadre de votre utilisation des Services, vous pouvez être amené à rencontrer des liens vers des sites Web et applis tiers ou en mesure d'interagir avec ces derniers. Cela peut inclure la possibilité de partager le Contenu à partir des Services, y compris votre Contenu, avec ces sites Web et applis tiers. Veuillez noter que les sites Web et applis tiers peuvent afficher publiquement ce Contenu partagé. Ces tiers peuvent facturer des frais pour l'utilisation de certains contenus ou services fournis sur ou par l'intermédiaire de leurs sites Web. Par conséquent, il vous incombe d'effectuer toute recherche que vous jugez nécessaire ou appropriée avant de procéder à une transaction avec un tiers afin de déterminer si celle-ci impliquera des frais. Lorsque les Sociétés Tripadvisor fournissent des détails sur les frais ou coûts pour les contenus ou services desdits tiers, ces informations ne sont fournies qu'à des fins pratiques et à titre d'information seulement. Toutes interactions avec les sites Web et applis tiers sont à vos propres risques. Vous reconnaissez et acceptez expressément que les Sociétés Tripadvisor ne sont en aucun cas responsables de ces sites Web ou applis tiers et, ce faisant, que leur responsabilité ne saurait nullement être engagée.

Les Sociétés Tripadvisor classent et vous présentent des pages établissement à l'aide de systèmes de recommandation. Différentes catégories d'établissements (comme les hôtels et restaurants, les activités et les vols) sont classées en fonction de critères tels que le prix, les favoris des voyageurs et la durée. Les détails complets de notre politique et de nos critères de classement sont disponibles sur la [page Fonctionnement du site](https://fr.tripadvisor.be/HowTheSiteWorks.html).

Certains Contenus que vous voyez ou auxquels vous accédez sur ou par l'intermédiaire des Services sont utilisés à des fins commerciales. Vous acceptez et comprenez que les Sociétés Tripadvisor peuvent diffuser des publicités et des promotions sur les Services à côté ou à proximité de votre Contenu (y compris, pour la vidéo ou tout autre contenu dynamique, avant, pendant ou après sa présentation), ainsi que du Contenu tiers.

**PRODUITS SUPPLÉMENTAIRES**

Les Sociétés Tripadvisor peuvent ponctuellement décider de modifier, mettre à jour ou interrompre certains produits et fonctionnalités des Services. Vous acceptez et comprenez que les Sociétés Tripadvisor n'ont nullement l'obligation de stocker ou de conserver votre Contenu ou toute autre information que vous fournissez, sauf dans la mesure requise par la législation en vigueur.

Nous proposons également d'autres services qui peuvent être régis par des conditions supplémentaires ou d'autres accords. Si vous utilisez d'autres services de ce type, les conditions supplémentaires seront mises à votre disposition et feront partie intégrante du présent Contrat, sauf si elles excluent expressément le présent Contrat ou le remplacent d'une quelconque autre manière. Par exemple, si vous utilisez ou achetez ces services supplémentaires à des fins commerciales ou d'affaires, vous devez accepter les conditions supplémentaires applicables. Si les autres conditions, quelles qu'elles soient, sont incompatibles avec les conditions générales du présent Contrat, les conditions supplémentaires s'appliqueront dans la mesure de l'incompatibilité s'agissant de ces services particuliers.

**ACTIVITÉS INTERDITES**

Les Contenus et informations disponibles sur les Services et par leur intermédiaire (y compris, mais de manière non exhaustive, les messages, données, informations, textes, musiques, sons, photos, graphiques, vidéos, cartes, icônes, logiciels, codes ou autres éléments), ainsi que les infrastructures utilisées pour fournir lesdits Contenus et informations, sont la propriété exclusive des Sociétés Tripadvisor ou concédés sous licence aux Sociétés Tripadvisor par des tiers. S'agissant de tout Contenu autre que le Contenu que vous soumettez, vous vous engagez à ne pas modifier, copier, distribuer, transmettre, afficher, exécuter, reproduire, publier, concéder sous licence, créer des œuvres dérivées, transférer ou vendre ou revendre tout logiciel, information, produit ou service obtenus depuis ou par l'intermédiaire des Services. En outre, vous vous engagez à ne pas :

*  (i) utiliser les Services ou le Contenu à des fins commerciales, en dehors du champ d'application des fins commerciales explicitement autorisées par le présent Contrat et les directives connexes mises à disposition par les Sociétés Tripadvisor ;
* (ii) accéder, surveiller, reproduire, distribuer, transmettre, diffuser, afficher, vendre, concéder sous licence, copier ou exploiter d'une quelconque autre manière tout Contenu des Services, y compris, mais de manière non exhaustive, les profils d'utilisateurs et les photos, à l'aide d'un robot, d'un « spider » (robot d'indexation), d'un « scraper » ou de tout autre moyen automatisé ou tout processus manuel, pour toute fin non conforme au présent Contrat ou sans notre autorisation écrite expresse ;
* (iii) enfreindre les restrictions figurant dans les protocoles d'exclusion des robots présents sur les Services ou contourner ou neutraliser les autres mesures employées pour empêcher ou limiter l'accès aux Services ;
* (iv) prendre toute mesure qui impose ou est susceptible d'imposer, à notre seule discrétion (ou à notre discrétion raisonnable si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni), une surcharge déraisonnable ou disproportionnée sur nos infrastructures ;
* (v) établir des liens profonds avec une quelconque partie des Services à toute fin sans notre autorisation écrite expresse ;
* (vi) « encadrer », « dupliquer » ou intégrer d'une quelconque autre manière toute partie des Services dans un autre site Web ou service sans notre autorisation écrite préalable ;
* (vii) tenter de modifier, de traduire, d'adapter, de décompiler, de désassembler ou d'effectuer de l'ingénierie inversée sur les programmes logiciels utilisés par les Sociétés Tripadvisor en lien avec les Services ;
* (viii) contourner, désactiver ou interférer d'une quelconque autre manière avec les fonctionnalités de sécurité des Services ou les fonctionnalités qui empêchent ou restreignent l'utilisation ou la copie de tout Contenu ;
* (ix) télécharger du Contenu à moins qu'il ne soit expressément mis à votre disposition par les Sociétés Tripadvisor en vue de son téléchargement ;
* (x) utiliser, aider, encourager ou permettre à un tiers d'avoir recours à tout robot, logiciel d'indexation, système d'intelligence artificielle (IA) ou autre dispositif, processus ou moyen automatisés pour accéder, récupérer, copier, extraire, agréger, recueillir, télécharger ou indexer de quelque manière que ce soit toute partie des Services ou tout Contenu, sauf dans la mesure où Tripadvisor le permet expressément par écrit.

**POLITIQUE DE CONFIDENTIALITÉ ET COMMUNICATIONS D'INFORMATIONS**

Toutes les informations personnelles que vous publiez sur les Services ou que vous soumettez d'une quelconque manière en lien avec ces derniers seront utilisées conformément à notre Déclaration relative à la confidentialité et aux cookies. Cliquez [ici](https://tripadvisor.mediaroom.com/FR-privacy-policy) pour consulter notre Déclaration relative à la confidentialité et aux cookies.

**AVIS, COMMENTAIRES ET UTILISATION DES AUTRES ZONES INTERACTIVES ; OCTROI DE LICENCE**

Nous sommes heureux de recevoir vos commentaires et autres contributions. Veuillez noter qu'en fournissant votre Contenu à nos Services ou par leur intermédiaire, que ce soit par e-mail, par une publication grâce à l'un des produits de synchronisation de Tripadvisor, à l'aide des services et applications tiers, ou d'une quelconque autre manière, y compris tout Contenu qui est transmis à votre compte Tripadvisor grâce à l'un des produits ou services des Services Tripadvisor, tout avis, toute question, photographie ou vidéo, tout commentaire, toute suggestion, idée, etc., figurant dans votre Contenu, vous accordez aux Sociétés Tripadvisor un droit non exclusif, exempt de redevance, perpétuel, transférable, irrévocable et entièrement cessible en sous-licence (a) d'héberger, d'utiliser, de reproduire, de modifier, d'adapter, de traduire, de diffuser, de publier, de créer des œuvres dérivées et d'afficher et exploiter publiquement ledit Contenu que vous soumettez dans le monde entier, sur tout support actuel ou à venir ; (b) de rendre votre Contenu disponible au reste du monde et laisser les autres faire de même ; (c) de fournir, de promouvoir et d'améliorer les Services et de mettre votre Contenu partagé sur les Services à la disposition d'autres personnes physiques ou morales pour la syndication, diffusion, distribution, promotion ou publication de votre Contenu sur d'autres médias et services, sous réserve de notre Politique de confidentialité et du présent Contrat ; et (b) d'utiliser le nom et/ou la marque commerciale que vous soumettez en relation avec vos Contenus. Vous reconnaissez que Tripadvisor peut choisir de vous attribuer votre Contenu à sa seule discrétion (ou à sa discrétion raisonnable, si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni). En outre, vous accordez aux Sociétés Tripadvisor le droit de poursuivre en justice toute personne physique ou morale ayant enfreint vos droits ou ceux de Tripadvisor dans vos Contenus par la violation du présent Contrat. Vous reconnaissez et acceptez que vos Contenus ne soient pas confidentiels et ne fassent l'objet d'aucun droit de propriété. Vous convenez que vous disposez des licences, droits (y compris les droits d'auteur et autres droits de propriété), consentements et autorisations nécessaires pour publier et utiliser d'une quelconque autre manière vos Contenus (et pour que les Sociétés Tripadvisor puissent les publier et les utiliser d'une quelconque autre manière), comme l'autorise le présent Contrat.

S'il est établi que vous conservez les droits moraux (y compris les droits d'attribution ou d'intégrité) relatifs à vos Contenus, vous déclarez par les présentes, dans les limites permises par la loi, (a) ne pas exiger que des données d'identification personnelle soient utilisées dans le cadre des Contenus, de toute œuvre dérivée ou de toute mise à jour ou niveau s'y rapportant ; (b) ne pas vous opposer à la publication, l'utilisation, la modification, la suppression et l'exploitation de vos Contenus par les Sociétés Tripadvisor ou leurs licenciés, successeurs et ayants droit ; (c) renoncer irrévocablement et accepter de ne pas revendiquer ou faire valoir un quelconque droit moral en tant qu'auteur de vos Contenus ; et (d) dégager définitivement les Sociétés Tripadvisor ainsi que leurs licenciés, successeurs et ayants droit, de toute responsabilité pour les réclamations qui sinon pourraient être présentées à l'encontre des Sociétés Tripadvisor en vertu desdits droits moraux.

Notez que vos commentaires et autres suggestions peuvent être utilisés à tout moment et que nous ne sommes pas tenus d'en préserver la confidentialité.

Les Services peuvent contenir des tableaux d'affichage, des services d'avis, des travel feeds sur les voyages, des forums de discussion ou autres au sein desquels vous pouvez publier vos Contenus, notamment des commentaires sur vos voyages, messages, documents ou autres éléments (les « Zones interactives »). Si Tripadvisor fournit ces Zones interactives sur les sites Web, vous êtes seul responsable de leur utilisation, et ce à vos propres risques. Les Sociétés Tripadvisor ne garantissent pas la confidentialité de tout Contenu que vous fournissez aux Services ou dans une Zone interactive.  Dans la mesure où une entité constituant l'une des Sociétés Tripadvisor fournit une forme quelconque de canal de communication privé entre les utilisateurs, vous convenez que cette ou ces entités peuvent surveiller le contenu desdites communications afin de contribuer à la protection de notre communauté et des Services. Vous comprenez que les Sociétés Tripadvisor ne modifient ni ne contrôlent les messages des utilisateurs postés ou distribués par le biais des Services, y compris par l'intermédiaire de salons de discussion (chat rooms), de tableaux d'affichage ou d'autres forums de communication, et déclinent toute responsabilité concernant lesdits messages.  En particulier, Tripadvisor ne modifie ni ne contrôle le Contenu des utilisateurs qui apparaît sur les sites Web.  Les Sociétés Tripadvisor se réservent néanmoins le droit de supprimer et/ou de restreindre sans préavis tout message ou autre Contenu des Services si elles estiment en toute bonne foi que ce Contenu enfreint le présent Contrat ou que la suppression et/ou la restriction sont raisonnablement nécessaires en vue de préserver leurs droits et/ou ceux d'autres utilisateurs des Services. Si vous n'êtes pas d'accord avec la suppression de votre Contenu des sites Web, vous pouvez faire appel de notre décision dans le cadre du processus interne de gestion des plaintes de Tripadvisor. Vous trouverez de plus amples informations sur la manière de contester et de faire appel d'une décision de suppression ou de restriction de votre Contenu [ici](https://www.tripadvisorsupport.com/fr-FR/hc/traveler/articles/623). En utilisant les Zones interactives, vous acceptez expressément de ne soumettre que du Contenu conforme aux directives publiées par Tripadvisor, en vigueur au moment de la soumission et mises à votre disposition par Tripadvisor.  Vous acceptez expressément de ne pas télécharger, transmettre, distribuer, stocker, créer, ni publier de quelque manière que ce soit l'un des éléments suivants par le biais des Services :

1. Tout Contenu faux, illégal, trompeur, calomnieux, diffamatoire, obscène, pornographique, indécent, inconvenant, suggestif, harcelant, ou qui encourage le harcèlement d'une autre personne, menaçant, irrespectueux de la vie privée ou du droit à l'image publique, injurieux, provocateur, frauduleux ou répréhensible d'une quelconque autre manière ;
2. Tout Contenu manifestement offensant pour la communauté en ligne, notamment les contenus qui incitent au racisme, au sectarisme, à la haine ou à la violence physique, quelle qu'elle soit, à l'encontre de tout groupe ou individu ;
3. Tout contenu qui constituerait, encouragerait, promouvrait ou fournirait des instructions relatives à la conduite d'une activité illégale et/ou d'une infraction pénale, engagerait la responsabilité civile, violerait les droits d'une quelconque partie dans n'importe quel pays du monde, ou engagerait par ailleurs une responsabilité ou violerait toute loi locale, nationale ou internationale, y compris, mais sans s'y limiter, les réglementations des États-Unis. Securities and Exchange Commission (SEC) ou toute règle d'une bourse de valeurs, y compris, mais sans s'y limiter, le New York Stock Exchange (NYSE), le NASDAQ ou le London Stock Exchange ;
4. Tout Contenu qui fournit des instructions relatives à la conduite d'activités illégales telles que la fabrication ou l'achat d'armes illégales, la violation de la vie privée d'autrui ou la fourniture ou la création de virus informatiques ;
5. Tout Contenu susceptible de porter atteinte à un brevet, une marque commerciale, un secret de fabrication, un droit d'auteur ou tout autre droit intellectuel ou de propriété d'un tiers ;  En particulier, tout Contenu incitant à la réalisation d'une copie illégale ou non autorisée d'une œuvre appartenant à un tiers et protégée par le droit d'auteur, telle que la fourniture de programmes informatiques piratés ou de liens vers ces programmes, la communication d'informations permettant de contourner les dispositifs d'origine destinés à protéger contre la copie ou la fourniture de fichiers de musique piratés ou de liens vers ces fichiers ;
6. Tout Contenu qui constitue du publipostage, des pourriels, des chaînes de lettre ou des systèmes pyramidaux ;
7. Tout Contenu qui usurpe l'identité de toute personne physique ou morale, ou présente de manière inexacte vos rapports avec une personne physique ou morale, y compris les Sociétés Tripadvisor ;
8. Tout Contenu divulguant les informations personnelles d'une tierce personne, y compris, mais sans s'y limiter : adresses physiques et électroniques, numéro de téléphone, numéro de sécurité sociale et numéro de carte bancaire  (veuillez noter que le nom de famille d'une personne peut être publié sur nos sites Web, mais seulement après avoir obtenu l'autorisation expresse de la personne identifiée) ;
9. Tout Contenu portant sur des pages à accès restreint ou par mot de passe uniquement, ou des pages ou des images cachées (qui ne contiennent pas de liens vers ou provenant d'une autre page accessible) ;
10. Tout Contenu comportant des virus, données corrompues ou autres fichiers nuisibles, perturbateurs ou destructeurs ou visant à en faciliter l'envoi ;
11. Tout Contenu sans rapport avec le sujet de la ou des Zones interactives au sein desquelles ledit Contenu est publié ;
12. À la seule discrétion de Tripadvisor (ou à sa discrétion raisonnable, si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni), tout Contenu qui (a) viole les précédents paragraphes des présentes, (b) enfreint les directives connexes de Tripadvisor que nous mettons à votre disposition, (c) s'avère répréhensible, (d) limite ou empêche une autre personne d'utiliser ou d'apprécier les Zones interactives ou tout autre aspect des Services ou (e) pourrait exposer l'une des Sociétés Tripadvisor ou leurs utilisateurs à tout type de dommages ou d'actions en responsabilité.

Dans la mesure permise par la loi applicable, les Sociétés Tripadvisor déclinent toute responsabilité quant aux Contenus publiés, stockés, transmis ou chargés sur les Services par vous (s'il s'agit de votre Contenu) ou par des tiers (pour tout Contenu en général), ou pour toutes les pertes ou tous les dommages liés auxdits Contenus. De plus, les Sociétés Tripadvisor déclinent toute responsabilité en cas d'erreurs, d'actes de diffamation, de calomnie, d'omissions, de mensonges, ou de contenus obscènes, pornographiques ou vulgaires que vous pourriez rencontrer. En tant que prestataire de services interactifs, et dans la mesure permise par la loi applicable, Tripadvisor décline toute responsabilité quant aux déclarations, représentations ou autres Contenus fournis par ses utilisateurs (y compris vous en ce qui concerne votre Contenu) sur ses sites Web ou tout autre forum. Bien que Tripadvisor n'ait aucune obligation de filtrer, modifier ou surveiller le Contenu publié dans les Zones interactives ou par le biais de ces dernières, Tripadvisor se réserve le droit, à son entière discrétion (ou à sa discrétion raisonnable, si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni), de supprimer, de filtrer, de traduire ou de modifier sans préavis tout Contenu publié ou stocké sur les Services à tout moment et pour quelque motif que ce soit, ou de faire exécuter de telles actions par des tiers en son nom. Dans la mesure permise par la loi applicable, il vous incombe de créer des copies de sauvegarde et de remplacer tout Contenu que vous publiez, nous soumettez ou stockez sur les Services, et ce à vos propres frais. Vous trouverez [ici](https://fr.tripadvisor.be/Trust) tous les détails de nos politiques de modération du Contenu, y compris les restrictions que nous pouvons imposer à votre propre Contenu.

**RESTRICTION ET/OU SUSPENSION DE VOTRE UTILISATION DES SERVICES**

Toute utilisation des Zones interactives ou d'autres aspects des Services non conformes aux conditions ci-dessous constitue une violation du présent Contrat et peut notamment entraîner l'annulation ou la suppression de vos droits d'utilisation des Zones interactives et/ou des Services de manière plus générale. Tripadvisor peut également suspendre le traitement des notifications et des plaintes envoyées par le biais des Services si vous soumettez fréquemment des notifications ou des plaintes manifestement injustifiées.

Notre approche de la suspension de votre utilisation de la plateforme est détaillée [ici](https://fr.tripadvisor.be/Trust).

**Limitation des droits de licence de Tripadvisor.** Vous pouvez choisir ultérieurement de limiter l'utilisation de votre Contenu par les Sociétés Tripadvisor dans le cadre du présent Contrat (tel que décrit ci-dessus) en choisissant de leur fournir une licence plus limitée telle que décrite ci-dessous (ladite licence étant désignée dans les présentes par l'expression « Licence d'utilisation limitée »).  Vous pouvez faire ce choix en sélectionnant l'octroi d'une Licence d'utilisation limitée [ici](https://fr.tripadvisor.be/Settings-cs) (veuillez noter que pour ce faire, vous devrez être connecté à votre compte). Si vous faites ce choix, les droits que vous accordez aux Sociétés Tripadvisor sur votre Contenu conformément aux conditions de licence énoncées ci-dessus (ci-après la « Licence standard ») seront limités de manière substantielle, tel que cela est décrit dans les paragraphes 1 à 6 directement ci-dessous, de telle sorte que les Sociétés Tripadvisor ne détiennent pas de Licence standard pour tout Contenu autre que vos avis textuels que vous publiez et les notes sous forme de bulles qui y sont associées (pour lesquels les Sociétés Tripadvisor continuent de bénéficier d'une Licence standard), mais reçoivent une « Licence d'utilisation limitée » pour le reste de votre Contenu, comme défini ci-dessous :

1. Lorsque vous publiez votre Contenu sur les Services, la licence que vous accordez aux Sociétés Tripadvisor pour Votre Contenu est limitée à une licence non exclusive, mondiale, exempte de redevance, transférable et pouvant être concédée en sous-licence pour héberger, utiliser, distribuer, modifier, exécuter, reproduire, afficher publiquement ou effectuer, traduire et créer des œuvres dérivées de votre Contenu en vue de l'affichage sur les Services, ainsi que de l'utilisation de votre nom et/ou marque commerciale en rapport avec ledit Contenu. Sous réserve du paragraphe 6 ci-dessous, la Licence d'utilisation limitée s'applique à tout votre Contenu (à l'exception de vos avis textuels et des notes sous forme de bulles qui y sont associées) que vous ou une autre personne agissant en votre nom (par exemple, un tiers qui contribue à votre compte ou le gère) mettez à disposition sur ou en rapport avec les Services.

1. S'agissant de tout élément individuel de votre Contenu soumis à la Licence d'utilisation limitée, vous pouvez mettre fin aux droits de licence des Sociétés Tripadvisor aux termes des présentes en supprimant ce message des Services.  De même, vous pouvez mettre fin aux droits de licence des Sociétés Tripadvisor sur l'ensemble de votre Contenu soumis à la Licence d'utilisation limitée en clôturant votre compte (une description de la procédure à suivre est disponible [ici](https://www.tripadvisorsupport.com/fr-FR/hc/traveler/articles/510)**)**. Nonobstant toute disposition contraire, votre Contenu (a) restera sur les Services dans la mesure où vous l'avez partagé avec d'autres personnes et que celles-ci l'ont copié ou stocké avant sa suppression par vos soins ou la clôture de votre compte, (b) peut continuer à être affiché sur les Services pendant une durée raisonnable après sa suppression par vos soins ou la clôture de votre compte alors que nous cherchons à le supprimer, et/ou (c) peut être conservé sous forme de copie de sauvegarde (mais non affiché publiquement) pendant un certain laps de temps pour des raisons techniques, réglementaires, légales ou de modération et de lutte contre la fraude.

1. Les Sociétés Tripadvisor n'utiliseront pas votre Contenu dans des publicités pour les produits et services de tiers sans votre consentement distinct (y compris en cas de Contenu sponsorisé), même si vous acceptez et comprenez que les Sociétés Tripadvisor peuvent placer des publicités et des promotions sur les Services à côté ou à proximité de votre Contenu (y compris, pour la vidéo ou tout autre contenu dynamique, avant, pendant ou après sa présentation), ainsi que du Contenu d'autrui.  À chaque fois que votre Contenu est affiché sur les Services, il vous est attribué ; pour ce faire, nous utilisons le nom et/ou la marque commerciale que vous soumettez avec votre Contenu.  

1. Les Sociétés Tripadvisor ne donneront pas à des tiers le droit de publier votre Contenu en dehors des Services. Toutefois, le partage de votre Contenu sur les Services (à l'exception de notre fonctionnalité « Voyages », qui peut être rendue privée) a pour conséquence de le rendre « public ». En outre, nous activons une fonctionnalité qui permet à d'autres utilisateurs de partager votre Contenu (en intégrant cette publication publique ou d'une quelconque autre manière, à l'exception des « Voyages » que vous pouvez configurer pour être privés, comme indiqué) sur des services tiers, et aux moteurs de recherche de rendre accessible votre Contenu public par l'intermédiaire de leurs services.

1. Sous réserve des modifications apportées par les paragraphes 1 à 6 de la présente section du présent Contrat, vos droits et obligations, ainsi que les nôtres, demeurent assujettis aux autres dispositions du présent Contrat. La licence que vous accordez aux Sociétés Tripadvisor, telle qu'elle est modifiée par ces paragraphes 1 à 6, constitue une « Licence d'utilisation limitée ».

1. Par souci de clarté, le Contenu que vous soumettez aux Services en relation avec d'autres services ou programmes des Sociétés Tripadvisor n'est pas soumis à la Licence d'utilisation limitée, mais régi par les conditions générales associées à ce service ou programme spécifique de Tripadvisor.

 

**RÉSERVATION AUPRÈS DE PRESTATAIRES TIERS PAR L'INTERMÉDIAIRE DE Tripadvisor**

**Utilisation des services de réservation de Tripadvisor.** Les sociétés Tripadvisor vous donnent la possibilité de rechercher, de sélectionner et d'effectuer des réservations de voyage auprès de prestataires tiers sans quitter les Services.

#### IMPORTANT : Tripadvisor en tant que tel ne vend pas de réservations de voyage. Tripadvisor permet à des tiers de vendre leurs réservations de voyage sur son site Web.

#### Tripadvisor peut faciliter la promotion et la vente de réservations de voyage par des tiers à votre intention. Toutefois, Tripadvisor n'est pas la partie qui assure la promotion et/ou la vente. Si vous effectuez une réservation auprès d'un prestataire tiers, le contrat afférent à la réservation est toujours conclu exclusivement entre vous et le prestataire concerné. Tripadvisor n'est pas (a) l'acheteur ou le vendeur de la réservation ; (b) une partie au contrat passé avec le tiers ou responsable de l'exécution dudit contrat ; ou (c) un agent agissant pour votre compte ou celui du tiers dans le cadre de l'achat ou de la vente. Si vous effectuez une réservation auprès d'un prestataire tiers, vous acceptez de prendre connaissance des conditions générales d'achat et d'utilisation du site Web dudit prestataire, de sa politique de confidentialité et de toute autre règle ou clause relative à son site ou à son établissement, et d'y être lié. Vos interactions avec les prestataires tiers sont à vos propres risques. Les Sociétés Tripadvisor n'assument aucune responsabilité à l'égard de tout acte, omission, erreur, déclaration, garantie, manquement ou négligence des prestataires tiers ou préjudice corporel, décès, dégât matériel ou autre dommage ou frais résultant de vos interactions avec les prestataires tiers.

Vos droits de recours dans le cadre d'un contrat conclu entre vous et un prestataire tiers (par exemple, en ce qui concerne les remboursements et les annulations) seront exercés entre vous et le prestataire tiers, comme stipulé dans le contrat établi avec ce dernier. Dans le cas peu probable où une réservation serait disponible lors de la validation d'une commande, mais deviendrait indisponible avant votre arrivée, votre seul recours sera de contacter le prestataire tiers afin de prendre d'autres dispositions ou d'annuler votre réservation et d'obtenir un remboursement (le cas échéant et sous réserve de la législation applicable et de votre contrat avec le prestataire).

#### Les prestataires tiers peuvent être des entreprises ou des particuliers. Si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni et que le prestataire tiers avec lequel vous concluez un contrat est un particulier, veuillez noter que vous ne pourrez pas faire valoir vos droits de consommateur vis-à-vis dudit prestataire.

Pour réserver des éléments de voyage via les sites Web, vous devez créer un compte (si ce n'est pas déjà fait). Vous reconnaissez également que vous acceptez les pratiques décrites dans notre Politique de confidentialité et dans le présent Contrat.

EN TANT QU'UTILISATEUR DES SERVICES, Y COMPRIS LES SERVICES D'AIDE À LA RÉSERVATION DES SOCIÉTÉS TRIPADVISOR, VOUS COMPRENEZ ET ACCEPTEZ QUE, DANS LA MESURE PERMISE PAR LA LOI APPLICABLE : (1) LES SOCIÉTÉS TRIPADVISOR NE PEUVENT PAS ÊTRE TENUES POUR RESPONSABLES ENVERS VOUS OU AUTRUI EN CAS DE TRANSACTIONS NON AUTORISÉES RÉALISÉES À L'AIDE DE VOTRE MOT DE PASSE OU DE VOTRE COMPTE ; ET (2) L'UTILISATION NON AUTORISÉE DE VOTRE MOT DE PASSE OU DE VOTRE COMPTE POURRAIT VOUS AMENER À ENGAGER LA RESPONSABILITÉ DE TRIPADVISOR, DE SES PARTENAIRES AFFILIÉS ET/OU D'AUTRUI.

L'utilisation de nos Services, y compris la création d'un compte Tripadvisor, est gratuite, et le paiement ne sera traité que si vous effectuez une réservation via les Services. Lorsque vous effectuez une réservation par l'intermédiaire des services d'aide à la réservation des Sociétés Tripadvisor, vos informations de paiement sont collectées et transmises au prestataire pour finaliser la transaction, tel que décrit dans notre Politique de confidentialité. Veuillez noter que la responsabilité du traitement de votre paiement et du respect de votre réservation incombe au prestataire, et non aux Sociétés Tripadvisor.

Les Sociétés Tripadvisor ne peuvent interférer de manière arbitraire dans les réservations, mais se réservent le droit de retirer les services d'aide à la réservation en raison de certaines circonstances atténuantes, par exemple lorsqu'une réservation n'est plus disponible ou lorsque nous avons des motifs raisonnables de croire qu'une demande de réservation pourrait être frauduleuse. Les Sociétés Tripadvisor se réservent également le droit de prendre des mesures pour vérifier votre identité afin de traiter votre demande de réservation.

**Prestataires tiers.** Les Sociétés Tripadvisor ne sont pas des agences de voyages et ne fournissent ni ne possèdent de services de transport, d'hébergement, de restauration ou d'organisation d'excursions, d'activités ou d'expériences. Bien que les Sociétés Tripadvisor affichent des informations sur les établissements proposés par des prestataires tiers et facilitent les réservations auprès de certains prestataires sur leurs sites Web ou par leur intermédiaire, de telles actions n'impliquent, ne suggèrent, ni ne constituent de quelque manière que ce soit un parrainage ou une approbation par les Sociétés Tripadvisor de ces prestataires tiers, ni même une quelconque affiliation entre les Sociétés Tripadvisor et ces derniers. Bien que les utilisateurs puissent noter et évaluer certains services de transport, hébergements, restaurants, excursions, activités ou expériences en fonction de leur propre vécu, les Sociétés Tripadvisor n'approuvent ni ne recommandent les produits ou les services des prestataires tiers, à l'exception des prix remis à certains établissements par Tripadvisor sur la base des avis publiés par les utilisateurs. Les Sociétés Tripadvisor n'approuvent aucun Contenu publié, soumis ou fourni d'une quelconque autre manière par un utilisateur ou un établissement, ni aucun conseil, opinion ou recommandation qui y est exprimé, et déclinent expressément toute responsabilité concernant ledit Contenu. Vous convenez que les Sociétés Tripadvisor ne sont pas responsables de l'exactitude ou de l'exhaustivité des informations qu'elles obtiennent auprès de prestataires tiers et qu'elles affichent sur les Services.

Les Services peuvent contenir des liens vers des sites Web des prestataires ou autres qui ne sont ni contrôlés ni exploités par Tripadvisor. Pour de plus amples informations, veuillez consulter la section « Liens vers des sites Web tiers » ci-dessous.

**Réservation de services de voyage via l'application mobile Tripadvisor.**  Certains services de voyage réservés via l'application mobile Tripadvisor sont régis par des conditions d'utilisation supplémentaires.  Celles-ci sont disponibles dans l'application elle-même et peuvent être consultées [ici](https://tripadvisor.mediaroom.com/us-terms-of-use-booking).

**Réservation de locations de vacances, de restaurants et d'expériences auprès de prestataires tiers répertoriés sur les sites Web de partenaires affiliés.** Certains partenaires affiliés de Tripadvisor interviennent en tant que marketplaces afin d'aider les voyageurs à (1) conclure des contrats de location de vacances avec des propriétaires et des gestionnaires (« Locations de vacances »), (2) réserver des restaurants (« Restaurants ») et/ou (3) réserver des excursions, activités et attractions (indifféremment les « Expériences ») auprès de prestataires tiers (chacun desdits prestataires de Locations de vacances et/ou d'Expériences étant désigné par le terme « Annonceur »). Ces partenaires affiliées de Tripadvisor publient leurs annonces auprès d'autres entités du groupe de Sociétés Tripadvisor, c'est pourquoi elles sont visibles sur les sites Web des Sociétés Tripadvisor.  En tant qu'utilisateur, vous devez être responsable de votre utilisation des Services (notamment des sites Web des Sociétés Tripadvisor) et de toute transaction impliquant des Locations de vacances, Restaurants ou Expériences facilitées par les partenaires affiliés de Tripadvisor. Nous ne possédons pas, ne gérons pas et n'établissons pas de contrats pour les Locations de vacances, les Restaurants ou les Expériences répertoriés sur les Services.

Ni Tripadvisor ni ses partenaires affiliés ne sont parties aux transactions de Location de vacances, réservations de Restaurant ou transactions liées à des Expériences effectuées entre les voyageurs et les Annonceurs. Tout litige ou différend impliquant une transaction réelle ou potentielle entre vous et un Annonceur, y compris concernant la qualité, les conditions, la sécurité ou la légalité d'une Location de vacances, d'un Restaurant ou d'une Expérience répertorié, l'exactitude du Contenu de la page Établissement, la capacité de l'Annonceur à louer un hébergement de Location de vacances, à vous fournir une réservation, un repas ou un autre service dans un Restaurant ou à vous fournir une Expérience, ou votre capacité à payer pour un hébergement de Location de vacances, un repas ou un service dans un Restaurant ou une Expérience, relèvent de la responsabilité exclusive de chaque utilisateur.

L'un des partenaires affiliés de Tripadvisor peut agir en qualité d'agent à mandat restreint de l'Annonceur uniquement dans le but de transmettre votre paiement à ce dernier. Vous acceptez de payer à un Annonceur, ou à un partenaire affilié de Tripadvisor agissant en tant qu'agent de recouvrement limité pour le compte d'un Annonceur, tous les frais qui vous sont spécifiés avant de réserver une Location de vacances ou une Expérience, ainsi que tous les frais qui vous sont facturés en raison de dommages résultant de votre utilisation de la Location de vacances ou de l'Expérience, conformément aux conditions ou à la politique en vigueur.

Pour plus d'informations sur les frais liés aux Location de vacances, les dépôts de garantie, les frais liés aux Expériences, le traitement des paiements, les remboursements et autres, veuillez consulter les conditions générales de nos partenaires affiliés (pour les Expériences, accédez au site de [Viator](https://www.viator.com/fr-FR/support/termsAndConditions) ; pour les Restaurants, à celui de [The Fork](https://www.thefork.fr/#geo_dmn_redirect=20210629AT100) ; pour les Locations de vacances, à celui des [Locations de vacances des Sociétés Tripadvisor](https://rentals.tripadvisor.com/fr_FR/termsandconditions/traveler). En réservant une Location de vacances, un Restaurant ou une Expérience auprès de l'un de nos partenaires affiliés, vous vous engagez à reconnaître et accepter ses conditions générales, ainsi que sa politique de confidentialité.

En cas de litige avec un Annonceur au sein de l'Union européenne, d'autres méthodes de résolution des différends sont disponibles en ligne à l'adresse [http://ec.europa.eu/consumers/odr/](https://ec.europa.eu/consumers/odr/main/?event=main.home2.show&lng=FR).

**DESTINATIONS DE VOYAGE**

**Voyage à l'international.** Lorsque vous effectuez des réservations de voyage à l'international auprès de prestataires tiers ou planifiez des voyages à l'international par l'intermédiaire des Services, il vous incombe de vous assurer que vous remplissez toutes les conditions d'entrée sur le territoire du pays de destination et que vos documents de voyage, passeport et visas compris, sont en ordre.

Pour connaître les exigences relatives aux passeports et aux visas, veuillez consulter l'ambassade ou le consulat concerné pour plus d'informations. Ces exigences pouvant changer à tout moment, assurez-vous de disposer d'informations actualisées avant la réservation et le départ. Les Sociétés Tripadvisor déclinent toute responsabilité vis-à-vis de tout voyageur qui se voit refuser l'accès à bord d'un avion ou l'entrée dans un pays en raison de son incapacité à présenter les documents de voyage requis par la compagnie aérienne, les autorités ou le pays, y compris les pays où le voyageur ne fait que transiter.

Il est également de votre responsabilité de consulter votre médecin pour des recommandations actuelles concernant les vaccinations avant tout voyage à l'étranger ainsi que de vous assurer de respecter toutes les exigences sanitaires d'entrée et de suivre tous les conseils médicaux relatifs à votre voyage.

Bien que la plupart des voyages, y compris vers des destinations étrangères, se déroulent sans incident, certaines destinations peuvent présenter des risques plus importants que d'autres. Tripadvisor recommande vivement aux passagers de se renseigner sur les interdictions de voyage, mises en garde, annonces et avis émis par leur gouvernement et les gouvernements de pays de destination avant la réservation de voyages vers des destinations internationales. Par exemple, des informations sur la situation dans différents pays et le niveau de risque associé aux voyages vers des destinations internationales particulières sont communiquées par le Gouvernement américain aux adresses suivantes : [www.state.gov](http://www.state.gov/), [www.tsa.gov](https://www.tsa.gov/),[www.dot.gov](http://www.dot.gov/), [www.faa.gov](http://www.faa.gov/), [www.cdc.gov](http://www.cdc.gov/), [www.treas.gov/ofac](http://www.treas.gov/ofac) et [www.customs.gov](http://www.customs.gov/).

BIEN QUE LES SOCIÉTÉS TRIPADVISOR FOURNISSENT DES INFORMATIONS SUR DES VOYAGES VERS DES DESTINATIONS INTERNATIONALES SPÉCIFIQUES, ELLES NE DÉCLARENT NI NE GARANTISSENT QUE DES VOYAGES VERS DE TELS LIEUX SONT RECOMMANDÉS OU SANS RISQUE, ET DÉCLINONS TOUTE RESPONSABILITÉ POUR LES PERTES OU DOMMAGES SUSCEPTIBLES D'EN RÉSULTER.

**LIMITATIONS DE RESPONSABILITÉ**

VEUILLEZ LIRE ATTENTIVEMENT LA PRÉSENTE SECTION. CETTE SECTION LIMITE LA RESPONSABILITÉ DES SOCIÉTÉS TRIPADVISOR À VOTRE ENCONTRE POUR LES PROBLÈMES SUSCEPTIBLES DE SE POSER DANS LE CADRE DE VOTRE UTILISATION DES SERVICES. SI VOUS NE COMPRENEZ PAS LES TERMES DE CETTE SECTION OU D'AUTRES PARTIES DU PRÉSENT CONTRAT, 

LES INFORMATIONS, LOGICIELS, PRODUITS ET SERVICES PUBLIÉS SUR LES SERVICES OU FOURNIS D'UNE QUELCONQUE AUTRE MANIÈRE PAR LEUR INTERMÉDIAIRE (Y COMPRIS, MAIS SANS S'Y LIMITER, CEUX CRÉÉS OU OPTIMISÉS PAR L'INTELLIGENCE ARTIFICIELLE) PEUVENT INCLURE DES INEXACTITUDES OU DES ERREURS, NOTAMMENT EN CE QUI CONCERNE LA DISPONIBILITÉ DES RÉSERVATIONS ET LES PRIX.  LES SOCIÉTÉS TRIPADVISOR NE GARANTISSENT PAS L'EXACTITUDE DES INFORMATIONS ET DES DESCRIPTIONS RELATIVES AUX HÉBERGEMENTS, AUX EXPÉRIENCES, AUX COMPAGNIES AÉRIENNES ET DE CROISIÈRE, AUX RESTAURANTS OU À TOUT AUTRE PRODUIT DE VOYAGE AFFICHÉS SUR LES SERVICES (Y COMPRIS, MAIS SANS S'Y LIMITER, LES PRIX, LA DISPONIBILITÉ, LES PHOTOGRAPHIES, LA LISTE DES HÉBERGEMENTS, LES EXPÉRIENCES, LES ÉQUIPEMENTS DES COMPAGNIES AÉRIENNES ET DE CROISIÈRE, DES RESTAURANTS OU DES AUTRES PRODUITS DE VOYAGE, LES DESCRIPTIONS GÉNÉRALES, LES AVIS ET LES NOTES, ETC.) ET DÉCLINENT TOUTE RESPONSABILITÉ EN CAS D'ERREURS OU D'INEXACTITUDES LES CONCERNANT. EN OUTRE, LES SOCIÉTÉS TRIPADVISOR SE RÉSERVENT EXPRESSÉMENT LE DROIT DE CORRIGER TOUTE ERREUR DE PRIX OU DE DISPONIBILITÉ SUR LES SERVICES ET/OU SUR LES RÉSERVATIONS EN COURS DE VALIDATION EFFECTUÉES AVEC UN PRIX INEXACT.

TRIPADVISOR NE FAIT AUCUNE DÉCLARATION D'AUCUNE SORTE QUANT À LA PERTINENCE DES SERVICES, Y COMPRIS DES INFORMATIONS CONTENUES SUR SES SITES WEB OU TOUTE PARTIE DE CEUX-CI, À QUELQUE FIN QUE CE SOIT, ET L'INCLUSION DE TOUT PRODUIT OU SERVICE SUR SES SITES WEB OU D'UNE QUELCONQUE AUTRE MANIÈRE PAR L'INTERMÉDIAIRE DES SERVICES NE CONSTITUE PAS UNE APPROBATION OU RECOMMANDATION DESDITS PRODUITS OU SERVICES PROPOSÉS PAR TRIPADVISOR, NONOBSTANT LES PRIX DÉCERNÉS SUR LA BASE DES AVIS DES UTILISATEURS. L'ENSEMBLE DE CES INFORMATIONS, LOGICIELS, PRODUITS ET SERVICES MIS À DISPOSITION PAR L'INTERMÉDIAIRE DES SERVICES SONT PROPOSÉS « EN L'ÉTAT », SANS GARANTIE D'AUCUNE SORTE. DANS LES LIMITES AUTORISÉES PAR LA LÉGISLATION APPLICABLE, TRIPADVISOR REJETTE TOUTE GARANTIE OU CONDITION QUELCONQUE STIPULANT QUE LES SERVICES, SES SERVEURS OU LES DONNÉES (Y COMPRIS LES E-MAILS) ENVOYÉES PAR SES SOINS SONT EXEMPTS DE VIRUS INFORMATIQUES OU D'AUTRES COMPOSANTS NUISIBLES. DANS LES LIMITES AUTORISÉES PAR LA LÉGISLATION APPLICABLE, TRIPADVISOR, PAR LES PRÉSENTES, REJETTE TOUTE GARANTIE ET CONDITION CONCERNANT CES INFORMATIONS, LOGICIELS, PRODUITS ET SERVICES, Y COMPRIS TOUTES LES GARANTIES ET CONDITIONS IMPLICITES OU AUTRES DE QUALITÉ MARCHANDE, D'ADÉQUATION À UN USAGE PARTICULIER, DE PROPRIÉTÉ, DE JOUISSANCE PAISIBLE ET D'ABSENCE DE CONTREFAÇON.

LES SOCIÉTÉS TRIPADVISOR REJETTENT EXPRESSÉMENT TOUTE GARANTIE, DÉCLARATION OU AUTRE CONDITION DE QUELQUE NATURE QUE CE SOIT QUANT À L'EXACTITUDE OU LE CARACTÈRE EXCLUSIF DU CONTENU MIS À DISPOSITION PAR L'INTERMÉDIAIRE DES SERVICES.

LES PRESTATAIRES TIERS QUI FOURNISSENT DES HÉBERGEMENTS, DES BILLETS D'AVION, DES LOCATIONS, DES EXPÉRIENCES, DES SERVICES DE RESTAURATION, DES INFORMATIONS SUR LES CROISIÈRES, DES VOYAGES OU D'AUTRES SERVICES SUR OU PAR L'INTERMÉDIAIRE DES SERVICES SONT DES ENTREPRENEURS INDÉPENDANTS ET NON DES AGENTS OU EMPLOYÉS DES SOCIÉTÉS TRIPADVISOR. LES SOCIÉTÉS TRIPADVISOR NE SONT PAS RESPONSABLES DES ACTES, ERREURS, OMISSIONS, DÉCLARATIONS, GARANTIES, INFRACTIONS OU NÉGLIGENCES DESDITS PRESTATAIRES ET DÉCLINENT TOUTE RESPONSABILITÉ EN CAS DE PRÉJUDICE CORPOREL, DÉCÈS, DÉGÂT MATÉRIEL OU AUTRE DOMMAGE OU FRAIS QUI EN RÉSULTENT. TRIPADVISOR DÉCLINE TOUTE RESPONSABILITÉ ET N'EFFECTUERA AUCUN REMBOURSEMENT EN CAS DE RETARD, D'ANNULATION, DE SURRÉSERVATION, DE GRÈVE, DE FORCE MAJEURE OU DE TOUTE AUTRE CAUSE HORS DE SON CONTRÔLE DIRECT ET N'EST PAS RESPONSABLE DE TOUTE DÉPENSE SUPPLÉMENTAIRE, DE TOUTE OMISSION, DE TOUT RETARD, DE TOUT CHANGEMENT D'ITINÉRAIRE OU DE TOUT ACTE D'UN QUELCONQUE GOUVERNEMENT OU D'UNE QUELCONQUE ADMINISTRATION.

SOUS RÉSERVE DE CE QUI PRÉCÈDE, VOUS UTILISEZ LES SERVICES À VOS PROPRES RISQUES ET EN AUCUN CAS LES SOCIÉTÉS TRIPADVISOR (OU LEURS DIRIGEANTS, ADMINISTRATEURS ET/OU EMPLOYÉS) NE SERAIENT ÊTRE TENUS DE VERSER DES DOMMAGES-INTÉRÊTS POUR TOUTE PERTE OU TOUT DOMMAGE DIRECT, INDIRECT, PUNITIF, ACCESSOIRE, SPÉCIAL OU CONSÉCUTIF, POUR TOUTE PERTE DE REVENUS, DE BÉNÉFICES, DE SURVALEUR (GOODWILL), DE DONNÉES, DE CONTRATS OU DE JOUISSANCE DE FONDS, OU POUR TOUTE PERTE OU TOUT DOMMAGE DÉCOULANT DE OU LIÉ DE QUELQUE FAÇON QUE CE SOIT À L'INTERRUPTION DE VOS ACTIVITÉS DE QUELQUE NATURE QUE CE SOIT EN RAISON DE VOTRE ACCÈS AUX SERVICES, DE LEUR AFFICHAGE OU DE LEUR UTILISATION, DE VOTRE INCAPACITÉ À Y ACCÉDER, À LES AFFICHER OU À LES UTILISER, OU DU RETARD AVEC LEQUEL VOUS Y ACCÉDEZ, LES AFFICHEZ OU LES UTILISEZ (Y COMPRIS, MAIS SANS S'Y LIMITER, VOTRE CONFIANCE DANS LES AVIS ET LES OPINIONS QUI APPARAISSENT SUR LES SERVICES OU PAR LEUR INTERMÉDIAIRE) ; POUR TOUT VIRUS, BUG, CHEVAL DE TROIE, INFORMATION, LOGICIEL, SITE LIÉ, PRODUIT ET SERVICE OBTENUS PAR L'INTERMÉDIAIRE DES SERVICES (Y COMPRIS, MAIS SANS S'Y LIMITER, TOUT PRODUIT DE SYNCHRONISATION DES SOCIÉTÉS TRIPADVISOR) ; POUR TOUT DOMMAGE CORPOREL OU MATÉRIEL, DE QUELQUE NATURE QUE CE SOIT, RÉSULTANT DE VOTRE UTILISATION DES SERVEURS DES SERVICES ET/OU TOUTE INFORMATION PERSONNELLE ET/OU FINANCIÈRE QUI Y SERAIT STOCKÉE ; POUR TOUTE ERREUR OU OMISSION DANS TOUT CONTENU OU POUR TOUTE PERTE OU TOUT DOMMAGE DE QUELQUE NATURE QUE CE SOIT RÉSULTANT DE L'UTILISATION DE TOUT CONTENU OU DÉCOULANT D'UNE QUELCONQUE AUTRE MANIÈRE DE L'ACCÈS, DE L'AFFICHAGE OU DE L'UTILISATION DES SERVICES, QUE LA DEMANDE SOIT FONDÉE SUR LA THÉORIE DE LA NÉGLIGENCE OU SUR LA RESPONSABILITÉ CONTRACTUELLE, DÉLICTUELLE, OBJECTIVE OU SUR UNE BASE D'UNE AUTRE NATURE, ET CE, MÊME SI TRIPADVISOR OU SES PARTENAIRES AFFILIÉS ONT ÉTÉ INFORMÉS DE LA POSSIBILITÉ DE TELS DOMMAGES-INTÉRÊTS.

Si les Sociétés Tripadvisor étaient tenues pour responsables de dommages ou de pertes découlant de ou rattachés de quelque manière que ce soit à votre utilisation des Services, la responsabilité des Sociétés Tripadvisor ne pourrait en aucun cas être engagée pour un montant total supérieur au plus élevé des montants ci-dessous : (a) les frais de transaction payés aux Sociétés Tripadvisor pour les transactions à l'origine de la réclamation sur les Services ou par leur intermédiaire ; ou (b) cent dollars (100,00 USD).

La limitation de responsabilité reflète la répartition des risques entre les parties. Les limitations spécifiées dans cette section continueront à s'appliquer même si un quelconque recours limité précisé dans les présentes conditions a manqué à son objectif essentiel. Les limitations de responsabilité prévues dans les présentes conditions d'utilisation s'appliquent au bénéfice des Sociétés Tripadvisor.

LE PRÉSENT CONTRAT ET LES LIMITES DE RESPONSABILITÉ SUSMENTIONNÉES N'AFFECTENT PAS VOS DROITS LÉGAUX OBLIGATOIRES, LESQUELS NE PEUVENT ÊTRE EXCLUS EN VERTU DE LA LÉGISLATION APPLICABLE, PAR EXEMPLE EN VERTU DES LOIS SUR LA PROTECTION DES CONSOMMATEURS EN VIGUEUR DANS CERTAINS PAYS. AU SEIN DE L'UNION EUROPÉENNE ET DU ROYAUME-UNI, CELA INCLUT LA RESPONSABILITÉ EN CAS DE DÉCÈS OU DE DOMMAGES CORPORELS CAUSÉS PAR NOTRE NÉGLIGENCE OU CELLE DE NOS EMPLOYÉS, AGENTS OU SOUS-TRAITANTS, AINSI QU'EN CAS DE FRAUDE OU DE DÉCLARATION FRAUDULEUSE.

Si vous êtes un consommateur résidant au sein de l'Union européenne ou du Royaume-Uni, le présent Contrat n'exclut pas la responsabilité pour les pertes et les dommages provoqués par un manquement à l'obligation d'agir avec la diligence et les compétences raisonnables ou un manquement au présent Contrat, à condition que les dommages qui en résultent aient été prévisibles au moment où les obligations ont été établies.     

SI LA LÉGISLATION DU PAYS DE RÉSIDENCE N'AUTORISE AUCUNE LIMITATION OU EXCLUSION DE RESPONSABILITÉ PARTICULIÈRE PRÉVUE DANS CETTE CLAUSE, CETTE LIMITATION NE S'APPLIQUE PAS. LA CLAUSE DE NON-RESPONSABILITÉ S'APPLIQUERA DANS TOUTE LA MESURE PERMISE PAR VOTRE LÉGISLATION LOCALE.

**INDEMNISATION**

Cette section « Indemnisation » ne s'applique pas aux consommateurs résidant au sein de l'Union européenne ou du Royaume-Uni.

Vous acceptez de défendre et d'indemniser les Sociétés Tripadvisor, ainsi que leurs dirigeants, administrateurs, employés et agents respectifs, contre l'ensemble des réclamations, causes d'action, requêtes, instances, recouvrements, pertes, dommages, amendes, pénalités ou autres frais ou dépenses de quelque nature ou type que ce soit (y compris, mais sans s'y limiter, les frais de justice et de comptabilité raisonnables), occasionnés par des tiers résultant des éléments suivants

* (i) votre violation du présent Contrat ou des documents évoqués aux présentes ;
* (d) votre violation de toute loi ou des droits d'une tierce partie ; ou
* (iii) votre utilisation des Services, y compris les sites Web des Sociétés Tripadvisor.

**LIENS VERS DES SITES INTERNET TIERS**

Les Services peuvent contenir des hyperliens vers des sites Web exploités par des parties autres que les Sociétés Tripadvisor. De tels liens vous sont fournis à titre informatif. Les Sociétés Tripadvisor ne contrôlent pas ces sites Web et ne sont pas responsables de leur contenu ni de leur respect de la vie privée ni de toute autre pratique sur lesdits sites Web. En outre, il vous appartient de prendre les précautions nécessaires pour vous assurer que, quel que soit le lien que vous sélectionnez ou le logiciel que vous téléchargez (que ce soit à partir du présent Site Web ou de tout autre site Web), celui-ci ne contient aucun élément tel que des virus, vers, chevaux de Troie, défauts et autres éléments destructeurs. L'inclusion par les Sociétés Tripadvisor d'hyperliens vers ces sites Web n'implique nullement une approbation du contenu de ces sites Web ou applis tiers ou une relation quelconque avec leurs exploitants.

Dans certains cas, un site Web ou une application tiers peut vous demander d'associer votre profil Tripadvisor à un profil sur un autre site tiers. Le choix de le faire ou non est purement facultatif et relève de votre responsabilité. En outre, vous pouvez revenir sur votre décision à tout moment en désactivant cette fonctionnalité (auprès du site Web ou de l'application tiers). Si vous choisissez de lier votre compte Tripadvisor à un site ou une application tiers, le site ou l'application tiers pourra accéder aux informations que vous avez stockées sur votre compte Tripadvisor, y compris les informations concernant les autres utilisateurs avec lesquels vous partagez des informations. Vous devez lire les conditions générales et la politique de confidentialité des sites et applis tiers que vous consultez, dans la mesure où ils ont leurs propres règles et autorisations pour l'utilisation de vos informations, lesquelles peuvent différer de celles de nos Services, y compris de nos sites Web. Nous vous encourageons à consulter ces sites et applis tiers ; vous les utilisez à vos propres risques.

UTILISATION DE LOGICIELS EN TANT QU'ÉLÉMENTS DE SERVICE ; LICENCES SUPPLÉMENTAIRES POUR APPAREILS MOBILES

Les logiciels accessibles depuis les Services sont soumis aux réglementations d'exportation de l'administration américaine. Aucun logiciel des Services ne peut être téléchargé, exporté ou réexporté : (a) à Cuba, en Iraq, au Soudan, en Corée du Nord, en Iran, en Syrie ou dans un autre pays (ou à un ressortissant ou résident d'un de ces pays) pour lequel les États-Unis ont décrété un embargo ; ou (b) à quiconque figurant sur la liste des ressortissants spécifiquement désignés du département du Trésor des États-Unis ou sur le tableau des commandes refusées du département du commerce des États-Unis. En utilisant les Services, vous reconnaissez que vous n'habitez pas dans un tel pays figurant sur lesdites listes, que vous n'en dépendez pas, ou que vous n'en êtes ni un ressortissant ni un résident.

Comme indiqué ci-dessus, les Services comprennent des logiciels qui peuvent parfois être appelés « applis ».  Tout logiciel mis à disposition pour téléchargement depuis les Services (« Logiciels ») est l'œuvre protégée de Tripadvisor ou d'un tiers tel qu'il est identifié. Votre utilisation desdits Logiciels est régie par les conditions de l'accord de licence utilisateur final qui, le cas échéant, accompagne ou est inclus avec les Logiciels. Vous ne pouvez ni installer ni utiliser un Logiciel qui est accompagné par, ou comprend un accord de licence, à moins d'avoir tout d'abord accepté les conditions dudit accord. Concernant les Logiciels mis à disposition pour téléchargement par l'intermédiaire des Services sans accord de licence, nous vous concédons par les présentes à vous, utilisateur, une licence limitée, personnelle et non cessible vous permettant d'utiliser les Logiciels exclusivement aux fins de consultation et d'utilisation des Services conformément aux conditions générales du présent Contrat (y compris les politiques mentionnées dans les présentes) et à aucune autre fin.

Veuillez noter que tous les Logiciels, y compris, sans s'y limiter, tous les codes HTML, XML et Java ainsi que tous les contrôles Active X contenus dans les Services, sont la propriété de Tripadvisor et sont protégés par les lois sur les droits d'auteur et les dispositions des traités internationaux. Toute reproduction ou redistribution des Logiciels est expressément interdite et peut entraîner de lourdes sanctions civiles et pénales. Les contrevenants seront poursuivis en justice dans toute la mesure du possible.

Des parties du logiciel Tripadvisor pour mobiles peuvent utiliser des supports protégés par des droits d'auteur et dont l'utilisation est reconnue par Tripadvisor. En outre, des conditions particulières s'appliquent à l'utilisation de certaines applications Tripadvisor pour mobiles. Rendez-vous sur la page [Licences pour appareils mobiles](https://tripadvisor.mediaroom.com/BEFR-mobile-licenses) afin de consulter les avis propres aux applications mobiles de Tripadvisor.

SANS LIMITER LA PORTÉE DE CE QUI PRÉCÈDE, LA COPIE OU REPRODUCTION DES LOGICIELS VERS UN AUTRE SERVEUR OU EMPLACEMENT POUR UNE NOUVELLE REPRODUCTION OU REDISTRIBUTION EST STRICTEMENT INTERDITE. SOUS RÉSERVE DE LA LÉGISLATION EN VIGUEUR, LES LOGICIELS SONT UNIQUEMENT GARANTIS, LE CAS ÉCHÉANT, CONFORMÉMENT AUX CONDITIONS DU CONTRAT DE LICENCE OU DU PRÉSENT CONTRAT (SELON LE CAS).

**AVIS DE DROITS D'AUTEUR ET DE MARQUE DÉPOSÉE**

Tripadvisor, le logo en forme de hibou, les notes sous forme de bulles et tous les autres noms de produit ou service ou slogans affichés sur les Services sont des marques déposées et/ou de droit commun de Tripadvisor LLC et/ou ses prestataires ou concédants de licence; et ne peuvent pas être copiés, imités ou utilisés, en tout ou partie, sans l'autorisation écrite préalable de Tripadvisor ou du détenteur de la marque, le cas échéant. En outre, la présentation des Services, y compris nos sites Web, l'ensemble des en-têtes de page, graphiques personnalisés, icônes de touche et scripts qui s'y rapportent, sont la marque de service, commerce et/ou d'habillage de marque de Tripadvisor et ne peuvent être ni copiée, ni imitée ni utilisée, en tout ou partie, sans l'autorisation écrite préalable de Tripadvisor. Toutes les autres marques, marques déposées, noms de produits et noms de sociétés ou logos mentionnés dans les Services sont la propriété de leurs propriétaires respectifs. Sauf disposition contraire dans le présent Contrat, la référence à des produits, services, procédés ou autres informations, par nom commercial, marque de commerce, fabricant, prestataire ou autre ne constitue pas ou n'implique pas une approbation, un parrainage ou une recommandation par Tripadvisor.

Tous droits réservés. Tripadvisor décline toute responsabilité quant aux contenus des sites Web exploités par des parties autres que Tripadvisor.

**Politique de notification et de retrait en cas de contenu illicite**

Tripadvisor fonctionne selon un principe de « notification et retrait ». Si vous avez des plaintes ou des objections concernant du Contenu, y compris les messages des utilisateurs affichés sur les Services, ou si vous pensez que les documents ou contenus affichés sur les Services portent atteinte à un droit d'auteur que vous détenez, veuillez nous contacter immédiatement en suivant notre procédure de notification et de retrait. [**Cliquez ici pour afficher la politique et la procédure en matière de droit d'auteur**](https://fr.tripadvisor.be/StaticVelocityXmlPage-a_xml.noticetakedown__2E__xml). Une fois cette procédure suivie, Tripadvisor répondra aux plaintes valides et dûment justifiées et fera tous les efforts raisonnables pour supprimer les contenus manifestement illicites dans un délai acceptable.

**MODIFICATIONS APPORTÉES AU PRÉSENT CONTRAT ET AUX SERVICES ; MISES À JOUR ; RÉSILIATION ET RETRAIT**

**Le présent Contrat :**

Tripadvisor peut modifier, ajouter ou supprimer les conditions générales du présent Contrat ou toute partie de celui-ci, périodiquement et à sa seule discrétion (ou à sa discrétion raisonnable pour les consommateurs résidant au sein de l'Union européenne et du Royaume-Uni), lorsque cela est jugé nécessaire à des fins légales, réglementaires et techniques de portée générale, ou en raison de changements apportés dans les Services fournis ou dans leur nature ou configuration. Dès lors, vous acceptez expressément d'être lié par les conditions générales du présent Contrat telles qu'elles auront été modifiées.

**Modifications :**

Les Sociétés Tripadvisor peuvent modifier, suspendre ou interrompre tout aspect des Services à tout moment, y compris la disponibilité des fonctionnalités, des bases de données ou du Contenu des Services, à tout moment et sans frais pour vous, y compris :

(a)        pour assurer la conformité avec la législation applicable et/ou refléter les changements dans les lois et les exigences réglementaires pertinentes, telles que les droits obligatoires des consommateurs ;

(b)        pour effectuer une maintenance temporaire, corriger des bugs, mettre en œuvre des ajustements techniques, à des fins opérationnelles ou d'amélioration, comme l'adaptation des Services à un nouvel environnement technique, leur transfert vers une nouvelle plateforme d'hébergement ou la garantie de leur compatibilité avec les appareils et les logiciels (tels que mis à jour de temps à autre) ;

(c)        pour mettre à niveau ou modifier les Services, notamment en publiant de nouvelles versions des sites Web sur certains appareils ou en apportant des modifications aux caractéristiques et fonctionnalités existantes ;

(d)        pour modifier la structure, la conception ou la présentation des Services, y compris en changeant leur nom ou leur marque, ou en modifiant, améliorant et/ou étendant les caractéristiques et les fonctionnalités disponibles ;

(e)        pour des raisons de sécurité.

Pour les utilisateurs basés au sein de l'Union européenne et du Royaume-Uni : si nous apportons des modifications aux Services dont nous estimons raisonnablement qu'elles auront un impact négatif important sur votre accès à ces derniers ou sur leur utilisation, nous vous en aviserons au moins 30 jours à l'avance. Si vous ne résiliez pas votre contrat avec nous avant le déploiement de ces changements, nous considérerons que vous les acceptez.

**Mises à jour :**

Nous pouvons procéder à des mises à jour afin de maintenir les Services en conformité avec les droits des consommateurs applicables. Si les mises à jour automatiques sont activées dans vos paramètres, nous mettrons automatiquement à jour les Services. Vous pouvez modifier vos paramètres à tout moment si vous préférez ne plus recevoir de mises à jour automatiques.

Afin de bénéficier d'une expérience optimale et de garantir le bon fonctionnement des Services, nous vous recommandons d'accepter toutes les mises à jour qui vous seront communiquées au fur et à mesure de leur disponibilité. Ce qui peut également vous obliger à mettre à jour le système d'exploitation de votre appareil. À mesure que de nouveaux systèmes d'exploitation et appareils sont disponibles, nous pouvons cesser de prendre en charge les versions plus anciennes.

Si vous choisissez de ne pas télécharger ou ni installer les mises à jour, il se peut que les Services ne soient plus disponibles, qu'ils ne soient plus pris en charge ou qu'ils conservent leurs fonctionnalités antérieures.

Nous ne sommes pas responsables du fait que vous n'ayez pas téléchargé ou installé une mise à jour ou la version publiée la plus récente des Services afin de bénéficier de caractéristiques et/ou de fonctionnalités nouvelles ou améliorées et/ou de répondre à des exigences de compatibilité lorsque nous vous avons informé de la mise à jour, expliqué les conséquences de sa non-installation et fourni des instructions d'installation.

Toute modification des Services non conforme à la présente disposition « Mises à jour » sera soumise à la section « Modifications » ci-dessus.

**Résiliation :**

Les Sociétés Tripadvisor peuvent également imposer des limites ou restreindre d'une quelconque autre manière votre accès à tout ou partie des Services sans préavis et sans engager leur responsabilité pour des raisons techniques ou de sécurité, afin d'empêcher tout accès non autorisé, toute perte ou toute destruction de données, ou lorsque Tripadvisor et/ou ses partenaires affiliés considèrent à leur seule discrétion (ou à leur discrétion raisonnable pour les consommateurs résidant au sein de l'Union européenne et du Royaume-Uni) que vous enfreignez une disposition du présent Contrat, une loi ou une réglementation, et lorsque Tripadvisor et/ou ses partenaires affiliés décident de cesser la fourniture d'un des aspects des Services.

Tripadvisor peut résilier le Contrat qu'il a conclu avec vous à tout moment, sans préavis, lorsqu'il estime de bonne foi que vous l'avez enfreint ou lorsqu'il juge cette résiliation raisonnablement nécessaire pour protéger les droits des Sociétés Tripadvisor et/ou des autres utilisateurs des Services. En conséquence, nous sommes en droit de mettre fin à la fourniture des Services.

**LOI APPLICABLE ET JURIDICTION COMPÉTENTE**

Ce site Web appartient et est contrôlé par Tripadvisor LLC, une société américaine à responsabilité limitée. Le présent Contrat sera régi par les lois du Commonwealth du Massachusetts (États-Unis) et tout litige ou réclamation (y compris les litiges ou réclamations non contractuels) découlant de ou en rapport avec celui-ci, son objet ou sa formation sera interprété conformément auxdites lois. Vous consentez par les présentes à la compétence exclusive des tribunaux du Massachusetts, États-Unis, ainsi qu'à l'équité et la commodité des procédures de ces tribunaux pour tous les litiges, contractuels et non contractuels, découlant de ou liés à votre utilisation ou à celle d'un tiers des Services. Vous acceptez que toute réclamation que vous pourriez avoir à l'encontre de Tripadvisor LLC résultant de ou liée aux Services soit entendue et résolue dans une cour de juridiction compétente en la matière située dans le Commonwealth du Massachusetts. L'utilisation des Services n'est pas autorisée dans les juridictions ne donnant pas effet à toutes les dispositions des présentes conditions, y compris, sans s'y limiter, le présent paragraphe. Aucune disposition de la présente clause ne limitera le droit de Tripadvisor LLC d'engager des poursuites à votre encontre devant un quelconque autre tribunal compétent. Les précédentes dispositions ne sont pas applicables dans la mesure où la législation en vigueur dans votre pays de résidence exige l'application d'une autre loi et/ou juridiction, en particulier si vous utilisez les Services en tant que consommateur. Une telle disposition ne peut être exclue par contrat. En outre, le présent Contrat ne sera pas régi par la Convention des Nations Unies sur les contrats de vente internationale de marchandises, si celle-ci est applicable par ailleurs.  Si vous utilisez les Services en tant que consommateur, et non en tant qu'entreprise ou Représentant d'un établissement, vous êtes en droit de porter plainte contre Tripadvisor devant les tribunaux de votre pays de résidence. La présente clause s'appliquera dans toute la mesure permise dans votre pays de résidence.

**CONVERTISSEUR DE DEVISES**

Les taux de change sont établis en fonction de plusieurs sources accessibles au public et doivent être utilisés à titre indicatif uniquement. L'exactitude de ces taux n'est pas vérifiée, et les taux réels peuvent varier. Les cours des devises ne peuvent pas être mis à jour sur une base quotidienne. Les informations qui sont fournies sont réputées correctes, mais les Sociétés Tripadvisor ne sauraient en garantir l'exactitude. Lors de l'utilisation de ces informations à des fins financières, nous vous conseillons de consulter un professionnel qualifié pour vérifier l'exactitude des taux de change. Nous n'autorisons pas l'utilisation de ces informations à des fins autres que pour votre usage personnel et il vous est expressément interdit de les revendre, de les redistribuer et de les exploiter à des fins commerciales.

**DISPOSITIONS GÉNÉRALES**

Nous nous réservons le droit de récupérer tout nom d'utilisateur, nom de compte, surnom, pseudonyme ou tout autre identifiant d'utilisateur pour quelque raison que ce soit sans que cela n'engage notre responsabilité à votre égard.

Vous convenez qu'aucune relation de coentreprise, d'agence, de partenariat ou d'emploi n'existe entre vous et Tripadvisor et/ou de partenaires affiliés à la suite du présent Contrat ou de l'utilisation des Services.

Notre exécution du présent Contrat est sujette aux lois et procédures judiciaires en vigueur, et aucune clause du présent Contrat ne constitue une dérogation au droit de Viator de respecter la loi ou les décisions administratives et judiciaires relatives à votre utilisation des Services ou des informations fournies ou collectées par nos soins quant à cette utilisation. Dans la mesure permise par la loi applicable, vous convenez que vous engagerez toute action en justice ou réclamation découlant de ou liée à votre accès ou utilisation des Services dans les deux (2) ans à compter de la date de survenue du fait générateur de ladite action en justice ou réclamation ; à défaut, celle-ci sera irrévocablement abandonnée.

Si une partie quelconque du présent Contrat est jugée invalide ou inapplicable conformément à la loi en vigueur, y compris, mais sans s'y limiter, les rejets de garantie et limitations de responsabilité énoncées ci-dessus, alors la disposition invalide ou inapplicable sera réputée être remplacée par une disposition valable et exécutoire qui correspond le mieux à l'esprit de la disposition originale, et le reste du présent Contrat demeurera en vigueur.

Le présent Contrat (ainsi que l'ensemble des conditions mentionnées aux présentes) constitue l'intégralité de l'accord conclu entre vous et Tripadvisor concernant les Services et remplace toutes les communications et propositions antérieures ou contemporaines, qu'elles soient électroniques, orales ou écrites, entre vous et Tripadvisor relativement aux Services. Les parties reconnaissent à la version imprimée du présent Contrat et toute notification émise sous format électronique la même force probante, dans le cadre et pour les besoins de procédures judiciaires ou administratives en lien avec le présent Contrat, que celle reconnue aux contrats ou autres documents commerciaux établis et conservés sous format papier.

Les sections suivantes demeureront en vigueur après la résiliation du présent Contrat, le cas échéant :

* #### Produits supplémentaires
    
* #### Activités interdites
    
* #### Avis, commentaires et utilisation des autres zones interactives ; octroi de licence
    
    * #### Limitation des droits de licence de Tripadvisor
        
* Destinations de voyage
    * Voyage à l'international
* Limitations de responsabilité
* Indemnisation
* Utilisation logicielle dans le cadre des services ; licences supplémentaires pour appareils mobiles
* #### Avis de droits d'auteur et de marque commerciale
    
    * #### Politique de notification et de retrait en cas de contenu illicite
        
* #### ·Modifications apportées aux services ; résiliation
    
* #### Loi applicable et juridiction compétente
    
* #### Dispositions générales
    
* #### Service d'assistance
    

Les conditions générales du présent Contrat sont disponibles dans la langue des sites Web et/ou applis de Tripadvisor sur lesquels les Services sont accessibles.

Il se peut que les sites Web et/ou applis sur lesquels les Services sont accessibles ne soient pas mis à jour régulièrement ou périodiquement ; par conséquent, ils ne doivent pas être enregistrés comme produit éditorial en vertu des lois applicables.

Les noms fictifs des sociétés, produits, personnes, personnages et/ou données mentionnés sur les Services ou par leur intermédiaire ne sont pas destinés à représenter des individus, sociétés, produits ou événements réels.

Aucune disposition du présent Contrat n'est réputée conférer des droits ou avantages à des tiers, à l'exclusion des cas où les partenaires affiliés de Tripadvisor sont considérés expressément comme des bénéficiaires tiers du présent Contrat.

Il vous est interdit de transférer vos droits ou obligations au titre du présent Contrat sans notre consentement.

Tous les droits non expressément accordés dans les présentes sont réservés.

**SERVICE D'ASSISTANCE**

Pour obtenir des réponses à vos questions ou savoir comment nous contacter, consultez notre [centre d'aide](https://www.tripadvisorsupport.com/fr-FR/hc/traveler) ou envoyez-nous un e-mail à l'adresse [help@tripadvisor.com](mailto:help@tripadvisor.com). Ou vous pouvez nous écrire à :

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, États-Unis

Veuillez noter que Tripadvisor LLC n'accepte pas les notifications ou significations d'une procédure judiciaire par tout autre moyen qu'une copie papier envoyée par la poste à l'adresse ci-dessus, à moins que la législation en vigueur ne l'y contraigne.  Dès lors, afin d'écarter toute ambiguïté et sans que la portée de la présente disposition ne puisse être limitée de quelque manière que ce soit, nous n'acceptons pas les notifications ou significations d'une procédure judiciaire déposées auprès de l'un de nos partenaires affiliés ou l'une de nos filiales, sauf dans les cas décrits ci-dessous.

**CONTACT RELATIF AU RÈGLEMENT SUR LES SERVICES NUMÉRIQUES POUR LES UTILISATEURS**

Vous pouvez nous contacter directement en utilisant les coordonnées ci-dessous :

Adresse e-mail : [help@tripadvisor.com](mailto:help@tripadvisor.com)

**CONTACT RELATIF AU RÈGLEMENT SUR LES SERVICES NUMÉRIQUES ET REPRÉSENTANT LÉGAL POUR LES AUTORITÉS**

Si vous êtes une autorité nationale d'un État membre, la Commission européenne ou le Comité européen des services numériques, vous pouvez nous contacter directement en utilisant les coordonnées ci-dessous :

Adresse e-mail : [poc-dsa@tripadvisor.com](mailto:poc-dsa@tripadvisor.com)

Les signaleurs de confiance et les organismes professionnels peuvent également nous contacter en utilisant cette adresse e-mail.

Les autorités publiques peuvent également contacter Tripadvisor Ireland Limited, qui est notre représentant légal aux fins du règlement sur les services numériques, en utilisant les coordonnées ci-dessous :

Adresse : 70 Sir John Rogerson's Quay, Dublin 2, Irlande

Adresse e-mail : [dsa.notifications@tripadvisor.com](mailto:dsa.notifications@tripadvisor.com)

Numéro de téléphone : +35315126124

Veuillez uniquement utiliser ces coordonnées si vous souhaitez joindre notre représentant légal de la part de l'un des organismes publics mentionnés ci-dessus. À défaut, Tripadvisor ne répondra pas à votre demande. Si vous avez des questions concernant la plateforme ou si vous souhaitez nous adresser une plainte, veuillez nous contacter en utilisant les coordonnées indiquées dans les sections « SERVICE D'ASSISTANCE » ou « CONTACT RELATIF AU RÈGLEMENT SUR LES SERVICES NUMÉRIQUES POUR LES UTILISATEURS » ci-dessus.

©2024 Tripadvisor LLC. Tous droits réservés.

Dernière mise à jour : 16 février 2024

* [](#print "print")
* [](https://twitter.com/share?url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34416%26item%3D32407 "Twitter Share")
* [](https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34416%26item%3D32407 "Facebook Share")
* [](https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34416%26item%3D32407 "Linkedin Share")
* [](#email "email")