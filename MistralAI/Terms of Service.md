Terms of Use
============

Preamble
--------

**Welcome to Mistral!** Our platform provides access to Mistral AI’s artificial intelligence models tailored for text generation and other Services for individuals, developers and businesses.

**Our Services.** By creating an Account on Our platform, You can access:

* **“La Plateforme”**, which is Our portable developer platform. La Plateforme serves our open and optimized Mistral AI Models through a set of APIs, for building fast and intelligent applications. La Plateforme also gives You access to specific AI tools such as our fine-tuning API and our agent builder.
* **“Le Chat”**, which is Our conversational assistant allowing You to directly interact with Mistral AI Models.

**Parties.** These Terms of Use are entered into by and between Mistral AI, a French simplified joint-stock company, registered at the Trade register of Paris under number 952 418 325, and having its corporate seat at 15 rue des Halles 75001, Paris, France (**“Mistral AI”** or **“Us”**, also referred to with the possessive adjective **“Our”**) and any person who uses, accesses or subscribes to the Services (**“You”** or the **“Customer”**).

**What these Terms of Use cover.** These Terms of Use apply to both La Plateforme and Le Chat. In addition, (i) Your use of some of our Services is subject to Additional Terms and (ii) if You are a Commercial Customer, Your use of our Services is also subject to the [Data Processing Agreement](https://mistral.ai/terms/#data-processing-agreement).

**What these Terms of Use do not cover.** These Terms of Use do not apply to Your use of Our Models through a Cloud Provider, which is subject to the [Partner Hosted Deployment Terms](https://mistral.ai/terms/#partner-hosted-deployment-terms).

Definitions
-----------

The capitalized words in this document shall have the meaning set forth below:

* **“Account”:** means Your account on Our Services which can be either:
    
    * An **“Admin Account":** means an Account that has full access to all features and settings, allowing the Administrator to manage and configure the Services; or
    * A **“Standard Account"**: means an Account that has limited access to the Services, providing the User with the functionalities and permissions defined by the Administrator.
* **“Additional Terms”**: means the additional terms for a Service that govern your use of that Service.
    
* **“Administrator”:** means any User that has an Admin Account.
    
* **“Agreement”:** means collectively (i) these Terms of Use, (ii) the Additional Terms, (iii) where applicable, the Data Processing Agreement, and (iv) all materials referred or linked to in the above mentioned documents, as may be amended from time to time.
    
* **“Applicable Data Protection Law”:** means any applicable national, federal, EU, state, provincial or other privacy, data security, or data protection law or regulation, including, to the extent applicable, Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 applicable since 25 May 2018 (the **“GDPR”**).
    
* **“Authorized Users”**: means a Commercial Customer’s employees and/or independent contractors, allowed by the Commercial Customer to access and use the Services, subject to the Authorized Users’ compliance with this Agreement.
    
* **“Beta Services”**: means any Services made available to You for beta testing purposes.
    
* **“Billing Cycle”**: means the frequency at which You are billed for the Fees, based on Your Subscription Plan.
    
* **“Commercial Customer”**: means any Customer subscribing, accessing to or using the Services as part of its business or professional operations and that does not act as a Consumer.
    
* **“Consumer”**: means any Customer who is acting for purposes which are outside their trade, business, craft or profession.
    
* **“Customer Offerings”:** means any of Your products or services, including but not limited to any applications edited or operated by You, that You make available to End-Users using Our Services or Your Outputs.
    
* **“Data Processing Agreement”**: means the data processing agreement entered by and between the Commercial Customer and Mistral AI and available [here](https://mistral.ai/terms/#data-processing-agreement).
    
* **“Effective Date”:** means the earlier of (i) the date You first use the Services or (ii) the date You accept these Terms of Use.
    
* **“End-Users”**: means any person who uses the Customer Offerings.
    
* **“Feedback”**: means Your feedback pertaining to the accuracy, relevance, and effectiveness of the Outputs, including any identified discrepancies or errors.
    
* **“Fees”**: means the fees paid or payable by You to Mistral AI under this Agreement in consideration for the Paid Services.
    
* **“Filters”**: means the automatic mechanisms implemented by Mistral AI that are designed to screen or remove offensive, inappropriate, or illegal content from the Output, such as moderation prompts.
    
* **“Help Center”**: means (i) the Mistral AI help center available at [https://help.mistral.ai/en/](https://help.mistral.ai/en/) or through your Account settings, and (ii) the ticketing portal allowing You to make and manage Your Customer Support requests.
    
* **“Input”**: means any data You provide or use to prompt, fine-tune or customize the Services, including textual data, prompts, fine-tuning data, documents, or images.
    
* **“Intellectual Property”**: means all patent rights, copyrights, trademark rights, rights in trade secrets, design rights, database rights, domain name rights, moral rights, and any other intellectual property rights (registered or unregistered) throughout the world. Mistral AI’s Intellectual Property includes the Models, the Services, and any modification or enhancement of each of the foregoing (the “**Mistral AI Intellectual Property**”).
    
* **“Model(s)”**: means (i) any version of any artificial intelligence model developed by Mistral AI and made accessible to You in any manner on the Services, (ii) any fine-tuned or modified version of such artificial intelligence model (the **“Fine-Tuned Model”**) and (iii) the associated documentation as may be updated from time to time (the **“Documentation”**).
    
* **“Output”**: means any and all content generated by the Services in response to an Input. For the avoidance of doubt, Output does not include any component or derivative of the Models, including the Model weights or parameters and any Fine-Tuned Model.
    
* **“Parties”**: means You and Mistral AI. In the singular, **“Party”** means one of the Parties. “Payment Services”: means the online payment services that We use to enable You to pay the Fees on the Services.
    
* **“Services”**: means any and all services provided by Mistral AI to You under this Agreement, available via the website accessible at [https://www.mistral.ai](https://www.mistral.ai/), including La Plateforme and Le Chat (as defined in their respective Additional Terms). The Services may be free of charge (the **“Free Services”**) or made available subject to Fees (the **“Paid Services”**).
    
* **“Subscription”**: means Your subscription to the Services.
    
* **“Subscription Plan”**: means the specific set of Services You subscribe to. The available Subscription Plans are mentioned on the Services and may be amended from time to time by Mistral AI at its sole discretion.
    
* **“Support”**: means the support Services provided by Mistral AI to the Customer under this Agreement.
    
* **“Term”**: means the term of this Agreement as set forth in Section 13.1 (Term) of these Terms of Use.
    
* **“Terms of Use”**: means these terms of use.
    
* **“User Data”**: means Your Feedback, Input and Output.
    
* **“Workspace”** : means the Commercial Customers’ workspace on the Services which Authorized Users can access, subject to the permissions set out by the Commercial Customer.
    

2\. Purpose and scope
---------------------

### 2.1. Purpose

**Purpose.** The purpose of this Agreement is to describe the rights and responsibilities of the Parties in connection with the provision and use of the Services.

### 2.2. Scope and contractual documents

**Scope.** These Terms of Use apply to any Subscription, access to or use of the Services by You.

**Additional Terms.** Some Services may be subject to Additional Terms specific to that Service. By accessing or using a Service covered by the Additional Terms, You also agree to such Additional Terms.

**Hierarchy.** In case of any conflicts or discrepancies between these Terms of Use and the Additional Terms, the applicable Additional Terms shall prevail.

3\. Acceptance
--------------

**General Principle.** By accessing or using any of Our Services, You accept this Agreement. You must read this Agreement carefully before using any of the Services. We recommend You download this Agreement, print it, and keep a copy. By clicking on “I agree” (or any similar button or checkbox) at the time You sign up for a Service or by using Our Services, You expressly agree to be bound by this Agreement.

**Agreement on behalf of another person.** If You agree to be bound by this Agreement on behalf of Your employer or another legal entity, You warrant and represent that You possess the authority to act and accept such terms on their behalf. In such a case, the words “You” or “Customer” in this Agreement will refer to Your employer or that other legal entity.

4\. Access to Our Services
--------------------------

### 4.1. What You need to access and use Our Services

**Age limitation.** You must be at least thirteen (13) years old to use Our Services. You must have parental or legal guardian permission if You are a minor registering for Our Services. We will promptly delete any Account found to be in violation of such requirements.

**Technical requirements.** You need a computer (or any compatible electronic device) and a high-speed internet connection to access and/or use Our Services. Depending on the Services You use, some additional technical requirements might be required, as further described in the Documentation and/or in the applicable Additional Terms. You are responsible for ensuring You comply with the foregoing technical requirements to utilize the Services effectively.

**Costs.** Unless otherwise stated, You are solely responsible for any costs that you incur in complying with the technical requirements.

### 4.2. Your Account

**Account creation.** You must create an Account to use the Services. In creating your Account, You must provide complete and accurate information to Mistral AI and promptly update the information on Your Account if any changes occur.

**Type of accounts.** We offer two types of accounts: (i) Admin Accounts and (ii) Standard Accounts.

#### 4.2.1. Admin Accounts

**Admin Accounts.** Subject to Your Subscription Plan, Admin Accounts are granted additional features compared to Standard Accounts, including:

* **Inviting Authorized Users:** Administrators may invite Authorized Users to join their Workspace by providing their email addresses. Upon receipt of an invitation, the Authorized Individual will be able to create an Account to access and use the Services.
* **Automatically adding Authorized Users:** Administrators may configure the Services to automatically add Authorized Individuals to their Workspace based on the domain name of their email addresses. For example, if an Administrator specifies that all email addresses ending in “@example.com” should be automatically added, any individual with an email address ending in “@example.com” who attempts to create an Account will be automatically added to the Administrator’s Workspace.
* **Managing Accounts within Your Workspace:** Administrators can manage their Workspace’s Accounts, including deleting such Accounts or changing Account permissions.

**Deletion of Your Admin Account.** You can ask Us to delete Your Admin Account at [support@mistral.ai](mailto:support@mistral.ai), subject to Section 13 (Term, Suspension and Termination) of these Terms of Use or the applicable Additional Terms. Unless otherwise approved by Mistral AI, the deletion of an Admin Account will result in the deletion of the associated Workspace and all related Accounts.

#### 4.2.2. Standard Accounts

**Deactivation by You.** Subject to the permissions given by Your Administrator (if You have one), You may delete Your Standard Account at any time by using the “Delete My Account” feature for the Services. If You subscribed to Paid Services, You may only delete Your Account after paying all outstanding Fees.

#### 4.2.3. Provisions applicable to every Account

**Restrictions.** Your Account is intended for Your individual use only. Unless otherwise stated, You may not share Your Account with any other person without Mistral AI’s prior written authorization. You represent and warrant that You will not create (a) any fake Account, (b) more than one (1) Account per individual User or (c) an Account on behalf of another individual or entity without such individual’s or entity’s prior authorization.

**Account security.** You must keep Your Account identifiers (email and password) secure and strictly confidential. Mistral AI does not commit to monitoring which individual is using Your Account. You are solely responsible for any action taken in connection with Your Account, whether that action was actually taken by You or not. You must notify Mistral AI at [support@mistral.ai](mailto:support@mistral.ai) or through the Help Center in the event of any (suspected or confirmed) unauthorized or fraudulent use of Your Account as soon as You become aware of such events.

**Suspension or deactivation by Mistral AI.** Mistral AI reserves the right to suspend or deactivate Your Account and/or Your Workspace if Mistral AI suspects or determines that Your Account may have been used for an unauthorized purpose. Suspension of Your Account is subject to Section 14 (Term, Suspension and Termination) of these Terms of Use.

### 4.3. Subscription to the Services

#### 4.3.1. Free Services

**Subscription Process**. To access and use the Free Services, You must:

* Create Your Account on the Platform in compliance with the Terms of Use,
* Read this Agreement, and
* Accept this Agreement.

#### 4.3.2. Paid Services

**Subscription Process**. To subscribe to the Paid Services, You must:

* Create Your Account, in compliance with the Terms of Use,
* Choose Your Subscription Plan,
* Choose Your payment method. If You choose to pay the Fees via credit card, an imprint of its credit card may be taken to verify the validity of the card. This process is solely for verification purposes and does not result in any immediate charges unless explicitly stated, Provide Your payment and billing information by filling in the form available on the platform. You must provide accurate and up-to-date payment and billing information. You shall promptly update Your payment and/or billing information in order to keep such information accurate and up-to-date. This change can be made through the Your Account,
* Read this Agreement,
* Accept this Agreement,
* Review Your Subscription. During this step, You will be able to modify Your Subscription Plan if necessary,
* Confirm Your Subscription, and
* Depending on Your Subscription Plan, You may be required to pay for the Fees for the initial subscription month, as outlined in the applicable Additional Terms.

**Confirmation of the Subscription**. Once the Subscription process is complete, a page will appear on the Platform to confirm Your Subscription. Mistral AI will send You a confirmation of Your Subscription at the email address provided by You when creating Your Account (unless such email address is not valid), alongside with a PDF copy of this Agreement. This confirmation does not constitute an invoice. You will be able to access and use the Paid Services once You have received such confirmation.

**Subscription verification process**. Mistral AI and/or, where applicable, the Payment Services Provider will verify the information provided by You during the Subscription process. Mistral AI reserves the right to decline the Your Subscription to the Paid Services in case of any suspected OFAC regulation violation or instances of fraud and/or misrepresentation. In such cases, (i) Mistral AI will notify You by email, (ii) Mistral AI will refund You of any pre-paid Fees under this Agreement and (iii) this Agreement will automatically terminate.

**Change Subscription Plan**. If You wish to access a Model or a Service that is not included in Your current Subscription Plan, You must subscribe to the relevant Subscription Plan to gain access.

5\. Your use of Our Services
----------------------------

### 5.1. Description of Our Services

**Our Services.** Our Services allow You to use, fine-tune or customize Our Model(s) to generate Outputs. You can access the full list and description of the Services on our platform and/or in the Documentation, as may be amended from time to time. Any User can access the Free Services free of charge. Access to the Paid Services is subject to payment of the applicable Fees.

**Restrictions.** Certain Services may be subject to any of the following restrictions:

* **Access Restrictions.** Certain Services may not be available to all Users, either because they are Paid Services or because access is to specific categories of Users, such as Commercial Customers. These Services are described on the platform and/or the applicable Documentation.
    
* **Service restrictions.** Certain Services may be subject to specific rate limits or usage restrictions. Such restrictions are set forth in the Additional Terms and/or in the applicable Documentation.
    
* \*\* Input Restrictions.\*\* For certain Services, Your Input may be subject to specific format or size restrictions. Any such restrictions are clearly specified on the Services at the time You use them and in the Documentation. We strongly advise You to review the Documentation for detailed information regarding these restrictions. You acknowledge and agree that non-compliance with the Input restrictions may result in Your inability to submit Your Input Data, thereby rendering the Services unable to perform as intended.
    

\*\* Evolution of the Services.\*\* Subject to the applicable Additional Terms, We reserve the right to modify, update or enhance Our Services without notice. Such modifications may include, but are not limited to, debugging, feature additions or removal, enhancements or alterations to improve Service quality. You agree that We are not obligated to maintain or provide prior features, functionalities or Services following modifications, updates or upgrades, unless otherwise stated.

**Model discontinuation.** We reserve the right, at Our sole discretion, to discontinue the availability of Models on Le Chat or La Plateforme. In the event that We elect to discontinue the availability of any Model, We shall provide a minimum notice period of six (6) months prior to the discontinuation of the Model at the email address associated with Your Account. During such notice period, the Model will not be available to any new Customers. We will notify You of any such discontinuation by email to the email address associated with Your Account. You are responsible for ensuring that Your contact information is up-to-date to receive such notifications. We are not liable for any damages, costs, or expenses incurred by You as a result of the discontinuation of any Model, provided that We have complied with the notice periods set forth above. In any case, You will be able to terminate the Agreement at any time in accordance with the applicable Additional Terms.

**Beta Services.** We may, from time to time, offer Beta Services. Beta Services will be mentioned as such. Beta Services are provided “as is” and “as available” without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose, and non-infringement. We do not warrant that the Beta Services will be uninterrupted, error-free, or free of harmful components. By using the Beta Services, you acknowledge and agree that:

* \*\* No guarantee of availability:\*\* We may discontinue, modify, or limit the availability of the Beta Services at any time without notice.
* \*\* No liability:\*\* We shall not be liable for any damages, losses, or costs arising from or related to Your use of the Beta Services.
* \*\* Feedback;\*\* You may be asked to provide feedback regarding the Beta Services. We shall own all rights, title, and interest in and to any feedback you provide, and you hereby assign to Us all rights in such feedback.

### 5.2. Availability

**Availability.** Mistral AI will use commercially reasonable efforts to make the Services available to You 24 hours a day, 7 days a week. However, You acknowledge that, due to various factors, including maintenance, technical issues or unforeseen circumstances, the Services may experience periods of downtime. Mistral AI will not be liable for any unavailability of our Services, but will make commercially reasonable efforts to restore the Services promptly. You understand and agree that the occurrence of downtime, regardless of its duration, does not entitle You to a refund of any Fees paid for the Paid Services. You shall however note that Our Services are subject to changes, in which case We will use commercially reasonable efforts to preserve backward compatibility without however guaranteeing the absence of breaking changes.

**Exclusions.** In any case, Mistral AI will not be liable for any unavailability of Our Services caused by:

* Factors beyond Mistral AI’s reasonable control, including any Force Majeure Event, operational difficulties or unforeseen and unintentional temporary interruptions of its services, including but not limited to interruptions of electricity, computer services or telecommunications, or Internet access issues,
* Poor transmission due to a failure or malfunction of the transmission networks, including when such transmission is provided by a third-party supplier;
* Breach of this Agreement by You or the Authorized Users,
* Your non-compliance with the Documentation and/or Your unlawful or unauthorized use of Our Services,
* Failure of any third party hardware, software, or technology,
* Scheduled maintenance work, provided that Mistral AI has duly notified You of such maintenance work in advance or
* Suspension of Your right to use Our Services.

### 5.3. Support

#### 5.3.1. Free Services

**Customer support**. You can submit Your support requests via the Help Center.

#### 5.3.2. Paid Services

**Customer support.** You can submit Your support requests via the Help Center. We will use commercially reasonable efforts to assist you in the effective use of the Service in a timely manner. This includes responding to your inquiries and addressing any issues promptly and professionally. However, You acknowledge that we may prioritize customer support requests in our sole discretion, including prioritizing customer support requests relating to Paid Services. Our support team is available during standard business hours.

**Customer Support exclusions.** Customer Support does not cover:

* Inquiries addressed in the Documentation;
* Initial training for the Services. You are responsible for training Your Authorized Users;
* Issues arising from third-party services, software, hardware, or products or services not provided by Mistral AI;
* Issues resulting from Your misuse, unlawful, or unauthorized use of the Services; or
* Support during periods when You have not paid the applicable Fees.

**Adaptive Maintenance.** Mistral AI may perform adaptive maintenance to keep the Services aligned with current technological and regulatory standards. You acknowledge and agree that:

* This Agreement does not automatically include upgrades and Mistral AI has no obligation to provide Upgrades or improve the Services; and
* Upgrades may require changes to Your Subscription Plan.

6\. Your User Data
------------------

**Responsibility.** You are solely responsible for Your use of Your Input and Output.

**Input ownership**. You represent that You own or have obtained all the required rights to use Your Input. You retain all the rights, including but not limited to all intellectual property rights in and to Your Input. We do not claim any ownership or intellectual property rights of any kind in and to the Input.

**Output ownership**.We do not claim any ownership or intellectual property rights of any kind in and to the Output. You are the sole owner of the Output and retain all the rights, including but not limited to all intellectual property rights, in and to Your Output. For the avoidance of doubt, Output does not include any third-party content, such as thumbnails or snippets, displayed as a source when you use our web-search features on Le Chat (the “Third-Party Content”). Any use of such Third-Party Content is subject to the Additional Terms for Le Chat.

**Output restriction**. You must not use the Outputs or any modified or derived version of the Outputs to reverse engineer the Services.

**Output similarity**. You agree that, due to the nature of Our Services, if another User uses an Input similar to Yours, our Services may generate an Output similar or identical to Yours. We do not warrant that Your Output is not similar or identical to another User’s Output. Accordingly, We shall not be obligated to indemnify You against any claim alleging that Your Output is similar to or identical with another User’s Output.

**Output accuracy**. You agree that the Outputs generated by Our Services may be incomplete, not up-to-date or not entirely accurate. Therefore, when using Our Services, You must:

* Assess the accuracy and/or the suitability of the Output to your needs before using or sharing the Output,
    
* Include in Your Input any relevant moderation prompt to better filter or adapt the Output, especially if You deactivated the Filters proposed by Mistral AI, and
    
* Check the information generated by the Output and, in any case, not rely on the Output as a unique source of truth and/or information, as safe and inoffensive in every circumstance, or as a replacement for professional guidance.
    

**Output Moderation**. We make commercially reasonable efforts to make sure Our Services do not generate Outputs that contain offensive, inappropriate or illicit content. To this end, We have implemented and / or are proposing different moderation mechanisms such as Filters to Our Model(s). Subject to Your Subscription Plan and to the option being available, You may deactivate these Filters for legitimate purposes pertaining to Your specific Use-Case. You agree that (a) We do not warrant that the Output generated will not be offensive, inappropriate or illicit, (b) You are solely responsible for the use of Your Output and (c) You shall in no way use the Output for any illicit or unlawful purpose and/or to harm Mistral AI and/or a third party.

**How we use Your User Data.** If You use La Plateforme, the way we use Your User Data is set forth in the Additional Terms for La Plateforme. If You use Le Chat, the way we use Your User Data is set forth in the Additional Terms for Le Chat.

7\. Fees, billing and payment
-----------------------------

**General Principle**. Specific payment terms may apply depending on the Services Your Subscribe to. Please make sure to check the applicable Additional Terms to know more about the applicable payment terms before Subscribing to a Service.

### 7.1. Fees

**Applicable Fees**. The Fees are listed on the platform inclusive of all Taxes. You must pay to Mistral AI the Fees listed in Mistral AI’s then-current price list available on the platform. Unless otherwise stated and to the extent permitted by applicable law, all amounts paid by You are non-refundable and non-cancellable.

**Pricing evolution**. Mistral AI may modify the price list at its own discretion. If such modification results in an increase of the applicable Fees for You, Mistral AI will provide You at least thirty (30) days’ notice in writing to the email address You provided upon Your Subscription. If You do not agree with such modification, You may terminate this Agreement before the end of this notice period. The Fees will not be increased during this notice period.

### 7.2. Billing

**Payment method.** You must pay the Fees via credit card or any other payment method available on the Services.

**Frequency.** You authorize Mistral AI and/or the Payment Service Provider to charge Your select payment method every month.

**Payment Services.** The payment services allowing the Customer to pay the applicable Fees are provided by the Payment Services Provider under its sole control and responsibility. Mistral AI is not responsible for the payment services provided by the Payment Services Provider.

**Billing.** Billing shall occur simultaneously as payment.

### 7.3. Suspension or termination

**Suspension or termination.** We reserve the right to suspend or terminate Your access to the Services in case of late-payment or non-payment under the conditions set forth in Section 13 (Term, Suspension and Termination) of these Terms of Use.

### 7.4. Provisions applicable to Commercial Customers

**Late payments**. The Commercial Customer may not withhold any amounts due under this Agreement. Any late payment will (i) be increased by a fixed indemnity of fourty (40) euros and the costs of collection (if any) and (ii) will incur a late payment interest rate of three (3) times the legal interest rate per day, starting from the day after the payment due date until full payment is received. For the purpose of this Agreement and subject to applicable law, the legal interest rate means the interest rate applied by the European Central Bank to its most recent refinancing operation, plus 10 percentage points.

**Taxes**. The Commercial Customer is responsible for settling any applicable Taxes that may be levied on top of the Fees and must pay Mistral AI for the Services without any deductions related to Taxes. If Mistral AI is required to collect or pay any Taxes, they will be invoiced to the Commercial Customer, who is obligated to settle them unless a valid tax exemption certificate regarding these Taxes is timely provided to Mistral AI. If the Commercial Customer is obligated by law to withhold Taxes from any payments under this Agreement, the Commercial Customer agrees to increase the payment amount to ensure that Mistral AI receives the full agreed-upon Fees notwithstanding these deductions. The Commercial Customer will be solely responsible for remitting the withheld amounts to the relevant authorities. The Commercial Customer shall provide Mistral AI with all pertinent tax identification information that Mistral AI may require under the applicable law to ensure compliance with prevailing tax regulations and the authorities of relevant jurisdictions. The Commercial Customer agrees to settle any potential interests, penalties, taxes, or fines resulting from the Commercial Customer’s failure to declare, or reimburse Mistral AI for such amounts.

8\. Your obligations
--------------------

**General obligations.** You shall use Our Services in compliance with this Agreement and all applicable laws and regulations, including but not limited to all intellectual property, data and privacy laws. Therefore, You are responsible for:

* **The information You provide Us.** You must provide honest, loyal, complete, accurate and up-to-date information to Mistral AI and promptly update such information as need may be.
    
* **Your use of Your Account and Our Services.** You must:
    
* Not use Our Services for any illicit, unlawful, prohibited and/or illegal purposes, to harm third parties or Mistral AI;
    
* Not use the Services for a benefit of a third party unless agreed otherwise in a separate contract with Us;
    
* Not use the Services to circumvent the intended features, functionality or limitations of the Services or to divert Our Services from their intended purposes as set forth in this Agreement; .
    
* Not interfere with or circumvent mechanisms in the Services intended to monitor Your use of Our Services or to limit such use, except as provided for in Section 7 (Your User Data) of these Terms of Use;
    
* Not use the Services in a manner in which, in Mistral AI’s opinion, would affect Mistral AI’s reputation or goodwill or any of its trademarks.
    
* **Your User Data.** You must:
    
    * Ensure that Your Input and Output do not violate this Agreement, the applicable law or the rights of third-parties, including but not limited to intellectual property rights, privacy or personality rights;
    * Only use Input to which Your own all required rights under applicable law and do so in a manner that is consistent with the applicable law;
    * Not use any Input that contains illegal content, including but not limited to child sexual abuse material;
    * Not intentionally (i) make the Services generate Outputs infringing the rights of third-parties, including but not limited to intellectual property rights, privacy or personality rights, or applicable law, or (ii) use such infringing Outputs after You become aware of such infringement; and
    * Not represent that the Output was generated by a human when it was generated by Our Services.
* **Not compromising the security of Our Services.** You must:
    
    * Not interfere with, circumvent or bypass mechanisms in the Services intended to ensure the security of our Services;
    * Not attempt to or engage in any activities that could compromise the security, moderation or proper functioning of the Services. Specifically, the Customer shall refrain from any attempt to inject malicious Input or carry-out Input injection attacks with the intent of manipulating the behavior of the Model;
    * Not attempt to or take any actions that may result in unauthorized access, interference or disruption of the Services’ operation;
    * Not disable, overly burden, impair or otherwise interfere with servers or networks connected with Our Services;
    * Not disseminate data that would diminish, disorganize, slow down or interrupt the normal functioning of Our Services;
    * Not attempt to gain unauthorized access to, interfere with, damage or disrupt the Mistral AI, or Mistral AI’s licensor or service providers’ computer systems or networks connected thereto;
    * Not use, store, transmit or disseminate data that contains any computer viruses, worms, malicious code, or any software intended to damage or alter a computer system or data; and
    * Not perform any vulnerability, penetration or similar testing of Our Services.
* **Not infringing Mistral AI’s or Mistral AI’s licensors, suppliers and service providers’ Intellectual Property.** Subject to applicable law, You must:
    
    * Not remove or otherwise obscure any copyright or proprietary notices on the Services, including but not limited to Our brands, trademarks or any other copyright notice;
    * Not bypass, attempt to bypass, alter, disable or in any way interfere with the digital rights management measures that may be integrated to the Services. You acknowledge that these protection mechanisms are essential for safeguarding the intellectual property and security of the Services;
    * Not incorporate Our Services into Your Customer Offerings, unless otherwise stated;
    * Not extract, by permanent or temporary transfer, all or part of the contents of Our Services, by any means and under any form whatsoever, including by scraping, except as otherwise authorized under the terms of this Agreement;
    * Not merge or combine the Services with any software, programme or technology, except as otherwise authorized under this Agreement;
    * Not copy, reproduce, transcode, adapt, translate, arrange, decompile, modify or create any derivative works of the Services except as expressly authorized by applicable law;
    * Not seek to reverse engineer or reverse engineer, disassemble, decompile, translate or otherwise seek to obtain or derive the source code, underlying ideas, algorithms, file formats or non-public APIs to any Services, except to the extent expressly permitted by applicable law (and then only upon advance notice to Us); and
    * Not grant a license, sub-licence or access to or sell, re-sell lend, lease or distribute, in any form whatsoever, the Services to third parties, unless otherwise permitted under this Agreement

**Third parties.** You shall not encourage or assist any other User or third party in doing anything that is strictly prohibited under this Agreement.

**Obligations specific to Commercial Customers**. When using the Services, You must:

* Comply with the Applicable Data Protection Law.
    * Provide a disclaimer to any individual accessing an Output or using the Services. Such disclaimer should highlight the potential inaccuracies and unpredictabilities in the Outputs and encourage individuals to check important information,
    * Document the use of the Services as deployer of the Models (where applicable);
    * Supervise the Authorized Users’ use of the Services. Accordingly, the Commercial Customer is solely responsible for:
    * Ensuring that all individual users, including the Authorized Users, are contractually bound to terms and conditions with the Commercial Customer that are as protective of Mistral AI’s rights as outlined in this Agreement,
    * Informing the Authorized Users about the proper use of the Services, including the guidelines, restrictions and usage limitations, and
    * Providing adequate training and educational resources to the Authorized Users, ensuring their understanding of the Services proper and responsible use, as well as compliance with the specified terms.
    * Ensure that all individuals added to their Workspace, whether through invitation or automatic addition, comply with the applicable provisions of this Agreement and any applicable laws and regulations.

9\. Our obligations
-------------------

**General obligations.** Subject to Your compliance with this Agreement, Mistral AI will use commercially reasonable efforts to make the Services accessible 24 hours a day, 7 days a week. However, Mistral AI reserves the right to suspend the availability of all or part of the Services (a) in case of a Force Majeure Event (as defined in Section 18 of these Terms of Use), (b) for maintenance purposes, including but not limited to, to fix anomalies, bugs or errors, to launch new or improved features or Services, or to address immediate security concerns. Unless otherwise stated, Mistral AI is not under an absolute obligation of any kind. Mistral AI shall not be held responsible for any disruptions, interruptions and/or anomalies that are not of its making and that affect, for example, transmissions via the Internet network and more generally via the communication network, whatever the extent and duration.

**Security.** Mistral AI will use commercially reasonable efforts to implement and maintain reasonable security measures to prevent unauthorized access to the Services, as well as fraudulent destruction, loss, or alteration of data. These security measures may include, but are not limited to, the use of firewalls, encryption, strict access controls, regular backups, and security protocols compliant with good industry standards.

**Illicit content.** You have the availability to report to Mistral AI any User Data that (a) incites hate, violence, or discrimination against individuals based on their origin, ethnicity, religion, gender, sexual orientation, etc. (b) glorifies sexual harassment, (c) violates human dignity (e.g., human trafficking or pimping), (d) glorifies crimes against humanity or denying their existence, (e) incites terrorism, (f) glorifies very serious crimes against individuals (e.g., murder or sexual assault) (g) glorifies crimes involving theft, extortion, or material damage posing a danger to individuals (h) are of a pedophilic nature (i) are dangerous for minors. You can report such content by using the report feature on the Services and/or by sending an email at [support@mistral.ai](mailto:support@mistral.ai). You grant Us the right to access the reported content to improve Our Services (e.g. to help Us make sure that the Model does not generate illicit content). This right is granted worldwide and for the duration of the intellectual property rights under applicable law. You must delete such content using the applicable feature on the Services.

10\. **Intellectual Property**
------------------------------

### 10.1. License to use the Services

**Right to use the Services.** Mistral AI grants You a worldwide, revocable, non-exclusive, non-sublicensable, non-transferable right to use the Services for the term of this Agreement, in compliance with this Agreement and the applicable law. For clarity, this right to use the Services is subject to the restrictions set out in the Additional Terms.

### 10.2. Intellectual property

**Your Intellectual Property.** You remain the sole owner of all right, title and interest, including all intellectual property rights in and to Your User Data.

**Mistral AI’s Intellectual Property.** Mistral AI remains the sole owner of all right, title and interest, including all intellectual property rights in and to Mistral AI’s Intellectual Property, including but not limited to the Model(s), the Documentation and the Services. No rights are granted to You, except as expressly set forth in this Agreement. All rights and licenses granted under this Agreement shall terminate when the applicable Agreement and/or the applicable Additional Terms terminates.The Services are made available on a limited access basis, and no ownership right is conveyed to the Customer, irrespective of the use of terms such as “purchase” or “Subscription”. Any representation or reproduction, in whole or in part, of the Services, by any process whatsoever, without Mistral AI’s prior express authorization, is strictly prohibited and will constitute an infringement punishable by the provisions of the applicable law.

11\. Warranties and indemnification
-----------------------------------

### 11.1. Mistral AI warranties

**Services provided “as is”**. The Services are provided to You “as is”. To the extent permitted by applicable law, Mistral AI makes no representations or warranties regarding the accuracy, reliability, or completeness of the Services or their suitability for Your specific requirements or use-case. Without limiting Mistral AI’s express obligations under this Agreement, Mistral AI does not warrant that Your use of the Services will increase Your revenues, be error-free, uninterrupted or that Mistral AI will review Your User Data for accuracy. You acknowledge and agree that any use of the Services is at Your own risk, and Mistral AI shall not be liable for any Losses arising from Your misuse, unauthorized or unlawful to use the Services, or from the non- suitability of the Services to Your specific requirements or use-case, including but not limited to direct, indirect, incidental, consequential, or punitive damages.

**Mistral AI Warranties**. Mistral AI warrants that:

* The Services comply with the Applicable Data Protection Law, and
* Mistral AI has the rights to all the intellectual property made accessible to You in the context of this Agreement.

### 11.2. The Customer’s warranties

You represent and warrant that:

* You have the authority to enter into this Agreement,
* You will use the Services in accordance with the applicable laws and regulations and this Agreement and that the Customer will not use the Services to commit illegal acts, including in particular to harm third parties, and
* You have obtained all necessary rights, including but not limited to, copyrights, patents, trademarks, and trade secrets, or have been granted the appropriate licenses, permissions, and consents to use and provide the Input for the purpose of using Our Services and to give us the license set forth in the Additional Terms.

### 11.3. Indemnification

**Indemnification by Mistral AI.** Mistral AI shall indemnify, defend, and hold the Customer harmless against any liabilities, damages and costs (including reasonable attorneys’ fees) payable to a third party arising out of a third party claim alleging that the Services infringe any third party intellectual property right.

**Exceptions.** Mistral AI’s indemnification obligations in this Section 11.3 (Indemnification) do not apply to the extent the claim arises from or relates to:

* The combination of Our Services with Customer or third-party software, hardware or any other equipment, software or application not provided by Mistral AI, including but not limited to Customer Offerings ;
* The modification of Our Services by You or any party other than Mistral AI, including but not limited to any fine-tuning or customization of Our Models by You or any third-party;
* Your use of Our Services or User Data in a manner that You knew, or reasonably should have known, was likely to violate third-party rights or applicable laws;
* Your modification of the Output;
* Your Input or non-Output User Data ;
* Your breach of this Agreement or the applicable laws or regulations.

**Indemnification by the Customer**. The Customer agrees to indemnify, defend, and hold Mistral AI and its affiliates and licensors harmless against any liabilities, damages, and costs (including reasonable attorneys’ fees) payable to a third party arising out of a third party claim related to (a) the use of the Services in violation of this Agreement, (b) the Customer Offerings (if any), or (c) the User Data.

**Indemnification Procedure**. The indemnification obligations this Section 11.3 (Indemnification) of these Terms of Use are subject to the indemnifying Party (a) receiving a prompt written notice of such claim ; (b) being granted the exclusive right to control and direct (including the authority to elect legal counsel) the investigation, defense or settlement strategy of such claim and (c) benefitting from all reasonable necessary cooperation and assistance, including access to the relevant information, by the indemnified Party at the indemnifying Party’s expense. In any case, Mistral AI shall consult You before entering into any settlement or compromise of any claim, and shall take into account all reasonable comments from You. To the extent permitted by applicable law, the liability cap set out in Section 12 (Liability) of these Terms of Use shall apply to the indemnification obligations under this Section 11.3 (Indemnification).

**Remedies**. The remedies in this section are the sole and exclusive remedies for any third-party claim that the Services, the Customer Offerings or the User Data infringe intellectual property rights.

12\. Liability
--------------

### 12.1. Disclaimer

**Disclaimer.** To the extent permitted by applicable law, neither Mistral AI nor Our shareholders, employees, affiliates, licensors, agents, suppliers, and service providers will be liable:

* In case of a Force Majeure Event (as defined in Section 14 of these Terms of Use),
* In case of temporary unavailability of the Services (a) for maintenance purposes, including but not limited to, to fix anomalies, bugs or errors, to launch new or improved features or Services, or to address immediate security concerns, or (b) in case We suspended Your Account,
* In relation to third-party content or websites linked or referred to on the Platform,
* For any cause not attributable to Mistral AI,
* For Your use of Your User Data,
* When You share Your Conversations with third-parties,
* For your breach of this Agreement,
* For the performance of the Model and/or any modified, customized or fine-tuned version of the Model in case You or any third-party customized, fine-tuned or otherwise modified the Model,
* In case Your Output is similar or identical to another User’s Outputs,
* For any loss of profits, income, revenue, business opportunities, loss or corruption of data or information,
* For any failure to realize expected revenues or savings, loss or damage to goodwill, pure economic loss or other economic or pecuniary loss (regardless of whether any of these type of loss or damage are direct, indirect, special or consequential), or
* For any indirect, special, incidental, punitive, exemplary, incidental or consequential damages of any kind, even if informed of the possibility of such damages in advance.

### 12.2. Limitation of liability

**Liability Cap for Free Services.** To the extent permitted by law and subject to the applicable Additional Terms, in no event will Mistral AI or Our shareholders, employees, affiliates, licensors, agents, suppliers and service providers’ total aggregate liability in connection with or under the Free Services, or Your use of or inability to use the Free services, exceed 100 euros.

**Liability Cap for Paid Services**. To the extent permitted by law, the total aggregate liability of Mistral AI and Our shareholders, employees, affiliates, licensors, agents, suppliers and service providers’ in respect of any Losses incurred by the Customer under or in relation to this Agreement will not exceed, in the aggregate, the greater of (i) the amount of the Fees paid or payable by the Customer in the twelve (12) calendar months preceding the date on which the first such event or events occurred or (ii) 10.000 euros.

**Multiple claims.** The existence of one or more claims under this Agreement will not increase the above mentioned liability caps. You agree that any Losses or claim You may have under this Agreement can only be recovered once and any such claim will exhaust all and any other claims that might otherwise arise against Mistral AI and Our shareholders, employees, affiliates, licensors, agents, suppliers and service providers in relation to which the Customer has been compensated or otherwise reimbursed.

**Legal action.** You agree that the limitations specified in this section apply regardless of the form of action, whether in contract, tort (including negligence), strict liability or otherwise.

13\. Term, suspension and termination
-------------------------------------

### 13.1. Duration

**Term.** This Agreement will commence on the Effective Date and continue until terminated by either Party.

### 13.2. Suspension of Your Account

**Suspension.** We reserve the right to suspend Your Account and/or Your access to all or part of the Services in case of:

* Your breach of this Agreement,
    
* Late payment or non-payment of the applicable fees (if any),
    
* Immediate security concerns.
    

**Notification.** We will notify You of the suspension and the reasons for such suspension seven (7) days prior to the suspension taking effect, except in the event of a serious breach by You of this Agreement or an immediate security concern, in which case the suspension will take effect with shorter notice.

**Effects of the suspension.** During the suspension, all rights and permissions granted to You under this Agreement will be suspended and You will not be able to access or use Our Services.

**Remediation.** You shall have a period of thirty (30) days to remedy the breach notified by Mistral AI and to notify Mistral AI accordingly. Failing that, Mistral AI reserves the right to terminate this Agreement for cause immediately, without further notice. Termination will be effective at the end of this thirty (30) days period.

### 13.3. Termination

**Termination for convenience.** You may terminate this Agreement at any time by sending an email to [support@mistral.ai](mailto:support@mistral.ai).

* If You subscribed to Free Services: termination shall become effective immediately.
* If You subscribed to Paid Services: termination shall become effective at the end of the then-current Billing Cycle. Upon any such termination (i) You will not be entitled to a refund of any pre-paid Fees and (ii) if You have not already paid all applicable Fees for the then-current Billing Cycle, any such Fees that are outstanding will become immediately due and payable.

**Termination for cause**. Either Party may terminate this Agreement if the other Party fails to cure a material breach of this Agreement and/or any relevant Purchase Order within thirty (30) days after notice of such breach, provided that such breach is remediable. For illustrative purposes, any of the following breaches by either Party shall be considered a material breach of this Agreement (a) any unauthorized use of the Services and/or, (b) failure to pay any amounts due under this Agreement.

**Effects of termination**. Upon termination or expiration of this Agreement:

* You will no longer have access to the Services;
* You must pay any outstanding Fees to Mistral AI within thirty (30) days of the issuance invoice;
* You are not entitled to a refund of any pre-paid Fees.

Termination or expiry of this Agreement will not automatically result in the deletion of Your Account or the Authorized Users’ Accounts. Subject to having paid any outstanding amounts to Mistral AI, You may delete Your Account after the termination of this Agreement, by using the applicable feature on the Services.

**Survival**. All payment obligations incurred during the term of this Agreement and the following Sections shall survive the expiration or termination of these Terms of Use, each for the duration necessary to achieve its own intended purpose (e.g. the liability provision will survive until the end of the applicable limitation period): Section 7 (Fees, billing and payment), Section 10 (Intellectual Property), Section 11 (Warranties and Indemnification), Section 12 (Liability), Section 13 (Term, Suspension and Termination), Section 18 (General Provisions) and Section 18 (Dispute resolution and applicable law).

14\. Personal Data
------------------

**Definitions.** For the purpose of this Section, the terms “Data Controller”, “Data Processor”, and “Personal Data” shall have the meaning given to them in the Applicable Data Protection Laws.

### 14.1. Mistral AI Data Controller

**Privacy Policy.** Mistral AI processes Your Personal Data as Data Controller for the purposes of (a) providing the Services, (b) commercially managing this Agreement, (c) billing and (d) marketing operations. If You want to know more about the way We process Your Personal Data and how you can exercise Your rights, please refer to our Privacy Policy. For clarity, the [Privacy Policy](https://mistral.ai/terms/#privacy-policy) shall not apply to the processing of Personal Data carried out by Mistral AI on behalf of the Commercial Customer.

### 14.1. Mistral AI Data Processor

**Data Processing Agreement**. Mistral AI may process Personal Data on behalf of the Professional Customer, as Data Processor. In such the latter case, the [Mistral Data Processing Agreement](https://mistral.ai/terms/#data-processing-agreement) shall apply between the Parties.

15\. Provisions that only apply to Consumers
--------------------------------------------

**Scope**. This section only applies to Consumers. In case of any discrepancies between the rest of this Agreement and this Section, this Section will prevail. For the avoidance of doubt, in this Section “You” shall only refer to Consumers.

### 15.1. Right of withdrawal (“Droit de rétractation”)

**Waiver**. By accepting these Terms of Use and utilizing the Paid Services:

* You acknowledge that the Paid Services will be provided before the expiration of the withdrawal period of fourteen (14) days from Your acceptance of this Agreement, and
* You expressly waive Your right of withdrawal (droit de rétractation).

We will send You a confirmation of Your waiver of its right of withdrawal along with the confirmation of Your Subscription under Section 4.3. (Subscription to the Services) of these Terms of Use.

### 15.2. Legal warranties

**Warranty**. Along with any commercial warranty provided to You under this Agreement, You also benefit from the legal warranty of compliance in the conditions set forth in Exhibit 1 of these Terms of Use.

### 15.3 Upgrades {upgrades.unnumbered}

**Upgrades.** For the purpose of this Section, “Upgrades” shall mean any update or upgrade to the Services. Upgrades do not include any new Models.

**Upgrades necessary to maintain the compliance of the Services.** We will inform You of any Upgrades necessary to maintain the compliance of the Services during the Term. We will notify You of the availability of Upgrades and the consequences of their non-installation for You. We shall not be held liable for any non-compliance related issues when You fail to install, within a reasonable timeframe, the necessary Upgrades to maintain the Services’ compliance.

**Upgrades not necessary to maintain the compliance of the Services.** We may propose Upgrades that are not necessary to maintain the compliance of the Services. In such cases, We will inform You in advance and through a durable medium about the planned Upgrades and their implementation date. You may refuse the aforementioned upgrade. In this event, You have the right to terminate this Agreement without charges (unless the Upgrade has minor implications for the Consumer or if, without this Upgrade, the Services remain compliant).

16\. **Changes to these Terms of Use**
--------------------------------------

**Non-substantial modifications.** We reserve the right to modify this Agreement at any time.

**Substantial modification.** In the event of any substantial modifications to this Agreement and subject to this substantial modification adversely impacting You, we will notify You of such modifications no later than thirty (30) days prior to the effective date of such modifications at the email address You provided upon registration. If You do not agree with such substantial modifications, You may terminate this Service Agreement in compliance with Section 14 (Term, Suspension and Termination) of these Terms of Use.

17\. **General provisions**
---------------------------

**Non waiver.** The fact that either of the Parties does not claim application of any clause whatever of this Agreement or condones its non-performance, shall not be construed as a waiver by that Party to the rights stemming for it from said clause. A waiver of any right or remedy under this Agreement or by law is only effective if given in writing and shall not be deemed a waiver of any subsequent right or remedy.

**Severance.** If a court or any other competent authority finds any provision of this Agreement (or part of any provision) to be invalid, illegal or unenforceable, that provision or part provision shall, to the extent required, be deemed deleted, and the validity and enforceability of the other provisions of this Agreement shall not be affected. If any invalid, unenforceable or illegal provision of this Agreement would be valid, enforceable and legal if some part of it were deleted, the provision shall apply with the minimum modification necessary to make it legal, valid and enforceable.

**No-partnership.** Nothing in this Agreement is intended to, or shall be deemed to constitute a partnership or joint venture of any kind between any of the Parties, nor constitute any Party the agent of another Party for any purpose. No Party shall have authority to act as agent for, or to bind, the other party in any way. Neither the User nor Mistral AI will suggest or claim any sponsorship, endorsement or affiliation with the other party, unless such a relationship is governed by a separate agreement.

**Entire Agreement.** This Agreement is the entire agreement between the Parties relating to the Services, and any other subject matter covered by this Agreement and supersedes all prior or contemporaneous oral or written communications, proposals and representations between the Parties, with respect to the Services or any other subject matter covered by the Agreement.

**Force Majeure.** Neither Party will be liable to the other for any delay or failure to perform any obligation under these Terms if the delay or failure is due to events which are beyond the reasonable control of such party, such as a strike, blockade, war, act of terrorism, riot, natural disaster, failure or diminishment of power or telecommunications or data networks or services, or refusal of a license by a government agency (the “**Force Majeure Event**”).

18\. Dispute resolution and applicable law
------------------------------------------

### 18.1. Applicable Law

**Applicable Law**. This Agreement and any dispute or claim (including non-contractual disputes or claims) arising out of or in connection with it or its subject matter or formation shall be governed by and construed in accordance with the laws of France.

### 18.2. Dispute resolution

#### 18.2.1. Amicable resolution

**General principle.** In the event of any controversy or claim arising out of or relating to this Agreement, the Parties will consult and negotiate with each other and, recognizing their mutual interests, attempt to reach a solution satisfactory to both Parties.

**You are a Consumer.** Where the User is located within the European Union, the User may contact:

* Our mediation service, the CMAP, by mail at Centre de Médiation et d’Arbitrage de Paris, 39 avenue Franklin D. Roosevelt, 75008 PARIS or by email at [cmap@cmap.fr](mailto:cmap@cmap.fr).
* Or the european platform for the online settlement of disputes accessible at the following address: [https://ec.europa.eu/consumers/odr/main/index.cfm?e](https://ec.europa.eu/consumers/odr/main/index.cfm?e)

You agree that the mediation process is not mandatory and that Either Party may withdraw from such mediation process at any time.

* Where the User is not located within the European Union, the User may contact Mistral AI directly at [support@mistral.ai](mailto:support@mistral.ai).

**You are a Commercial Customer.** If the Parties do not reach settlement within a period of sixty (60) days, either Party may escalate the controversy or to the senior executives or both Parties (the “**Executives**”). The Executives shall then promptly engage in discussions and negotiations to seek a mutually agreeable resolution in the best interest of both Parties.

#### 18.2.2. Competent jurisdiction

**General Principle.** If the Parties do not reach an amicable settlement, each Party may pursue relief as may be available under this Agreement. All negotiations pursuant to this section will be considered confidential information and shall not be shared with any third parties without the disclosing Party’s prior written consent even after the termination and/or expiration of this Agreement.

**Where the User is located in France,** the Parties agree that the courts of Paris, France shall have exclusive jurisdiction over any disputes arising out of or in connection with this Agreement or its subject matter or formation.

**Where the User is not located in France,** all disputes arising out of or in connection with this Agreement or its subject matter or formation shall be finally settled under the rules of arbitration of the international chamber of commerce (the “**ICC**”) by one arbitrator appointed in accordance with the said rules. The arbitration proceedings shall take place exclusively at the ICC headquarters in Paris, France. The appointed arbitrator shall adjudicate the dispute in accordance with the applicable law.

**EXHIBIT 1 - Legal warranty applicable to french customers acting as Consumers** Le consommateur a droit à la mise en œuvre de la garantie légale de conformité en cas d’apparition d’un défaut de conformité durant la période de la fourniture du contenu numérique ou du service numérique. Durant ce délai, le consommateur n’est tenu d’établir que l’existence du défaut de conformité et non la date d’apparition de celui-ci. La garantie légale de conformité emporte obligation de fournir toutes les mises à jour nécessaires au maintien de la conformité du contenu numérique ou du service numérique durant la durée d’enregistrement du compte du consommateur sur la plateforme. La garantie légale de conformité donne au consommateur droit à la mise en conformité du contenu numérique ou du service numérique sans retard injustifié suivant sa demande, sans frais et sans inconvénient majeur pour lui. Le consommateur peut obtenir une réduction du prix en conservant le contenu numérique ou le service numérique, ou il peut mettre fin au contrat en se faisant rembourser intégralement contre renoncement au contenu numérique ou au service numérique, si : 1° Le professionnel refuse de mettre le contenu numérique ou le service numérique en conformité ; 2° La mise en conformité du contenu numérique ou du service numérique est retardée de manière injustifiée ; 3° La mise en conformité du contenu numérique ou du service numérique ne peut intervenir sans frais imposés au consommateur ; 4° La mise en conformité du contenu numérique ou du service numérique occasionne un inconvénient majeur pour le consommateur ; 5° La non-conformité du contenu numérique ou du service numérique persiste en dépit de la tentative de mise en conformité du professionnel restée infructueuse. Le consommateur a également droit à une réduction du prix ou à la résolution du contrat lorsque le défaut de conformité est si grave qu’il justifie que la réduction du prix ou la résolution du contrat soit immédiate. Le consommateur n’est alors pas tenu de demander la mise en conformité du contenu numérique ou du service numérique au préalable. Dans les cas où le défaut de conformité est mineur, le consommateur n’a droit à l’annulation du contrat que si le contrat ne prévoit pas le paiement d’un prix. Toute période d’indisponibilité du contenu numérique ou du service numérique en vue de sa remise en conformité suspend la garantie qui restait à courir jusqu’à la fourniture du contenu numérique ou du service numérique de nouveau conforme. Ces droits résultent de l’application des articles L. 224-25-1 à L. 224-25-31 du code de la consommation. Le professionnel qui fait obstacle de mauvaise foi à la mise en œuvre de la garantie légale de conformité encourt une amende civile d’un montant maximal de 300 000 euros, qui peut être porté jusqu’à 10 % du chiffre d’affaires moyen annuel ([article L. 242-18-1 du code de la consommation](https://www.legifrance.gouv.fr/affichCodeArticle.do?cidTexte=LEGITEXT000006069565&idArticle=LEGIARTI000044138681&dateTexte=&categorieLien=cid)). Le consommateur bénéficie également de la garantie légale des vices cachés en application des articles 1641 à 1649 du code civil, pendant une durée de deux ans à compter de la découverte du défaut. Cette garantie donne droit à une réduction de prix si le contenu numérique ou le service numérique est conservé, ou à un remboursement intégral contre renonciation au contenu numérique ou au service numérique.

* * *

Additional Terms - La Plateforme
================================

Preamble
--------

**La Plateforme.** Mistral AI provides La Plateforme, a SaaS platform that allows You to:

* Use Our Models through a set of APIs,
* Fine-Tune Our Models through Our Fine-Tuning API,
* Build and deploy conversational Agents,
* Access a range of complementary Services.

**Parties.** These Additional Terms constitute a binding agreement between Mistral AI and You.

**Purpose and Scope.** These Additional Terms set forth the rights and responsibilities of Mistral AI and You in connection with the provision and use of La Plateforme. These Additional Terms constitute an integral part of the Agreement between the Parties and are incorporated by reference into the Agreement.

**Contractual hierarchy.** In the event of any conflict or inconsistency between these Additional Terms and any other document or agreement, the provisions of these Terms shall prevail and govern.

**Effective Date and Term.** These Additional Terms are effective as of the Effective Date and shall continue for the Term of the Agreement, unless terminated by the Parties in accordance with the Agreement.

**Acceptance.** Any access, use or Subscription to La Plateforme implies Your unreserved agreement to these Additional Terms. By clicking on “I agree” (or any similar button or checkbox) or by using La Plateforme, You expressly agree to be bound by these Additional Terms.

1\. Definitions
---------------

**Definitions provided in this document.** The capitalized words in this document shall have the meaning set forth below:

* **“Agent”:** means a conversational agent created by You based on a Model You customized through the Agent Builder.
    
* **“Agent Builder”:** means the feature on La Plateforme that allows You to create and share an Agent within Your Workspace.
    
* **“API”:** means Mistral AI’s application programming interface that allows You to use the Model(s) on La Plateforme. This definition includes (a) the APIs that allows You to use and deploy the Models and/or Agents (the “Models APIs”) (b) the API that allows You to Fine-Tune the Models (the “Fine-Tuning API”) and (c) any other API provided by Mistral AI on La Plateforme.
    
* **“API Key”:** means the unique authentication code provided by Mistral AI to the Customer allowing the Customer to access and use the API.
    
* **“Effective Date”:** means the earlier of (i) the date You first use La Plateforme or (ii) the date the You accept these Additional Terms.
    
* **“Fine-Tuned Model”:** means any Model that has been Fine-Tuned using the Fine-Tuning API.
    
* **“Fine-Tuning” or “Fine-Tune” or “Fine-Tuned”**: means the technical process of adjusting or modifying a Model in order to improve its performance, accuracy, or efficiency.
    
* **“La Plateforme”**: means the online platform available at [http://console.mistral.ai](http://console.mistral.ai/) in SaaS mode, and all its features and components, including but not limited to the APIs and the Agent Builder.
    

**Definitions not provided in these Additional Terms.** Any capitalized words that are not defined in these Additional Terms shall have the meaning given in the Agreement.

2\. Your use of La Plateforme
-----------------------------

### 2.1 La Plateforme

**Provision of the La Plateforme.** During the Term and subject to Your compliance with this Agreement, Mistral AI will make La Plateforme available to You under the terms and conditions set forth in this Agreement.

**Permitted Use.** Mistral AI grants You a worldwide, revocable, non-exclusive, non-sublicensable, non-transferable right to use La Plateforme for the Term, for Your personal use or for own internal business purposes, in accordance with the Agreement.

**Restrictions.** For the avoidance of any doubt, Your right to use La Plateforme is subject to Section 8 (Your Obligations) of the Terms of Use.

### 2.2 The APIs

#### 2.2.1 Terms applicable to any API provided on La Plateforme

**Permitted Use.** Mistral AI grants You a worldwide, revocable, non-exclusive, non-sublicensable, non-transferable, sub-licensable right to use the APIs for the Subscription Term, for the purpose of (i) incorporating the APIs into Your Customer Offerings and/or (ii) using the APIs for Your own internal business needs, in compliance with this Agreement and the applicable law. The right to use the APIs includes the right to allow (i) the Authorized Users to use the APIs and (ii) the End Users to use the APIs through the Customer Offerings.

**Restrictions.** For the avoidance of any doubt, Your right to use the APIs is subject to Section 8 (Your Obligations) of the Terms of Use.

**API Key.** Once You have subscribed to the Services, Mistral AI will provide You with an API Key. You shall set-up and use the API Key in compliance with the associated Documentation to be able to use the API. You acknowledge and agree that the API Key is confidential information. Consequently, You shall not share the API Key with any third party without Mistral AI’s prior written consent. At the expiration of this Agreement for any reason whatsoever, You agree to delete the API Key. Any unauthorized disclosure of the API Key by You or Your Authorized Users will constitute a material breach of this Agreement.

**API call limitations.** The volume of API calls made by You within a specific period may be subject to limitation at the sole discretion of Mistral AI. Such limitations could be influenced by factors including the expected usage volume associated with the Customer Offerings.

**Third-Party Services.** Our APIs are compatible with the Third-Party Services listed on La Plateforme. To use a Third-Party Service on La Plateforme, You may have to (a) create an account on the Third-Party Service and (b) connect this account to La Plateforme by following the instructions given on La Plateforme. You acknowledge that Mistral AI does not provide the Third-Party Service and therefore cannot be held liable under any circumstances for any loss, damage, or claim arising out of or in relation with Your use of a Third-Party Service, including when such use occurs on La Plateforme.

#### 2.2.2 Terms applicable to the Models API

#### 2.2.2.1 Our Free Services

**Rate limits.** Any use of Our Free Services may be subject to the rate limits set forth on La Plateforme. You acknowledge and agree that if You reach these rate limits, Your access to or use of the Free Services will be temporarily suspended until the rate limits reset or unless You subscribe to the Paid Services.

**How we use Your User Data:** If You use Our Free Services, we use Your User Data to:

* Generate the Output based on Your Input,
* Monitor abuse (meaning, to monitor any breach by You of the Agreement). To this end, we retain Your Input and Outputs for a period of thirty (30) days, and,
* Improve, enhance or train our Models.

Accordingly, You grant Us a worldwide, royalty-free, license to use Your User Data for the purposes listed above, for the duration of the Agreement. The license granted to improve, enhance or train Our Models is granted for the duration of the applicable intellectual property rights.

#### 2.2.2.2 Our Paid Services

**Rate limits.** For Our Paid Services, You have the ability to set up Your own consumption limits for API credits. For example, if You set a limit of 150 euros, Your usage of the API will be automatically blocked once this limit is reached. You may unblock Your access by adjusting the limit as needed. Any other applicable rate limit is set out on Your Account on La Plateforme.

**How we use Your User Data:** If You subscribe to Our Paid Services, we use Your User Data to:

* Generate the Output based on Your Input, and
* Monitor abuse (meaning, to monitor any breach by You of the Agreement), unless You activate Zero Data Retention (as defined below). To this end, we retain Your Input and Output for a period of thirty (30) days.

Accordingly, You grant Us a worldwide, royalty-free, license to use Your User Data for the purposes listed above, for the duration of the Agreement.

We do not use Your User Data to train, improve or enhance our Models.

**Zero data retention.** Customers with legitimate reasons may request zero data retention from Mistral AI (“Zero Data Retention”). With this option, Your Inputs and Outputs are only processed for the duration necessary to generate the Output and are not retained for any longer period, except as required by law. For clarity, when this option is activated, Your User Data is not stored for abuse monitoring purposes. To request Zero Data Retention, You must submit Your request via the Help section of Your Account and provide legitimate reasons for Your request. Mistral AI will review Your request and, at its sole discretion, may approve or deny Your request. If approved, Mistral AI will implement the zero data retention option for Your Account and notify You when the process is complete. If denied, Mistral AI will notify You.

#### 2.2.3 Terms applicable to the Fine-Tuning API

**Fine-Tuning API.** Our Fine-Tuning API allows You to Fine-Tune the compatible Models provided by Mistral AI. The list of the compatible Models is provided on the Platform. To use the Fine-Tuning API, You must:

* Prepare Your Input,
* Upload Your Input on La Plateforme,
* Follow the instructions on La Plateforme,
* Review the Fine-Tuning of the Model on La Plateforme.

**Fine-Tuned Model.** Upon completion of the Fine-Tuning process, You will be granted access to use the Fine-Tuned Model directly on La Plateforme for the duration of this Agreement. You will have no rights in the Fine-Tuned Model after the expiration or termination of this Agreement. We will not use the Fine-Tuned Model, except to make it available to You for the purpose of providing You with the Services.

**Deletion of the Fine-Tuned Model.** You may delete the Fine-Tuned Model from La Plateforme at any time. Following the expiration or termination of this Agreement, We will retain the Fine-Tuned Model for a period of one (1) year, to allow You to use such Fine-Tuned Model again in case You re-subscribe to Our Fine-Tuning API. Notwithstanding the foregoing, You may request that We delete the Fine-Tuned Model prior to the expiration of the one (1) year retention period by submitting a written request via Our Help Center. We will use commercially reasonable efforts to delete the Fine-Tuned Model within thirty (30) days of receiving such request.

**Confidentiality of the Fine-Tuned Model.** The Fine-Tuned Model shall remain confidential and Mistral AI shall not disclose or distribute the Fine-Tuned Model to any third party except under Your directive and for authorized transfer to authorized Mistral AI cloud provider distributors. Your responsibility. You are solely responsible for the Fine-Tuning of the Model and for Your Input. You acknowledge that Mistral AI cannot be held responsible for the performance of the Fine-Tuned Model, including for the Outputs generated by such Fine-Tuned Model.

**Limited Assistance.** Upon Your written and express request and subject to Mistral AI’s prior approval, Mistral AI may provide You with guidance on how to improve Your Input to make the Fine-Tuning process more efficient, subject to Your prior express agreement on Mistral AI having an access to the User Input Data for these purposes only.

**How we use Your User Data.** On the Fine-Tuning API, We use Your User Data to:

* Fine-Tune the Model with Your Input, and
* Provide You with limited assistance in accordance with Section 2.2.3 of these Additional Terms.

Accordingly, You grant us a worldwide, royalty-free license to use Your Input for the above mentioned purposes, for the duration of these Additional Terms.

**Training.** We do not use Your Input to train or improve Our Models, unless You use Our Free Services. In such a case, You grant us a worldwide, royalty-free license to use Your User Data to train or improve Our Models for the duration of the applicable intellectual property rights.

### 2.3 The Agent Builder

**Agent creation.** You may create an Agent either through Our Fine-tuning API or through the Agent Builder feature on La Plateforme, by following the instructions given on La Plateforme.

**Agent sharing.** Subject to Your Subscription Plan, You may make Your Agent available to other Authorized Users of Your Workspace through Our API or on Le Chat. Any use of Your Agent via Le Chat will be governed by the Additional Terms for Le Chat. By default, all Agents created by You will be shared within Your Workspace. You are solely responsible for setting permissions if You wish to restrict access to the Agent to specific Authorized Users. By default, Authorized Users will be able to interact with Your Agent by tagging such Agent in their Input (for instance: “@Agent what is the capital of France ?”).

**Agent ownership.** You will have no rights in the Agent after the expiration or termination of this Agreement. We will not use the Agent, except to make it available to You for the purpose of providing You with Our Services. To this end, You grant Us a non-exclusive, worldwide, royalty-free license to use and display the Agent solely for the purpose of making the Agent available to You through Our Services, for the duration of this Agreement. For clarity, We will not use Your Agent for any other purpose and will not make the Agent available to any other Customer or third-party.

**Your responsibility.** You are solely responsible for the creation and the performance of Your Agent. In particular, You are responsible for:

* **Your User Data:** ensuring that You have all the applicable rights or authorizations to use Your Input, including but not limited to any data You use to create or customize Your Agent.
* **The Instructions You give to Your Agent:** ensuring that the instructions given to Your Agent comply with applicable laws and regulations and are not intended to harm Mistral AI or third-parties.
* **Naming Your Agent.** You must ensure that the name of Your Agent does not violate the rights of third-parties and complies with all applicable laws.

**Deletion of the Agent.** You may delete the Agent from La Plateforme at any time. Following the expiration or termination of this Agreement, We will retain the Agent for a period of thirty (30) days, to allow You to use such Agent again in case You re-subscribe to Our Services. Notwithstanding the foregoing, You may request that We delete the Agent prior to the expiration of the thirty (30) days retention period by submitting a written request via Our Help Center. We will use commercially reasonable efforts to delete the Agent within ten (10) days of receiving such request.

3\. Fees, payment and billing
-----------------------------

### 3.1. Free Services

**Free of charge.** If You use our Free Services, no payment is required.

### 3.2. Paid Services

**Advance payments.** Mistral AI reserves the right to charge the Customer for usage of la Plateforme in advance of the current Billing Cycle (the “Advance Payment”), subject to predefined consumption thresholds available on La Plateforme, as may be updated time to time by Mistral AI (the “Consumption Threshold”). Upon the Customer’s consumption meeting or surpassing a Consumption Threshold, Mistral AI reserves the right to charge one or multiple Advance Payments to the Customer corresponding to the applicable Consumption Threshold(s) until the end of the current billing Cycle, using the Customer’s chosen payment method. A notification will be sent to the Customer’s email address to inform the Customer about the advance payment and to issue the applicable invoice. At the end of each Billing Cycle, Mistral AI will charge the Customer for the applicable Fees, deducing the Advance Payment and will send a summary invoice to the Customer.

**Billing.** Billing shall occur simultaneously as payment or Advance Payment(s).

**Disputed Fees.** In the event that the Customer disputes any charges billed by Mistral AI, the Customer must inform Mistral AI within thirty (30) days from the invoice date to request an adjustment or credit. Should a discrepancy arise, the Customer acknowledges the obligation to settle any undisputed amounts as per the previously outlined payment terms. Both Parties commit to engaging in good faith discussions to resolve any contested charges.

4\. Term and termination
------------------------

**Termination.** Either Party may terminate these Additional Terms under the conditions set forth in Section 13 (Term, Suspension and Termination) of the Terms of Use.

**Effects of termination.** Upon termination or expiration of these Additional Terms, You must delete the API Key.

* * *

Additional Terms - Le Chat
==========================

Preamble
--------

**Le Chat.** Mistral AI provides a chat interface that allows You to interact with Our Services, including Your Agents.

**Parties.** These Additional Terms constitute a binding agreement between Mistral AI and You. Purpose and Scope. These Additional Terms set forth the rights and responsibilities of Mistral AI and the Customer in connection with the provision and use of Le Chat. These Additional Terms constitute an integral part of the Agreement between the Parties and are incorporated by reference into the Agreement. Any use of an Agent through Le Chat is subject to these Additional Terms.

**Contractual hierarchy.** In the event of any conflict or inconsistency between these Additional Terms and any other document or agreement, the provisions of these Additional Terms shall prevail and govern.

**Effective Date and Term.** These Additional Terms are effective as of the Effective Date and shall continue for the Term of the Agreement.

**Acceptance.** Any access, use or Subscription to Le Chat implies Your unreserved agreement to these Additional Terms. By clicking on “I agree” (or any similar button or checkbox) or by using Le Chat, You expressly agree to be bound by these Additional Terms.

1\. Definitions
---------------

Definitions provided in this document. The capitalized words in this document shall have the meaning set forth below:

* **“Agent”** : means a conversational agent created by You based on a Model You customized using La Plateforme.
* **“Conversation”:** means the interactive exchange between You and Le Chat, which includes Your Inputs, Outputs.
* **“Effective Date”:** means the earlier of (i) the date You first use Le Chat or (ii) the date You accept the Agreement.
* **“Le Chat”:** means the chat interface provided by Mistral AI, where You can interact with Our Services, including through an Agent.
* **“Shared Link”:** means the hyperlink You can share with third-party to allow them to read a Conversation.

**Definitions not provided in this document.** Any capitalized words that are not defined in these Additional Terms shall have the meaning given in the Agreement.

**2\. Le Chat**
---------------

**Le Chat.** Le Chat consists of an online interface that, depending on Your Subscription Plan, allows You to generate an Output based on Your Input through our conversational assistant, including through an Agent.

**Provision of Le Chat.** During the Subscription Term and subject to Your compliance with this Agreement, Mistral AI will make Le Chat available to You under the terms and conditions set forth in this Agreement.

**Permitted Use.** Subject to this Agreement and for the duration of the Term, Mistral AI grants You a non-exclusive, worldwide, revocable, non-sublicensable, and non-transferable license to use Le Chat solely for (i) Your own personal purposes or Your own internal business needs and (ii) to authorize Authorized Users to use Le Chat for that purpose, all in compliance with this Agreement and applicable law.

**Restrictions.**

* You may not integrate or combine Le Chat with Your Customer Offerings, nor sublicense Le Chat to any third-party without Mistral AI’s prior written authorization.
* Our news offering allows you to access content provided by the Agence France Presse, having its office located at 11-13-15 place de la Bourse, 75002 Paris, in a beta test version (the “News Offering”). For the purpose of this Agreement, “Media Customer” means any entity that disseminates information for editorial purposes on a regular basis and in a professional capacity, whether to the general public or to professionals, even if such activity is conducted by a specific department within the entity and constitutes only a part of its overall business activities. If You are a Media Customer, You are not authorized to use Our News Offering on Le Chat for editorial use, and any use by You of Our News Offering may be automatically blocked. If You are a Media Customer and wish to use the News Offering for editorial purposes, You must contact Mistral AI at [legal@mistral.ai](mailto:legal@mistral.ai). Any unauthorized use of the News Offering by a Media Customer will result in the immediate suspension of such Customer’s account.

**Rate Limits.** Any use of Le Chat or Third-Party Services available on Le Chat may be subject to the rate limits set forth on the Services and/or Your Subscription Plan. You acknowledge and agree that if You reach these rate limits, Your access to or use of Le Chat will be temporarily restricted or suspended until the rate limits reset or unless You subscribe to another Subscription Plan.

**Activation of Services.** Depending on Your Subscription Plan, You may be able to access certain Services through Le Chat. These Services are activated by default on Your Workspace. Administrators can deactivate and reactivate Services for their Workspace on their Account.

**Shared Links.** You can share a Conversation between You and Le Chat with a third-party (the “Recipient”) by creating a unique Shared Link via Le Chat. This Shared Link will allow the Recipient to read the Conversation You selected. The Recipient will only access the Conversation You selected, and will not access (i) Your username (unless you expressly include it in a Prompt), (ii) any addition You make to the Conversation after sharing it, or (iii) any other Conversation. You acknowledge that all individuals who have access to the Shared Link are given viewing privileges to the shared Conversation and that, as a consequence, if the Recipient shares Your Shared Link with a third-party, such third-party will be able to access Your shared Conversation. Consequently, You must ensure that the shared Conversation does not include (i) any Personal Data or (ii) any sensitive business data such as trade secrets, confidential information, etc. You are solely responsible for the sharing of Your Conversations. Mistral AI cannot be held responsible for any third-party access to Your shared Conversation.

**3\. Your User Data**
----------------------

**How We use Your User Data.** We use your User Data to:

* Generate Outputs,
* Display your Conversation history on Le Chat,
* Allow You to share Your Conversations with third-parties;
* Train or improve our Models and Services, unless You meet the following conditions:
    * You use Our Paid Services; and
    * You have opted-out of the training by using the applicable setting on Your Account.

Accordingly, You grant Us a non-exclusive, worldwide, irrevocable license to use your User Data for these purposes and for the duration of this Agreement. Notwithstanding the foregoing, the license You grant Us to train or improve Our Models and Our Services is granted for the duration of the applicable intellectual property rights.

**Output restrictions.** You shall not use image Outputs to develop or train any competing image-generation service. You shall not use the Outputs generated by our web search features (including our News Offering) or any associated Third-Party Content such as thumbnails or snippets, to (ii) copy, store, archive, cache or create a database of the Outputs or Third-Party Content, (iii) redistribute, resell or sublicense the Outputs or the Third-Party Content, (iv) use the Outputs or the Third-Party Content as part of any machine learning or similar algorithmic activity nor (v) use the Outputs or the Third-Party Content to create, train, evaluate or improve Your Customer Offerings.

**4.Le Chat Moderation Policy**
-------------------------------

Le Chat is part of Mistral AI’s effort to make technology more accessible and showcase how generative AI enables greater access to knowledge. We follow strong principles to ensure responsible AI development and deployment.

**Our principles:**

* **Be neutral:** we believe technology should be as neutral as possible.
* **Empower people:** we believe in minimizing the risk of abuse while trusting that our Users and Customers should be empowered to define and use robust controls adapted to them and their business priorities. This option is already available to Customers who subscribe to the Paid Services.
* **Build trust through transparency:** We believe in building Services that provide everyone the right to access generative AI and in being transparent about Our Models, Our policies, and

**Our enforcement.** We believe in the power of open source to drive greater accountability. Mistral AI’s Services, including Le Chat, should not be used to create unlawful or harmful content. By using our Services, you agree to adhere to our policies as set out in this Agreement. Violating our policies could result in action against Your Account, up to suspension or termination as set out in the Terms Of Use.

As part of Mistral AI’s obligations to fight against online threats we continuously engage work and efforts to automatically detect the following categories of Input and/or Output, and to submit them to a range of responses:

* **Preventing harm against children:** Mistral AI does not tolerate any form of child abuse and is committed to prevent Le Chat to be misused to create any Output that exploits and harms children. Repeated actions to generate such Outputs that contain child sexual abuse material may lead to Account suspension.
* **Warning Users of potentially harmful Inputs and Outputs:** by default, we continuously engage work and efforts to automatically flag harmful content , especially:
    * Instructions that describe how to perform activities that are meant to kill or severely harm others; praising, memorializing or justifying prominent terrorist, extremist, or criminal figures in order to encourage others to carry out acts of violence;
    * Inputs or Outputs aimed at recruiting new members to violent extremist, criminal, or terrorist organizations.
    * Generation of hateful, harassing, or violent Outputs: Outputs or activity that expresses, incites, or promotes hate based on an individual’s race, gender, ethnicity, religion, nationality, sexual orientation, disability status, or caste is strictly prohibited. any content or activity intended to harass, threaten, bully, or include violence or serious harm towards an individual or group, regardless of their affiliation; any content that promotes, glorifies, or celebrates violence, or the suffering or humiliation of others, is also not permitted; User Data that expresses, incites, or promotes harassing language towards any target.
    * Generation of Outputs that denies or minimizes well-documented, major violent events or the victimhood of such an event such as the Holocaust.
    * Generation of Outputs about activity that has high risk of physical harm, including: weapons development, military and warfare, management or operation of critical infrastructure in energy, transportation, and water, content that promotes, encourages, or depicts acts of self-harm, such as suicide, cutting, and eating disorders.
    * Generation of Outputs that ​​help recruiting people for, facilitates or exploits people,including through human trafficking or sexual services.
    * Generation of Outputs (including image outputs) that infringe on the rights of third-parties: Outputs that infringe on third-party intellectual property rights, privacy, or personality rights, such as any individual’s right to control their image.

The Paid Services allow You to tailor Your moderation features to your needs. You are solely responsible for any moderation feature You implement on Le Chat. Accordingly, You acknowledge and agree that we disclaim all liability in case of generation of an illegal, offensive or infringing Output where such generation results from Your moderation features.

**5.Termination**
-----------------

**Termination.** Either Party may terminate these Additional Terms under the conditions set forth in Section 13 (Term, Suspension and Termination) of the Terms of Use.

**Effects of termination.** Your User Data will be deleted ninety (90) days from the termination of Your Account, unless You subscribed to Free Services or have not opted-out of training, in which case it may be de-identified and retained to improve or train Our Services.

* * *

Partner Hosted - Deployment Terms
=================================

Preamble
--------

**These Deployment Terms only apply if You use Our Services through a Cloud Provider who provides Our Services in Mistral AI’s name.**

**Welcome to Mistral!** We provide artificial intelligence models (the “**Models**”) that allow You to submit a textual instruction (the “**Prompt**”) and receive a result generated by a Model (the “**Output**”), including, depending on your subscription, via an API, alongside with a complementary support services (the Models and such support services, the “**Services**”). We also provide technical documentation and other material for the use of Our Models (the “**Documentation**”). You can integrate Our Services into Your applications or softwares (the “Customer Products and Services”), allowing You or Your end-users (the “**End-Users**”) to submit Prompts and receive Outputs.

**The Deployment Terms.** These Deployment Terms (the “**Deployment Terms**”) constitute a binding legal agreement between Mistral AI, registered with the Trade and Companies Register of Paris under number 952 418 325 and headquartered at 15 rue des Halles 75001, Paris, France (“**Mistral AI**” or “**Us**”) and any person or entity who accesses, uses, or subscribes to Our Services through a Cloud Provider (the “**Customer**” or “**You**”). These Deployment Terms govern and apply to any use by You of Our Services through a Cloud Provider. By subscribing to, accessing, or using Our Services through a Cloud Provider (as defined below), You accept and agree to be bound by these Deployment Terms. These Deployment Terms do not supersede the Cloud Provider Terms (as defined below).

**Key points.** Before You read Our Deployment Terms, here are some key points to understand about Our Services:

* You can use Our Models directly on the Cloud Infrastructure provided by Your Cloud Provider. We have zero access to Your Cloud Infrastructure.
* When using Our Models through a Cloud Provider, You have a contract with them and they become Your main point of contact. They handle Your subscription, contract termination, fee collection, and provide technical support. If they need Our assistance to provide You with technical support, they will reach out to Us.
* Your Cloud Provider is solely responsible for the cloud infrastructure they provide to You. This means they’re in charge of the availability and security of the Model and Your User Data. If You have any issues or questions about the cloud infrastructure, please contact them directly.
* Your Data is Yours, and only Yours. We do not access Your Models or Data (if any) stored on the cloud provider’s infrastructure. You have full control and ownership of your User Data, including Your User Input Data, Output and Prompts.
* Rest assured that We respect Your data privacy and ownership rights. We cannot access Your Data and We never use Your data for any purpose, including to train, retrain or improve Our Models.

**Purpose.** The purpose of these Deployment Terms is to set forth the rights and responsibilities of Mistral AI and the Customer in connection with the use of the Services through a Cloud Provider.

**Scope.** For clarity, Mistral AI’s Terms of Use, API Deployment Terms, Le Chat Deployment Terms, or Data Processing Agreement do not apply to Your use of Our Services through a Cloud Provider.

1\. Definitions
---------------

The capitalized words in these Deployment Terms shall have the meaning set forth below:

* “**Affiliate**”: means any person, persons or entity which own or control, are controlled by, or are under common ownership or control with that person from time to time, where control or controlled relates to the possession, direct or indirect, of the power (whether direct or indirect) to direct or cause the direction of its management, policies or affairs, whether by means of holding shares, possessing voting power, exercising contractual powers or otherwise.
    
* “**Applicable Data Protection Law**”: means (i) Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 applicable since 25 May 2018 (the “GDPR”) and (ii) the data protection laws and regulations applicable in France.
    
* “**Authorized Users**”: means You or Your Affiliates’ employees and/or independent contractors, allowed by You to access and use the Services, subject to the Authorized Users’ compliance with the applicable terms associated with Authorized Users in these Deployment Terms.
    
* “**Cloud Provider**”: means any third-party partner who distributes Our Services to You, in the name of Mistral AI, via their Cloud Provider Infrastructure.
    
* “**Cloud Provider Fees**”: means the fees paid or payable by You to the Cloud Provider, in consideration for the Services.
    
* “**Cloud Provider Infrastructure**” or “**Cloud Infrastructure**”: means the infrastructure provided by a Cloud Provider and that You use to host and run the Solution.
    
* “**Cloud Provider Terms**”: means the terms entered by and between You and the Cloud Provider. Mistral AI is not a party to the Cloud Provider Terms.
    
* “**Confidential Information**”: means any and all information, in any medium, which is provided by one party to these Deployment Terms (“Discloser”) to the other party (“Recipient”), that is either (i) related to business practices, financial statements, financial information, pricing, customers, products, methods, know-how, techniques, processes, apparatuses, trade secrets, proprietary knowledge and employee data; or (ii) marked using a legend such as “confidential”, “proprietary” or similar words, or if disclosed orally must be confirmed as such by the Discloser; or (iii) any information which the Recipient should have reasonably considered to be confidential under the circumstances surrounding disclosure. In the context of this Agreement, Confidential Information include but is not limited to the terms and conditions of the Model (including the weights), the Modified Model, the Solution, the weights of the Model or the Modified Model, any information regarding the training dataset of the Model, and any non-public information or material regarding the Solution and the associated Services.
    
* “**Customer**” or “**You**”: means any person who uses, accesses, browses and/or subscribes to the Services, and its Affiliates (if any).
    
* “**Customer Products and Services**”: means any of Customer owned and/or licensed products or services, including but not limited to any applications edited or operated by Customer, that Customer makes available using the Services or the Outputs.
    
* “**Data Controller**”: means the legal person who determines the means and the purposes of the processing of Personal Data.
    
* “**Data Processor**”: means the legal person that processes the Personal Data on behalf of the Data Controller and under its documented instructions.
    
* “**Deployment Terms**”: means this agreement entered into by and between the Parties and governing the use of Our Services, including but not limited to the Specific Access Services. These Deployment Terms do not supersede the Cloud Provider Terms regarding the Cloud Provider or the Cloud Provider Services.
    
* “**Effective Date**”: means the earlier of (i) the date You first use Our Services or (ii) the date You accept these Deployment Termss.
    
* “**Filters**”: means the automatic mechanisms such as moderation prompts implemented by Mistral AI to the Models designed to screen or remove offensive, inappropriate or illicit content from the Output.
    
* “**Losses**”: means any loss, damage, liability, charge, expense, outgoing or cost of any nature or kind.
    
* “**Mistral AI’s Intellectual Property**”: means the Model(s), the Services, and any trade names, trademarks, logos, patents, trade secrets, know-how, designs, drawings, copyrights, engineering, photographs, samples, software, models, algorithm, image, literature, information, ideas, concepts, or improvements pertaining to the Services and other data of any kind that is protectable through copyrights, patent, trade secrets, trademarks, trade dress, service marks, or and includes any modification or enhancement of the Services.
    
* “**Model(s)**”: means (i) any version of any artificial intelligence model developed by Mistral AI and made accessible on the Cloud Provider Infrastructure including but not limited to the model weights (the “**Weights**”) and (ii) the associated documentation as may be amended from time to time (the “**Documentation**”).
    
* “**Modified Model**” or “**Fine-Tuned Model**”: “Modified Models” or “Fine-Tuned Models”: means any modified, enhanced, fine-tuned or customized version of a Model.
    
* “**Parties**”: means You and Mistral AI. In the singular, “Party” means one of the Parties.
    
* **“Personal Data**”: means any data related to an identified or identifiable natural person.
    
* “**Services**”: means the services provided by Mistral AI to You under these Deployment Terms, including but not limited to the Model(s), the Documentation and any other services provided by Mistral AI along with any associated software or application.
    
* “**Specific Access**”: means the services consisting in making the Model available to You on a Cloud Provider Infrastructure, where You can access the Weights of the Model.
    
* “**Technical Support**”: means the service consisting in fixing the problems notified by You to the Cloud Provider, and notified by the Cloud Provider to Us, where applicable.
    
* “**User Data**” or “**Data**”: means any of the following data:
    
    * “**User Input Data**”: means any data that is used by You for the purpose of prompting, fine-tuning or customizing the Services to Your specific needs or use-case, for the duration of these Deployment Terms.
    * “**Outputs**”: means any and all content generated by the Services in response to a Prompt.
    * “**Prompts**”: means any and all instructions, queries or textual cues given by You to the Services in order to generate an Output.

2\. Allocation of responsibilities
----------------------------------

**Our responsibility**. During the Term and subject to Your compliance with these Deployment Terms, Mistral AI will provide the Services to You in compliance with these Deployment Termss. The Services consist of:

* Granting You the right to use the Solution on the Cloud Provider Infrastructure, and
* Providing Technical Support through the Cloud Provider.

**The Cloud Provider’s responsibility.** Subject to the Cloud Provider Terms, the Cloud Provider is responsible for:

* Providing You with the Solution on the Cloud Provider Infrastructure,
* Providing the Cloud Provider Services, including technical support services,
* Billing the Services,
* Collecting the Cloud Provider Fees, and
* Being Your sole point of contact for (i) any request or notice related to Our Services, (ii) any Technical Support request You may have, and (iii) transferring such requests to us, if the Cloud Provider is not capable of providing You with the requested assistance.

**Your responsibility.** Subject to the Cloud Provider Terms, You are responsible for:

* Where applicable, integrating Our Services into Your Customer Products and Services, subject to the license granted in this Section. We are not responsible for any damage resulting from any failure to integrate Our Services into the Customer Products and Services,
* Using Our Services and Your User Data in compliance with these Deployment Terms,
* Paying the Cloud Provider Fees to the Cloud Provider, and
* Sending any notice or request You may have about Our Services solely to the Cloud Provider.

3\. Your use of Our Services
----------------------------

### 3.1. Terms applicable to any use of Our Services through a Cloud Provider

**License.** Subject to the terms and conditions of these Deployment Terms, We grant You a limited, revocable, non-exclusive, license to use the Services during the Term for the purpose of (i) integrating the Services into Your products and services, including but not limited to the Customer Products and Services, and/or (ii) using the Services for Your internal business purposes, in each case in compliance with these Deployment Terms. The license granted herein is non-sublicensable and non-transferable, except for the following cases: You may transfer and sublicense the rights granted herein (i) the Customer’s Affiliates and the Authorized Users to access and use the Cloud Services, and (ii) the End Users to access and use the Services through the Customer Products and Services (if any) to submit Prompts and generate Outputs. In any case, You must not distribute or market Our Solution as a stand alone product or service and/or act as a distributor of Our Solution.

**Technical Support.** Any request for Technical Support must be made to the Cloud Provider, in accordance with the Cloud Provider Terms. Any request for Technical Support made directly to Mistral AI will not be processed.

**Cloud Provider Infrastructure.** If You subscribe to Our Services through a Cloud Provider, You will be able to use Our Solution on the Cloud Provider Infrastructure.You acknowledge and agree that the Cloud Provider Infrastructure is the sole responsibility of the Cloud Provider and that any issues or claims arising from or relating to the Cloud Provider infrastructure or services, including but not limited to, uptime, downtime, data loss, or security should be directed to and addressed by the Cloud Provider pursuant to the Cloud Provider Terms. We disclaim all liability and responsibility for any issues or claims arising from or relating to the Cloud Provider Infrastructure or any service provided by the Cloud Provider to You. Given that Mistral AI is not providing the Cloud Provider Infrastructure and has no access or use of User Data, You further acknowledge and agree that We do not provide any warranties, express or implied, regarding the availability and reliability of any User Data stored or processed on the Cloud Provider Infrastructure and regarding the availability of the Services. You acknowledge and agree that Our sole responsibility with respect to the Services is to (i) grant You the right to use the Solution on the Cloud Provider Infrastructure and (ii) provide You with Technical Support, in each case subject to the terms of the Service.

**Your obligations.** You must use Our Services in compliance with these Deployment Terms and all applicable laws and regulations, including but not limited to all intellectual property, data and privacy laws. Therefore, You are responsible for:

* **Your use of the Services.** You shall:
    
    * Not use the Services for any illicit, unlawful, prohibited and/or illegal purposes, to harm third parties or Mistral AI.
    * Comply with the license granted in Section 3.1 of these Deployment Termss and not use Our Services in a way that is not provided for by such license; .
    * Not use the Models to circumvent the intended features, functionality or limitations of the Model or to divert the Models from their intended purposes as set forth in these Deployment Terms.
    * Not infringe the rights of third parties, including but not limited to intellectual property rights or privacy. You shall only use Prompts to which You own all required rights under applicable law and to do so in a manner that is consistent with the applicable law.
    * Provide a disclaimer to any individual accessing an Output or using the Services. Such disclaimer should highlight the potential inaccuracies and unpredictabilities in the Outputs and encourage individuals to check important information.
* **Your User Data.** You must:
    
    * Not represent that the Output was generated by a human when it was generated by a Model.
    * Not use the Output if You are aware that it infringes the rights of third parties, including but not limited to intellectual property rights.
    * Comply with the Applicable Data Protection Laws if You use Personal Data as part of Your User Data.
* **Not infringing Mistral AI’s Intellectual Property.** Subject to applicable law, You must:
    
    * Not remove or otherwise obscure any copyright or proprietary notices on the Model, including but not limited to Our brands, trademarks or any other copyright notice.
    * Not bypass, attempt to bypass, alter, disable or in any way interfere with the digital rights management measures that may be integrated to the Services. You acknowledge that these protection mechanisms are essential for safeguarding the intellectual property and security of the Services.These digital rights management do not allow Us to access Your User Data, or the Cloud Provider Infrastructure.
    * Not seek to or reverse engineer, disassemble, decompile, translate or otherwise seek to obtain or derive the source code, underlying ideas, algorithms, file formats or non-public APIs to any Services, except to the extent expressly permitted by applicable law (and then only upon advance notice to Us).
    * Not use Outputs to reverse-engineer Our Services.

**Affiliates and Authorized Users.** You are responsible for Your Affiliates’ and Authorized User’s compliance with these Deployment Terms.

**Third Parties.** You shall not encourage or assist any party in doing anything that is strictly prohibited under these Deployment Terms.

**Model discontinuation.** We reserve the right, at Our sole discretion, to discontinue the availability of Models. In the event that We elect to discontinue the availability of any Model, We shall provide a minimum notice period of six (6) months prior to the discontinuation of the Model. During such notice period, the Model will not be available to any new Customers. We will notify You of any such discontinuation by email to the email address provided during registration. You are responsible for ensuring that Your contact information is up-to-date to receive such notifications. We shall not be liable for any damages, costs, or expenses incurred by You as a result of the discontinuation of any Model, provided that We have complied with the notice periods set forth above.

### 3.2. Terms applicable to the use of Specific Access Services

**Specific Access.** If You Subscribe to the Specific Access, the Cloud Provider hosts the Solution on Cloud Provider Infrastructures that grants You access to the source form of Our Solution, including Model Weights. As a consequence, the use of the Specific Access is subject to Appendix 1, in addition to these Deployment Terms.

4\. Your User Data
------------------

**No Access to Your User Data.** We do not access Your User Data, unless (a) You grant us such access for Technical Support purposes, and (b) such access is strictly necessary to fix a problem You reported to the Cloud Provider.

**User Data ownership.** You are the sole owner of Your User Data. We make no claim to ownership of Your User Data.

**Model Training.** We do not use Your User Data for any purpose, including to train, retrain or improve Our Models.

**Your responsibility.** You are solely responsible for Your use of Your Prompts and Outputs. You must not intentionally make the Model generate Outputs infringing intellectual property rights, third party rights or applicable law, or use such infringing Outputs after You become aware of such infringement.

**Output similarity.** You agree that, due to the nature of Our Services, if another User uses a Prompt similar to Yours, our Services may generate an Output similar or identical to Yours. We do not warrant that Your Output is not similar or identical to another User’s Output. Consequently and unless otherwise stated, we will not indemnify You in case Your Output is similar or identical to another User’s Output.

**Output accuracy.** You acknowledge and agree that Our Services are inherently subject to certain unpredictabilities, particularly with the Outputs generated, as such Outputs depend on Your Prompt and as the technology behind Our Services is complex and continuously evolving. To the extent permitted by applicable law, Our Services are provided without any express or implied warranty regarding the quality or the accuracy of the Outputs. Consequently, You agree that the Outputs generated by Our Services may be incomplete, not up-to-date or not entirely accurate. Therefore, when using Our Services, You must:

* Ensure the quality of Your Prompts,
* Assess the accuracy and/or the suitability of the Output to your needs before using or sharing the Output,
* Include in Your Prompt any relevant moderation prompt to better filter or adapt the Output, especially if You deactivated the Filters proposed by Mistral AI or by Cloud Provider, and
* Check the information generated by the Output and, in any case, not rely on the Output as a unique source of truth and/or information, as safe and inoffensive in every circumstance, or as a replacement for professional guidance.

**Moderation.** We make commercially reasonable efforts to make sure Our Services do not generate Outputs that contain offensive, inappropriate or illicit content. To this end, We have implemented and / or are proposing different moderation mechanisms such as Filters to Our Model(s). If You deactivate these Filters, You agree that (a) We do not warrant that the Output generated will not be offensive, inappropriate or illicit, (b) You are solely responsible for the use of Your Output and (c) You shall in no way use the Output for any illicit or unlawful purpose and/or to harm Mistral AI and/or a third party.

5\. Payment
-----------

**Cloud Provider Fees.** You must pay the applicable Cloud Provider Fees and any taxes related to the use of the Services to the Cloud Provider.

6\. Term, suspension and termination
------------------------------------

### 6.1. Term

**Term**. These Deployment Terms are effective as of the Effective Date and shall continue until (i) terminated under this Section or (ii) the Cloud Provider otherwise ceases to make the Services available to You as a Cloud Provider Service.

### 6.2. Suspension

**Suspension.** Mistral AI may require the Cloud Provider to suspend Your access to any or all of Our Services (i) if You breach these Deployment Terms, (ii) for immediate security concerns, (iii) if the Cloud Provider has suspended or terminated Our right to provide Our Services through the Cloud Provider Infrastructure, and subject to applicable wind-down period between You and the Cloud Provider.

**Notice.** We will use commercially reasonable efforts to request the Cloud Provider to provide You with a written notice of any suspension of Your access to Our Services under this Section 6 of Our Deployment Terms and provide You with regular updates, where applicable.

**Resumption of Our Services.** We will use commercially reasonable efforts to request the Cloud Provider to resume providing You access to Our Services as soon as reasonably possible after the event giving rise to the suspension is cured (if possible). We will have no liability for any Losses or other consequences You may incur as a result of the suspension of Our Services.

### 6.3. Termination

**Termination for breach.** Either Party may terminate these Deployment Terms if the other Party fails to cure a material breach of these Deployment Terms within thirty (30) days after notice of such breach, provided that such breach is remediable. For illustrative purposes, the unauthorized use or misuse of the Services by You (which includes, for instance: if You use Our Services for a purpose that is not authorized by the license granted in Section 3.1. Of these Deployment Terms or if You reverse engineer Our Services) , Your Affiliates or an Authorized User will be considered a material breach of these Deployment Terms.

**Effect of Termination.** Upon termination of these Deployment Terms, (i) all rights granted to You under these Deployment Terms will terminate, (ii) You will no longer access or use Our Services on the Cloud Provider Infrastructure.

**Survival.** The following provisions, in their relevant parts, will survive termination or expiration of these Deployment Terms, each for the duration necessary to achieve its own intended purpose (e.g. the liability provision will survive until the end of the applicable limitation period): Section 6 (Term, suspension and termination), Section 7 (Liability), Section 8 (Intellectual Property), Section 9 (Warranties and Indemnification), Section 11 (Confidentiality) and Section 14 (Governing law and competent jurisdiction).

7\. Liability
-------------

**Disclaimer.** To the extent permitted by applicable law, Mistral AI will not be liable:

* In case of a Force Majeure Event (as defined below),
* For the Cloud Provider Infrastructure and any damage caused by such Cloud Provider Infrastructure,
* For Your use of such User Data,
* For Your breach of these Deployment Terms,
* For the performance of the Model and/or the Modified Model in case You or any third-party customized, fine-tuned or otherwise modified the Model,
* In case Your Output is similar or identical to a third-party’s Outputs,
* For the Outputs, if such Outputs are modified by You,
* For any loss of profits, income, revenue, business opportunities, loss or corruption of data or information,
* For any failure to realize expected revenues or savings, loss or damage to goodwill, pure economic loss or other economic or pecuniary loss (regardless of whether any of these type of loss or damage are direct, indirect, special or consequential), or
* For any indirect, special, incidental, punitive, exemplary, incidental or consequential damages of any kind, even if informed of the possibility of such damages in advance.

**Liability Cap.** To the extent permitted by law, the total aggregate liability of Mistral AI in respect of any Losses incurred by the Customer under or in relation to these Deployment Terms will not exceed, in the aggregate the amount of the Cloud Provider Fees paid or payable by the Customer to the Cloud Provider in connection with the performance of these Deployment Terms in the twelve (12) calendar months preceding the date on which the first such event or events occurred.

**Multiple claims.** The existence of one or more claims under these Deployment Terms will not increase the above mentioned liability cap. You agree that any Losses or claim You may have under these Deployment Terms can only be recovered once and any such claim will exhaust all and any other claims that might otherwise arise against Mistral AI in relation to which You or Your Affiliates have been compensated or otherwise reimbursed.

**Legal action.** You agree that the limitations specified in this section apply regardless of the form of action, whether in contract, tort (including negligence), strict liability or otherwise.

8\. Intellectual Property
-------------------------

**Your intellectual property.** You remain the sole owner of all right, title and interest, including all intellectual property rights in and to Your User Data and Customer Products and Services.

**Mistral AI’s Intellectual Property.** Mistral AI remains the sole owner of all right, title and interest, including all intellectual property rights in and to Mistral AI’s Intellectual Property, including but not limited to the Model(s), the Documentation and the Services. The Services are made available on a limited access basis, and no ownership right is conveyed to You, irrespective of the use of terms such as “purchase” or “Subscription”. Any representation or reproduction, in whole or in part, of the Services, by any process whatsoever, without Mistral AI’s prior express authorisation, is strictly prohibited and will constitute an infringement punishable by the provisions of the applicable law.

**Modified Model.** During the term of these Deployment Terms and following any termination, Mistral AI does not access or use the Modified Models stored on the Cloud Provider Instances and does not share the Modified Models with any third-party. You will have no rights in the Modified Model after the expiration or termination of this Agreement. Upon termination or expiration of these Deployment Terms, (i) Your rights to use the Modified Model will automatically terminate, (ii) Mistral AI will still have no rights to use of access the Modified Model, (iii) You shall promptly cease all use of the Modified Model and (iv) Mistral AI will request the Cloud Provider to delete the Modified Model.

9\. Warranties and indemnification
----------------------------------

### 9.1. Mutual warranties

**General warranties.** Each Party represents and warrants that You have the authority and capacity to enter into these Deployment Terms.

### 9.2. Mistral AI warranties

**Services provided “as is”.** The Services are provided to You “as is”. To the extent permitted by applicable law, We make no representations or warranties regarding the accuracy, reliability, or completeness of the Services or their suitability for Your specific requirements or use-case. Without limiting Our express obligations under these Deployment Terms, We do not warrant that Your use of the Services will increase Your revenues, be error-free, uninterrupted or that We will review Your User Data for accuracy. You acknowledge and agree that any use of the Services is at Your own risk, and Mistral AI shall not be liable for any Losses arising from Your misuse, unauthorized or unlawful to use the Services, or from the non- suitability of the Services to Your specific requirements or use-case, including but not limited to direct, indirect, incidental, consequential, or punitive damages.

**Mistral AI Warranties.** Mistral AI warrants that:

* The Services comply with the Applicable Data Protection Law,
* Mistral AI has the rights to all the intellectual property made accessible to You in the context of these Deployment Terms, and
* The Services, when made available to the Cloud Provider for distribution, are free from any virus or malwares.

### 9.3. Your warranties

**Customer warranties.** You represent and warrant that:

* You have the authority to enter into these Deployment Terms,
* You will use the Services in accordance with the applicable laws and regulations and these Deployment Terms and that the Customer will not use the Services to commit illegal acts, including in particular to harm third parties, and
* You have obtained all necessary intellectual property rights, including but not limited to, copyrights, patents, trademarks, and trade secrets, or have been granted the appropriate licenses, permissions, and consents to use and provide the Prompts and User Input Data for the purpose of using Our Services.

### 9.4. Indemnification

**Indemnification by Mistral AI.** Mistral AI shall indemnify, defend, and hold You harmless against any liabilities, damages and costs (including reasonable attorneys’ fees) payable to a third party arising out of a third party claim arising out of, related to or alleging that the Services infringe any third party intellectual property right. We will not indemnify You for any claim arising out of, related to:

* The combination of the Services with the Customer Products and Services or third-party software (including but not limited to the Customer Products and Services), hardware or any other equipment not provided by Mistral AI,
* Any modification of Our Services made (i) by You or a third-party, including but not limited to any fine-tuning of Our Models, or (ii) by Us following Your request,
* The Cloud Provider Infrastructure or Services,
* Your User Data or Your use of Your User Data, including but not limited to any modification by You or a third-party of an Output,
* Any breach by You of these Deployment Terms, or
* Your failure to comply with the applicable laws and regulations.

To the extent permitted by applicable law, the liability cap set out in Section 7 of these Deployment Terms shall apply to the indemnification obligations under this Section.

**Indemnification by You.** You agree to indemnify, defend, and hold Mistral AI and its affiliates and licensors harmless against any liabilities, damages, and costs (including reasonable attorneys’ fees) payable to a third party arising out of a third party claim arising out of or related to Your use of Our Services, including (i) Your use of the Services in violation of these Deployment Terms or applicable law, (ii) Your Customer Products and Services (if any) and/or (iii) Your User Data. Nothing in this Agreement should limit the Customer’s obligation to indemnify Mistral AI of such a claim in case of unauthorized use of the Services by the Customer or the Authorized User.

**Indemnification Procedure.** The indemnification obligations this section of these Deployment Terms are subject to the indemnifying Party (a) receiving a prompt written notice of such claim ; (b) being granted the exclusive right to control and direct (including the authority to elect legal counsel) the investigation, defense or settlement strategy of such claim and (c) benefitting from all reasonable necessary cooperation and assistance, including access to the relevant information, by the indemnified Party at the indemnifying Party’s expense. Mistral AI shall consult Customer before entering into any settlement or compromise of any claim, and shall take into account all reasonable comments from Customer.

**Remedies.** The remedies in this section are the sole and exclusive remedies for any third-party claim.

10\. **Personal Data**
----------------------

**Personal Data.** We do not access or process any Personal Data contained in Your User Data when You use Our Services, unless You grant us access to Your Personal Data as part of our technical support services. In such a case, the Data Processing Agreement will apply and We will process such Personal Data as Data Processor. In any case, You are the Data Controller for any processing of Personal Data carried-out by You when You use Our Services.

11\. Confidentiality
--------------------

**Confidentiality.** Each Party acknowledges that during the course of performing its obligations hereunder it may receive or disclose Confidential Information. Each Party expressly acknowledges that the Confidential Information of the other Party consists of trade secrets and proprietary information having significant commercial value, and that knowledge of all or any part of the Confidential Information would potentially yield a competitive advantage over others not having such knowledge. For clarity, the Weights of the Model or the Modified Model are Confidential Information.

**Obligations.** Accordingly, neither Party will (i) use the Confidential Information of the other Party except to exercise rights or perform obligations under this Agreement or (ii) disclose the Confidential Information of the other Party to any third party except to the Recipient’s directors, employees, or consultants to the extent necessary to carry out the purposes of this Agreement, provided that all such recipients are obligated by a written agreement containing confidentiality obligations at least as stringent as described herein. Each Party will take such steps as may be reasonable in the circumstances, or as may be reasonably requested by the other party, to prevent any unauthorized disclosure, copying or use of the Confidential Information by such third parties. Each Party may also disclose Confidential Information to the extent required by judicial or governmental order or as necessary to comply with any applicable law or regulation governing regulated businesses or the issuance of securities to the public, provided that the party making the disclosure gives the other party reasonable notice prior to such disclosure and, in the case of a judicial or governmental order, complies with any applicable protective order or equivalent. The Recipient agrees to exercise due care in protecting Discloser’s Confidential Information from unauthorized use and disclosure, and at a minimum will use at least the degree of care a reasonable person would use.

**Term.** The obligations of the Parties under this Section shall survive the expiration or termination of this Agreement until such time as the information is no longer Confidential Information under this Agreement, except through a breach by either Party of their confidentiality obligations under this Agreement.

**Exclusions.** Confidential Information herein shall not include information that (i) the Recipient can demonstrate by its written records to have had in its possession prior to disclosure by the Discloser, (ii) was part of the public knowledge or literature, not as a result of any action or inaction of the Recipient, (iii) was subsequently disclosed to the Recipient from a source other than the Discloser who was not bound by an obligation of confidentiality to the Discloser, (iv) the Recipient can demonstrate by its written records to have been independently developed by the Recipient without the use, directly or indirectly, of any Confidential Information, or (v) the Recipient is required to disclose pursuant to a court order or as otherwise required by law, provided, however, that the Recipient notifies the Discloser within sufficient time to give the Discloser a reasonable period to contest such order.

**Breach of Confidentiality.** Each Party will promptly notify the other Party if it knows or suspects a breach of confidentiality of the other Party’s Confidential Information has occurred. Each Party reserves the right to seek indemnification, including but not limited to damages or any other form of compensation, from the other Party for any breach by the Customer of its confidentiality obligations.

12\. Changes to these Deployment Terms
--------------------------------------

**Update.** We may update these Deployment Terms at any time. Any modification of these Deployment Terms will be effective thirty (30) days after they are posted by the Cloud Provider or You otherwise receive notice. Any modification to these Deployment Terms will not apply retroactively.

13\. General provisions
-----------------------

**Non waiver.** The fact that either of the Parties does not claim application of any clause whatever of these Deployment Terms or condones its non-performance, shall not be construed as a waiver by that Party to the rights stemming for it from said clause. A waiver of any right or remedy under these Deployment Terms or by law is only effective if given in writing and shall not be deemed a waiver of any subsequent right or remedy.

**Severance.** If a court or any other competent authority finds any provision of these Deployment Terms (or part of any provision) to be invalid, illegal or unenforceable, that provision or part provision shall, to the extent required, be deemed deleted, and the validity and enforceability of the other provisions of these Deployment Terms shall not be affected. If any invalid, unenforceable or illegal provision of these Deployment Terms would be valid, enforceable and legal if some part of it were deleted, the provision shall apply with the minimum modification necessary to make it legal, valid and enforceable.

**No-partnership.** Nothing in these Deployment Terms is intended to, or shall be deemed to constitute a partnership or joint venture of any kind between any of the Parties, nor constitute any Party the agent of another Party for any purpose. No Party shall have authority to act as agent for, or to bind, the other party in any way. Neither You nor Mistral AI will suggest or claim any sponsorship, endorsement or affiliation with the other party, unless such a relationship is governed by a separate agreement.

**Entire agreement.** These Deployment Terms constitute the entire agreement between the Parties relating to the Services, and any other subject matter covered by these Deployment Terms and supersedes all prior or contemporaneous oral or written communications, proposals and representations between the Parties, with respect to the Services or any other subject matter covered by these Deployment Terms.

**Force Majeure.** Neither Party will be liable to the other for any delay or failure to perform any obligation under these Deployment Terms if the delay or failure is due to events which are beyond the reasonable control of such party, such as a strike, blockade, war, act of terrorism, riot, natural disaster, failure or diminishment of power or telecommunications or data networks or services, or refusal of a license by a government agency (the “Force Majeure Event”).

14\. Governing law and dispute resolution
-----------------------------------------

**Governing law.** These Deployment Terms and any dispute or claim (including non-contractual disputes or claims) arising out of or in connection with it or its subject matter or formation shall be governed by and construed in accordance with the laws of France.

**Informal resolution.** In the event of any controversy or claim arising out of or relating to these Deployment Terms, the Parties will consult and negotiate with each other and, recognizing their mutual interests, attempt to reach a solution satisfactory to both Parties.If the Parties do not reach settlement within a period of sixty (60) days, either Party may escalate the controversy or to the senior executives or both Parties (the “Executives”). The Executives shall then promptly engage in discussions and negotiations to seek a mutually agreeable resolution in the best interest of both Parties.

**Formal resolution.** If You are located in France, the Parties agree that the courts of Paris, France shall have exclusive jurisdiction over any disputes arising out of or in connection with these Deployment Terms or their subject matter or formation. If You are not located in France, all disputes arising out of or in connection with these Deployment Terms or their matter or formation shall be finally settled under the rules of arbitration of the international chamber of commerce (the “ICC”) by one arbitrator appointed in accordance with the said rules. The arbitration proceedings shall take place exclusively at the ICC headquarters in Paris, France. The appointed arbitrator shall adjudicate the dispute in accordance with the applicable law.

**Appendix 1 - Terms applicable to Specific Access Services**

This Appendix sets out the terms and conditions applicable to the Specific Access. For clarity, this Exhibit applies in addition to the Deployment Terms. In case of any conflicts or discrepancies between the Deployment Terms and this Appendix, this Appendix shall prevail.

1\. Definitions
---------------

Any capitalized term that is not defined in the Deployment Terms shall have the meaning set out below:

* “**Authorized Individuals**”: means the Customer’s employees authorized to access the Closed-Weight Model’s source code and Weights.
* “**Closed-Weight Model**”: means a Model with the Weights not disclosed to the public, including any Modified Model.
* “**Security Incident**”: means any event having an actual or reasonably anticipated adverse effect on any element of the security of the Closed-Weight Model made available to the Customer under the Deployment Terms, or any event involving any breach of security leading to the accidental or unauthorized disclosure of, or access to the Closed-Weight Model and/or the Weights. This includes in each case a reasonably suspected or “near miss” event of which the Customer is aware.

2\. Security and confidentiality of the Model
---------------------------------------------

**Security and confidentiality.** You must ensure the confidentiality and security of the source form of the Closed-Weight Model and especially the Weights. To this end, You must always use and have in place the following security measures:

* **Administrative measures.**
    
    * Access to Closed-Weight Model’s source form and Weights shall be restricted solely to Authorized Individuals.
    * Authorized Individuals shall be subject to a confidentiality commitment substantially covering the following:
    * The Authorized Individuals shall maintain the Closed-Weight Model in strict confidentiality and segregated from the Customer’s team members not listed as Authorized Individuals,
    * The Authorized Individuals are expressly prohibited from exporting, sharing or disclosing the Closed-Weight Model source form and Weights to any individual not listed as an Authorized Individual, including team members, interns, external providers and third parties.
    * The Customer is responsible for providing training to Authorized Individuals on the confidentiality of the Closed-Weight Model and their corresponding obligations.
* **Technical measures.**
    
    * Access to the Closed Weight Models shall be restricted and monitored.
    * State of the art IT protection shall govern workstations of Authorized Individuals.
    * Authorized Individuals shall use a unique identifier for accessing and utilizing the Closed-Weight Model source form and Weights.
    * The Customer shall not copy the Closed-Weight Model or hold any copy of the Closed-Weight Model that is not stored on the Cloud Provider’s Infrastructure. For clarity, the Customer is prohibited from exporting the Closed-Weight Model and the Weights from the Cloud Provider Infrastructure.

3\. Traceability of the Model
-----------------------------

**Traceability of the Models.** Mistral AI implements security measures, such as watermarking, into each Closed-Weight Model provided to the Customer, to ensure the Model’s traceability (the “**Traceability Measures**”). These Traceability Measures allow Mistral AI to distinctly identify each Closed-Weight provided to a Customer. These Traceability Measures do not allow Mistral AI to access or use the Customer’s User Data. The Customer shall not in any way intentionally attempt to delete or alter the Traceability Measures implemented by Mistral AI. Mistral AI keeps a record of the Traceability Measures implemented into each Model. The record of Traceability Measures is subject to strict security protocols, including but not limited to a strict authorization policy. Mistral AI warrants that it permanently destroys any copy of the Model (as provided to the Customer) in its possession and control from its own IT infrastructure, as soon as such Model has been made available to the Customer. Therefore, once the Closed-Weight has been made available to the Customer, the Customer shall be the sole holder of the Model.

4\. Unauthorized Disclosure of the Model
----------------------------------------

**Unauthorized disclosure of the Model.** In case of any unauthorized access or disclosure of all or part of the source code of the Model and/or the Weights (the “Leak”):

* Mistral AI shall appoint an independent expert (the “IT Expert”) to conduct an investigation. The costs of this investigation shall be borne by Mistral AI.
* The IT Expert shall possess the necessary skills and expertise to trace and identify the Model and determine its origin using the record of Traceability Measures.
* If the IT Expert’s investigation confirms that the leaked Model can be traced back to the Customer through the implemented Traceability Measures:
    * Mistral AI shall send the Customer an exact copy of the IT Expert’s report.
    * Mistral AI may terminate this Agreement immediately without prior notice and/or request from Customer indemnification under Sections 7 and 11 of the Deployment Terms.

In any case, the Customer shall not make or publish any public statement regarding the Leak without Mistral AI’s prior written consent.

5\. Security incident
---------------------

**Notification.** The Customer shall notify Mistral AI in writing about any Security Incident immediately (and in any event within 24 hours) after the Customer is aware that a Security Incident has occurred and will update any notification given with any subsequent relevant information and will attend any meetings with Mistral AI on request by Mistral AI in relation to such notification. This notification will contain the following information:

* The time the Security Incident occurred and the duration of the Security Incident,
* The Closed-Weight Model affected by the Security Incident,
* The nature and impact of the Security Incident,
* The measures which have been taken or which are proposed to be taken to address the Security Incident and to mitigate its possible adverse effects.

Where it is not possible to provide all such information at the same time, the information may be provided in phases without undue delay, but the Customer may not delay notification under this section on the basis that an investigation is incomplete or ongoing.

If the Security Incident resulted in a Leak, Mistral AI reserves the right to (i) terminate these Deployment Terms immediately without prior notice and/or request indemnification from Customer under Sections 7 and 11 of the Deployment Terms.

6\. intellectual Property
-------------------------

**Modified Model.** Mistral AI does not access the Modified Models stored on the Cloud Provider Instances and does not share the Modified Models with any third-party. However, Mistral AI retains sole and exclusive ownership of all right, title, and interest in and to the Modified Model, including all intellectual property rights (such as but not limited to, copyrights, patents, trademarks, and trade secrets) therein. For clarity, the license granted in Section 3 of these Deployment Terms also applies to the Modified Model. Mistral AI shall not use the Modified Model. Upon termination or expiration of these Deployment Terms, (i) Your rights to use the Modified Model will automatically terminate, (ii) Mistral AI will still have no rights to use of access the Modified Model, (iii) You shall promptly cease all use of the Modified Model and (iv) Mistral AI will request the Cloud Provider to delete the Modified Model.

* * *

Privacy Policy
==============

Preamble
--------

**Overview.** At Mistral AI, we are committed to protecting your privacy and ensuring the security of your personal data. This Privacy Policy is designed to help you understand how we collect and process your personal data when you use our Services.

**Scope.** This Privacy Policy does not apply where Mistral AI processes personal data on behalf of the Commercial Customer, as Data Processor. In such a case, the [Data processing Agreement](https://mistral.ai/terms/#data-processing-agreement) controls the processing of personal data by Mistral AI as Data Processor.

1\. Definitions
---------------

The capitalized words in this document will have the meaning given below:

* **“Agreement”:** means the service agreement made by and between You and Mistral AI composed of (i) the Terms of Use, (ii) the applicable Additional Terms and, where applicable, (iii) the [Data processing Agreement](https://mistral.ai/terms/#data-processing-agreement).
    
* **“Data Controller”:** means the person who makes decisions on Your Personal Data. For instance, the Data Controller decides which Personal Data to collect, where to store such data, for how long, etc.
    
* **“Data Processor”:** means the person who uses Your Personal Data on behalf of the Data Controller and under the Data Controller’s instructions. For instance, our hosting services provider acts as Data Processor when it stores Your Personal Data on Our behalf and under Our instructions.
    
* **“La Plateforme”:** means the online platform available at [http://console.mistral.ai](http://console.mistral.ai/) in SaaS mode, and all its features and components.
    
* **“Le Chat”:** means the online chat interface where You can interact with Our Services, including Our Models.
    
* **“Mistral AI” or “We”:** means Mistral AI, a French entity registered at the Trade register of Paris under number 952 418 325, having its corporate seat at 15 rue des Halles 75001, Paris, France and its affiliates.
    
* **“Mistral AI Training Data”:** means Mistral AI’s data set used to train Mistral AI’s Models.
    
* **“Model”:** means any of Our artificial intelligence models that we make available to You as part of Our Services.
    
* **“Personal Data”:** means any data that directly or indirectly relates to You.
    
* **“Privacy Policy”:** means this document describing the Processing activities carried-out by Mistral AI as Data Controller.
    
* **“Processing”:** means any operation relating to Your Personal Data (for instance: collection, use, access, transfer, deletion, etc.).
    
* **“Services”:** means any and all services provided by Mistral AI to You via the platform available at [https://www.mistral.ai](https://www.mistral.ai/), including La Plateforme and Le Chat (as defined in their respective Additional Terms). The Services may be free of charge (the **“Free Services”**) or made available subject to Fees (the \*\*“Paid Services”).
    
* **“User Data”:** means
    
    * **The “Feedback”:** means Your feedback pertaining to the accuracy, relevance, and effectiveness of the Outputs, including but not limited to any identified discrepancies or errors.
    * **The “Input”:** means any data (for instance textual data, prompts, fine-tuning data, documents, images, etc.) You provide or use for the purpose of prompting, fine-tuning or customizing the Services to Your specific needs or use-case, for the duration of this Agreement.
    * The **“Outputs”:** means any and all content generated by the Services in response to an Input. For the avoidance of doubt, Output does not include any component of the Models, including but not limited to the Model weights or parameters.
* **“You”** or the **“Customer”:** means any person who uses, accesses, browses and/or subscribes to the Services. The terms “Your”, and “Yours” are also used throughout this Agreement to specifically refer to such Customer. The Customer can be:
    
    * A **“Commercial Customer”:** means any Customer subscribing, accessing to or using the Services as part of its business or professional operations and that does not act as a Consumer.
    * A **“Consumer”**: means any Customer who is acting for purposes which are outside their trade, business, craft or profession

2\. Who is the Data Controller ?
--------------------------------

### 2.1. Mistral AI as Data Controller

Mistral AI is a French entity registered at the Trade register of Paris under number 952 418 325.

Your Personal Data may also be processed by Our affiliate located in the United Kingdom and in the United States.

You can contact us :

* By email at [privacy@mistral.ai](mailto:privacy@mistral.ai)
* By mail at Mistral AI, 15 rue des Halles, 75001 Paris, France.

### 2.2. Mistral AI as Data Processor

If You are a Commercial Customer, Mistral AI may also process Personal Data on Your behalf, as Data Processor.

In such a case, the Processing activities We carry-out are described in the [Data Processing Agreement](https://mistral.ai/terms/#data-processing-agreement) entered into between You and Us.

This Privacy Policy only covers the Processing activities We carry out as Data Controller and does not apply to the Processing activities carried-out as Data Processor.

3\. What kind of Personal Data do We collect ?
----------------------------------------------

### 3.1. Personal Data We collect directly from You

* **Identity, account and contact data:**
    * When You create your account on our platform to use Our Services;
    * When You subscribe to Our newsletter.
* **Payment and billing information.**
    * When You subscribe to Our Paid Services.
* **Inputs (if You include Personal Data in Your Input) and Feedback.**
    * When You use Our Services.

### 3.2. Personal Data generated by Your Use of Our Services

When You use Our Services, we collect the following information about You:

* Security logs;
* Technical information (cookies), subject to Your prior consent;
* Outputs, if You include Your Personal Data in Your Input or if You ask the Services to generate Personal Data.

### 3.3. Personal Data that is indirectly provided to Us

Our Models are trained on data that are publicly available on the Internet, which may contain Personal Data, even if we use good practices to filter out such Personal Data.

4\. Why do we use Your Personal Data?
-------------------------------------

We use Your Personal Data for the following purposes:

### 4.1. Provide Our Services

**If You are a Commercial Customer we use Your Personal Data to:**

* Create and administer Your account;
* Make aggregated and anonymized statistics about the use of Our Services.

**If You are a Consumer, we use Your Personal Data to:**

* Create and administer Your account;
* Provide Le Chat:
    * Generate Outputs based on Your Input;
    * Display Your Inputs and Outputs history on Le Chat;
    * Depending on the feature You use, share your User Data with our Data Processors to generate an Output.
* Provide La Plateforme:
    * Generate Outputs based on Your Input;
    * Allow You to Fine-Tune Our Models using Our Fine-Tuning API; and..
    * Allow You to create agents through the Agent Builder.
* Answer and manage Your Support requests, including fixing errors or bugs relating to Your use of Our Services.
* Make aggregated and anonymized statistics about the use of Our Services;

**Lawful basis:**

* Performance of the Agreement.
* Our legitimate interest when we make aggregated and anonymized statistics about the use of Our Services.

### 4.2. General administration of our Services

We use Your Personal Data to:

* Manage the security of Our Services;
* Communicate with You for purposes other than marketing (for instance, notify You in case of temporary unavailability of the Services); and
* Cookie Management.

**Lawful basis:**

* Our legitimate interests in administrating our Services.
* Your Consent for cookies that are not technically required.

### 4.3. Develop and train our Models

#### 4.3.1. General provisions

If Your Personal Data is publicly available on the Internet, and even if we apply good practices to filter out Personal Data, We may use Your Personal Data to train Our Models (large language models) to answer questions, generate text according to context/Inputs (e-mails, letters, reports, computer code, etc), translating, summarizing and correcting text, classifying text, analyzing feelings, etc.

**Lawful basis:** Our legitimate interests in providing state-of-the-art Models to Our Customers.

#### 4.3.2. La Plateforme

**If you use our Free Services on La Plateforme and include Personal Data in Your User Data**, We use commercially reasonable efforts to de-identify it before using it to train or improve Our Models (large language models) to answer questions, generate text according to context/Inputs (e-mails, letters, reports, computer code, etc), translating, summarizing and correcting text, classifying text, analyzing feelings, etc.

**If You use Our Paid Services on La Plateforme,** we do not use Your User Data to train or improve Our Models.

**Lawful basis:** Our legitimate interests in providing state-of-the-art Models to Our Customers.

#### 4.3.3. Le Chat

**If You use Le Chat and include Personal Data in Your User Data**, We will de-identify it before using it to train or improve our Models (large language models) to answer questions, generate text according to context/Inputs (e-mails, letters, reports, computer code, etc), translating, summarizing and correcting text, classifying text, analyzing feelings, etc.

**If You use Our Paid Services on Le Chat,** You can opt-out from the Mistral AI Training Data at any time, by using the opt-out feature on Your account. In such a case, We will not use Your User Data to train Our Models.

**Lawful basis:** Our legitimate interests in providing state-of-the-art Models to Our Customers.

### 4.4. Marketing operations

We use Your Personal Data to:

* Send you Our newsletters about Our Services.
* Lead development; and
* Invite You to Our events.

**Lawful basis:** Your Consent to receive our newsletters. Our legitimate interests to promote Our Services and to grow Our business for the other purposes.

### 4.5. Commercial Management

We use Your Personal Data for:

* Contract management.
* Invoicing the applicable fees.
* Processing Your payment.

**Lawful basis:** The performance of the Agreement. Our legal obligation to invoice for Our Services.

### 4.6. Dispute Resolution

We use Your Personal Data to:

* Investigate and resolve disputes
* Enforce Our contract (suspension of Your Account, monitor abuse, etc.)

**Lawful basis:** Our legitimate interest in protecting and exercising Our legal rights. The performance of the Agreement.

### 4.7. Data Subject Requests

We use Your Personal Data to:

* Reply to Your requests to exercise Your rights on Your Personal Data.

**Lawful basis:** Our legal obligation to reply to Your requests.

5\. How long do We store Your Personal Data ?
---------------------------------------------

We keep Your Personal Data for as long as necessary to achieve the purposes mentioned in Section 4 of this Privacy Policy. We may retain your Personal Data for longer periods when We are required by applicable law to do so or when it is necessary to exercise Our rights in legal proceedings.

For illustrative purposes, please find below the applicable data retention periods:

### Personal Data We use to Provide the Services:

* **Account data :** for the duration of your registration on the Services and for 1 year from the end of your registration for evidentiary purposes.
* **Security data:** the security logs are stored for 1 rolling year.
* **User Data** :
    * If You use Our Models APIs: for the duration necessary to generate the Output and for thirty (30) rolling days to monitor abuse;
    * If You use Our Fine-Tuning API: for the duration of your subscription on La Plateforme or until You delete it from La Plateforme by using the applicable feature;
    * If You use Le Chat: for the duration of Your registration on the Services (unless You delete Your User Data from Le Chat) and for 90 days.
    * If You use Our Free Services: Your User Data will be de-identified after the above mentioned retention periods and used to train or improve Our Models.

**Support data:** for the duration necessary to process Your Support request and for 5 years for evidentiary purposes.

### Personal Data We use to develop or train Our Models:

* **User Data :**
    * **If You use Our Free Services on La Plateforme or Le Chat** : Your User Data will be de-identified and used to train or improve Our Models.
    * **If You use Our Paid Services on Le Chat and have not opted-out of the MIstral AI Training Data:** Your User Data will be de-identified and used to train or improve Our Models. You can opt-out of the Mistral AI Training Data at any time by using Your account settings.

### Personal Data We use to administer Our Services:

* **Security Logs**: for a maximum period of 6 months;
* **Cookies**: For the duration of Your Consent, if applicable.

### Personal Data We use for commercial management purposes:

* **Identity, account and contact data, contract/subscription data:** for the duration of your registration on the Website and for 5 years from the end of your registration for evidentiary purposes.
* **Invoices:** for ten (10) years from the year-end date.

### Personal Data we use for marketing operations:

* **Leads identity and contact data:** 3 years from the collection of Your Personal Data.
* **Guests identity and contact data:** 1 year from the collection of Your Personal Data, unless You consent to Your Data being stored for a longer period of time.

### Personal Data We use for dispute resolution purposes:

* **Input and Output on the Models API:** for 30 rolling days to monitor abuse, unless you vactivated zero data retention.
* **Identity, account and contact data, contract/ subscription data:** for the duration of your registration on the Services and - for 5 additional years from the end of your registration for evidential purposes.
* **Legal data (e.g. court decision, legal evidence, etc. ):** until the expiration of the appeal period. We may retain such legal data for archival purposes.

6\. Who do we share Your Personal Data with?
--------------------------------------------

We may share Your Personal Data to the following persons on a need-to-know basis:

* The authorized members of our teams,
* Financial organizations (banks, etc.),
* Supervisory authorities such as the French data protection authority (CNIL), and
* Where appropriate, the competent courts, mediators, accountants, auditors, lawyers, bailiffs, debt collection agencies.

We may also share all or part of Your Personal Data with Our providers. Before engaging with any provider, we conduct audits to assess their privacy and security standards and we sign a dedicated data protection agreement.

Our main providers are:

* **Azure:**
    
    * **Purpose:** Cloud Infrastructure
    * **Data location:** Sweden
* **Black Forest Labs:**
    
    * **Purpose:** Image-generation based on Your Input. Black Forest Labs accesses Your Input and Output. Your Input is stored for 15 days and then permanently deleted from Black Forest Labs’ systems. Your Outputs are permanently destroyed within 1 day from Black Forest Labs’ systems. Black Forest Labs does not use Your User Data to train their models.
    * **Data location:** United States
* **Brave, Inc.:**
    
    * **Purpose:** Search-engine providing content to generate Outputs. Brave accesses Your Input and the content sent to Mistral AI to generate the Output. Brave Inc. does not access the Output. Brave, Inc. does not use Your User Data to train any artificial intelligence models or for any other purposes than to provide content to generate an Output.
    * **Data location:** United States
* **Google Cloud Platform:**
    
    * **Purpose:** Cloud Infrastructure
    * **Data location:** Ireland
* **Google Cloud Platform:**
    
    * **Purpose:** Cloud Infrastructure for the US version of Our APIs
    * **Data location:** United States
* **Intercomm:**
    
    * **Purpose:** Support
    * **Data location:** United States
* **Kong:**
    
    * **Purpose:** API security
    * **Data location:** United States
* **Lago:**
    
    * **Purpose:** Billing
    * **Data location:** EEA
* **Mailjet:**
    
    * **Purpose:** Mailing
    * **Data location:** United States
* **Ory:**
    
    * **Purpose:** Authentication on the Services
    * **Data location:** EEA
* **Stripe:**
    
    * **Purpose:** Payment management
    * **Data location:** United States

7\. Do we transfer Your Personal Data Outside of the European Union?
--------------------------------------------------------------------

We prioritize selecting providers within the European Union that strictly adhere to the GDPR. However, in exceptional cases, we may opt for non-EU providers that meet our high standards of data security and Personal Data protection.

We take the necessary steps to ensure that all contracts with service providers who process personal data outside the European Union have adequate safeguards in compliance with Article 46 of the GDPR. Additionally, We attach the most recent version of the European Commission’s Standard Contractual Clauses to all such contracts.

8\. Your rights
---------------

You can exercise:

* **Access.** You have the right to know if Mistral AI processes Your Personal Data. You also have the right to request a copy of such Personal Data and to obtain further information about the way We process Your Personal Data.
* **Rectification.** You have the right to update or correct Your Personal Data.
* **Deletion.** You have the right to delete and/or ask us to delete Your Personal Data.
* **Objection.** You have the right to object to the processing of Your Personal Data. This right does not apply when we have a legal obligation to process Your Personal Data.
* **Consent withdrawal.** You have the right to withdraw Your consent to the processing of Your Personal Data at any time.
* **Limitation.** You have the right to ask us to freeze the processing of Your Personal Data.
* **Automated decision.** You have the right to not be subject to an automated decision (including profiling) and to appeal such a decision. Mistral AI does not engage in profiling or automated decision-making in the Processing of Personal Data.
* **Portability.** You have the right to obtain and transfer Your Personal Data to another entity.
* **Post mortem.** You have the right to tell us how You would like us to process Your Personal Data after your death.
* **Lodge a complaint.** You have the right to lodge a complaint before the competent data protection authority, including the French data protection authority (the CNIL).

We will take every step to make sure we reply to Your requests. However, when your request concerns the training of Our Models, it’s important to note that Your rights have technical limitations and fulfilling Your requests might involve a complex technical process.

You can exercise these rights:

* By sending us an email at `privacy@mistral.ai`
* By making a request using Our Ticketing Solution, available directly on Our Website, through the help section of your account
* By sending us a letter at Mistral AI, Attn: Privacy Team, Mistral AI, 15 rue des Halles, 75001 Paris, France.

### 9\. Changes to this Privacy Policy

We may amend this Privacy Policy from time to time as Our Services continuously evolve. Make sure to check this Privacy Policy frequently.

* * *

Data Processing Agreement
=========================

**Preamble.** This Data Processing Agreement amends and forms part of the Agreement between Mistral AI and the Customer. This Data Processing Agreement prevails over any conflicting terms of the Agreement but does not otherwise modify the Agreement.

1\. Definitions
---------------

The capitalized words in this Agreement shall have the meaning given below:

* **“Agreement”:** means the service agreement entered into by and between the Parties, governing the provision of the Services by Mistral AI to the Customer.
    
* **“Applicable Data Protection Law**”: means any applicable national, federal, EU, state, provincial or other privacy, data security, or data protection law or regulation, including, to the extent applicable, Regulation (EU) 2016/679 of the European Parliament and of the Council of 27 April 2016 applicable since 25 May 2018 (the “GDPR”).
    
* “**Authorized Recipient**”: means (i) Mistral AI’s affiliates, (ii) Mistral AI’s team members, (ii) Mistral AI’s Sub-processors or (iv) any third party that is authorized by the Applicable Data Protection Law to access the Personal Data.
    
* “**Authorized Purpose**”: means the authorized purpose for the Processing as mentioned in Exhibit 1.
    
* “**Customer**”: means any legal person who subscribes to the Services and, where applicable, its affiliates.
    
* **“Data Controller**”: means the person who determines the purposes and the means of the Processing.
    
* “**Data Processing Agreement**” or “**DPA**”: means this data processing agreement governing the Processing carried-out by the Parties, that forms part of the Agreement.
    
* “**Data Processor**”: means the person who carries-out the Processing on behalf of the Data Controller and under its documented instructions.
    
* “**Data Subjects**”: means the person whose Personal Data is processed.
    
* “**“International Data Transfer**”: means any disclosure of Personal Data by an organization establishes in the EEA to a Restricted Country.
    
* “**Mistral AI**”: means Mistral AI, a French simplified joint-stock company, registered at the Trade register of Paris under number 952 418 325, having its corporate seat at 15 rue des Halles 75001, Paris, France and its affiliates.
    
* “**Personal Data**”: means any data relating to an identified or identifiable Data Subject.
    
* “**Personal Data Breach**”: means any breach of security leading to the accidental or unlawful destruction, loss, alteration, unauthorized disclosure of, or access to, Personal Data, likely to result in a risk for the rights and freedoms of Data Subjects.
    
* “**Processing**”: means the processing of Personal Data described in Exhibit 1.
    
* “**Restricted Country**”: means any country located outside of the European Economic Area (EEA) and that does not benefit from an adequacy decision from the European Commission.
    
*     “**SCC”¨¨: means the clauses annexed to the EU Commission Implementing Decision 2021/914 of June 4, 2021 on standard contractual clauses for the transfer of personal data to third countries pursuant to Regulation (EU) 2016/679 of the European Parliament and of the Council as amended or replaced from time to time.
        
    
* “**Services**”: means the services provided by Mistral AI to the Customer under the Services Agreement.
    
* “**Sub-processor**”: means any Data Processor appointed by Mistral AI to carry-out all or part of the Processing on behalf of the Customer.
    
* “**Supervisory Authority**”: means any independent authority competent to supervise the Processing.
    

**Terms not defined in this document.** Any capitalized word that is not defined in this DPA shall have the meaning given in the Agreement.

2\. Role of the Parties
-----------------------

### 2.1. Mistral AI as Data Processor

**Services provided on Le Chat and La Plateforme.** When Customer uses Le Chat or La Plateforme:

* The Customer is the Data Controller;
* Mistral AI is the Data Processor for the limited and specific Authorized Purposes set out in Exhibit 1.

**Services provided via Cloud Providers.** When Customer uses the Services through a Cloud Provider:

* The Customer is the Data Controller;
* The Cloud Provider processes the Personal Data provided by the Customer as Data Processor for the purpose of making the Models available to the Customer on the Cloud Provider’s Infrastructure.
* Mistral AI will only process Personal Data provided by the Customer as Data Processor for the limited and specific Authorized Purposes set out in Exhibit 1.

**Description of the Processing.** Mistral AI processes the Personal Data on behalf of the Customer in order to provide the Customer with the Services under the Agreement. A description of the Processing is available in Exhibit 1 of this DPA. The Customer agrees that Mistral AI may update the description of the Processing from time to time to reflect new Services, features or functionalities.

### 2.2. Mistral AI as Data Controller

**Mistral AI as Data Controller.** The Customer authorizes Mistral AI to process the User Data and as Data Controller for the purpose of:

* Monitoring abuse;
* Processing voluntary reports;
* Research and development purposes, including to improve the training of Our Models only if Customer:
    * Uses the Free Services; or
    * Uses the Paid Services on Le Chat and has not opted-out of the Mistral Ai training data set by using the applicable feature on its Account.

3\. General obligations of the Parties
--------------------------------------

Each Party shall comply with their respective obligations under the Applicable Personal Data Protection Law and shall not, by any act or omission, cause the other to be in breach of any such obligations under the Applicable Data Protection Law.

### 3.1. General obligations of Mistral AI

Mistral AI shall:

* Process the Personal Data only in accordance with the documented lawful instructions of the Customer as set forth in this DPA, the Agreement or by email and for no other purpose, unless required to do so by the applicable laws. In such a case, Mistral AI shall promptly inform the Customer of that legal requirement, unless prohibited to do so by applicable law and/or on important grounds of public interest,
    
* Promptly inform the Customer if, in its opinion, the Customer’s instructions infringe the Applicable Data Protection Law. In such an event, Mistral AI is entitled to refuse to perform the Processing of Personal Data that it believes to be in violation of the Applicable Data Protection Law,
    
* Ensure that any person Mistral AI authorizes to process Personal Data (including Mistral AI team members and the Subprocessors), are subject to a duty of confidentiality, whether by contract or statutory, and must not allow any person to process Personal Data who is not under such confidentiality obligations, and
    
* Taking into account the nature of the Processing and the information available to Mistral AI, upon the Customer’s written request and to the extent that is commercially reasonable and required by the Applicable Data Protection Laws, provide the Customer with reasonable and timely assistance (i) in the event of an investigation from a Supervisory Authority related to the Processing, (ii) to conduct a data protection impact assessment, a prior consultation with a Supervisory Authority, (iii) to comply with its obligations under Article 32 GDPR.
    

### 3.2. General obligations of the Customer

The Customer agrees that:

* It will comply with its obligations under the Applicable Data Protection Law regarding the Processing and any Processing instruction it issues to Mistral AI,
    
* It is responsible for providing guidance to Authorized users regarding the use of the Services, and in particular the use of Personal Data within the Services,
    
* It is responsible for applying filters to prevent any unauthorized use of Personal Data by the Authorized Users,
    
* Mistral AI’s security obligations under this DPA apply without prejudice to the Customer’s own security obligations under the Applicable Data Protection Law, and
    
* It has provided notice and obtained all consents and rights necessary under the Applicable Data Protection Law for Mistral Ai to process Personal Data under this DPA.
    

4\. Data Subjects
-----------------

**Information.** As Data Controller, the Customer is solely responsible to provide the Data Subjects with any information required by the Applicable Data Protection Law.

**Data Subject requests.** Taking into account the nature of the Processing and upon the Customer’s request, Mistral AI shall provide the Customer with commercially reasonable assistance to enable the Customer to respond to any request from Data Subjects to exercise any of their rights under the Applicable Data Protection Law.

**Requests made directly to Mistral AI.** In the event that any request is made directly to Mistral AI, Mistral AI will not respond to such request directly without the Customer’s prior consent, unless required to do so by applicable law. Instead, Mistral AI will transfer that request to the Customer who will then be solely responsible to respond to such request. If Mistral AI is legally required to respond to the Data Subjects’ request, Mistral AI will promptly notify the Customer and provide it with a copy of the request unless prohibited to do so by applicable law.

5\. Security and Personal Data Breach
-------------------------------------

### 5.1. Security measures

**Security measures.** Taking into account the state of the art, the costs of implementation and the nature, scope, context and purposes of Processing, as well as the risk of varying likelihood and severity for the rights and freedoms of natural persons, Mistral AI shall implement and maintain appropriate technical and organizational measures to protect Personal Data from any Personal Data Breach and to preserve the security and confidentiality of the Personal Data.

**Evolution of the security measures.** The Customer acknowledges that such security measures are subject to technical progress and development and that Mistral AI may update them from time to time, provided that such updates do not materially decrease the overall security of the Processing.

### 5.2. Personal Data Breach

**Personal Data Breach.** Taking into account the nature of the Processing and the information available to Mistral AI, Mistral AI shall notify the Customer of any Personal Data Breach without undue delay and where feasible no later than seventy-two (72) hours after becoming aware of such Personal Data Breach. Mistral AI’s notification of or response to a Personal Data Breach in accordance with this Section 6.2. will not be construed as an acknowledgment by Mistral AI of any fault or liability with respect to the Personal Data Breach.

**Notification to the Customer.** This notification shall include:

* (a) The name and contact details of Mistral AI’s point of contact point where more information can be obtained;
    
* (b) The nature of the Personal Data Breach, including but not limited to the categories and number of Data Subjects and Beneficiaries Personal Data concerned by the Personal Data Breach;
    
* (c) A description of the measures the Beneficiaries could take to mitigate the possible adverse effects of the Personal Data Breach and to prevent from another potential Personal Data Breach;
    
* (d) The likely consequences of the Personal Data Breach;
    
* (e) The measures proposed or taken by the Company following the Personal Data Breach, including to prevent from any new occurrence.
    

**Notification to the Supervisory Authority and Communication to the Data Subject.** The Customer is solely responsible for notifying the Personal Data Breach to the Supervisory Authority and/or to the Data Subjects.

**Assistance.** Upon the Customer’s written request, taking into account the nature of the Processing and the information available to Mistral AI, Mistral AI shall provide the Customer with commercially reasonable assistance with respect to the Customer’s compliance with its obligation to communicate the Personal Data Breach to Data Subjects, when required by the Applicable Data Protection Laws. If necessary, Mistral AI shall provide the Customer with commercially reasonable and timely assistance to mitigate or remediate the Personal Data Breach.

6\. Sub-processing
------------------

**General authorization.** The Customer provides a prior and general authorization allowing Mistral AI to appoint any Subprocessors to assist Mistral AI in the provision of the Services and in the Processing, in accordance with the terms of this DPA. This authorisation is subject to the following:

* Mistral AI will maintain an up-to-date list of its Sub-processors on the Website,
    
* Mistral AI will notify the Customer of any changes to this list,
    
* Mistral AI will enter into a written agreement with each Subprocessor imposing data protection terms that require the Subprocessor to protect the Personal Data to the same standards provided by this DPA, and
    
* Mistral AI will remain liable to the Customer if such Subprocessor fails to fulfill its data protection obligations with regard to the relevant Processing activities under the DPA.
    

**Changes to the list of Sub-processors.** Mistral AI will provide notice to the Customer of any changes to the list of Sub-processors as soon as reasonably practicable and no later than thirty (30) days prior to engaging such Sub-processor. The Customer may object in writing to Mistral AI’s appointment of a new Sub-processor during this notice period, provided that such objection is based on reasonable grounds relating to the Applicable Data Protection Laws. In such an event, the Parties will consult and negotiate in good faith to find an amicable resolution that allows the Customer to keep benefiting from the Services. If no resolution is achieved during this notice period, the Customer may, as its sole and exclusive remedy, terminate all or part of the Agreement for convenience.

7\. Transfers of Personal Data to a Restricted Country
------------------------------------------------------

\*\*General Authorization. \*\*Customer hereby authorizes Mistral AI to perform International Data Transfers to any country deemed to have an adequate level of data protection by the European Commission or the competent authorities, as appropriate; on the basis of adequate safeguards in accordance with Applicable Data Protection Laws; or pursuant to the SCCs.

**Customer located in a Restricted Country or Customer using the US API.** By accepting this DPA, Mistral AI and Customer conclude Module 4 (Processor-to-Controller) of the SCCs which applies to any International Data Transfer conducted by Mistral AI acting as a Data Processor pursuant to Section 2 of this DPA when Customer uses the Services and is hereby incorporated and completed as follows:

* The “data exporter” is Mistral AI and the “data importer” is the Customer;
* The optional docking clause in CLause 7 is implemented; Option 2 of Clause 9(a) is implemented and the time period therein is specified in Section XX above; the optional redress clause in Clause 11(a) is struck;
* The governing law in Clause 17 is the law of France;
* The courts in Clause 18(b) are the Courts of France; and
* Annex I and II to the Module 4 of the SCCs are Exhibit 1 and 2 to this DPA respectively.

8\. Audit
---------

**Documentary audit.** Upon the Customer’s written request, Mistral AI will make available all documents and information to demonstrate that the Processing carried-out by Mistral AI complies with this DPA in a timely manner, to the extent that is commercially reasonable and required by the Applicable Data Protection Laws.

**Audit on Mistral AI’s premises.** Only to the extent the Customer cannot reasonably be satisfied with Mistral AI’s compliance with this DPA through the exercise of a documentary audit, the Customer may conduct up to one (1) audit per year to verify Mistral AI’s compliance with this DPA, under the conditions defined below:

* This audit must me conducted with reasonable advance written notice of at least thirty (30) calendar days,
    
* This audit shall be carried out by an independent auditor selected jointly by the Parties for its expertise, independence and impartiality and which is, in any event, not a direct or indirect competitor of the Mistral AI,
    
* The selected auditor shall be bound by a confidentiality agreement and/or by professional secrecy,
    
* This audit shall be conducted during Mistral AI’s regular business hours,
    
* This audit shall restrict its findings to only information and/or Personal Data relevant to the Customer,
    
* The audit shall not unreasonably impair or slow down the Services offered by Mistral AI or affect the organizational management of the Company,
    
* An identical copy of the audit report shall be given to both Parties following the completion of the audit. Each Party may make observations regarding the audit report,
    
* The costs of this audit shall be borne exclusively by the Customer.
    

9\. Return or destruction of Personal Data
------------------------------------------

After the end of the provision of the Services, Mistral Ai will delete or return to the Customer all Personal Data processed on the Customer’s behalf, in accordance with Mistral AI’s deletion policies and procedures. The Customer acknowledges that the Personal Data will no longer be accessible upon the expiry of a thirty (30) days period following the termination of the Customer’s access to and use of the Services.

10\. Term
---------

This DPA shall commence on the effective date of the Agreement and will continue for the duration of the Agreement.

11\. Limitation of Liability
----------------------------

The liability of each Party and each Party’s affiliates under this DPA is subject to the exclusions and limitations of liability set out in the Agreement.

EXHIBIT 1 - Description of the Processing
-----------------------------------------

Mistral AI may update the description of the Processing from time to time to reflect new Services, features or functionality.

1\. La Plateforme
-----------------

### A. List of the Parties

* **Data Controller:** The Customer
* **Data Processor:** Mistral AI
* **Mistral AI privacy contact:** [privacy@mistral.ai](mailto:privacy@mistral.ai)

### B. Description of the Processing

#### Categories of Data Subjects:

* The Customer
* The Authorized Users
* The End-User
* Any other natural person whose Personal Data is used by the Customer or the Authorized User as a User Data.

#### Categories of Personal Data:

* Account data (professional contact details, name of the organization, Mistral AI User ID etc.)
* The API Key (where applicable)
* Any Personal Data included in the User Data
* Any Personal Data associated with a support request (name, email address, any other personal data included within the request)

#### Special categories of Personal Data:

* None. Customer shall not process sensitive data under this DPA. In case Customer wishes to process sensitive data, please contact [privacy@mistral.ai](mailto:privacy@mistral.ai)

#### Frequency of the Processing:

* On a continuous basis

#### Nature of the Processing:

* The Personal Data will be processed as described in the Agreement.

#### Authorized Purposes:

* The provision La Plateforme:
    * Authorized Users’ Account management
    * Generation of Outputs based on Inputs
    * Fine-tuning Models based on Input
    * Agent creation and management based on Input
    * Processing Support requests
* When Customer uses the Free Services: de-identifying the Personal Data included by Customer in the User Data for the purpose of training or improving the Models on such de-identified data.

#### Duration of the Processing:

* The term of this DPA.

#### Retention Periods:

* **User Data:**
    * Processed through the Models API: the Inputs and Outputs are stored for the duration of the generation of the Outputs. Mistral AI retains the Inputs and Outputs for thirty (30) rolling days to monitor abuse as a Data Controller, unless Customer has activated zero data retention.
    * Processed through the Fine-tuning API: the Inputs are stored for the duration of the fine-tuning job and until (i) deletion of the Inputs by Customer on the Services or (ii) one (1) year from the termination of Customer’s Account.
* **Account Data:**
    * For the duration of the registration of the Customer and Customer’s Authorized Users and for 1 year from the termination of such registration.
* **Customer Support:**
    * For the duration necessary to process Customer’s request and for five (5) additional years for evidential purposes.
* Mistral AI may process Personal Data as Data Controller for the duration mentioned in the Privacy Policy.

### C. Authorized Recipients

#### Sub-processors:

* **Azure:** Cloud Infrastructure. The Personal Data is stored in Sweden.
* **Google Cloud Platform:** Cloud Infrastructure.
    * If You use the standard API, the Personal Data is stored in Ireland.
    * If You use the US API, the Personal Data is stored in the United States.

2\. Le Chat
-----------

### A. List of the Parties

* **Data Controller:** The Customer
* **Data Processor:** Mistral AI
* **Mistral AI privacy contact:** [privacy@mistral.ai](mailto:privacy@mistral.ai)

### B. Description of the Processing

#### Categories of Data Subjects:

* The Customer
* The Authorized Users
* The End-User
* Any other natural person whose Personal Data is used by the Customer or the Authorized User as a User Data.

#### Categories of Personal Data:

* Account data (professional contact details, name of the organization, Mistral AI User ID etc.)
* Any Personal Data included in the User Data
* Any Personal Data associated with a support request (name, email address, any other personal data included within the request)

#### Special categories of Personal Data:

* None. Customer shall not process sensitive data under this DPA. In case Customer wishes to process sensitive data, please contact [privacy@mistral.ai](mailto:privacy@mistral.ai)

#### Frequency of the Processing:

* On a continuous basis

#### Nature of the Processing:

* The Personal Data will be processed as described in the Agreement.

#### Authorized Purposes:

* The provision of Le Chat:
    * Authorized Users’ Account management
    * Generation of Outputs based on Inputs
    * Displaying Inputs and Outputs on Le Chat, including in the chat history
    * Processing Support requests
* When Customer uses the Free Services or has not opted-out of the Mistral AI Training Data: de-identifying the Personal Data included by Customer in the User Data for the purpose of training or improving the Models on such de-identified data.

#### Duration of the Processing:

* The term of this DPA.

#### Retention Periods:

* **User Data:**
    * The Inputs and Outputs are stored for the Term of the Agreement, unless deleted by Customer prior to the expiry of such Term.
* **Account Data:**
    * For the duration of the registration of the Customer and Customer’s Authorized Users and for 1 year from the termination of such registration.
* **Customer Support:**
    * For the duration necessary to process Customer’s request and for five (5) additional years for evidential purposes.
* Mistral AI may process Personal Data as Data Controller for the duration mentioned in the Privacy Policy.

### C. Authorized Recipients

#### Sub-processors:

* **Azure:** Cloud Infrastructure. The Personal Data is stored in Sweden.
* **Black Forest Labs (if You activate this options) :** Image generation. Any Personal Data included in the Input and Output is transferred to the United States. The Input is stored for 15 days and then permanently deleted from Black Forest Labs’ systems. The Outputs are permanently destroyed within 1 day from Black Forest Labs’ systems.
* **Brave (if You activate this option) :** Search-engine providing content to generate Outputs. Any Personal Data included in the Input is transferred to the United States.
* **Google Cloud Platform:** Cloud Infrastructure. The Personal Data is stored in Ireland.

3\. Providing Support Services through Cloud Providers
------------------------------------------------------

### A. List of the Parties

* **Data Controller:** The Customer
* **Data Processor:** Mistral AI
* **Mistral AI privacy contact:** [privacy@mistral.ai](mailto:privacy@mistral.ai)

### B. Description of the Processing

#### Categories of Data Subjects:

* The Customer
* Any person whose Personal Data is included in Customer’s support request.

#### Categories of Personal Data:

* Professional contact details
* Any Personal Data included in Customer’s support request.

#### Special categories of Personal Data:

* None.

#### Frequency of the Processing:

* On a continuous basis

#### Nature of the Processing:

* The Personal Data will be processed as described in the Agreement.

#### Authorized Purposes:

* The provision of support services when Customer uses the Models through Cloud Providers.

#### Duration of the Processing:

* The term of this DPA.

#### Retention Periods:

* **Customer Support:**
    * For the duration necessary to process Customer’s request and for five (5) additional years for evidential purposes.

### C. Authorized Recipients

#### Sub-processors:

* **Intercom:** Support management. The Personal data are stored in the United States.

#### Third-Parties:

* The applicable Cloud Provider.

EXHIBIT 2 - Technical and organizational measures
-------------------------------------------------

The Technical and organizational measures implemented by Mistral AI are listed in Mistral AI’s trust center available at [https://trust.mistral.ai/](https://trust.mistral.ai/).

* * *