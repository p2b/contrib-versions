Vilkår, betingelser og meddelelser angående Tripadvisors website
================================================================

Tripadvisors brugsbetingelser

Senest opdateret: 16. februar 2024

**Brugsbetingelser**

[BRUG AF TJENESTERNE](#_USE_OF_THE)

[YDERLIGERE PRODUKTER](#_ADDITIONAL_PRODUCTS)

[FORBUDTE AKTIVITETER](#_PROHIBITED_ACTIVITIES)

[POLITIK OM BESKYTTELSE AF PERSONLIGE OPLYSNINGER OG VIDEREGIVELSE](#_PRIVACY_POLICY_AND)

[ANMELDELSER, KOMMENTARER OG BRUG AF ANDRE INTERAKTIVE OMRÅDER, TILDELING AF LICENS](#_REVIEWS,_COMMENTS_AND)

* [Begrænsning af Tripadvisors licensrettigheder](#_Restricting_Tripadvisor%E2%80%99s_Licence)

[BOOKINGER HOS TREDJEPARTSLEVERANDØRER GENNEM Tripadvisor](#_BOOKING_WITH_THIRD-PARTY)

* [Brug af Tripadvisors bookingtjenester](#_Use_of_Tripadvisor)
* [Tredjepartsleverandører](#_Third-Party_Suppliers._The)
* [Booking af ferieboliger, bordreservationer på restauranter og oplevelser hos tredjepartsleverandører, der er opført på tilknyttede websites](#_Booking_Holiday_Rentals,)

[REJSEMÅL](#_TRAVEL_DESTINATIONS)

* [Internationale rejser](#_International_Travel._When)

[ANSVARSFRASKRIVELSE](#_LIABILITY_DISCLAIMER_1)

[SKADESLØSHOLDELSE](#_Indemnification)

[LINKS TIL TREDJEPARTSWEBSITES](#_LINKS_TO_THIRD-PARTY)

[SOFTWARE SOM EN DEL AF TJENESTER, YDERLIGERE MOBILLICENSER](#_SOFTWARE_AS_PART)

[COPYRIGHT- OG VAREMÆRKEMEDDELELSER](#_COPYRIGHT_AND_TRADEMARK)

* [Politik om anmeldelse og fjernelse af ulovligt indhold](#_Notice_and_Take-Down)

[ÆNDRINGER AF NÆRVÆRENDE AFTALE; TJENESTERNE; OPDATERINGER; OPHØR OG FORTRYDELSE](#_MODIFICATIONS_TO_THE)

[VÆRNETING OG GÆLDENDE LOVGIVNING](#_JURISDICTION_AND_GOVERNING)

[VALUTAOMREGNER](#_CURRENCY_CONVERTER)

[GENERELLE BESTEMMELSER](#_GENERAL_PROVISIONS)

[KUNDESERVICE/HJÆLP](#_SERVICE_HELP)

Velkommen til Tripadvisors websites og mobile aktiver, som er opført på www.Tripadvisor.dk og relevante nationale topdomæner (herunder underdomæner, der er knyttet til disse), relaterede programmer (nogle gange kaldet "apps"), data, SMS, API'er, e-mail, chat og telefonkorrespondance, knapper, widgets og annoncer (i dette dokument kaldes alle disse elementer samlet for "**tjenester**", og mere generelt kaldes Tripadvisors websites og mobile aktiver for "websites").

Tjenesterne leveres af Tripadvisor LLC, der er et aktieselskab med hovedsæde i Delaware i USA ("**Tripadvisor**") på adressen \[Tripadvisor LLC, 400 1st Avenue, Needham, MA 02494, USA\]. Termerne "vi", "os", "vores" skal forstås i overensstemmelse hermed.

Ved at oprette en Tripadvisor-konto accepterer du at være bundet af de vilkår, betingelser og meddelelser, der er angivet nedenfor (samlet benævnt nærværende "aftale"). Nærværende aftale gælder ikke, hvis du blot gennemser vores websites eller bruger tjenesterne uden at oprette en Tripadvisor-konto.  Læs aftalen omhyggeligt, da den indeholder oplysninger omkring dine juridiske rettigheder og begrænsninger af disse rettigheder, såvel som et afsnit om den gældende lovgivning og værneting ved tvister. Hvis du er forbruger i EU eller Storbritannien, har du juridisk ret til at fortryde nærværende aftale inden for 14 dage efter aftalens indgåelse. Du kan gøre dette eller opsige nærværende aftale på et hvilket som helst andet tidspunkt ved at lukke din konto (ved at kontakte os eller ved at gå til "Kontooplysninger", mens du er logget på, og vælge muligheden "luk konto") eller ved ikke længere at tilgå eller bruge tjenesterne.

Hvis du er forbruger i EU eller Storbritannien, har du visse obligatoriske forbrugerrettigheder. Hvis du er forbruger, der bor et andet sted, har du muligvis også forbrugerrettigheder i henhold til lovgivningen i dit område. Intet i nærværende aftale påvirker dine obligatoriske forbrugerrettigheder.

Alle oplysninger, tekst, links, grafik, fotos, lyd, videoer, data, kode eller andet materiale eller materialesamlinger, som du kan få vist, tilgå eller på anden måde bruge gennem disse tjenester, kaldes for "indhold". "Tjenester" som defineret ovenfor henviser til tjenester, der leveres af Tripadvisor eller vores søsterselskaber (når der henvises til et af disse, kaldes Tripadvisor og sådanne enheder samlet for "Tripadvisors virksomheder"). For at undgå tvivl ejes og kontrolleres alle websites af Tripadvisor.  Nogle specifikke tjenester, der er tilgængelige via disse websites, kan dog ejes og kontrolleres af Tripadvisors søsterselskaber, f.eks. tjenester, der formidler [**bookinger af ferieboliger, bordreservation på restauranter og oplevelser hos tredjepartsleverandører**](https://tripadvisor.mediaroom.com/DK-terms-of-use#OLE_LINK11) (se nedenfor). Som en del af vores tjenester kan vi sende dig meddelelser om særtilbud, produkter eller yderligere tjenester, som udbydes af os, vores tilknyttede selskaber eller vores partnere, og som kan være interessante for dig. Sådanne meddelelser sendes typisk via nyhedsbreve og marketingmeddelelser og er en del af vores indsats for at lære dig og dine præferencer bedre at kende på tværs af vores tjenester og vores tilknyttede selskabers tjenester. Dette giver til gengæld mulighed for personlig tilpasning af tjenesterne i overensstemmelse med disse præferencer.

Termen "du/dig" eller "bruger/brugeren" henviser til den person, virksomhed, erhvervsorganisation eller andre juridiske personer, der har oprettet en Tripadvisor-konto og bruger tjenesterne og/eller bidrager med indhold til dem. Det indhold, som du bidrager med, sender til, overfører til og/eller slår op på eller igennem tjenesterne, kaldes for "dit indhold" og/eller "indhold, du sender".

Tjenesterne leveres kontinuerligt og udbydes udelukkende for at:

1. hjælpe brugerne med indsamling af rejseoplysninger, offentliggørelse af indhold samt med at søge efter og booke rejsetjenester og foretage andre bookinger
2. hjælpe virksomheder i rejse-, turist-, hotel- og restaurationsbranchen med at kommunikere med brugere og potentielle kunder i form af gratis tjenester og/eller betalingstjenester, der udbydes af eller gennem Tripadvisors virksomheder.

#### Produkter, tjenester, oplysninger og indhold på vores tjenester ændres regelmæssigt, så de er opdateret (f.eks. med aktuelle oplysninger og tilbud). Det betyder f.eks., at i forbindelse med tredjepartstjenester kan nye transporttjenester, overnatningssteder, restauranter, ture, aktiviteter eller oplevelser blive tilgængelige, som du kan booke, mens andre tjenester kan blive utilgængelige.

Vi kan i fremtiden ændre eller på anden måde justere aftalen i henhold til vilkårene og betingelserne heri, og du forstår og accepterer, at din fortsatte tilgang til eller brug af websitet, efter sådanne ændringer er trådt i kraft, betyder, at du accepterer den opdaterede eller ændrede aftale. Vi noterer datoen for, hvornår opdateringer senest blev foretaget i nærværende aftale, nederst på denne side, og alle opdateringer træder i kraft, når de offentliggøres. Vi giver dig besked om væsentlige ændringer af disse vilkår og betingelser ved at sende en meddelelse til den e-mailadresse, der er knyttet til din profil, eller ved at offentliggøre en meddelelse på vores websites. Du bør besøge denne side med jævne mellemrum, så du kan læse den seneste udgave af nærværende aftale. Hvis du ikke ønsker at acceptere ændringer af nærværende aftale, kan du lukke din konto og ophøre med at tilgå tjenesterne (en beskrivelse af, hvordan du gør det, findes [her](https://www.tripadvisorsupport.com/da-DK/hc/traveler/articles/510)).

**BRUG AF TJENESTERNE**

Som en betingelse for din brug af tjenesterne accepterer du, (i) at alle oplysninger, som du angiver via tjenesterne til Tripadvisors virksomheder, er sande, nøjagtige, opdaterede og fuldstændige, (ii) at du vil beskytte dine kontooplysninger og holde øje med og påtage dig det fulde ansvar for enhver brug af din konto af andre end dig selv, og (iii) at du er mindst 13 år gammel. Tripadvisors virksomheder indsamler ikke med overlæg oplysninger, som er leveret af personer under 13 år. Hvis du er forbruger bosiddende i EU eller Storbritannien, kan vi nægte dig adgang til tjenesterne, hvis du overtræder nærværende aftale, eller af en anden rimelig årsag. Hvis du ikke er forbruger bosiddende i EU eller Storbritannien, har vi til enhver tid ret til efter eget skøn at nægte dig adgang til tjenesterne af enhver årsag, herunder f.eks. overtrædelse af nærværende aftale. Ved at bruge tjenesterne, herunder eventuelle produkter eller tjenester, der muliggør deling af indhold med eller fra tredjeparters websites, er du indforstået med, at du er eneansvarlig for de oplysninger, du deler med Tripadvisors virksomheder. Du kan udelukkende tilgå tjenesterne efter hensigten qua tjenesternes funktionalitet og som tilladt i henhold til denne aftale.

Kopiering, overførsel, reproduktion, replikering, opslag eller gendistribuering af (a) indhold eller dele deraf og/eller (b) tjenesterne mere generelt er strengt forbudt uden forudgående skriftlig tilladelse fra Tripadvisors virksomheder. Vil du anmode om tilladelse, bedes du rette henvendelse til:

Director, Partnerships and Business Development

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, USA

Du er nødt til at oprette en konto for at få adgang til visse af tjenesternes funktioner. Når du opretter en konto, skal du oplyse fulde og nøjagtige oplysninger. Du er eneansvarlig for den aktivitet, der finder sted på din konto, herunder interaktion og kommunikation med andre, og du skal derfor beskytte din konto. Dette betyder, at du accepterer altid at holde dine kontaktoplysninger opdaterede. 

Hvis du opretter en Tripadvisor-konto til erhvervsformål og accepterer nærværende aftale på vegne af en virksomhed, organisation eller anden juridisk person, accepterer du, at du har tilladelse til at gøre dette og har bemyndigelse til at binde denne juridiske person til nærværende aftale. Derved henviser termerne "du/dig" og "din/dit/dine", som de anvendes i nærværende aftale, til denne juridiske person, og den enkeltperson, der handler på vegne af virksomheden, kaldes for en "virksomhedsrepræsentant".

Under brug af tjenesterne kan du støde på links til tredjeparters websites og apps eller være i stand til at interagere med tredjeparters websites og apps. Dette kan omfatte muligheden for at dele indhold fra tjenesterne, herunder dit indhold, med disse tredjeparters websites og apps. Vær opmærksom på, at tredjeparters websites og apps kan offentliggøre delt indhold. Tredjeparter kan opkræve et gebyr for brug af bestemt indhold eller bestemte tjenester på eller via deres websites. Inden du gennemfører eventuelle transaktioner med tredjeparter, bør du derfor foretage alle de undersøgelser, du finder nødvendige eller passende, for at få afdækket, om et gebyr vil blive opkrævet. Når Tripadvisors virksomheder giver oplysninger om gebyrer eller opkrævninger ved brug af tredjeparters indhold eller tjenester, er disse oplysninger udelukkende til orientering. Eventuelle interaktioner med tredjeparters websites og apps er på eget ansvar. Du anerkender og accepterer udtrykkeligt, at Tripadvisors virksomheder på ingen måde kan drages til ansvar eller hæfte for sådanne tredjeparters websites eller apps.

Tripadvisors virksomheder rangerer og viser registreringer for dig ved hjælp af anbefalingssystemer. De forskellige registreringskategorier (f.eks. hoteller og restauranter, aktiviteter og flyrejser) rangeres ud fra forskellige kriterier, såsom pris, "favoritter blandt rejsende" og varighed. Alle oplysninger om vores politik og kriterier for placering på ranglisten findes på [websiden Sådan fungerer websitet](https://www.tripadvisor.dk/HowTheSiteWorks.html).

Noget af det indhold, du får vist eller på anden måde tilgår på eller via tjenesterne, anvendes til kommercielle formål. Du er indforstået med, at Tripadvisors virksomheder kan placere reklamer og kampagner i tjenesterne sammen med, i nærheden af, tilstødende eller på anden måde tæt på dit indhold (herunder inden, under eller efter afspilning af videoer eller andet dynamisk materiale) samt andres indhold.

**YDERLIGERE PRODUKTER**

Tripadvisors virksomheder kan fra tid til anden vælge at ændre, opdatere eller annullere visse af tjenesternes produkter og funktioner. Du accepterer og forstår, at Tripadvisors virksomheder udelukkende er forpligtet til at gemme eller vedligeholde dit indhold eller andre oplysninger, du indsender, i det omfang, som kræves for at overholde gældende lovgivning.

Vi tilbyder også andre tjenester, der kan være underlagt yderligere vilkår eller aftaler. Hvis du bruger nogen af disse tjenester, vil de yderligere vilkår blive gjort tilgængelige, og de vil være en del af denne aftale undtagen i tilfælde, hvor disse yderligere betingelser udtrykkeligt udelades eller på anden måde erstatter denne aftale. Du skal acceptere de gældende yderligere vilkår, hvis du f.eks. bruger eller køber sådanne ekstra tjenester til kommercielle formål eller andet erhvervsformål. I det omfang andre vilkår er i konflikt med vilkårene og betingelserne i nærværende aftale, vil de yderligere vilkår være gældende for de specifikke tjenester i de pågældende tilfælde.

**FORBUDTE AKTIVITETER**

Indholdet og oplysningerne, der er tilgængelige via tjenesterne (herunder f.eks. beskeder, data, oplysninger, tekst, musik, lyd, billeder, grafik, video, kort, ikoner, software, kode eller andet materiale), samt den infrastruktur, der bruges til at levere dette indhold og disse oplysninger, tilhører Tripadvisors virksomheder eller er licenseret til Tripadvisors virksomheder af tredjeparter. For alt andet indhold end dit eget indvilliger du i ikke på anden måde at ændre, kopiere, distribuere, sende, vise, udføre, gengive, udgive, give licens til, fremstille afledte værker af, overføre eller sælge eller videresælge oplysninger, software, produkter eller tjenester, som du får fra eller via tjenesterne. Endvidere indvilliger du i ikke at:

*  (i) anvende tjenesterne eller indholdet til noget kommercielt formål ud over de kommercielle formål, der udtrykkeligt er tilladt under nærværende aftale og relaterede retningslinjer, som offentliggøres af Tripadvisors virksomheder
* (ii) tilgå, overvåge, gengive, distribuere, sende, transmittere, vise, sælge, give licens til, kopiere eller på anden måde udnytte tjenesternes indhold, herunder f.eks. brugerprofiler og billeder, ved hjælp af en robot, spider, scraper eller andre automatiske midler eller enhver manuel proces til et formål, der ikke er i overensstemmelse med denne aftale eller uden vores udtrykkelige skriftlige tilladelse
* (iii) overtræde begrænsningerne ved robotudelukkelsesoverskrifter på tjenesterne eller omgå andre metoder, som anvendes til at forhindre eller begrænse adgang til tjenesterne
* (iv) undlade at gøre noget, der efter vores eget skøn (eller hvis du er forbruger bosiddende i Storbritannien eller EU, efter vores rimelige skøn) indebærer eller kan indebære en urimelig eller uforholdsmæssig stor belastning af vores infrastruktur
* (v) lave dybe links til en del af tjenesterne uanset formålet uden vores udtrykkelige skriftlige tilladelse
* (vi) frame, spejle eller på anden måde integrere dele af tjenesterne på et andet website eller en anden tjeneste uden vores forudgående skriftlige tilladelse
* (vii) forsøge at ændre, oversætte, tilpasse, dekompilere, opdele eller foretage reverse engineering på softwareprogrammer, som bruges af Tripadvisors virksomheder i forbindelse med tjenesterne
* (viii) omgå, deaktivere eller på anden måde påvirke tjenesternes sikkerhedsrelaterede funktioner eller funktioner, der forhindrer eller begrænser brugen eller kopiering af indholdet
* (ix) hente indhold, medmindre det udtrykkeligt er gjort tilgængeligt med henblik på downloading af Tripadvisors virksomheder
* (x) bruge eller hjælpe, opfordre til eller gøre det muligt for tredjeparter at bruge robotter, spiders, AI-systemer (kunstig intelligens) eller andre automatiserede enheder, processer eller midler til at få adgang til, hente, kopiere, scrape, samle, indsamle, downloade eller på anden måde indeksere dele af tjenesterne eller indholdet, undtagen hvis det er udtrykkeligt skriftligt tilladt af Tripadvisor.

**POLITIK OM BESKYTTELSE AF PERSONLIGE OPLYSNINGER OG VIDEREGIVELSE**

Alle personlige oplysninger, du slår op på eller på anden måde sender i forbindelse med tjenesterne, anvendes i overensstemmelse med vores erklæring om beskyttelse af personlige oplysninger og cookies. Klik [her](https://tripadvisor.mediaroom.com/dk-privacy-policy) for at se vores erklæring om beskyttelse af personlige oplysninger og cookies.

**ANMELDELSER, KOMMENTARER OG BRUG AF ANDRE INTERAKTIVE OMRÅDER, TILDELING AF LICENS**

Vi sætter pris på at høre fra dig. Vær opmærksom på, at når du sender dit indhold til eller gennem tjenesterne, hvad enten det sker via e-mail, offentliggørelse gennem et Tripadvisor-synkroniseringsprodukt, andres tjenester og programmer eller på anden måde, herunder evt. indhold, der sendes til din Tripadvisor-konto qua produkter eller tjenester, der tilhører Tripadvisors virksomheder, vil anmeldelser, spørgsmål, billeder eller videoer, kommentarer, forslag, idéer eller lignende, der forefindes i dit indhold, være underlagt Tripadvisors virksomheders ikke-eksklusive, royaltyfrie, bestandige, overdragelige, uopsigelige og fuldt viderelicenserbare ret til at (a) hoste, bruge, gengive, ændre, køre, tilpasse, oversætte, distribuere, udgive, skabe afledte værker af og offentligt vise dit indhold alle steder i verden på et hvilket som helst eksisterende eller senere udviklet medie, (b) gøre dit indhold tilgængeligt i hele verden og lade andre gøre det samme, (c) levere, reklamere for og forbedre tjenesterne og gøre det indhold, du deler på tjenesterne, tilgængeligt for andre virksomheder, organisationer eller personer med henblik på syndikering, transmission, distribution, kampagneførelse eller offentliggørelse af dette indhold på andre medier og tjenester, i henhold til vores politik om beskyttelse af personlige oplysninger samt nærværende aftale og (d) bruge navnet og/eller varemærket, du indsender i forbindelse med dit indhold. Du accepterer, at Tripadvisor efter eget skøn (eller hvis du er forbruger bosiddende i Storbritannien eller EU, efter vores rimelige skøn) kan vælge at tilskrive dig dit indhold. Du giver endvidere Tripadvisors virksomheder ret til at anlægge sag mod enhver person eller juridisk person, der overtræder dine eller Tripadvisors virksomheders rettigheder til dit indhold ved misligholdelse af nærværende aftale. Du anerkender og accepterer, at dit indhold ikke er fortroligt og ikke er omfattet af nogen ejendomsret. Du accepterer, at du ejer eller har de nødvendige licenser, rettigheder (herunder ophavsrettigheder og andre ejendomsrettigheder), samtykker og tilladelser til at offentliggøre og på anden vis bruge (og til, at Tripadvisors virksomheder offentliggør og på anden vis bruger) dit indhold som tilladt i henhold til nærværende aftale.

Hvis det bestemmes, at du beholder ideelle rettigheder (herunder tilskrivelses- eller respektret) til dit indhold, erklærer du hermed i det omfang, som det er tilladt ved gældende lovgivning, (a) at du ikke stiller noget krav om, at der anvendes nogen personidentificerende oplysninger i forbindelse med indholdet eller eventuelle deraf afledte værker eller opgraderinger eller opdateringer af samme, (b) at du ikke vil gøre indsigelse mod, at Tripadvisors virksomheder eller disses licenstagere, erhververe eller efterfølgere offentliggør, bruger, ændrer, sletter eller udnytter af indholdet, (c) at du for stedse giver afkald på enhver form for forfatters ideelle rettigheder til noget af dit indhold, og (d) at du for stedse frigør Tripadvisors virksomheder og disses licenstagere, erhververe og efterfølgere fra ethvert krav, som du ellers i medfør af sådanne ideelle rettigheder ville kunne gøre gældende mod Tripadvisors virksomheder.

Bemærk, at enhver form for feedback og andre forslag, du måtte indsende, til enhver tid kan bruges, og vi er ikke forpligtet til at holde dem fortrolige.

Tjenesterne kan indeholde diskussionsfora, opslagstavler, anmeldelsestjenester, rejsefeeds eller andre fora, hvor du kan slå dit indhold op, som f.eks. anmeldelser af rejseoplevelser, meddelelser, materiale eller andre elementer ("interaktive områder"). Såfremt Tripadvisor tilbyder sådanne interaktive områder på websitene, er du eneansvarlig for din brug heraf, og brugen sker for egen risiko. Tripadvisors virksomheder garanterer ikke fortrolighed vedrørende det indhold, du indsender til tjenesterne eller i et interaktivt område.  I det omfang at en juridisk enhed, der er en af Tripadvisors virksomheder, stiller nogen form for privat kommunikationskanal til rådighed mellem brugere, accepterer du, at denne/disse juridisk(e) enhed(er) kan overvåge indholdet af denne kommunikation for at beskytte vores fællesskab og tjenester. Du er indforstået med, at Tripadvisor ikke redigerer eller kontrollerer de brugermeddelelser, som slås op på eller distribueres gennem tjenesterne, herunder evt. chatrum, opslagstavler eller andre kommunikationsfora, og fralægger sig alt ansvar og al erstatningspligt for disse meddelelser.  I særdeleshed redigerer eller kontrollerer Tripadvisor ikke brugeres indhold, der vises på websitene.  Tripadvisors virksomheder forbeholder sig dog retten til at fjerne og/eller begrænse meddelelserne eller andet indhold fra tjenesterne uden varsel, hvis man i god tro vurderer, at dette indhold overtræder nærværende aftale, eller man på anden måde mener, at fjernelsen og/eller begrænsningen med rimelighed er nødvendig for at sikre Tripadvisors virksomheders og/eller andre af tjenesternes brugeres rettigheder. Hvis du er uenig i fjernelsen af dit indhold fra websitene, kan du appellere vores afgørelse gennem Tripadvisors interne klagebehandlingsproces. Du kan finde yderligere oplysninger om, hvordan du gør indsigelse mod og appellerer en beslutning om at fjerne eller begrænse dit indhold, [her](https://www.tripadvisorsupport.com/da-DK/hc/traveler/articles/623). Ved at bruge et interaktivt område accepterer du udtrykkeligt, at du kun indsender indhold, der overholder Tripadvisors offentliggjorte retningslinjer, som er i kraft ved indsendelsen, og som Tripadvisor stiller til rådighed for dig.  Du accepterer udtrykkeligt ikke at opslå, uploade, sende, distribuere, gemme, oprette eller på anden måde via tjenesterne offentliggøre indhold som:

1. Er usandt, ulovligt, vildledende, ærekrænkende, injurierende, uanstændigt, pornografisk, sjofelt, vovet, hentydende, chikanerende (eller som opfordrer til chikane af en anden person), truende, krænker personlige forhold eller retten til at kontrollere brugen af eget navn, eller som er groft, provokerende, svigagtigt eller på anden måde anstødeligt.
2. Er åbenlyst krænkende for onlinefællesskabet, såsom indhold, der fremmer racisme, intolerance, had eller fysisk skade af enhver type imod enhver gruppe eller enkeltperson.
3. Udgør, opfordrer til, fremmer eller giver instruktioner til strafbare handlinger, lovovertrædelse, ansvarspådragelse, krænker rettighederne for enhver part i ethvert land i verden eller på anden vis forårsager ansvarspådragelse eller overtræder nogen lokal, national eller international lovgivning, herunder, men ikke begrænset til, børstilsynsregler udstukket af U.S. Securities and Exchange Commission (SEC) eller regler udstukket af nogen fondsbørs, herunder, men ikke begrænset til, New York Stock Exchange (NYSE), NASDAQ eller London Stock Exchange.
4. Giver anvisninger i ulovlige aktiviteter såsom at fremstille eller købe ulovlige våben, krænke en anden persons personlige forhold eller sende eller fremstille computervira.
5. Kan udgøre en krænkelse af noget patent, varemærke, nogen forretningshemmelighed, ophavsret eller nogen anden immateriel rettighed eller ejendomsret. Særligt indhold, som fremmer en ulovlig eller uautoriseret kopi af en andens ophavsretligt beskyttede arbejde, såsom levering af piratkopierede computerprogrammer eller links til disse, levering af oplysninger til omgåelse af producentinstallerede ophavsretligt beskyttede enheder eller levering af piratkopieret musik eller links til piratkopierede musikfiler.
6. Udgør husstandsomdelte reklamer eller "spamming", "reklamer", "kædebreve" eller "pyramidespil".
7. Foregiver at være en person eller juridisk person eller på anden vis afgiver urigtige oplysninger om tilknytning til nogen person eller juridisk person, herunder Tripadvisors virksomheder.
8. Er private oplysninger om tredjemand, herunder, men ikke begrænset til, adresser, telefonnumre, e-mailadresser, CPR-numre og kreditkortnumre.  Bemærk, at en persons efternavn kan slås op på vores websites, dog udelukkende ved udtrykkelig tilladelse fra den pågældende person.
9. Indeholder sider med begrænset adgang eller adgangskodebeskyttede sider, skjulte sider eller billeder (dem, som der ikke linkes til, eller fra en anden tilgængelig side).
10. Indeholder eller skal facilitere vira, inficerede data eller andre skadelige, genevoldende eller ødelæggende filer.
11. Ikke er relevant for emnet for det eller de(t) interaktive område(r), som indholdet offentliggøres på.
12. Efter Tripadvisors eget skøn (eller hvis du er forbruger bosiddende i Storbritannien eller EU, efter vores rimelige skøn): (a) overtræder ovenstående underafsnit, (b) overtræder Tripadvisors relaterede retningslinjer, som Tripadvisor stiller til rådighed for dig, (c) er stødende, (d) begrænser eller hæmmer andre personer i at bruge eller nyde de interaktive områder eller andre aspekter af tjenesterne eller (e) evt. kan forvolde skade på Tripadvisors virksomheder eller deres brugere eller gøre dem erstatningspligtige i nogen form for udstrækning.

I det omfang, at det er tilladt ved gældende lovgivning, er Tripadvisors virksomheder ikke ansvarlige for og påtager sig ingen erstatningspligt for indhold, som slås op, gemmes, sendes eller overføres til tjenesterne af dig (hvis der er tale om dit indhold) eller tredjepart (hvis der er tale om alt indhold mere generelt), eller for eventuelle tab eller skader. Ydermere er Tripadvisors virksomheder ikke ansvarlige for evt. fejl, bagvaskelser, ærekrænkelser, injurier, udeladelser, usandheder, sjofelheder, pornografi eller stødende ord, du måtte blive udsat for. Som leverandør af interaktive tjenester, og i det omfang, at det er tilladt ved gældende lovgivning, er Tripadvisor s virksomheder ikke ansvarlige for eventuelle meddelelser, erklæringer eller andet indhold, der indsendes af brugerne (herunder dit indhold) på websitene eller andre fora. Selvom Tripadvisors virksomheder ikke er forpligtet til at sortere, redigere eller overvåge indhold, der offentliggøres eller distribueres gennem et interaktivt område, forbeholder Tripadvisor sig retten til og kan efter eget skøn (eller hvis du er forbruger bosiddende i Storbritannien eller EU, efter vores rimelige skøn) og uden varsel fjerne, sortere, oversætte eller redigere indhold, der er offentliggjort eller lagret på tjenesterne på alle tidspunkter og af alle årsager eller få tredjeparter til at udføre sådanne handlinger på deres vegne. I det omfang, at det er tilladt i henhold til gældende lovgivning, er du eneansvarlig for at oprette sikkerhedskopier af og erstatte indhold, som du slår op eller på anden måde indsender til os eller gemmer på tjenesterne, og du er eneansvarlig for omkostninger og udgifter, der er forbundet dermed. Alle oplysninger om vores politikker for indholdsmoderation, herunder de begrænsninger, som vi kan pålægge dit indhold, findes [her](https://www.tripadvisor.dk/Trust).

**BEGRÆNSNING OG/ELLER SUSPENDERING AF DIN BRUG AF TJENESTERNE**

Enhver brug af interaktive områder eller øvrige aspekter af tjenesterne, som strider imod ovenstående, udgør en overtrædelse af vilkårene i nærværende aftale og kan bl.a. medføre permanent eller midlertidigt ophør af dine rettigheder til at bruge de interaktive områder og/eller tjenesterne mere generelt. Tripadvisor kan også suspendere behandlingen af meddelelser og klager, der indsendes via tjenesterne, hvis du ofte indsender åbenlyst ubegrundede meddelelser eller klager.

Vores tilgang til suspendering af din brug af platformen er nærmere beskrevet [her](https://www.tripadvisor.dk/Trust).

**Begrænsning af Tripadvisors licensrettigheder.** Du kan fremadrettet vælge at begrænse Tripadvisors virksomheders brug af dit indhold ifølge nærværende aftale (som beskrevet ovenfor) ved at vælge at give Tripadvisors virksomheder en mere begrænset licens som yderligere beskrevet nedenfor (heri benævnt "begrænset licens").  Du kan foretage dette valg ved at vælge tildeling af begrænset licens [her](https://www.tripadvisor.dk/Settings-cs) (bemærk, at du skal være logget ind på din konto for at foretage denne handling). Hvis du foretager dette valg, vil de rettigheder til dit indhold, som du tildeler Tripadvisors virksomheder i henhold til de licensbetingelser, der er angivet ovenfor (under termen "standardlicens"), være begrænset på nogle vigtige områder, som er beskrevet i styk 1 til 6 nedenfor, således at Tripadvisors virksomheder ikke har en standardlicens til noget af dit indhold, på nær de tekstbaserede anmeldelser og tilknyttede boblevurderinger, som du slår op (her vil Tripadvisors virksomheder fortsat være tildelt en standardlicens), men blive tildelt en "begrænset licens" i forhold til dit indhold som defineret nedenfor:

1. Når du slår dit indhold op på tjenesterne, vil den licens, du har givet Tripadvisors virksomheder til dit indhold, være begrænset til en ikke-eksklusiv, royaltyfri, overdragelig, viderelicenserbar og verdensomspændende licens til at hoste, bruge, distribuere, ændre, køre, reproducere, offentligt vise eller udføre, oversætte og skabe afledte værker af dit indhold med henblik på at vise det på tjenesterne, samt at bruge dit navn og/eller varemærke i forbindelse med dette indhold. I henhold til styk 6 nedenfor gælder den begrænsede licens for alt dit indhold (som nævnt på nær tekstbaserede anmeldelser og tilknyttede boblevurderinger), som du eller en anden på dine vegne (f.eks. en tredjepart, der bidrager til eller på anden måde administrerer din konto) gør tilgængeligt på eller i forbindelse med tjenesterne.

1. Du kan ophæve Tripadvisors virksomheders licensrettigheder til et enkelt element i dit indhold, der er underlagt en begrænset licens, ved at slette opslaget fra tjenesterne.  Tilsvarende kan du ophæve Tripadvisors virksomheders licensrettigheder til alt dit indhold, der er underlagt en begrænset licens, ved at lukke din konto (du kan finde en vejledning til at lukke en konto [her](https://www.tripadvisorsupport.com/da-DK/hc/traveler/articles/510)). Uagtet andre oplysninger vil dit indhold (a) forblive på tjenesterne i det omfang, du har delt det med andre, og disse personer har kopieret eller gemt det, før du har slettet det eller lukket din konto, (b) muligvis stadig blive vist på tjenesterne i et rimeligt tidsrum, mens vi forsøger at fjerne det, efter du har slettet det eller lukket din konto og/eller (c) blive bevaret (men ikke vist offentligt) som sikkerhedskopi i et vist tidsrum grundet tekniske, lovgivningsmæssige eller juridiske årsager samt for at bekæmpe bedrageri.

1. Tripadvisors virksomheder bruger ikke dit indhold i annoncer for tredjeparters produkter og tjenester, medmindre du har givet dit samtykke til dette (herunder sponsoreret indhold), men du er indforstået med, at Tripadvisors virksomheder kan placere reklamer og kampagner i tjenesterne sammen med, i nærheden af, tilstødende eller på anden måde tæt på dit indhold (herunder inden, under eller efter afspilning af videoer eller andet dynamisk materiale) samt andres indhold.  I alle tilfælde, hvor dit indhold vises på tjenesterne, tilskriver vi indholdet til dig ved at bruge det navn og/eller varemærke, du tilknytter dit indhold.  

1. Tripadvisors virksomheder giver ikke tredjepart lov til at offentliggøre dit indhold andre steder end på tjenesterne. Når du deler dit indhold på tjenesterne (undtagen under funktionen "Rejser", der kan gøres privat) vil dette dog resultere i, at dit indhold gøres "offentligt", og vi vil aktivere en funktion, som gør det muligt for andre brugere at dele (ved enten at indsætte det offentlige opslag eller på anden vis) dit indhold (undtagen indhold under Rejser, som du kan indstille til at være privat) på tredjepartstjenester, og vi vil gøre det muligt for søgemaskiner at finde dit offentlige indhold via deres tjenester.

1. Dine rettigheder og forpligtelser er stadig underlagt nærværende aftales ordlyd, undtaget ved de ændringer, der forekommer i styk 1 til 6 i aftalen. Den licens, du giver Tripadvisors virksomheder som ændret af styk 1 til 6, kaldes for en "begrænset licens".

1. For overskuelighedens skyld er det indhold, du sender til tjenesterne i forbindelse med andre tjenester eller programmer tilhørende Tripadvisors virksomheder, ikke underlagt en begrænset licens, men vil i stedet være underlagt de vilkår og betingelser, der er tilknyttet den pågældende Tripadvisor-tjeneste eller det pågældende Tripadvisor-program.

 

**BOOKINGER HOS TREDJEPARTSLEVERANDØRER GENNEM Tripadvisor**

**Brug af Tripadvisors bookingtjenester.** Tripadvisors virksomheder giver dig mulighed for at søge efter, vælge og foretage rejsebookinger hos tredjepartsleverandører uden at forlade tjenesterne.

#### VIGTIGT! Tripadvisor sælger ikke selv rejsereservationer. Tripadvisor giver tredjeparter mulighed for at sælge deres rejsereservationer på Tripadvisor.

#### Tripadvisor muliggør muligvis promovering og salg af rejsereservationer fra tredjeparter til dig. Tripadvisor er dog ikke den ansvarlige part for kampagnen og/eller salget. Hvis du booker en reservation hos en tredjepartsleverandør, er reservationsaftalen altid udelukkende mellem dig og tredjeparten. Tripadvisor er ikke (a) køber eller sælger af reservationen, (b) en part i reservationsaftalen med tredjeparten eller ansvarlig for opfyldelsen af denne aftale eller (c) en agent for dig eller tredjeparten i forbindelse med køb eller salg. Hvis du foretager en booking hos en tredjepartsleverandør, accepterer du at skulle gennemse og være bundet af leverandørens vilkår og betingelser for køb og brug af websitet, politik om beskyttelse af personlige oplysninger og andre regler eller politikker med relation til leverandørens website eller ejendom. Din interaktion med tredjepartsleverandører er på egen risiko. Tripadvisors virksomheder er ikke ansvarlige for handlinger, undladelser, fejl, erklæringer, garantier, brud eller forsømmelse fremsat eller begået af disse tredjepartsleverandører eller for nogen form for personskade, død, materiel skade eller andre skader eller udgifter, der er en følge af din interaktion med tredjepartsleverandører.

Din regresret i forbindelse med en aftale mellem dig og en tredjepartsleverandør (f.eks. i forbindelse med refusioner og annulleringer) vil være mellem dig og tredjeparten som angivet i aftalen med den pågældende tredjepart. I det usandsynlige tilfælde, at en reservation er tilgængelig, når du afgiver en ordre, men bliver utilgængelig før indtjekning, er dit eneste retsmiddel mellem os og dig at kontakte leverandøren for at foretage alternative arrangementer eller for at annullere din reservation og få en refusion (hvis det er relevant og underlagt gældende lovgivning og din aftale med tredjepartsleverandøren).

#### Tredjepartsleverandører kan være virksomheder eller forbrugere. Hvis du er forbruger bosiddende i Storbritannien eller EU, og den tredjepartsleverandør, som du har en aftale med, også er forbruger, skal du være opmærksom på, at du ikke kan håndhæve nogen forbrugerrettigheder over for denne leverandør.

Når du booker rejsereservationer via vores websites, skal du oprette en konto, hvis du ikke allerede har gjort det, og du anerkender, at du accepterer den praksis, der er beskrevet i vores politik om beskyttelse af personlige oplysninger og i nærværende aftale.

SOM BRUGER AF TJENESTERNE, HERUNDER BOOKINGTJENESTERNE TILHØRENDE TRIPADVISORS VIRKSOMHEDER, ER DU, I DET OMFANG, AT DET ER TILLADT I HENHOLD TIL GÆLDENDE LOVGIVNING, INDFORSTÅET MED, AT: (1) TRIPADVISORS VIRKSOMHEDER IKKE HAR NOGET ANSVAR OVER FOR DIG OG ANDRE VED EVT. ULOVLIGE TRANSAKTIONER, DER ER FORETAGET VIA DIN ADGANGSKODE ELLER KONTO, OG (2) DU VED ULOVLIG BRUG AF DIN ADGANGSKODE ELLER KONTO KAN BLIVE HOLDT ANSVARLIG OVER FOR TRIPADVISOR, TILKNYTTEDE VIRKSOMHEDER OG/ELLER ANDRE.

Det er gratis at bruge vores tjenester, herunder oprettelse af en Tripadvisor-konto, og en betaling vil kun blive behandlet, når du booker en reservation via tjenesterne. Når du foretager en booking via Tripadvisors virksomheder, indhenter vi dine betalingsoplysninger og overfører dem til leverandøren for at fuldføre transaktionen, som beskrevet i vores politik om beskyttelse af personlige oplysninger. Bemærk, at leverandøren, og ikke Tripadvisors virksomheder, er ansvarlig for at behandle din betaling og opfylde din booking.

Tripadvisors virksomheder griber ikke vilkårligt ind i bookinger, men forbeholder sig retten til at tilbagekalde bookingtjenester ved bestemte formildende omstændigheder, som f.eks. når en booking ikke længere er tilgængelig, eller når vi har begrundet mistanke om, at en bookinganmodning kan være svigagtig. Tripadvisors virksomheder forbeholder sig også retten til at verificere din identitet for at behandle din bookinganmodning.

**Tredjepartsleverandører.** Tripadvisors virksomheder er ikke rejsebureauer og leverer eller ejer ikke transporttjenester, indkvarteringssteder, restauranter, ture, aktiviteter eller oplevelser. Selvom Tripadvisors virksomheder viser oplysninger om ejendomme, der ejes af tredjepartsleverandører, og formidler bookinger mellem dig og visse leverandører på eller igennem websites tilhørende Tripadvisors virksomheder, vil sådanne handlinger på ingen måde indikere eller udgøre en sponsorering eller godkendelse af tredjepartsleverandører eller nogen form for tilknytning mellem Tripadvisors virksomheder og tredjepartsleverandører. Selvom brugere kan bedømme og anmelde bestemte transporttjenester, indkvarteringssteder, restauranter, ture, aktiviteter eller oplevelser baseret på deres egne oplevelser, støtter eller anbefaler Tripadvisors virksomheder ikke produkter eller tjenester tilhørende tredjepartsleverandører, undtagen når Tripadvisor tildeler bestemte virksomheder priser, der er baseret på anmeldelser af brugere. Tripadvisors virksomheder støtter ikke indhold, der slås op, indsendes eller på anden måde offentliggøres af en bruger eller virksomhed, eller nogen holdning, anbefaling eller noget råd udtrykt heri, og Tripadvisors virksomheder fraskriver sig udtrykkeligt ethvert ansvar i forbindelse med dette indhold. Du accepterer, at Tripadvisors virksomheder ikke er ansvarlige for rigtigheden eller fuldkommenheden af oplysninger, de indhenter fra tredjepartsleverandører og viser på tjenesterne.

Tjenesterne kan indeholde links til leverandørers websites eller andre websites, som Tripadvisor ikke driver eller har kontrol over. Du kan få flere oplysninger ved at se afsnittet "Links til tredjepartswebsites" nedenfor.

**Booking af rejsetjenester via Tripadvisor-mobilappen.**  Nogle af de rejsetjenester, der bookes via Tripadvisors mobilapp, er underlagt yderligere brugsbetingelser.  Disse betingelser er tilgængelige via selve appen og kan findes [her](https://tripadvisor.mediaroom.com/us-terms-of-use-booking).

**_Booking af ferieboliger, bordreservationer på restauranter og oplevelser hos tredjepartsleverandører, der er opført på tilknyttede websites._** Nogle af Tripadvisors søsterselskaber fungerer som markedspladser, der gør rejsende i stand til at (1) indgå aftaler om leje af ferieboliger med ejendommens ejere og bestyrere ("ferieboliger"), (2) reservere bord på restauranter ("restauranter") og/eller (3) booke plads på ture, til aktiviteter og seværdigheder (samlet "oplevelser") hos tredjepartsleverandører af disse oplevelser (hver enkelt leverandør af en feriebolig og/eller oplevelse kaldes for en "annoncør"). Tripadvisors søsterselskaber syndikerer deres annoncer til andre enheder i gruppen, der udgør Tripadvisors virksomheder, og derfor får du dem vist på websites tilhørende Tripadvisors virksomheder.  Som bruger er du ansvarlig for din brug af tjenesterne (herunder i særdeleshed websites tilhørende Tripadvisors virksomheder) og enhver transaktion omhandlende ferieboliger, restauranter eller oplevelser, der formidles af Tripadvisors søsterselskaber. Vi hverken ejer, administrerer eller indgår kontrakter for nogen ferieboliger, restauranter eller oplevelser, der er opført på tjenesterne.

Da hverken Tripadvisor eller Tripadvisors søsterselskaber er parter i transaktioner vedrørende ferieboliger, bordreservationer på restauranter eller oplevelsesrelaterede transaktioner mellem rejsende og annoncører, er konflikter angående en faktisk eller potentiel transaktion mellem dig og en annoncør, herunder om kvalitet, stand, sikkerhed eller legalitet af en registreret feriebolig, restaurant eller oplevelse, nøjagtigheden af det registrerede indhold, annoncørens ret til at udleje en feriebolig, reservere et bord eller levere et måltid eller anden service på en restaurant eller levere en oplevelse eller din mulighed for at betale for en feriebolig, et måltid på en restaurant eller en oplevelse, udelukkende den enkelte brugers ansvar.

Et af Tripadvisors søsterselskaber kan fungere som en annoncørs midlertidige agent med det ene formål at overføre din betaling til annoncøren. Du accepterer at betale en annoncør eller et af Tripadvisors søsterselskaber, der fungerer som midlertidig betalingsagent på vegne af en annoncør, eventuelle gebyrer, der er angivet til dig, før du foretager en booking for en ferieudlejningsreservation eller oplevelse, og eventuelle gebyrer, du opkræves som følge af skader forårsaget af din brug af udlejningsreservationen eller oplevelsen i overensstemmelse med gældende betingelser eller politikker.

Ønsker du yderligere oplysninger om gebyrer for ferieboliger, depositummer, gebyrer for oplevelser, betalingsbehandling, tilbagebetalinger og lignende, bedes du læse vores søsterselskabers vilkår og betingelser (for oplevelser bedes du gå til [Viator](https://www.viator.com/da-DK/support/termsAndConditions), for restauranter skal du gå til [The Fork](https://www.thefork.co.uk/#geo_dmn_redirect=20210629AT100), for ferieboliger skal du gå til [Tripadvisors virksomheders ferieboliger](https://rentals.tripadvisor.com/en_UK/termsandconditions/traveler). Når du lejer en feriebolig, reserverer et bord på en restaurant eller booker en oplevelse via et af vores søsterselskaber, skal du bekræfte og acceptere dette selskabs vilkår og betingelser samt politik om beskyttelse af personlige oplysninger.

Hvis du indgår i en tvist med en annoncør i EU, kan du finde alternative metoder til løsning af denne tvist her: [http://ec.europa.eu/consumers/odr/](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=DA).

**REJSEMÅL**

**Internationale rejser.** Når du foretager internationale rejsebookinger hos tredjepartsleverandører eller planlægger internationale rejser via tjenesterne, er du ansvarlig for at sikre, at du lever op til alle krav til adgang som udlænding, og at dine rejsedokumenter, herunder pas og visa, er i orden.

Kontakt den relevante ambassade eller det relevante konsulat for at få oplysninger om krav til pas og visa. Da kravene kan ændre sig når som helst, skal du gennemse de opdaterede oplysninger før booking og før afgang. Tripadvisors virksomheder fralægger sig ethvert ansvar for, at rejsende kan blive nægtet adgang til flyrejser eller til lande på grund af manglende rejsedokumenter, som er påkrævet af flyselskabet, myndighederne eller landet, herunder lande, hvor den rejsende kun er på gennemrejse på vej til sit rejsemål.

Det er også dit ansvar at opsøge en læge for at få de aktuelle anbefalinger til vacciner, inden du rejser internationalt, og for at sikre, at du lever op til alle helbredsmæssige adgangskrav og følger al medicinsk vejledning i relation til din rejse.

Selvom de fleste rejser, herunder rejser til rejsemål i udlandet, sker, uden at der opstår problemer, kan rejser til visse rejsemål indebære en større risiko end andre. Tripadvisor opfordrer rejsende til at undersøge og gennemgå rejseforbud, advarsler, bekendtgørelser og anbefalinger, der udstedes af deres egne myndigheder og myndigheder i destinationslandet, forud for booking af rejser til internationale rejsemål. Dette kan f.eks. være oplysninger om forhold i forskellige lande og risikoniveauet ved rejser til bestemte internationale rejsemål, som leveres af den amerikanske regering på [www.state.gov](http://www.state.gov/), [www.tsa.gov](https://www.tsa.gov/), [www.dot.gov](http://www.dot.gov/), [www.faa.gov](http://www.faa.gov/), [www.cdc.gov](http://www.cdc.gov/), [www.treas.gov/ofac](http://www.treas.gov/ofac) og [www.customs.gov](http://www.customs.gov/).

VED AT REGISTRERE OPLYSNINGER OM REJSER TIL BESTEMTE INTERNATIONALE REJSEMÅL GIVER TRIPADVISORS VIRKSOMHEDER INGEN ERKLÆRINGER ELLER GARANTI FOR, AT DISSE STEDER ANBEFALES ELLER ER UDEN RISIKO, OG VI ER IKKE ANSVARLIGE FOR SKADE ELLER TAB SOM FØLGE AF REJSE TIL DISSE REJSEMÅL.

**ANSVARSFRASKRIVELSE**

LÆS DETTE AFSNIT OMHYGGELIGT. DETTE AFSNIT BEGRÆNSER TRIPADVISORS VIRKSOMHEDERS ANSVAR OVER FOR DIG FOR PROBLEMER, SOM KAN FOREKOMME I FORBINDELSE MED DIN BRUG AF TJENESTERNE. HVIS DU IKKE FORSTÅR BETINGELSERNE I DETTE AFSNIT ELLER ANDRE STEDER I NÆRVÆRENDE AFTALE:

OPLYSNINGER, SOFTWARE, PRODUKTER OG TJENESTER, DER ER OFFENTLIGGJORT PÅ ELLER PÅ ANDEN MÅDE LEVERET AF TJENESTERNE (HERUNDER, MEN IKKE BEGRÆNSET TIL, DEM, DER ER OPRETTET ELLER DREVET AF KUNSTIG INTELLIGENS), KAN VÆRE UNØJAGTIGE ELLER INDEHOLDE FEJL, HERUNDER BOOKINGERS TILGÆNGELIGHED OG PRISFEJL. TRIPADVISORS VIRKSOMHEDER GARANTERER IKKE FOR NØJAGTIGHEDEN AF, OG FRASKRIVER SIG ETHVERT ANSVAR FOR, ALLE ÅBENLYSE FEJL ELLER ANDRE UNØJAGTIGHEDER VEDRØRENDE DE OPLYSNINGER OG BESKRIVELSER AF INDKVARTERINGSSTEDER, OPLEVELSER, LUFTFART, KRYDSTOGTER, RESTAURANTER ELLER ANDRE REJSEPRODUKTER, DER ER OPFØRT PÅ TJENESTERNE (HERUNDER, MEN IKKE BEGRÆNSET TIL, PRISER, RÅDIGHED, BILLEDER, LISTE OVER INDKVARTERINGSSTEDER, OPLEVELSER, LUFTFART, KRYDSTOGTER, RESTAURANTER ELLER ANDRE REJSEPRODUKTER OG -FACILITETER, GENERELLE PRODUKTBESKRIVELSER, ANMELDELSER OG VURDERINGER OSV.). TRIPADVISORS VIRKSOMHEDER FORBEHOLDER SIG ENDVIDERE UDTRYKKELIGT RET TIL AT ÆNDRE FEJL I TILGÆNGELIGHED OG ÅBENLYSE FEJL I PRISER PÅ TJENESTERNE OG/ELLER I KOMMENDE BOOKINGER, DER ER FORETAGET TIL EN FORKERT PRIS.

TRIPADVISOR UDTRYKKER SIG IKKE PÅ NOGEN MÅDE OM TJENESTERNES EGNETHED, HERUNDER OPLYSNINGER PÅ TRIPADVISORS WEBSITES ELLER NOGEN DEL HERAF, TIL NOGET SOM HELST FORMÅL, OG INDBEFATNING ELLER TILBUD AF PRODUKTER ELLER SERVICEYDELSER PÅ TRIPADVISORS WEBSITES ELLER PÅ ANDEN VIS VIA TJENESTERNE UDGØR IKKE EN GODKENDELSE ELLER ANBEFALING AF DISSE PRODUKTER ELLER SERVICEYDELSER AF TRIPADVISOR, UAGTET EVENTUELLE PRISER, SOM MÅTTE VÆRE UDDELT BASERET PÅ BRUGERNES ANMELDELSER. ALLE SÅDANNE OPLYSNINGER, SOFTWARE, PRODUKTER OG SERVICEYDELSER STILLES TIL RÅDIGHED AF ELLER VIA TJENESTERNE "I FORHÅNDENVÆRENDE STAND" UDEN NOGEN GARANTI AF NOGEN ART. I DET MAKSIMALE OMFANG DET ER TILLADT I HENHOLD TIL GÆLDENDE LOVGIVNING, FRALÆGGER TRIPADVISOR SIG ALLE GARANTIER OG BETINGELSER FOR, AT TJENESTER, SERVERE ELLER DATA (HERUNDER E-MAILS), DER SENDES FRA TRIPADVISOR, ER FRI FOR VIRA ELLER ANDRE SKADELIGE KOMPONENTER. I DET MAKSIMALE OMFANG DET ER TILLADT I HENHOLD TIL GÆLDENDE LOVNING, FRALÆGGER TRIPADVISOR SIG HERMED ALLE GARANTIER OG BETINGELSER MED HENSYN TIL DISSE OPLYSNINGER, SOFTWARE, PRODUKTER OG TJENESTER, HERUNDER ALLE STILTIENDE GARANTIER OG BETINGELSER ELLER VILKÅR AF NOGEN SLAGS FOR SALGBARHED, EGNETHED TIL ET BESTEMT FORMÅL, TITEL, PASSIV BESIDDELSE OG IKKE-KRÆNKELSE.

TRIPADVISORS VIRKSOMHEDER FRALÆGGER SIG OGSÅ UDTRYKKELIGT GARANTIER, ERKLÆRINGER ELLER ANDRE BETINGELSER AF NOGEN SLAGS I FORHOLD TIL NØJAGTIGHEDEN ELLER EJENDOMSRETTEN FOR INDHOLD, DER STILLES TIL RÅDIGHED AF ELLER VIA TJENESTERNE.

TREDJEPARTSLEVERANDØRERNE, SOM LEVERER OPLYSNINGER OM INDKVARTERINGSSTEDER, FLYREJSER, FERIEBOLIGER, OPLEVELSER, RESTAURANTER ELLER KRYDSTOGTER, ELLER SOM TILBYDER REJSER ELLER ANDRE SERVICEYDELSER PÅ ELLER VIA TJENESTERNE, ER SELVSTÆNDIGE ERHVERVSDRIVENDE OG IKKE AGENTER ELLER MEDARBEJDERE I TRIPADVISORS VIRKSOMHEDER. TRIPADVISORS VIRKSOMHEDER ER IKKE ANSVARLIGE FOR HANDLINGER, FEJL, UNDLADELSER, ERKLÆRINGER, GARANTIER, BRUD ELLER FORSØMMELSE FREMSAT ELLER BEGÅET AF DISSE LEVERANDØRER ELLER FOR NOGEN FORM FOR PERSONSKADE, DØD, MATERIEL SKADE ELLER ANDRE SKADER ELLER UDGIFTER, DER ER EN FØLGE HERAF. TRIPADVISOR KAN IKKE HOLDES ANSVARLIG FOR OG VIL IKKE FORETAGE REFUSIONER I TILFÆLDE AF NOGEN SOM HELST FORSINKELSE, AFBESTILLING, OVERBOOKNING, STREJKE, FORCE MAJEURE ELLER ANDRE ÅRSAGER, DER LIGGER UDEN FOR TRIPADVISORS KONTROL, OG VIRKSOMHEDEN PÅTAGER SIG IKKE NOGET ANSVAR FOR EKSTRAUDGIFTER, UNDLADELSER, FORSINKELSER, OMDIRIGERING ELLER HANDLINGER, SOM KAN HENFØRES TIL EN REGERING ELLER MYNDIGHED.

UNDERLAGT OVENSTÅENDE BRUGER DU TJENESTERNE PÅ EGET ANSVAR, OG UNDER INGEN OMSTÆNDIGHEDER VIL TRIPADVISORS VIRKSOMHEDER (ELLER DISSES MELLEMLEDERE, DIREKTØRER OG/ELLER MEDARBEJDERE) VÆRE ANSVARLIGE FOR NOGEN DIREKTE, INDIREKTE, STRAFFEMÆSSIGT, TILFÆLDIGT, SPECIELT ELLER FØLGEMÆSSIGT TAB ELLER SKADE SAMT TAB AF INDKOMST, OVERSKUD, GOODWILL, DATA, KONTRAKTER, BRUG AF PENGE ELLER TAB ELLER SKADE, DER OPSTÅR SOM FØLGE AF ELLER PÅ ANDEN MÅDE ER TILKNYTTET FORSTYRRELSER I FORRETNINGSAKTIVITETER AF NOGEN ART, DER OPSTÅR ELLER PÅ ANDEN MÅDE ER TILKNYTTET DIN ADGANG TIL, VISNING AF ELLER BRUG AF TJENESTERNE ELLER EN OPSTÅET FORSINKELSE ELLER MANGLENDE EVNE TIL AT FÅ ADGANG TIL, FÅ VIST ELLER BRUGE TJENESTERNE (HERUNDER UDEN BEGRÆNSNING DIN TILLID TIL ANMELDELSER OG MENINGER, DER VISES PÅ ELLER VIA TJENESTERNE, VIRA, FEJL, TROJANSKE HESTE, OPLYSNINGER, SOFTWARE, WEBSITES, DER LINKES TIL, PRODUKTER OG SERVICEYDELSER, DER OPNÅS VIA TJENESTERNE (HERUNDER UDEN BEGRÆNSNING ALLE SYNKRONISERINGSPRODUKTER TILHØRENDE TRIPADVISORS VIRKSOMHEDER), PERSONSKADER ELLER BESKADIGELSE AF EJENDOM AF NOGEN ART, SOM FØLGE AF DIN BRUG AF TJENESTERNES SERVERE OG/ELLER ALLE PERSONLIGE OPLYSNINGER OG/ELLER FINANSIELLE OPLYSNINGER, DER GEMMES HERI, ALLE FEJL ELLER UDELADELSER I ALT INDHOLD ELLER FOR ALT TAB ELLER BESKADIGELSE AF NOGEN ART, DER OPSTÅR SOM FØLGE AF BRUGEN AF INDHOLD ELLER PÅ ANDEN MÅDE OPSTÅR SOM FØLGE AF ADGANGEN TIL, VISNINGEN AF ELLER BRUGEN AF TJENESTERNE), HVAD ENTEN DETTE ER BASERET PÅ EN ANTAGELSE OM SKØDESLØSHED, EN KONTRAKT, TORT, OBJEKTIVT ANSVAR ELLER PÅ ANDEN MÅDE, OG SELVOM TRIPADVISOR ELLER TRIPADVISORS SØSTERSELSKABER ER BLEVET UNDERRETTET OM MULIGHEDEN FOR SÅDANNE SKADER.

Hvis Tripadvisors virksomheder kendes ansvarlige for tab eller skader, der opstår som følge af eller på nogen måde er tilknyttet din brug af tjenesterne, vil Tripadvisors virksomheders ansvar under ingen omstændigheder sammenlagt overstige det største af (a) de transaktionsgebyrer, der er betalt til Tripadvisors virksomheder for transaktionen/transaktionerne på eller via tjenesterne, og som danner grundlag for kravet eller (b) et hundrede dollars (USD 100,00).

Ansvarsbegrænsningen afspejler risikoens fordeling mellem parterne. De i dette afsnit indeholdte begrænsninger vil fortsat være gældende, selv hvis et eventuelt begrænset retsmiddel anført i nærværende vilkår måtte vise sig at være utilstrækkeligt. Ansvarsbegrænsningerne, der fremgår af nærværende vilkår, er til fordel for Tripadvisors virksomheder.

NÆRVÆRENDE AFTALE OG FOREGÅENDE ANSVARSFRASKRIVELSE PÅVIRKER IKKE OBLIGATORISKE JURIDISKE RETTIGHEDER, SOM MAN IKKE KAN UNDTAGES FRA I HENHOLD TIL GÆLDENDE LOVGIVNING, F.EKS. DEN FORBRUGERLOVGIVNING DER MÅTTE VÆRE GÆLDENDE I VISSE LANDE. I STORBRITANNIEN OG EU OMFATTER DETTE ANSVAR FOR DØDSFALD ELLER PERSONSKADE FORÅRSAGET AF VORES UFORSØMMELIGHED ELLER VORES MEDARBEJDERES, AGENTERS ELLER UNDERLEVERANDØRERS UFORSØMMELIGHED OG FOR SVIG ELLER SVIGAGTIG FREMSTILLING.

Hvis du er forbruger i Storbritannien eller EU, udelukker nærværende aftale ikke ethvert ansvar for tab og skader forårsaget af misligholdelse af enhver forpligtelse til at handle med rimelig omhu og dygtighed eller ved misligholdelse af nærværende aftale, forudsat at de deraf følgende skader var forudsigelige, da forpligtelserne opstod.     

HVIS LOVGIVNINGEN I DET LAND, HVOR DU BOR, IKKE TILLADER EN BESTEMT BEGRÆNSNING ELLER FRASKRIVELSE AF ANSVAR, SOM BESKREVET I DETTE AFSNIT, VIL BEGRÆNSNINGEN IKKE VÆRE GÆLDENDE. ANSVARSFRASKRIVELSEN VIL UNDER ALLE ANDRE OMSTÆNDIGHEDER VÆRE GÆLDENDE I VIDEST MULIGE UDSTRÆKNING, SOM ER TILLADT AF DIN NATIONALE LOVGIVNING.

**SKADESLØSHOLDELSE**

Dette afsnit "Skadesløsholdelse" gælder ikke for forbrugere, der er bosiddende i EU eller Storbritannien.

Du accepterer at forsvare og skadesløsholde Tripadvisors virksomheder og deres mellemledere, direktører, medarbejdere og agenter fra nogen som helst ansvarsholdelse, søgsmålsgrunde, krav, erstatninger, tab, skader, bøder, straffe eller andre udgifter eller omkostninger af nogen slags, herunder, men ikke begrænset til, rimelige juridiske og regnskabsmæssige gebyrer fremsat af tredjemand som følge af:

* (i) din misligholdelse af denne aftale eller dokumenter, der er henvist til heri
* (ii) din overtrædelse af nogen lov eller tredjeparts rettigheder
* (iii) din brug af tjenesterne, herunder websites tilhørende Tripadvisors virksomheder.

**LINKS TIL TREDJEPARTSWEBSITES**

Tjenesterne kan indeholde hyperlinks til websites, der drives af andre end Tripadvisors virksomheder. Disse hyperlinks er kun vejledende. Tripadvisors virksomheder har ingen kontrol over disse websites og er ikke ansvarlige for deres indhold eller behandling af personlige oplysninger eller anden praksis på disse websites. Endvidere er det op til dig selv at tage de nødvendige foranstaltninger for at sikre, at ethvert link, som du trykker på, eller software, som du downloader (fra dette eller fra andre websites), ikke indeholder vira, orme, trojanske heste, defekter eller andet skadeligt indhold. Tripadvisors virksomheders medtagelse af hyperlinks til disse websites betyder ikke, at vi anbefaler materialet på disse websites eller har forbindelse til dem, der driver disse websites.

I nogle tilfælde kan et tredjepartswebsite eller en tredjepartsapp bede dig om at knytte din profil på Tripadvisor til en profil på et andet tredjepartswebsite. Det er helt op til dig at beslutte dig for, om du vil gøre dette, og beslutningen om at tillade, at disse oplysninger tilknyttes, kan deaktiveres (på tredjepartswebsitet eller appen) når som helst. Hvis du vælger at knytte din Tripadvisor-konto til et tredjepartswebsite eller en tredjepartsapp, vil tredjepartens website eller app kunne få adgang til de oplysninger, du har gemt på din Tripadvisor-konto, herunder oplysninger om andre brugere, som du deler oplysninger med. Du bør læse alle vilkår og betingelser samt politik om beskyttelse af personlige oplysninger for de tredjepartswebsites og -apps, du besøger, da deres regler for og tilladelser til brug af dine oplysninger kan afvige fra vores tjenester, herunder vores websites. Vi opfordrer dig til at gennemse disse tredjepartswebsites og -apps og huske, at de bruges på eget ansvar.

**SOFTWARE SOM EN DEL AF TJENESTER, YDERLIGERE MOBILLICENSER**

Software fra tjenesterne er endvidere underlagt USA's eksportregulerende foranstaltninger. Ingen software fra tjenesterne må downloades eller på anden vis eksporteres eller reeksporteres (a) til (eller til en statsborger eller indbygger i) Cuba, Irak, Sudan, Nordkorea, Iran, Syrien eller et andet land, som USA har et handelsembargo mod, eller (b) til nogen, der står på det amerikanske finansministeriums liste over "Specially Designated Nationals" (særligt udpegede statsborgere) eller på det amerikanske handelsministeriums "Table of Deny Orders" (liste over afviste handelspartnere). Når du bruger tjenesterne, anerkender du, at du ikke befinder dig i, kontrolleres af eller er statsborger eller indbygger i et sådant land eller er opført på en sådan liste.

Som nævnt ovenfor omfatter tjenesterne software, der til tider kan kaldes for "apps".  Al software, som kan downloades fra tjenesterne ("software"), er ophavsbeskyttede værker tilhørende Tripadvisor eller andre anførte parter. Brugen af denne software er underlagt vilkårene i den eventuelle slutbrugerlicensaftale, der følger med eller er inkluderet med softwaren. Du må ikke installere eller bruge software, som er inkluderet i, eller hvortil der medfølger, en licensaftale, medmindre du først accepterer vilkårene i denne licensaftale. For software, der kan downloades via tjenesterne, hvor der ikke medfølger en licensaftale, giver vi dig hermed som bruger en begrænset, personlig licens, der ikke kan overdrages, til at bruge softwaren til visning og anden brug af tjenesterne i henhold til nærværende aftales vilkår og betingelser (herunder de politikker der nævnes heri) og ikke til noget andet formål.

Du skal vide, at softwaren, herunder f.eks. al HTML-, XML- og Java-kode samt Active X-kontrolfunktioner, der indgår i tjenesterne, ejes eller licenseres af Tripadvisor og beskyttes af lovgivning om ophavsret og internationale traktatbestemmelser. Enhver reproduktion eller videredistribution af softwaren er udtrykkeligt forbudt og kan resultere i alvorlige civil- og strafferetlige sanktioner. Overtrædere vil blive retsforfulgt i videst muligt omfang.

Dele af Tripadvisors mobilsoftware anvender muligvis ophavsretligt materiale, og denne brug er godkendt af Tripadvisor. Endvidere gælder særlige vilkår for anvendelsen af visse af Tripadvisors mobilapps. Gå til siden for [mobillicenser](https://tripadvisor.mediaroom.com/DK-mobile-licenses) for at finde meddelelser, som er specifikke for Tripadvisors mobilapps.

UDEN AT BEGRÆNSE OVENSTÅENDE ER KOPIERING ELLER REPRODUKTION AF SOFTWAREN TIL EN ANDEN SERVER ELLER PLACERING TIL YDERLIGERE REPRODUKTION ELLER DISTRIBUTION UDTRYKKELIGT FORBUDT. UNDERLAGT GÆLDENDE LOVGIVNING GIVES DER KUN GARANTI FOR SOFTWAREN, OM NOGEN OVERHOVEDET, I HENHOLD TIL VILKÅRENE I LICENSAFTALEN ELLER NÆRVÆRENDE AFTALE (SOM RELEVANT).

**COPYRIGHT- OG VAREMÆRKEMEDDELELSER**

Tripadvisor, uglelogoet, vurderingsboblerne og alle andre produkt- og tjenestenavne eller -slogans, som vises på tjenesterne, er registrerede og/eller ikke-registrerede civilretligt ejede varemærker tilhørende Tripadvisor LLC og/eller dennes leverandører eller licensgivere og må ikke kopieres, efterlignes eller bruges, hverken helt eller delvist, uden forudgående skriftlig tilladelse fra Tripadvisor eller indehaveren af det pågældende varemærke. Derudover er udseendet og udtrykket af tjenesterne, herunder vores websites såvel som overskrifter, tilpasset grafik, knappers ikoner og scripts tilknyttet samme, tjenestemærker, varemærker og/eller varemærkeudstyr, der tilhører Tripadvisor, og må ikke kopieres, efterlignes eller bruges, hverken helt eller delvist, uden forudgående skriftlig tilladelse fra Tripadvisor. Alle andre varemærker, registrerede varemærker, produktnavne og navne på virksomheder eller logoer, der er omtalt på tjenesterne, tilhører de respektive ejere. Undtagen i det omfang, der er angivet andetsteds i nærværende aftale, udgør eller indebærer henvisning til produkter, tjenester, processer eller andre oplysninger ved handelsnavn, varemærke, producent, leverandør eller andet ikke Tripadvisors anbefaling eller sponsorat deraf.

Alle rettigheder forbeholdes. Tripadvisor er ikke ansvarlig for indhold på websites, der drives af andre end Tripadvisor.

**Politik om anmeldelse og fjernelse af ulovligt indhold**

Tripadvisor anvender princippet om "anmeldelse og fjernelse". Hvis du har klager eller indvendinger imod noget indhold, herunder brugermeddelelser, som lægges op på tjenesterne, eller hvis du mener, at materiale eller indhold, der offentliggøres på tjenesterne, krænker en ophavsret, som tilhører dig, skal du kontakte os omgående ved at følge vores procedure for anmeldelse og fjernelse. [**Klik her for at se politikken om ophavsret og tilhørende procedure**](https://www.tripadvisor.co.uk/pages/noticetakedown.html). Når denne procedure er blevet fulgt, vil Tripadvisor reagere på gyldige og korrekt underbyggede klager ved at foretage alle rimelige bestræbelser på at fjerne åbenlyst ulovligt indhold inden for et rimeligt tidsrum.

**ÆNDRINGER AF NÆRVÆRENDE AFTALE; TJENESTERNE; OPDATERINGER; OPHØR OG FORTRYDELSE**

**Nærværende aftale:**

Tripadvisor kan til enhver tid ændre, tilføje eller slette nærværende aftales vilkår og betingelser eller en del deraf efter eget skøn (eller efter vores rimelige skøn for forbrugere bosiddende i Storbritannien eller EU), når vi vurderer, det er nødvendigt i henhold til juridiske, generelle lovgivningsmæssige og tekniske formål, eller grundet ændringer i de tjenester, der leveres, eller tjenesternes form eller layout. Derefter accepterer du udtrykkeligt at være bundet af nærværende aftales vilkår og betingelser med senere ændringer.

**Ændringer:**

Tripadvisors virksomheder kan til enhver tid og uden beregning for dig ændre, suspendere eller stoppe alle aspekter af tjenesterne, herunder tilgængeligheden af tjenesternes funktioner, databaser eller indhold, herunder:

(a)        at sikre overholdelse af gældende lovgivning og/eller afspejle ændringer af relevant lovgivning og relevante myndighedskrav, såsom obligatorisk forbrugerlovgivning

(b)        at udføre midlertidig vedligeholdelse, rette fejl, implementere tekniske justeringer med henblik på drift eller foretage forbedringer, såsom at tilpasse tjenesterne til et nyt teknisk miljø, overføre tjenesterne til en ny hostingplatform eller sikre tjenestens kompatibilitet med enhederne og softwaren (som f.eks. opdateres fra tid til anden)

(c)        at opgradere eller ændre tjenesterne, herunder frigivelse af nye versioner af websites på visse enheder eller på anden måde ændre eller foretage ændringer af eksisterende funktioner og funktionalitet

(d)        at ændre strukturen, designet eller layoutet af tjenesterne, herunder ændre navnet på eller rebranding af tjenesterne eller ændre, forbedre og/eller udvide de tilgængelige funktioner

(e)        af sikkerhedsmæssige årsager.

For brugere i Storbritannien og EU: Hvis vi foretager ændringer af tjenesterne, som vi med rimelighed mener vil påvirke din adgang til eller brug af tjenesterne væsentligt negativt, giver vi dig mindst 30 dages varsel om ændringerne. Hvis du ikke opsiger din kontrakt med os, før disse ændringer finder sted, betragter vi det som din accept af ændringerne.

**Opdateringer:**

Vi kan foretage opdateringer for at holde tjenesterne i overensstemmelse med gældende forbrugerlovgivning. Hvis automatiske opdateringer er aktiveret i dine indstillinger, opdaterer vi automatisk tjenesterne. Du kan til enhver tid ændre dine indstillinger, hvis du foretrækker ikke længere at modtage automatiske opdateringer.

For at få den bedste oplevelse og for at sikre, at tjenesterne fungerer korrekt, anbefaler vi, at du accepterer alle opdateringer af tjenesterne, som vi informerer dig om, når de bliver tilgængelige. Dette kræver muligvis også, at du opdaterer enhedens operativsystem. Efterhånden som nye operativsystemer og enheder frigives, kan vi med tiden stoppe med at understøtte ældre versioner.

Hvis du vælger ikke at downloade eller installere nogen opdateringer, er tjenesterne muligvis ikke længere tilgængelige eller understøttede, eller de har tidligere funktionalitet.

Vi kan ikke stilles til ansvar, hvis du undlader at downloade eller installere en opdatering eller den seneste offentliggjorte version af tjenesterne for at kunne drage fordel af nye eller forbedrede funktioner og/eller opfylde kompatibilitetskrav, hvor vi har informeret dig om opdateringen, forklaret konsekvenserne af at undlade at installere den og leveret installationsvejledninger.

Alle ændringer af tjenesten, der ikke er i overensstemmelse med dette afsnit "Opdateringer", er underlagt afsnittet "Ændringer" ovenfor.

**Ophør:**

Tripadvisors virksomheder kan også indføre begrænsninger for eller på anden måde indskrænke din adgang til alle eller nogle af tjenesterne uden varsel eller ansvar af tekniske eller sikkerhedsmæssige årsager, for at forhindre uautoriseret adgang, tab af eller destruktion af data, eller hvor Tripadvisor og/eller dennes søsterselskaber efter eget skøn (eller efter vores/deres rimelige skøn for forbrugere bosiddende i Storbritannien eller EU) vurderer, at du er i strid med en bestemmelse i nærværende aftale eller overtræder nogen lovgivning eller bestemmelse, og hvor Tripadvisor og/eller dennes søsterselskaber beslutter sig for at indstille leveringen af et aspekt af tjenesterne.

Tripadvisor kan uden varsel ophæve nærværende aftale med dig, hvis det i god tro vurderes, at du har overtrådt nærværende aftale, eller det på anden måde skønnes, at en ophævelse er nødvendig med al rimelighed for at sikre de rettigheder, der tilhører Tripadvisors virksomheder og/eller andre brugere af tjenesterne. Dette betyder, at vi kan ophøre med at levere tjenester til dig.

**VÆRNETING OG GÆLDENDE LOVGIVNING**

Dette website ejes og kontrolleres af Tripadvisor LLC, et amerikansk aktieselskab. Nærværende aftale og alle tvister eller krav (herunder tvister eller krav uden for kontraktforhold), der opstår som følge af eller i forbindelse med aftalen eller dennes indhold, er underlagt og fortolkes i henhold til lovgivningen i Massachusetts, USA. Du giver hermed dit samtykke til, at søgsmål skal anlægges ved domstolene i Massachusetts, USA, som har eksklusivt værneting, og accepterer retfærdigheden og egnetheden af processer ved sådanne domstole i forbindelse med enhver tvist, den være sig kontraktlig eller uden for kontraktforhold, som følge af eller vedrørende din eller en tredjeparts brug af tjenesterne. Du accepterer, at alle de erstatningskrav, som du kunne have imod Tripadvisor LLC som følge af eller vedrørende tjenesterne, skal høres og løses i en domstol med kompetent værneting for emnet i Massachusetts. Brug af tjenesterne er ikke godkendt i værneting, som ikke håndhæver alle bestemmelser i nærværende vilkår og betingelser, herunder uden begrænsning dette afsnit. Intet i denne klausul begrænser Tripadvisor LLC's ret til at tage retslige skridt imod dig ved en anden domstol eller andre domstole eller i et andet kompetent værneting. Forudgående gælder ikke i det omfang, som gældende lovgivning i dit bopælsland kræver anvendelse af en anden lovgivning og/eller et andet værneting – i særdeleshed hvis du bruger tjenesterne som forbruger – og dette kan ikke omgås ved kontrakt og vil ikke være underlagt FN's konvention om aftaler om internationale køb, hvis denne i øvrigt er gældende.  Hvis du bruger tjenesterne som forbruger, og ikke som virksomhed eller virksomhedsrepræsentant, kan du have ret til at indbringe et krav mod Tripadvisor ved domstolene i dit bopælsland. Denne klausul gælder i det omfang, det er tilladt i dit bopælsland.

**VALUTAOMREGNER**

Valutakurserne er baseret på forskellige tilgængelige offentlige kilder og er kun vejledende. Nøjagtigheden af kurserne kontrolleres ikke, og faktiske valutakurser kan være anderledes. Valutakurser opdateres muligvis ikke dagligt. De angivne oplysninger menes at være korrekte, men Tripadvisors virksomheder garanterer ikke for nøjagtigheden. Ved brug af oplysningerne til et økonomisk formål anbefaler vi, at du kontakter en kvalificeret ekspert, der kan bekræfte, at valutakurserne er nøjagtige. Vi autoriserer ikke brugen af disse oplysninger til andet formål end din personlige brug, og du forbydes udtrykkeligt at gensælge, redistribuere og bruge oplysningerne til et kommercielt formål.

**GENERELLE BESTEMMELSER**

Vi forbeholder os ret til at inddrage ethvert brugernavn, kontonavn, tilnavn, enhver titel eller ethvert andet bruger-id af en hvilken som helst årsag uden at være ansvarlig over for dig.

Du accepterer, at der ikke er tale om et joint venture, agentur, partnerskab eller ansættelsesforhold mellem dig og Tripadvisor og/eller dennes søsterselskaber som følge af nærværende aftale eller brugen af tjenesterne.

Vores opfyldelse af nærværende aftale er underlagt gældende lovgivning og retslige processer, og der er intet i nærværende aftale, der begrænser vores ret til at overholde lovens håndhævelse eller andre juridiske henvendelser eller krav vedrørende din brug af tjenesterne eller oplysninger, der gives til eller indsamles af os, hvad angår denne anvendelse. I det omfang, at det tillades i henhold til gældende lovgivning, accepterer du at indbringe et krav eller et søgsmål som følge af eller i forbindelse med din adgang til eller brug af tjenesterne inden to (2) år efter datoen, hvor kravet eller søgsmålet opstod, da kravet eller søgsmålet da ellers annulleres uigenkaldeligt.

Hvis det besluttes, at en del af nærværende aftale er ugyldig eller ikke kan håndhæves i henhold til gældende lovgivning, herunder uden begrænsning de garantiundtagelser og ansvarsbegrænsninger, som er beskrevet ovenfor, vil den bestemmelse, som er ugyldig eller ikke kan håndhæves, anses for erstattet af en bestemmelse, der er gyldig og kan håndhæves, og som mest præcist svarer til indholdet i den oprindelige bestemmelse, og resten af aftalen er fortsat gældende.

Nærværende aftale (og øvrige vilkår og betingelser, der nævnes i den) udgør hele aftalen mellem dig og Tripadvisor, hvad angår tjenesterne, og erstatter tidligere eller midlertidig kommunikation og tilbud, uanset om det er sket elektronisk, mundtligt eller skriftligt mellem dig og Tripadvisor vedrørende tjenesterne. En trykt udgave af nærværende aftale og enhver meddelelse i elektronisk format kan gøres gældende i retlige eller administrative sager på grundlag af eller vedrørende nærværende aftale i det samme omfang og med forbehold af de samme betingelser som andre forretningsdokumenter og registreringer, der oprindeligt er udarbejdet og vedligeholdt på papir.

Følgende afsnit vil eventuelt fortsat være gældende efter nærværende aftales ophør:

* #### Yderligere produkter
    
* #### Forbudte aktiviteter
    
* #### Anmeldelser, kommentarer og brug af andre interaktive områder, tildeling af licens
    
    * #### Begrænsning af Tripadvisors licensrettigheder
        
* Rejsemål
    * Internationale rejser
* Ansvarsfraskrivelse
* Skadesløsholdelse
* Software som en del af tjenester, yderligere mobillicenser
* #### Copyright- og varemærkemeddelelser
    
    * #### Politik om anmeldelse og fjernelse af ulovligt indhold
        
* #### Ændringer af tjenesterne, ophævelse
    
* #### Værneting og gældende lovgivning
    
* #### Generelle bestemmelser
    
* #### Kundeservice/hjælp
    

Vilkårene og betingelserne i nærværende aftale er tilgængelige på samme sprog som de af Tripadvisors websites og/eller apps, hvorfra tjenesterne kan tilgås.

Websites og/eller apps, hvorfra tjenesterne kan tilgås, bliver muligvis ikke altid opdateret jævnligt eller regelmæssigt og skal derfor ikke registreres som redaktionelt produkt i henhold til relevant lovgivning.

Det er ikke meningen, at fiktive navne på firmaer, produkter, personer, karakterer og/eller data, der nævnes i, på eller via tjenesterne, repræsenterer eksisterende personer, firmaer, produkter eller begivenheder.

Intet i nærværende aftale overdrager rettigheder eller goder til tredjeparter, dog betragtes Tripadvisors søsterselskaber som værende begunstigede tredjeparter i henhold til aftalen.

Du har ikke ret til at overføre dine rettigheder eller forpligtelser i henhold til nærværende aftale til andre uden vores tilladelse.

Rettigheder, der ikke udtrykkeligt er givet heri, forbeholdes.

**KUNDESERVICE/HJÆLP**

Du kan få svar på dine spørgsmål eller kontakte os ved at besøge vores [supportafdeling](https://www.tripadvisorsupport.com/da-DK/hc/traveler) eller sende en e-mail til os på [help@tripadvisor.com](mailto:help@tripadvisor.com). Eller du kan skrive til os på:

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, USA

Bemærk, at Tripadvisor LLC udelukkende accepterer retslig korrespondance eller retslige forkyndelser af stævninger via post stilet til adressen ovenfor, medmindre andet er nødvendigt i henhold til gældende lovgivning.  For at undgå enhver tvivl og uden begrænsning modtager vi derfor ikke bekendtgørelser eller stævninger, der er sendt til et af vores søster- eller datterselskaber, bortset fra som beskrevet nedenfor.

**KONTAKT FOR BRUGERE ANGÅENDE FORORDNINGEN OM DIGITALE TJENESTER**

Du kan kontakte os direkte ved hjælp af kontaktoplysningerne nedenfor:

E-mailadresse: [help@tripadvisor.com](mailto:help@tripadvisor.com)

**KONTAKT OG JURIDISK REPRÆSENTANT FOR MYNDIGHEDER ANGÅENDE FORORDNINGEN OM DIGITALE TJENESTER**

Hvis du udgør en national myndighed i en medlemsstat, Europa-Kommissionen eller Det Europæiske Råd for Digitale Tjenester, kan du kontakte os direkte ved hjælp af kontaktoplysningerne nedenfor:

E-mailadresse: [poc-dsa@tripadvisor.com](mailto:poc-dsa@tripadvisor.com)

Pålidelige indberettere og fagorganisationer kan også kontakte os via denne e-mailadresse.

Offentlige myndigheder kan desuden kontakte Tripadvisor Ireland Limited, som er vores juridiske repræsentant i henhold til Digital Services Act, ved hjælp af nedenstående oplysninger:

Adresse: 70 Sir John Rogerson’s Quay, Dublin 2, Irland

E-mailadresse: [dsa.notifications@tripadvisor.com](mailto:dsa.notifications@tripadvisor.com)

Telefonnummer: (+353) 15126124

Undlad at kontakte vores juridiske repræsentant ved hjælp af disse oplysninger, medmindre du kontakter os på vegne af en af de offentlige instanser, der er nævnt ovenfor, da Tripadvisor ikke besvarer din anmodning. Hvis du har spørgsmål om platformen eller ønsker at indsende en klage, skal du kontakte os ved hjælp af kontaktoplysningerne, der er angivet i afsnittene "KUNDESERVICE/HJÆLP" eller "KONTAKT FOR BRUGERE ANGÅENDE FORORDNINGEN OM DIGITALE TJENESTER" ovenfor.

©2024 Tripadvisor LLC. Alle rettigheder forbeholdes.

Senest opdateret 16. februar 2024

* [](#print "print")
* [](https://twitter.com/share?url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34356%26item%3D32219 "Twitter Share")
* [](https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34356%26item%3D32219 "Facebook Share")
* [](https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34356%26item%3D32219 "Linkedin Share")
* [](#email "email")