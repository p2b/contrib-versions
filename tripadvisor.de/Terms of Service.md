Allgemeine Nutzungsbedingungen von Tripadvisor
==============================================

Tripadvisor-Nutzungsbedingungen

Zuletzt aktualisiert am: **16\. Februar 2024**

**Nutzungsbedingungen**

[NUTZUNG DER SERVICES](#_USE_OF_THE)

[ZUSÄTZLICHE PRODUKTE](#_ADDITIONAL_PRODUCTS)

[UNZULÄSSIGE AKTIVITÄTEN](#_PROHIBITED_ACTIVITIES)

[DATENSCHUTZERKLÄRUNG UND OFFENLEGUNGEN](#_PRIVACY_POLICY_AND)

[BEWERTUNGEN, KOMMENTARE UND VERWENDUNG ANDERER INTERAKTIVER BEREICHE; LIZENZGEWÄHRUNG](#_REVIEWS,_COMMENTS_AND)

* [Einschränkung der Lizenzrechte von Tripadvisor](#_Restricting_Tripadvisor%E2%80%99s_Licence)

[BUCHUNG BEI DRITTANBIETERN ÜBER Tripadvisor](#_BOOKING_WITH_THIRD-PARTY)

* [Nutzung der Buchungsdienste von Tripadvisor](#_Use_of_Tripadvisor)
* [Drittanbieter](#_Third-Party_Suppliers._The)
* [**Buchen von Ferienwohnungen, Restaurantreservierungen und Erfahrungen mit Drittanbietern, die auf Partner-Websites aufgeführt sind.**](#_Booking_Holiday_Rentals,)

[REISEZIELE](#_TRAVEL_DESTINATIONS)

* [Internationale Reisen](#_International_Travel._When)

[HAFTUNGSAUSSCHLUSS](#_LIABILITY_DISCLAIMER_1)

[HAFTUNGSFREISTELLUNG](#_Indemnification)

[LINKS AUF WEBSITES VON DRITTANBIETERN](#_LINKS_TO_THIRD-PARTY)

[SOFTWARE ALS TEIL DER DIENSTLEISTUNG; ZUSÄTZLICHE MOBILE LIZENZEN](#_SOFTWARE_AS_PART)

[HINWEISE ZU URHEBERRECHT UND MARKEN](#_COPYRIGHT_AND_TRADEMARK)

* [Richtlinie für das Benachrichtigungsverfahren zur Löschung illegaler Inhalte](#_Notice_and_Take-Down)

[ÄNDERUNGEN AN DIESEM VERTRAG, DEN SERVICES, UPDATES, KÜNDIGUNG UND RÜCKTRITT](#_MODIFICATIONS_TO_THE)

[GERICHTSBARKEIT UND ANWENDBARES RECHT](#_JURISDICTION_AND_GOVERNING)

[WÄHRUNGSRECHNER](#_CURRENCY_CONVERTER)

[ALLGEMEINE BESTIMMUNGEN](#_GENERAL_PROVISIONS)

[SERVICEHILFE](#_SERVICE_HELP)

Willkommen auf der Website und den mobilen Eigenschaften von Tripadvisor unter www.Tripadvisor.de und den jeweiligen länderspezifischen Domänen oberster Stufe (einschließlich der damit verbundenen Sub-Domänen), Softwareanwendungen (manchmal als „Apps“ bezeichnet), Daten-, SMS-, APIs-, E-Mail-, Chat- und Telefonkorrespondenz, Schaltflächen, Widgets und Werbeanzeigen (kollektiv wird sich in diesem Dokument auf all diese Elemente als die „Services“ bezogen; ganz allgemein wird sich auf die Websites und mobilen Eigenschaften von Tripadvisor in diesem Dokument nachstehend als „Websites“ bezogen).

Die Dienste werden von Tripadvisor LLC angeboten, einer Gesellschaft mit beschränkter Haftung in Delaware mit Sitz in den USA (" **Tripadvisor**") mit Geschäftssitz in Tripadvisor LLC, 400 1st Avenue, Needham, MA 02494, USA. Die Begriffe "wir", "uns" und "unser" sind entsprechend auszulegen.

Durch die Erstellung eines Tripadvisor-Kontos stimmen Sie den unten aufgeführten Geschäftsbedingungen und Hinweisen (zusammen als "Vertrag" bezeichnet) zu. Dieser Vertrag gilt nicht, wenn Sie lediglich die Websites durchsuchen oder die Services nutzen, ohne ein Tripadvisor-Konto zu erstellen.  Lesen Sie diese Vereinbarung bitte sorgfältig durch, denn diese enthält Informationen zu Ihren Rechten und Einschränkungen dieser Rechte sowie einen Abschnitt über das anwendbare Recht und den Gerichtsstand bei Streitigkeiten. Wenn Sie ein Verbraucher in der EU oder im Vereinigten Königreich sind, haben Sie das gesetzliche Recht, diesen Vertrag innerhalb von 14 Tagen nach Abschluss des Vertrags zu kündigen. Sie können dies tun oder diesen Vertrag zu einem anderen Zeitpunkt kündigen, indem Sie Ihr Konto schließen (indem Sie uns kontaktieren oder indem Sie im eingeloggten Zustand auf "Kontoinformationen" zugreifen und die Option "Konto schließen" auswählen) und nicht mehr auf die Services zugreifen oder diese nutzen.

Wenn Sie ein Verbraucher in der EU oder im Vereinigten Königreich sind, haben Sie bestimmte obligatorische Verbraucherrechte. Wenn Sie als Verbraucher in einem anderen Land leben, haben Sie möglicherweise auch Verbraucherrechte gemäß den Gesetzen Ihres Landes. Nichts in diesem Vertrag berührt Ihre zwingenden Verbraucherrechte.

Auf sämtliche Informationen, Texte, Links, Grafiken, Fotos, Tonaufnahmen, Videos, Daten, Codes oder andere Materialien und Materialarrangements, die Sie durch die Services sehen, zugreifen oder anderweitig mit ihnen interagieren können, wird sich mit „Inhalte“ bezogen. „Services”, wie oben definiert, bezieht auf die von Tripadvisor oder unseren Partnerunternehmen erbrachten Services (Tripadvisor und solche Unternehmen werden, wenn sich auf eines oder mehrere bezogen wird, zusammen als „Tripadvisor-Unternehmen” bezeichnet). Um Zweifel auszuschließen, befinden sich alle Websites im Besitz und unter der Kontrolle von Tripadvisor.  Allerdings können sich bestimmte spezifische Services, die über die Websites bereitgestellt werden, im Besitz und unter der Kontrolle von Partnerunternehmen von Tripadvisor befinden, zum Beispiel Services zur [**Buchung von Ferienwohnungen, Reservierungen in Restaurants und von Erlebnissen bei Drittanbietern**](https://tripadvisor.mediaroom.com/uk-terms-of-use#OLE_LINK11) (siehe unten). Im Rahmen unserer Dienste senden wir Ihnen möglicherweise Benachrichtigungen zu Aktionsangeboten, Produkten oder zusätzlichen Diensten von uns, unseren verbundenen Unternehmen oder unseren Partnern, die für Sie interessant sein könnten. Solche Benachrichtigungen werden normalerweise in Form von Newslettern und Marketing-Mitteilungen versendet. Wir möchten auf diesem Wege mehr über Sie und Ihre Präferenzen in Bezug auf unsere Dienste und die Dienste unserer verbundenen Unternehmen erfahren. Wir können unsere Dienste dadurch besser auf Ihre Präferenzen abstimmen.

Der Begriff "Sie" oder "Nutzer" bezieht sich auf die natürliche Person, das Unternehmen, die Geschäftsorganisation oder eine andere juristische Person, die ein Tripadvisor-Konto erstellt hat und die Services nutzt und/oder Inhalte zu ihnen beisteuert. Die Inhalte, die Sie beitragen, einreichen, übertragen und/oder auf oder über die Services einstellen, werden wechselnd als „Ihre Inhalte“, „Inhalte von Ihnen“ und/oder „von Ihnen eingestellte Inhalte“ bezeichnet.

Die Services werden kontinuierlich bereitgestellt und dienen ausschließlich folgenden Zwecken:

1. Kunden die Möglichkeit zu geben, Reiseinformationen zu sammeln, Inhalte einzustellen und nach Reisen zu suchen und Buchungen von Reisereservierungen vorzunehmen; und
2. Unterstützen Sie Unternehmen aus den Bereichen Reisen, Tourismus und Gastgewerbe dabei, mit Benutzern und potenziellen Kunden in Kontakt zu treten, indem Sie kostenlose und/oder kostenpflichtige Services anbieten, die von oder über die Tripadvisor-Unternehmen angeboten werden.

#### Produkte, Services, Informationen und Inhalte unserer Dienste ändern sich regelmäßig, sodass sie aktuell sind (z.B. mit aktuellen Informationen und Angeboten). Das bedeutet beispielsweise, dass bei Drittanbieterdiensten neue Transportdienste, Unterkünfte, Restaurants, Touren, Aktivitäten oder Erlebnisse für Sie verfügbar werden können, während andere Dienste möglicherweise nicht mehr verfügbar sind.

Wir können diese Vereinbarung zukünftig in Übereinstimmung mit den hier angegebenen Nutzungsbedingungen ändern oder auf sonstige Weise anpassen, und Sie erkennen an und stimmen zu, dass Sie durch den fortgesetzten Zugriff auf die Services oder deren fortgesetzte Nutzung nach einer solchen Änderung die aktualisierte oder geänderte Vereinbarung annehmen. Wir geben unten auf dieser Vereinbarung das Datum an, an dem sie zuletzt überarbeitet wurde, und alle Überarbeitungen treten mit ihrer Einstellung in Kraft. Wir benachrichtigen Sie über wesentliche Änderungen an diesen Nutzungsbedingungen, indem wir entweder einen Hinweis an die mit Ihrem Profil verbundene E-Mail-Adresse senden oder einen Hinweis auf unseren Websites platzieren. Rufen Sie diese Seite bitte in regelmäßigen Abständen auf, um die aktuelle Version dieser Vereinbarung zu überprüfen. Wenn Sie keine Änderungen an dieser Vereinbarung akzeptieren möchten, können Sie Ihr Konto schließen und den Zugriff auf die Services sperren. (Eine Beschreibung dazu finden Sie [hier](https://www.tripadvisorsupport.com/de-DE/hc/traveler/articles/510).)

**NUTZUNG DER SERVICES**

Als Bedingung für Ihre Nutzung der Services erklären Sie sich damit einverstanden, dass (i) alle von Ihnen über die Services an die Tripadvisor-Unternehmen bereitgestellten Informationen wahr, korrekt, aktuell und vollständig sind, (ii) Sie Ihre Kontoinformationen schützen und die Nutzung Ihres Kontos durch andere Personen als Sie überwachen und vollständig verantwortlich sind, (iii) Sie 13 Jahre oder älter sind. Die Tripadvisor-Unternehmen erfassen nicht wissentlich die Informationen von Personen, die jünger als 13 Jahre sind. Wenn Sie ein Verbraucher mit Wohnsitz in der EU oder im Vereinigten Königreich sind, können wir Ihnen den Zugang zu den Diensten verweigern, wenn Sie gegen diesen Vertrag verstoßen oder aus einem anderen vernünftigen Grund. Wenn Sie nicht in der EU oder im Vereinigten Königreich ansässig sind, behalten wir uns das Recht vor, Ihnen den Zugang zu den Diensten jederzeit und aus beliebigem Grund, einschließlich, aber nicht beschränkt auf einen Verstoß gegen diesen Vertrag, nach eigenem Ermessen zu verweigern. Wenn Sie die Services nutzen, einschließlich Produkten und Dienstleistungen für das Teilen von Inhalten von oder auf Websites von Dritten, erklären Sie sich damit einverstanden, dass Sie die ausschließliche Verantwortung für Informationen tragen, die Sie an die Tripadvisor-Unternehmen weitergeben. Sie dürfen auf die Services nur wie vorgesehen und durch diesen Vertrag gestattet über die bereitgestellten Funktionen der Services zugreifen.

Das Kopieren oder die Übertragung, Reproduktion, Replikation, Veröffentlichung oder Weiterverteilung von (a) Inhalten oder jedweden Teilen davon und/oder (b) den Services im Allgemeinen ist ohne die vorherige schriftliche Genehmigung der Tripadvisor-Unternehmen strengstens untersagt. Um die Genehmigung zu beantragen, richten Sie Ihre Anfrage bitte an:

Director, Partnerships and Business Development

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, USA

Um auf bestimmte Funktionen der Services zugreifen zu können, müssen Sie ein Konto erstellen. Bei der Kontoerstellung müssen Sie vollständige und wahrheitsgemäße Angaben machen. Sie tragen die alleinige Verantwortung für die Aktivitäten, die in Ihrem Konto stattfinden, einschließlich Ihrer Interaktion und Kommunikation mit anderen, und Sie müssen Ihr Konto schützen. Zu diesem Zweck erklären Sie sich damit einverstanden, Ihre Kontaktinformationen auf dem neuesten Stand zu halten. 

Wenn Sie ein Tripadvisor-Konto für gewerbliche Zwecke erstellen und diese Vereinbarung im Namen eines Unternehmens, einer Organisation oder einer anderen Rechtseinheit akzeptieren, erklären und garantieren Sie, dass Sie dazu berechtigt sind und die Befugnis haben, diese Einheit an diese Vereinbarung zu binden. In diese Falle beziehen sich die Wörter „Sie“ und „Ihr“ in dieser Vereinbarung auf die Einheit und die Einzelperson, die im Namen des Unternehmens handelt, wird als „Unternehmensvertreter“ bezeichnet.

Durch die Nutzung unserer Services treffen Sie möglicherweise auf Links zu den Sites oder Apps Dritter oder können mit den Sites oder Apps Dritter interagieren. Dies kann die Möglichkeit umfassen, Inhalte von den Services, einschließlich Ihrer Inhalte, mit diesen Sites und Apps Dritter zu teilen. Bitte beachten Sie, dass die Sites und Apps Dritter diese geteilten Inhalte öffentlich anzeigen können. Diese Drittparteien erheben möglicherweise eine Gebühr für die Nutzung bestimmter Inhalte oder Services, die auf oder über ihre Website bereitgestellt werden. Bevor Sie Transaktionen mit Drittparteien abschließen, sollten Sie deshalb alle Nachforschungen anstellen, die Sie für erforderlich oder geeignet halten, um festzustellen, ob Gebühren anfallen. Wenn die Tripadvisor-Unternehmen Angaben zu den Preisen und Gebühren der Inhalte oder Services von Drittparteien machen, erfolgt dies ausschließlich zu Informationszwecken. Sämtliche Interaktionen mit den Sites und Apps von Drittanbietern erfolgen auf eigene Gefahr. Sie erkennen ausdrücklich an und stimmen zu, dass die Tripadvisor-Unternehmen in keiner Weise für die Sites oder Apps von Drittanbietern haften.

Tripadvisor-Unternehmen bewerten und präsentieren Einträge anhand von Empfehlungssystemen. Verschiedene Kategorien von Einträgen (z. B. Hotels und Restaurants, Aktivitäten und Flüge) werden basierend auf verschiedenen Kriterien wie Preis, "Favoriten der Reisenden" und Dauer eingestuft. Alle Informationen zu unseren Richtlinien und den Kriterien für das Ranking finden Sie auf der Webseite "[So funktioniert die Website](https://www.tripadvisor.de/HowTheSiteWorks.html)".

Manche Inhalte, die Sie sehen oder auf die Sie anderweitig auf den oder über die Services Zugang erlangen, werden für Werbezwecke genutzt. Sie willigen ein und verstehen, dass die Tripadvisor-Unternehmen Werbeanzeigen und Angebote neben, nahe, an oder anderweitig in der Nähe Ihrer Inhalte (einschließlich, im Falle von Videos oder anderen dynamischen Inhalten, vor, während oder nach ihrer Darstellung) sowie den Inhalten anderer platzieren können.

Zusätzliche Produkte

Die Tripadvisor-Unternehmen können von Zeit zu Zeit beschließen, bestimmte Produkte oder Funktionen der Services zu ändern, zu aktualisieren oder einzustellen. Sie willigen ein und verstehen, dass die Tripadvisor-Unternehmen nicht verpflichtet sind, Ihre Inhalte oder andere von Ihnen bereitgestellten Informationen zu speichern oder zu verwahren, außer in dem gesetzlich vorgeschriebenen Umfang.

Wir bieten auch andere Dienstleistungen an, für die möglicherweise zusätzliche Nutzungsbedingungen gelten. Wenn Sie irgendwelche dieser Dienstleistungen in Anspruch nehmen, werden Ihnen die zusätzlichen Bedingungen zur Verfügung gestellt und werden Teil dieser Vereinbarung, außer in Fällen, in denen die zusätzlichen Bedingungen diese Vereinbarung ausdrücklich ausschließen oder anderweitig ersetzen. Wenn Sie zum Beispiel zusätzliche Services für gewerbliche oder geschäftliche Zwecke nutzen oder erwerben, müssen Sie den entsprechenden zusätzlichen Bedingungen zustimmen. Insoweit, wie irgendwelche anderen Bedingungen mit den Nutzungsbedingungen dieser Vereinbarung in Konflikt stehen, gelten die zusätzlichen Bedingungen bis zum Punkt des Konflikts in Bezug auf die spezifischen Dienstleistungen.

**UNZULÄSSIGE AKTIVITÄTEN**

Die auf den oder über die Services verfügbaren Inhalte und Informationen (unter anderem Meldungen, Daten, Informationen, Text, Musik, Sound, Fotos, Grafiken, Videos, Karten, Symbole, Software, Code oder anderes Material) sowie die für die Bereitstellung dieser Inhalte und Informationen verwendete Infrastruktur stehen im Eigentum der Tripadvisor-Unternehmen oder wurden den Tripadvisor-Unternehmen von Dritten überlassen. Für alle Inhalte außer Ihren eigenen Inhalten stimmen Sie zu, dass Sie keine von den oder über die Services erhaltenen Informationen, Softwareprogramme, Produkte oder Services ändern, kopieren, verteilen, übermitteln, anzeigen, vorführen, vervielfältigen, veröffentlichen, lizenzieren, übertragen, verkaufen, weiterverkaufen oder hiervon Bearbeitungen erstellen. Weiterhin stimmen Sie zu, Folgendes zu unterlassen:

*  (i) Verwendung der Services oder Inhalte für irgendwelche gewerblichen Zwecke außerhalb des Umfangs jener gewerblichen Zwecke, die ausdrücklich unter dieser Vereinbarung und den damit verbundenen Richtlinien, die von den Tripadvisor-Unternehmen bereitgestellt werden, gestattet sind;
* (ii) Aufruf, Überwachung, Reproduktion, Verteilung, Übertragung, Anzeige, Verkauf, Lizenzierung, Kopieren oder anderweitige Verwertung von Inhalten der Services, einschließlich, aber nicht beschränkt auf Benutzerprofile und Fotos, mithilfe von Robotern, Spidern, Scrapern oder anderen automatisierten Mitteln oder manuellen Prozessen zu beliebigen Zwecken, die nicht mit dieser Vereinbarung übereinstimmen, ohne unsere ausdrückliche schriftliche Einwilligung;
* (iii) Verstoß gegen Einschränkungen zum Verbot von Robotern bei den Services oder Umgehung anderer Maßnahmen, durch die der Zugriff auf die Services verhindert oder eingeschränkt werden soll;
* (iv) alle Maßnahmen zu ergreifen, die unsere Infrastruktur nach alleinigem Ermessen (oder, wenn Sie in Großbritannien oder in der EU ansässig sind, nach billigem Ermessen) mit einer unangemessenen oder unverhältnismäßig großen Belastung belasten;
* (v) Erstellen von Deep-Links auf irgendeinen Teil der Services zu irgendeinem Zweck ohne unsere ausdrückliche schriftliche Einwilligung;
* (vi) Erstellen von „Frames“ oder Spiegeln oder sonstige Einbettung von Teilen der Services in eine andere Website oder einen anderen Service ohne unsere vorherige schriftliche Einwilligung;
* (vii) Versuch, Softwareprogramme, die von den Tripadvisor-Unternehmen in Verbindung mit den Services verwendet werden, zu ändern, zu übersetzen, anzupassen, zu bearbeiten, zu dekompilieren, zu disassemblieren oder zurückzuentwickeln;
* (viii) Umgehen, Deaktivieren oder anderweitiges Stören von sicherheitsbezogenen Funktionen der Services oder von Funktionen, die die Nutzung oder das Kopieren von irgendwelchen Inhalten verhindern; oder
* (ix) Herunterladen jeglicher Inhalte, es sei denn, diese werden ausdrücklich für das Herunterladen von den Tripadvisor-Unternehmen bereitgestellt.
* (x) Roboter, Spinnen, Systeme künstlicher Intelligenz (KI) oder andere automatisierte Geräte, Prozesse oder Mittel zum Zugriff, Abrufen, Kopieren, Kratzen, Aggregieren, Sammeln, Herunterladen oder zur sonstigen Indexierung von Teilen der Services oder Inhalte zu verwenden, zu unterstützen, zu fördern oder Dritten die Nutzung zu ermöglichen, es sei denn, dies wird von Tripadvisor ausdrücklich in schriftlicher Form gestattet.

**DATENSCHUTZERKLÄRUNG UND OFFENLEGUNGEN**

Alle von Ihnen in Verbindung mit den Services eingestellten oder anderweitig bereitgestellten personenbezogenen Daten werden in Übereinstimmung mit unserer Datenschutzerklärung verwendet. Klicken Sie [hier](https://tripadvisor.mediaroom.com/de-privacy-policy), um unsere Datenschutzerklärung und Cookie-Richtlinie anzuzeigen.

**BEWERTUNGEN, KOMMENTARE UND VERWENDUNG ANDERER INTERAKTIVER BEREICHE; LIZENZGEWÄHRUNG**

Wir hören gerne von Ihnen. Bitte beachten Sie, dass Sie durch die Bereitstellung Ihrer Inhalte an oder über die Services, sei es per E-Mail, Einstellung über irgendeines der Tripadvisor-Synchronisierungsprodukte, über die Services und Anwendungen anderer oder auf andere Art und Weise, einschließlich irgendwelche Ihrer Inhalte, die über ein Produkt oder einen Dienst der Tripadvisor-Unternehmen an Ihr Tripadvisor-Konto übertragen wurden, Bewertungen, Fragen, Fotos und Videos, Kommentare, Empfehlungen, Ideen oder ähnliches aus Ihren Inhalten, den Tripadvisor-Unternehmen das nicht-exklusive, gebührenfreie, unbefristete, übertragbare, unwiderrufliche und voll unterlizenzierbare Recht gewähren, (a) Ihre Inhalte weltweit in allen derzeit und künftig bekannten Medien aufzunehmen, zu nutzen, zu reproduzieren, zu ändern, auszuführen, anzupassen, zu übersetzen, zu verbreiten, zu veröffentlichen, abgeleitete Werke daraus zu erstellen und öffentlich zu verbreiten und darzustellen; (b) Ihre Inhalte dem Rest der Welt zugänglich zu machen und andere das Gleiche tun zu lassen; (c) die Services bereitzustellen, zu bewerben und zu verbessern und die von Ihnen über die Services geteilten Inhalte anderen Unternehmen, Organisationen und Personen zu ihrer Syndikation, Ausstrahlung, Verbreitung, Promotion oder Veröffentlichung in anderen Medien oder Diensten in Übereinstimmung mit unserer Datenschutzrichtlinie und dieser Vereinbarung zur Verfügung zu stellen; und (d) den von Ihnen in Verbindung mit diesen Inhalten eingestellten Namen/Handelsmarke zu verwenden. Sie erkennen an, dass Tripadvisor die Zuweisung Ihrer Inhalte nach eigenem Ermessen (oder, wenn Sie ein Verbraucher mit Wohnsitz im Vereinigten Königreich oder in der EU sind, nach unserem angemessenen Ermessen) zur Verfügung stellen kann. Sie erteilen den Tripadvisor-Unternehmen weiterhin das Recht, gegen Personen oder Unternehmen zu klagen, die Ihre Rechte oder die Rechte der Tripadvisor-Unternehmen an Ihren Inhalten durch einen Verstoß gegen diese Vereinbarung verletzen. Sie erkennen an und stimmen zu, dass Ihre Inhalte nicht vertraulich und nicht geschützt sind. Sie bestätigen, erklären und garantieren, dass Sie Eigentümer Ihrer Inhalte sind oder die notwendigen Lizenzen, Rechte (einschließlich des Urheberrechts und anderer Eigentumsrechte), Einwilligungen und Genehmigungen besitzen, um Ihre Inhalte zu veröffentlichen oder anderweitig zu nutzen (und dass die Tripadvisor-Unternehmen sie veröffentlichen und anderweitig nutzen können), wie in dieser Vereinbarung genehmigt.

Wird festgestellt, dass Sie geistige Urheberrechte (einschließlich des Rechts auf Nutzung unter Angabe Ihres Namens oder auf Unverletzlichkeit des Werks) an Ihren Inhalten haben, erklären Sie hiermit, dass Sie, soweit gesetzlich zulässig, (a) nicht verlangen, dass in Verbindung mit den Inhalten oder daraus abgeleiteten Werken oder verbesserten oder aktualisierten Fassungen davon Ihre Person identifizierende Informationen veröffentlicht werden; (b) keine Einwände gegen die Veröffentlichung, Nutzung, Änderung, Löschung und Verwendung Ihres Inhalts durch die Tripadvisor-Unternehmen, ihre Lizenznehmer, Nachfolger oder Abtretungsempfänger haben; (c) für alle Zeiten auf etwaige Ansprüche und jegliche einem Autor zustehenden geistigen Urheberrechte an Ihren Inhalten verzichten und keine solchen geltend machen werden; und (d) die Tripadvisor-Unternehmen, ihre Lizenznehmer, Nachfolger oder Abtretungsempfänger für alle Zeiten von allen Forderungen frei halten, die Sie ansonsten kraft dieser geistigen Urheberrechte gegen die Tripadvisor-Unternehmen geltend machen könnten.

Bitte beachten Sie, dass das Feedback und andere Empfehlungen, die Sie abgeben, jederzeit verwendet werden können und wir nicht verpflichtet sind, diese vertraulich zu behandeln.

Die Services enthalten möglicherweise Diskussionsforen, Bulletin Boards, Bewertungsservices, Reise-Feeds oder andere Foren, bei denen Sie Ihre Inhalte, wie Bewertungen über Reiseerfahrungen, Nachrichten, Materialien oder andere Elemente, einstellen können („interaktive Bereiche“). Falls Tripadvisor solche interaktiven Bereiche auf den Websites bereitstellt, sind Sie alleine für Ihre Nutzung dieser interaktiven Bereiche verantwortlich und nutzen diese auf eigene Gefahr. Die Tripadvisor-Unternehmen garantieren keine Vertraulichkeit in Bezug auf Ihre Inhalte, die Sie den Services oder in irgendeinem interaktiven Bereich bereitstellen.  In dem Umfang, in dem eine Einrichtung, die zu den Tripadvisor-Unternehmen gehört, eine Form des privaten Kommunikationskanals zwischen den Benutzern bereitstellt, stimmen Sie zu, dass diese Einrichtung(en) den Inhalt dieser Mitteilungen überwachen darf/dürfen, um zur Sicherung unserer Community und der Dienste beizutragen. Sie erkennen an, dass die Tripadvisor-Unternehmen keine Benutzernachrichten bearbeiten oder kontrollieren, die über die Services, unter anderem in Chatrooms oder auf Bulletin Boards oder anderen Kommunikationsforen, eingestellt oder verbreitet werden, und sie in keiner Weise für diese Benutzernachrichten haften oder Verantwortung tragen.  Insbesondere bearbeitet oder kontrolliert Tripadvisor nicht die Inhalte der Benutzer, die auf den Websites erscheinen.  Die Tripadvisor-Unternehmen behalten sich allerdings das Recht vor, ohne Vorankündigung sämtliche Nachrichten oder andere Inhalte von den Services zu entfernen, wenn sie in gutem Glauben davon ausgehen, dass diese gegen diese Vereinbarung verstoßen, oder wenn sie anderweitig der Ansicht sind, dass eine Entfernung vernünftigerweise erforderlich ist, um die Rechte der Tripadvisor-Unternehmen und/oder anderer Nutzer der Services zu schützen. Wenn Sie mit der Entfernung Ihrer Inhalte von den Websites nicht einverstanden sind, können Sie über das interne Beschwerdeverfahren von Tripadvisor gegen unsere Entscheidung Widerspruch einlegen. Weitere Informationen dazu, wie Sie Widerspruch einlegen und Widerspruch gegen eine Entscheidung einlegen können, Ihren Inhalt zu entfernen oder einzuschränken, finden Sie [hier](https://www.tripadvisorsupport.com/de-DE/hc/traveler/articles/623). Durch die Nutzung der Interaktiven Bereiche willigen Sie ausdrücklich ein, nur solche Inhalte von Ihnen einzureichen, die mit den veröffentlichten Richtlinien von Tripadvisor im Einklang steht, die zum Zeitpunkt der Einreichung gelten und Ihnen von Tripadvisor zur Verfügung gestellt werden.  Sie stimmen ausdrücklich zu, keine der folgenden Inhalte von Ihnen auf die Services hochzuladen oder zu übertragen oder über diese einzustellen, zu verbreiten, zu speichern, zu erstellen oder auf sonstige Weise zu veröffentlichen:

1. Inhalte, die falsch, unrechtmäßig, irreführend, verleumderisch, diffamierend, obszön, pornografisch, anstößig, unzüchtig oder anzüglich sind, diskriminierend sind (oder die Diskriminierung einer anderen Person befürworten), Drohungen enthalten, das Recht auf Privatsphäre oder das Persönlichkeitsrecht verletzen oder beleidigend, aufhetzerisch, betrügerisch oder auf sonstige Weise zu beanstanden sind;
2. Inhalte, die für die Online-Community offenkundig beleidigend sind, wie Inhalte, die Rassismus, Fanatismus, Hass oder physische Gewalt gegen irgendeine Gruppe oder Person fördern;
3. Sie würden Anweisungen für die Durchführung einer illegalen Aktivität, einer Straftat, einer zivilrechtlichen Haftung, einer Verletzung der Rechte einer Partei in einem beliebigen Land der Welt oder einer anderweitigen Haftung oder eines lokalen, nationalen oder internationalen Rechts, einschließlich, aber nicht beschränkt auf die Vorschriften der USA, erstellen, fördern oder bereitstellen. Securities and Exchange Commission (SEC) oder Regeln jeder Wertpapierbörse, einschließlich, aber nicht beschränkt auf die New York Stock Exchange (NYSE), die NASDAQ oder die London Stock Exchange;
4. Inhalte, die Anleitungen zu gesetzwidrigen Aktivitäten wie der Herstellung oder dem Kauf verbotener Waffen, der Verletzung der Privatsphäre anderer oder der Bereitstellung oder Erstellung von Computerviren bereitstellen;
5. Inhalte, die möglicherweise gegen Patent-, Marken-, Urheber- oder Eigentumsrechte oder Rechte an Geschäftsgeheimnissen oder anderem geistigem Eigentum irgendeiner Partei verstoßen. Insbesondere Inhalte, die das gesetzwidrige oder unberechtigte Kopieren urheberrechtlich geschützter Werke anderer Personen fördern, wie die Bereitstellung von raubkopierten Computerprogrammen oder von Links auf diese, die Bereitstellung von Informationen zur Umgehung von vom Hersteller installierten Kopierschutzvorrichtungen oder die Bereitstellung von raubkopierter Musik oder von Links auf raubkopierte Musikdateien;
6. Inhalte, die Massenversendungen oder „Spam“, unerwünschte E-Mails, Kettenbriefe oder Pyramidensysteme darstellen;
7. Inhalte, durch die Sie sich als eine andere Person oder ein anderes Unternehmen ausgeben oder durch die Sie Ihre Zugehörigkeit zu einer anderen Person oder einem anderen Unternehmen, unter anderem den Tripadvisor-Unternehmen, falsch darstellen;
8. private Informationen von Dritten, unter anderem Adressen, Telefonnummern, E-Mail-Adressen, Sozialversicherungsnummern und Kreditkartennummern.   beachten Sie, dass der Nachname (Familienname) einer Person auf der Website veröffentlicht werden kann, aber nur, wenn zuvor die ausdrückliche Zustimmung der identifizierten Person eingeholt wurde;
9. Inhalte mit Seiten, die nur eingeschränkt oder nur mit Passwort zugänglich sind, oder versteckte Seiten oder Bilder (die nicht mit oder von einer anderen frei zugänglichen Seite aus verlinkt sind);
10. Viren, beschädigte Daten oder andere schädliche, störende oder zerstörende Dateien;
11. Inhalte, die nicht im Zusammenhang mit dem Thema der interaktiven Bereiche stehen, in denen die Inhalte eingestellt wurden; oder
12. Inhalte, die im alleinigen Ermessen von Tripadvisor, (a) gegen die Vorgaben gemäß den vorigen Absätzen verstoßen, (b) gegen die genannten Richtlinien von Tripadvisor verstoßen, die Ihnen von Tripadvisor zur Verfügung gestellt wurden, (c) anstößig sind, (d) andere daran hindern, die interaktiven Bereiche oder andere Aspekte der Services zu nutzen oder ungestört zu nutzen, oder (e) irgendeines der Tripadvisor-Unternehmen oder deren Benutzer Schäden oder Haftungen jeglicher Art aussetzen könnte.

Soweit gesetzlich zulässig, übernehmen die Tripadvisor-Unternehmen keine Verantwortung und Haftung für Inhalte, die von Ihnen (im Falle Ihrer Inhalte) oder Dritten (im Falle aller Inhalte allgemein) veröffentlicht, gespeichert, übertragen oder auf die Dienste hochgeladen werden, oder für Verluste oder Schäden daran. Die Tripadvisor-Unternehmen sind auch nicht für Fehler, Verleumdungen, Verleumdungen, Verleumdungen, Auslassungen, Unwahrheiten, Obszönitäten, Pornografie oder Respektlosigkeiten haftbar, auf die Sie möglicherweise stoßen. Als Anbieter interaktiver Dienste und soweit gesetzlich zulässig, haftet Tripadvisor nicht für Aussagen, Zusicherungen oder andere Inhalte, die von seinen Benutzern (einschließlich Ihnen in Bezug auf Ihre Inhalte) auf den Websites oder in einem anderen Forum bereitgestellt werden. Obwohl Tripadvisor nicht verpflichtet ist, die in einem interaktiven Bereich veröffentlichten oder dort verbreiteten Inhalte zu überprüfen, zu bearbeiten oder zu überwachen, behält sich Tripadvisor das Recht vor und liegt im absoluten Ermessen (oder, wenn Sie ein Verbraucher mit Wohnsitz im Vereinigten Königreich oder in der EU sind, nach billigem Ermessen), Inhalte, die in den Diensten veröffentlicht oder gespeichert sind, jederzeit und aus beliebigem Grund ohne Vorankündigung zu entfernen, zu überprüfen, zu übersetzen oder zu bearbeiten oder solche Handlungen von Dritten in ihrem Namen durchführen zu lassen. Soweit gesetzlich zulässig, sind Sie allein dafür verantwortlich, Sicherungskopien von Inhalten zu erstellen und diese zu ersetzen, die Sie veröffentlichen oder anderweitig an uns übermitteln oder auf den Diensten speichern, und zwar auf eigene Kosten. Alle Informationen zu unseren Richtlinien zur Moderation von Inhalten, einschließlich der Einschränkungen, die wir Ihren Inhalten auferlegen können, finden Sie [hier](https://www.tripadvisor.de/Trust).

**EINSCHRÄNKUNG UND/ODER AUSSETZUNG IHRER NUTZUNG DER SERVICES**

Jede Nutzung der interaktiven Bereiche oder anderer Aspekte der Services unter Verstoß gegen das Vorstehende verstößt gegen die Bedingungen dieser Vereinbarung und kann unter anderem die Kündigung oder Aufhebung Ihrer Rechte zur Nutzung der interaktiven Bereiche und/oder der Services im Allgemeinen nach sich ziehen. Tripadvisor kann auch die Bearbeitung von Mitteilungen und Beschwerden, die über die Services eingereicht werden, aussetzen, wenn Sie häufig offensichtlich unbegründete Mitteilungen oder Beschwerden einreichen.

Unser Ansatz zur Aussetzung Deiner Nutzung der Plattform wird [hier](https://www.tripadvisor.de/Trust) näher erläutert.

**Einschränkung der Lizenzrechte von** **Tripadvisor** Sie können sich zukünftig dazu entscheiden, die Nutzung Ihrer Inhalte durch die Tripadvisor-Unternehmen unter dieser Vereinbarung (wie oben beschrieben) einzuschränken, indem Sie entscheiden, den Tripadvisor-Unternehmen eine beschränktere Lizenz zur Verfügung zu stellen, wie weiter unten beschrieben wird (auf diese beschränkten Lizenzen wird sich nachstehend in diesem Dokument als „Begrenzte Lizenz“ bezogen).  Sie können diese Wahl treffen, in dem Sie die Gewährung der Begrenzten Lizenz [**hier**](https://www.tripadvisor.de/Settings-cs) auswählen (beachten Sie, dass Sie dafür in Ihrem Konto angemeldet sein müssen). Wenn Sie diese Wahl treffen, sind die Rechte, die Sie gemäß der oben aufgeführten Lizenzbedingungen den Tripadvisor-Unternehmen an Ihren Inhalten einräumen (als “Standardlizenz” bezeichnet), auf bedeutende Weise einschränkt, wie in den Abschnitten 1 bis 6 unten beschrieben wird, sodass die Tripadvisor-Unternehmen keine Standardlizenz für irgendwelche Ihrer Inhalte besitzen, außer den textbasierten Bewertungen und den damit verbundenen Punktebewertungen, die Sie veröffentlichen (für die den Tripadvisor-Unternehmen nach wie vor eine Standardlizenz gewährt wird), sondern nur eine „Begrenzte Lizenz“ über den Großteil Ihres Inhalts gewährt bekommen, wie unten definiert:

1. Wenn Sie Ihre Inhalte auf den Services einstellen, ist die Lizenz, die Sie den Tripadvisor-Unternehmen an Ihren Inhalten gewähren, auf eine nicht-exklusive, gebührenfreie, übertragbare, unterlizenzierbare und weltweite Lizenz zur Aufnahme, Nutzung, Verbreitung, Änderung, Ausführung, Reproduktion, öffentlichen Anzeige sowie zur Darstellung, Übersetzung oder Erstellung derivativer Werke Ihrer Inhalte für deren Anzeige auf den Services, sowie zur Nutzung Ihres Namens und/oder Ihrer Handelsmarke in Verbindung mit diesen Inhalten beschränkt. Gemäß Abschnitt 6 unten gilt die Begrenzte Lizenz für all Ihre Inhalte (wie bereits erwähnt, außer textbasierten Bewertungen und damit dazugehörigen Punktebewertungen), die Sie oder jemand anderes in Ihrem Namen (z.B. eine Drittpartei, die zu Ihrem Konto beiträgt oder es verwaltet) auf oder in Verbindung mit den Services zugänglich macht.

1. In Bezug auf jedwedes einzelne Element Ihrer Inhalte, das einer Begrenzten Lizenz unterliegt, können Sie die Lizenzrechte der Tripadvisor-Unternehmen daran beenden, indem Sie den Beitrag von den Services löschen.  Entsprechend können Sie die Lizenzrechte der Tripadvisor-Unternehmen an all Ihren Inhalten, die der Begrenzten Lizenz unterliegen, beenden, indem Sie Ihr Konto kündigen (eine Beschreibung, wie Sie dies tun können, finden Sie [**hier**](https://www.tripadvisorsupport.com/de-DE/hc/traveler/articles/510)). Unbeschadet anders lautender Bestimmungen, (a) verbleiben Ihre Inhalte in dem Umfang auf den Services, in dem Sie sie mit anderen geteilt haben und diese sie kopiert oder gespeichert haben, bevor Sie sie gelöscht oder Ihr Konto gekündigt haben, (b) können Ihre Inhalte weiterhin während eines zumutbaren Zeitraums auf den Services angezeigt werden, nachdem Sie sie gelöscht oder Ihr Konto gekündigt haben, während wir daran arbeiten, Sie zu entfernen, und/oder (c) können Ihre Inhalte aus technischen Gründen, zur Betrugsbekämpfung oder aus gesetzlichen Gründen auf bestimmte Zeit als Sicherheitskopie einbehalten (aber nicht öffentlich angezeigt) werden.

1. Die Tripadvisor-Unternehmen verwenden Ihre Inhalte nicht in Werbeanzeigen für die Produkte und Dienstleistungen von Dritten an andere ohne Ihre separate Zustimmung (einschließlich gesponserte Inhalte), obwohl Sie einwilligen und verstehen, dass die Tripadvisor-Unternehmen Werbeanzeigen und Angebote neben, nahe, an oder anderweitig in der Nähe Ihrer Inhalte (einschließlich, im Falle von Videos oder anderen dynamischen Inhalten, vor, während oder nach ihrer Darstellung) sowie den Inhalten anderer platzieren können.  Jedes Mal, wenn Ihre Inhalte auf den Services angezeigt werden, erfolgt die Nutzung unter Angabe des Namens und/oder der Handelsmarke, die Sie in Verbindung mit Ihren Inhalten angeben.  

1. Die Tripadvisor-Unternehmen geben keinen Drittparteien das Recht, Ihre Inhalte außerhalb der Services zu veröffentlichen. Allerdings führt das Teilen Ihrer Inhalte auf den Services (außer der „Trips“-Funktion, die privat gestellt werden kann) dazu, dass Ihre Inhalte „_öffentlich_" gemacht werden und wir eine Funktion aktivieren, die es anderen Benutzern ermöglicht, Ihre Inhalte (außer, wie gesagt, „Trips“, die Sie als privat kennzeichnen) auf den Services Dritter zu teilen (durch Einbinden des öffentlichen Beitrags oder auf andere Weise), und wir ermöglichen es Suchmaschinen, Ihre öffentlichen Inhalte über ihre Dienste auffindbar zu machen.

1. Mit Ausnahme der Änderungen durch die Paragraphen 1 bis 6 dieses Abschnitts dieser Vereinbarung unterliegen Ihre und unsere Rechte und Pflichten weiterhin den Bedingungen dieser Vereinbarung. Die Lizenz, die Sie den Tripadvisor-Unternehmen wie in den Paragraphen 1 bis 6 geändert gewähren, wird als „Begrenzte Lizenz“ bezeichnet.

1. Der Klarheit halber sei gesagt, dass die Inhalte, die Sie in Verbindung mit anderen Tripadvisor-Unternehmen oder -Programmen in die Services einstellen, nicht der Begrenzten Lizenz unterliegen, sondern von den Geschäftsbedingungen des betreffenden Tripadvisor-Diensts oder -Programms geregelt werden.

 

**BUCHUNG BEI DRITTANBIETERN ÜBER Tripadvisor**

**Nutzung der Tripadvisor Sofortbuchungsdienste.** Die Tripadvisor-Unternehmen bieten Ihnen die Möglichkeit, bei Drittanbietern nach Reisen zu suchen, Reisen auszuwählen und Reservierungen vorzunehmen, ohne die Services zu verlassen.

#### WICHTIGER HINWEIS: Tripadvisor verkauft selbst keine Reisereservierungen. Tripadvisor erlaubt es Dritten, ihre Reisereservierungen auf Tripadvisor zu verkaufen.

#### Tripadvisor kann die Bewerbung und den Verkauf von Reisereservierungen durch Dritte an Sie erleichtern. Tripadvisor ist jedoch nicht die Partei, die das Angebot und/oder den Verkauf anbietet. Wenn Sie eine Reservierung bei einem Drittanbieter buchen, liegt der Vertrag über die Reservierung immer ausschließlich zwischen Ihnen und dem Drittanbieter. Tripadvisor ist nicht (a) der Käufer oder der Verkäufer der Reservierung; (b) eine Partei des Vertrags mit dem Dritten oder verantwortlich für die Erfüllung dieses Vertrags; oder (c) ein Vertreter von Ihnen oder dem Dritten im Zusammenhang mit dem Kauf oder Verkauf. Wenn Sie eine Reservierung bei einem Drittanbieter buchen, stimmen Sie zu, die Einkaufs- und Nutzungsbedingungen des Anbieters, die Datenschutzerklärung und alle anderen Regeln oder Richtlinien im Zusammenhang mit der Website oder dem Unternehmen des Anbieters zu lesen und an diese gebunden zu sein. Ihre Interaktionen mit Drittanbietern erfolgen auf eigene Gefahr. Die Tripadvisor-Unternehmen haften nicht für die Handlungen, Unterlassungen, Fehler, Erklärungen, Gewährleistungen, Verstöße oder die Fahrlässigkeit von Drittanbietern oder für Personen- oder Sachschäden, Tod oder andere Schäden oder Ausgaben, die aus Ihren Interaktionen mit Drittanbietern entstehen.

Ihre Rückgriffsrechte im Zusammenhang mit einem Vertrag zwischen Ihnen und einem Drittanbieter (z. B. in Bezug auf Rückerstattungen und Stornierungen) liegen zwischen Ihnen und dem Drittanbieter, wie im Vertrag mit diesem Drittanbieter festgelegt. Für den unwahrscheinlichen Fall, dass eine Reservierung verfügbar ist, wenn Sie eine Bestellung aufgeben, aber vor dem Check-in nicht mehr verfügbar ist, haben Sie und wir den einzigen Rechtsbehelf, sich mit dem Anbieter in Verbindung zu setzen, um alternative Vorkehrungen zu treffen oder Ihre Reservierung zu stornieren und eine Rückerstattung zu erhalten (falls zutreffend und vorbehaltlich des geltenden Rechts und Ihres Vertrags mit dem Drittanbieter).

#### Drittanbieter können Unternehmen oder Verbraucher sein. Wenn Sie im Vereinigten Königreich oder in der EU ansässig sind und der Drittanbieter, mit dem Sie einen Vertrag abschließen, auch Verbraucher ist, beachten Sie bitte, dass Sie keine Verbraucherrechte gegen diesen Anbieter durchsetzen können.

Wenn Sie Reisereservierungen über die Websites buchen, müssen Sie ein Konto erstellen, sofern Sie dies noch nicht getan haben, und Sie erkennen an, dass Sie die in unserer Datenschutzerklärung und dieser Vereinbarung beschriebenen Praktiken akzeptieren.

ALS NUTZER DER SERVICES, EINSCHLIESSLICH DER BUCHUNGSERLEICHTERUNGSSERVICES VON TRIPADVISOR-UNTERNEHMEN, ERKENNEN SIE AN UND STIMMEN ZU, DASS, SOWEIT GESETZLICH ZULÄSSIG, (1) DIE TRIPADVISOR-UNTERNEHMEN NICHT GEGENÜBER IHNEN ODER ANDEREN FÜR UNBERECHTIGTE TRANSAKTIONEN HAFTEN, DIE UNTER VERWENDUNG IHRES KENNWORTS ODER KONTOS ERFOLGEN; UND (2) SIE BEI EINER UNBERECHTIGTEN VERWENDUNG IHRES KENNWORTS ODER KONTOS MÖGLICHERWEISE GEGENÜBER TRIPADVISOR, SEINEN PARTNERUNTERNEHMEN UND/ODER ANDEREN HAFTEN.

Die Nutzung unserer Dienste, einschließlich der Erstellung eines Tripadvisor-Kontos, ist kostenlos. Eine Zahlung wird nur abgewickelt, wenn Sie eine Reservierung über die Dienste buchen. Wenn Sie eine Reservierung über die Tripadvisor-Unternehmen vornehmen, erfassen wir Ihre Zahlungsinformationen und übermitteln diese zum Abschluss der Transaktion an den Anbieter, wie in unserer Datenschutzerklärung beschrieben. Bitte beachten Sie, dass der Anbieter, nicht die Tripadvisor-Unternehmen, für die Verarbeitung Ihrer Zahlung und die Erfüllung Ihrer Reservierung verantwortlich ist.

Die Tripadvisor-Unternehmen behandeln Reservierungen nicht willkürlich, aber wir behalten uns das Recht vor, unter bestimmten Umständen die Buchungsdienste zurückzuziehen, etwa wenn eine Reservierung nicht mehr verfügbar ist oder wenn ausreichender Grund zu der Annahme besteht, dass eine Reservierungsanfrage mit betrügerischen Absichten erfolgt. Die Tripadvisor-Unternehmen behalten sich auch das Recht vor, Ihre Identität im Rahmen der Reservierungsverarbeitung zu überprüfen.

**Drittanbieter** Die Tripadvisor-Unternehmen sind keine Reisebüros und bieten oder besitzen keine Transportdienstleistungen, Unterkünfte, Restaurants, Touren, Aktivitäten oder Erlebnisse an. Auch wenn die Tripadvisor-Unternehmen Informationen zu Unterkünften im Eigentum von Drittanbietern anzeigen und Reservierungen bei bestimmten Anbietern auf der oder durch die Tripadvisor-Website vereinfachen, implizieren, suggerieren oder beschreiben diese Aktionen in keiner Weise eine Förderung oder Zustimmung von Drittanbietern durch die Tripadvisor-Unternehmen oder eine Verbindung zwischen den Tripadvisor-Unternehmen und Drittanbietern. Obwohl Mitglieder bestimmte Transportdienstleistungen, Unterkünfte, Restaurants, Touren, Aktivitäten oder Erlebnisse aufgrund ihrer eigenen Erfahrungen einstufen und bewerten können, befürworten oder empfehlen die Tripadvisor-Unternehmen die Produkte oder Dienstleistungen von Drittanbietern nicht, es sei denn, Tripadvisor vergibt bestimmte Unternehmenspreise, die auf den von Mitgliedern veröffentlichten Bewertungen basieren. Die Tripadvisor-Unternehmen billigen weder Inhalte, die von Nutzern oder Unternehmen veröffentlicht, eingereicht oder anderweitig zur Verfügung gestellt werden, noch Meinungen, Empfehlungen oder Ratschläge, die darin zum Ausdruck gebracht werden. Außerdem lehnen die Tripadvisor-Unternehmen ausdrücklich jegliche Haftung im Zusammenhang mit solchen Inhalten ab. Sie bestätigen, dass die Tripadvisor-Unternehmen nicht für die Richtigkeit oder Vollständigkeit der Informationen verantwortlich sind, die sie von Drittanbietern erhalten und auf den Services anzeigen.

Über die Services werden Sie möglicherweise mit Websites von Anbietern oder anderen Websites verlinkt, die nicht von Tripadvisor betrieben oder kontrolliert werden. Weitere Informationen hierzu finden Sie im nachfolgenden Abschnitt „Links auf Websites von Drittanbietern“.

**Buchung von Reisediensten über die Mobile-App von Tripadvisor**  Für einige der über die Mobile-App von Tripadvisor gebuchten Reiseleistungen gelten zusätzliche Nutzungsbedingungen.  Diese Bedingungen sind über die App selbst verfügbar und finden Sie [hier](https://tripadvisor.mediaroom.com/us-terms-of-use-booking).  

**Buchen von Ferienwohnungen, Restaurantreservierungen und Erfahrungen mit Drittanbietern, die auf Partner-Websites aufgeführt sind.** Einige der mit Tripadvisor verbundenen Unternehmen fungieren als Marktplätze, um Reisenden die Möglichkeit zu geben, (1) Verträge über Ferienwohnungen mit Eigentümern und Verwaltern von Objekten abzuschließen ("Ferienwohnungen"), (2) Reservierungen für Restaurants ("Restaurants") vorzunehmen und/oder (3) Reservierungen für Touren, Aktivitäten und Sehenswürdigkeiten ("Erlebnisse") bei Drittanbietern solcher Erlebnisse vorzunehmen (jeder dieser Anbieter einer Ferienwohnung und/oder eines Erlebnisses wird als "Inserent" bezeichnet). Diese Partner von Tripadvisor syndizieren ihre Anzeigen an andere Unternehmen innerhalb der Tripadvisor-Unternehmensgruppe, und deshalb sehen Sie diese auf den Websites der Tripadvisor-Unternehmen.  Als Nutzer müssen Sie für Ihre Nutzung der Services (insbesondere der Websites der Tripadvisor-Unternehmen) und jede Transaktion im Zusammenhang mit Ferienwohnungen, Restaurants oder Erlebnissen verantwortlich sein, die von den Partnern von Tripadvisor ermöglicht wird. Wir sind weder Eigentümer noch Verwalter oder Vertragsnehmer von Ferienwohnungen, Restaurants oder Erlebnissen, die in den Services aufgeführt sind.

Da weder Tripadvisor noch seine verbundenen Unternehmen Parteien von Buchungen von Ferienwohnungen, Restaurantreservierungen oder Transaktionen im Zusammenhang mit Erlebnissen zwischen Reisenden und Inserenten sind, liegt die alleinige Verantwortung für jegliche Streitigkeiten oder Konflikte im Zusammenhang mit einer tatsächlichen oder potenziellen Transaktion zwischen Ihnen und einem Inserenten, einschließlich der Qualität, des Zustands/der Bedingungen, der Sicherheit oder der Rechtmäßigkeit einer eingetragenen Ferienwohnung, eines eingetragenen Restaurants oder Erlebnisses, der Richtigkeit des Eintragsinhalts, der Fähigkeit des Inserenten, eine Ferienwohnung zu mieten, Ihnen eine Reservierung, einen Restaurantbesuch oder eine andere Dienstleistung in einem Restaurant bereitzustellen oder für ein Erlebnis zu zahlen von jedem Benutzer.

Ein Partner von Tripadvisor kann nur zum Zweck der Übermittlung Ihrer Zahlung an den Werbetreibenden als eingeschränkter Vertreter des Werbetreibenden agieren. Sie erklären sich damit einverstanden, an einen Inserenten oder ein Affiliate-Unternehmen von Tripadvisor, das als beschränkter Zahlungsvermittler im Namen eines Inserenten handelt, alle Gebühren zu zahlen, die Ihnen vor der Buchung einer Buchung für eine Ferienwohnungsbuchung oder ein Erlebnis in Rechnung gestellt werden, sowie alle Gebühren, die Ihnen infolge von Schäden in Rechnung gestellt werden, die durch Ihre Nutzung der Ferienwohnung oder des Erlebnisses in Übereinstimmung mit den geltenden Bedingungen oder Richtlinien entstanden sind.

Weitere Informationen zu Ferienwohnungsgebühren, Sicherheitsleistungen, Erlebnisgebühren, Zahlungsverarbeitung, Rückerstattungen und dergleichen finden Sie in den Geschäftsbedingungen unserer verbundenen Unternehmen (für Erlebnisse besuchen Sie [Viator](https://www.viator.com/en-GB/support/termsAndConditions); für Restaurants besuchen Sie [The Fork](https://www.thefork.de/#geo_dmn_redirect=20210629AT100); für Ferienwohnungen besuchen Sie die [Ferienwohnungen](https://rentals.tripadvisor.com/de/termsandconditions/traveler) von Tripadvisor-Unternehmen. Wenn Sie eine Reservierung für eine Ferienwohnung, ein Restaurant oder ein Erlebnis vornehmen, die von einem unserer Partner ermöglicht wird, müssen Sie die Allgemeinen Geschäftsbedingungen sowie die Datenschutzerklärung anerkennen und bestätigen.

Wenn Sie einen Rechtsstreit mit einem Werbetreibenden in der EU eingehen, stehen Ihnen hier alternative Methoden zur Lösung dieses Streits zur Verfügung: [http://ec.europa.eu/consumers/odr/](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=EN).

**REISEZIELE**

**Internationale Reisen.** Wenn Sie über Services Reservierungen für internationale Reisen bei Drittanbietern vornehmen, müssen Sie sicherstellen, dass Sie alle Einreiseanforderungen für das jeweilige Land erfüllen und dass Ihre Reisedokumente wie Reisepässe und Visa gültig sind.

Informationen zu Reisepass- und Visumanforderungen erhalten Sie bei den zuständigen Botschaften oder Konsulaten. Da sich die Anforderungen jederzeit ändern können, müssen Sie vor der Buchung und Abreise unbedingt aktuelle Informationen einholen. Die Tripadvisor-Unternehmen haften nicht gegenüber Reisenden, denen der Einstieg in ein Flugzeug oder die Einreise in ein Land verweigert wird, weil der Reisende nicht die Reisedokumente bei sich führt, die von der Airline, der Behörde oder dem Land, einschließlich der Länder, die der Reisende auf dem Weg zu seinem Ziel nur durchreist, gefordert werden.

Sie sind außerdem dafür verantwortlich, Ihren Arzt vor internationalen Reisen nach aktuellen Impfempfehlungen zu fragen und sicherzustellen, dass Sie alle Gesundheitsanforderungen für die Einreise erfüllen und alle medizinischen Ratschläge in Bezug auf Ihre Reise einhalten.

Auch wenn die meisten Reisen, einschließlich Reisen zu internationalen Zielen, ohne Vorfälle verlaufen, bestehen bei Reisen zu bestimmten Zielen größere Risiken als bei anderen. Tripadvisor fordert Reisende auf, Reiseverbote, Warnungen, Ankündigungen und Ratschläge ihrer eigenen Regierungen und der Regierungen der Zielländer zu recherchieren und zu überprüfen, bevor sie Reisen zu internationalen Zielen buchen. Informationen über die Bedingungen in verschiedenen Ländern und das mit Reisen zu bestimmten internationalen Zielen verbundene Risiko werden beispielsweise von der Regierung der Vereinigten Staaten zur Verfügung gestellt unter: [www.state.gov](http://www.state.gov/), [www.tsa.gov](https://www.tsa.gov/), [www.dot.gov](http://www.dot.gov/), [www.faa.gov](http://www.faa.gov/), [www.cdc.gov](http://www.cdc.gov/), [www.treas.gov/ofac](http://www.treas.gov/ofac) und [www.customs.gov](http://www.customs.gov/).

DURCH DIE ANGABE VON INFORMATIONEN, DIE FÜR DIE REISE ZU BESTIMMTEN INTERNATIONALEN ZIELEN RELEVANT SIND, ERKLÄREN ODER GARANTIEREN DIE TRIPADVISOR-UNTERNEHMEN NICHT, DASS REISEN ZU DIESEN ORTEN EMPFEHLENSWERT ODER RISIKOFREI SIND, UND HAFTEN NICHT FÜR SCHÄDEN ODER VERLUSTE, DIE SICH AUS REISEN ZU DIESEN ZIELEN ERGEBEN KÖNNEN.

**HAFTUNGSAUSSCHLUSS**

LESEN SIE DIESEN ABSCHNITT BITTE SORGFÄLTIG DURCH. DIESER ABSCHNITT BESCHRÄNKT DIE HAFTUNG VON TRIPADVISOR-UNTERNEHMEN IHNEN GEGENÜBER IN BEZUG AUF PROBLEME, DIE IN VERBINDUNG MIT IHRER NUTZUNG DER SERVICES AUFTRETEN KÖNNEN. WENN SIE DIE BEDINGUNGEN IN DIESEM ABSCHNITT ODER AN ANDERER STELLE IN DIESER VEREINBARUNG NICHT VERSTEHEN,

DIE INFORMATIONEN, SOFTWARE, PRODUKTE UND DIENSTE, DIE AUF DEN DIENSTEN VERÖFFENTLICHT ODER ANDERWEITIG ÜBER DIESE BEREITGESTELLT WERDEN (EINSCHLIESSLICH, ABER NICHT BESCHRÄNKT AUF DIEJENIGEN, DIE DURCH KI ERSTELLT ODER BETRIEBEN WERDEN), KÖNNEN UNGENAUIGKEITEN ODER FEHLER ENTHALTEN, EINSCHLIESSLICH FEHLERN BEI DER RESERVIERUNGSVERFÜGBARKEIT UND DEN PREISEN. DIE TRIPADVISOR-UNTERNEHMEN GARANTIEREN NICHT FÜR DIE RICHTIGKEIT DER INFORMATIONEN UND BESCHREIBUNGEN DER AUF DIESER WEBSITE DARGESTELLTEN UNTERKÜNFTE, ERLEBNISSE, FLÜGE, KREUZFAHRTEN, RESTAURANTS ODER ANDERER REISEPRODUKTE (EINSCHLIESSLICH, ABER NICHT BESCHRÄNKT AUF PREISE, VERFÜGBARKEIT, FOTOS, LISTE DER UNTERKÜNFTE, ERLEBNISSE, FLÜGE, KREUZFAHRTEN, RESTAURANTS ODER ANDERE REISEPRODUKT-ANNEHMLICHKEITEN, DIE ALLGEMEINEN PRODUKTBESCHREIBUNGEN, DIE BERICHTE UND BEWERTUNGEN USW.) UND LEHNEN JEDE HAFTUNG FÜR FEHLER ODER ANDERE UNGENAUIGKEITEN DIESBEZÜGLICH AB. DARÜBER HINAUS BEHALTEN SICH DIE TRIPADVISOR-UNTERNEHMEN AUSDRÜCKLICH DAS RECHT VOR, ALLE VERFÜGBARKEITS- UND OFFENSICHTLICHEN PREISFEHLER BEI DEN SERVICES UND/ODER AUSSTEHENDEN RESERVIERUNGEN ZU EINEM OFFENSICHTLICH FALSCHEN PREIS ZU KORRIGIEREN.

TRIPADVISOR ÄUSSERT SICH IN KEINER WEISE ÜBER DIE EIGNUNG DER SERVICES, EINSCHLIESSLICH DER INFORMATIONEN, DIE AUF SEINEN WEBSITES ODER EINEM TEIL DAVON ENTHALTEN SIND, FÜR EINEN BESTIMMTEN ZWECK, UND DIE AUFNAHME ODER DAS ANBIETEN VON PRODUKTEN ODER DIENSTLEISTUNGSANGEBOTEN AUF SEINEN WEBSEITEN ODER ANDERWEITIG ÜBER DIE SERVICES STELLT KEINE BEFÜRWORTUNG ODER EMPFEHLUNG SOLCHER PRODUKTE ODER DIENSTLEISTUNGSANGEBOTE DURCH TRIPADVISOR DAR, UNGEACHTET DER AUFGRUND VON BENUTZERBEWERTUNGEN VERGEBENEN AUSZEICHNUNGEN. ALLE DIESE INFORMATIONEN, SOFTWAREPROGRAMME, PRODUKTE UND SERVICES, DIE DURCH ODER ÜBER DIE SERVICES ZUR VERFÜGUNG GESTELLT WERDEN, WERDEN OHNE MÄNGELGEWÄHR UND OHNE GEWÄHRLEISTUNG IRGENDEINER ART BEREITGESTELLT. TRIPADVISOR LEHNT IM GESETZLICH ZULÄSSIGEN UMFANG ALLE GEWÄHRLEISTUNGEN, BEDINGUNGEN ODER SONSTIGEN BEDINGUNGEN JEGLICHER ART AB, DASS DIE SERVICES, SEINE SERVER ODER ALLE VON TRIPADVISOR GESENDETEN DATEN (EINSCHLIESSLICH E-MAILS) FREI VON VIREN ODER ANDEREN SCHÄDLICHEN KOMPONENTEN SIND. IM NACH ANWENDBAREM RECHT MAXIMAL ZULÄSSIGEN UMFANG SCHLIESST TRIPADVISOR HIERMIT ALLE GEWÄHRLEISTUNGEN UND ZUSICHERUNGEN IN BEZUG AUF DIESE INFORMATIONEN, SOFTWAREPROGRAMME, PRODUKTE UND SERVICES AUS, EINSCHLIESSLICH ALLER STILLSCHWEIGENDEN GEWÄHRLEISTUNGEN UND ZUSICHERUNGEN ODER BEDINGUNGEN IRGENDEINER ART HINSICHTLICH MARKTGÄNGIGKEIT, EIGNUNG FÜR EINEN BESTIMMTEN ZWECK, EIGENTUMSRECHT, UNGESTÖRTEM BESITZ UND NICHTVERLETZUNG VON RECHTEN DRITTER.

DIE TRIPADVISOR-UNTERNEHMEN SCHLIESSEN AUSSERDEM AUSDRÜCKLICH JEDE GEWÄHRLEISTUNG, ERKLÄRUNG ODER ANDERE BEDINGUNG IRGENDEINER ART IN BEZUG AUF DIE RICHTIGKEIT ODER DEN URHEBERRECHTLICHEN CHARAKTER DER INHALTE, DIE ÜBER DIE SERVICES VERFÜGBAR SIND, AUS.

DIE DRITTANBIETER, DIE UNTERKÜNFTE, FLÜGE, VERMIETUNGEN, ERLEBNISSE, RESTAURANTS ODER INFORMATIONEN ZU KREUZFAHRTEN, REISEN ODER ANDEREN DIENSTLEISTUNGEN AUF DEN SERVICES ANBIETEN, SIND UNABHÄNGIGE DIENSTLEISTER UND KEINE VERTRETER ODER MITARBEITER DER TRIPADVISOR-UNTERNEHMEN. DIE TRIPADVISOR-UNTERNEHMEN HAFTEN NICHT FÜR DIE HANDLUNGEN, FEHLER, UNTERLASSUNGEN, ERKLÄRUNGEN, GEWÄHRLEISTUNGEN ODER VERSTÖSSE ODER DIE FAHRLÄSSIGKEIT DIESER ANBIETER ODER FÜR PERSONEN- UND SACHSCHÄDEN, TOD ODER ANDERE SCHÄDEN ODER AUSGABEN, DIE DARAUS ENTSTEHEN. TRIPADVISOR ÜBERNIMMT KEINE HAFTUNG UND LEISTET KEINE ERSTATTUNG IM FALLE EINER VERZÖGERUNG, STORNIERUNG ODER ÜBERBUCHUNG, BEI EINEM STREIK ODER BEI HÖHERER GEWALT ODER IM FALLE ANDERER GRÜNDE, DIE AUSSERHALB DER DIREKTEN KONTROLLE VON TRIPADVISOR LIEGEN, UND IST NICHT FÜR ZUSÄTZLICHE AUSGABEN, UNTERLASSUNGEN, VERZÖGERUNGEN, ROUTENÄNDERUNGEN ODER HANDLUNGEN EINER REGIERUNG ODER BEHÖRDE VERANTWORTLICH.

VORBEHALTLICH DES VORSTEHENDEN NUTZEN SIE DIE SERVICES AUF EIGENE GEFAHR UND IN KEINEM FALL HAFTEN DIE TRIPADVISOR-UNTERNEHMEN (ODER IHRE LEITENDE ANGESTELLTE, DIREKTOREN UND MITARBEITER) FÜR MITTELBARE, UNMITTELBARE, STRAFRECHTLICHE, ZUFÄLLIGE, BESONDERE ODER FOLGESCHÄDEN ODER VERLUSTE ODER FÜR ENTGANGENE EINNAHMEN ODER GEWINNE, DEN VERLUST VON FIRMENWERT, DATEN ODER VERTRÄGEN, DIE AUFWENDUNG VON GELD ODER VERLUSTE ODER SCHÄDEN IN IRGENDEINER FORM AUS ODER IM ZUSAMMENHANG MIT GESCHÄFTSUNTERBRECHUNGEN IRGENDEINER ART AUS ODER IN IRGENDEINER FORM IN VERBINDUNG MIT IHREM ZUGRIFF AUF DIE SERVICES, IHRER ANZEIGE ODER NUTZUNG DER SERVICES ODER MIT DER VERZÖGERUNG ODER UNMÖGLICHKEIT DES ZUGRIFFS AUF DIE SERVICES ODER DEREN ANZEIGE ODER NUTZUNG (UNTER ANDEREM IHR VERTRAUEN AUF BEWERTUNGEN UND MEINUNGEN, DIE ÜBER DIE DIENSTE ANGEZEIGT WERDEN); VIREN, BUGS, TROJANISCHEN PFERDE, INFORMATIONEN, SOFTWAREPROGRAMME, VERKNÜPFTE WEBSITES, PRODUKTE UND DIENSTLEISTUNGEN, DIE ÜBER DIE DIENSTE ERHALTEN WURDEN (EINSCHLIESSLICH, ABER NICHT BESCHRÄNKT AUF SYNCHRONISATIONSPRODUKTE DER TRIPADVISOR-UNTERNEHMEN); PERSONEN- ODER SACHSCHÄDEN JEGLICHER ART, DIE SICH AUS IHRER NUTZUNG DER SERVICE-SERVER UND/ODER ALLER DARIN GESPEICHERTEN PERSÖNLICHEN INFORMATIONEN UND/ODER FINANZINFORMATIONEN ERGEBEN; FEHLER ODER AUSLASSUNGEN IN INHALTEN ODER VERLUSTE ODER SCHÄDEN JEGLICHER ART, DIE DURCH DIE NUTZUNG VON INHALTEN ODER ANDERWEITIG AUS DEM ZUGRIFF AUF DIE ANZEIGE ODER DIE NUTZUNG DER SERVICES ENTSTEHEN), EGAL OB BASIEREND AUF FAHRLÄSSIGKEIT, VERTRAG, UNERLAUBTER HANDLUNG, GEFÄHRDUNGSHAFTUNG ODER AUF SONSTIGE WEISE UND SELBST DANN, WENN TRIPADVISOR ODER SEINE PARTNER AUF DIE MÖGLICHKEIT SOLCHER SCHÄDEN HINGEWIESEN WURDE.

Wird festgestellt, dass Tripadvisor-Unternehmen für Verluste oder Schäden haftbar sind, die sich aus oder in Verbindung mit Ihrer Nutzung der Services ergeben, dann übersteigt die Haftung der Tripadvisor-Unternehmen insgesamt unter keinen Umständen (a) die Transaktionsentgelte, die für die Transaktion(en) auf den oder über die Services, welche die Forderung begründen, an die Tripadvisor-Unternehmen bezahlt wurden, oder (b) einhundert US-Dollar (100,00 USD), wobei der höhere Betrag maßgeblich ist.

Die Haftungsbeschränkung ist Teil der Risikoverteilung zwischen den Parteien. Die in diesem Abschnitt angegebenen Beschränkungen bestehen fort und gelten selbst dann, wenn festgestellt wird, dass ein beschränkter Abhilfeanspruch in diesen Bedingungen seinen wesentlichen Zweck nicht erfüllt hat. Die in diesen Bedingungen bereitgestellten Haftungsbeschränkungen kommen den Tripadvisor-Unternehmen zugute.

DIESER VERTRAG UND DER VORSTEHENDE HAFTUNGSAUSSCHLUSS BERÜHREN KEINE ZWINGENDEN GESETZLICHEN RECHTE, DIE NACH GELTENDEM RECHT, BEISPIELSWEISE NACH DEN IN BESTIMMTEN LÄNDERN GELTENDEN VERBRAUCHERSCHUTZGESETZEN, NICHT AUSGESCHLOSSEN WERDEN KÖNNEN. IM VEREINIGTEN KÖNIGREICH UND IN DER EU UMFASST DIES DIE HAFTUNG FÜR TOD ODER PERSONENSCHÄDEN, DIE DURCH UNSERE FAHRLÄSSIGKEIT ODER DIE FAHRLÄSSIGKEIT UNSERER MITARBEITER, VERTRETER ODER SUBUNTERNEHMER VERURSACHT WURDEN, SOWIE FÜR BETRUG ODER BETRÜGERISCHE FALSCHDARSTELLUNG.

Wenn Sie ein Verbraucher im Vereinigten Königreich oder in der EU sind, schließt diese Vereinbarung keine Haftung für Verluste und Schäden aus, die durch eine Verletzung einer Pflicht zur angemessenen Sorgfalt und Geschicklichkeit oder durch einen Verstoß gegen diese Vereinbarung verursacht werden, vorausgesetzt, die daraus resultierenden Schäden waren zum Zeitpunkt der Entstehung der Verpflichtungen vorhersehbar.     

WENN DAS RECHT DES LANDES, IN DEM SIE LEBEN, KEINE BESONDERE BESCHRÄNKUNG ODER KEINEN BESONDEREN AUSSCHLUSS DER IN DIESER KLAUSEL VORGESEHENEN HAFTUNG ZULÄSST, GILT DIESE BESCHRÄNKUNG NICHT. DER HAFTUNGSAUSSCHLUSS GILT ANSONSTEN FÜR DEN MAXIMALEN UMFANG, DER DURCH IHR LOKALES RECHT ZULÄSSIG IST.

**HAFTUNGSFREISTELLUNG**

Dieser Abschnitt "Freistellung" gilt nicht für Verbraucher, die ihren Wohnsitz in der EU oder im Vereinigten Königreich haben.

Sie stimmen zu, die Tripadvisor-Unternehmen und alle ihre leitenden Angestellten, Direktoren, Mitarbeiter und Vertreter vor und gegen alle Ansprüche, Forderungen, Beitreibungen, Verlusten, Schäden, Geldstrafen, Strafen oder anderen Kosten oder Ausgaben irgendeiner Art zu verteidigen und schadlos zu halten, einschließlich angemessener Anwalts- und Verwaltungsgebühren, die von Dritten auf den folgenden Grundlagen geltend gemacht werden:

* (i) von Ihnen begangene Verstöße gegen diese Vereinbarung oder die hier referenzierten Dokumente,
* (ii) von Ihnen begangene Verstöße gegen Gesetze oder die Rechte Dritter; oder
* (iii) Ihre Nutzung der Serviceleistungen, einschließlich der Website der Tripadvisor-Unternehmen.

**LINKS AUF WEBSITES VON DRITTANBIETERN**

Die Services enthalten möglicherweise Hyperlinks auf Websites, die von anderen Parteien als den Tripadvisor-Unternehmen betrieben werden. Diese Hyperlinks werden Ihnen nur zu Referenzzwecken bereitgestellt. Die Tripadvisor-Unternehmen haben keine Kontrolle über diese Websites und sind nicht für deren Inhalte oder die Datenschutz- oder sonstigen Praktiken dieser Websites verantwortlich. Weiterhin sind Sie selbst für geeignete Vorkehrungen verantwortlich, um sicherzustellen, dass die von Ihnen ausgewählten Links oder heruntergeladene Software (egal ob auf dieser oder auf anderen Websites) keine Elemente wie Viren, Würmer, Trojanische Pferde, Defekte und andere zerstörende Elemente enthalten. Die Aufnahme von Hyperlinks zu solchen Websites durch die Tripadvisor-Unternehmen impliziert keine Befürwortung des Materials auf diesen Websites oder Apps von Drittanbietern und keine Verbindung zu deren Betreibern.

In einigen Fällen werden Sie möglicherweise auf der Website oder der App eines Drittanbieters dazu aufgefordert, Ihr Tripadvisor-Konto mit einem Profil auf einer anderen Website eines Drittanbieters zu verlinken. Sie sind verantwortlich, zu beurteilen, ob Sie sich dafür entscheiden. Diese rein optionale Entscheidung, die Verlinkung dieser Informationen zuzulassen, kann jederzeit (auf der Website oder App des Drittanbieters) zurückgenommen werden. Wenn Sie sich dafür entscheiden, Ihr Tripadvisor-Konto mit einer Website oder App eines Drittanbieters zu verlinken, kann die Website oder App eines Drittanbieters auf die Informationen zugreifen, die Sie in Ihrem Tripadvisor-Konto gespeichert haben, einschließlich Informationen über andere Nutzer, mit denen Sie Informationen austauschen. Sie sollten die Allgemeinen Geschäftsbedingungen und die Datenschutzerklärung der von Ihnen besuchten Websites und Apps von Drittanbietern lesen, da diese über Regelungen und Berechtigungen verfügen, wie sie Ihre Informationen verwenden, die sich von den Services, einschließlich unserer Websites, unterscheiden können. Wir empfehlen Ihnen, diese Websites und Apps von Drittanbietern zu überprüfen und auf eigenes Risiko zu nutzen.

Software als Teil der Dienstleistung; Zusätzliche mobile Lizenzen

Software von den Services unterliegt der US-amerikanischen Exportkontrolle. Software aus den Diensten darf nicht heruntergeladen oder anderweitig exportiert oder reexportiert werden (a) in (oder an einen Staatsangehörigen oder Einwohner von) Kuba, Irak, Sudan, Nordkorea, Iran, Syrien oder ein anderes Land, gegen das die USA ein Warenembargo verhängt haben, oder (b) an eine Person in den USA. Liste der Staatsangehörigen der USA oder des US-Finanzministeriums Table of Deny Orders des Handelsministeriums. Durch die Nutzung der Services erklären und gewährleisten Sie, dass Sie sich nicht in einem solchen Land befinden und nicht unter der Kontrolle eines solchen Landes stehen, kein Staatsangehöriger oder Einwohner eines solchen Landes sind und nicht auf einer solchen Liste aufgeführt sind.

Wie bereits erwähnt, beinhalten die Services Software, die manchmal als „Apps“ bezeichnet werden kann.  Jede Software, die von den Services zum Herunterladen zur Verfügung gestellt wird („Software“), ist das urheberrechtlich geschützte Werk von Tripadvisor oder einer anderen Partei, wie entsprechend gekennzeichnet. Für Ihre Nutzung dieser Software gelten die Bedingungen der Endnutzerlizenzvereinbarung, falls vorhanden, welche die Software begleitet oder in dieser enthalten ist. Sie dürfen Software, die von einer Lizenzvereinbarung begleitet wird oder eine solche enthält, erst installieren oder nutzen, wenn Sie den Bedingungen der Lizenzvereinbarung zugestimmt haben. Für Software, die über die Services zum Herunterladen zur Verfügung gestellt und nicht von einer Lizenzvereinbarung begleitet wird, erteilen wir Ihnen, dem Nutzer, hiermit eine beschränkte, persönliche, nicht übertragbare Lizenz zur Nutzung der Software für die Anzeige oder sonstige Nutzung der Services in Übereinstimmung mit den Allgemeinen Geschäftsbedingungen dieser Vereinbarung (einschließlich der hierin genannten Richtlinien) und zu keinem anderen Zweck.

Bitte beachten Sie, dass die Software, einschließlich, aber nicht beschränkt auf die in den Services enthaltenen HTML-, XML- oder Java-Codes und Active X-Steuerelemente, Eigentum von Tripadvisor ist oder von Tripadvisor lizenziert wird und durch Copyright-Gesetze und die Bestimmungen internationaler Verträge geschützt sind. Jedes Vervielfältigen oder Weiterverteilen der Software ist ausdrücklich verboten und kann schwere zivil- und strafrechtliche Folgen nach sich ziehen. Verstöße werden im maximal möglichen Umfang verfolgt.

In Teilen der Software von Tripadvisor für Mobiltelefone wird gegebenenfalls urheberrechtlich geschütztes Material verwendet. Tripadvisor bestätigt die Nutzung besagten Materials. Darüber hinaus gibt es spezifische Bedingungen, die für die Nutzung bestimmter mobiler Tripadvisor-Anwendungen gelten. Auf der Seite [Mobile Lizenzen](https://tripadvisor.mediaroom.com/de-mobile-licenses) finden Sie spezielle Hinweise zu Anwendungen von Tripadvisor für Mobiltelefone.

OHNE EINSCHRÄNKUNG DES VORSTEHENDEN IST DAS KOPIEREN ODER VERVIELFÄLTIGEN DER SOFTWARE AUF ANDERE SERVER ODER AN ANDERE ORTE ZUR WEITEREN VERVIELFÄLTIGUNG ODER WEITERVERTEILUNG AUSDRÜCKLICH VERBOTEN. VORBEHALTLICH GELTENDER GESETZE WIRD DIE SOFTWARE, WENN ÜBERHAUPT, NUR GEMÄSS DEN BEDINGUNGEN DER LIZENZVEREINBARUNG ODER DIESER VEREINBARUNG (SOFERN ZUTREFFEND) GEWÄHRLEISTET.

Hinweise zu Urheberrecht und Marken

Tripadvisor, das Eulenlogo, die „Blasen“ zur Anzeige der Gesamtwertung und alle anderen auf den Services angezeigten Namen oder Slogans von Produkten oder Services sind eingetragene Marken und/oder Marken nach dem Common law von Tripadvisor LLC und/oder dessen Anbietern oder Lizenzgebern und dürfen ohne vorherige schriftliche Einwilligung von Tripadvisor oder dem jeweiligen Markeninhaber weder ganz noch teilweise kopiert, imitiert oder genutzt werden. Darüber hinaus ist das Aussehen und Verhalten dieser Services, einschließlich unserer Website sowie aller Seitenüberschriften, individuellen Grafiken, Schaltflächensymbole und Skripts, die damit zusammenhängen, die Dienstleistungsmarke, Marke und/oder Handelsaufmachung von Tripadvisor und darf ohne vorherige schriftliche Einwilligung von Tripadvisor weder ganz noch teilweise kopiert, imitiert oder genutzt werden. Alle anderen auf den Services erwähnten Marken, eingetragenen Marken, Produktnamen und Unternehmensnamen oder -logos sind das Eigentum der jeweiligen Inhaber. Mit Ausnahme des an anderer Stelle in dieser Vereinbarung genannten Umfangs stellt der Verweis auf Produkte, Services, Prozesse oder andere Informationen durch Angabe von Handelsnamen, Marken, Herstellern, Anbietern oder Sonstigem keine Billigung, Förderung oder Empfehlung derselben durch Tripadvisor dar.

Alle Rechte vorbehalten. Tripadvisor ist nicht für Inhalte auf Websites verantwortlich, die von anderen Parteien als Tripadvisor betrieben werden.

**Richtlinie für das Benachrichtigungsverfahren zur Löschung illegaler Inhalte**

Tripadvisor stellt ein Benachrichtigungsverfahren zur Löschung von Inhalten bereit. Wenn Sie Beschwerden oder Einwände gegen Inhalte haben, einschließlich Benutzernachrichten, die in den Services veröffentlicht werden, oder wenn Sie glauben, dass Materialien oder Inhalte, die in den Services veröffentlicht werden, gegen Ihr Urheberrecht verstoßen, setzen Sie sich bitte umgehend über unser Benachrichtigungsverfahren zur Löschung von Inhalten mit uns in Verbindung. [**Klicken Sie hier, um unsere Urheberrechtsrichtlinien und -verfahren anzuzeigen**](https://www.tripadvisor.de/pages/noticetakedown.html). Nach der Durchführung des Verfahrens wird Tripadvisor auf gültige und ordnungsgemäß begründete Beschwerden reagieren und alle angemessenen Anstrengungen unternehmen, um offensichtlich illegale Inhalte innerhalb einer angemessenen Frist zu entfernen.

**ÄNDERUNGEN AN DIESEM VERTRAG, DEN SERVICES, UPDATES, KÜNDIGUNG UND RÜCKTRITT**

**Diese Vereinbarung:**

Tripadvisor ist berechtigt, diese Geschäftsbedingungen oder Teile davon nach eigenem Ermessen (oder nach angemessenem Ermessen für Verbraucher mit Wohnsitz im Vereinigten Königreich oder in der EU) von Zeit zu Zeit zu ändern, hinzuzufügen oder zu löschen, wenn wir dies für rechtliche, allgemeine regulatorische und technische Zwecke oder aufgrund von Änderungen bei den bereitgestellten Diensten oder der Art oder dem Layout der Dienste für erforderlich halten. Sie stimmen deshalb ausdrücklich zu, an solche geänderte Geschäftsbedingungen dieser Vereinbarung gebunden zu sein.

**Änderungen:**

Die Tripadvisor-Unternehmen sind berechtigt, die Services jederzeit und kostenlos zu ändern, auszusetzen oder einzustellen, einschließlich der Verfügbarkeit der Funktionen, Datenbanken oder Inhalte der Services, einschließlich:

(a)           Sicherstellung der Einhaltung geltender Gesetze und/oder Berücksichtigung von Änderungen einschlägiger Gesetze und regulatorischer Anforderungen, wie z. B. verbindlicher Verbrauchergesetze;

(b)           Vorübergehende Wartung, Behebung von Bugs, Implementierung technischer Anpassungen für betriebliche Zwecke oder Verbesserung, wie z. B. Anpassung der Services an eine neue technische Umgebung, Übertragung der Services auf eine neue Hosting-Plattform oder Sicherstellung der Service-Kompatibilität mit den Geräten und der Software (wie von Zeit zu Zeit aktualisiert);

(c)           die Dienste zu aktualisieren oder zu ändern, einschließlich der Veröffentlichung neuer Versionen der Websites auf bestimmten Geräten oder anderweitiger Änderungen oder Modifizierungen bestehender Funktionen;

(d)           zur Änderung der Struktur, des Designs oder des Layouts der Services, einschließlich der Änderung des Namens oder der Markenänderung der Services, oder zur Änderung, Verbesserung und/oder Erweiterung der verfügbaren Funktionen und Funktionen; und

(e)           aus Sicherheitsgründen.

Für Benutzer im Vereinigten Königreich und in der EU: Wenn wir Änderungen an den Diensten vornehmen, die sich nach billigem Ermessen negativ auf Ihren Zugang zu den Diensten oder Ihre Nutzung der Dienste auswirken, werden wir Sie mindestens 30 Tage über die Änderungen informieren. Wenn Sie Ihren Vertrag mit uns nicht kündigen, bevor diese Änderungen vorgenommen werden, betrachten wir dies als Ihre Annahme der Änderungen.

**Updates:**

Wir nehmen möglicherweise Aktualisierungen vor, um die Services mit den geltenden Verbrauchergesetzen in Einklang zu bringen. Wenn in Ihren Einstellungen automatische Updates aktiviert sind, aktualisieren wir automatisch die Services. Sie können Ihre Einstellungen jederzeit ändern, wenn Sie keine automatischen Updates mehr erhalten möchten.

Wir empfehlen Ihnen, alle Aktualisierungen der Services zu akzeptieren, über die wir Sie informieren, sobald und sobald sie verfügbar sind, um das bestmögliche Nutzererlebnis zu bieten und sicherzustellen, dass die Services ordnungsgemäß funktionieren. Möglicherweise müssen Sie dazu auch das Betriebssystem Ihres Geräts aktualisieren. Sobald neue Betriebssysteme und Geräte veröffentlicht werden, werden wir möglicherweise mit der Zeit keine älteren Versionen mehr unterstützen.

Wenn Sie keine Updates herunterladen oder installieren, sind die Services möglicherweise nicht mehr verfügbar, werden nicht mehr unterstützt oder haben frühere Funktionen.

Wir sind nicht verantwortlich für das Versäumnis, ein Update oder die zuletzt veröffentlichte Version der Services herunterzuladen oder zu installieren, um von neuen oder verbesserten Funktionen und/oder Funktionen zu profitieren und/oder Kompatibilitätsanforderungen zu erfüllen, wenn wir Sie über das Update informiert, die Folgen einer nicht erfolgten Installation erläutert und Installationsanweisungen bereitgestellt haben.

Jegliche Änderungen an dem Service, die nicht mit dieser "Updates"-Bestimmung in Einklang stehen, unterliegen dem Abschnitt "Änderungen" oben.

**Kündigung**

Die Tripadvisor-Unternehmen können auch Grenzen festlegen oder Ihren Zugriff auf die gesamten oder Teile der Services ohne Vorankündigung oder Haftung aus technischen oder sicherheitsbezogenen Gründen beschränken, um den unbefugten Zugriff auf Daten oder den Verlust oder die Zerstörung von Daten zu verhindern, oder wenn Tripadvisor und/oder seine Partner nach eigenem Ermessen erachten, dass Sie gegen eine Bestimmung dieser Vereinbarung oder eines Gesetzes oder einer Verordnung verstoßen, und wenn Tripadvisor und/oder seine Partner beschließen, die Bereitstellung eines Teils der Services einzustellen.

Tripadvisor kann diese Vereinbarung mit Ihnen jederzeit und ohne Vorankündigung kündigen, wenn es in gutem Glauben davon ausgeht, dass Sie gegen diese Vereinbarung verstoßen haben, oder wenn es anderweitig der Ansicht ist, dass eine Kündigung vernünftigerweise erforderlich ist, um die Rechte der Tripadvisor-Unternehmen und/oder anderer Nutzer der Services zu schützen. Das bedeutet, dass wir die Bereitstellung von Services für Sie einstellen können.

**GERICHTSBARKEIT UND ANWENDBARES RECHT**

Diese Website befindet sich im Eigentum und wird kontrolliert von Tripadvisor LLC, einer US-amerikanischen Gesellschaft mit beschränkter Haftung. Diese Vereinbarung und alle Streitigkeiten oder Forderungen (einschließlich außervertraglicher Streitigkeiten oder Forderungen), die sich aus oder im Zusammenhang mit der Vereinbarung oder ihrem Gegenstand oder ihrer Gründung ergeben, unterliegen dem Recht des Commonwealth of Massachusetts, USA, und werden in Übereinstimmung mit diesem ausgelegt. Sie willigen hiermit in die ausschließliche sachliche und örtliche Zuständigkeit der Gerichte in Massachusetts, USA, ein und erkennen die Billigkeit und Angemessenheit von Verfahren in diesen Gerichten für alle Streitigkeiten an, die sich aus oder im Zusammenhang mit der Nutzung der Services durch Sie oder Dritte ergeben. Sie stimmen zu, dass über alle Forderungen, die Sie möglicherweise aus oder im Zusammenhang mit den Services gegen Tripadvisor LLC haben, von einem sachlich zuständigen Gericht im Commonwealth of Massachusetts entschieden werden muss. Die Nutzung der Services ist in jeder Region unzulässig, in der nicht alle Bestimmungen dieser Allgemeinen Geschäftsbedingungen, unter anderem dieser Absatz, rechtswirksam sind. Nichts in dieser Bestimmung beschränkt das Recht von Tripadvisor LLC, gerichtlich gegen Sie in einem anderen zuständigen Gericht oder Gerichten vorzugehen. Das Vorstehende gilt nicht in dem Umfang, in dem das anwendbare Recht im Land Ihres Wohnsitzes die Anwendung eines anderen Rechts und/oder einer anderen Gerichtsbarkeit erfordert – insbesondere wenn Sie die Services als Verbraucher nutzen –, und dies kann nicht vertraglich ausgeschlossen werden und unterliegt nicht den Übereinkommen der Vereinten Nationen über Verträge über den internationalen Warenkauf, sofern anderweitig anwendbar.  Wenn Sie die Services als Verbraucher und nicht als Geschäfts- oder Handelsvertreter nutzen, sind Sie möglicherweise berechtigt, Forderungen gegen Tripadvisor vor den Gerichten Ihres Wohnsitzlandes zu erheben. Diese Klausel gilt ansonsten in dem Umfang, der in Ihrem Land oder Wohnsitz maximal zulässig ist.

**WÄHRUNGSRECHNER**

Währungskurse basieren auf verschiedenen öffentlich zugänglichen Quellen und sollten nur als ungefähre Werte verwendet werden. Die Kurse sind nicht als genau verifiziert und die tatsächlichen Kurse können hiervon abweichen. Die Kursangaben werden möglicherweise nicht tägliche aktualisiert. Es wird davon ausgegangen, dass die bereitgestellten Informationen genau sind, aber diese Genauigkeit wird von den Tripadvisor-Unternehmen nicht gewährleistet oder garantiert. Wenn Sie diese Informationen zu finanziellen Zwecken nutzen, sollten Sie einen qualifizierten Fachmann hinzuziehen, um die Genauigkeit der Währungskurse zu verifizieren. Wir autorisieren keine Nutzung dieser Informationen zu anderen Zwecken als Ihrer persönlichen Nutzung, und es ist Ihnen ausdrücklich verboten, diese Informationen zu Geschäftszwecken weiterzuverkaufen, weiterzuverteilen und zu nutzen.

Allgemeine Bestimmungen

Wir behalten uns das Recht vor, jeden Benutzernamen, Kontonamen, Spitznamen, Identifikator oder jede andere Benutzerkennung aus irgendeinem Grund und ohne Haftung Ihnen gegenüber zurückzufordern.

Sie stimmen zu, dass durch diese Vereinbarung oder die Nutzung der Services kein Gemeinschaftsunternehmen, kein Beschäftigungsverhältnis und keine Vertretung oder Partnerschaft zwischen Ihnen und Tripadvisor und/oder seinen Partnern begründet wird.

Unsere Erfüllung dieser Vereinbarung unterliegt geltendem Recht und der Gerichtsbarkeit, und nichts in dieser Vereinbarung beschränkt unser Recht, Anfragen oder Anforderungen von Vollstreckungsbehörden oder Gerichten in Bezug auf Ihre Nutzung der Services oder in Bezug auf uns bereitgestellte oder von uns erfasste Informationen zu dieser Nutzung nachzukommen. Im nach anwendbarem Recht zulässigen Umfang stimmen Sie zu, dass Sie Ansprüche aus oder im Zusammenhang mit Ihrem Zugriff auf die Services oder deren Nutzung innerhalb von zwei (2) Jahren ab dem Datum erheben, an dem dieser Anspruch entstanden ist, und andernfalls unwiderruflich auf diesen Anspruch verzichten.

Wenn sich ein Teil dieser Vereinbarung als ungültig oder nicht durchsetzbar in Übereinstimmung mit dem anwendbaren Recht herausstellt, einschließlich, aber nicht beschränkt auf die oben dargelegten Gewährleistungsausschlüsse und Haftungsbeschränkungen, dann gilt die ungültige oder nicht durchsetzbare Bestimmung als durch eine gültige, durchsetzbare Bestimmung ersetzt, die der Absicht der ursprünglichen Bestimmung am nächsten kommt, und die übrigen Bestimmungen in dieser Vereinbarung bleiben in Kraft.

Diese Vereinbarung (und alle anderen hierin genannten Bedingungen) stellt die gesamte Vereinbarung zwischen Ihnen und Tripadvisor in Bezug auf die Services dar und ersetzt alle früheren oder gleichzeitigen Mitteilungen und Vorschläge, ob elektronisch, mündlich oder schriftlich, zwischen Ihnen und Tripadvisor in Bezug auf die Services. Eine gedruckte Fassung von dieser Vereinbarung und von elektronischen Benachrichtigungen ist in Gerichts- und Verwaltungsverfahren basierend auf dieser Vereinbarung oder in Bezug auf diese im selben Umfang zulässig wie andere geschäftliche Dokumente und Unterlagen, die ursprünglich in gedruckter Form erstellt und verwaltet wurden, und es gelten hierbei dieselben Bedingungen.

Die folgenden Abschnitte gelten auch nach einer Beendigung dieser Vereinbarung:

* #### Zusätzliche Produkte
    
* #### Unzulässige Aktivitäten
    
* #### Bewertungen, Kommentare und Verwendung anderer interaktiver Bereiche; Lizenzgewährung
    
    * #### Einschränkung der Lizenzrechte von Tripadvisor
        
* Reiseziele
    * Internationale Reisen
* **HAFTUNGSAUSSCHLUSS**
* **HAFTUNGSFREISTELLUNG**
* Software als Teil der Dienstleistung; Zusätzliche mobile Lizenzen
* #### ·Hinweise zu Urheberrecht und Marken
    
    * #### Richtlinie für das Benachrichtigungsverfahren zur Löschung illegaler Inhalte
        
* #### Änderungen an den Services; Kündigung
    
* #### Gerichtsbarkeit und anwendbares Recht
    
* #### Allgemeine Bestimmungen
    
* #### Servicehilfe
    

Die Geschäftsbedingungen dieser Vereinbarung sind in der Sprache der Tripadvisor-Websites und/oder -Anwendungen verfügbar, auf denen die Services aufgerufen werden können.

Die Websites und/oder Apps, auf denen die Services abgerufen werden können, werden möglicherweise nicht immer regelmäßig aktualisiert und müssen deshalb nicht als redaktionelles Produkt nach geltendem Recht eingetragen werden.

Fiktive Namen von Unternehmen, Produkten, Personen, Figuren und/oder Daten, die in den, auf den oder über die Services erwähnt werden, sind nicht dazu bestimmt, echte Personen, Unternehmen, Produkte oder Ereignisse zu repräsentieren.

Nichts in dieser Vereinbarung gilt als Übertragung von Rechten oder Vorteilen Dritter, es sei denn, die Partner von Tripadvisor gelten als ausdrückliche Drittbegünstigte dieser Vereinbarung.

Es ist Ihnen untersagt, Ihre Rechte oder Pflichten aus dieser Vereinbarung ohne unsere Zustimmung auf Dritte zu übertragen.

Alle hier nicht ausdrücklich erteilten Rechte bleiben vorbehalten.

**SERVICEHILFE**

Wenn Sie Fragen haben oder uns kontaktieren möchten, rufen Sie bitte unsere [Hilfe](https://www.tripadvisorsupport.com/de-DE/hc/traveler) auf oder senden Sie uns eine E-Mail an [help@tripadvisor.com](mailto:help@tripadvisor.com). Sie können uns gerne auch ein Schreiben an folgende Adresse senden:

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, USA

Bitte beachten Sie, dass Tripadvisor LLC keine rechtliche Korrespondenz oder Zustellung von Gerichtsverfahren mit anderen Mitteln als der Zustellung von gedruckten Exemplaren an die Adresse unmittelbar oben akzeptiert, es sei denn, dies ist durch geltende Gesetze vorgeschrieben.  Zur Vermeidung von Zweifeln und ohne Einschränkung akzeptieren wir daher keine Mitteilungen oder Rechtsdienstleistungen, die bei unseren Partnern oder Tochtergesellschaften hinterlegt sind.

**DIGITAL SERVICES ACT - Kontakt FÜR NUTZER**

Sie können uns direkt über die unten angegebenen Kontaktinformationen kontaktieren:

E-Mail-Adresse: [help@tripadvisor.com](mailto:help@tripadvisor.com) 

**DIGITAL SERVICES ACT KONTAKT UND RECHTLICHER VERTRETER FÜR BEHÖRDEN**

Wenn Sie eine nationale Behörde eines Mitgliedstaats, die Europäische Kommission oder der Europäische Ausschuss für digitale Dienste sind, können Sie uns direkt über die unten angegebenen Kontaktinformationen kontaktieren:

E-Mail-Adresse: [poc-dsa@tripadvisor.com](mailto:poc-dsa@tripadvisor.com) 

Vertrauenswürdige Flaggen und Berufsverbände können uns ebenfalls über diese E-Mail-Adresse kontaktieren.

Alternativ können sich die Behörden über die unten aufgeführten Kontaktdaten an Tripadvisor Ireland Limited wenden, unseren gesetzlichen Vertreter für die Zwecke des Digital Services Act:

Adresse: 70 Sir John Rogerson's Quay, Dublin 2, Irland

E-Mail-Adresse: [dsa.notifications@tripadvisor.com](mailto:dsa.notifications@tripadvisor.com)

Telefonnummer:

Bitte kontaktieren Sie uns nicht über unseren gesetzlichen Vertreter unter Verwendung dieser Daten, es sei denn, Sie kontaktieren uns im Namen einer der oben genannten öffentlichen Stellen, da Tripadvisor auf Ihre Anfrage nicht antworten wird. Wenn Sie Fragen zu unserer Plattform haben oder eine Beschwerde einreichen möchten, kontaktieren Sie uns bitte über die Kontaktinformationen, die in den Abschnitten "SERVICEHILFE" oder "DIGITAL SERVICES ACT - Kontakt FÜR NUTZER" oben angegeben sind.

©2024 Tripadvisor LLC. Alle Rechte vorbehalten.

Zuletzt aktualisiert 16. Februar 2024