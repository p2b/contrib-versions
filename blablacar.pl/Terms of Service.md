**Regulamin**

Regulamin | [Polityka Prywatności](https://www.blablacar.pl/about-us/privacy-policy) | [Pliki Cookie](https://www.blablacar.pl/about-us/cookies-policy) | [Prawa pasażerów autobusów](https://transport.ec.europa.eu/index_en) | [Super Driver](https://blog.blablacar.pl/about-us/reliable-drivers) | [DSA – Transparency reports](https://blog.blablacar.fr/digital-service-act-transparency-reports/)

_Regulamin obowiązuje od 12 listopada 2024._

**1\. Przedmiot**
-----------------

Comuto SA opracowała platformę umożliwiającą wspólne przejazdy, dostępną na witrynie internetowej pod adresem [www.blablacar.pl](http://www.blablacar.pl/) lub w postaci aplikacji na urządzenia mobilne (dalej „**Platforma**”). Celem platformy jest (i) umożliwienie nawiązywania kontaktu pomiędzy kierowcami podróżującymi do określonego miejsca docelowego a pasażerami zamierzającymi udać się w tym samym kierunku, w celu zapewnienia im możliwości wspólnego Przejazdu i podziału związanych z nim kosztów oraz (ii) umożliwienie znalezienia i pozyskania informacji dotyczących podróży autobusem publikowanych przez profesjonalnych Przewoźników dostarczających usługi transportowe rezerwacji i kupna biletu autobusowego. Platforma jest obsługiwana przez Comuto SA w częsci dotyczącej przejazdów wspólnych oraz przez Busfor.pl sp. z o.o w częsci dotyczącej rezerwacji biletów autobusowych. Comuto SA i Busfor.pl sp. z o.o. są dalej zwane “**BlaBlaCar**”. 

W niektórych przypadkach, w zależności od okoliczności, możesz zostać przekierowany na witrynę internetową prowadzoną przez Busfor.pl Sp. z o.o., aby dokończyć zakup Biletu autobusowego (dalej zwaną „**Witryną Internetową Strony Trzeciej**”). Prosimy zwrócić uwagę, że Busfor.pl Sp. z o. o. w tym przypadku działa jako oddzielny administrator danych.

Witryna Internetowa Strony trzeciej posiada swoją własną politykę prywatności i regulamin. BlaBlaCar nie ponosi żadnej odpowiedzialności i nie akceptuje roszczeń za treść i aktywności dokonywanych na Witrynie Internetowej Strony Trzeciej. BlaBlaCar poleca, przed użyciem Witryny Internetowej Strony Trzeciej, zapoznanie się z polityką prywatności i regulaminem na Witrynie Strony Trzeciej.

Przedmiotem niniejszego Regulaminu jest zdefiniowanie zasad dostępu oraz użytkowania Platformy. Prosimy o dokładne zapoznanie się z nimi. BlaBlaCar nie jest stroną jakichkolwiek porozumień, umów lub relacji umownych jakiejkolwiek natury pomiędzy Użytkownikami Platformy bądź pomiędzy Użytkownikami a Przewoźnikami.

 Do niektórych usług na Platformie mogą mieć zastosowanie dodatkowe warunki, takie jak warunki dotyczące konkretnego wydarzenia, działania lub promocji, i takie dodatkowe warunki zostaną ujawnione w odpowiednim trybie.

Jeśli korzystasz ze swojego Konta w celu zalogowania się na platformę BlaBlaCar innego kraju (na przykład, www.blablacar.com.br), należy pamiętać, że (i) regulamin tej platformy, (ii) polityka prywatności tej platformy oraz (iii) przepisy ustawowe tego kraju mają zastosowanie. Oznacza to również, że informacje o Koncie, w tym dane osobowe, mogą być przekazywane do podmiotu prawnego obsługującego tę inną platformę. Należy zwrócić uwagę, że BlaBlaCar zastrzega sobie prawo do ograniczenia dostępu do Platformy użytkownikom zasadnie zidentyfikowanym jako znajdującym się poza terytorium Unii Europejskiej.

Klikając “Zaloguj się przez Facebook” lub “Zarejestruj się, używając swojego adresu e-mail”, a także przy zakupie Biletu autobusowego na Platformie bez rejestracji lub autoryzacji na Platformie Użytkownik oświadcza, że zapoznał się i zaakceptował niniejszy Regulamin.

**2\. Definicje**
-----------------

W niniejszym Regulaminie:

„**Bilet autobusowy**” oznacza dokument wydany Użytkownikowi (Pasażerowi) po dokonaniu Rezerwacji Przejazdu autobusem, potwierdzający zawarcie umowy przewozu z określonym Przewoźnikiem za pośrednictwem Platformy, uregulowany w Ogólnym Regulaminie Sprzedaży, z zastrzeżeniem jakiegokolwiek szczególnego warunku dodatkowo określonego pomiędzy Pasażerem a Przewoźnikiem i wyszczególnionego na Bilecie;

„**BlaBlaCar**” ma znaczenie określone w Artykule 1 powyżej;

„**Cena**” oznacza, w przypadku danego Przejazdu autobusem, cenę zawierającą wszystkie podatki, opłaty i koszty odpowiednich usług, uiszczaną przez Klienta na Platformie w momencie walidacji zakupu Biletu Autobusowego, za Miejsce na danym Przejeździe autobusem;

„**Kierowca**” oznacza Użytkownika, który jest osobą fizyczną dokonującą czynności w ramach Platformy, które nie są związane z jej działalnością gospodarczą lub zawodową i która korzysta z Platformy w celu oferowania innym osobom możliwości ich przewozu w zamian za Udział w Kosztach w odniesieniu do określonego Przejazdu, w terminie określanym wyłącznie przez Kierowcę;

„**Klient**” oznacza każdą osobę fizyczną (Użytkownika lub nie), kupującą dla siebie lub w imieniu innej osoby, która będzie Pasażerem, Bilet autobusowy za pośrednictwem Platformy w celu wykonania Przejazdu autobusem realizowanego przez Przewoźnika;

„**Konto**” oznacza konto, które musi zostać założone w celu zostania Użytkownikiem i uzyskania dostępu do niektórych usług oferowanych na Platformie;

„**Konto w serwisie Facebook**” ma znaczenie określone w Artykule 3.2 poniżej;

„**Miejsce**” oznacza miejsce siedzące zarezerwowane przez Pasażera w pojeździe Kierowcy lub w pojeździe Przewoźnika;

„**Odcinek**” ma znaczenie określone w Artykule 4.1 poniżej;

„**Ogłoszenie o wspólnym przejeździe**” oznacza ogłoszenie dotyczące Przejazdu, zamieszczone na Platformie przez Kierowcę;

„**Ogłoszenie przejazdu autobusem**” oznacza ogłoszenie dotyczące przejazdu autobusem Przewoźnika Autobusowego zamieszczone na Platformie;

„**Ogólny Regulamin Sprzedaży**” oznacza Ogólny Regulamin Sprzedaży Przewoźnika w zależności od wybranego przez Klienta Przejazdu autobusem oraz szczególne warunki dostępne u Przewoźnika, które zostały uznane za przeczytane przez Klienta przed złożeniem zamówienia;

„**Opłata serwisowa**” ma znaczenie nadane jej w Artykule 5.2 poniżej;

„**Pasażer**” oznacza Użytkownika, który zaakceptował ofertę przewozu ze strony Kierowcy lub Przewoźnika, lub – jeżeli dotyczy – osobę, na rzecz której Użytkownik zarezerwował Miejsce;

„**Platforma**” ma znaczenie określone w Artykule 1 powyżej;

„**Potwierdzenie Rezerwacji**” ma znaczenie określone w Artykule 4.2.1 poniżej;

„**Przejazd**” oznacza Wspólny Przejazd, lub Przejazd Autobusem;

„**Przejazd Autobusem**” oznacza podróż, która jest przedmiotem Ogłoszenia Przejazdu Autobusem opublikowana na Platformie;

„**Przewoźnik**” oznacza podmiot, który prowadzi we własnym imieniu działalność gospodarczą lub zawodową i który oferuje w ramach Platformy swoje profesjonalne Usługi transportowe, w związku z prowadzoną działalnością gospodarczą lub zawodową;

„**Regulamin**” oznacza niniejszy Regulamin;

„**Rezerwacja**” ma znaczenie określone w Artykule 4.2.1 poniżej;

„**Treści Użytkownika**” mają znaczenie określone w Artykule 11.2 poniżej;

„**Udział w Kosztach**” oznacza, w odniesieniu do określonego Przejazdu, sumę pieniędzy wymaganą przez Kierowcę i zaakceptowaną przez Pasażera jako jego udział w kosztach Przejazdu;

„**Usługi**” oznaczają wszystkie usługi świadczone przez BlaBlaCar za pośrednictwem Platformy;

„**Usługi transportowe**” oznaczają usługi transportowe subskrybowane przez Pasażera Przejazdu autobusem i świadczone przez Przewoźnika;

„**Użytkownik**” oznacza dowolną osobę, która założyła Konto na Platformie;

„**Witryna internetowa**” oznacza witrynę dostępną pod adresem www.blablacar.pl;

„**Wspólny Przejazd**” oznacza podróż stanowiącą przedmiot Ogłoszenia o wspólnym przejeździe zamieszczonego przez Kierowcę na Platformie, na którego podstawie wyraża on zgodę na przewóz Pasażerów w zamian za Udział w Kosztach.

**3\. Rejestracja na Platformie i założenie Konta**
---------------------------------------------------

### **3.1. Warunki rejestracji na Platformie**

Platforma może być wykorzystywana przez osoby, które ukończyły 18 lat. Dokonanie rejestracji na Platformie przez osobę niepełnoletnią jest całkowicie zabronione. Poprzez dostęp, korzystanie lub rejestrację na Platformie Użytkownik oświadcza, że ma ukończone 18 lat.

### **3.2. Założenie Konta**

Platforma umożliwia Użytkownikom zamieszczanie i wyświetlanie Ogłoszeń o wspólnym przejeździe oraz wzajemną interakcję celem rezerwacji Miejsca, a także przeglądanie ogłoszeń o Przejazdach autobusowych i kupowanie Biletów autobusowych. Ogłoszenia mogą być również wyświetlane przez osoby, które nie są zarejestrowane na Platformie. Niemniej jednak, zamieszczenie Ogłoszenia lub przeprowadzenie rezerwacji Miejsca nie jest możliwe bez wcześniejszego założenia Konta i zostania Użytkownikiem.

W niektórych przypadkach, w zależności od okoliczności, będziesz mógł zarezerwować Miejsce na Przejazd autobusem bez zakładania Konta, ale zgodnie z niniejszym Regulaminem i warunkami Przewoźnika, za pomocą którego wybierasz się w podróż.

Aby założyć Konto, należy:

(i) wypełnić wszystkie obowiązkowe pola formularza rejestracji;

(ii) lub zalogować się do swojego konta w serwisie Facebook za pośrednictwem Platformy (dalej “Konto w serwisie Facebook”). Korzystając z tej funkcji Użytkownik zdaje sobie sprawę, że BlaBlaCar będzie posiadać dostęp do informacji znajdujących się na jego Koncie w serwisie Facebook będzie je publikować na Platformie i przechowywać. Użytkownik może w dowolnej chwili usunąć powiązanie pomiędzy jego Kontem a Kontem w serwisie Facebook, korzystając ze strony [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl). Aby uzyskać więcej informacji dotyczących wykorzystywania danych pochodzących z twojego Konta w serwisie Facebook, należy zapoznać się z postanowieniami naszej Polityki Prywatności, jak również polityką prywatności serwisu Facebook.

Aby zarejestrować się na Platformie należy wcześniej przeczytać i zaakceptować niniejszy Regulamin.

Zakładając swoje Konto – niezależnie od wybranej metody – Użytkownik zobowiązuje się do przekazania kompletnych i prawdziwych informacji oraz ich aktualizacji za pośrednictwem profilu na swoim Koncie lub poprzez zawiadomienie BlaBlaCar w celu zapewnienia ich stosowności i dokładności przez okres trwania umowy z BlaBlaCar.

W przypadku rejestracji za pośrednictwem poczty e-mail, Użytkownik zobowiązuje się do zachowania w poufności hasła wybranego podczas zakładania Konta oraz nieujawniania go osobom trzecim. W razie utraty lub ujawnienia swojego hasła, Użytkownik jest zobowiązany do natychmiastowego zawiadomienia o tym BlaBlaCar. Użytkownik ponosi pełną odpowiedzialność za wykorzystanie jego Konta przez jakiekolwiek strony trzecie, chyba że wyraźnie zawiadomił BlaBlaCar o utracie, niedozwolonym wykorzystaniu, lub ujawnieniu swojego hasła osobie trzeciej.

Użytkownik zobowiązuje się, że nie będzie tworzył lub wykorzystywał – posługując się tożsamością własną lub osoby trzeciej – żadnych innych Kont, niż utworzone pierwotnie.

### **3.3. Weryfikacja**

BlaBlaCar może, w celu zapewnienia przejrzystości, zwiększenia zaufania lub zapobiegania oszustwom lub ich wykrywania, skonfigurować system weryfikacji niektórych informacji podanych przez Ciebie w swoim profilu. Dzieje się tak zwłaszcza w przypadku, gdy podajesz swój numer telefonu lub przedstawiasz nam dokument tożsamości.

Użytkownik jest świadom i godzi się z faktem, że jakiekolwiek odniesienia na Platformie lub w ramach Usług do „zweryfikowanych” (co obejmuje „Zweryfikowany Profil”) informacji lub podobnego rodzaju wyrażenia oznaczają jedynie, że dany Użytkownik pozytywnie przeszedł procedurę weryfikacji na Platformie lub w ramach Usług, w celu przekazania bardziej szczegółowych informacji na temat Użytkownika, z którym zamierza odbyć podróż. BlaBlaCar nie może zagwarantować prawdziwości, rzetelności ani ważności informacji będących przedmiotem procedury weryfikacji.

**4\. Korzystanie z Usług**
---------------------------

### **4.1. Zamieszczanie Ogłoszeń o wspólnym przejeździe**

Jako Użytkownik, pod warunkiem spełnienia poniższych warunków, możesz tworzyć i zamieszczać na Platformie swoje Ogłoszenia o wspólnym przejeździe, podając informacje dotyczące planowanego Wspólnego Przejazdu (data/godzina, miejsce wyjazdu i przyjazdu, liczba oferowanych miejsc, dostępne opcje, wysokość Udziału w Kosztach itp.).

Zamieszczając swoje Ogłoszenie o wspólnym przejeździe, Użytkownik może określić miasta, w których zamierza się zatrzymać, aby przyjąć lub wysadzić Pasażerów. Poszczególne odcinki Wspólnego Przejazdu pomiędzy tymi miastami lub jednym z tych miast, a miejscem wyjazdu lub przyjazdu są dalej określane jako “Odcinki”.

Użytkownik może zamieszczać Ogłoszenia o wspólnym przejeździe jedynie, jeśli spełnia łącznie wszystkie określone poniżej warunki:

(i) posiada ważne prawo jazdy;

(ii) zamieszcza Ogłoszenia o wspólnym przejeździe dotyczące wyłącznie pojazdów, których jest właścicielem lub z których korzysta za wyraźną zgodą ich właściciela, a w każdym przypadku, do których wykorzystania jest uprawniony w celu odbywania wspólnych przejazdów;

(iii) jest i pozostaje głównym kierowcą pojazdu stanowiącego przedmiot Ogłoszenia o wspólnym przejeździe;

(iv) pojazd stanowi przedmiot ważnej polisy ubezpieczeniowej dotyczącej ubezpieczenia osób trzecich;

(v) nie ma żadnych przeciwwskazań medycznych w odniesieniu do prowadzenia pojazdów;

(vi) pojazd, który zamierza wykorzystać w celu realizacji Wspólnego Przejazdu to samochód osobowy posiadający 4 koła i maksymalnie 7 miejsc;

(vii) nie zamierza zamieścić innego ogłoszenia dotyczącego tego samego Wspólnego Przejazdu na Platformie;

(viii) nie oferuje większej liczby Miejsc, niż ich liczba dostępna w jego pojeździe;

(ix) wszystkie oferowane Miejsca są wyposażone w pasy bezpieczeństwa, nawet jeśli pojazd został zatwierdzony do ruchu z miejscami nieposiadającymi pasów bezpieczeństwa;

(x) korzysta z pojazdu znajdującego się w dobrym stanie, spełniającego wszystkie obowiązujące przepisy prawa i praktyki, a w szczególności posiadającego ważny certyfikat kontroli technicznej (potwierdzający, że pojazd nadaje się do jazdy);

(xi) jest konsumentem i nie działa w charakterze służbowym.

Użytkownik ponosi we własnym zakresie pełną odpowiedzialność za treść Ogłoszenia o wspólnym przejeździe zamieszczanego na Platformie. W związku z powyższym, Użytkownik oświadcza, że wszystkie informacje zamieszczone w jego Ogłoszeniu o wspólnym przejeździe są prawidłowe i prawdziwe oraz zobowiązuje się do realizacji Wspólnego Przejazdu na warunkach określonych w tym Ogłoszeniu o wspólnym przejeździe.

Pod warunkiem, że Ogłoszenie o wspólnym przejeździe zamieszczone przez Użytkownika jest zgodne z niniejszym Regulaminem, zostanie ono zamieszczone na Platformie i będzie w ten sposób widoczne dla innych Użytkowników oraz osób odwiedzających, także niebędących Użytkownikami, przeprowadzających wyszukiwanie na Platformie bądź na witrynie internetowej partnerów BlaBlaCar. BlaBlaCar zastrzega sobie prawo – na podstawie wyłącznie własnej decyzji – odmowy zamieszczenia lub usunięcia w dowolnej chwili jakiegokolwiek Ogłoszenia o wspólnym przejeździe niespełniającego warunków niniejszego Regulaminu lub jeżeli uważa, że przynosi ono jakiekolwiek szkody dla wizerunku BlaBlaCar, Platformy bądź Usług i/lub zawieszenia Konta Użytkownika zamieszczającego takie Ogłoszenia o wspólnym przejeździe w trybie postanowień Artykułu 9 niniejszego Regulaminu.

Informujemy, że w przypadku podawania się jako konsument korzystając z Platformy, podczas gdy w rzeczywistości działasz w charakterze służbowym, narażasz się na sankcje przewidziane przez obowiązujące prawo.

### **4.2. Rezerwacja Miejsca**

„BlaBlaCar” stworzył system rezerwacji Miejsc online („**Rezerwacja**”) dla Przejazdów oferowanych na Platformie. 

Metody rezerwacji Miejsca są uzależnione od rodzaju planowanego Przejazdu.

BlaBlaCar udostępnia Użytkownikom Platformy wyszukiwarkę opartą na różnych kryteriach wyszukiwania (miejsce pochodzenia, cel podróży, daty, liczba podróżnych itp.). Pewne dodatkowe funkcjonalności udostępniane są w wyszukiwarce, gdy użytkownik jest połączony ze swoim Kontem. BlaBlaCar zachęca Użytkownika, niezależnie od stosowanego procesu rezerwacji, do dokładnego przeanalizowania oferty i skorzystania z wyszukiwarki w celu ustalenia oferty najbardziej dopasowanej do jego potrzeb. Więcej informacji można znaleźć [tutaj](https://blog.blablacar.pl/about-us/transparentnosc-platform). Klient, rezerwując Przejazd autobusem offline poza Platformą, może również zwrócić się do Przewoźnika lub przedstawiciela obsługi klienta o przeprowadzenie wyszukiwania.

**4.2.1. Wspólny Przejazd**

Kiedy Pasażer jest zainteresowany w zamieszczeniu Ogłoszenia o wspólnym przejeździe, może przesłać za pośrednictwem sieci zapytanie dotyczące Rezerwacji. Zapytanie dotyczące Rezerwacji może (i) zostać zaakceptowane automatycznie (jeżeli Kierowca wybierze tę opcję podczas zamieszczania swojego Ogłoszenia o wspólnym przejeździe) lub (ii) zostać zaakceptowane ręcznie przez Kierowcę. W momencie Rezerwacji Pasażer dokonuje płatności Opłaty serwisowej, o ile ma to zastosowanie. Po zatwierdzeniu zapytania dotyczącego Rezerwacji przez Kierowcę – jeżeli dotyczy – Pasażer otrzymuje Potwierdzenie Rezerwacji (dalej “Potwierdzenie Rezerwacji”).

Jeżeli użytkownik jest Kierowcą i w chwili zamieszczenia Ogłoszenia o wspólnym przejeździe wybrał opcję akceptacji ręcznej zapytań dotyczących Rezerwacji, jest on zobowiązany do udzielania odpowiedzi na wszystkie zapytania dotyczące Rezerwacji w terminie określonym przez Pasażera w jego zapytaniu. W przeciwnym wypadku, zapytanie dotyczące Rezerwacji automatycznie wygaśnie, a Pasażerowi zwrócone zostaną wszystkie kwoty zapłacone w związku z tym zapytaniem, jeżeli istnieją.

W chwili Potwierdzenia Rezerwacji, BlaBlaCar ponownie prześle Użytkownikowi numer telefonu Kierowcy (w przypadku Użytkownika będącego Pasażerem) lub Pasażera (w przypadku Użytkownika będącego Kierowcą). Użytkownik ponosi wyłączną odpowiedzialność za wypełnienie umowy wiążącej go z innym Użytkownikiem.

**4.2.2. Przejazdy autobusem**

W przypadku Przejazdu autobusem BlaBlaCar umożliwia rezerwację Miejsc na dany Przejazd autobusowy Przewoźnika za pośrednictwem Platformy lub poprzez przekierowanie na Witrynę Internetową Strony Trzeciej. Potwierdzasz, że rezerwacja Miejsc i zakup biletów dla Przejazdów autobusem odbywa się w zależności od dostępności Miejsc.

Usługi transportowe podlegają Ogólnemu Regulaminowi Sprzedaży odpowiedniego Przewoźnika, w zależności od wybranego przez Klienta Przejazdu, który Klient musi przeczytać i zaakceptować przed dokonaniem rezerwacji i opłaceniem Miejsca. BlaBlaCar nie świadczy żadnych usług transportowych w odniesieniu do Przejazdów autobusem, Przewoźnicy są wyłącznie stroną Ogólnego Regulaminu Sprzedaży. Prosimy zapytać stosownego Przewoźnika o Ogólny Regulamin Sprzedaży, w szczególności w odniesieniu do przewozu osób niepełnoletnich i bagażu oraz odwołania Przejazdu autobusem.

Ogólny Regulamin Sprzedaży jest przedstawiany przez Przewoźników, w tym za pośrednictwem ich stron internetowych. Warunki anulowania Biletu autobusowego mogą być również prezentowane na Platformie. Wszelkie informacje o Przejazdach Autobusem, publikowane na Platformie, w tym dostępność, trasy, rozkłady jazdy i Ceny, pochodzą od Przewoźników. BlaBlaCar nie zmienia tych informacji ani nie ponosi odpowiedzialności za ich jakość.

BlaBlaCar zwraca uwagę na fakt, że niektóre Usługi transportowe oferowane przez Przewoźnika i wymienione na Platformie mogą zostać wycofane, w szczególności z powodów klimatycznych, sezonowego charakteru lub w przypadku wystąpienia siły wyższej.

Aby zarezerwować Miejsce na Przejazd autobusem i kupić Bilet autobusowy, niezależnie od tego, czy jesteś zalogowany na swoje Konto na Platformie, czy nie (w stosownych przypadkach), musisz podać stosowne dane osobowe zgodnie z naszą [Polityką prywatności](https://blog.blablacar.pl/about-us/privacy-policy). Należy pamiętać, że Polityka prywatności Przewoźnika, za pomocą której podróżujesz, może mieć również zastosowanie. BlaBlaCar nie zmienia tych informacji ani nie ponosi odpowiedzialności za niedokładność danych wprowadzonych podczas procesu rezerwacji Przejazdu autobusem.

Bilet autobusowy jest rezerwowany, a Miejsce jest rezerwowane (jeśli pozwala na to wybór miejsc) po zakończeniu płatności. Jeśli płatność nie zostanie dokonana, Bilet autobusowy zostanie ponownie wystawiony na sprzedaż, a Miejsce może zostać zarezerwowane przez innego Pasażera.

Po zakończeniu zakupu otrzymasz wiadomość e-mail z kopią Biletu autobusowego, potwierdzeniem rezerwacji i szczegółowymi informacjami na temat Przejazdu autobusem. Zachęcamy do sprawdzenia ustawień skrzynki odbiorczej swojego adresu e-mail, a w szczególności do upewnienia się, że e-mail potwierdzający nie trafia bezpośrednio do spamu.

Kwestie organizacji Przejazdu autobusem i realizacji umowy przewozu są wyłączną odpowiedzialnością Przewoźnika i muszą być rozwiązywane bezpośrednio z Przewoźnikiem. BlaBlaCar nie jest stroną jakichkolwiek umów pomiędzy Klientem, Pasażerem a Przewoźnikiem.

**4.2.3. Imienny charakter rezerwacji Miejsca i warunki korzystania z Usług na rzecz osób trzecich**

Korzystanie z Usług – zarówno w charakterze Pasażera, jak i Kierowcy – ma charakter imienny. Dane Kierowcy i Pasażera muszą odpowiadać informacji dotyczących ich tożsamości, przekazanych BlaBlaCar, Przewoźnikom (jeśli dotyczy) oraz innym uczestniczącym w Przejeździe Użytkownikom.

Niemniej jednak, w odniesieniu do Wspólnych Przejazdów, BlaBlaCar umożliwia Użytkownikom zarezerwowanie jednego lub większej liczby Miejsc na rzecz osób trzecich. W takim przypadku Użytkownik zobowiązuje się do precyzyjnego przekazania Kierowcy w chwili dokonywania Rezerwacji lub przesyłania wiadomości do Kierowcy (w razie Przejazdu bez Rezerwacji), imię i nazwiska, wiek oraz numeru telefonu osoby, na rzecz której dokonuje rezerwacji Miejsca. Absolutnie zabronione jest zarezerwowanie Miejsca do Wspólnego Przejazdu dla podróżującej samotnie osoby niepełnoletniej w wieku poniżej 13 lat. W przypadku rezerwacji Miejsca do Wspólnego Przejazdu dla podróżującej samotnie osoby niepełnoletniej w wieku powyżej 13 lat, Użytkownik zobowiązuje się do uzyskania wcześniejszej zgody Kierowcy oraz przekazania mu prawidłowo sporządzonego upoważnienia, podpisanego przez opiekunów prawnych osoby niepełnoletniej.

Ponadto, przedmiotem działalności Platformy jest dokonywanie rezerwacji Miejsc jedynie dla osób. Absolutnie zabronione jest dokonywanie rezerwacji Miejsca w celu przewozu jakichkolwiek przedmiotów, przesyłek, zwierząt podróżujących bez opieki i dowolnych materiałów.

Ponadto zabronione jest zamieszczenie Ogłoszenia w imieniu i na rzecz innego Kierowcy.

Odnośnie Przejazdów autobusem zachęcamy do uwzględnienia Ogólnego Regulaminu Sprzedaży danego Przewoźnika. Jeśli rezerwujesz Przejazd autobusem dla osoby trzeciej, bierzesz pełną odpowiedzialność za posiadanie odpowiednich uprawnień do wykonywania działań w imieniu i na rzecz takiej osoby trzeciej.

Należy pamiętać, że dzieci w wieku poniżej 16 lat mogą dokonywać Przejazdy autobusem wyłącznie pod warunkiem, że ich rodzice lub przedstawiciele prawni posiadają odpowiednie uprawnienia, udzielają im pomocy i wsparcia zgodnie z obowiązującym prawem. Rodzice lub inni przedstawiciele prawni ponoszą odpowiedzialność za wszelkie działania osób niepełnoletnich podczas korzystania z Platformy i Przejazdów ajutobusem, w tym za wszelkie nielegalne działania i / lub zaniechania ze strony osób niepełnoletnich podczas korzystania z Platformy.

W przypadku rezerwacji Miejsca na Przejazd autobusowy dla osoby poniżej 16 roku życia ponosisz wyłączną odpowiedzialność za zapewnienie tego, żeby osoba niepełnoletnia posiadała odpowiednie uprawnienia do reprezentowania takiej osoby i jej towarzyszenia , do uzyskiwania i udzielania niezbędnych zezwoleń i zgody oraz wykonywania innych obowiązków zgodnie z obowiązującym prawem.

Przed zakupem Biletu autobusowego zalecamy zapoznanie się z Ogólnym Regulaminem Sprzedaży Przewoźnika dotyczącym przewozu osób niepełnoletnich.

### **4.3. Treści Użytkownika, moderacja i system ocen**

**4.3.1. Zasady działania systemu ocen**

BlaBlaCar zachęca użytkowników do zamieszczania recenzji dotyczących Kierowców (w przypadku, jeżeli Użytkownik jest Pasażerem) lub dotyczących Pasażerów (w przypadku, jeżeli Użytkownik jest Kierowcą), z którymi korzystał z Wspólnych Przejazdów bądź miał korzystać z Przejazdów (tzn. zarezerwował Wspólny Przejazd. Niemniej jednak, Użytkownik nie może zamieszczać recenzji dotyczących innych Pasażerów, jeżeli sam jest Pasażerem lub na temat Użytkowników, z którymi nie podróżował lub nie miał podróżować. Masz możliwość wystawienia opinii w ciągu 14 od Przejazdu.

Recenzja zamieszczona przez Użytkownika lub dotycząca go recenzja zamieszczona przez innego Użytkownika – jeżeli dotyczy – jest widoczna iI publikowana na Platformie w krótszym z następujących okresów: (i) natychmiast po zamieszczeniu recenzji przez osoby wymienione powyżej lub (ii) po upływie 14 dni od pierwszej recenzji.

W niektórych przypadkach Użytkownik może udzielić odpowiedzi na recenzję zamieszczoną przez innegorofileik anaa jegorofileu w ciągu 14 dni od otrzymania takiej recenzji. Recenzja oraz odpowiedź Użytkownika zostaną zamieszczone na jegorofileu.

**4.3.2. Moderacja**

**4.3.2.1. Treści Użytkownika**

Użytkownik uznaje i wyraża zgodę na fakt, że BlaBlaCar może dokonywać moderacji przed publikacją, przy użyciu narzędzi automatycznych lub ręcznie, Treści Użytkownika, o których mowa w Artykule 11.2. Jeżeli BlaBlaCar uzna takie Treści Użytkownika za naruszające właściwy przepisy obowiązującego prawa lub niniejszym Regulamin, zastrzega sobie prawo do:

* uniemożliwienia publikacji lub usunięcia takich Treści Użytkownika
* wystosowania ostrzeżenia do Użytkownika z przypomnieniem o obowiązku przestrzegania właściwych przepisów obowiązującego prawa lub niniejszego Regulaminu i/lub
* zastosowania środków restrykcyjnych, o których mowa w Artykule 9 niniejszego Regulaminu.

Stosowanie przez BlaBlaCar takich automatycznych lub ręcznych narzędzi moderacji nie będzie interpretowane jako zobowiązanie do monitorowania ani zobowiązanie do aktywnego poszukiwania wszelkich przypadków czynności niezgodnych z prawem i/lub Treści Użytkownika publikowanych na Platformie. Nie będzie również, w zakresie dozwolonym przez właściwe przepisy obowiązującego prawa, stanowić podstawy do jakiejkolwiek odpowiedzialności ani zobowiązań BlaBlaCar z powyższego tytułu.

**4.3.2.2. Wiadomości**

Wymiana wiadomości między Użytkownikami za pośrednictwem naszej Platformy („Wiadomości”) odbywa się wyłącznie w celu wymiany informacji dotyczących Wspólnych Przejazdów.

BlaBlaCar może, przy użyciu zautomatyzowanego oprogramowania i algorytmów, wykrywać treści Wiadomości na potrzeby zapobiegania oszustwom, usprawnienia poziomu usług, wsparcia klienta oraz wykonywania umów zawartych z naszymi użytkownikami (takich jak niniejszy Regulamin). W razie wykrycia wszelkich treści w Wiadomości, które noszą znamiona oszustwa, działania niezgodnego z prawem lub działania służącego obejściu Platformy bądź są w inny sposób niezgodne z niniejszym Regulaminem:

* takie treści nie mogą zostać opublikowane
* Użytkownik, który wysłał Wiadomość, może dostać ostrzeżenie z przypomnieniem, że ma obowiązek przestrzegać właściwych przepisów obowiązującego prawa lub niniejszego Regulaminu lub
* konto Użytkownika może podlegać zawieszeniu w trybie Artykułu 9 niniejszego Regulaminu.

Stosowanie przez BlaBlaCar takich automatycznych lub ręcznych narzędzi moderacji nie będzie interpretowane jako zobowiązanie do monitorowania ani zobowiązanie do aktywnego poszukiwania wszelkich przypadków czynności niezgodnych z prawem i/lub Treści Użytkownika publikowanych na Platformie.  Nie będzie również, w zakresie dozwolonym przez właściwe przepisy obowiązującego prawa, stanowić podstawy do jakiejkolwiek odpowiedzialności ani zobowiązań BlaBlaCar z powyższego tytułu.

Użytkownik uznaje i wyraża zgodę na fakt, że BlaBlaCar zastrzega sobie prawo niezamieszczenia lub usunięcia dowolnych recenzji, pytań, komentarzy lub odpowiedzi, które uzna za niezgodne z postanowieniami niniejszego Regulaminu.

**4.3.3. Ograniczenie**

Zgodnie z Artykułem 9 niniejszego Regulaminu BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub rozwiązania stosunku prawnego obowiązującego na podstawie niniejszego Regulaminu w przypadku, jeśli (i) Użytkownik otrzymał co najmniej trzy recenzje i (ii) jedna lub średnia ocena uzyskana w ramach tych recenzji jest niższa lub równa 3 w zależności od powagi opinii pozostawionej w tym trybie przez Użytkownika. 

**5\. Warunki finansowe**
-------------------------

### **5.1. Udział w Kosztach i Cenie**

5.1.1 W przypadku Wspólnego przejazdu wartość Udziału w Kosztach jest określona przez Użytkownika będącego Kierowcą, na jego wyłączną odpowiedzialność. Absolutnie zabronione jest uzyskiwanie jakichkolwiek zysków z używania naszej Platformy. W związku z powyższym, Użytkownik zobowiązuje się do ograniczenia kwoty Udziału w Kosztach, której żąda od swoich Pasażerów do wartości kosztów rzeczywiście ponoszonych w związku z Przejazdem. W przeciwnym przypadku, Użytkownik ponosi we własnym zakresie pełne ryzyko w odniesieniu do zmiany kwalifikacji podatkowej transakcji zrealizowanej za pośrednictwem Platformy.

W chwili zamieszczenia Ogłoszenia przez Użytkownika, BlaBlaCar zasugeruje kwotę Udziału w Kosztach, przy uwzględnieniu rodzaju Przejazdu oraz jego odległości. Kwota ta stanowi wyłącznie sugestię, a Użytkownik może zwiększyć lub zmniejszyć ją na podstawie rzeczywistych kosztów ponoszonych w związku z Przejazdem. Aby uniknąć możliwości nadużyć, BlaBlaCar zastrzega sobie prawo modyfikacji wartości Udziału w Kosztach.

5.1.2 W przypadku Przejazdu autobusem Cenę za Miejsce określa Przewoźnik. Klient jest proszony o zapoznanie się z odpowiednim Ogólnym Regulaminem Sprzedaży w celu zrozumienia obowiązujących zasad dotyczących składania zamówienia Biletów.

### **5.2. Opłata serwisowa**

BlaBlaCar w zamian za korzystanie z Platformy w momencie Rezerwacji pobiera opłaty serwisowe (zwane dalej „**Opłatą serwisową**”). W razie konieczności użytkownik zostanie poinformowany przed zastosowaniem Opłaty serwisowej.

Sposób płatności Opłaty serwisowej może się różnić w zależności od używanej platformy BlaBlaCar. Dozwolone sposoby płatności Opłaty serwisowej są następujące: 

* Karta kredytowa dostarczona przez Adyen NV, spółkę publiczną zarejestrowaną w Holandii pod numerem 34259528 z siedzibą pod adresem Simon Carmiggeltstraat 6-50, 1011 DJ, Amsterdam, Holandia, lub
* Blik, dostarczony przez spółkę Polski Standard Płatności sp. z o.o. z siedzibą w Warszawie ul. Cypryjska 72, wpisaną do rejestru przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XIII Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS 0000493783, NIP: 5213664494, Statystyczny Numer Identyfikacyjny (REGON): 147055889.

Metody naliczania Opłaty serwisowej mogą zostać opublikowane przez BlaBlaCar odrębnie, mają charakter wyłącznie informacyjny i nie mają wartości umownej. Opłata serwisowa może być naliczana w zależności od różnych czynników, w szczególności długości Podróży i Udziału w kosztach. BlaBlaCar zastrzega sobie prawo do zmiany sposobu naliczania Opłaty serwisowej w dowolnym momencie. Zmiany te nie będą miały wpływu na Opłatę serwisową zaakceptowaną przez Użytkownika przed datą wejścia w życie tych zmian.

W przypadku podróży transgranicznych należy pamiętać, że metody obliczania wysokości Opłaty serwisowej oraz obowiązującego podatku VAT różnią się w zależności od miejsca odbioru i/lub celu podróży.

W przypadku korzystania z Platformy w podróżach transgranicznych lub poza granicami Polski, Opłata serwisowa może zostać pobrana przez spółkę stowarzyszoną BlaBlaCar obsługującą lokalną platformę.

### **5.3. Zaokrąglenia**

BlaBlaCar może w zależności wyłącznie od własnej decyzji zaokrąglać kwoty Udziału w Kosztach lub Opłaty serwisowej w górę lub w dół.

### **5.4. Sposoby płatności Udziału w kosztach na rzecz Kierowcy i Ceny**

Należy zwrócić uwagę, że dostawcy usług płatniczych mogą nakładać procedury i ograniczenia na niektóre transakcje ze względu na ich zobowiązania dotyczące zgodności zgodnie z własnymi regulaminami.

**5.4.1. Płatność Udziału w kosztach na rzecz Kierowcy**

Pasażer zobowiązuje się do zapłacenia Kierowcy Udziału w Kosztach najpóźniej w chwili opuszczania jego pojazdu w miejscu docelowym.

Kierowca nie może domagać się zapłaty Udziału w Kosztach w całości lub w części przed realizacją Przejazdu.

**5.4.2 Płatność Ceny** 

Płatność Ceny za każdą rezerwację Przejazdu autobusem dokonana za pośrednictwem Platformy jest dokonywana za pomocą jednego z dozwolonych poniżej sposobów:

* Karta kredytowa dostarczona przez Adyen NV, spółkę publiczną zarejestrowaną w Holandii pod numerem 34259528 z siedzibą pod adresem Simon Carmiggeltstraat 6-50, 1011 DJ, Amsterdam, Holandia, lub
* Blik, dostarczony przez spółkę Polski Standard Płatności sp. z o.o. z siedzibą w Warszawie ul. Cypryjska 72, wpisaną do rejestru przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XIII Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS 0000493783, NIP: 5213664494, Statystyczny Numer Identyfikacyjny (REGON): 147055889.

Sposób płatności może się różnić w zależności od używanej platformy BlaBlaCar.

Kupując Miejsce na Przejazd autobusowy za pośrednictwem Platformy, płacisz Cenę stosownego odpowiedzialnego Przewoźnika, biorąc pod uwagę obowiązujące podatki i opłaty (jeżeli są pobierane).

Aby dokonać płatności, możesz zostać przekierowany na stronę zewnętrznego dostawcy usług płatniczych — partnera BlaBlaCar. BlaBlaCar w żadnym wypadku nie może zagwarantować, że usługi świadczone przez firmę odpowiedzialną za dokonywanie płatności będą świadczone bez błędów, zakłóceń, awarii, opóźnień lub innych wad. BlaBlaCar nie ponosi żadnej odpowiedzialności za zarządzanie, pośrednictwo lub dokonywanie płatności. W każdym przypadku BlaBlaCar nie ponosi odpowiedzialności za transakcje dokonywane między Użytkownikiem, Klientem i Przewoźnikami.

Bilet autobusowy jest wydawany Użytkownikowi tylko wtedy, gdy płatność za Cenę jest pełna, zakończona i skuteczna. Płatność musi zostać dokonana nie później niż w terminie wskazanym na Platformie. Jeśli Płatność nie zostanie dokonana w ramach wskazanego terminu, jest nieprawidłowa, niekompletna lub nie została dokonana z przyczyn leżących po stronie Klienta, rezerwacja Przejazdu Autobusowego zostanie automatycznie anulowana.

Płatność Ceny jest ostateczna. W związku z tym wszelkie zmiany spowodują wymianę lub anulowanie zgodnie z obowiązującym Ogólnym Regulaminem Sprzedaży Przewoźnika. Obowiązkiem Klienta jest dopilnowanie, aby Usługi transportowe zostały wybrane zgodnie z potrzebami i oczekiwaniami Pasażera. Za dokładność danych osobowych wprowadzonych przez Klienta odpowiada ten ostatni, z wyjątkiem przypadku, gdy zostanie udowodnione, że BlaBlaCar nie gromadzi, nie przechowuje ani nie chroni takich Danych osobowych.

Ceny mogą zostać zmienione przez Przewoźników. Od czasu do czasu na Platformie mogą być wyświetlane promocje lub oferty specjalne do tyczące Cen. Odnośnie takich promocji i ofert mogą obowiązywać dodatkowe warunki.

**6\. Niehandlowy i niebiznesowy charakter Usług oraz Platformy**
-----------------------------------------------------------------

Użytkownik zobowiązuje się do korzystania z Usług oraz Platformy jedynie w celu nawiązania kontaktów na zasadach niehandlowych i niebiznesowych z osobami zamierzającymi skorzystać ze wspólnego Przejazdu.

W kontekście Wspólnego Przejazdu przyjmujesz do wiadomości, że prawa konsumenckie wynikające z unijnego prawa ochrony konsumentów nie mają zastosowania do Twoich relacji z innymi Członkami.

Użytkownik będący Kierowcą zobowiązuje się, że nie będzie żądał Udziału w Kosztach w kwocie wyższej, niż rzeczywiście poniesione koszty jak również w celu uzyskiwania jakichkolwiek zysków, a ponadto, na zasadzie wspólnego ponoszenia kosztów, Kierowca musi ponieść własną część kosztów związanych z Przejazdem. Kierowca ponosi wyłączną odpowiedzialność za obliczenie kosztów ponoszonych w ramach Przejazdu oraz sprawdzenie, czy kwota wymagana od Pasażerów nie przekracza rzeczywiście ponoszonych kosztów (przy uwzględnieniu udziału własnego użytkownika).

BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika w przypadku, jeżeli wykorzystuje samochód firmowy, służbowy lub taksówkę generując w ten sposób zysk za pośrednictwem Platformy. Użytkownik zobowiązuje się do przedstawienia firmie BlaBlaCar na każde jej żądanie kopii dowodu rejestracyjnego jego samochodu i/lub dowolnego innego dokumentu potwierdzającego, że jest on uprawniony do korzystania z danego pojazdu na Platformie i nie uzyskuje w ten sposób jakichkolwiek zysków.

Zgodnie z Artykułem 9 niniejszego Regulaminu BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub zakończenia obowiązywania niniejszego Regulaminu w przypadku prowadzenia przez Użytkownika na Platformie jakiejkolwiek działalności, która – ze względu na charakter oferowanych Przejazdów, ich częstotliwość, liczbę przewożonych Pasażerów oraz żądany Udział w Kosztach – umożliwia Użytkownikowi uzyskiwanie zysków lub na podstawie których BlaBlaCar może podejrzewać, że Użytkownik uzyskuje zyski za pośrednictwem Platformy.

**7\. Polityka dotycząca anulowania**
-------------------------------------

### **7.1. Warunki zwrotu środków w przypadku rezygnacji**

Przedmiotem niniejszych zasad anulowania są wyłącznie Wspólne Przejazdy; BlaBlaCar nie udziela żadnej gwarancji, jakiejkolwiek natury, w przypadku anulowania z jakiegokolwiek powodu.

Anulowanie Miejsca na Wspólny przejazd przez Kierowcę lub Pasażera po Potwierdzeniu Rezerwacji podlega poniższym postanowieniom:

– W przypadku rezygnacji z winy Kierowcy, Pasażerowi zwracana jest Opłata serwisowa. Dzieje się tak zwłaszcza w przypadku, gdy Kierowca:

* Nie potwierdził prośby o Rezerwację w wyznaczonym terminie (jeżeli taka opcja została wybrana przez Kierowcę);
* Anuluje Wspólny przejazd lub nie przybył na miejsce spotkania w ciągu 15 minut od uzgodnionej godziny;

\-W przypadku odwołania z winy Pasażera Opłatę serwisową zatrzymuje BlaBlaCar

Sposób zwrotu Opłaty serwisowej ustalany jest w zależności od sposobu uiszczenia Opłaty Serwisowej, natomiast termin zwrotu Opłaty serwisowej uzależniony jest od terminów przelewów dokonywanych przez instytucje bankowe.

Jeżeli odwołanie nastąpi przed odjazdem i z winy Pasażera, Miejsce (Miejsca) odwołane przez Pasażera zostaną automatycznie udostępnione innym Pasażerom, którzy mogą dokonać rezerwacji online i odpowiednio podlegają warunkom niniejszego Regulaminu.

BlaBlaCar ocenia, według własnego uznania, na podstawie dostępnych informacji, zasadność wniosków o zwrot środków.

### **7.2. Przejazdy autobusowe**

Procedura dotycząca anulowania Przejazdu Autobusem wynika z Ogólnego Regulaminu Sprzedaży danego Przewoźnika. Możesz anulować lub zmienić rezerwację Przejazdu autobusem na Platformie lub na Witrynie Internetowej Osoby Trzeciej, w zależności od okoliczności.

Środki zostaną zwrócone zgodnie z metodą płatności stosowaną przy zakupie biletu na Przejazd autobusem w terminie określonym przez Twoj bank.

Należy pamiętać, że opłaty za wprowadzenie zmian lub anulowanie rezerwacji Przejazdu autobusowego mogą być naliczane zgodnie z obowiązującym prawem i Ogólnym Regulaminem Sprzedaży danego Przewoźnika i mogą być odliczane od zapłaconej Ceny. Wymienione zwroty środków (jeżeli należne) oraz Ogólny Regulamin Sprzedaży może być wskazany na Platformie, Witrynie internetowej Osoby Trzeciej (w zależności od okoliczności), lub w kopii Biletu autobusowego przesłanej do Ciebie pocztą elektroniczną.

W odpowiednich przypadkach możesz zwrócić Bilet autobusowy:

(i) na swoim Koncie lub 

(ii) klikając link w wiadomości e-mail z twoim potwierdzeniem rezerwacji Biletu autobusowego.

### **7.3. Prawo do odstąpienia od umowy**

Akceptując niniejszy Regulamin, wyraźnie akceptujesz, że umowa pomiędzy Tobą a BlaBlaCar, polegająca na powiązaniu z innym Członkiem, ma zostać zawarta przed upływem terminu odstąpienia od Potwierdzenia Rezerwacji i wyraźnie zrzekasz się prawa do odstąpienia od umowy, zgodnie z postanowieniami art. 27 Ustawy z dnia 30 maja 2014 r. O prawach konsumenta.

**8\. Zachowanie użytkowników Platformy i jej Członków**
--------------------------------------------------------

### **8.1. Zobowiązania wszystkich użytkowników Platformy**

Użytkownik ponosi wyłączną odpowiedzialność za przestrzeganie wszystkich przepisów prawnych, regulacji i obowiązków dotyczących korzystania z Platformy.

Ponadto, w odniesieniu do korzystania z Platformy i podczas Przejazdów, Użytkownik zobowiązuje się, że:

(i) nie będzie wykorzystywał Platformy do jakichkolwiek celów zawodowych, handlowych lub umożliwiających uzyskiwanie zysków;

(ii) nie będzie przekazywał BlaBlaCar (w szczególności podczas tworzenia lub aktualizacji swojego Konta) lub innym Użytkownikom jakichkolwiek informacji nieprawdziwych, wprowadzających w błąd bądź mających na celu popełnienie oszustwa;

(iii) nie będzie zamieszczał na Platformie jakichkolwiek wypowiedzi lub postów (w tym Wiadomości) zawierających treści o charakterze oszczerczym, obraźliwym, obscenicznym, pornograficznym, wulgarnym, agresywnym, niedozwolonym, gwałtownym, stanowiącym groźby lub przypadki molestowania, o charakterze rasistowskim bądź ksenofobicznym, posiadających podłoże seksualne, nakłaniające do przemocy, dyskryminacji bądź nienawiści, zachęcającym do korzystania z substancji niedozwolonych, a w ujęciu bardziej ogólnym niezgodnych z prawem, niezgodnych z właściwymi przepisami obowiązującego prawa oraz niniejszym Regulaminem oraz celami działalności Platformy oraz mogącymi przynosić szkody dla BlaBlaCar, bądź jakichkolwiek stron trzecich, a także niezgodnych z dobrymi obyczajami.

(iv) nie będzie naruszał jakichkolwiek praw BlaBlaCar oraz jej wizerunku, a w szczególności jej praw własności intelektualnej;

(v) nie będzie otwierał więcej, niż jednego Konta na Platformie oraz nie będzie otwierał Konta w imieniu jakiejkolwiek innej osoby;

(vi) nie będzie podejmował jakichkolwiek prób obchodzenia dostępnego na Platformie internetowego systemu rezerwacji, w szczególności poprzez przesyłanie innym Użytkownikom swoich danych kontaktowych w celu przeprowadzania rezerwacji poza Platformą i niepłacenia Opłaty Serwisowej;

(vii) nie będzie kontaktował się z innymi Użytkownikami, w szczególności za pośrednictwem Platformy, w żadnych innych celach, niż określenie warunków wspólnego wykorzystywania samochodów;

(viii) nie będzie przyjmował lub dokonywał jakichkolwiek płatności poza Platformą, za wyjątkiem przypadku dozwolonych postanowieniami niniejszego Regulaminu;

(ix) będzie przestrzegał wszystkich postanowień niniejszego Regulaminu.

### **8.2. Zobowiązania Kierowców**

Ponadto, korzystając z Platformy jako Kierowca, Użytkownik zobowiązuje się do:

(i) przestrzegania wszystkich przepisów, regulacji i kodeksów obowiązujących w odniesieniu do prowadzenia pojazdów, a w szczególności posiadania ubezpieczenia od odpowiedzialności cywilnej, obowiązującego na dzień Przejazdu oraz ważnego prawa jazdy;

(ii) upewnienia się, że warunki posiadanego ubezpieczenia obejmują wspólne wykorzystywanie pojazdu, a jego Pasażerowie są uważani za strony trzecie przebywające w pojeździe i w związku z tym są objęci ochroną ubezpieczeniową;

(iii) niepodejmowania jakiegokolwiek ryzyka podczas jazdy oraz niezażywania żadnych produktów, które mogłyby naruszać jego zdolność do zachowania koncentracji oraz prowadzenia pojazdu w sposób uważny i całkowicie bezpieczny;

(iv) zamieszczania Ogłoszeń o wspólnym przejeździe odpowiadających wyłącznie rzeczywiście planowanym Wspólnym Przejazdom;

(v) realizacji Wspólnego Przejazdu w sposób określony w Ogłoszeniu o wspólnym przejeździe (w szczególności w odniesieniu do korzystania z autostrad lub nie) oraz przestrzegania terminów i miejsc uzgodnionych z innymi Użytkownikami Platformy (w szczególności w zakresie miejsc spotkań oraz zakończenia Wspólnych Przejazdów);

(vi) niezabierania większej liczby Pasażerów, niż wynosi określona w Ogłoszeniu o wspólnym przejeździe liczba Miejsc;

(vii) korzystania z pojazdu znajdującego się w dobrym stanie, spełniającego wszystkie obowiązujące przepisy prawa i praktyki, a w szczególności posiadającego ważny certyfikat kontroli technicznej (potwierdzający, że pojazd nadaje się do jazdy).

(viii) przekazania firmie BlaBlaCar lub każdemu Pasażerowi, który tego zażąda, informacji dotyczących jego prawa jazdy, dowodu rejestracyjnego samochodu, ubezpieczenia, certyfikatu kontroli technicznej oraz wszelkich innych dokumentów potwierdzających jego uprawnienia do prowadzenia pojazdu jako Kierowca zarejestrowany na Platformie;

(ix) w razie opóźnienia lub zmiany terminu Wspólnego Przejazdu, do natychmiastowego zawiadomienia o tym swoich Pasażerów;

(x) w razie Wspólnego Przejazdu międzynarodowego, posiadania i przedstawienia w razie konieczności Pasażerom lub uprawnionym władzom, które mogą tego zażądać, wszelkich dokumentów wymaganych w celu kontroli jego tożsamości oraz praw przekroczenia granicy;

(xi) oczekiwania na Pasażerów w uzgodnionym miejscu spotkania przez co najmniej 15 minut po uzgodnionej godzinie;

(xii) niezamieszczania jakichkolwiek Ogłoszeń o Wspólnym przejeździe dotyczących pojazdów, których nie jest właścicielem lub których nie może wykorzystywać dla celów Wspólnych Przejazdów;

(xiii) zapewnienia możliwości kontaktu ze strony Pasażerów za pośrednictwem numeru telefonu zamieszczonego na jego profilu;

(xiv) niewymagania żadnego kodu zatwierdzenia przed wysadzeniem Pasażera;

(xv) nieuzyskiwania jakichkolwiek zysków za pośrednictwem Platformy;

(xvi) wykazania, że nie ma żadnych przeciwwskazań medycznych w odniesieniu do prowadzenia pojazdów;

(xvii) zachowywania się podczas Wspólnego Przejazdu w sposób właściwy i odpowiedzialny, zgodnie z duchem wspólnych przejazdów;

(xviii) nieodrzucania jakichkolwiek Rezerwacji na podstawie kryteriów dotyczących rasy, koloru skóry, pochodzenia etnicznego lub narodowościowego, wyznania, orientacji seksualnej, stanu cywilnego, niepełnosprawności, wyglądu fizycznego, ciąży, niekorzystnej sytuacji finansowej, nazwiska, miejsca zamieszkania, stanu zdrowia, przekonań politycznych lub wieku.

### **8.3. Zobowiązania Pasażerów**

**8.3.1. Wspólne przejazdy**

Korzystając z Platformy jako Pasażer, Użytkownik zobowiązuje się do:

(i) właściwego zachowania podczas Wspólnego Przejazdu, nie powodując jakiegokolwiek rozproszenia uwagi Kierowcy lub zakłócenia prowadzenia przez niego pojazdu, a także spokoju pozostałych Pasażerów;

(ii) poszanowania pojazdu Kierowcy i zachowania jego czystości;

(iii) w razie opóźnienia, natychmiastowego zawiadomienia o tym Kierowcy;

(iv) zapłaty Kierowcy uzgodnionej kwoty Udziału w Kosztach;

(v) oczekiwania na Kierowcę w uzgodnionym miejscu spotkania przez co najmniej 15 minut po uzgodnionej godzinie;

(vi) okazania firmie BlaBlaCar lub dowolnemu Kierowcy, który tego zażąda, swojego dowodu osobistego lub jakiegokolwiek innego dokumentu potwierdzającego jego tożsamość;

(vii) nieprzewożenia podczas Wspólnego Przejazdu jakichkolwiek produktów, towarów, substancji lub zwierząt, które mogłyby powodować zakłócenie prowadzenia pojazdu lub rozproszenie uwagi Kierowcy, bądź też których posiadanie lub przewożenie jest niezgodne z obowiązującymi przepisami;

(viii) w razie Wspólnego Przejazdu międzynarodowego, posiadania i przedstawienia w razie konieczności Kierowcy lub uprawnionym władzom, które mogą tego zażądać, wszelkich dokumentów wymaganych w celu kontroli jego tożsamości oraz praw przekroczenia granicy;

(ix) zapewnienia możliwości kontaktu ze strony Pasażerów za pośrednictwem telefonu na numer zamieszczony na jego profilu, w tym także w uzgodnionym miejscu spotkania;

W przypadku, jeżeli Użytkownik dokonał Rezerwacji jednego lub większej liczby Miejsc na rzecz osób trzecich w sposób zgodny z postanowieniami Artykułu 4.2.3, jest on zobowiązany do zapewnienia przestrzegania niniejszego Artykułu oraz wszystkich innych postanowień tego Regulaminu przez te osoby trzecie. BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub zakończenia obowiązywania niniejszego Regulaminu w przypadku naruszenia jego postanowień przez jakąkolwiek osobę trzecią, na której rzecz Użytkownik dokonał rezerwacji Miejsca w sposób określony w tym Regulaminie. zgodnie z Artykułem 9 niniejszego Regulaminu.

**8.3.2. Przejazdy autobusowe**

Wszystkie kwestie dotyczące korzystania Pasażerów z Usług Transportowych uregulowane są umową pomiędzy Pasażerem a Przewoźnikiem – Ogólnym Regulaminem Sprzedaży. Zasady korzystania z tych usług dostarczone są przez Przewoźników.

### **8.4. Zgłaszanie treści niewłaściwych lub niezgodnych z prawem (Mechanizm zawiadomienia i działania)**

Użytkownik może zgłaszać podejrzane, niewłaściwe lub niezgodne z prawem Treści Użytkownika lub Wiadomości w w trybie omówionym [tutaj](https://support.blablacar.com/s/article/Zg%C5%82aszanie-niedozwolonych-tre%C5%9Bci-1729197120604?language=pl).

BlaBlaCar, po otrzymaniu należytego zawiadomienia w trybie postanowień niniejszego Artykułu lub ze strony właściwych władz, niezwłocznie usunie wszelkie niezgodne z prawem Treści Użytkownika, jeżeli:

* Treści Użytkownika są w sposób oczywisty niezgodne z prawem lub obowiązującymi przepisami; lub 
* BlaBlaCar uzna takie treści za naruszające niniejszy Regulamin.

W takich przypadkach BlaBlaCar zastrzega sobie prawo do usunięcia Treści Użytkownika, które zostały zgłoszone, w trybie wystarczająco szczegółowym i przejrzystym i/lub do zawieszenia w trybie natychmiastowym zgłoszonego Konta.

Odpowiedni Użytkownik może odwoływać się od powyższych decyzji BlaBlaCar w trybie uregulowanym w Artykule 15.1 poniżej.

**9\. Ograniczenia użytkowania Platformy, zawieszenie konta, ograniczenie dostępu i rozwiązanie umowy**
-------------------------------------------------------------------------------------------------------

Użytkownik może rozwiązać umowę z BlaBlaCar w dowolnej chwili i z jakiekolwiek przyczyny, bez konieczności ponoszenia z tego tytułu żadnych kosztów. W tym celu wystarczy wybrać zakładkę “Chcę zamknąć moje konto” na stronie profilu Użytkownika.

W razie (i) naruszenia postanowień niniejszego Regulaminu, a w szczególności, ale nie wyłącznie, zobowiązań Użytkownika określonych postanowieniami Artykułów 6 i 8 powyżej, (ii) przekroczenia ograniczenia określonego w powyższym Artykule 4.3.3 lub (iii) jeżeli BlaBlaCar będzie posiadać uzasadnione podstawy, aby podejrzewać, że jest to konieczne dla zapewnienia jej bezpieczeństwa własnego oraz Użytkowników i osób trzecich, jak również w ramach prowadzonych postępowań wyjaśniających lub w celu zapobieżenia możliwości popełniania oszustw, BlaBlaCar zastrzega sobie prawo do:

(i) zakończenia obowiązywania Regulaminu wiążącego Użytkownika z firmą BlaBlaCar ze skutkiem natychmiastowym bez konieczności przesłania jakiegokolwiek zawiadomienia; i/lub

(ii) uniemożliwienia Użytkownikowi zamieszczania lub usuwania wszelkich Treści Użytkownika opublikowanych na Platformie; i/lub

(iii) ograniczenia dostępu Użytkownika do Platformy i jego możliwości korzystania z niej; i/lub

(iv) tymczasowego lub ostatecznego zawieszenia Konta Użytkownika.

Zawieszenie Konta może także oznaczać, w odpowiednich przypadkach, że Użytkownik nie otrzyma swoich nierozliczonych wypłat.

W zależności od okoliczności BlaBlaCar może też wysyłać użytkownikowi ostrzeżenie z przypomnieniem o zobowiązaniu do przestrzegania właściwych przepisów obowiązującego prawa i/lub niniejszego Regulaminu. 

Jeżeli będzie to konieczne, użytkownik zostanie zawiadomiony o zastosowaniu określonych powyżej środków, aby mógł potwierdzić swoje zobowiązanie do przestrzegania właściwych przepisów obowiązującego prawa i/lub niniejszego Regulaminu, udzielić wyjaśnień BlaBlaCar bądź zakwestionować podjętą decyzję. BlaBlaCar podejmie całkowicie we własnym zakresie oraz z należytym uwzględnieniem wszelkich okoliczności każdego przypadku oraz istotności naruszenia, decyzję dotyczącą ewentualnego anulowania zastosowanych środków.

**10\. Dane osobowe**
---------------------

W związku z wykorzystywaniem Platformy przez Użytkownika, BlaBlaCar będzie pozyskiwać i przetwarzać jego dane osobowe w sposób omówiony w [Polityce Prywatności](https://blog.blablacar.pl/about-us/privacy-policy).

**11\. Prawa własności intelektualnej**
---------------------------------------

### **11.1. Treści publikowane przez firmę BlaBlaCar**

W odniesieniu do wszelkich treści przekazywanych przez Użytkowników Platformy, BlaBlaCar jest jedynym właścicielem wszystkich praw własności intelektualnej do Usługi, Platformy, jej treści (w szczególności tekstów, zdjęć, projektów, logo, wideo, dźwięków, danych, elementów graficznych) oraz oprogramowania i baz danych zapewniających ich działanie.

BlaBlaCar udziela Użytkownikowi niewyłącznego, osobistego i niezbywalnego prawa do korzystania z Platformy i Usług, jedynie do jego własnego użytku osobistego oraz prywatnego, w celach niehandlowych i zgodnie z przedmiotem działania Platformy oraz Usług.

Użytkownik nie może wykorzystywać Platformy i Usług oraz ich treści w jakichkolwiek innych celach bez wcześniejszego uzyskania pisemnej zgody BlaBlaCar. W szczególności, Użytkownik nie ma prawa do:

(i) reprodukowania, modyfikowania, adaptowania, dystrybucji, publicznego przedstawiania i rozpowszechniania Platformy, Usług oraz ich treści, bez wcześniejszego uzyskania wyraźnej zgody BlaBlaCar;

(ii) dekompilacji i przeprowadzania inżynierii wstecznej w odniesieniu do Platformy oraz Usług, za wyjątkiem przypadków określonych obowiązującymi przepisami;

(iii) ekstrakcji lub podejmowania prób ekstrakcji (w szczególności przy wykorzystaniu jakichkolwiek robotów pozyskujących dane lub innych podobnego rodzaju narzędzi wykorzystywanych w celu zbierania danych) istotnej części danych zamieszczonych na Platformie.

### **11.2. Treści zamieszczane przez użytkownika na Platformie**

Aby zapewnić możliwość świadczenia Usług w sposób zgodny z przedmiotem działania Platformy, Użytkownik przyznaje firmie BlaBlaCar niewyłączną licencję dotyczącą wykorzystywania treści i danych przekazywanych przez niego w ramach korzystania z Usług, które mogą obejmować wnioski o Rezerwacje, Ogłoszenia o Wspólnych Przejazdach oraz komentarze, dane biograficzne, zdjęcia, recenzje i odpowiedzi na recenzje (dalej zwane ”Treściami Użytkownika”), a także Wiadomości. Aby umożliwić firmie BlaBlaCar prowadzenie dystrybucji za pośrednictwem sieci cyfrowej i przy wykorzystaniu dowolnych protokołów komunikacyjnych (w szczególności Internetu i sieci mobilnych) oraz publicznego udostępniania treści Platformy, Użytkownik zezwala BlaBlaCar na reprodukowanie, przedstawianie, adaptowanie i tłumaczenie jego Treści Użytkownika na następujących warunkach:

(i) Użytkownik zezwala BlaBlaCar na reprodukowanie jego Treści Użytkownika w całości lub w części przy użyciu wszelkich cyfrowych nośników zapisu danych, zarówno znanych obecnie, jak i nieznanych, a w szczególności na serwerach, twardych dyskach, kartach pamięci lub jakichkolwiek innych nośnikach podobnego rodzaju, w dowolnym formacie i przy użyciu wszystkich procesów, zarówno znanych obecnie, jak i nie, w zakresie niezbędnym dla przechowywania, wykonywania kopii zapasowych, transmisji lub pobierania danych w celach związanych z działalnością Platformy oraz świadczeniem Usług;

(ii) Użytkownik zezwala firmie BlaBlaCar na adaptację i tłumaczenie jego Treści Użytkownika oraz reprodukowanie tych adaptacji na dowolnych nośnikach cyfrowych, zarówno dostępnych obecnie, jak i w przyszłości, o których mowa w powyższym punkcie (i), w celu świadczenia Usług, w szczególności w różnych językach. Prawo to obejmuje między innymi opcję przeprowadzania modyfikacji formatu Treści Użytkownika, z zastrzeżeniem przysługujących mu praw moralnych, w celu zapewnienia przestrzegania karty tożsamości graficznej Platformy i/lub zapewnienia jej kompatybilności technicznej dla umożliwienia publikacji treści na Platformie.

Użytkownik pozostaje odpowiedzialny za oraz posiada wszelkie prawa do Treści Użytkownika oraz Wiadomości publikowanych na naszej Platformie.

**12\. Rola BlaBlaCar**
-----------------------

Platforma stanowi dostępne w sieci Internet narzędzie umożliwiające jej Użytkownikom tworzenie i zamieszczanie Ogłoszeń dotyczących Przejazdów w celu wspólnych przejazdów. Ogłoszenia te mogą być w szczególności wyświetlane przez innych Użytkowników w celu sprawdzenia warunków dotyczących Przejazdu, a także – w przypadkach, których dotyczy – bezpośredniej rezerwacji Miejsca w pojeździe Użytkownika, który zamieścił Ogłoszenie, lub rezerwować Bilety autobusowe na Platformie lub na Witrynie Internetowej Strony Trzeciej.

Korzystając z Platformy i akceptując niniejszy Regulamin, Użytkownik uznaje, że BlaBlaCar nie jest stroną jakiejkolwiek umowy zawieranej przez niego z innymi Użytkownikami w celu wspólnego poniesienia kosztów Przejazdu.

BlaBlaCar nie sprawuje żadnej kontroli zachowania Użytkowników oraz pozostałych użytkowników Platformy, lub Przewoźników i ich agentów. Nie jest właścicielem, nie prowadzi eksploatacji, ani nie zarządza pojazdami stanowiącymi przedmiot Ogłoszeń i nie oferuje żadnych Przejazdów na Platformie.

Użytkownik uznaje, że BlaBlaCar nie kontroluje w żaden sposób prawidłowości i prawdziwości Ogłoszeń, Miejsc i Przejazdów oraz ich zgodności z obowiązującymi przepisami. Działając jedynie jako pośrednik w ramach wspólnych przejazdów, BlaBlaCar nie świadczy żadnych usług transportowych i nie prowadzi działalności Przewoźnika – rola BlaBlaCar jest ograniczona wyłącznie do zapewnienia dostępu do Platformy.

Użytkownicy (Kierowcy lub Pasażerowie) i Przewoźnicy działają wyłącznie na własną odpowiedzialność.

Działając jedynie jako pośrednik, BlaBlaCar nie ponosi żadnej odpowiedzialności za rzeczywistą realizację Przejazdu, a w szczególności za:

(i) zamieszczenie przez Kierowcę lub Przewoźnika w jego Ogłoszeniu nieprawdziwych informacji lub ich przekazanie w jakikolwiek inny sposób w odniesieniu do Przejazdu oraz jego warunków;

(ii) odwołania lub modyfikacji Przejazdu przez Użytkownika;

(iii) brak płatności Udziału w Kosztach przez Pasażera;

(iv) zachowanie Użytkowników podczas Przejazdu, przed nim lub po jego zakończeniu.

W odniesieniu do Przejazdów autobusowych, Użytkownik rozumie, że ponieważ BlaBlaCar nie świadczy usług transportowych, nie zapewnia gwarancji i nie ponosi odpowiedzialności za jakiekolwiek wady usług transportowych świadczonych przez Przewoźników, takie jak opóźnienia, awarie sprzętu, wypadki, jakiekolwiek szkody, straty i szkody lub inne nieprzewidywalne zdarzenia przed, w trakcie lub po Przejeździe autobusem.

**13\. Działalność, dostępność i funkcje Platformy**
----------------------------------------------------

BlaBlaCar dołoży wszelkich starań, aby zapewnić dostępność Platformy przez 7 dni w tygodniu i 24 godziny na dobę. Niemniej jednak, dostęp do Platformy może zostać tymczasowo zawieszony bez jakiegokolwiek wcześniejszego zawiadomienia ze względów dotyczących przeprowadzenia czynności konserwacji technicznej, migracji lub aktualizacji, a także nieprawidłowego działania sieci lub innych uwarunkowań związanych z jej wykorzystywaniem.

Ponadto BlaBlaCar zastrzega sobie prawo do zmiany Platformy lub jej części dla niektórych użytkowników w celu przetestowania nowych funkcji i zapewnienia lepszego doświadczenia użytkownika, a także do modyfikacji lub zawieszenia całości lub części dostępu do Platformy lub jej funkcjonalności, według własnego uznania, tymczasowo lub na stałe.

**14\. Modyfikacja Regulaminu**
-------------------------------

Niniejszy Regulamin oraz dokumenty, do których zawiera odniesienia stanowią całość umowy zawieranej przez Użytkownika z BlaBlaCar w odniesieniu do korzystania z Usług. Jakiekolwiek inne dokumenty, a w szczególności informacje zamieszczane na Platformie (odpowiedzi na często zadawane pytania itd.) mają jedynie znaczenie poglądowe.

BlaBlaCar może zmodyfikować niniejszy Regulamin w celu jego adaptacji do nowych osiągnięć technologicznych oraz handlowych, a także spełnienia wymogów obowiązujących przepisów. Jakiekolwiek modyfikacje niniejszego Regulaminu zostaną opublikowane na Platformie wraz z określeniem terminu ich wejścia w życie, a Użytkownik zostanie o nich powiadomiony przez firmę BlaBlaCar przed tym terminem.

**15\. Prawo właściwe – Rozstrzyganie sporów**
----------------------------------------------

Niniejszy Regulamin został opracowany w języku polskim i podlega prawodawstwu polskiemu.

### **15.1 Wewnętrzny system obsługi reklamacji**

Użytkownik może odwoływać się od naszych decyzji podejmowanych w następującym zakresie:

* Treści Użytkownika: na przykład, usunęliśmy, ograniczyliśmy widoczność lub odmówiliśmy usunięcia wszelkich Treści Użytkownika przekazanych podczas użytkowania Platformy lub
* Konto Użytkownika: zawiesiliśmy dostęp Użytkownika do Platformy,

jeżeli podejmowaliśmy takie decyzje na podstawie faktu, że Treści Użytkownika stanowią treści niezgodne z prawem lub z niniejszym Regulaminem. Tryb odwoławczy omówiono [tutaj](https://support.blablacar.com/s/article/Jak-odwo%C5%82a%C4%87-si%C4%99-od-decyzji-o-usuni%C4%99ciu-tre%C5%9Bci-lub-zawieszeniu-konta-1729197123037?language=pl).

### **15.2 Pozasądowy tryb rozstrzygania sporów**

W razie potrzeby, Użytkownik może również przedstawić skargi dotyczące działania naszej Platformy lub Usług w celu rozstrzygnięcia sporu za pośrednictwem platformy udostępnionej przez Komisję Europejską pod adresem [tutaj](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.show&lng=FR). W takim przypadku Komisja Europejska prześle skargę Użytkownika do rzecznika praw obywatelskich w jego kraju. Zgodnie z przepisami obowiązującymi w odniesieniu do mediacji, użytkownik jest zobowiązany – przed złożeniem jakiegokolwiek wniosku dotyczącego mediacji – do zawiadomienia BlaBlaCar o wystąpieniu jakiegokolwiek sporu w celu znalezienia rozwiązania polubownego.

Użytkownik może również wystąpić z wnioskiem do organu rozstrzygania sporów we własnym kraju (listę można znaleźć [tutaj](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)).

**16\. Informacje prawne**
--------------------------

Wydawcą Platformy jest Comuto SA, spółka z ograniczoną odpowiedzialnością z kapitałem zakładowym w wysokości 155 880,999 euro, wpisana do Rejestru Handlowego Spółek miasta Paryża pod numerem 491.904.546 (numer identyfikacji podatkowej wewnątrzwspólnotowego VAT: FR76491904546), z siedzibą pod adresem 84, avenue de la République, 75011 Paryż (Francja), reprezentowana przez swojego Dyrektora Naczelnego, pana Nicolas’a Brusson, Dyrektora Publikacji Internetowej.

Witryna internetowa znajduje się na serwerach Google Ireland Limited z siedzibą w Gordon House, Barrow Street, Dublin 4 (Irlandia).

Comuto SA jest zarejestrowana w Rejestrze Operatorów Turystycznych pod numerem: IM075180037.

Gwarancji finansowej udziela: Groupama Assurance-Crédit & Warning, 8-10 rue d’Astorg, 75008 Paryż — Francja.

Ubezpieczenie odpowiedzialności cywilnej zawodowej zawarte jest z następującym podmiotem: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paryż – Francja.

Comuto SA jest zarejestrowana w Rejestrze Pośredników Ubezpieczeniowych oraz bankowo-finansowym pod numerem rejestrowym (Orias) 15003890.

W razie jakichkolwiek pytań, prosimy o kontakt z firmą Comuto SA za pośrednictwem tego [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl).

**17\. Digital Services Act (Akt o usługach cyfrowych)**
--------------------------------------------------------

### **17.1.  Informacje na temat średniej miesięcznej liczby aktywnych odbiorców usługi w UE**

Zgodnie z Artykułem 24.2 Rozporządzenia Parlamentu Europejskiego i Rady w sprawie rynku wewnętrznego usług cyfrowych zmieniającego dyrektywę 2000/31/WE („DSA”), dostawcy platform online mają obowiązek publikować informacje o średniej miesięcznej liczbie aktywnych odbiorców ich usługi w UE, obliczonej jako średnia z ostatnich sześciu miesięcy. Celem publikacji jest ustalenie, czy dostawca platformy online spełnia kryterium „bardzo dużych platform online” w rozumieniu DSA, tj. czy przekracza próg średniej miesięcznej liczby aktywnych odbiorców w UE na poziomie 45 milionów.

Na dzień 17 sierpnia 2024 r. średnia miesięczna liczba aktywnych odbiorców usługi BlaBlaCar za okres od lutego do lipca 2024 r. obliczona zgodnie z Punktem 77 i Artykułem 3 DSA (licząc użytkowników, którzy mieli styczność z treścią Platformy tylko raz w okresie 6 miesięcy), zanim wydano odpowiedni akt delegowany, wynosiła około 3,28 mln w UE.

Informację publikuje się wyłącznie w celu dostosowania się do wymogów DSA i nie należy na niej polegać dla żadnych innych celów. Dane będą aktualizowane co najmniej raz na sześć miesięcy. Nasze podejście do obliczeń przedmiotowej wartości może ulegać zmianie lub może wymagać zmiany wraz z upływem czasu z powodu, na przykład, zmian produktowych bądź nowych technologii.

### **17.2. Punkt kontaktu dla organów**

Zgodnie z Artykułem 11 DSA przedstawiciele właściwych organów UE, Komisji Europejskiej lub Europejskiej Rady Usług Cyfrowych  mogą kontaktować się z nami w sprawach DSA za pośrednictwem adresu e-mail [\[email protected\]](https://blog.blablacar.pl/cdn-cgi/l/email-protection).

Można się z nami kontaktować w języku angielskim i francuskim.

Zastrzega się, że ten adres e-mail nie jest przeznaczony do komunikacji z użytkownikami. Wszelkie pytania dotyczące użytkowania Platformy BlaBlaCar Użytkownik może przesyłać za pośrednictwem [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl).

\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_\_

_Regulamin obowiązuje od 20 sierpnia 2024_

**1\. Przedmiot**
-----------------

Comuto SA opracowała platformę umożliwiającą wspólne przejazdy, dostępną na witrynie internetowej pod adresem [www.blablacar.pl](http://www.blablacar.pl/) lub w postaci aplikacji na urządzenia mobilne (dalej „**Platforma**”). Celem platformy jest (i) umożliwienie nawiązywania kontaktu pomiędzy kierowcami podróżującymi do określonego miejsca docelowego a pasażerami zamierzającymi udać się w tym samym kierunku, w celu zapewnienia im możliwości wspólnego Przejazdu i podziału związanych z nim kosztów oraz (ii) umożliwienie znalezienia i pozyskania informacji dotyczących podróży autobusem publikowanych przez profesjonalnych Przewoźników dostarczających usługi transportowe rezerwacji i kupna biletu autobusowego. Platforma jest obsługiwana przez Comuto SA w częsci dotyczącej przejazdów wspólnych oraz przez Busfor.pl sp. z o.o w częsci dotyczącej rezerwacji biletów autobusowych. Comuto SA i Busfor.pl sp. z o.o. są dalej zwane “**BlaBlaCar**”. 

W niektórych przypadkach, w zależności od okoliczności, możesz zostać przekierowany na witrynę internetową prowadzoną przez Busfor.pl Sp. z o.o., aby dokończyć zakup Biletu autobusowego (dalej zwaną „**Witryną Internetową Strony Trzeciej**”). Prosimy zwrócić uwagę, że Busfor.pl Sp. z o. o. w tym przypadku działa jako oddzielny administrator danych.

Witryna Internetowa Strony trzeciej posiada swoją własną politykę prywatności i regulamin. BlaBlaCar nie ponosi żadnej odpowiedzialności i nie akceptuje roszczeń za treść i aktywności dokonywanych na Witrynie Internetowej Strony Trzeciej. BlaBlaCar poleca, przed użyciem Witryny Internetowej Strony Trzeciej, zapoznanie się z polityką prywatności i regulaminem na Witrynie Strony Trzeciej.

Przedmiotem niniejszego Regulaminu jest zdefiniowanie zasad dostępu oraz użytkowania Platformy. Prosimy o dokładne zapoznanie się z nimi. BlaBlaCar nie jest stroną jakichkolwiek porozumień, umów lub relacji umownych jakiejkolwiek natury pomiędzy Użytkownikami Platformy. Do niektórych usług na Platformie mogą mieć zastosowanie dodatkowe warunki, takie jak warunki dotyczące konkretnego wydarzenia, działania lub promocji, i takie dodatkowe warunki zostaną ujawnione w odpowiednim trybie.

Jeśli korzystasz ze swojego Konta w celu zalogowania się na platformę BlaBlaCar innego kraju (na przykład, www.blablacar.com.br), należy pamiętać, że (i) regulamin tej platformy, (ii) polityka prywatności tej platformy oraz (iii) przepisy ustawowe tego kraju mają zastosowanie. Oznacza to również, że informacje o Koncie, w tym dane osobowe, mogą być przekazywane do podmiotu prawnego obsługującego tę inną platformę. Należy zwrócić uwagę, że BlaBlaCar zastrzega sobie prawo do ograniczenia dostępu do Platformy użytkownikom zasadnie zidentyfikowanym jako znajdującym się poza terytorium Unii Europejskiej.

Niniejszym akceptujesz, że BlaBlaCar nie jest stroną żadnych umów i stosunków umownych, które mogą powstawać pomiędzy Użytkownikami Platformy lub pomiędzy Użytkownikiem a Przewoźnikiem.

Klikając “Zaloguj się przez Facebook” lub “Zarejestruj się, używając swojego adresu e-mail”, a także przy zakupie Biletu autobusowego na Platformie bez rejestracji lub autoryzacji na Platformie Użytkownik oświadcza, że zapoznał się i zaakceptował niniejszy Regulamin.

**2\. Definicje**
-----------------

W niniejszym Regulaminie:

„**Bilet autobusowy**” oznacza dokument wydany Użytkownikowi (Pasażerowi) po dokonaniu Rezerwacji Przejazdu autobusem, potwierdzający zawarcie umowy przewozu z określonym Przewoźnikiem za pośrednictwem Platformy, uregulowany w Ogólnym Regulaminie Sprzedaży, z zastrzeżeniem jakiegokolwiek szczególnego warunku dodatkowo określonego pomiędzy Pasażerem a Przewoźnikiem i wyszczególnionego na Bilecie;

„**BlaBlaCar**” ma znaczenie określone w Artykule 1 powyżej;

„**Cena**” oznacza, w przypadku danego Przejazdu autobusem, cenę zawierającą wszystkie podatki, opłaty i koszty odpowiednich usług, uiszczaną przez Klienta na Platformie w momencie walidacji zakupu Biletu Autobusowego, za Miejsce na danym Przejeździe autobusem;

„**Kierowca**” oznacza Użytkownika korzystającego z Platformy w celu oferowania innym osobom możliwości ich przewozu w zamian za Udział w Kosztach w odniesieniu do określonego Przejazdu, w terminie określanym wyłącznie przez Kierowcę;

„**Klient**” oznacza każdą osobę fizyczną (Użytkownika lub nie), kupującą dla siebie lub w imieniu innej osoby, która będzie Pasażerem, Bilet autobusowy za pośrednictwem Platformy w celu wykonania Przejazdu autobusem realizowanego przez Przewoźnika;

„**Konto**” oznacza konto, które musi zostać założone w celu zostania Użytkownikiem i uzyskania dostępu do niektórych usług oferowanych na Platformie;

„**Konto w serwisie Facebook**” ma znaczenie określone w Artykule 3.2 poniżej;

„**Miejsce**” oznacza miejsce siedzące zarezerwowane przez Pasażera w pojeździe Kierowcy lub w pojeździe Przewoźnika;

„**Odcinek**” ma znaczenie określone w Artykule 4.1 poniżej;

„**Ogłoszenie o wspólnym przejeździe**” oznacza ogłoszenie dotyczące Przejazdu, zamieszczone na Platformie przez Kierowcę;

„**Ogłoszenie przejazdu autobusem**” oznacza ogłoszenie dotyczące przejazdu autobusem Przewoźnika Autobusowego zamieszczone na Platformie;

„**Ogólny Regulamin Sprzedaży**” oznacza Ogólny Regulamin Sprzedaży Przewoźnika w zależności od wybranego przez Klienta Przejazdu autobusem oraz szczególne warunki dostępne u Przewoźnika, które zostały uznane za przeczytane przez Klienta przed złożeniem zamówienia;

„**Opłata serwisowa**” ma znaczenie nadane jej w Artykule 5.2 poniżej;

„**Pasażer**” oznacza Użytkownika, który zaakceptował ofertę przewozu ze strony Kierowcy lub Przewoźnika, lub – jeżeli dotyczy – osobę, na rzecz której Użytkownik zarezerwował Miejsce;

„**Platforma**” ma znaczenie określone w Artykule 1 powyżej;

„**Potwierdzenie Rezerwacji**” ma znaczenie określone w Artykule 4.2.1 poniżej;

„**Przejazd**” oznacza Wspólny Przejazd, lub Przejazd Autobusem;

„**Przejazd Autobusem**” oznacza podróż, która jest przedmiotem Ogłoszenia Przejazdu Autobusem opublikowana na Platformie;

„**Przewoźnik**” oznacza podmiot oferujący profesjonalne Usługi transportowe, którego Bilety Autobusowe dystrybuowane są na Platformie;

„**Regulamin**” oznacza niniejszy Regulamin;

„**Rezerwacja**” ma znaczenie określone w Artykule 4.2.1 poniżej;

„**Treści Użytkownika**” mają znaczenie określone w Artykule 11.2 poniżej;

„**Udział w Kosztach**” oznacza, w odniesieniu do określonego Przejazdu, sumę pieniędzy wymaganą przez Kierowcę i zaakceptowaną przez Pasażera jako jego udział w kosztach Przejazdu;

„**Usługi**” oznaczają wszystkie usługi świadczone przez BlaBlaCar za pośrednictwem Platformy;

„**Usługi transportowe**” oznaczają usługi transportowe subskrybowane przez Pasażera Przejazdu autobusem i świadczone przez Przewoźnika;

„**Użytkownik**” oznacza dowolną osobę, która założyła Konto na Platformie;

„**Witryna internetowa**” oznacza witrynę dostępną pod adresem www.blablacar.pl;

„**Wspólny Przejazd**” oznacza podróż stanowiącą przedmiot Ogłoszenia o wspólnym przejeździe zamieszczonego przez Kierowcę na Platformie, na którego podstawie wyraża on zgodę na przewóz Pasażerów w zamian za Udział w Kosztach.

**3\. Rejestracja na Platformie i założenie Konta**
---------------------------------------------------

### **3.1. Warunki rejestracji na Platformie**

Platforma może być wykorzystywana przez osoby, które ukończyły 18 lat. Dokonanie rejestracji na Platformie przez osobę niepełnoletnią jest całkowicie zabronione. Poprzez dostęp, korzystanie lub rejestrację na Platformie Użytkownik oświadcza, że ma ukończone 18 lat.

### **3.2. Założenie Konta**

Platforma umożliwia Użytkownikom zamieszczanie i wyświetlanie Ogłoszeń o wspólnym przejeździe oraz wzajemną interakcję celem rezerwacji Miejsca, a także przeglądanie ogłoszeń o Przejazdach autobusowych i kupowanie Biletów autobusowych. Ogłoszenia mogą być również wyświetlane przez osoby, które nie są zarejestrowane na Platformie. Niemniej jednak, zamieszczenie Ogłoszenia lub przeprowadzenie rezerwacji Miejsca nie jest możliwe bez wcześniejszego założenia Konta i zostania Użytkownikiem.

W niektórych przypadkach, w zależności od okoliczności, będziesz mógł zarezerwować Miejsce na Przejazd autobusem bez zakładania Konta, ale zgodnie z niniejszym Regulaminem i warunkami Przewoźnika, za pomocą którego wybierasz się w podróż.

Aby założyć Konto, należy:

(i) wypełnić wszystkie obowiązkowe pola formularza rejestracji;

(ii) lub zalogować się do swojego konta w serwisie Facebook za pośrednictwem Platformy (dalej “Konto w serwisie Facebook”). Korzystając z tej funkcji Użytkownik zdaje sobie sprawę, że BlaBlaCar będzie posiadać dostęp do informacji znajdujących się na jego Koncie w serwisie Facebook będzie je publikować na Platformie i przechowywać. Użytkownik może w dowolnej chwili usunąć powiązanie pomiędzy jego Kontem a Kontem w serwisie Facebook, korzystając ze strony [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl). Aby uzyskać więcej informacji dotyczących wykorzystywania danych pochodzących z twojego Konta w serwisie Facebook, należy zapoznać się z postanowieniami naszej Polityki Prywatności, jak również polityką prywatności serwisu Facebook.

Aby zarejestrować się na Platformie należy wcześniej przeczytać i zaakceptować niniejszy Regulamin.

Zakładając swoje Konto – niezależnie od wybranej metody – Użytkownik zobowiązuje się do przekazania kompletnych i prawdziwych informacji oraz ich aktualizacji za pośrednictwem profilu na swoim Koncie lub poprzez zawiadomienie BlaBlaCar w celu zapewnienia ich stosowności i dokładności przez okres trwania umowy z BlaBlaCar.

W przypadku rejestracji za pośrednictwem poczty e-mail, Użytkownik zobowiązuje się do zachowania w poufności hasła wybranego podczas zakładania Konta oraz nieujawniania go osobom trzecim. W razie utraty lub ujawnienia swojego hasła, Użytkownik jest zobowiązany do natychmiastowego zawiadomienia o tym BlaBlaCar. Użytkownik ponosi pełną odpowiedzialność za wykorzystanie jego Konta przez jakiekolwiek strony trzecie, chyba że wyraźnie zawiadomił BlaBlaCar o utracie, niedozwolonym wykorzystaniu, lub ujawnieniu swojego hasła osobie trzeciej.

Użytkownik zobowiązuje się, że nie będzie tworzył lub wykorzystywał – posługując się tożsamością własną lub osoby trzeciej – żadnych innych Kont, niż utworzone pierwotnie.

### **3.3. Weryfikacja**

BlaBlaCar może, w celu zapewnienia przejrzystości, zwiększenia zaufania lub zapobiegania oszustwom lub ich wykrywania, skonfigurować system weryfikacji niektórych informacji podanych przez Ciebie w swoim profilu. Dzieje się tak zwłaszcza w przypadku, gdy podajesz swój numer telefonu lub przedstawiasz nam dokument tożsamości.

Użytkownik jest świadom i godzi się z faktem, że jakiekolwiek odniesienia na Platformie lub w ramach Usług do „zweryfikowanych” (co obejmuje „Zweryfikowany Profil”) informacji lub podobnego rodzaju wyrażenia oznaczają jedynie, że dany Użytkownik pozytywnie przeszedł procedurę weryfikacji na Platformie lub w ramach Usług, w celu przekazania bardziej szczegółowych informacji na temat Użytkownika, z którym zamierza odbyć podróż. BlaBlaCar nie może zagwarantować prawdziwości, rzetelności ani ważności informacji będących przedmiotem procedury weryfikacji.

**4\. Korzystanie z Usług**
---------------------------

### **4.1. Zamieszczanie Ogłoszeń o wspólnym przejeździe**

Jako Użytkownik, pod warunkiem spełnienia poniższych warunków, możesz tworzyć i zamieszczać na Platformie swoje Ogłoszenia o wspólnym przejeździe, podając informacje dotyczące planowanego Wspólnego Przejazdu (data/godzina, miejsce wyjazdu i przyjazdu, liczba oferowanych miejsc, dostępne opcje, wysokość Udziału w Kosztach itp.).

Zamieszczając swoje Ogłoszenie o wspólnym przejeździe, Użytkownik może określić miasta, w których zamierza się zatrzymać, aby przyjąć lub wysadzić Pasażerów. Poszczególne odcinki Wspólnego Przejazdu pomiędzy tymi miastami lub jednym z tych miast, a miejscem wyjazdu lub przyjazdu są dalej określane jako “Odcinki”.

Użytkownik może zamieszczać Ogłoszenia o wspólnym przejeździe jedynie, jeśli spełnia łącznie wszystkie określone poniżej warunki:

(i) posiada ważne prawo jazdy;

(ii) zamieszcza Ogłoszenia o wspólnym przejeździe dotyczące wyłącznie pojazdów, których jest właścicielem lub z których korzysta za wyraźną zgodą ich właściciela, a w każdym przypadku, do których wykorzystania jest uprawniony w celu odbywania wspólnych przejazdów;

(iii) jest i pozostaje głównym kierowcą pojazdu stanowiącego przedmiot Ogłoszenia o wspólnym przejeździe;

(iv) pojazd stanowi przedmiot ważnej polisy ubezpieczeniowej dotyczącej ubezpieczenia osób trzecich;

(v) nie ma żadnych przeciwwskazań medycznych w odniesieniu do prowadzenia pojazdów;

(vi) pojazd, który zamierza wykorzystać w celu realizacji Wspólnego Przejazdu to samochód osobowy posiadający 4 koła i maksymalnie 7 miejsc;

(vii) nie zamierza zamieścić innego ogłoszenia dotyczącego tego samego Wspólnego Przejazdu na Platformie;

(viii) nie oferuje większej liczby Miejsc, niż ich liczba dostępna w jego pojeździe;

(ix) wszystkie oferowane Miejsca są wyposażone w pasy bezpieczeństwa, nawet jeśli pojazd został zatwierdzony do ruchu z miejscami nieposiadającymi pasów bezpieczeństwa;

(x) korzysta z pojazdu znajdującego się w dobrym stanie, spełniającego wszystkie obowiązujące przepisy prawa i praktyki, a w szczególności posiadającego ważny certyfikat kontroli technicznej (potwierdzający, że pojazd nadaje się do jazdy);

(xi) jest konsumentem i nie działa w charakterze służbowym.

Użytkownik ponosi we własnym zakresie pełną odpowiedzialność za treść Ogłoszenia o wspólnym przejeździe zamieszczanego na Platformie. W związku z powyższym, Użytkownik oświadcza, że wszystkie informacje zamieszczone w jego Ogłoszeniu o wspólnym przejeździe są prawidłowe i prawdziwe oraz zobowiązuje się do realizacji Wspólnego Przejazdu na warunkach określonych w tym Ogłoszeniu o wspólnym przejeździe.

Pod warunkiem, że Ogłoszenie o wspólnym przejeździe zamieszczone przez Użytkownika jest zgodne z niniejszym Regulaminem, zostanie ono zamieszczone na Platformie i będzie w ten sposób widoczne dla innych Użytkowników oraz osób odwiedzających, także niebędących Użytkownikami, przeprowadzających wyszukiwanie na Platformie bądź na witrynie internetowej partnerów BlaBlaCar. BlaBlaCar zastrzega sobie prawo – na podstawie wyłącznie własnej decyzji – odmowy zamieszczenia lub usunięcia w dowolnej chwili jakiegokolwiek Ogłoszenia o wspólnym przejeździe niespełniającego warunków niniejszego Regulaminu lub jeżeli uważa, że przynosi ono jakiekolwiek szkody dla wizerunku BlaBlaCar, Platformy bądź Usług i/lub zawieszenia Konta Użytkownika zamieszczającego takie Ogłoszenia o wspólnym przejeździe w trybie postanowień Artykułu 9 niniejszego Regulaminu.

Informujemy, że w przypadku podawania się jako konsument korzystając z Platformy, podczas gdy w rzeczywistości działasz w charakterze służbowym, narażasz się na sankcje przewidziane przez obowiązujące prawo.

### **4.2. Rezerwacja Miejsca**

„BlaBlaCar” stworzył system rezerwacji Miejsc online („**Rezerwacja**”) dla Przejazdów oferowanych na Platformie. 

Metody rezerwacji Miejsca są uzależnione od rodzaju planowanego Przejazdu.

BlaBlaCar udostępnia Użytkownikom Platformy wyszukiwarkę opartą na różnych kryteriach wyszukiwania (miejsce pochodzenia, cel podróży, daty, liczba podróżnych itp.). Pewne dodatkowe funkcjonalności udostępniane są w wyszukiwarce, gdy użytkownik jest połączony ze swoim Kontem. BlaBlaCar zachęca Użytkownika, niezależnie od stosowanego procesu rezerwacji, do dokładnego przeanalizowania oferty i skorzystania z wyszukiwarki w celu ustalenia oferty najbardziej dopasowanej do jego potrzeb. Więcej informacji można znaleźć [tutaj](https://blog.blablacar.pl/about-us/transparentnosc-platform). Klient, rezerwując Przejazd autobusem offline poza Platformą, może również zwrócić się do Przewoźnika lub przedstawiciela obsługi klienta o przeprowadzenie wyszukiwania.

**4.2.1. Wspólny Przejazd**

Kiedy Pasażer jest zainteresowany w zamieszczeniu Ogłoszenia o wspólnym przejeździe, może przesłać za pośrednictwem sieci zapytanie dotyczące Rezerwacji. Zapytanie dotyczące Rezerwacji może (i) zostać zaakceptowane automatycznie (jeżeli Kierowca wybierze tę opcję podczas zamieszczania swojego Ogłoszenia o wspólnym przejeździe) lub (ii) zostać zaakceptowane ręcznie przez Kierowcę. W momencie Rezerwacji Pasażer dokonuje płatności Opłaty serwisowej, o ile ma to zastosowanie. Po zatwierdzeniu zapytania dotyczącego Rezerwacji przez Kierowcę – jeżeli dotyczy – Pasażer otrzymuje Potwierdzenie Rezerwacji (dalej “Potwierdzenie Rezerwacji”).

Jeżeli użytkownik jest Kierowcą i w chwili zamieszczenia Ogłoszenia o wspólnym przejeździe wybrał opcję akceptacji ręcznej zapytań dotyczących Rezerwacji, jest on zobowiązany do udzielania odpowiedzi na wszystkie zapytania dotyczące Rezerwacji w terminie określonym przez Pasażera w jego zapytaniu. W przeciwnym wypadku, zapytanie dotyczące Rezerwacji automatycznie wygaśnie, a Pasażerowi zwrócone zostaną wszystkie kwoty zapłacone w związku z tym zapytaniem, jeżeli istnieją.

W chwili Potwierdzenia Rezerwacji, BlaBlaCar ponownie prześle Użytkownikowi numer telefonu Kierowcy (w przypadku Użytkownika będącego Pasażerem) lub Pasażera (w przypadku Użytkownika będącego Kierowcą). Użytkownik ponosi wyłączną odpowiedzialność za wypełnienie umowy wiążącej go z innym Użytkownikiem.

**4.2.2. Przejazdy autobusem**

W przypadku Przejazdu autobusem BlaBlaCar umożliwia rezerwację Miejsc na dany Przejazd autobusowy Przewoźnika za pośrednictwem Platformy lub poprzez przekierowanie na Witrynę Internetową Strony Trzeciej. Potwierdzasz, że rezerwacja Miejsc i zakup biletów dla Przejazdów autobusem odbywa się w zależności od dostępności Miejsc.

Usługi transportowe podlegają Ogólnemu Regulaminowi Sprzedaży odpowiedniego Przewoźnika, w zależności od wybranego przez Klienta Przejazdu, który Klient musi przeczytać i zaakceptować przed dokonaniem rezerwacji i opłaceniem Miejsca. BlaBlaCar nie świadczy żadnych usług transportowych w odniesieniu do Przejazdów autobusem, Przewoźnicy są wyłącznie stroną Ogólnego Regulaminu Sprzedaży. Prosimy zapytać stosownego Przewoźnika o Ogólny Regulamin Sprzedaży, w szczególności w odniesieniu do przewozu osób niepełnoletnich i bagażu oraz odwołania Przejazdu autobusem.

Ogólny Regulamin Sprzedaży jest przedstawiany przez Przewoźników, w tym za pośrednictwem ich stron internetowych. Warunki anulowania Biletu autobusowego mogą być również prezentowane na Platformie. Wszelkie informacje o Przejazdach Autobusem, publikowane na Platformie, w tym dostępność, trasy, rozkłady jazdy i Ceny, pochodzą od Przewoźników. BlaBlaCar nie zmienia tych informacji ani nie ponosi odpowiedzialności za ich jakość.

BlaBlaCar zwraca uwagę na fakt, że niektóre Usługi transportowe oferowane przez Przewoźnika i wymienione na Platformie mogą zostać wycofane, w szczególności z powodów klimatycznych, sezonowego charakteru lub w przypadku wystąpienia siły wyższej.

Aby zarezerwować Miejsce na Przejazd autobusem i kupić Bilet autobusowy, niezależnie od tego, czy jesteś zalogowany na swoje Konto na Platformie, czy nie (w stosownych przypadkach), musisz podać stosowne dane osobowe zgodnie z naszą [Polityką prywatności](https://blog.blablacar.pl/about-us/privacy-policy). Należy pamiętać, że Polityka prywatności Przewoźnika, za pomocą której podróżujesz, może mieć również zastosowanie. BlaBlaCar nie zmienia tych informacji ani nie ponosi odpowiedzialności za niedokładność danych wprowadzonych podczas procesu rezerwacji Przejazdu autobusem.

Bilet autobusowy jest rezerwowany, a Miejsce jest rezerwowane (jeśli pozwala na to wybór miejsc) po zakończeniu płatności. Jeśli płatność nie zostanie dokonana, Bilet autobusowy zostanie ponownie wystawiony na sprzedaż, a Miejsce może zostać zarezerwowane przez innego Pasażera.

Po zakończeniu zakupu otrzymasz wiadomość e-mail z kopią Biletu autobusowego, potwierdzeniem rezerwacji i szczegółowymi informacjami na temat Przejazdu autobusem. Zachęcamy do sprawdzenia ustawień skrzynki odbiorczej swojego adresu e-mail, a w szczególności do upewnienia się, że e-mail potwierdzający nie trafia bezpośrednio do spamu.

Kwestie organizacji Przejazdu autobusem i realizacji umowy przewozu są wyłączną odpowiedzialnością Przewoźnika i muszą być rozwiązywane bezpośrednio z Przewoźnikiem. BlaBlaCar nie jest stroną jakichkolwiek umów pomiędzy Klientem, Pasażerem a Przewoźnikiem.

**4.2.3. Imienny charakter rezerwacji Miejsca i warunki korzystania z Usług na rzecz osób trzecich**

Korzystanie z Usług – zarówno w charakterze Pasażera, jak i Kierowcy – ma charakter imienny. Dane Kierowcy i Pasażera muszą odpowiadać informacji dotyczących ich tożsamości, przekazanych BlaBlaCar, Przewoźnikom (jeśli dotyczy) oraz innym uczestniczącym w Przejeździe Użytkownikom.

Niemniej jednak, w odniesieniu do Wspólnych Przejazdów, BlaBlaCar umożliwia Użytkownikom zarezerwowanie jednego lub większej liczby Miejsc na rzecz osób trzecich. W takim przypadku Użytkownik zobowiązuje się do precyzyjnego przekazania Kierowcy w chwili dokonywania Rezerwacji lub przesyłania wiadomości do Kierowcy (w razie Przejazdu bez Rezerwacji), imię i nazwiska, wiek oraz numeru telefonu osoby, na rzecz której dokonuje rezerwacji Miejsca. Absolutnie zabronione jest zarezerwowanie Miejsca do Wspólnego Przejazdu dla podróżującej samotnie osoby niepełnoletniej w wieku poniżej 13 lat. W przypadku rezerwacji Miejsca do Wspólnego Przejazdu dla podróżującej samotnie osoby niepełnoletniej w wieku powyżej 13 lat, Użytkownik zobowiązuje się do uzyskania wcześniejszej zgody Kierowcy oraz przekazania mu prawidłowo sporządzonego upoważnienia, podpisanego przez opiekunów prawnych osoby niepełnoletniej.

Ponadto, przedmiotem działalności Platformy jest dokonywanie rezerwacji Miejsc jedynie dla osób. Absolutnie zabronione jest dokonywanie rezerwacji Miejsca w celu przewozu jakichkolwiek przedmiotów, przesyłek, zwierząt podróżujących bez opieki i dowolnych materiałów.

Ponadto zabronione jest zamieszczenie Ogłoszenia w imieniu i na rzecz innego Kierowcy.

Odnośnie Przejazdów autobusem zachęcamy do uwzględnienia Ogólnego Regulaminu Sprzedaży danego Przewoźnika. Jeśli rezerwujesz Przejazd autobusem dla osoby trzeciej, bierzesz pełną odpowiedzialność za posiadanie odpowiednich uprawnień do wykonywania działań w imieniu i na rzecz takiej osoby trzeciej.

Należy pamiętać, że dzieci w wieku poniżej 16 lat mogą dokonywać Przejazdy autobusem wyłącznie pod warunkiem, że ich rodzice lub przedstawiciele prawni posiadają odpowiednie uprawnienia, udzielają im pomocy i wsparcia zgodnie z obowiązującym prawem. Rodzice lub inni przedstawiciele prawni ponoszą odpowiedzialność za wszelkie działania osób niepełnoletnich podczas korzystania z Platformy i Przejazdów ajutobusem, w tym za wszelkie nielegalne działania i / lub zaniechania ze strony osób niepełnoletnich podczas korzystania z Platformy.

W przypadku rezerwacji Miejsca na Przejazd autobusowy dla osoby poniżej 16 roku życia ponosisz wyłączną odpowiedzialność za zapewnienie tego, żeby osoba niepełnoletnia posiadała odpowiednie uprawnienia do reprezentowania takiej osoby i jej towarzyszenia , do uzyskiwania i udzielania niezbędnych zezwoleń i zgody oraz wykonywania innych obowiązków zgodnie z obowiązującym prawem.

Przed zakupem Biletu autobusowego zalecamy zapoznanie się z Ogólnym Regulaminem Sprzedaży Przewoźnika dotyczącym przewozu osób niepełnoletnich.

### **4.3. Treści Użytkownika, moderacja i system ocen**

**4.3.1. Zasady działania systemu ocen**

BlaBlaCar zachęca użytkowników do zamieszczania recenzji dotyczących Kierowców (w przypadku, jeżeli Użytkownik jest Pasażerem) lub dotyczących Pasażerów (w przypadku, jeżeli Użytkownik jest Kierowcą), z którymi korzystał z Wspólnych Przejazdów bądź miał korzystać z Przejazdów (tzn. zarezerwował Wspólny Przejazd. Niemniej jednak, Użytkownik nie może zamieszczać recenzji dotyczących innych Pasażerów, jeżeli sam jest Pasażerem lub na temat Użytkowników, z którymi nie podróżował lub nie miał podróżować. Masz możliwość wystawienia opinii w ciągu 14 od Przejazdu.

Recenzja zamieszczona przez Użytkownika lub dotycząca go recenzja zamieszczona przez innego Użytkownika – jeżeli dotyczy – jest widoczna iI publikowana na Platformie w krótszym z następujących okresów: (i) natychmiast po zamieszczeniu recenzji przez osoby wymienione powyżej lub (ii) po upływie 14 dni od pierwszej recenzji.

W niektórych przypadkach Użytkownik może udzielić odpowiedzi na recenzję zamieszczoną przez innegorofileik anaa jegorofileu w ciągu 14 dni od otrzymania takiej recenzji. Recenzja oraz odpowiedź Użytkownika zostaną zamieszczone na jegorofileu.

**4.3.2. Moderacja**

**a. Treści Użytkownika**

Użytkownik uznaje i wyraża zgodę na fakt, że BlaBlaCar może dokonywać moderacji przed publikacją, przy użyciu narzędzi automatycznych lub ręcznie, Treści Użytkownika, o których mowa w Artykule 11.2. Jeżeli BlaBlaCar uzna takie Treści Użytkownika za naruszające właściwy przepisy obowiązującego prawa lub niniejszym Regulamin, zastrzega sobie prawo do:

* uniemożliwienia publikacji lub usunięcia takich Treści Użytkownika
* wystosowania ostrzeżenia do Użytkownika z przypomnieniem o obowiązku przestrzegania właściwych przepisów obowiązującego prawa lub niniejszego Regulaminu i/lub
* zastosowania środków restrykcyjnych, o których mowa w Artykule 9 niniejszego Regulaminu.

Stosowanie przez BlaBlaCar takich automatycznych lub ręcznych narzędzi moderacji nie będzie interpretowane jako zobowiązanie do monitorowania ani zobowiązanie do aktywnego poszukiwania wszelkich przypadków czynności niezgodnych z prawem i/lub Treści Użytkownika publikowanych na Platformie. Nie będzie również, w zakresie dozwolonym przez właściwe przepisy obowiązującego prawa, stanowić podstawy do jakiejkolwiek odpowiedzialności ani zobowiązań BlaBlaCar z powyższego tytułu.

**b. Wiadomości**

Wymiana wiadomości między Użytkownikami za pośrednictwem naszej Platformy („Wiadomości”) odbywa się wyłącznie w celu wymiany informacji dotyczących Wspólnych Przejazdów.

BlaBlaCar może, przy użyciu zautomatyzowanego oprogramowania i algorytmów, wykrywać treści Wiadomości na potrzeby zapobiegania oszustwom, usprawnienia poziomu usług, wsparcia klienta oraz wykonywania umów zawartych z naszymi użytkownikami (takich jak niniejszy Regulamin). W razie wykrycia wszelkich treści w Wiadomości, które noszą znamiona oszustwa, działania niezgodnego z prawem lub działania służącego obejściu Platformy bądź są w inny sposób niezgodne z niniejszym Regulaminem:

* takie treści nie mogą zostać opublikowane
* Użytkownik, który wysłał Wiadomość, może dostać ostrzeżenie z przypomnieniem, że ma obowiązek przestrzegać właściwych przepisów obowiązującego prawa lub niniejszego Regulaminu lub
* konto Użytkownika może podlegać zawieszeniu w trybie Artykułu 9 niniejszego Regulaminu.

Stosowanie przez BlaBlaCar takich automatycznych lub ręcznych narzędzi moderacji nie będzie interpretowane jako zobowiązanie do monitorowania ani zobowiązanie do aktywnego poszukiwania wszelkich przypadków czynności niezgodnych z prawem i/lub Treści Użytkownika publikowanych na Platformie.  Nie będzie również, w zakresie dozwolonym przez właściwe przepisy obowiązującego prawa, stanowić podstawy do jakiejkolwiek odpowiedzialności ani zobowiązań BlaBlaCar z powyższego tytułu.

Użytkownik uznaje i wyraża zgodę na fakt, że BlaBlaCar zastrzega sobie prawo niezamieszczenia lub usunięcia dowolnych recenzji, pytań, komentarzy lub odpowiedzi, które uzna za niezgodne z postanowieniami niniejszego Regulaminu.

**4.3.3. Ograniczenie**

Zgodnie z Artykułem 9 niniejszego Regulaminu BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub rozwiązania stosunku prawnego obowiązującego na podstawie niniejszego Regulaminu w przypadku, jeśli (i) Użytkownik otrzymał co najmniej trzy recenzje i (ii) jedna lub średnia ocena uzyskana w ramach tych recenzji jest niższa lub równa 3 w zależności od powagi opinii pozostawionej w tym trybie przez Użytkownika.  

**5\. Warunki finansowe**
-------------------------

### **5.1. Udział w Kosztach i Cenie**

5.1.1 W przypadku Wspólnego przejazdu wartość Udziału w Kosztach jest określona przez Użytkownika będącego Kierowcą, na jego wyłączną odpowiedzialność. Absolutnie zabronione jest uzyskiwanie jakichkolwiek zysków z używania naszej Platformy. W związku z powyższym, Użytkownik zobowiązuje się do ograniczenia kwoty Udziału w Kosztach, której żąda od swoich Pasażerów do wartości kosztów rzeczywiście ponoszonych w związku z Przejazdem. W przeciwnym przypadku, Użytkownik ponosi we własnym zakresie pełne ryzyko w odniesieniu do zmiany kwalifikacji podatkowej transakcji zrealizowanej za pośrednictwem Platformy.

W chwili zamieszczenia Ogłoszenia przez Użytkownika, BlaBlaCar zasugeruje kwotę Udziału w Kosztach, przy uwzględnieniu rodzaju Przejazdu oraz jego odległości. Kwota ta stanowi wyłącznie sugestię, a Użytkownik może zwiększyć lub zmniejszyć ją na podstawie rzeczywistych kosztów ponoszonych w związku z Przejazdem. Aby uniknąć możliwości nadużyć, BlaBlaCar zastrzega sobie prawo modyfikacji wartości Udziału w Kosztach.

5.1.2 W przypadku Przejazdu autobusem Cenę za Miejsce określa Przewoźnik. Klient jest proszony o zapoznanie się z odpowiednim Ogólnym Regulaminem Sprzedaży w celu zrozumienia obowiązujących zasad dotyczących składania zamówienia Biletów.

### **5.2. Opłata serwisowa**

BlaBlaCar w zamian za korzystanie z Platformy w momencie Rezerwacji pobiera opłaty serwisowe (zwane dalej „**Opłatą serwisową**”). W razie konieczności użytkownik zostanie poinformowany przed zastosowaniem Opłaty serwisowej.

Sposób płatności Opłaty serwisowej może się różnić w zależności od używanej platformy BlaBlaCar. Dozwolone sposoby płatności Opłaty serwisowej są następujące: 

* Karta kredytowa dostarczona przez Adyen NV, spółkę publiczną zarejestrowaną w Holandii pod numerem 34259528 z siedzibą pod adresem Simon Carmiggeltstraat 6-50, 1011 DJ, Amsterdam, Holandia, lub
* Blik, dostarczony przez spółkę Polski Standard Płatności sp. z o.o. z siedzibą w Warszawie ul. Cypryjska 72, wpisaną do rejestru przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XIII Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS 0000493783, NIP: 5213664494, Statystyczny Numer Identyfikacyjny (REGON): 147055889.

Metody naliczania Opłaty serwisowej mogą zostać opublikowane przez BlaBlaCar odrębnie, mają charakter wyłącznie informacyjny i nie mają wartości umownej. Opłata serwisowa może być naliczana w zależności od różnych czynników, w szczególności długości Podróży i Udziału w kosztach. BlaBlaCar zastrzega sobie prawo do zmiany sposobu naliczania Opłaty serwisowej w dowolnym momencie. Zmiany te nie będą miały wpływu na Opłatę serwisową zaakceptowaną przez Użytkownika przed datą wejścia w życie tych zmian.

W przypadku podróży transgranicznych należy pamiętać, że metody obliczania wysokości Opłaty serwisowej oraz obowiązującego podatku VAT różnią się w zależności od miejsca odbioru i/lub celu podróży.

W przypadku korzystania z Platformy w podróżach transgranicznych lub poza granicami Polski, Opłata serwisowa może zostać pobrana przez spółkę stowarzyszoną BlaBlaCar obsługującą lokalną platformę.

### **5.3. Zaokrąglenia**

BlaBlaCar może w zależności wyłącznie od własnej decyzji zaokrąglać kwoty Udziału w Kosztach lub Opłaty serwisowej w górę lub w dół.

### **5.4. Sposoby płatności Udziału w kosztach na rzecz Kierowcy i Ceny**

Należy zwrócić uwagę, że dostawcy usług płatniczych mogą nakładać procedury i ograniczenia na niektóre transakcje ze względu na ich zobowiązania dotyczące zgodności zgodnie z własnymi regulaminami.

**5.4.1. Płatność Udziału w kosztach na rzecz Kierowcy**

Pasażer zobowiązuje się do zapłacenia Kierowcy Udziału w Kosztach najpóźniej w chwili opuszczania jego pojazdu w miejscu docelowym.

Kierowca nie może domagać się zapłaty Udziału w Kosztach w całości lub w części przed realizacją Przejazdu.

**5.4.2 Płatność Ceny** 

Płatność Ceny za każdą rezerwację Przejazdu autobusem dokonana za pośrednictwem Platformy jest dokonywana za pomocą jednego z dozwolonych poniżej sposobów:

* Karta kredytowa dostarczona przez Adyen NV, spółkę publiczną zarejestrowaną w Holandii pod numerem 34259528 z siedzibą pod adresem Simon Carmiggeltstraat 6-50, 1011 DJ, Amsterdam, Holandia, lub
* Blik, dostarczony przez spółkę Polski Standard Płatności sp. z o.o. z siedzibą w Warszawie ul. Cypryjska 72, wpisaną do rejestru przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XIII Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS 0000493783, NIP: 5213664494, Statystyczny Numer Identyfikacyjny (REGON): 147055889.

Sposób płatności może się różnić w zależności od używanej platformy BlaBlaCar.

Kupując Miejsce na Przejazd autobusowy za pośrednictwem Platformy, płacisz Cenę stosownego odpowiedzialnego Przewoźnika, biorąc pod uwagę obowiązujące podatki i opłaty (jeżeli są pobierane).

Aby dokonać płatności, możesz zostać przekierowany na stronę zewnętrznego dostawcy usług płatniczych — partnera BlaBlaCar. BlaBlaCar w żadnym wypadku nie może zagwarantować, że usługi świadczone przez firmę odpowiedzialną za dokonywanie płatności będą świadczone bez błędów, zakłóceń, awarii, opóźnień lub innych wad. BlaBlaCar nie ponosi żadnej odpowiedzialności za zarządzanie, pośrednictwo lub dokonywanie płatności. W każdym przypadku BlaBlaCar nie ponosi odpowiedzialności za transakcje dokonywane między Użytkownikiem, Klientem i Przewoźnikami.

Bilet autobusowy jest wydawany Użytkownikowi tylko wtedy, gdy płatność za Cenę jest pełna, zakończona i skuteczna. Płatność musi zostać dokonana nie później niż w terminie wskazanym na Platformie. Jeśli Płatność nie zostanie dokonana w ramach wskazanego terminu, jest nieprawidłowa, niekompletna lub nie została dokonana z przyczyn leżących po stronie Klienta, rezerwacja Przejazdu Autobusowego zostanie automatycznie anulowana.

Płatność Ceny jest ostateczna. W związku z tym wszelkie zmiany spowodują wymianę lub anulowanie zgodnie z obowiązującym Ogólnym Regulaminem Sprzedaży Przewoźnika. Obowiązkiem Klienta jest dopilnowanie, aby Usługi transportowe zostały wybrane zgodnie z potrzebami i oczekiwaniami Pasażera. Za dokładność danych osobowych wprowadzonych przez Klienta odpowiada ten ostatni, z wyjątkiem przypadku, gdy zostanie udowodnione, że BlaBlaCar nie gromadzi, nie przechowuje ani nie chroni takich Danych osobowych.

Ceny mogą zostać zmienione przez Przewoźników. Od czasu do czasu na Platformie mogą być wyświetlane promocje lub oferty specjalne do tyczące Cen. Odnośnie takich promocji i ofert mogą obowiązywać dodatkowe warunki.

**6\. Niehandlowy i niebiznesowy charakter Usług oraz Platformy**
-----------------------------------------------------------------

Użytkownik zobowiązuje się do korzystania z Usług oraz Platformy jedynie w celu nawiązania kontaktów na zasadach niehandlowych i niebiznesowych z osobami zamierzającymi skorzystać ze wspólnego Przejazdu.

W kontekście Wspólnego Przejazdu przyjmujesz do wiadomości, że prawa konsumenckie wynikające z unijnego prawa ochrony konsumentów nie mają zastosowania do Twoich relacji z innymi Członkami.

Użytkownik będący Kierowcą zobowiązuje się, że nie będzie żądał Udziału w Kosztach w kwocie wyższej, niż rzeczywiście poniesione koszty jak również w celu uzyskiwania jakichkolwiek zysków, a ponadto, na zasadzie wspólnego ponoszenia kosztów, Kierowca musi ponieść własną część kosztów związanych z Przejazdem. Kierowca ponosi wyłączną odpowiedzialność za obliczenie kosztów ponoszonych w ramach Przejazdu oraz sprawdzenie, czy kwota wymagana od Pasażerów nie przekracza rzeczywiście ponoszonych kosztów (przy uwzględnieniu udziału własnego użytkownika).

BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika w przypadku, jeżeli wykorzystuje samochód firmowy, służbowy lub taksówkę generując w ten sposób zysk za pośrednictwem Platformy. Użytkownik zobowiązuje się do przedstawienia firmie BlaBlaCar na każde jej żądanie kopii dowodu rejestracyjnego jego samochodu i/lub dowolnego innego dokumentu potwierdzającego, że jest on uprawniony do korzystania z danego pojazdu na Platformie i nie uzyskuje w ten sposób jakichkolwiek zysków.

Zgodnie z Artykułem 9 niniejszego Regulaminu BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub zakończenia obowiązywania niniejszego Regulaminu w przypadku prowadzenia przez Użytkownika na Platformie jakiejkolwiek działalności, która – ze względu na charakter oferowanych Przejazdów, ich częstotliwość, liczbę przewożonych Pasażerów oraz żądany Udział w Kosztach – umożliwia Użytkownikowi uzyskiwanie zysków lub na podstawie których BlaBlaCar może podejrzewać, że Użytkownik uzyskuje zyski za pośrednictwem Platformy.

**7\. Polityka dotycząca anulowania**
-------------------------------------

### **7.1. Warunki zwrotu środków w przypadku rezygnacji**

Przedmiotem niniejszych zasad anulowania są wyłącznie Wspólne Przejazdy; BlaBlaCar nie udziela żadnej gwarancji, jakiejkolwiek natury, w przypadku anulowania z jakiegokolwiek powodu.

Anulowanie Miejsca na Wspólny przejazd przez Kierowcę lub Pasażera po Potwierdzeniu Rezerwacji podlega poniższym postanowieniom:

– W przypadku rezygnacji z winy Kierowcy, Pasażerowi zwracana jest Opłata serwisowa. Dzieje się tak zwłaszcza w przypadku, gdy Kierowca:

* Nie potwierdził prośby o Rezerwację w wyznaczonym terminie (jeżeli taka opcja została wybrana przez Kierowcę);
* Anuluje Wspólny przejazd lub nie przybył na miejsce spotkania w ciągu 15 minut od uzgodnionej godziny;

\-W przypadku odwołania z winy Pasażera Opłatę serwisową zatrzymuje BlaBlaCar

Sposób zwrotu Opłaty serwisowej ustalany jest w zależności od sposobu uiszczenia Opłaty Serwisowej, natomiast termin zwrotu Opłaty serwisowej uzależniony jest od terminów przelewów dokonywanych przez instytucje bankowe.

Jeżeli odwołanie nastąpi przed odjazdem i z winy Pasażera, Miejsce (Miejsca) odwołane przez Pasażera zostaną automatycznie udostępnione innym Pasażerom, którzy mogą dokonać rezerwacji online i odpowiednio podlegają warunkom niniejszego Regulaminu.

BlaBlaCar ocenia, według własnego uznania, na podstawie dostępnych informacji, zasadność wniosków o zwrot środków.

### **7.2. Przejazdy autobusowe**

Procedura dotycząca anulowania Przejazdu Autobusem wynika z Ogólnego Regulaminu Sprzedaży danego Przewoźnika. Możesz anulować lub zmienić rezerwację Przejazdu autobusem na Platformie lub na Witrynie Internetowej Osoby Trzeciej, w zależności od okoliczności.

Środki zostaną zwrócone zgodnie z metodą płatności stosowaną przy zakupie biletu na Przejazd autobusem w terminie określonym przez Twoj bank.

Należy pamiętać, że opłaty za wprowadzenie zmian lub anulowanie rezerwacji Przejazdu autobusowego mogą być naliczane zgodnie z obowiązującym prawem i Ogólnym Regulaminem Sprzedaży danego Przewoźnika i mogą być odliczane od zapłaconej Ceny. Wymienione zwroty środków (jeżeli należne) oraz Ogólny Regulamin Sprzedaży może być wskazany na Platformie, Witrynie internetowej Osoby Trzeciej (w zależności od okoliczności), lub w kopii Biletu autobusowego przesłanej do Ciebie pocztą elektroniczną.

W odpowiednich przypadkach możesz zwrócić Bilet autobusowy:

(i) na swoim Koncie lub 

(ii) klikając link w wiadomości e-mail z twoim potwierdzeniem rezerwacji Biletu autobusowego.

### **7.3. Prawo do odstąpienia od umowy**

Akceptując niniejszy Regulamin, wyraźnie akceptujesz, że umowa pomiędzy Tobą a BlaBlaCar, polegająca na powiązaniu z innym Członkiem, ma zostać zawarta przed upływem terminu odstąpienia od Potwierdzenia Rezerwacji i wyraźnie zrzekasz się prawa do odstąpienia od umowy, zgodnie z postanowieniami art. 27 Ustawy z dnia 30 maja 2014 r. O prawach konsumenta.

**8\. Zachowanie użytkowników Platformy i jej Członków**
--------------------------------------------------------

### **8.1. Zobowiązania wszystkich użytkowników Platformy**

Użytkownik ponosi wyłączną odpowiedzialność za przestrzeganie wszystkich przepisów prawnych, regulacji i obowiązków dotyczących korzystania z Platformy.

Ponadto, w odniesieniu do korzystania z Platformy i podczas Przejazdów, Użytkownik zobowiązuje się, że:

(i) nie będzie wykorzystywał Platformy do jakichkolwiek celów zawodowych, handlowych lub umożliwiających uzyskiwanie zysków;

(ii) nie będzie przekazywał BlaBlaCar (w szczególności podczas tworzenia lub aktualizacji swojego Konta) lub innym Użytkownikom jakichkolwiek informacji nieprawdziwych, wprowadzających w błąd bądź mających na celu popełnienie oszustwa;

(iii) nie będzie zamieszczał na Platformie jakichkolwiek wypowiedzi lub postów (w tym Wiadomości) zawierających treści o charakterze oszczerczym, obraźliwym, obscenicznym, pornograficznym, wulgarnym, agresywnym, niedozwolonym, gwałtownym, stanowiącym groźby lub przypadki molestowania, o charakterze rasistowskim bądź ksenofobicznym, posiadających podłoże seksualne, nakłaniające do przemocy, dyskryminacji bądź nienawiści, zachęcającym do korzystania z substancji niedozwolonych, a w ujęciu bardziej ogólnym niezgodnych z prawem, niezgodnych z właściwymi przepisami obowiązującego prawa oraz niniejszym Regulaminem oraz celami działalności Platformy oraz mogącymi przynosić szkody dla BlaBlaCar, bądź jakichkolwiek stron trzecich, a także niezgodnych z dobrymi obyczajami.

(iv) nie będzie naruszał jakichkolwiek praw BlaBlaCar oraz jej wizerunku, a w szczególności jej praw własności intelektualnej;

(v) nie będzie otwierał więcej, niż jednego Konta na Platformie oraz nie będzie otwierał Konta w imieniu jakiejkolwiek innej osoby;

(vi) nie będzie podejmował jakichkolwiek prób obchodzenia dostępnego na Platformie internetowego systemu rezerwacji, w szczególności poprzez przesyłanie innym Użytkownikom swoich danych kontaktowych w celu przeprowadzania rezerwacji poza Platformą i niepłacenia Opłaty Serwisowej;

(vii) nie będzie kontaktował się z innymi Użytkownikami, w szczególności za pośrednictwem Platformy, w żadnych innych celach, niż określenie warunków wspólnego wykorzystywania samochodów;

(viii) nie będzie przyjmował lub dokonywał jakichkolwiek płatności poza Platformą, za wyjątkiem przypadku dozwolonych postanowieniami niniejszego Regulaminu;

(ix) będzie przestrzegał wszystkich postanowień niniejszego Regulaminu.

### **8.2. Zobowiązania Kierowców**

Ponadto, korzystając z Platformy jako Kierowca, Użytkownik zobowiązuje się do:

(i) przestrzegania wszystkich przepisów, regulacji i kodeksów obowiązujących w odniesieniu do prowadzenia pojazdów, a w szczególności posiadania ubezpieczenia od odpowiedzialności cywilnej, obowiązującego na dzień Przejazdu oraz ważnego prawa jazdy;

(ii) upewnienia się, że warunki posiadanego ubezpieczenia obejmują wspólne wykorzystywanie pojazdu, a jego Pasażerowie są uważani za strony trzecie przebywające w pojeździe i w związku z tym są objęci ochroną ubezpieczeniową;

(iii) niepodejmowania jakiegokolwiek ryzyka podczas jazdy oraz niezażywania żadnych produktów, które mogłyby naruszać jego zdolność do zachowania koncentracji oraz prowadzenia pojazdu w sposób uważny i całkowicie bezpieczny;

(iv) zamieszczania Ogłoszeń o wspólnym przejeździe odpowiadających wyłącznie rzeczywiście planowanym Wspólnym Przejazdom;

(v) realizacji Wspólnego Przejazdu w sposób określony w Ogłoszeniu o wspólnym przejeździe (w szczególności w odniesieniu do korzystania z autostrad lub nie) oraz przestrzegania terminów i miejsc uzgodnionych z innymi Użytkownikami Platformy (w szczególności w zakresie miejsc spotkań oraz zakończenia Wspólnych Przejazdów);

(vi) niezabierania większej liczby Pasażerów, niż wynosi określona w Ogłoszeniu o wspólnym przejeździe liczba Miejsc;

(vii) korzystania z pojazdu znajdującego się w dobrym stanie, spełniającego wszystkie obowiązujące przepisy prawa i praktyki, a w szczególności posiadającego ważny certyfikat kontroli technicznej (potwierdzający, że pojazd nadaje się do jazdy).

(viii) przekazania firmie BlaBlaCar lub każdemu Pasażerowi, który tego zażąda, informacji dotyczących jego prawa jazdy, dowodu rejestracyjnego samochodu, ubezpieczenia, certyfikatu kontroli technicznej oraz wszelkich innych dokumentów potwierdzających jego uprawnienia do prowadzenia pojazdu jako Kierowca zarejestrowany na Platformie;

(ix) w razie opóźnienia lub zmiany terminu Wspólnego Przejazdu, do natychmiastowego zawiadomienia o tym swoich Pasażerów;

(x) w razie Wspólnego Przejazdu międzynarodowego, posiadania i przedstawienia w razie konieczności Pasażerom lub uprawnionym władzom, które mogą tego zażądać, wszelkich dokumentów wymaganych w celu kontroli jego tożsamości oraz praw przekroczenia granicy;

(xi) oczekiwania na Pasażerów w uzgodnionym miejscu spotkania przez co najmniej 15 minut po uzgodnionej godzinie;

(xii) niezamieszczania jakichkolwiek Ogłoszeń o Wspólnym przejeździe dotyczących pojazdów, których nie jest właścicielem lub których nie może wykorzystywać dla celów Wspólnych Przejazdów;

(xiii) zapewnienia możliwości kontaktu ze strony Pasażerów za pośrednictwem numeru telefonu zamieszczonego na jego profilu;

(xiv) niewymagania żadnego kodu zatwierdzenia przed wysadzeniem Pasażera;

(xv) nieuzyskiwania jakichkolwiek zysków za pośrednictwem Platformy;

(xvi) wykazania, że nie ma żadnych przeciwwskazań medycznych w odniesieniu do prowadzenia pojazdów;

(xvii) zachowywania się podczas Wspólnego Przejazdu w sposób właściwy i odpowiedzialny, zgodnie z duchem wspólnych przejazdów;

(xviii) nieodrzucania jakichkolwiek Rezerwacji na podstawie kryteriów dotyczących rasy, koloru skóry, pochodzenia etnicznego lub narodowościowego, wyznania, orientacji seksualnej, stanu cywilnego, niepełnosprawności, wyglądu fizycznego, ciąży, niekorzystnej sytuacji finansowej, nazwiska, miejsca zamieszkania, stanu zdrowia, przekonań politycznych lub wieku.

### **8.3. Zobowiązania Pasażerów**

**8.3.1. Wspólne przejazdy**

Korzystając z Platformy jako Pasażer, Użytkownik zobowiązuje się do:

(i) właściwego zachowania podczas Wspólnego Przejazdu, nie powodując jakiegokolwiek rozproszenia uwagi Kierowcy lub zakłócenia prowadzenia przez niego pojazdu, a także spokoju pozostałych Pasażerów;

(ii) poszanowania pojazdu Kierowcy i zachowania jego czystości;

(iii) w razie opóźnienia, natychmiastowego zawiadomienia o tym Kierowcy;

(iv) zapłaty Kierowcy uzgodnionej kwoty Udziału w Kosztach;

(v) oczekiwania na Kierowcę w uzgodnionym miejscu spotkania przez co najmniej 15 minut po uzgodnionej godzinie;

(vi) okazania firmie BlaBlaCar lub dowolnemu Kierowcy, który tego zażąda, swojego dowodu osobistego lub jakiegokolwiek innego dokumentu potwierdzającego jego tożsamość;

(vii) nieprzewożenia podczas Wspólnego Przejazdu jakichkolwiek produktów, towarów, substancji lub zwierząt, które mogłyby powodować zakłócenie prowadzenia pojazdu lub rozproszenie uwagi Kierowcy, bądź też których posiadanie lub przewożenie jest niezgodne z obowiązującymi przepisami;

(viii) w razie Wspólnego Przejazdu międzynarodowego, posiadania i przedstawienia w razie konieczności Kierowcy lub uprawnionym władzom, które mogą tego zażądać, wszelkich dokumentów wymaganych w celu kontroli jego tożsamości oraz praw przekroczenia granicy;

(ix) zapewnienia możliwości kontaktu ze strony Pasażerów za pośrednictwem telefonu na numer zamieszczony na jego profilu, w tym także w uzgodnionym miejscu spotkania;

W przypadku, jeżeli Użytkownik dokonał Rezerwacji jednego lub większej liczby Miejsc na rzecz osób trzecich w sposób zgodny z postanowieniami Artykułu 4.2.3, jest on zobowiązany do zapewnienia przestrzegania niniejszego Artykułu oraz wszystkich innych postanowień tego Regulaminu przez te osoby trzecie. BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub zakończenia obowiązywania niniejszego Regulaminu w przypadku naruszenia jego postanowień przez jakąkolwiek osobę trzecią, na której rzecz Użytkownik dokonał rezerwacji Miejsca w sposób określony w tym Regulaminie. zgodnie z Artykułem 9 niniejszego Regulaminu.

**8.3.2. Przejazdy autobusowe**

Wszystkie kwestie dotyczące korzystania Pasażerów z Usług Transportowych uregulowane są umową pomiędzy Pasażerem a Przewoźnikiem – Ogólnym Regulaminem Sprzedaży. Zasady korzystania z tych usług dostarczone są przez Przewoźników.

**8.4. Zgłaszanie treści niewłaściwych lub niezgodnych z prawem (Mechanizm zawiadomienia i działania)**

Użytkownik może zgłaszać podejrzane, niewłaściwe lub niezgodne z prawem Treści Użytkownika lub Wiadomości w w trybie omówionym [tutaj](https://support.blablacar.com/s/article/Zg%C5%82aszanie-niedozwolonych-tre%C5%9Bci-1729197120604?language=pl).

BlaBlaCar, po otrzymaniu należytego zawiadomienia w trybie postanowień niniejszego Artykułu lub ze strony właściwych władz, niezwłocznie usunie wszelkie niezgodne z prawem Treści Użytkownika, jeżeli:

* Treści Użytkownika są w sposób oczywisty niezgodne z prawem lub obowiązującymi przepisami; lub 
* BlaBlaCar uzna takie treści za naruszające niniejszy Regulamin.

W takich przypadkach BlaBlaCar zastrzega sobie prawo do usunięcia Treści Użytkownika, które zostały zgłoszone, w trybie wystarczająco szczegółowym i przejrzystym i/lub do zawieszenia w trybie natychmiastowym zgłoszonego Konta.

Odpowiedni Użytkownik może odwoływać się od powyższych decyzji BlaBlaCar w trybie uregulowanym w Artykule 15.1 poniżej.

**9\. Ograniczenia użytkowania Platformy, zawieszenie konta, ograniczenie dostępu i rozwiązanie umowy**
-------------------------------------------------------------------------------------------------------

Użytkownik może rozwiązać umowę z BlaBlaCar w dowolnej chwili i z jakiekolwiek przyczyny, bez konieczności ponoszenia z tego tytułu żadnych kosztów. W tym celu wystarczy wybrać zakładkę “Chcę zamknąć moje konto” na stronie profilu Użytkownika.

W razie (i) naruszenia postanowień niniejszego Regulaminu, a w szczególności, ale nie wyłącznie, zobowiązań Użytkownika określonych postanowieniami Artykułów 6 i 8 powyżej, (ii) przekroczenia ograniczenia określonego w powyższym Artykule 4.3.3 lub (iii) jeżeli BlaBlaCar będzie posiadać uzasadnione podstawy, aby podejrzewać, że jest to konieczne dla zapewnienia jej bezpieczeństwa własnego oraz Użytkowników i osób trzecich, jak również w ramach prowadzonych postępowań wyjaśniających lub w celu zapobieżenia możliwości popełniania oszustw, BlaBlaCar zastrzega sobie prawo do:

(i) zakończenia obowiązywania Regulaminu wiążącego Użytkownika z firmą BlaBlaCar ze skutkiem natychmiastowym bez konieczności przesłania jakiegokolwiek zawiadomienia; i/lub

(ii) uniemożliwienia Użytkownikowi zamieszczania lub usuwania wszelkich Treści Użytkownika opublikowanych na Platformie; i/lub

(iii) ograniczenia dostępu Użytkownika do Platformy i jego możliwości korzystania z niej; i/lub

(iv) tymczasowego lub ostatecznego zawieszenia Konta Użytkownika.

Zawieszenie Konta może także oznaczać, w odpowiednich przypadkach, że Użytkownik nie otrzyma swoich nierozliczonych wypłat.

W zależności od okoliczności BlaBlaCar może też wysyłać użytkownikowi ostrzeżenie z przypomnieniem o zobowiązaniu do przestrzegania właściwych przepisów obowiązującego prawa i/lub niniejszego Regulaminu. 

Jeżeli będzie to konieczne, użytkownik zostanie zawiadomiony o zastosowaniu określonych powyżej środków, aby mógł potwierdzić swoje zobowiązanie do przestrzegania właściwych przepisów obowiązującego prawa i/lub niniejszego Regulaminu, udzielić wyjaśnień BlaBlaCar bądź zakwestionować podjętą decyzję. BlaBlaCar podejmie całkowicie we własnym zakresie oraz z należytym uwzględnieniem wszelkich okoliczności każdego przypadku oraz istotności naruszenia, decyzję dotyczącą ewentualnego anulowania zastosowanych środków.

**10\. Dane osobowe**
---------------------

W związku z wykorzystywaniem Platformy przez Użytkownika, BlaBlaCar będzie pozyskiwać i przetwarzać jego dane osobowe w sposób omówiony w [Polityce Prywatności](https://blog.blablacar.pl/about-us/privacy-policy).

**11\. Prawa własności intelektualnej**
---------------------------------------

### **11.1. Treści publikowane przez firmę BlaBlaCar**

W odniesieniu do wszelkich treści przekazywanych przez Użytkowników Platformy, BlaBlaCar jest jedynym właścicielem wszystkich praw własności intelektualnej do Usługi, Platformy, jej treści (w szczególności tekstów, zdjęć, projektów, logo, wideo, dźwięków, danych, elementów graficznych) oraz oprogramowania i baz danych zapewniających ich działanie.

BlaBlaCar udziela Użytkownikowi niewyłącznego, osobistego i niezbywalnego prawa do korzystania z Platformy i Usług, jedynie do jego własnego użytku osobistego oraz prywatnego, w celach niehandlowych i zgodnie z przedmiotem działania Platformy oraz Usług.

Użytkownik nie może wykorzystywać Platformy i Usług oraz ich treści w jakichkolwiek innych celach bez wcześniejszego uzyskania pisemnej zgody BlaBlaCar. W szczególności, Użytkownik nie ma prawa do:

(i) reprodukowania, modyfikowania, adaptowania, dystrybucji, publicznego przedstawiania i rozpowszechniania Platformy, Usług oraz ich treści, bez wcześniejszego uzyskania wyraźnej zgody BlaBlaCar;

(ii) dekompilacji i przeprowadzania inżynierii wstecznej w odniesieniu do Platformy oraz Usług, za wyjątkiem przypadków określonych obowiązującymi przepisami;

(iii) ekstrakcji lub podejmowania prób ekstrakcji (w szczególności przy wykorzystaniu jakichkolwiek robotów pozyskujących dane lub innych podobnego rodzaju narzędzi wykorzystywanych w celu zbierania danych) istotnej części danych zamieszczonych na Platformie.

### **11.2. Treści zamieszczane przez użytkownika na Platformie**

Aby zapewnić możliwość świadczenia Usług w sposób zgodny z przedmiotem działania Platformy, Użytkownik przyznaje firmie BlaBlaCar niewyłączną licencję dotyczącą wykorzystywania treści i danych przekazywanych przez niego w ramach korzystania z Usług, które mogą obejmować wnioski o Rezerwacje, Ogłoszenia o Wspólnych Przejazdach oraz komentarze, dane biograficzne, zdjęcia, recenzje i odpowiedzi na recenzje (dalej zwane ”Treściami Użytkownika”), a także Wiadomości. Aby umożliwić firmie BlaBlaCar prowadzenie dystrybucji za pośrednictwem sieci cyfrowej i przy wykorzystaniu dowolnych protokołów komunikacyjnych (w szczególności Internetu i sieci mobilnych) oraz publicznego udostępniania treści Platformy, Użytkownik zezwala BlaBlaCar na reprodukowanie, przedstawianie, adaptowanie i tłumaczenie jego Treści Użytkownika na następujących warunkach:

(i) Użytkownik zezwala BlaBlaCar na reprodukowanie jego Treści Użytkownika w całości lub w części przy użyciu wszelkich cyfrowych nośników zapisu danych, zarówno znanych obecnie, jak i nieznanych, a w szczególności na serwerach, twardych dyskach, kartach pamięci lub jakichkolwiek innych nośnikach podobnego rodzaju, w dowolnym formacie i przy użyciu wszystkich procesów, zarówno znanych obecnie, jak i nie, w zakresie niezbędnym dla przechowywania, wykonywania kopii zapasowych, transmisji lub pobierania danych w celach związanych z działalnością Platformy oraz świadczeniem Usług;

(ii) Użytkownik zezwala firmie BlaBlaCar na adaptację i tłumaczenie jego Treści Użytkownika oraz reprodukowanie tych adaptacji na dowolnych nośnikach cyfrowych, zarówno dostępnych obecnie, jak i w przyszłości, o których mowa w powyższym punkcie (i), w celu świadczenia Usług, w szczególności w różnych językach. Prawo to obejmuje między innymi opcję przeprowadzania modyfikacji formatu Treści Użytkownika, z zastrzeżeniem przysługujących mu praw moralnych, w celu zapewnienia przestrzegania karty tożsamości graficznej Platformy i/lub zapewnienia jej kompatybilności technicznej dla umożliwienia publikacji treści na Platformie.

Użytkownik pozostaje odpowiedzialny za oraz posiada wszelkie prawa do Treści Użytkownika oraz Wiadomości publikowanych na naszej Platformie.

**12\. Rola BlaBlaCar**
-----------------------

Platforma stanowi dostępne w sieci Internet narzędzie umożliwiające jej Użytkownikom tworzenie i zamieszczanie Ogłoszeń dotyczących Przejazdów w celu wspólnych przejazdów. Ogłoszenia te mogą być w szczególności wyświetlane przez innych Użytkowników w celu sprawdzenia warunków dotyczących Przejazdu, a także – w przypadkach, których dotyczy – bezpośredniej rezerwacji Miejsca w pojeździe Użytkownika, który zamieścił Ogłoszenie, lub rezerwować Bilety autobusowe na Platformie lub na Witrynie Internetowej Strony Trzeciej.

Korzystając z Platformy i akceptując niniejszy Regulamin, Użytkownik uznaje, że BlaBlaCar nie jest stroną jakiejkolwiek umowy zawieranej przez niego z innymi Użytkownikami w celu wspólnego poniesienia kosztów Przejazdu.

BlaBlaCar nie sprawuje żadnej kontroli zachowania Użytkowników oraz pozostałych użytkowników Platformy, lub Przewoźników i ich agentów. Nie jest właścicielem, nie prowadzi eksploatacji, ani nie zarządza pojazdami stanowiącymi przedmiot Ogłoszeń i nie oferuje żadnych Przejazdów na Platformie.

Użytkownik uznaje, że BlaBlaCar nie kontroluje w żaden sposób prawidłowości i prawdziwości Ogłoszeń, Miejsc i Przejazdów oraz ich zgodności z obowiązującymi przepisami. Działając jedynie jako pośrednik w ramach wspólnych przejazdów, BlaBlaCar nie świadczy żadnych usług transportowych i nie prowadzi działalności Przewoźnika – rola BlaBlaCar jest ograniczona wyłącznie do zapewnienia dostępu do Platformy.

Użytkownicy (Kierowcy lub Pasażerowie) i Przewoźnicy działają wyłącznie na własną odpowiedzialność.

Działając jedynie jako pośrednik, BlaBlaCar nie ponosi żadnej odpowiedzialności za rzeczywistą realizację Przejazdu, a w szczególności za:

(i) zamieszczenie przez Kierowcę lub Przewoźnika w jego Ogłoszeniu nieprawdziwych informacji lub ich przekazanie w jakikolwiek inny sposób w odniesieniu do Przejazdu oraz jego warunków;

(ii) odwołania lub modyfikacji Przejazdu przez Użytkownika;

(iii) brak płatności Udziału w Kosztach przez Pasażera;

(iv) zachowanie Użytkowników podczas Przejazdu, przed nim lub po jego zakończeniu.

W odniesieniu do Przejazdów autobusowych, Użytkownik rozumie, że ponieważ BlaBlaCar nie świadczy usług transportowych, nie zapewnia gwarancji i nie ponosi odpowiedzialności za jakiekolwiek wady usług transportowych świadczonych przez Przewoźników, takie jak opóźnienia, awarie sprzętu, wypadki, jakiekolwiek szkody, straty i szkody lub inne nieprzewidywalne zdarzenia przed, w trakcie lub po Przejeździe autobusem.

**13\. Działalność, dostępność i funkcje Platformy**
----------------------------------------------------

BlaBlaCar dołoży wszelkich starań, aby zapewnić dostępność Platformy przez 7 dni w tygodniu i 24 godziny na dobę. Niemniej jednak, dostęp do Platformy może zostać tymczasowo zawieszony bez jakiegokolwiek wcześniejszego zawiadomienia ze względów dotyczących przeprowadzenia czynności konserwacji technicznej, migracji lub aktualizacji, a także nieprawidłowego działania sieci lub innych uwarunkowań związanych z jej wykorzystywaniem.

Ponadto BlaBlaCar zastrzega sobie prawo do zmiany Platformy lub jej części dla niektórych użytkowników w celu przetestowania nowych funkcji i zapewnienia lepszego doświadczenia użytkownika, a także do modyfikacji lub zawieszenia całości lub części dostępu do Platformy lub jej funkcjonalności, według własnego uznania, tymczasowo lub na stałe.

**14\. Modyfikacja Regulaminu**
-------------------------------

Niniejszy Regulamin oraz dokumenty, do których zawiera odniesienia stanowią całość umowy zawieranej przez Użytkownika z BlaBlaCar w odniesieniu do korzystania z Usług. Jakiekolwiek inne dokumenty, a w szczególności informacje zamieszczane na Platformie (odpowiedzi na często zadawane pytania itd.) mają jedynie znaczenie poglądowe.

BlaBlaCar może zmodyfikować niniejszy Regulamin w celu jego adaptacji do nowych osiągnięć technologicznych oraz handlowych, a także spełnienia wymogów obowiązujących przepisów. Jakiekolwiek modyfikacje niniejszego Regulaminu zostaną opublikowane na Platformie wraz z określeniem terminu ich wejścia w życie, a Użytkownik zostanie o nich powiadomiony przez firmę BlaBlaCar przed tym terminem.

**15\. Prawo właściwe – Rozstrzyganie sporów**
----------------------------------------------

Niniejszy Regulamin został opracowany w języku polskim i podlega prawodawstwu polskiemu.

**15.1 Wewnętrzny system obsługi reklamacji**

Użytkownik może odwoływać się od naszych decyzji podejmowanych w następującym zakresie:

* Treści Użytkownika: na przykład, usunęliśmy, ograniczyliśmy widoczność lub odmówiliśmy usunięcia wszelkich Treści Użytkownika przekazanych podczas użytkowania Platformy lub
* Konto Użytkownika: zawiesiliśmy dostęp Użytkownika do Platformy,

jeżeli podejmowaliśmy takie decyzje na podstawie faktu, że Treści Użytkownika stanowią treści niezgodne z prawem lub z niniejszym Regulaminem. Tryb odwoławczy omówiono [tutaj](https://support.blablacar.com/s/article/Jak-odwo%C5%82a%C4%87-si%C4%99-od-decyzji-o-usuni%C4%99ciu-tre%C5%9Bci-lub-zawieszeniu-konta-1729197123037?language=pl).

**15.2 Pozasądowy tryb rozstrzygania sporów**

W razie potrzeby, Użytkownik może również przedstawić skargi dotyczące działania naszej Platformy lub Usług w celu rozstrzygnięcia sporu za pośrednictwem platformy udostępnionej przez Komisję Europejską pod adresem [tutaj](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.show&lng=FR). W takim przypadku Komisja Europejska prześle skargę Użytkownika do rzecznika praw obywatelskich w jego kraju. Zgodnie z przepisami obowiązującymi w odniesieniu do mediacji, użytkownik jest zobowiązany – przed złożeniem jakiegokolwiek wniosku dotyczącego mediacji – do zawiadomienia BlaBlaCar o wystąpieniu jakiegokolwiek sporu w celu znalezienia rozwiązania polubownego.

Użytkownik może również wystąpić z wnioskiem do organu rozstrzygania sporów we własnym kraju (listę można znaleźć [tutaj](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)). 

**16\. Informacje prawne**
--------------------------

Wydawcą Platformy jest Comuto SA, spółka z ograniczoną odpowiedzialnością z kapitałem zakładowym w wysokości 155 880,999 euro, wpisana do Rejestru Handlowego Spółek miasta Paryża pod numerem 491.904.546 (numer identyfikacji podatkowej wewnątrzwspólnotowego VAT: FR76491904546), z siedzibą pod adresem 84, avenue de la République, 75011 Paryż (Francja), reprezentowana przez swojego Dyrektora Naczelnego, pana Nicolas’a Brusson, Dyrektora Publikacji Internetowej.

Witryna internetowa znajduje się na serwerach Google Ireland Limited z siedzibą w Gordon House, Barrow Street, Dublin 4 (Irlandia).

Comuto SA jest zarejestrowana w Rejestrze Operatorów Turystycznych pod numerem: IM075180037.

Gwarancji finansowej udziela: Groupama Assurance-Crédit & Warning, 8-10 rue d’Astorg, 75008 Paryż — Francja.

Ubezpieczenie odpowiedzialności cywilnej zawodowej zawarte jest z następującym podmiotem: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paryż – Francja.

Comuto SA jest zarejestrowana w Rejestrze Pośredników Ubezpieczeniowych oraz bankowo-finansowym pod numerem rejestrowym (Orias) 15003890.

W razie jakichkolwiek pytań, prosimy o kontakt z firmą Comuto SA za pośrednictwem tego [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl).

**17\. Digital Services Act (Akt o usługach cyfrowych)**
--------------------------------------------------------

**17.1.  Informacje na temat średniej miesięcznej liczby aktywnych odbiorców usługi w UE**

Zgodnie z Artykułem 24.2 Rozporządzenia Parlamentu Europejskiego i Rady w sprawie rynku wewnętrznego usług cyfrowych zmieniającego dyrektywę 2000/31/WE („DSA”), dostawcy platform online mają obowiązek publikować informacje o średniej miesięcznej liczbie aktywnych odbiorców ich usługi w UE, obliczonej jako średnia z ostatnich sześciu miesięcy. Celem publikacji jest ustalenie, czy dostawca platformy online spełnia kryterium „bardzo dużych platform online” w rozumieniu DSA, tj. czy przekracza próg średniej miesięcznej liczby aktywnych odbiorców w UE na poziomie 45 milionów.

Na dzień 17 sierpnia 2024 r. średnia miesięczna liczba aktywnych odbiorców usługi BlaBlaCar za okres od lutego do lipca 2024 r. obliczona zgodnie z Punktem 77 i Artykułem 3 DSA (licząc użytkowników, którzy mieli styczność z treścią Platformy tylko raz w okresie 6 miesięcy), zanim wydano odpowiedni akt delegowany, wynosiła około 3,28 mln w UE.

Informację publikuje się wyłącznie w celu dostosowania się do wymogów DSA i nie należy na niej polegać dla żadnych innych celów. Dane będą aktualizowane co najmniej raz na sześć miesięcy. Nasze podejście do obliczeń przedmiotowej wartości może ulegać zmianie lub może wymagać zmiany wraz z upływem czasu z powodu, na przykład, zmian produktowych bądź nowych technologii.

**17.2. Punkt kontaktu dla organów**

Zgodnie z Artykułem 11 DSA przedstawiciele właściwych organów UE, Komisji Europejskiej lub Europejskiej Rady Usług Cyfrowych  mogą kontaktować się z nami w sprawach DSA za pośrednictwem adresu e-mail [\[email protected\]](https://blog.blablacar.pl/cdn-cgi/l/email-protection).

Można się z nami kontaktować w języku angielskim i francuskim.

Zastrzega się, że ten adres e-mail nie jest przeznaczony do komunikacji z użytkownikami. Wszelkie pytania dotyczące użytkowania Platformy BlaBlaCar Użytkownik może przesyłać za pośrednictwem [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl).

* * *

_Regulamin obowiązuje od 19 kwietnia 2024_

**1\. Przedmiot**
-----------------

Comuto SA opracowała platformę umożliwiającą wspólne przejazdy, dostępną na witrynie internetowej pod adresem [www.blablacar.pl](http://www.blablacar.pl/) lub w postaci aplikacji na urządzenia mobilne (dalej „**Platforma**”). Celem platformy jest (i) umożliwienie nawiązywania kontaktu pomiędzy kierowcami podróżującymi do określonego miejsca docelowego a pasażerami zamierzającymi udać się w tym samym kierunku, w celu zapewnienia im możliwości wspólnego Przejazdu i podziału związanych z nim kosztów oraz (ii) umożliwienie znalezienia i pozyskania informacji dotyczących podróży autobusem publikowanych przez profesjonalnych Przewoźników dostarczających usługi transportowe rezerwacji i kupna biletu autobusowego. Platforma jest obsługiwana przez Comuto SA w częsci dotyczącej przejazdów wspólnych oraz przez Busfor.pl sp. z o.o w częsci dotyczącej rezerwacji biletów autobusowych. Comuto SA i Busfor.pl sp. z o.o. są dalej zwane “**BlaBlaCar**”. 

W niektórych przypadkach, w zależności od okoliczności, możesz zostać przekierowany na witrynę internetową prowadzoną przez Busfor.pl Sp. z o.o., aby dokończyć zakup Biletu autobusowego (dalej zwaną „**Witryną Internetową Strony Trzeciej**”). Prosimy zwrócić uwagę, że Busfor.pl Sp. z o. o. w tym przypadku działa jako oddzielny administrator danych.

Witryna Internetowa Strony trzeciej posiada swoją własną politykę prywatności i regulamin. BlaBlaCar nie ponosi żadnej odpowiedzialności i nie akceptuje roszczeń za treść i aktywności dokonywanych na Witrynie Internetowej Strony Trzeciej. BlaBlaCar poleca, przed użyciem Witryny Internetowej Strony Trzeciej, zapoznanie się z polityką prywatności i regulaminem na Witrynie Strony Trzeciej.

Przedmiotem niniejszego Regulaminu jest zdefiniowanie zasad dostępu oraz użytkowania Platformy. Prosimy o dokładne zapoznanie się z nimi. BlaBlaCar nie jest stroną jakichkolwiek porozumień, umów lub relacji umownych jakiejkolwiek natury pomiędzy Użytkownikami Platformy. Do niektórych usług na Platformie mogą mieć zastosowanie dodatkowe warunki, takie jak warunki dotyczące konkretnego wydarzenia, działania lub promocji, i takie dodatkowe warunki zostaną ujawnione w odpowiednim trybie.

Jeśli korzystasz ze swojego Konta w celu zalogowania się na platformę BlaBlaCar innego kraju (na przykład, www.blablacar.com.br), należy pamiętać, że (i) regulamin tej platformy, (ii) polityka prywatności tej platformy oraz (iii) przepisy ustawowe tego kraju mają zastosowanie. Oznacza to również, że informacje o Koncie, w tym dane osobowe, mogą być przekazywane do podmiotu prawnego obsługującego tę inną platformę. Należy zwrócić uwagę, że BlaBlaCar zastrzega sobie prawo do ograniczenia dostępu do Platformy użytkownikom zasadnie zidentyfikowanym jako znajdującym się poza terytorium Unii Europejskiej.

Niniejszym akceptujesz, że BlaBlaCar nie jest stroną żadnych umów i stosunków umownych, które mogą powstawać pomiędzy Użytkownikami Platformy lub pomiędzy Użytkownikiem a Przewoźnikiem.

Klikając “Zaloguj się przez Facebook” lub “Zarejestruj się, używając swojego adresu e-mail”, a także przy zakupie Biletu autobusowego na Platformie bez rejestracji lub autoryzacji na Platformie Użytkownik oświadcza, że zapoznał się i zaakceptował niniejszy Regulamin.

**2\. Definicje**
-----------------

W niniejszym Regulaminie:

„**Bilet autobusowy**” oznacza dokument wydany Użytkownikowi (Pasażerowi) po dokonaniu Rezerwacji Przejazdu autobusem, potwierdzający zawarcie umowy przewozu z określonym Przewoźnikiem za pośrednictwem Platformy, uregulowany w Ogólnym Regulaminie Sprzedaży, z zastrzeżeniem jakiegokolwiek szczególnego warunku dodatkowo określonego pomiędzy Pasażerem a Przewoźnikiem i wyszczególnionego na Bilecie;

„**BlaBlaCar**” ma znaczenie określone w Artykule 1 powyżej;

„**Cena**” oznacza, w przypadku danego Przejazdu autobusem, cenę zawierającą wszystkie podatki, opłaty i koszty odpowiednich usług, uiszczaną przez Klienta na Platformie w momencie walidacji zakupu Biletu Autobusowego, za Miejsce na danym Przejeździe autobusem;

„**Kierowca**” oznacza Użytkownika korzystającego z Platformy w celu oferowania innym osobom możliwości ich przewozu w zamian za Udział w Kosztach w odniesieniu do określonego Przejazdu, w terminie określanym wyłącznie przez Kierowcę;

„**Klient**” oznacza każdą osobę fizyczną (Użytkownika lub nie), kupującą dla siebie lub w imieniu innej osoby, która będzie Pasażerem, Bilet autobusowy za pośrednictwem Platformy w celu wykonania Przejazdu autobusem realizowanego przez Przewoźnika;

„**Konto**” oznacza konto, które musi zostać założone w celu zostania Użytkownikiem i uzyskania dostępu do niektórych usług oferowanych na Platformie;

„**Konto w serwisie Facebook**” ma znaczenie określone w Artykule 3.2 poniżej;

„**Miejsce**” oznacza miejsce siedzące zarezerwowane przez Pasażera w pojeździe Kierowcy lub w pojeździe Przewoźnika;

„**Odcinek**” ma znaczenie określone w Artykule 4.1 poniżej;

„**Ogłoszenie o wspólnym przejeździe**” oznacza ogłoszenie dotyczące Przejazdu, zamieszczone na Platformie przez Kierowcę;

„**Ogłoszenie przejazdu autobusem**” oznacza ogłoszenie dotyczące przejazdu autobusem Przewoźnika Autobusowego zamieszczone na Platformie;

„**Ogólny Regulamin Sprzedaży**” oznacza Ogólny Regulamin Sprzedaży Przewoźnika w zależności od wybranego przez Klienta Przejazdu autobusem oraz szczególne warunki dostępne u Przewoźnika, które zostały uznane za przeczytane przez Klienta przed złożeniem zamówienia;

„**Opłata serwisowa**” ma znaczenie nadane jej w Artykule 5.2 poniżej;

„**Pasażer**” oznacza Użytkownika, który zaakceptował ofertę przewozu ze strony Kierowcy lub Przewoźnika, lub – jeżeli dotyczy – osobę, na rzecz której Użytkownik zarezerwował Miejsce;

„**Platforma**” ma znaczenie określone w Artykule 1 powyżej;

„**Potwierdzenie Rezerwacji**” ma znaczenie określone w Artykule 4.2.1 poniżej;

„**Przejazd**” oznacza Wspólny Przejazd, lub Przejazd Autobusem;

„**Przejazd Autobusem**” oznacza podróż, która jest przedmiotem Ogłoszenia Przejazdu Autobusem opublikowana na Platformie;

„**Przewoźnik**” oznacza podmiot oferujący profesjonalne Usługi transportowe, którego Bilety Autobusowe dystrybuowane są na Platformie;

„**Regulamin**” oznacza niniejszy Regulamin;

„**Rezerwacja**” ma znaczenie określone w Artykule 4.2.1 poniżej;

„**Treści Użytkownika**” mają znaczenie określone w Artykule 11.2 poniżej;

„**Udział w Kosztach**” oznacza, w odniesieniu do określonego Przejazdu, sumę pieniędzy wymaganą przez Kierowcę i zaakceptowaną przez Pasażera jako jego udział w kosztach Przejazdu;

„**Usługi**” oznaczają wszystkie usługi świadczone przez BlaBlaCar za pośrednictwem Platformy;

„**Usługi transportowe**” oznaczają usługi transportowe subskrybowane przez Pasażera Przejazdu autobusem i świadczone przez Przewoźnika;

„**Użytkownik**” oznacza dowolną osobę, która założyła Konto na Platformie;

„**Witryna internetowa**” oznacza witrynę dostępną pod adresem www.blablacar.pl;

„**Wspólny Przejazd**” oznacza podróż stanowiącą przedmiot Ogłoszenia o wspólnym przejeździe zamieszczonego przez Kierowcę na Platformie, na którego podstawie wyraża on zgodę na przewóz Pasażerów w zamian za Udział w Kosztach.

**3\. Rejestracja na Platformie i założenie Konta**
---------------------------------------------------

### **3.1. Warunki rejestracji na Platformie**

Platforma może być wykorzystywana przez osoby, które ukończyły 18 lat. Dokonanie rejestracji na Platformie przez osobę niepełnoletnią jest całkowicie zabronione. Poprzez dostęp, korzystanie lub rejestrację na Platformie Użytkownik oświadcza, że ma ukończone 18 lat.

### **3.2. Założenie Konta**

Platforma umożliwia Użytkownikom zamieszczanie i wyświetlanie Ogłoszeń o wspólnym przejeździe oraz wzajemną interakcję celem rezerwacji Miejsca, a także przeglądanie ogłoszeń o Przejazdach autobusowych i kupowanie Biletów autobusowych. Ogłoszenia mogą być również wyświetlane przez osoby, które nie są zarejestrowane na Platformie. Niemniej jednak, zamieszczenie Ogłoszenia lub przeprowadzenie rezerwacji Miejsca nie jest możliwe bez wcześniejszego założenia Konta i zostania Użytkownikiem.

W niektórych przypadkach, w zależności od okoliczności, będziesz mógł zarezerwować Miejsce na Przejazd autobusem bez zakładania Konta, ale zgodnie z niniejszym Regulaminem i warunkami Przewoźnika, za pomocą którego wybierasz się w podróż.

Aby założyć Konto, należy:

(i) wypełnić wszystkie obowiązkowe pola formularza rejestracji;

(ii) lub zalogować się do swojego konta w serwisie Facebook za pośrednictwem Platformy (dalej “Konto w serwisie Facebook”). Korzystając z tej funkcji Użytkownik zdaje sobie sprawę, że BlaBlaCar będzie posiadać dostęp do informacji znajdujących się na jego Koncie w serwisie Facebook będzie je publikować na Platformie i przechowywać. Użytkownik może w dowolnej chwili usunąć powiązanie pomiędzy jego Kontem a Kontem w serwisie Facebook, korzystając ze strony [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl). Aby uzyskać więcej informacji dotyczących wykorzystywania danych pochodzących z twojego Konta w serwisie Facebook, należy zapoznać się z postanowieniami naszej Polityki Prywatności, jak również polityką prywatności serwisu Facebook.

Aby zarejestrować się na Platformie należy wcześniej przeczytać i zaakceptować niniejszy Regulamin.

Zakładając swoje Konto – niezależnie od wybranej metody – Użytkownik zobowiązuje się do przekazania kompletnych i prawdziwych informacji oraz ich aktualizacji za pośrednictwem profilu na swoim Koncie lub poprzez zawiadomienie BlaBlaCar w celu zapewnienia ich stosowności i dokładności przez okres trwania umowy z BlaBlaCar.

W przypadku rejestracji za pośrednictwem poczty e-mail, Użytkownik zobowiązuje się do zachowania w poufności hasła wybranego podczas zakładania Konta oraz nieujawniania go osobom trzecim. W razie utraty lub ujawnienia swojego hasła, Użytkownik jest zobowiązany do natychmiastowego zawiadomienia o tym BlaBlaCar. Użytkownik ponosi pełną odpowiedzialność za wykorzystanie jego Konta przez jakiekolwiek strony trzecie, chyba że wyraźnie zawiadomił BlaBlaCar o utracie, niedozwolonym wykorzystaniu, lub ujawnieniu swojego hasła osobie trzeciej.

Użytkownik zobowiązuje się, że nie będzie tworzył lub wykorzystywał – posługując się tożsamością własną lub osoby trzeciej – żadnych innych Kont, niż utworzone pierwotnie.

### **3.3. Weryfikacja**

BlaBlaCar może, w celu zapewnienia przejrzystości, zwiększenia zaufania lub zapobiegania oszustwom lub ich wykrywania, skonfigurować system weryfikacji niektórych informacji podanych przez Ciebie w swoim profilu. Dzieje się tak zwłaszcza w przypadku, gdy podajesz swój numer telefonu lub przedstawiasz nam dokument tożsamości.

Użytkownik jest świadom i godzi się z faktem, że jakiekolwiek odniesienia na Platformie lub w ramach Usług do „zweryfikowanych” (co obejmuje „Zweryfikowany Profil”) informacji lub podobnego rodzaju wyrażenia oznaczają jedynie, że dany Użytkownik pozytywnie przeszedł procedurę weryfikacji na Platformie lub w ramach Usług, w celu przekazania bardziej szczegółowych informacji na temat Użytkownika, z którym zamierza odbyć podróż. BlaBlaCar nie może zagwarantować prawdziwości, rzetelności ani ważności informacji będących przedmiotem procedury weryfikacji.

**4\. Korzystanie z Usług**
---------------------------

### **4.1. Zamieszczanie Ogłoszeń o wspólnym przejeździe**

Jako Użytkownik, pod warunkiem spełnienia poniższych warunków, możesz tworzyć i zamieszczać na Platformie swoje Ogłoszenia o wspólnym przejeździe, podając informacje dotyczące planowanego Wspólnego Przejazdu (data/godzina, miejsce wyjazdu i przyjazdu, liczba oferowanych miejsc, dostępne opcje, wysokość Udziału w Kosztach itp.).

Zamieszczając swoje Ogłoszenie o wspólnym przejeździe, Użytkownik może określić miasta, w których zamierza się zatrzymać, aby przyjąć lub wysadzić Pasażerów. Poszczególne odcinki Wspólnego Przejazdu pomiędzy tymi miastami lub jednym z tych miast, a miejscem wyjazdu lub przyjazdu są dalej określane jako “Odcinki”.

Użytkownik może zamieszczać Ogłoszenia o wspólnym przejeździe jedynie, jeśli spełnia łącznie wszystkie określone poniżej warunki:

(i) posiada ważne prawo jazdy;

(ii) zamieszcza Ogłoszenia o wspólnym przejeździe dotyczące wyłącznie pojazdów, których jest właścicielem lub z których korzysta za wyraźną zgodą ich właściciela, a w każdym przypadku, do których wykorzystania jest uprawniony w celu odbywania wspólnych przejazdów;

(iii) jest i pozostaje głównym kierowcą pojazdu stanowiącego przedmiot Ogłoszenia o wspólnym przejeździe;

(iv) pojazd stanowi przedmiot ważnej polisy ubezpieczeniowej dotyczącej ubezpieczenia osób trzecich;

(v) nie ma żadnych przeciwwskazań medycznych w odniesieniu do prowadzenia pojazdów;

(vi) pojazd, który zamierza wykorzystać w celu realizacji Wspólnego Przejazdu to samochód osobowy posiadający 4 koła i maksymalnie 7 miejsc;

(vii) nie zamierza zamieścić innego ogłoszenia dotyczącego tego samego Wspólnego Przejazdu na Platformie;

(viii) nie oferuje większej liczby Miejsc, niż ich liczba dostępna w jego pojeździe;

(ix) wszystkie oferowane Miejsca są wyposażone w pasy bezpieczeństwa, nawet jeśli pojazd został zatwierdzony do ruchu z miejscami nieposiadającymi pasów bezpieczeństwa;

(x) korzysta z pojazdu znajdującego się w dobrym stanie, spełniającego wszystkie obowiązujące przepisy prawa i praktyki, a w szczególności posiadającego ważny certyfikat kontroli technicznej (potwierdzający, że pojazd nadaje się do jazdy);

(xi) jest konsumentem i nie działa w charakterze służbowym.

Użytkownik ponosi we własnym zakresie pełną odpowiedzialność za treść Ogłoszenia o wspólnym przejeździe zamieszczanego na Platformie. W związku z powyższym, Użytkownik oświadcza, że wszystkie informacje zamieszczone w jego Ogłoszeniu o wspólnym przejeździe są prawidłowe i prawdziwe oraz zobowiązuje się do realizacji Wspólnego Przejazdu na warunkach określonych w tym Ogłoszeniu o wspólnym przejeździe.

Pod warunkiem, że Ogłoszenie o wspólnym przejeździe zamieszczone przez Użytkownika jest zgodne z niniejszym Regulaminem, zostanie ono zamieszczone na Platformie i będzie w ten sposób widoczne dla innych Użytkowników oraz osób odwiedzających, także niebędących Użytkownikami, przeprowadzających wyszukiwanie na Platformie bądź na witrynie internetowej partnerów BlaBlaCar. BlaBlaCar zastrzega sobie prawo – na podstawie wyłącznie własnej decyzji – odmowy zamieszczenia lub usunięcia w dowolnej chwili jakiegokolwiek Ogłoszenia o wspólnym przejeździe niespełniającego warunków niniejszego Regulaminu lub jeżeli uważa, że przynosi ono jakiekolwiek szkody dla wizerunku BlaBlaCar, Platformy bądź Usług i/lub zawieszenia Konta Użytkownika zamieszczającego takie Ogłoszenia o wspólnym przejeździe w trybie postanowień Artykułu 9 niniejszego Regulaminu.

Informujemy, że w przypadku podawania się jako konsument korzystając z Platformy, podczas gdy w rzeczywistości działasz w charakterze służbowym, narażasz się na sankcje przewidziane przez obowiązujące prawo.

### **4.2. Rezerwacja Miejsca**

„BlaBlaCar” stworzył system rezerwacji Miejsc online („**Rezerwacja**”) dla Przejazdów oferowanych na Platformie. 

Metody rezerwacji Miejsca są uzależnione od rodzaju planowanego Przejazdu.

BlaBlaCar udostępnia Użytkownikom Platformy wyszukiwarkę opartą na różnych kryteriach wyszukiwania (miejsce pochodzenia, cel podróży, daty, liczba podróżnych itp.). Pewne dodatkowe funkcjonalności udostępniane są w wyszukiwarce, gdy użytkownik jest połączony ze swoim Kontem. BlaBlaCar zachęca Użytkownika, niezależnie od stosowanego procesu rezerwacji, do dokładnego przeanalizowania oferty i skorzystania z wyszukiwarki w celu ustalenia oferty najbardziej dopasowanej do jego potrzeb. Więcej informacji można znaleźć [tutaj](https://blog.blablacar.pl/about-us/transparentnosc-platform). Klient, rezerwując Przejazd autobusem offline poza Platformą, może również zwrócić się do Przewoźnika lub przedstawiciela obsługi klienta o przeprowadzenie wyszukiwania.

**4.2.1. Wspólny Przejazd**

Kiedy Pasażer jest zainteresowany w zamieszczeniu Ogłoszenia o wspólnym przejeździe, może przesłać za pośrednictwem sieci zapytanie dotyczące Rezerwacji. Zapytanie dotyczące Rezerwacji może (i) zostać zaakceptowane automatycznie (jeżeli Kierowca wybierze tę opcję podczas zamieszczania swojego Ogłoszenia o wspólnym przejeździe) lub (ii) zostać zaakceptowane ręcznie przez Kierowcę. W momencie Rezerwacji Pasażer dokonuje płatności Opłaty serwisowej, o ile ma to zastosowanie. Po zatwierdzeniu zapytania dotyczącego Rezerwacji przez Kierowcę – jeżeli dotyczy – Pasażer otrzymuje Potwierdzenie Rezerwacji (dalej “Potwierdzenie Rezerwacji”).

Jeżeli użytkownik jest Kierowcą i w chwili zamieszczenia Ogłoszenia o wspólnym przejeździe wybrał opcję akceptacji ręcznej zapytań dotyczących Rezerwacji, jest on zobowiązany do udzielania odpowiedzi na wszystkie zapytania dotyczące Rezerwacji w terminie określonym przez Pasażera w jego zapytaniu. W przeciwnym wypadku, zapytanie dotyczące Rezerwacji automatycznie wygaśnie, a Pasażerowi zwrócone zostaną wszystkie kwoty zapłacone w związku z tym zapytaniem, jeżeli istnieją.

W chwili Potwierdzenia Rezerwacji, BlaBlaCar ponownie prześle Użytkownikowi numer telefonu Kierowcy (w przypadku Użytkownika będącego Pasażerem) lub Pasażera (w przypadku Użytkownika będącego Kierowcą). Użytkownik ponosi wyłączną odpowiedzialność za wypełnienie umowy wiążącej go z innym Użytkownikiem.

**4.2.2. Przejazdy autobusem**

W przypadku Przejazdu autobusem BlaBlaCar umożliwia rezerwację Miejsc na dany Przejazd autobusowy Przewoźnika za pośrednictwem Platformy lub poprzez przekierowanie na Witrynę Internetową Strony Trzeciej. Potwierdzasz, że rezerwacja Miejsc i zakup biletów dla Przejazdów autobusem odbywa się w zależności od dostępności Miejsc.

Usługi transportowe podlegają Ogólnemu Regulaminowi Sprzedaży odpowiedniego Przewoźnika, w zależności od wybranego przez Klienta Przejazdu, który Klient musi przeczytać i zaakceptować przed dokonaniem rezerwacji i opłaceniem Miejsca. BlaBlaCar nie świadczy żadnych usług transportowych w odniesieniu do Przejazdów autobusem, Przewoźnicy są wyłącznie stroną Ogólnego Regulaminu Sprzedaży. Prosimy zapytać stosownego Przewoźnika o Ogólny Regulamin Sprzedaży, w szczególności w odniesieniu do przewozu osób niepełnoletnich i bagażu oraz odwołania Przejazdu autobusem.

Ogólny Regulamin Sprzedaży jest przedstawiany przez Przewoźników, w tym za pośrednictwem ich stron internetowych. Warunki anulowania Biletu autobusowego mogą być również prezentowane na Platformie. Wszelkie informacje o Przejazdach Autobusem, publikowane na Platformie, w tym dostępność, trasy, rozkłady jazdy i Ceny, pochodzą od Przewoźników. BlaBlaCar nie zmienia tych informacji ani nie ponosi odpowiedzialności za ich jakość.

BlaBlaCar zwraca uwagę na fakt, że niektóre Usługi transportowe oferowane przez Przewoźnika i wymienione na Platformie mogą zostać wycofane, w szczególności z powodów klimatycznych, sezonowego charakteru lub w przypadku wystąpienia siły wyższej.

Aby zarezerwować Miejsce na Przejazd autobusem i kupić Bilet autobusowy, niezależnie od tego, czy jesteś zalogowany na swoje Konto na Platformie, czy nie (w stosownych przypadkach), musisz podać stosowne dane osobowe zgodnie z naszą [Polityką prywatności](https://blog.blablacar.pl/about-us/privacy-policy). Należy pamiętać, że Polityka prywatności Przewoźnika, za pomocą której podróżujesz, może mieć również zastosowanie. BlaBlaCar nie zmienia tych informacji ani nie ponosi odpowiedzialności za niedokładność danych wprowadzonych podczas procesu rezerwacji Przejazdu autobusem.

Bilet autobusowy jest rezerwowany, a Miejsce jest rezerwowane (jeśli pozwala na to wybór miejsc) po zakończeniu płatności. Jeśli płatność nie zostanie dokonana, Bilet autobusowy zostanie ponownie wystawiony na sprzedaż, a Miejsce może zostać zarezerwowane przez innego Pasażera.

Po zakończeniu zakupu otrzymasz wiadomość e-mail z kopią Biletu autobusowego, potwierdzeniem rezerwacji i szczegółowymi informacjami na temat Przejazdu autobusem. Zachęcamy do sprawdzenia ustawień skrzynki odbiorczej swojego adresu e-mail, a w szczególności do upewnienia się, że e-mail potwierdzający nie trafia bezpośrednio do spamu.

Kwestie organizacji Przejazdu autobusem i realizacji umowy przewozu są wyłączną odpowiedzialnością Przewoźnika i muszą być rozwiązywane bezpośrednio z Przewoźnikiem. BlaBlaCar nie jest stroną jakichkolwiek umów pomiędzy Klientem, Pasażerem a Przewoźnikiem.

**4.2.3. Imienny charakter rezerwacji Miejsca i warunki korzystania z Usług na rzecz osób trzecich**

Korzystanie z Usług – zarówno w charakterze Pasażera, jak i Kierowcy – ma charakter imienny. Dane Kierowcy i Pasażera muszą odpowiadać informacji dotyczących ich tożsamości, przekazanych BlaBlaCar, Przewoźnikom (jeśli dotyczy) oraz innym uczestniczącym w Przejeździe Użytkownikom.

Niemniej jednak, w odniesieniu do Wspólnych Przejazdów, BlaBlaCar umożliwia Użytkownikom zarezerwowanie jednego lub większej liczby Miejsc na rzecz osób trzecich. W takim przypadku Użytkownik zobowiązuje się do precyzyjnego przekazania Kierowcy w chwili dokonywania Rezerwacji lub przesyłania wiadomości do Kierowcy (w razie Przejazdu bez Rezerwacji), imię i nazwiska, wiek oraz numeru telefonu osoby, na rzecz której dokonuje rezerwacji Miejsca. Absolutnie zabronione jest zarezerwowanie Miejsca do Wspólnego Przejazdu dla podróżującej samotnie osoby niepełnoletniej w wieku poniżej 13 lat. W przypadku rezerwacji Miejsca do Wspólnego Przejazdu dla podróżującej samotnie osoby niepełnoletniej w wieku powyżej 13 lat, Użytkownik zobowiązuje się do uzyskania wcześniejszej zgody Kierowcy oraz przekazania mu prawidłowo sporządzonego upoważnienia, podpisanego przez opiekunów prawnych osoby niepełnoletniej.

Ponadto, przedmiotem działalności Platformy jest dokonywanie rezerwacji Miejsc jedynie dla osób. Absolutnie zabronione jest dokonywanie rezerwacji Miejsca w celu przewozu jakichkolwiek przedmiotów, przesyłek, zwierząt podróżujących bez opieki i dowolnych materiałów.

Ponadto zabronione jest zamieszczenie Ogłoszenia w imieniu i na rzecz innego Kierowcy.

Odnośnie Przejazdów autobusem zachęcamy do uwzględnienia Ogólnego Regulaminu Sprzedaży danego Przewoźnika. Jeśli rezerwujesz Przejazd autobusem dla osoby trzeciej, bierzesz pełną odpowiedzialność za posiadanie odpowiednich uprawnień do wykonywania działań w imieniu i na rzecz takiej osoby trzeciej.

Należy pamiętać, że dzieci w wieku poniżej 16 lat mogą dokonywać Przejazdy autobusem wyłącznie pod warunkiem, że ich rodzice lub przedstawiciele prawni posiadają odpowiednie uprawnienia, udzielają im pomocy i wsparcia zgodnie z obowiązującym prawem. Rodzice lub inni przedstawiciele prawni ponoszą odpowiedzialność za wszelkie działania osób niepełnoletnich podczas korzystania z Platformy i Przejazdów ajutobusem, w tym za wszelkie nielegalne działania i / lub zaniechania ze strony osób niepełnoletnich podczas korzystania z Platformy.

W przypadku rezerwacji Miejsca na Przejazd autobusowy dla osoby poniżej 16 roku życia ponosisz wyłączną odpowiedzialność za zapewnienie tego, żeby osoba niepełnoletnia posiadała odpowiednie uprawnienia do reprezentowania takiej osoby i jej towarzyszenia , do uzyskiwania i udzielania niezbędnych zezwoleń i zgody oraz wykonywania innych obowiązków zgodnie z obowiązującym prawem.

Przed zakupem Biletu autobusowego zalecamy zapoznanie się z Ogólnym Regulaminem Sprzedaży Przewoźnika dotyczącym przewozu osób niepełnoletnich.

### **4.3. Treści Użytkownika, moderacja i system ocen**

**4.3.1. Zasady działania systemu ocen**

BlaBlaCar zachęca użytkowników do zamieszczania recenzji dotyczących Kierowców (w przypadku, jeżeli Użytkownik jest Pasażerem) lub dotyczących Pasażerów (w przypadku, jeżeli Użytkownik jest Kierowcą), z którymi korzystał z Wspólnych Przejazdów bądź miał korzystać z Przejazdów (tzn. zarezerwował Wspólny Przejazd. Niemniej jednak, Użytkownik nie może zamieszczać recenzji dotyczących innych Pasażerów, jeżeli sam jest Pasażerem lub na temat Użytkowników, z którymi nie podróżował lub nie miał podróżować. Masz możliwość wystawienia opinii w ciągu 14 od Przejazdu.

Recenzja zamieszczona przez Użytkownika lub dotycząca go recenzja zamieszczona przez innego Użytkownika – jeżeli dotyczy – jest widoczna iI publikowana na Platformie w krótszym z następujących okresów: (i) natychmiast po zamieszczeniu recenzji przez osoby wymienione powyżej lub (ii) po upływie 14 dni od pierwszej recenzji.

W niektórych przypadkach Użytkownik może udzielić odpowiedzi na recenzję zamieszczoną przez innegorofileik anaa jegorofileu w ciągu 14 dni od otrzymania takiej recenzji. Recenzja oraz odpowiedź Użytkownika zostaną zamieszczone na jegorofileu.

**4.3.2. Moderacja**

**a. Treści Użytkownika**

Użytkownik uznaje i wyraża zgodę na fakt, że BlaBlaCar może dokonywać moderacji przed publikacją, przy użyciu narzędzi automatycznych lub ręcznie, Treści Użytkownika, o których mowa w Artykule 11.2. Jeżeli BlaBlaCar uzna takie Treści Użytkownika za naruszające właściwy przepisy obowiązującego prawa lub niniejszym Regulamin, zastrzega sobie prawo do:

* uniemożliwienia publikacji lub usunięcia takich Treści Użytkownika
* wystosowania ostrzeżenia do Użytkownika z przypomnieniem o obowiązku przestrzegania właściwych przepisów obowiązującego prawa lub niniejszego Regulaminu i/lub
* zastosowania środków restrykcyjnych, o których mowa w Artykule 9 niniejszego Regulaminu.

Stosowanie przez BlaBlaCar takich automatycznych lub ręcznych narzędzi moderacji nie będzie interpretowane jako zobowiązanie do monitorowania ani zobowiązanie do aktywnego poszukiwania wszelkich przypadków czynności niezgodnych z prawem i/lub Treści Użytkownika publikowanych na Platformie. Nie będzie również, w zakresie dozwolonym przez właściwe przepisy obowiązującego prawa, stanowić podstawy do jakiejkolwiek odpowiedzialności ani zobowiązań BlaBlaCar z powyższego tytułu.

**b. Wiadomości**

Wymiana wiadomości między Użytkownikami za pośrednictwem naszej Platformy („Wiadomości”) odbywa się wyłącznie w celu wymiany informacji dotyczących Wspólnych Przejazdów.

BlaBlaCar może, przy użyciu zautomatyzowanego oprogramowania i algorytmów, wykrywać treści Wiadomości na potrzeby zapobiegania oszustwom, usprawnienia poziomu usług, wsparcia klienta oraz wykonywania umów zawartych z naszymi użytkownikami (takich jak niniejszy Regulamin). W razie wykrycia wszelkich treści w Wiadomości, które noszą znamiona oszustwa, działania niezgodnego z prawem lub działania służącego obejściu Platformy bądź są w inny sposób niezgodne z niniejszym Regulaminem:

* takie treści nie mogą zostać opublikowane
* Użytkownik, który wysłał Wiadomość, może dostać ostrzeżenie z przypomnieniem, że ma obowiązek przestrzegać właściwych przepisów obowiązującego prawa lub niniejszego Regulaminu lub
* konto Użytkownika może podlegać zawieszeniu w trybie Artykułu 9 niniejszego Regulaminu.

Stosowanie przez BlaBlaCar takich automatycznych lub ręcznych narzędzi moderacji nie będzie interpretowane jako zobowiązanie do monitorowania ani zobowiązanie do aktywnego poszukiwania wszelkich przypadków czynności niezgodnych z prawem i/lub Treści Użytkownika publikowanych na Platformie.  Nie będzie również, w zakresie dozwolonym przez właściwe przepisy obowiązującego prawa, stanowić podstawy do jakiejkolwiek odpowiedzialności ani zobowiązań BlaBlaCar z powyższego tytułu.

Użytkownik uznaje i wyraża zgodę na fakt, że BlaBlaCar zastrzega sobie prawo niezamieszczenia lub usunięcia dowolnych recenzji, pytań, komentarzy lub odpowiedzi, które uzna za niezgodne z postanowieniami niniejszego Regulaminu.

**4.3.3. Ograniczenie**

Zgodnie z Artykułem 9 niniejszego Regulaminu BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub rozwiązania stosunku prawnego obowiązującego na podstawie niniejszego Regulaminu w przypadku, jeśli (i) Użytkownik otrzymał co najmniej trzy recenzje i (ii) jedna lub średnia ocena uzyskana w ramach tych recenzji jest niższa lub równa 3 w zależności od powagi opinii pozostawionej w tym trybie przez Użytkownika.  

**5\. Warunki finansowe**
-------------------------

### **5.1. Udział w Kosztach i Cenie**

5.1.1 W przypadku Wspólnego przejazdu wartość Udziału w Kosztach jest określona przez Użytkownika będącego Kierowcą, na jego wyłączną odpowiedzialność. Absolutnie zabronione jest uzyskiwanie jakichkolwiek zysków z używania naszej Platformy. W związku z powyższym, Użytkownik zobowiązuje się do ograniczenia kwoty Udziału w Kosztach, której żąda od swoich Pasażerów do wartości kosztów rzeczywiście ponoszonych w związku z Przejazdem. W przeciwnym przypadku, Użytkownik ponosi we własnym zakresie pełne ryzyko w odniesieniu do zmiany kwalifikacji podatkowej transakcji zrealizowanej za pośrednictwem Platformy.

W chwili zamieszczenia Ogłoszenia przez Użytkownika, BlaBlaCar zasugeruje kwotę Udziału w Kosztach, przy uwzględnieniu rodzaju Przejazdu oraz jego odległości. Kwota ta stanowi wyłącznie sugestię, a Użytkownik może zwiększyć lub zmniejszyć ją na podstawie rzeczywistych kosztów ponoszonych w związku z Przejazdem. Aby uniknąć możliwości nadużyć, BlaBlaCar zastrzega sobie prawo modyfikacji wartości Udziału w Kosztach.

5.1.2 W przypadku Przejazdu autobusem Cenę za Miejsce określa Przewoźnik. Klient jest proszony o zapoznanie się z odpowiednim Ogólnym Regulaminem Sprzedaży w celu zrozumienia obowiązujących zasad dotyczących składania zamówienia Biletów.

### **5.2. Opłata serwisowa**

BlaBlaCar w zamian za korzystanie z Platformy w momencie Rezerwacji pobiera opłaty serwisowe (zwane dalej „**Opłatą serwisową**”). W razie konieczności użytkownik zostanie poinformowany przed zastosowaniem Opłaty serwisowej.

Sposób płatności Opłaty serwisowej może się różnić w zależności od używanej platformy BlaBlaCar. Dozwolone sposoby płatności Opłaty serwisowej są następujące: 

* Karta kredytowa dostarczona przez Adyen NV, spółkę publiczną zarejestrowaną w Holandii pod numerem 34259528 z siedzibą pod adresem Simon Carmiggeltstraat 6-50, 1011 DJ, Amsterdam, Holandia, lub
* Blik, dostarczony przez spółkę Polski Standard Płatności sp. z o.o. z siedzibą w Warszawie ul. Cypryjska 72, wpisaną do rejestru przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XIII Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS 0000493783, NIP: 5213664494, Statystyczny Numer Identyfikacyjny (REGON): 147055889.

Metody naliczania Opłaty serwisowej mogą zostać opublikowane przez BlaBlaCar odrębnie, mają charakter wyłącznie informacyjny i nie mają wartości umownej. Opłata serwisowa może być naliczana w zależności od różnych czynników, w szczególności długości Podróży i Udziału w kosztach. BlaBlaCar zastrzega sobie prawo do zmiany sposobu naliczania Opłaty serwisowej w dowolnym momencie. Zmiany te nie będą miały wpływu na Opłatę serwisową zaakceptowaną przez Użytkownika przed datą wejścia w życie tych zmian.

W przypadku podróży transgranicznych należy pamiętać, że metody obliczania wysokości Opłaty serwisowej oraz obowiązującego podatku VAT różnią się w zależności od miejsca odbioru i/lub celu podróży.

W przypadku korzystania z Platformy w podróżach transgranicznych lub poza granicami Polski, Opłata serwisowa może zostać pobrana przez spółkę stowarzyszoną BlaBlaCar obsługującą lokalną platformę.

### **5.3. Zaokrąglenia**

BlaBlaCar może w zależności wyłącznie od własnej decyzji zaokrąglać kwoty Udziału w Kosztach lub Opłaty serwisowej w górę lub w dół.

### **5.4. Sposoby płatności Udziału w kosztach na rzecz Kierowcy i Ceny**

Należy zwrócić uwagę, że dostawcy usług płatniczych mogą nakładać procedury i ograniczenia na niektóre transakcje ze względu na ich zobowiązania dotyczące zgodności zgodnie z własnymi regulaminami.

**5.4.1. Płatność Udziału w kosztach na rzecz Kierowcy**

Pasażer zobowiązuje się do zapłacenia Kierowcy Udziału w Kosztach najpóźniej w chwili opuszczania jego pojazdu w miejscu docelowym.

Kierowca nie może domagać się zapłaty Udziału w Kosztach w całości lub w części przed realizacją Przejazdu.

**5.4.2 Płatność Ceny** 

Płatność Ceny za każdą rezerwację Przejazdu autobusem dokonana za pośrednictwem Platformy jest dokonywana za pomocą jednego z dozwolonych poniżej sposobów:

* Karta kredytowa dostarczona przez Adyen NV, spółkę publiczną zarejestrowaną w Holandii pod numerem 34259528 z siedzibą pod adresem Simon Carmiggeltstraat 6-50, 1011 DJ, Amsterdam, Holandia, lub
* Blik, dostarczony przez spółkę Polski Standard Płatności sp. z o.o. z siedzibą w Warszawie ul. Cypryjska 72, wpisaną do rejestru przez Sąd Rejonowy dla m.st. Warszawy w Warszawie, XIII Wydział Gospodarczy Krajowego Rejestru Sądowego pod numerem KRS 0000493783, NIP: 5213664494, Statystyczny Numer Identyfikacyjny (REGON): 147055889.

Sposób płatności może się różnić w zależności od używanej platformy BlaBlaCar.

Kupując Miejsce na Przejazd autobusowy za pośrednictwem Platformy, płacisz Cenę stosownego odpowiedzialnego Przewoźnika, biorąc pod uwagę obowiązujące podatki i opłaty (jeżeli są pobierane).

Aby dokonać płatności, możesz zostać przekierowany na stronę zewnętrznego dostawcy usług płatniczych — partnera BlaBlaCar. BlaBlaCar w żadnym wypadku nie może zagwarantować, że usługi świadczone przez firmę odpowiedzialną za dokonywanie płatności będą świadczone bez błędów, zakłóceń, awarii, opóźnień lub innych wad. BlaBlaCar nie ponosi żadnej odpowiedzialności za zarządzanie, pośrednictwo lub dokonywanie płatności. W każdym przypadku BlaBlaCar nie ponosi odpowiedzialności za transakcje dokonywane między Użytkownikiem, Klientem i Przewoźnikami.

Bilet autobusowy jest wydawany Użytkownikowi tylko wtedy, gdy płatność za Cenę jest pełna, zakończona i skuteczna. Płatność musi zostać dokonana nie później niż w terminie wskazanym na Platformie. Jeśli Płatność nie zostanie dokonana w ramach wskazanego terminu, jest nieprawidłowa, niekompletna lub nie została dokonana z przyczyn leżących po stronie Klienta, rezerwacja Przejazdu Autobusowego zostanie automatycznie anulowana.

Płatność Ceny jest ostateczna. W związku z tym wszelkie zmiany spowodują wymianę lub anulowanie zgodnie z obowiązującym Ogólnym Regulaminem Sprzedaży Przewoźnika. Obowiązkiem Klienta jest dopilnowanie, aby Usługi transportowe zostały wybrane zgodnie z potrzebami i oczekiwaniami Pasażera. Za dokładność danych osobowych wprowadzonych przez Klienta odpowiada ten ostatni, z wyjątkiem przypadku, gdy zostanie udowodnione, że BlaBlaCar nie gromadzi, nie przechowuje ani nie chroni takich Danych osobowych.

Ceny mogą zostać zmienione przez Przewoźników. Od czasu do czasu na Platformie mogą być wyświetlane promocje lub oferty specjalne do tyczące Cen. Odnośnie takich promocji i ofert mogą obowiązywać dodatkowe warunki.

**6\. Niehandlowy i niebiznesowy charakter Usług oraz Platformy**
-----------------------------------------------------------------

Użytkownik zobowiązuje się do korzystania z Usług oraz Platformy jedynie w celu nawiązania kontaktów na zasadach niehandlowych i niebiznesowych z osobami zamierzającymi skorzystać ze wspólnego Przejazdu.

W kontekście Wspólnego Przejazdu przyjmujesz do wiadomości, że prawa konsumenckie wynikające z unijnego prawa ochrony konsumentów nie mają zastosowania do Twoich relacji z innymi Członkami.

Użytkownik będący Kierowcą zobowiązuje się, że nie będzie żądał Udziału w Kosztach w kwocie wyższej, niż rzeczywiście poniesione koszty jak również w celu uzyskiwania jakichkolwiek zysków, a ponadto, na zasadzie wspólnego ponoszenia kosztów, Kierowca musi ponieść własną część kosztów związanych z Przejazdem. Kierowca ponosi wyłączną odpowiedzialność za obliczenie kosztów ponoszonych w ramach Przejazdu oraz sprawdzenie, czy kwota wymagana od Pasażerów nie przekracza rzeczywiście ponoszonych kosztów (przy uwzględnieniu udziału własnego użytkownika).

BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika w przypadku, jeżeli wykorzystuje samochód firmowy, służbowy lub taksówkę generując w ten sposób zysk za pośrednictwem Platformy. Użytkownik zobowiązuje się do przedstawienia firmie BlaBlaCar na każde jej żądanie kopii dowodu rejestracyjnego jego samochodu i/lub dowolnego innego dokumentu potwierdzającego, że jest on uprawniony do korzystania z danego pojazdu na Platformie i nie uzyskuje w ten sposób jakichkolwiek zysków.

Zgodnie z Artykułem 9 niniejszego Regulaminu BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub zakończenia obowiązywania niniejszego Regulaminu w przypadku prowadzenia przez Użytkownika na Platformie jakiejkolwiek działalności, która – ze względu na charakter oferowanych Przejazdów, ich częstotliwość, liczbę przewożonych Pasażerów oraz żądany Udział w Kosztach – umożliwia Użytkownikowi uzyskiwanie zysków lub na podstawie których BlaBlaCar może podejrzewać, że Użytkownik uzyskuje zyski za pośrednictwem Platformy.

**7\. Polityka dotycząca anulowania**
-------------------------------------

### **7.1. Warunki zwrotu środków w przypadku rezygnacji**

Przedmiotem niniejszych zasad anulowania są wyłącznie Wspólne Przejazdy; BlaBlaCar nie udziela żadnej gwarancji, jakiejkolwiek natury, w przypadku anulowania z jakiegokolwiek powodu.

Anulowanie Miejsca na Wspólny przejazd przez Kierowcę lub Pasażera po Potwierdzeniu Rezerwacji podlega poniższym postanowieniom:

– W przypadku rezygnacji z winy Kierowcy, Pasażerowi zwracana jest Opłata serwisowa. Dzieje się tak zwłaszcza w przypadku, gdy Kierowca:

* Nie potwierdził prośby o Rezerwację w wyznaczonym terminie (jeżeli taka opcja została wybrana przez Kierowcę);
* Anuluje Wspólny przejazd lub nie przybył na miejsce spotkania w ciągu 15 minut od uzgodnionej godziny;

\-W przypadku odwołania z winy Pasażera Opłatę serwisową zatrzymuje BlaBlaCar

Sposób zwrotu Opłaty serwisowej ustalany jest w zależności od sposobu uiszczenia Opłaty Serwisowej, natomiast termin zwrotu Opłaty serwisowej uzależniony jest od terminów przelewów dokonywanych przez instytucje bankowe.

Jeżeli odwołanie nastąpi przed odjazdem i z winy Pasażera, Miejsce (Miejsca) odwołane przez Pasażera zostaną automatycznie udostępnione innym Pasażerom, którzy mogą dokonać rezerwacji online i odpowiednio podlegają warunkom niniejszego Regulaminu.

BlaBlaCar ocenia, według własnego uznania, na podstawie dostępnych informacji, zasadność wniosków o zwrot środków.

### **7.2. Przejazdy autobusowe**

Procedura dotycząca anulowania Przejazdu Autobusem wynika z Ogólnego Regulaminu Sprzedaży danego Przewoźnika. Możesz anulować lub zmienić rezerwację Przejazdu autobusem na Platformie lub na Witrynie Internetowej Osoby Trzeciej, w zależności od okoliczności.

Środki zostaną zwrócone zgodnie z metodą płatności stosowaną przy zakupie biletu na Przejazd autobusem w terminie określonym przez Twoj bank.

Należy pamiętać, że opłaty za wprowadzenie zmian lub anulowanie rezerwacji Przejazdu autobusowego mogą być naliczane zgodnie z obowiązującym prawem i Ogólnym Regulaminem Sprzedaży danego Przewoźnika i mogą być odliczane od zapłaconej Ceny. Wymienione zwroty środków (jeżeli należne) oraz Ogólny Regulamin Sprzedaży może być wskazany na Platformie, Witrynie internetowej Osoby Trzeciej (w zależności od okoliczności), lub w kopii Biletu autobusowego przesłanej do Ciebie pocztą elektroniczną.

W odpowiednich przypadkach możesz zwrócić Bilet autobusowy:

(i) na swoim Koncie lub 

(ii) klikając link w wiadomości e-mail z twoim potwierdzeniem rezerwacji Biletu autobusowego.

### **7.3. Prawo do odstąpienia od umowy**

Akceptując niniejszy Regulamin, wyraźnie akceptujesz, że umowa pomiędzy Tobą a BlaBlaCar, polegająca na powiązaniu z innym Członkiem, ma zostać zawarta przed upływem terminu odstąpienia od Potwierdzenia Rezerwacji i wyraźnie zrzekasz się prawa do odstąpienia od umowy, zgodnie z postanowieniami art. 27 Ustawy z dnia 30 maja 2014 r. O prawach konsumenta.

**8\. Zachowanie użytkowników Platformy i jej Członków**
--------------------------------------------------------

### **8.1. Zobowiązania wszystkich użytkowników Platformy**

Użytkownik ponosi wyłączną odpowiedzialność za przestrzeganie wszystkich przepisów prawnych, regulacji i obowiązków dotyczących korzystania z Platformy.

Ponadto, w odniesieniu do korzystania z Platformy i podczas Przejazdów, Użytkownik zobowiązuje się, że:

(i) nie będzie wykorzystywał Platformy do jakichkolwiek celów zawodowych, handlowych lub umożliwiających uzyskiwanie zysków;

(ii) nie będzie przekazywał BlaBlaCar (w szczególności podczas tworzenia lub aktualizacji swojego Konta) lub innym Użytkownikom jakichkolwiek informacji nieprawdziwych, wprowadzających w błąd bądź mających na celu popełnienie oszustwa;

(iii) nie będzie zamieszczał na Platformie jakichkolwiek wypowiedzi lub postów (w tym Wiadomości) zawierających treści o charakterze oszczerczym, obraźliwym, obscenicznym, pornograficznym, wulgarnym, agresywnym, niedozwolonym, gwałtownym, stanowiącym groźby lub przypadki molestowania, o charakterze rasistowskim bądź ksenofobicznym, posiadających podłoże seksualne, nakłaniające do przemocy, dyskryminacji bądź nienawiści, zachęcającym do korzystania z substancji niedozwolonych, a w ujęciu bardziej ogólnym niezgodnych z prawem, niezgodnych z właściwymi przepisami obowiązującego prawa oraz niniejszym Regulaminem oraz celami działalności Platformy oraz mogącymi przynosić szkody dla BlaBlaCar, bądź jakichkolwiek stron trzecich, a także niezgodnych z dobrymi obyczajami.

(iv) nie będzie naruszał jakichkolwiek praw BlaBlaCar oraz jej wizerunku, a w szczególności jej praw własności intelektualnej;

(v) nie będzie otwierał więcej, niż jednego Konta na Platformie oraz nie będzie otwierał Konta w imieniu jakiejkolwiek innej osoby;

(vi) nie będzie podejmował jakichkolwiek prób obchodzenia dostępnego na Platformie internetowego systemu rezerwacji, w szczególności poprzez przesyłanie innym Użytkownikom swoich danych kontaktowych w celu przeprowadzania rezerwacji poza Platformą i niepłacenia Opłaty Serwisowej;

(vii) nie będzie kontaktował się z innymi Użytkownikami, w szczególności za pośrednictwem Platformy, w żadnych innych celach, niż określenie warunków wspólnego wykorzystywania samochodów;

(viii) nie będzie przyjmował lub dokonywał jakichkolwiek płatności poza Platformą, za wyjątkiem przypadku dozwolonych postanowieniami niniejszego Regulaminu;

(ix) będzie przestrzegał wszystkich postanowień niniejszego Regulaminu.

### **8.2. Zobowiązania Kierowców**

Ponadto, korzystając z Platformy jako Kierowca, Użytkownik zobowiązuje się do:

(i) przestrzegania wszystkich przepisów, regulacji i kodeksów obowiązujących w odniesieniu do prowadzenia pojazdów, a w szczególności posiadania ubezpieczenia od odpowiedzialności cywilnej, obowiązującego na dzień Przejazdu oraz ważnego prawa jazdy;

(ii) upewnienia się, że warunki posiadanego ubezpieczenia obejmują wspólne wykorzystywanie pojazdu, a jego Pasażerowie są uważani za strony trzecie przebywające w pojeździe i w związku z tym są objęci ochroną ubezpieczeniową;

(iii) niepodejmowania jakiegokolwiek ryzyka podczas jazdy oraz niezażywania żadnych produktów, które mogłyby naruszać jego zdolność do zachowania koncentracji oraz prowadzenia pojazdu w sposób uważny i całkowicie bezpieczny;

(iv) zamieszczania Ogłoszeń o wspólnym przejeździe odpowiadających wyłącznie rzeczywiście planowanym Wspólnym Przejazdom;

(v) realizacji Wspólnego Przejazdu w sposób określony w Ogłoszeniu o wspólnym przejeździe (w szczególności w odniesieniu do korzystania z autostrad lub nie) oraz przestrzegania terminów i miejsc uzgodnionych z innymi Użytkownikami Platformy (w szczególności w zakresie miejsc spotkań oraz zakończenia Wspólnych Przejazdów);

(vi) niezabierania większej liczby Pasażerów, niż wynosi określona w Ogłoszeniu o wspólnym przejeździe liczba Miejsc;

(vii) korzystania z pojazdu znajdującego się w dobrym stanie, spełniającego wszystkie obowiązujące przepisy prawa i praktyki, a w szczególności posiadającego ważny certyfikat kontroli technicznej (potwierdzający, że pojazd nadaje się do jazdy).

(viii) przekazania firmie BlaBlaCar lub każdemu Pasażerowi, który tego zażąda, informacji dotyczących jego prawa jazdy, dowodu rejestracyjnego samochodu, ubezpieczenia, certyfikatu kontroli technicznej oraz wszelkich innych dokumentów potwierdzających jego uprawnienia do prowadzenia pojazdu jako Kierowca zarejestrowany na Platformie;

(ix) w razie opóźnienia lub zmiany terminu Wspólnego Przejazdu, do natychmiastowego zawiadomienia o tym swoich Pasażerów;

(x) w razie Wspólnego Przejazdu międzynarodowego, posiadania i przedstawienia w razie konieczności Pasażerom lub uprawnionym władzom, które mogą tego zażądać, wszelkich dokumentów wymaganych w celu kontroli jego tożsamości oraz praw przekroczenia granicy;

(xi) oczekiwania na Pasażerów w uzgodnionym miejscu spotkania przez co najmniej 15 minut po uzgodnionej godzinie;

(xii) niezamieszczania jakichkolwiek Ogłoszeń o Wspólnym przejeździe dotyczących pojazdów, których nie jest właścicielem lub których nie może wykorzystywać dla celów Wspólnych Przejazdów;

(xiii) zapewnienia możliwości kontaktu ze strony Pasażerów za pośrednictwem numeru telefonu zamieszczonego na jego profilu;

(xiv) niewymagania żadnego kodu zatwierdzenia przed wysadzeniem Pasażera;

(xv) nieuzyskiwania jakichkolwiek zysków za pośrednictwem Platformy;

(xvi) wykazania, że nie ma żadnych przeciwwskazań medycznych w odniesieniu do prowadzenia pojazdów;

(xvii) zachowywania się podczas Wspólnego Przejazdu w sposób właściwy i odpowiedzialny, zgodnie z duchem wspólnych przejazdów;

(xviii) nieodrzucania jakichkolwiek Rezerwacji na podstawie kryteriów dotyczących rasy, koloru skóry, pochodzenia etnicznego lub narodowościowego, wyznania, orientacji seksualnej, stanu cywilnego, niepełnosprawności, wyglądu fizycznego, ciąży, niekorzystnej sytuacji finansowej, nazwiska, miejsca zamieszkania, stanu zdrowia, przekonań politycznych lub wieku.

### **8.3. Zobowiązania Pasażerów**

**8.3.1. Wspólne przejazdy**

Korzystając z Platformy jako Pasażer, Użytkownik zobowiązuje się do:

(i) właściwego zachowania podczas Wspólnego Przejazdu, nie powodując jakiegokolwiek rozproszenia uwagi Kierowcy lub zakłócenia prowadzenia przez niego pojazdu, a także spokoju pozostałych Pasażerów;

(ii) poszanowania pojazdu Kierowcy i zachowania jego czystości;

(iii) w razie opóźnienia, natychmiastowego zawiadomienia o tym Kierowcy;

(iv) zapłaty Kierowcy uzgodnionej kwoty Udziału w Kosztach;

(v) oczekiwania na Kierowcę w uzgodnionym miejscu spotkania przez co najmniej 15 minut po uzgodnionej godzinie;

(vi) okazania firmie BlaBlaCar lub dowolnemu Kierowcy, który tego zażąda, swojego dowodu osobistego lub jakiegokolwiek innego dokumentu potwierdzającego jego tożsamość;

(vii) nieprzewożenia podczas Wspólnego Przejazdu jakichkolwiek produktów, towarów, substancji lub zwierząt, które mogłyby powodować zakłócenie prowadzenia pojazdu lub rozproszenie uwagi Kierowcy, bądź też których posiadanie lub przewożenie jest niezgodne z obowiązującymi przepisami;

(viii) w razie Wspólnego Przejazdu międzynarodowego, posiadania i przedstawienia w razie konieczności Kierowcy lub uprawnionym władzom, które mogą tego zażądać, wszelkich dokumentów wymaganych w celu kontroli jego tożsamości oraz praw przekroczenia granicy;

(ix) zapewnienia możliwości kontaktu ze strony Pasażerów za pośrednictwem telefonu na numer zamieszczony na jego profilu, w tym także w uzgodnionym miejscu spotkania;

W przypadku, jeżeli Użytkownik dokonał Rezerwacji jednego lub większej liczby Miejsc na rzecz osób trzecich w sposób zgodny z postanowieniami Artykułu 4.2.3, jest on zobowiązany do zapewnienia przestrzegania niniejszego Artykułu oraz wszystkich innych postanowień tego Regulaminu przez te osoby trzecie. BlaBlaCar zastrzega sobie prawo zawieszenia Konta Użytkownika, ograniczenia jego dostępu do Usług lub zakończenia obowiązywania niniejszego Regulaminu w przypadku naruszenia jego postanowień przez jakąkolwiek osobę trzecią, na której rzecz Użytkownik dokonał rezerwacji Miejsca w sposób określony w tym Regulaminie. zgodnie z Artykułem 9 niniejszego Regulaminu.

**8.3.2. Przejazdy autobusowe**

Wszystkie kwestie dotyczące korzystania Pasażerów z Usług Transportowych uregulowane są umową pomiędzy Pasażerem a Przewoźnikiem – Ogólnym Regulaminem Sprzedaży. Zasady korzystania z tych usług dostarczone są przez Przewoźników.

**8.4. Zgłaszanie treści niewłaściwych lub niezgodnych z prawem (Mechanizm zawiadomienia i działania)**

Użytkownik może zgłaszać podejrzane, niewłaściwe lub niezgodne z prawem Treści Użytkownika lub Wiadomości w w trybie omówionym [tutaj](https://support.blablacar.com/s/article/Zg%C5%82aszanie-niedozwolonych-tre%C5%9Bci-1729197120604?language=pl).

BlaBlaCar, po otrzymaniu należytego zawiadomienia w trybie postanowień niniejszego Artykułu lub ze strony właściwych władz, niezwłocznie usunie wszelkie niezgodne z prawem Treści Użytkownika, jeżeli:

* Treści Użytkownika są w sposób oczywisty niezgodne z prawem lub obowiązującymi przepisami; lub 
* BlaBlaCar uzna takie treści za naruszające niniejszy Regulamin.

W takich przypadkach BlaBlaCar zastrzega sobie prawo do usunięcia Treści Użytkownika, które zostały zgłoszone, w trybie wystarczająco szczegółowym i przejrzystym i/lub do zawieszenia w trybie natychmiastowym zgłoszonego Konta.

Odpowiedni Użytkownik może odwoływać się od powyższych decyzji BlaBlaCar w trybie uregulowanym w Artykule 15.1 poniżej.

**9\. Ograniczenia użytkowania Platformy, zawieszenie konta, ograniczenie dostępu i rozwiązanie umowy**
-------------------------------------------------------------------------------------------------------

Użytkownik może rozwiązać umowę z BlaBlaCar w dowolnej chwili i z jakiekolwiek przyczyny, bez konieczności ponoszenia z tego tytułu żadnych kosztów. W tym celu wystarczy wybrać zakładkę “Chcę zamknąć moje konto” na stronie profilu Użytkownika.

W razie (i) naruszenia postanowień niniejszego Regulaminu, a w szczególności, ale nie wyłącznie, zobowiązań Użytkownika określonych postanowieniami Artykułów 6 i 8 powyżej, (ii) przekroczenia ograniczenia określonego w powyższym Artykule 4.3.3 lub (iii) jeżeli BlaBlaCar będzie posiadać uzasadnione podstawy, aby podejrzewać, że jest to konieczne dla zapewnienia jej bezpieczeństwa własnego oraz Użytkowników i osób trzecich, jak również w ramach prowadzonych postępowań wyjaśniających lub w celu zapobieżenia możliwości popełniania oszustw, BlaBlaCar zastrzega sobie prawo do:

(i) zakończenia obowiązywania Regulaminu wiążącego Użytkownika z firmą BlaBlaCar ze skutkiem natychmiastowym bez konieczności przesłania jakiegokolwiek zawiadomienia; i/lub

(ii) uniemożliwienia Użytkownikowi zamieszczania lub usuwania wszelkich Treści Użytkownika opublikowanych na Platformie; i/lub

(iii) ograniczenia dostępu Użytkownika do Platformy i jego możliwości korzystania z niej; i/lub

(iv) tymczasowego lub ostatecznego zawieszenia Konta Użytkownika.

Zawieszenie Konta może także oznaczać, w odpowiednich przypadkach, że Użytkownik nie otrzyma swoich nierozliczonych wypłat.

W zależności od okoliczności BlaBlaCar może też wysyłać użytkownikowi ostrzeżenie z przypomnieniem o zobowiązaniu do przestrzegania właściwych przepisów obowiązującego prawa i/lub niniejszego Regulaminu. 

Jeżeli będzie to konieczne, użytkownik zostanie zawiadomiony o zastosowaniu określonych powyżej środków, aby mógł potwierdzić swoje zobowiązanie do przestrzegania właściwych przepisów obowiązującego prawa i/lub niniejszego Regulaminu, udzielić wyjaśnień BlaBlaCar bądź zakwestionować podjętą decyzję. BlaBlaCar podejmie całkowicie we własnym zakresie oraz z należytym uwzględnieniem wszelkich okoliczności każdego przypadku oraz istotności naruszenia, decyzję dotyczącą ewentualnego anulowania zastosowanych środków.

**10\. Dane osobowe**
---------------------

W związku z wykorzystywaniem Platformy przez Użytkownika, BlaBlaCar będzie pozyskiwać i przetwarzać jego dane osobowe w sposób omówiony w [Polityce Prywatności](https://blog.blablacar.pl/about-us/privacy-policy).

**11\. Prawa własności intelektualnej**
---------------------------------------

### **11.1. Treści publikowane przez firmę BlaBlaCar**

W odniesieniu do wszelkich treści przekazywanych przez Użytkowników Platformy, BlaBlaCar jest jedynym właścicielem wszystkich praw własności intelektualnej do Usługi, Platformy, jej treści (w szczególności tekstów, zdjęć, projektów, logo, wideo, dźwięków, danych, elementów graficznych) oraz oprogramowania i baz danych zapewniających ich działanie.

BlaBlaCar udziela Użytkownikowi niewyłącznego, osobistego i niezbywalnego prawa do korzystania z Platformy i Usług, jedynie do jego własnego użytku osobistego oraz prywatnego, w celach niehandlowych i zgodnie z przedmiotem działania Platformy oraz Usług.

Użytkownik nie może wykorzystywać Platformy i Usług oraz ich treści w jakichkolwiek innych celach bez wcześniejszego uzyskania pisemnej zgody BlaBlaCar. W szczególności, Użytkownik nie ma prawa do:

(i) reprodukowania, modyfikowania, adaptowania, dystrybucji, publicznego przedstawiania i rozpowszechniania Platformy, Usług oraz ich treści, bez wcześniejszego uzyskania wyraźnej zgody BlaBlaCar;

(ii) dekompilacji i przeprowadzania inżynierii wstecznej w odniesieniu do Platformy oraz Usług, za wyjątkiem przypadków określonych obowiązującymi przepisami;

(iii) ekstrakcji lub podejmowania prób ekstrakcji (w szczególności przy wykorzystaniu jakichkolwiek robotów pozyskujących dane lub innych podobnego rodzaju narzędzi wykorzystywanych w celu zbierania danych) istotnej części danych zamieszczonych na Platformie.

### **11.2. Treści zamieszczane przez użytkownika na Platformie**

Aby zapewnić możliwość świadczenia Usług w sposób zgodny z przedmiotem działania Platformy, Użytkownik przyznaje firmie BlaBlaCar niewyłączną licencję dotyczącą wykorzystywania treści i danych przekazywanych przez niego w ramach korzystania z Usług, które mogą obejmować wnioski o Rezerwacje, Ogłoszenia o Wspólnych Przejazdach oraz komentarze, dane biograficzne, zdjęcia, recenzje i odpowiedzi na recenzje (dalej zwane ”Treściami Użytkownika”), a także Wiadomości. Aby umożliwić firmie BlaBlaCar prowadzenie dystrybucji za pośrednictwem sieci cyfrowej i przy wykorzystaniu dowolnych protokołów komunikacyjnych (w szczególności Internetu i sieci mobilnych) oraz publicznego udostępniania treści Platformy, Użytkownik zezwala BlaBlaCar na reprodukowanie, przedstawianie, adaptowanie i tłumaczenie jego Treści Użytkownika na następujących warunkach:

(i) Użytkownik zezwala BlaBlaCar na reprodukowanie jego Treści Użytkownika w całości lub w części przy użyciu wszelkich cyfrowych nośników zapisu danych, zarówno znanych obecnie, jak i nieznanych, a w szczególności na serwerach, twardych dyskach, kartach pamięci lub jakichkolwiek innych nośnikach podobnego rodzaju, w dowolnym formacie i przy użyciu wszystkich procesów, zarówno znanych obecnie, jak i nie, w zakresie niezbędnym dla przechowywania, wykonywania kopii zapasowych, transmisji lub pobierania danych w celach związanych z działalnością Platformy oraz świadczeniem Usług;

(ii) Użytkownik zezwala firmie BlaBlaCar na adaptację i tłumaczenie jego Treści Użytkownika oraz reprodukowanie tych adaptacji na dowolnych nośnikach cyfrowych, zarówno dostępnych obecnie, jak i w przyszłości, o których mowa w powyższym punkcie (i), w celu świadczenia Usług, w szczególności w różnych językach. Prawo to obejmuje między innymi opcję przeprowadzania modyfikacji formatu Treści Użytkownika, z zastrzeżeniem przysługujących mu praw moralnych, w celu zapewnienia przestrzegania karty tożsamości graficznej Platformy i/lub zapewnienia jej kompatybilności technicznej dla umożliwienia publikacji treści na Platformie.

Użytkownik pozostaje odpowiedzialny za oraz posiada wszelkie prawa do Treści Użytkownika oraz Wiadomości publikowanych na naszej Platformie.

**12\. Rola BlaBlaCar**
-----------------------

Platforma stanowi dostępne w sieci Internet narzędzie umożliwiające jej Użytkownikom tworzenie i zamieszczanie Ogłoszeń dotyczących Przejazdów w celu wspólnych przejazdów. Ogłoszenia te mogą być w szczególności wyświetlane przez innych Użytkowników w celu sprawdzenia warunków dotyczących Przejazdu, a także – w przypadkach, których dotyczy – bezpośredniej rezerwacji Miejsca w pojeździe Użytkownika, który zamieścił Ogłoszenie, lub rezerwować Bilety autobusowe na Platformie lub na Witrynie Internetowej Strony Trzeciej.

Korzystając z Platformy i akceptując niniejszy Regulamin, Użytkownik uznaje, że BlaBlaCar nie jest stroną jakiejkolwiek umowy zawieranej przez niego z innymi Użytkownikami w celu wspólnego poniesienia kosztów Przejazdu.

BlaBlaCar nie sprawuje żadnej kontroli zachowania Użytkowników oraz pozostałych użytkowników Platformy, lub Przewoźników i ich agentów. Nie jest właścicielem, nie prowadzi eksploatacji, ani nie zarządza pojazdami stanowiącymi przedmiot Ogłoszeń i nie oferuje żadnych Przejazdów na Platformie.

Użytkownik uznaje, że BlaBlaCar nie kontroluje w żaden sposób prawidłowości i prawdziwości Ogłoszeń, Miejsc i Przejazdów oraz ich zgodności z obowiązującymi przepisami. Działając jedynie jako pośrednik w ramach wspólnych przejazdów, BlaBlaCar nie świadczy żadnych usług transportowych i nie prowadzi działalności Przewoźnika – rola BlaBlaCar jest ograniczona wyłącznie do zapewnienia dostępu do Platformy.

Użytkownicy (Kierowcy lub Pasażerowie) i Przewoźnicy działają wyłącznie na własną odpowiedzialność.

Działając jedynie jako pośrednik, BlaBlaCar nie ponosi żadnej odpowiedzialności za rzeczywistą realizację Przejazdu, a w szczególności za:

(i) zamieszczenie przez Kierowcę lub Przewoźnika w jego Ogłoszeniu nieprawdziwych informacji lub ich przekazanie w jakikolwiek inny sposób w odniesieniu do Przejazdu oraz jego warunków;

(ii) odwołania lub modyfikacji Przejazdu przez Użytkownika;

(iii) brak płatności Udziału w Kosztach przez Pasażera;

(iv) zachowanie Użytkowników podczas Przejazdu, przed nim lub po jego zakończeniu.

W odniesieniu do Przejazdów autobusowych, Użytkownik rozumie, że ponieważ BlaBlaCar nie świadczy usług transportowych, nie zapewnia gwarancji i nie ponosi odpowiedzialności za jakiekolwiek wady usług transportowych świadczonych przez Przewoźników, takie jak opóźnienia, awarie sprzętu, wypadki, jakiekolwiek szkody, straty i szkody lub inne nieprzewidywalne zdarzenia przed, w trakcie lub po Przejeździe autobusem.

**13\. Działalność, dostępność i funkcje Platformy**
----------------------------------------------------

BlaBlaCar dołoży wszelkich starań, aby zapewnić dostępność Platformy przez 7 dni w tygodniu i 24 godziny na dobę. Niemniej jednak, dostęp do Platformy może zostać tymczasowo zawieszony bez jakiegokolwiek wcześniejszego zawiadomienia ze względów dotyczących przeprowadzenia czynności konserwacji technicznej, migracji lub aktualizacji, a także nieprawidłowego działania sieci lub innych uwarunkowań związanych z jej wykorzystywaniem.

Ponadto, BlaBlaCar zastrzega sobie prawo do modyfikacji lub zawieszenia dostępu do Platformy bądź jej funkcji w całości lub w części w sposób tymczasowy lub ostateczny, w zależności tylko i wyłącznie od własnej decyzji.

**14\. Modyfikacja Regulaminu**
-------------------------------

Niniejszy Regulamin oraz dokumenty, do których zawiera odniesienia stanowią całość umowy zawieranej przez Użytkownika z BlaBlaCar w odniesieniu do korzystania z Usług. Jakiekolwiek inne dokumenty, a w szczególności informacje zamieszczane na Platformie (odpowiedzi na często zadawane pytania itd.) mają jedynie znaczenie poglądowe.

BlaBlaCar może zmodyfikować niniejszy Regulamin w celu jego adaptacji do nowych osiągnięć technologicznych oraz handlowych, a także spełnienia wymogów obowiązujących przepisów. Jakiekolwiek modyfikacje niniejszego Regulaminu zostaną opublikowane na Platformie wraz z określeniem terminu ich wejścia w życie, a Użytkownik zostanie o nich powiadomiony przez firmę BlaBlaCar przed tym terminem.

**15\. Prawo właściwe – Rozstrzyganie sporów**
----------------------------------------------

Niniejszy Regulamin został opracowany w języku polskim i podlega prawodawstwu polskiemu.

**15.1 Wewnętrzny system obsługi reklamacji**

Użytkownik może odwoływać się od naszych decyzji podejmowanych w następującym zakresie:

* Treści Użytkownika: na przykład, usunęliśmy, ograniczyliśmy widoczność lub odmówiliśmy usunięcia wszelkich Treści Użytkownika przekazanych podczas użytkowania Platformy lub
* Konto Użytkownika: zawiesiliśmy dostęp Użytkownika do Platformy,

jeżeli podejmowaliśmy takie decyzje na podstawie faktu, że Treści Użytkownika stanowią treści niezgodne z prawem lub z niniejszym Regulaminem. Tryb odwoławczy omówiono [tutaj](https://support.blablacar.com/s/article/Jak-odwo%C5%82a%C4%87-si%C4%99-od-decyzji-o-usuni%C4%99ciu-tre%C5%9Bci-lub-zawieszeniu-konta-1729197123037?language=pl).

**15.2 Pozasądowy tryb rozstrzygania sporów**

W razie potrzeby, Użytkownik może również przedstawić skargi dotyczące działania naszej Platformy lub Usług w celu rozstrzygnięcia sporu za pośrednictwem platformy udostępnionej przez Komisję Europejską pod adresem [tutaj](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home.show&lng=FR). W takim przypadku Komisja Europejska prześle skargę Użytkownika do rzecznika praw obywatelskich w jego kraju. Zgodnie z przepisami obowiązującymi w odniesieniu do mediacji, użytkownik jest zobowiązany – przed złożeniem jakiegokolwiek wniosku dotyczącego mediacji – do zawiadomienia BlaBlaCar o wystąpieniu jakiegokolwiek sporu w celu znalezienia rozwiązania polubownego.

Użytkownik może również wystąpić z wnioskiem do organu rozstrzygania sporów we własnym kraju (listę można znaleźć [tutaj](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)). 

**16\. Informacje prawne**
--------------------------

Wydawcą Platformy jest Comuto SA, spółka z ograniczoną odpowiedzialnością z kapitałem zakładowym w wysokości 155 880,999 euro, wpisana do Rejestru Handlowego Spółek miasta Paryża pod numerem 491.904.546 (numer identyfikacji podatkowej wewnątrzwspólnotowego VAT: FR76491904546), z siedzibą pod adresem 84, avenue de la République, 75011 Paryż (Francja), reprezentowana przez swojego Dyrektora Naczelnego, pana Nicolas’a Brusson, Dyrektora Publikacji Internetowej.

Witryna internetowa znajduje się na serwerach Google Ireland Limited z siedzibą w Gordon House, Barrow Street, Dublin 4 (Irlandia).

Comuto SA jest zarejestrowana w Rejestrze Operatorów Turystycznych pod numerem: IM075180037.

Gwarancji finansowej udziela: Groupama Assurance-Crédit & Warning, 8-10 rue d’Astorg, 75008 Paryż — Francja.

Ubezpieczenie odpowiedzialności cywilnej zawodowej zawarte jest z następującym podmiotem: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paryż – Francja.

Comuto SA jest zarejestrowana w Rejestrze Pośredników Ubezpieczeniowych oraz bankowo-finansowym pod numerem rejestrowym (Orias) 15003890.

W razie jakichkolwiek pytań, prosimy o kontakt z firmą Comuto SA za pośrednictwem tego [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl).

**17\. Digital Services Act (Akt o usługach cyfrowych)**
--------------------------------------------------------

**17.1.  Informacje na temat średniej miesięcznej liczby aktywnych odbiorców usługi w UE**

Zgodnie z Artykułem 24.2 Rozporządzenia Parlamentu Europejskiego i Rady w sprawie rynku wewnętrznego usług cyfrowych zmieniającego dyrektywę 2000/31/WE („DSA”), dostawcy platform online mają obowiązek publikować informacje o średniej miesięcznej liczbie aktywnych odbiorców ich usługi w UE, obliczonej jako średnia z ostatnich sześciu miesięcy. Celem publikacji jest ustalenie, czy dostawca platformy online spełnia kryterium „bardzo dużych platform online” w rozumieniu DSA, tj. czy przekracza próg średniej miesięcznej liczby aktywnych odbiorców w UE na poziomie 45 milionów.

Na dzień 31 stycznia 2024 r. średnia miesięczna liczba aktywnych odbiorców usługi BlaBlaCar za okres od sierpnia 2023 r. do stycznia 2024 r. obliczona zgodnie z Punktem 77 i Artykułem 3 DSA, zanim wydano odpowiedni akt delegowany, wynosiła około 4,82 mln w UE.

Informację publikuje się wyłącznie w celu dostosowania się do wymogów DSA i nie należy na niej polegać dla żadnych innych celów. Dane będą aktualizowane co najmniej raz na sześć miesięcy. Nasze podejście do obliczeń przedmiotowej wartości może ulegać zmianie lub może wymagać zmiany wraz z upływem czasu z powodu, na przykład, zmian produktowych bądź nowych technologii.

**17.2. Punkt kontaktu dla organów**

Zgodnie z Artykułem 11 DSA przedstawiciele właściwych organów UE, Komisji Europejskiej lub Europejskiej Rady Usług Cyfrowych  mogą kontaktować się z nami w sprawach DSA za pośrednictwem adresu e-mail [\[email protected\]](https://blog.blablacar.pl/cdn-cgi/l/email-protection).

Można się z nami kontaktować w języku angielskim i francuskim.

Zastrzega się, że ten adres e-mail nie jest przeznaczony do komunikacji z użytkownikami. Wszelkie pytania dotyczące użytkowania Platformy BlaBlaCar Użytkownik może przesyłać za pośrednictwem [formularza kontaktowego](https://support.blablacar.com/s/contactsupport?language=pl).