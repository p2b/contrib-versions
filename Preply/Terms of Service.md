Effective: February 24, 2025



PREPLY TERMS OF SERVICE



Please read these Terms of Service (hereinafter - the “Terms”) carefully before using the PreplyPlatform and/or Services (as defined below). If you do not accept these Terms in their entirety,including the agreement to arbitrate on an individual basis any claims between you and Preply,Inc. (referred to as “Preply”, “we”, and “us”), a corporation registered and organized under thelaws of the state of Delaware, you must not access or use the Preply Platform.



1. GENERAL INFORMATION



1.1. Terms of Service



These Terms describe the terms and conditions applicable to your access and use of the website,

located at www.preply.com, and its sub-domains (the “Website”), related mobile device

application, and software (collectively with the Website, the “Preply Platform”).



Use of the Services is subject to these Terms and any other rules or policies published on theWebsite or otherwise made available to you by Preply.



You understand that by creating an account on the Preply Platform, you enter into a legallybinding contract with Preply with effect from this date and indicate your unconditionalacceptance of these Terms.



These Terms, as well as the Refund and Payment Policy, Privacy Policy, Cookies Policy, and

any service-specific terms as made available in the Legal Center, are legally binding

agreements governing the relationship between Preply and any User (as defined below) orvisitor of the Preply Platform.



As a User or visitor of the Preply Platform, the collection, use, and sharing of your personal

data are subject to the Privacy Policy and Cookies Policy, as may be amended from time to

time.



1.2. Preply Services



You may access and use the Preply Platform as: (a) a registered user seeking online tutoringservices by using the Preply Platform or other tools made available by Preply (the “Student”);(b) a registered user providing online tutoring services via the Preply Platform or other tools

made available by Preply (the “Tutor”); or (c) a visitor of the Website, including the Preply

Blog.



Throughout these Terms, “you” or “your” may refer as applicable to the Student, the Tutor, or avisitor accessing or using any of our Services (each the "User").



Preply services include access to the Preply Platform for Students and Tutors to connect foronline tutoring services, facilitation of payments between Students and Tutors, and customersupport and related services (the “Services”). Preply does not provide language teaching andtutoring services. All available tutoring services on the Preply Platform are provided according

to the services' specific terms outlined in the Legal Center.



You are responsible for obtaining and paying for any equipment and Internet service necessary

to access the Services. We may alter, suspend, or discontinue the Preply Platform or theServices in whole or in part, at any time and for any reason, without notice. The PreplyPlatform may also become unavailable periodically due to maintenance or malfunction ofcomputer equipment or other reasons. We may provide access to third-party services andproducts from time to time or to our own products or Services. You acknowledge that thePreply Platform is evolving and that the form and nature of the Services may change from timeto time without notice to you.



1.3. Amendments to these Terms



We reserve the right to amend or modify the Terms at any time by posting a revised version onthe Website and by notifying you through the Preply Platform, or via the email addressassociated with you in case of material change hereto. The latest version is published on thispage.



We encourage you to check this page regularly. If you do not agree with the amendments tothese Terms, you have the right to terminate these Terms by discontinuing your use of theServices and providing a termination notice to Preply or deleting your account. By continuingto use the Services following the amendment of these Terms, you consent to be bound by theTerms as amended.



2. OBLIGATIONS



2.1. Services Eligibility



Services are available only to and may only be used by individuals who are 18 years and olderand who can form legally binding contracts under applicable law. Individuals under the age of18 but not younger than the age of 13 can use our Services only in conjunction with and underthe supervision of a parent or a legal guardian. The parent or the legal guardian shall at alltimes be responsible for any and all activities related to the use of the Services by theaforementioned individuals.



You agree and confirm that you will not allow any individual younger than the age of 13 to usethe Services.



Due to the ongoing Russian aggression against Ukraine, Services are not available to userslocated in Russia or Belarus.



2.2. Verification



Preply may request a Tutor to provide a government-issued ID (passport, driver’s license, etc.)and the documents confirming the claimed educational credentials (educational backgrounddocuments, such as diplomas, certificates, etc.). The Tutor will be asked to upload a copy ofthe Tutor’s government-issued ID and their educational document(s). As a Tutor passes theverification procedure, a special badge is added to the Tutor’s profile, and the uploaded copiesof the documents get deleted from our servers.



Preply does not endorse or make any representations or warranties regarding the accuracy,completeness, and reliability of any information provided by the Tutor within the verificationprocedure.



Preply cannot confirm that each User is who they claim to be. You agree and understand that

you assume all risks when using the Services, including without limitation any and all of therisks associated with any online or offline interactions with other Users.



When interacting with other Users, you should exercise caution and common sense to protectyour personal safety and property, just as you would when interacting with other people youdon’t know. Neither Preply nor its affiliates or licensors is responsible for the conduct, whetheronline or offline, of any User of the Services.



2.3. Consumer Reports



Preply may or may not utilize third-party consumer reporting agencies that perform, amongother things, criminal background checks, sex offender registry checks, motor vehicle recordschecks, credit checks, and identification verifications (“Consumer reports”). Preply does notendorse or make any representations or warranties regarding the reliability of such Consumerreports or the accuracy, timeliness, or completeness of any information in the Consumerreports. Preply does not independently verify information in the Consumer reports.



You hereby consent to Preply collecting, using, and disclosing the information in theConsumer reports. You understand and agree that Preply may, in its sole discretion, review andrely on the information in the Consumer reports in deciding whether to suspend or terminate aUser account or to investigate a complaint about a User, but that Preply shall not beresponsible or liable in any way in the event that any information in the Consumer reportsabout any person, including without limitation any User, is not accurate, timely or complete.Users who are the subject of Consumer reports may contact the third-party consumer reportingagency to dispute such information's accuracy, timeliness, or completeness. Preply reserves theright to suspend and/or terminate a User account based on the information in the Consumerreports or for any other reason at Preply's sole discretion.



2.4. Privacy



You may use the Preply Platform without providing personally identifiable information. Whenusing the Services, you may be requested to provide your personal data. To learn more about

our privacy practices, please refer to the Preply Privacy Policy.



2.5. Payment



Payment processing on the Preply Platform is provided by third-party payment processorsincluding, but not limited to Braintree, PayPal, Stripe, Skrill, Payoneer, and TransferWise,allowing us to:



a. bill the Students in lieu of directly processing your billing information;

b. enable payouts to the Tutors.



For the avoidance of doubt, payment made by the Student to Preply shall satisfy the Student’sobligation with respect to the payment to the Tutor for the tutoring services provided via thePreply Platform.



Please review the additional payment terms as specified in the Refund and Payment Policy.



More details on the security of your payment and billing information may be found in our

Privacy Policy.

2.6. Refund



Preply strives to ensure a clear understanding of the financial relations between Students and

Tutors with respect to the Services we provide. Please check our Refund and Payment Policy

to find out more about how we handle refunds. To the fullest extent permitted by law, anyrefunds at any time are at our sole discretion only.



3. RIGHTS AND LIMITS



3.1. Your Right to Use the Preply Platform



In case you are a Student, Preply hereby grants you, on the Terms set forth herein, anon-transferable, non-sublicensable, non-exclusive, limited right to access and use the PreplyPlatform solely for your non-commercial personal use.



In case you are a Tutor, Preply hereby grants you, on the Terms set forth herein, anon-transferable, non-sublicensable, non-exclusive, limited right to access and use the PreplyPlatform solely for the provision of the tutoring services to the Students.



You agree not to view, copy, or procure content or information from the Preply Platform byautomated means (such as scripts, bots, spiders, crawlers, or scrapers), or to use other datamining technology or processes to frame, mask, extract data or other materials from the PreplyPlatform (except as may be a result of the standard search engine or Internet browser usage)unless formally authorized by Preply under separate written agreement.



You agree not to inquire about the engagement of or to engage Tutors to completeassignments, write papers, take quizzes, or otherwise do work on your behalf. Further, youagree not to use the Services for any purpose that violates the academic honesty policy orother conduct policies of your school, university, academic institution, or workplace.



No Preply materials made available to you as part of the Services may be copied, reproduced,modified, republished, downloaded, uploaded, posted, transmitted, or distributed in any formor by any means without Preply's prior written permission or as expressly provided in theseTerms.



You may not share or transfer your account credentials with any third party.



Preply may impose reasonable limits on your scope of access to the Preply Platform, includinglimits on time or volume of information accessed or devices used to access the PreplyPlatform, to prevent unauthorized third-party use of the Services.



All rights not expressly granted herein are reserved.



3.2. Your Account



Certain of our Services are reserved for registered users only. To become a registered user, youmust create a Student or Tutor account on the Preply Platform. You agree that you areresponsible for protecting your account credentials from unauthorized use, and you areresponsible for all activity that occurs under those account credentials. You agree to notify usimmediately if you believe that any of your account credentials have been or may be usedwithout your permission so that appropriate action can be taken.

You may use the Preply Platform and its features without being a registered user. This shallnot preclude the application of these Terms and other Preply’s rules and policies applicable toyour use of the Preply Platform, including but not limited to when you interact with the PreplyPlatform.



You may not (i) create more than two accounts (one as a Tutor and one as a Student) to accessthe Preply Platform, (ii) share your account credentials with any third party, or (iii) transferyour account to any third party. Preply is not responsible for any loss or damage caused by orexpense incurred by you as a result of your failure to safeguard your account credentials. Youagree that you shall not rent, resell, or remarket the Preply Platform or provide access to theServices to any third party.



When you create an account on Preply, we may collect certain personal data directly from you,or if you create your account using a third-party service (Facebook, Google, Apple), we maycollect personal data about you from the third-party service (your username or user IDassociated with that third-party service). By choosing to create an account using a third-partyservice, you authorize us to collect the personal data necessary to authenticate your accountwith the third-party service provider.

You may select a profile photo or connect your Google or Facebook account to be displayedwithin your Student account. Please be advised that the provision of your photo is notobligatory to use our Services. You may edit your account at your sole discretion. The security

of any personal data you choose to share within your account is subject to our Privacy Policy.



When you create a Tutor account, it is obligatory to provide your profile photo and a videointroduction. You may edit your account at your sole discretion. Adding more details to yourTutor account may help you get the most out of your use of the Services, for instance, findmore Students. Therefore, it is your choice whether to include additional information withinyour account, such as country, language skills, education, and work experience. The security

of any personal data you choose to share within your account is subject to our Privacy Policy.



3.3. Direct Interactions



Preply does not take part in direct interactions between Students and Tutors except when weconsider it advisable:



a. to ensure compliance with these Terms;

b. to improve our Services; or

c. as stated in our Refund and Payment Policy.



Preply does not have control over the tutoring services provided by the Tutors, any reviews orratings provided by the Students, or any User acts and omissions.



3.4. Representations and Warranties



Tutor-Specific Representations



If you use the Services as a Tutor:

a. you will provide the tutoring services in accordance with these Terms and otherpolicies set forth by Preply and made available to you via email or by posting on theWebsite;

b. you will provide the tutoring services in accordance with laws and regulationsapplicable in the state or country where you are providing the tutoring services;

c. you are solely responsible and fully liable for any violation of any local laws andregulations that apply to your provision of the tutoring services;

d. you will provide the tutoring services with reasonable care and skill and in accordancewith generally recognized practices;

e. you have obtained all registrations, certifications, licenses, and other documentationthat are required in the applicable jurisdiction for providing the tutoring services. It isyour obligation to maintain the validity of all aforementioned documentation;

f. you will not provide tutoring services to the Students outside of the Preply Platform,receive payments from the Students directly, or encourage or solicit payment from theStudent directly or through any channels other than those provided by Preply;

g. you acknowledge and agree that Preply may advertise tutoring services that youprovide via the Preply Platform without any additional payment or obligation to you;

h. you acknowledge and agree that Preply may improve the video you provided for youraccount. The improvements can be made by editing the video, adding the Preply logoto the video, improving the quality of sound in the video, and publishing the video onPreply social media accounts with a description and link to the Tutor’s profile.



Student-Specific Representations



If you use the Services as a Student:

a. you agree to honor the commitments you make to the Tutor via the Preply Platform;

b. you agree that you will not circumvent or manipulate the fee structure, the billingprocess, or fees owed to Preply or the Tutor; and

c. you agree to use good faith efforts to interact with the Tutors.



3.5. Tutor Introduction Video and Profile Photo



Tutors grant Preply the rights to use Tutor’s introduction video, name, and profile photo formarketing, advertising, or promotional purposes, including but not limited to publishing onsocial media channels, video hosting, and streaming services, such as YouTube, Vimeo,Facebook, or others, as to ensure accessibility and visibility to the Students.



You may always request to remove any introduction videos published on social media

channels, video hosting, and streaming services by writing to support@preply.com.



3.6. Tutor Ranking



Preply uses many pieces of information in order to show the Student the Tutors that best matchthe Student’s preferences. The order in which Tutors are displayed depends on a number offactors including, but not limited to, the following:

● the subject the Student is searching for;

● the country of the Student;

● the Student’s preferred language on the Preply Platform;

● the time zone of the Student;

● the overlap in standard business hours between the Tutor and Student’s time zones;

● the overall availability of time slots on a Tutor’s calendar;

● the past performance of a Tutor in helping their Students achieve their learning goals;

● the Tutor profile information, including its completeness and the quality of the profilephoto and introduction video;

● how responsive the Tutor is to the Students’ messages;

● the Student reviews and ratings;

● Student’s learning history on the Preply Platform.

3.7. User Complaints



If a User has a complaint about Preply Services, they should contact Preply at

complaint@preply.com with the subject line ‘Formal Complaint’ (the “Complaint”),

providing as much detail as possible about the Complaint. Preply shall respond to the Userconfirming receipt and shall investigate the matter.



Upon receiving the Complaint, Preply customer support shall investigate the Complaintinternally, taking into account the importance and complexity of the issue or issues raised. Ifthe Complaint relates to a specific Preply employee, another Preply representative will helpwith the investigation in their place.



Preply shall respond to the User with its findings in response to the Complaint, and, whereapplicable, with a suggested solution.



3.8. Referral Program



The Students (henceforth the “Student Referrer”) and the Tutors (henceforth the “TutorReferrer”) may invite another individual who is not and has never been, a registered user of thePreply Platform (henceforth the “Referee”) with a referral link. Clicking on this referral linkentitles the Referee to a discount on their initial purchase via the Preply Platform.



Referral links are only valid for and may be redeemed by the Referee to whom they are sent.We reserve the right to terminate any accounts found in violation and remove any credits orreferral bonuses accumulated in breach of these Terms or Promotional terms with or withoutnotice.



Eligibility for discounts and referral bonuses is determined by Preply at its sole discretion, andwe reserve the right to revoke a discount or referral bonus if we determine you are not eligible.The eligibility requirements and other conditions will be disclosed when you sign-up for theServices or as otherwise communicated to you by Preply.



Students



The Student Referrer will be entitled to receive a discount when the Referee makes their initialpurchase using the discount from the referral link. The discount amount for the Student Referrerand the Referee is subject to change.



Credits are valid only for booking lessons on Preply, are not redeemable for cash, and may notbe resold. Credits will be valid for 180 days from the Referrer’s most recent log-in date. If theReferrer has not logged in for 180 days, all credits will expire and will no longer berecoverable.



Tutors



The Tutor Referrer will be entitled to receive a referral bonus for every purchase the Refereemakes. The referral bonus can be withdrawn as cash. The referral bonus amount for the TutorReferrer and Referee is subject to change.



3.9. Limits



While using the Services, you agree to abide by the User Code of Conduct.

4. PREPLY CONTENT



Users have a personal, non-transferable, non-exclusive right to use the Preply Content of thePreply Platform subject to these Terms. The “Preply Content” means all information, text,images, data, links, or other material created by us and accessible through the Preply Platform.The Content may contain typographical errors, other inadvertent errors, or inaccuracies. Wereserve the right to make changes to the Preply Content without obligation to issue any noticeof such changes. You may view, copy, download, and print the Preply Content that is availableon the Preply Platform, subject to the following conditions:

a. The Preply Content is available solely for your personal use. No part of the PreplyPlatform or the Preply Content may be reproduced or transmitted in any form, by anymeans, electronic or mechanical, including photocopying and recording for any otherpurpose;

b. The Preply Content may not be modified;

c. Copyright, trademark, and other proprietary notices may not be removed.



Nothing contained on the Preply Platform should be construed as granting, by implication,estoppel, or otherwise, any license or right to use the Preply Platform or any Preply Content,except: (a) as expressly permitted by these Terms; or (b) with our prior written permission orthe permission of such third party that may own the trademark or copyright of the PreplyContent.



5. USER-GENERATED CONTENT



The “User-generated Content” means all comments, remarks, information, feedback, text,photographs, links, data, images, video, music, or other material that you or any User post toany part of the Preply Platform or provide to Preply, including such content or information thatis posted as a result of surveys.



We are not responsible or liable for the conduct of Users or for views, opinions, and statementsexpressed in the User-generated Content submitted for public display through the PreplyPlatform. We do not prescreen information posted online. We are acting as a passive conduitfor such distribution and may not be responsible for the User-generated Content. Anyopinions, advice, statements, services, offers, or other information in the User-generatedContent expressed or made available by Users are those of the respective author(s) ordistributor(s) and not of Preply. We neither endorse nor guarantee the accuracy, completeness,or usefulness of any such User-generated Content. You are responsible for ensuring that theUser-generated Content submitted to the Preply Platform is not provided in violation of anycopyright, trade secret, or other intellectual property rights of another person or entity. Youshall be solely liable for any damages resulting from any infringement of copyrights, tradesecrets, or other intellectual property rights, or any other harm resulting from your use,uploading, posting, or submission of the User-generated Content to the Preply Platform.



We have the right, but not the obligation, to randomly monitor, edit or remove anyUser-generated Content submitted on or through the Preply Platform at any time.



If you believe that your intellectual property rights have been infringed, please submit your

complaint to legal@preply.com. You may report all types of intellectual property claims,

including, but not limited to, copyright, trademark, and patent claims. We respond quickly tothe concerns of rights owners about any alleged infringement, and we terminate repeatinfringers in appropriate circumstances.

Tutor Materials. The Tutor retains full ownership of any original materials they create, includingbut not limited to lesson plans, worksheets, presentations, and other teaching resources(“Tutor-Created Materials”). Any third-party materials properly licensed by the Tutor(“Tutor-Licensed Materials”) remain the property of the original rights holder.



By sharing Tutor-Created Materials or Tutor-Licensed Materials with the Student as part of thetutoring services, the Tutor allows Preply to facilitate the Student’s access to, display and storesuch materials solely to support the delivery of the tutoring service for as long as it is reasonablynecessary to complete educational objectives initiated during the tutoring services.



Lobbying. Federal law restricts lobbying activities by tax-exempt organizations. “Lobbying”includes certain activities intended to influence legislation. The User-generated Content doesnot constitute lobbying by Preply but may constitute lobbying by you or an organization yourepresent. You are responsible for complying with any applicable lobbying restrictions.



6. LINKS TO THIRD-PARTY WEBSITES



The Preply Platform may contain links to non-Preply websites. These links are provided toyou as a convenience and/or ancillary to the Services, and Preply is not responsible for thecontent of any linked website. Any non-Preply website accessed from the Preply Platform isindependent from Preply, and Preply has no control over that website’s content. In addition, alink to any non-Preply website does not imply that Preply accepts any responsibility for thecontent or use of such a website. Use of any third-party website is subject to its terms ofservice and privacy policy. We request that the Users exercise caution and good judgmentwhen using third-party websites.



7. ADVERTISEMENT



Preply may run advertisements and promotions sponsored by third parties on the PreplyPlatform. Your correspondence or business dealings with, or participation in promotions ofadvertisers other than Preply found on or through the Services, including payment anddelivery of related goods or services, and any other terms, conditions, warranties, orrepresentations associated with such dealings, are solely between you and such advertiser.Preply is not responsible or liable for any loss or damage of any sort incurred as the result ofany such dealings or as the result of the presence of such non-Preply advertisers on the PreplyPlatform.



Preply may display advertisements on non-Preply websites to promote the tutoring servicesyou provide via the Preply Platform and help you generate more Student leads.



8. NO IMPLIED ENDORSEMENTS



In no event shall any reference to any third party or third party product or service be construedas an approval or endorsement by Preply of that third party or of any product or serviceprovided by a third party. Preply does not endorse, warrant, or guarantee any product orservice offered by any third party and will not be a party to or in any way monitor anytransaction involving any third-party providers of products or services. As with the purchase ofa product or service through any medium or in any environment, you are responsible forexercising caution and good judgment.

9. RELATIONS BETWEEN PREPLY AND USERS



Providing the Services Preply is acting as an on-demand intermediary connecting Students andTutors and providing the tools to facilitate tutoring services. You acknowledge and agree thatPreply is a technology services provider that does not provide online tutoring services orfunctions as a language learning school.



Preply does not serve as an employer of any User unless separately subject to a signed, writtenemployment contract signed by both the User and Preply. Users may use the Services only forthe provision and receipt of the tutoring services subject to these Terms.



As such, Preply will not be liable for any tax or withholding, including but not limited tounemployment insurance, employer’s liability, workers’ compensation insurance, socialsecurity, or payroll withholding tax in connection with your use of Services. You are solelyresponsible for adhering to all applicable tax regulations that may apply in connection withyour use of the Services. You hereby agree to compensate Preply for all state fees, claims,payments, fines, or other tax liabilities that Preply will incur in connection with the obligationsarising from applicable tax or other regulations not being met by you.



In all cases of use of the Preply Platform, Users are acting as independent contractors and notas Preply’s or any other party’s employee, agent, franchisee, or servant. Accordingly, you willbe solely responsible for all costs incurred by you or your organization. You may not act as anemployee, agent, or representative of Preply nor bind any contract on behalf of Preply. Where,by implication of mandatory law or otherwise, you shall be deemed an employee of Preply,you hereby agree to waive any claims against us that may arise as a result of such impliedemployment relationship. No User is entitled to participate in any Preply vacation, groupmedical or life insurance, disability, profit sharing or retirement benefits, or any other fringebenefits or benefit plans offered by Preply to its employees.



The Services provide connection to Tutors who are willing to be engaged by Students asindependent contractors. As independent contractors, each Tutor decides when and how oftenthe Tutor will be available to provide the tutoring services to Students. Each Tutor controls themethods, materials, content, and all aspects of the tutoring services.

Students are responsible for selecting a Tutor suitable for their learning goals. Students shouldcheck each Tutor's self-reported credentials, education, and experience, as well as reviewsfrom other Students. Each Tutor has the sole discretion to accept or decline a request fortutoring services, as well as continue or discontinue a tutoring relationship with any Student.



Tutors may and, in fact, are expected to perform the tutoring services for others or do othertypes of work (either as an independent contractor or employee or other) while these Terms arein effect, including with Preply’s competitors if desired, provided that such other activities donot result in the Tutor’s violation of the Terms.



10. ASSIGNMENT



You may not assign or transfer your rights or obligations under these Terms in whole or in partto any third party without Preply’s written consent. These Terms shall be binding and inure tothe benefit of the parties to these Terms and their respective successors, permitted transferees,and assigns.

11. FEEDBACK



You acknowledge and agree that we may provide you with a mechanism to provide feedback,suggestions, and ideas about the Services or the Preply Platform (the "Feedback").



By submitting any Feedback, you provide us written consent to use your Feedback for theimprovement and promotion of the Services. You agree that submitting Feedback is gratuitous,unsolicited, and without restriction and will not place us under any fiduciary or otherobligation and that we are free to use the Feedback without any additional compensation toyou and/or to disclose the Feedback on a non-confidential basis or otherwise to anyone.



You further acknowledge that, by accepting your Feedback, Preply does not waive any rightsto use similar or related ideas previously known to Preply, developed by its employees, orobtained from sources other than you. You agree that we may, in our sole discretion, use theFeedback you provide to us in any way, including in future enhancement and modifications tothe Services. You hereby grant to us and our assigns a perpetual, worldwide, fully transferable,sublicensable, irrevocable, royalty-free license to use, reproduce, modify, create derivativeworks from, distribute, and display the Feedback in any manner for any purpose, or without itin any media, software, or technology of any kind now existing or developed in the future,without any obligation to provide attribution or compensation to you or any third party.



12. REVIEWS



You acknowledge and agree that Preply may calculate a composite rating based on commentsand reviews left by other Users. Tutors agree to be rated by Students along several criteria, assuggested by Preply. Preply provides its automatic feedback and rating system as a meansthrough which Users can express their opinions publicly, and Preply does not monitor orcensor these opinions or investigate any remarks posted by Users for accuracy or reliabilityunless a User brings the posting to Preply's attention. You may be held legally responsible fordamages suffered by other Users or third parties as a result of these remarks if a court findsthat these remarks are legally actionable or defamatory. Preply is not legally responsible forany feedback or comments posted or made available on the Preply Platform by any Users orthird parties, even if that information is defamatory or otherwise legally actionable. You agreeto report violations or abuses of our rating and feedback system immediately by contactingCustomer Support.



13. NOTIFICATIONS



Unless you otherwise indicate in writing, Preply will communicate with you by email, regularmail or by posting communications on the Preply Platform. You consent to receivecommunications from us electronically, and you agree that these electronic communicationssatisfy any legal requirement that such communications be in writing. You will be consideredto have received a communication when we send it to the email address you have provided toPreply within your account or when we post such communication on the Preply Platform. Youshould keep your email address updated in your account and regularly check this Website forpostings. If you fail to respond to an email message from Preply regarding the violation,dispute, or complaint within 2 (two) business days, we will have the right to terminate orsuspend your account. All notices to Preply intended to have a legal effect concerning theseTerms must be in writing and delivered either in person or by means evidenced by a deliveryreceipt to the following address: 1309 Beacon St., Suite 300, Brookline, MA 02446.



To stop receiving specific communications from Preply, please, submit a notification to us by

email at support@preply.com in order to change the types and frequency of such

communications, you may also change notification preferences in your account.



14. TERMINATION



Other than Tutors, we may terminate any User’s access to the Preply Platform in our solediscretion, for any reason and at any time, with or without prior notice. It is our right toterminate access for Users who violate these Terms, as deemed appropriate in our solediscretion.



We may terminate a Tutor’s use of the Services (a) immediately for failure to comply with the

Terms, including User Code of Conduct, which is considered a material breach of the

agreement; (b) for other cause, including, but not limited to, sexual or other unwelcomeharassment, threats or intimidation, fraud, falsification of documents or qualifications; or(c) upon 30 days’ advance written notice for any reason.



We may also delete or restrict access to or use of all related information and files. Preply willnot be liable to Users or any third party for any modification, suspension, or termination of theService, or loss of related information.



In case Preply suspends or terminates your account due to the breach of these Terms or any ofPreply's policies, you understand and agree that you shall receive no refund or compensationfor any unused credits or scheduled lessons, or loss of any content or information associatedwith your account. In addition to the aforementioned, Preply is entitled to withhold any fundsremaining on your account as liquidated damages.



In case you haven’t logged into your Preply account for more than 180 days your account willbe suspended, and your remaining balance will expire.



Even after your right to use the Services has been terminated or suspended, these Terms willremain enforceable against you.



15. INTELLECTUAL PROPERTY RIGHTS



All intellectual property in the design and layout of the Preply Platform and the material andinformation published on the Website pages or within the Preply Platform functionalitybelongs to and is vested in Preply or its licensors. You may not copy any part of the PreplyPlatform or otherwise do anything in relation to any part of the Preply Platform. You may nototherwise use or reproduce any of the Preply Platform or the material contained within it inany manner other than those listed above without first obtaining the prior written permissionof Preply.



Unless otherwise noted, all Preply Content contained on the Preply Platform is the property ofPreply and/or its affiliates or licensors and is protected from unauthorized copying anddissemination by United States copyright law, trademark law, international conventions, andother intellectual property laws. The service marks and trademarks of Preply, includingwithout limitation Preply and the Preply logos are service marks owned by Preply, Inc. Anyother trademarks, service marks, logos, and/or trade names appearing via the Service are theproperty of their respective owners. You may not copy or use any of these marks, logos, ortrade names without the express prior written consent of the owner.



You may not link or frame to any pages of the Website or any Preply Content contained

therein, whether in whole or in part, without prior written consent from Preply. You may likeor follow Preply or share links to the Website via social networking technology referenced onthe Website. Any rights not expressly granted herein are reserved.



16. COPYRIGHT INFRINGEMENT. DMCA NOTICE



Since we respect content owner rights, it is Preply’s policy to respond to alleged infringementnotices that comply with the Digital Millennium Copyright Act of 1998 ("DMCA"). If youbelieve that any materials on our Services infringe your copyright, you may request that theybe removed. Please notify Preply's copyright agent as set forth in the DMCA. For yourcomplaint to be valid under the DMCA, your request must bear the signature (or electronicequivalent) of the copyright holder or an authorized representative and must include thefollowing information:



a. An electronic or physical signature of a person authorized to act on behalf of thecopyright owner;

b. Identification of the copyrighted work that you claim has been infringed;

c. Identification of the material that is claimed to be infringing and where it is located onthe Website;

d. Information reasonably sufficient to permit Preply to contact you, such as youraddress, telephone number, and email address;

e. A statement that you have a good faith belief that use of the material in the mannercomplained of is not authorized by the copyright owner, its agent, or law; and

f. A statement, made under penalty of perjury, that the above information is accurate, andthat you are the copyright owner or are authorized to act on behalf of the owner.



The above information must be submitted to the following DMCA Agent: Attn: DMCANotice Preply, Inc.



Address: 1309, Beacon St., Suite 300, Brookline, MA 02446. Email: legal@preply.com.



Under federal law, if you knowingly misrepresent that online material is infringing, you maybe subject to criminal prosecution for perjury and civil penalties, including monetary damages,court costs, and attorney's fees.



Please note that this procedure is exclusively for notifying Preply and its affiliates that yourcopyrighted material has been infringed. The preceding requirements are intended to complywith Preply’s rights and obligations under the DMCA, including 17 U.S.C. §512(c), but do notconstitute legal advice. It may be advisable to contact an attorney regarding your rights andobligations under the DMCA and other applicable laws.



In accordance with the DMCA and other applicable laws, Preply may also, at its solediscretion, limit access to the Service and/or terminate the accounts of any Users who infringethe intellectual property rights of others, whether or not there is a repeat infringement.



17. CONFIDENTIALITY



You may obtain direct access via the Services to certain confidential information of Preply, itsaffiliates or Users, including but not limited to personally identifiable information, technical,contractual, product, program, pricing, marketing and other valuable information that shouldreasonably be understood as confidential (“Confidential Information”). You agree to holdConfidential Information in strict confidence and not use the Confidential Information except

for the purposes set forth in these Terms and not disclose such Confidential Information to anythird party. All rights, title and interest in the Confidential Information remains with Preply, itsaffiliates and its Users. No obligation is imposed upon you with respect to ConfidentialInformation that you can establish by legally sufficient evidence: (a) you possessed prior toyour receipt from Preply, without an obligation to maintain its confidentiality; (b) is orbecomes generally known to the public through no act or omission by you, or otherwisewithout violation of the Terms; (c) you obtained from a third party who had the right todisclose it, without an obligation to keep such information confidential; (d) you independentlydeveloped without the use of Confidential Information and without the participation ofindividuals who have had access to it, or (e) is disclosed in response to a valid order by a courtor other governmental body, or as otherwise required by law, or as necessary to establish therights of either party under these Terms and as disclosed after prior notice to Preply adequateto afford Preply the opportunity to object to the disclosure.



18. DISCLAIMER OF WARRANTY



Use of the Services is entirely at your own risk. Preply disclaims all liability in connectionwith any interactions, correspondence, transactions, and other dealings that you have with anythird parties, including without limitation Students or Tutors found on or through the PreplyPlatform (including on or via linked websites or advertisements) are solely between you andthe third party (including issues related to the content of third-party advertisements, payments,services, delivery of goods, warranties (including product warranties), privacy and datasecurity, and the like). Under no circumstances will we be liable for any loss or damage causedby your reliance on the information in any content on the Preply Platform. It is yourresponsibility to evaluate the accuracy, completeness, or usefulness of any information,opinion, advice, or other content available through the Preply Platform.



You acknowledge that the Preply Platform and all Services, text, images, and otherinformation on or accessible from the Preply Platform are provided "as is" and are based inpart on listings provided by Tutors, which are not verified by Preply, and that any tutoringservices, listings or other content acquired through the use of the Preply Platform are at yoursole risk and discretion. Preply and its affiliates and licensors are not liable or responsible forany results generated through the use of the Services. We provide no warranty of any kind,either express or implied, including but not limited to the implied warranties ofmerchantability, fitness for a particular purpose, and non-infringement. Specifically, butwithout limitation, Preply does not warrant that: (i) the information available on the PreplyPlatform is free of errors; (ii) the functions or services (including but not limited tomechanisms for the downloading and uploading of content) provided by the Preply Platformwill be uninterrupted, secure, or free of errors; (iii) defects will be corrected, or (iv) the PreplyPlatform or the server(s) that makes it available are free of viruses or other harmfulcomponents. Neither Preply nor its affiliates or licensors is responsible for the conduct,whether online or offline, between Users.



In addition, notwithstanding any feature a Student may use to expedite Tutor selection, eachStudent is responsible for selecting their Tutor and negotiating a contract, and Preply does notwarrant any goods or tutoring services purchased by a Student and does not recommend anyparticular Tutor. Preply does not provide any warranties or guarantees regarding any Tutor’sprofessional accreditation, registration, or license.



Preply expressly disclaims any liability or claims that may arise between Users of its Services.You are solely responsible for your interactions with all other Users and any disputes that arisefrom those interactions with other Users. Preply is not obliged but may attempt to assist in

resolving disputes between Users.



19. LIMITATION OF LIABILITY



19.1. Disclaimer



In no event shall Preply, its affiliates, and licensors be liable to any User of the Preply Platformor any other person or entity for any direct, indirect, special, incidental, consequential, orexemplary damages (including, but not limited to, damages for loss of profits, loss of data, lossof use, or costs of obtaining substitute goods or services) arising out of use, inability to use,unauthorized access to or use or misuse of the Preply Platform or any information containedthereon, whether based upon warranty, contract, tort (including negligence) or otherwise, evenif has been advised of the possibility of such damages or losses.



19.2. Limitation



You agree that Preply’s total cumulative liability in connection with these Terms, the PreplyPlatform, the Services, the Preply Content, or any listing or services whether in contract, tort, orotherwise, shall not exceed the amounts, if any, you paid to Preply for the Services in thethen-prior three months.



19.3. Waiver of Class Action



Any claims brought by you or Preply must be brought in that party’s individual capacity, andnot as a plaintiff or class member in any purported class or representative proceeding.



20. EXCLUSIONS



Some jurisdictions do not allow the exclusion of certain warranties or the limitation orexclusion of liability for incidental or consequential damages. Accordingly, some of the abovelimitations may not apply to you.



21. INTERNATIONAL



The Preply Platform may be accessed from countries other than the United States. The PreplyPlatform and the Services may contain products or references to products that are onlyavailable within the United States and U.S. territories. Any such references do not imply thatsuch products will be made available outside the United States.



If you access and use the Preply Platform outside the United States, you are responsible forcomplying with all applicable local laws and regulations.

We make no representation that information on the Preply Platform is appropriate or availablefor use outside the United States. Those who choose to access the Preply Platform fromoutside the United States do so on their own initiative and at their own risk.



22. EXPORT CONTROL



The laws of the United States of America prohibit the transmission, export, and re-export ofcertain products, services, downloadable software, and data (technical data) to particularpersons, territories, and foreign states. Nothing from the Services may be exported, in anyway, in violation of United States law.

The United States export control regulations prohibit U.S. businesses, such as Preply, fromoffering services to users in specific sanctioned regions.



In order to comply with these regulations, it is not allowed for users in the following areas toaccess all or certain parts of the Preply Platform:

● Iran,

● Sudan,

● Crimea,

● Cuba,

● Syria,

● North Korea,

● Temporary occupied territories of Donetsk and Luhansk oblast of Ukraine,

● Any other country or region restricted by law (as may be changed from time to time).



Depending on your exact location, you may encounter an IP or payment purchase block whenattempting to enroll in or otherwise access the Preply Platform.



23. LIQUIDATED DAMAGES



Preply and a User hereto acknowledge and agree that the funds that may be withheld underSection 14 of these Terms shall constitute liquidated damages and not penalties and are inaddition to all other rights of Preply in case of the breach of these Terms. Preply and a Userfurther acknowledge that (i) the amount of loss or damages likely to be incurred is incapable oris difficult to precisely estimate, (ii) the amounts specified in the abovementioned section bear areasonable relationship to, and are not plainly or grossly disproportionate to, the probable losslikely to be incurred in connection with any material breach of the agreement by a User (iii) oneof the reasons for Preply and a User reaching an agreement as to such amounts was theuncertainty and cost of litigation regarding the question of actual damages.



24. INDEMNIFICATION



By using the Services, you agree to indemnify, hold harmless and defend Preply and itssubsidiaries, affiliates, shareholders, officers, directors, agents, licensors, suppliers, otherpartners, employees, and representatives from any claims, damages, losses, liabilities, and allcosts and expenses of defense, including but not limited to, attorneys' fees, resulting directly orindirectly from a claim by a third party that arises in connection with your use of the Services,including but not limited to (a) acts and/or omissions on or off the Preply Platform; (b)violation of any rights of another, including without limitation any alleged infringement ofintellectual property or other right of any person or entity relating to the Preply Platform; (c)breach of these Terms; (d) disputes with or between other Users; (e) use and/or misuse of thePreply Platform, including without limitation any information, content and/or materialsthereon; (f) violation of any applicable law or regulation; (g) inaccurate, untimely, incompleteor misleading User information, including without limitation with respect to registration,profile or eligibility; (h) misstatements and/or misrepresentations; (i) use of links to third partywebsites, including without limitation such websites' availability, terms of use, privacy policy,information, content, materials, advertising, products and/or services; (j) User information andany acts or omissions with respect to such User information; (k) use of any information inthird-party reports; (l) use of third-party payment processors; and/or (m) use of any services orproducts or any contracts or arrangements made or provided based on information, contentand/or materials obtained on or through the Preply Platform . You further agree that you willcooperate as requested by Preply in defense of such claims. Preply reserves the right, at itsown expense, to assume the exclusive defense and control of any matter otherwise subject to

indemnification by Users, and you shall not, in any event, settle any claim or matter on behalfof Preply without the written consent of Preply.



In the event that you have a dispute with any User, you hereby release Preply (and its officers,directors, agents, investors, subsidiaries, employees, contractors, and any other third partiesrelated to the Services) from any and all claims, demands, or damages (actual orconsequential) of every kind, known and unknown, arising out of or in any way related withsuch disputes.



If you are a California resident, you waive California Civil Code Section 1542, which says: “Ageneral release does not extend to claims that the creditor or releasing party does not know orsuspect to exist in his or her favor at the time of executing the release and that, if known byhim or her, would have materially affected his or her settlement with the debtor or releasedparty.”



25. DISPUTE RESOLUTION



Any dispute arising out of consumer claims shall be finally resolved by individual arbitrationbefore a single arbitrator is conducted in the English language in Delaware, USA, under theCommercial Arbitration Rules of the American Arbitration Association (AAA).



All disputes arising out of or relating to these Terms shall be finally resolved by individualarbitration. The arbitration will be conducted in Delaware, USA, except that a User residingin the US may elect to have the arbitration conducted at a location of Preply’s choice no morethan 160 kilometers from the User’s residence. The individual arbitration must be before asingle arbitrator is conducted in English under the Commercial Arbitration Rules of theAmerican Arbitration Association (AAA).



Any arbitration under these Terms will be between an individual User and Preply. To thefullest extent permitted by applicable law, and except as expressly provided below, you andPreply expressly waive any entitlement to resolve disputes in court or on a class, collective, orrepresentative basis. You and Preply shall appoint as sole arbitrator a person mutually agreedby you and Preply or, if you and Preply cannot agree within thirty (30) days of either party’srequest for arbitration, such single arbitrator shall be selected by the AAA upon the request ofeither party.



The parties shall bear equally the cost of the arbitration except (a) to the extent prohibited byapplicable law; (b) that if the arbitrator determines that costs unique to arbitration (i.e., filing,administration, and arbitrator’s fees) would preclude a User from asserting a claim inarbitration, the arbitrator may require Preply to pay a greater share of such costs unique toarbitration; and (c) the prevailing party shall, to the extent permitted or required by applicablelaw, be entitled to an award of reasonable attorneys' fees and costs incurred in connection withthe arbitration in such an amount as may be determined by the arbitrator.



All decisions of the arbitrator shall be final and binding on both parties and enforceable in anycourt of competent jurisdiction. Notwithstanding this, the application may be made to anycourt for a judicial acceptance of the award or order of enforcement. Notwithstanding theforegoing, Preply shall be entitled to seek temporary injunctive relief, security, or otherequitable remedies from the United States District Court for the District of Delaware or anyother court of competent jurisdiction.

26. GOVERNING LAW



These Terms shall be governed by the laws of the State of Delaware, USA, excluding theUnited Nations Convention on Contracts for the International Sale of Goods; the 1974Convention on the Limitation Period in the International Sale of Goods; and the Protocolamending the 1974 Convention, done at Vienna April 11, 1980; provided, however, that to thefullest extent permissible under law the Federal Arbitration Act (9 U.S.C.§ 1 et seq.) shallgovern Section 25 of these Terms (entitled Dispute Resolution).



27. NON-SOLICITATION



Without limitation, the Services may not be used to solicit for any other business, website, orservice. You may not solicit, advertise for, or contact in any form Users for employment,contracting, or any other purpose not related to the Service facilitated through Preply withoutexpress written permission from Preply.



You may not use the Service to collect usernames and/or email addresses of Users byelectronic or other means without Preply's express prior written consent.



28. HEADINGS



The headings and captions used in these Terms are used for convenience only and are not to beconsidered in construing or interpreting these Terms.



29. ENTIRE AGREEMENT. SEVERABILITY



These Terms, together with any amendments and any additional agreements you may enterinto with Preply in connection with the Services, shall constitute the entire agreement betweenyou and Preply concerning the Services. If any part of these Terms is held to be unlawful,void, or unenforceable, that part will be deemed severable and shall not affect the validity andenforceability of the remaining provisions.



30. CONTACTS



For additional information and in case you have any questions about these Terms, please

contact support@preply.com.