**Généralité**

Les présentes Conditions Générales ont vocation à régir les relations entre LOCASUN, Société par actions simplifiée au capital de 1 000 000 euros dont le siège social est situé 24 rue des Jeûneurs 75002 PARIS, immatriculée sous le n° 488 157 389 au RCS Paris, et les personnes capables et douées de discernement souhaitant bénéficier de prestations de vacances telles que décrites ci-dessous (ci-après le « Client »).  
  
LOCASUN propose des prestations de vacances et plus particulièrement des prestations d’hébergement de toutes sortes (maison, appartements, mobil-homes, bateaux …) situés sur le territoire français, européen et étranger (ci-après les « Prestations d’Hébergement »).  
  
A ce titre, LOCASUN exploite le site internet accessible à l’adresse www.locasun.fr (ci-après le « Site » - ainsi que ses versions traduites) via lequel elle propose la mise à disposition à titre onéreux d’hébergement (ci-après l’« Hébergement ») pour une durée correspondant à celle du séjour (ci-après le « Séjour »).  
  
Les présentes Conditions Générales seront complétées de documents spécifiques (notamment le contrat de location et le bon d’entrée), remis au Client ou aux personnes occupant l’Hébergement (ci-après les « Vacanciers »), lesquels forment avec les présentes, le contrat liant le Client à LOCASUN.  
  
Toute réservation souscrite auprès de LOCASUN implique l’acceptation sans réserve des présentes Conditions Générales.

**ARTICLE 1.** **INSCRIPTION, CONCLUSION DU CONTRAT ET TARIFS**

La réservation d’une Prestation d’Hébergement est réalisée sur le Site par le Client selon le processus suivant :  
\- réservation et choix de l’Hébergement ;  
\- indication du nombre de nuitées ou de semaines et de Vacanciers ;  
\- vérification du détail de la réservation, de son prix total ;  
\- paiement du prix de la Prestation d’Hébergement ou d’un acompte sur le prix de la Prestation d’Hébergement ;  
\- consultation et acceptation des Conditions Générales avant validation de la réservation par le Client ;  
\- validation de la réservation par le Client ;  
\- envoi par courriel au Client du récapitulatif de la réservation.  
  
Pour des questions de législation sur certaines résidences mais aussi de confort et de sécurité, LOCASUN se réserve le droit de demander aux Clients et/ou Vacanciers la communication de certaines informations personnelles (date de naissance, plaque d’immatriculation, numéro de passeport…).  
  
Le Client s’engage à compléter l’intégralité des renseignements demandés à l’occasion de la réservation et atteste de la véracité et de l’exactitude des informations transmises.  
  
Les Hébergements proposés sont disponibles en petite quantité et sont gérés en temps réel sur le Site. L’Hébergement présenté peut devenir indisponible si plusieurs utilisateurs le réservent en même temps. Le premier utilisateur à effectuer sa réservation est prioritaire. Le Client qui aura pré-réservé un Hébergement devenu indisponible sera informé par LOCASUN par courriel et sera immédiatement remboursé des sommes versées. Il recevra des propositions pour d’autres Hébergements.  
  
La validation par LOCASUN de la réservation est matérialisée par l’envoi dans les 72 heures du récapitulatif du contrat de location par courriel au Client indiquant le nombre de nuitées, la date de début et de fin de Séjour et le prix de la prestation (ci-après le « Contrat de Location »).  
  
Conformément à l’article L221-28 du Code de la consommation et autres dispositions législatives et règlementaires applicables à la conclusion de contrats de voyages et Séjours en ligne, le droit de rétractation de 14 jours ne s’applique pas pour les prestations de services d’hébergement. Par conséquent, le Client ne dispose pas d’un droit de rétractation dans le cadre de la conclusion d’un contrat de location avec LOCASUN.  
  
Après paiement total du solde du prix de la Prestation d’Hébergement, le Client reçoit un bon d’entrée sur lequel sont indiquées les coordonnées, les dates et heures d’arrivée et de départ (ci-après le « Bon d’Entrée »).  
  
Le Client peut à tout moment consulter l’avancement de sa réservation et toutes les informations afférentes à partir de son compte en ligne qu’il aura préalablement créé, dans la rubrique « mes réservations ».

  

**ARTICLE 2.** **PRIX ET PAIEMENT**

**_2.1_**  **Prix**

Le prix de la Prestation d’Hébergement comprend le prix de l’Hébergement, les frais de dossier, et, sur libre adhésion, la souscription à une assurance annulation et/ou multirisque décrite à l’article 5 (ci-après le ou les « Prix »).  
  
Les Prix incluent la TVA au taux légal en vigueur au jour de la réservation et sont payables en euros.  
  
Le prix de l'Hébergement est communiqué par le partenaire de LOCASUN.  
  
Lorsque des offres de réduction sont proposées, à côté du prix de vente, est indiqué un prix de référence communiqué par le partenaire de LOCASUN. Conformément à l'article L.112-1-1 du code de la consommation, le prix de référence renseigné doit correspondre au prix le plus bas pratiqué par le partenaire à l'égard de tous les consommateurs au cours des trente derniers jours précédant l'application de la réduction de prix.  
  

**_2.2_**  **Paiement du Prix**

Le paiement peut être effectué en plusieurs fois dans les conditions décrites en infra, sauf s’il s’agit d’une réservation spéciale, lesquelles nécessitent le règlement de la totalité du Prix lors de la réservation (ci-après la « Réservation Spéciale »).  
  
Les Réservations spéciales concernent les trois hypothèses suivantes :  
\- la réservation a été faite au plus tard trente-cinq (35) jours avant la date d’arrivée, (la date d’arrivée étant le premier jour du Séjour, ci-après la « Date d’Arrivée ») ;  
\- le Prix total de la Prestation d’Hébergement est strictement inférieure à deux cents euros (200 €) ;  
\- l’annonce de l’Hébergement indique dans la rubrique « Contact » un des « CODE TO » suivants : 160, 171, 271 ou 317.  
  
Le paiement peut être effectué par carte bancaire (CB, Visa ou MasterCard), chèques bancaires ou chèques-vacances, virement bancaire, téléphone avec un conseiller, ou paypal.  
Lorsque le Client opte pour le paiement par paypal, il doit régler l’intégralité du Prix de la Prestation d’Hébergement.  
  
Pour tous les autres moyens de paiement, le Client peut régler :  
\- soit l’intégralité du Prix de la Prestation d’Hébergement lors de la réservation ;  
\- soit en plusieurs fois de la manière suivante :  
· 30% du Prix de l’Hébergement au jour de la réservation, ainsi que les frais de dossier et l’éventuelle assurance annulation et/ ou multirisque, si elle a été souscrite;  
· 70% du Prix de l’Hébergement à régler au plus tard trente-cinq (35) jours avant la Date d’Arrivée, ainsi que la taxe de séjour si celle-ci est collectée par LOCASUN.  
  
Si le Client opte pour un paiement par chèques bancaires, virement bancaire ou chèques-vacances, il recevra une proposition de séjour. Néanmoins, l’Hébergement ne sera réservé qu’à partir du moment où LOCASUN recevra le règlement de la Prestation d’Hébergement :  
\- le Client peut alors procéder dans un premier temps à un paiement par carte bancaire ou paypal afin que la demande soit immédiatement traitée. Dans un second temps et après réception et encaissement par LOCASUN des chèques bancaires ou chèques-vacances, LOCASUN créditera le compte bancaire du Client des sommes qui correspondent aux montants des chèques encaissés ;  
\- si le Client ne souhaite pas réserver sa Prestation d’Hébergement en procédant à un premier paiement par carte bancaire ou paypal et se contente d’envoyer les chèques, ou le virement bancaire à LOCASUN, il prend alors le risque que l’Hébergement qui l’intéressait soit réservé par un autre Client plus diligent. Le tarif de la location peut aussi souffrir une évolution entre le moment où le client reçoit la proposition de séjour et le moment où Locasun reçoit son règlement. Le Client est conscient de cette possibilité, et peut décider de procéder au règlement des frais supplémentaires, d’annuler son séjour sans frais, ou bien de se positionner sur un autre logement.  
  
Locasun n’accepte pas les VACAF, les e-chèques-vacances, les espèces et la carte American express.

  

**ARTICLE 3.** **PRESTATION D’HEBERGEMENT**

La Prestation d’Hébergement fournie par LOCASUN est composée des différents services indissociables mentionnés dans le Contrat de Location et comprend :  
\- la mise à disposition de l’Hébergement ;  
\- la mise à disposition du mobilier meublant l’Hébergement.  
  
Les photographies de l’Hébergement publiées sur le Site ou transmises par courriel avec le Contrat de Location ou le Bon d’Entrée présentent une illustration de l’Hébergement. Les photographies d’une même annonce peuvent correspondre à plusieurs logements quasi identiques. L’Hébergement pourrait donc ne pas correspondre exactement aux illustrations (en termes d’agencement, d’exposition, de décoration etc.).  
  
Le mobilier de l’Hébergement est également susceptible de ne pas correspondre en tout point à celui présenté dans les photographies susvisées. LOCASUN garantit que l’Hébergement contient autant de couchages que le nombre maximum de Vacanciers prévus pour ledit Hébergement. Toutefois, le type de couchage présenté sur la photographie (lits doubles, lits simples, lits superposés, canapés lits) ne peut être garanti.  
  
L’utilisation d’un Hébergement au maximum de sa capacité peut engendrer un confort moindre, ce que le Client reconnaît et accepte expressément.  
  
Si le descriptif mentionne la présence de mobilier de jardin, LOCASUN ne garantit pas que ledit mobilier correspondra au nombre maximum de Vacanciers prévus pour ledit Hébergement.  
  
Si une télévision est mentionnée dans le Contrat de Location, LOCASUN ne peut garantir que les différentes chaînes puissent être reçues dans différentes langues.  
  
Le descriptif de l’Hébergement mentionne également, notamment pour les Hébergements situés dans des résidences de tourisme, les prestations proposées (ex : les piscines ou restaurants) qui sont accessibles pendant la haute saison. Pendant la basse saison ou en cas de mauvais temps, nettoyage, de maintenance ou de faible fréquentation, les prestations proposées peuvent être moins nombreuses, ce que les Vacanciers reconnaissent et acceptent.  
  
Les Vacanciers qui doivent respecter les règles de baignade spécifiquement formulées sont informés que toutes les législations étrangères n’imposent pas de sécurisation des piscines. La responsabilité de LOCASUN ne pourra être recherchée pour ces faits.  
  
Compte tenu de la diversité des Hébergements proposées sur le Site, LOCASUN ne peut pas garantir des aménagements adaptés à l’accueil et au Séjour de personnes à mobilité réduite ou personnes âgées nécessitant une assistance particulière. Le Client peut se rapprocher de LOCASUN afin de savoir si lesdits Hébergements permettraient cet accueil, à charge pour le Client d’assurer leur sécurité et surveillance.  
  
Les difficultés rencontrées par le Client ou les Vacanciers en raison de l’agencement de la location (escaliers étroits, absence d’ascenseur, chemins de terre…) n’engagent pas la responsabilité de LOCASUN, le logement étant pris en l’état.

  

**ARTICLE 4.** **FORMULES SKI**

LOCASUN propose deux formules spéciales relatives à certains Séjours en montagne. Ces options ne sont toutefois pas disponibles sur l’ensemble desdits Séjours.  
  
Lorsqu’elles sont proposées lors de la réservation en ligne le Client a la possibilité de souscrire aux offres suivantes :  
\- location de matériel de ski ;  
\- achat d’un forfait pour les remontées mécaniques (ci-après un le « Forfait »).

**_4.1_**  **Location de matériel de ski**

La prestation de location de matériel de ski permet au Client de louer pour son compte ou celui des Vacanciers le matériel de ski aux fins d’utilisation sur le domaine skiable pendant toute la durée du Séjour.  
  
La location du matériel de ski peut faire l’objet de différents types d’offres (économique, évolution, sensation et excellence) qui permettent la location de ski seuls, chaussures seules ou du pack complet. Le prix des offres varie en fonction de la station de ski concernée.  
  
Le Client peut souscrire à la location de matériel jusqu’à la veille de la Date d’Arrivée.  
  
Le Client certifie qu’il est apte à pouvoir se servir personnellement du matériel et décharge LOCASUN de toute responsabilité à ce titre. Le choix et la location du matériel sont placés sous l’unique responsabilité du Client.  
  
Le Client s’engage à utiliser le matériel loué avec soin et à prendre toutes les précautions nécessaires afin d’éviter de l’endommager. Le Client assume la garde matérielle et juridique du matériel loué. Le Client est ainsi entièrement responsable du matériel loué, de son enlèvement jusqu’à son retour.  
  
Le Client bénéficiaire de la location du matériel est tenu de respecter les règles de sécurité et d’entretien relatives à l’utilisation dudit matériel de ski. Le Client reconnait que le matériel ne pourra faire l’objet d’un prêt ou d’une sous-location.  
  
Le Client retournera le matériel :  
\- au lieu de location à la date indiquée avant la fermeture dudit lieu ;  
\- en bonne condition et en bon état de fonctionnement, c’est-à-dire sans autre modification que l’usure normale consécutive à un emploi normal du matériel conformément à sa destination.  
  
Le Client s’interdit de substituer à un matériel loué un quelconque matériel de remplacement.  
  
Le Client est informé que le matériel est numéroté ou marqué et qu’il doit être restitué avec les mêmes numéros et marques.  
  
En cas de non-retour du matériel ou d’une partie du matériel ou de sa dégradation empêchant son utilisation, le Client sera redevable du montant correspondant à la valeur neuve desdits produits.

**_4.2_**  **Achat d’un Forfait**

L’achat d’un Forfait permet au Client, titulaire pour son compte et des Vacanciers titulaires, d’utiliser les remontées mécaniques en service du domaine skiable pendant toute la durée du Séjour.  
  
Sous réserve de la production de justificatifs (pièces d’identité ou livret de famille), plusieurs tarifs de Forfait (en général jeune, étudiant, adulte, famille) sont proposés à des prix définis selon la station de ski concernée. Une photographie récente du titulaire peut également être demandée.  
  
Le Client peut souscrire à ce forfait jusqu’à quatre (4) jours avant la Date d’Arrivée.  
  
Tout Forfait volé ou perdu ne sera pas remplacé. Tout Forfait réservé et payé qui n’est pas utilisé par son titulaire pendant le Séjour ne sera pas ni échangé, ni remboursé.  
  
Un Forfait est personnel, incessible et intransmissible pendant toute sa durée de validité. Le titulaire doit être capable de présenter son Forfait et de justifier de sa titularité à tout moment pendant la durée du Séjour, notamment lors des contrôles par les autorités compétentes.  
  
L’utilisation frauduleuse du Forfait ou sa falsification, est passible de poursuites pénales et de condamnation à des dommages-intérêts.  
  
Le Client titulaire du Forfait est tenu de respecter les règles de sécurité relatives aux transports par les remontées mécaniques de la station de ski.

  

**ARTICLE 5.** **ASSURANCES**

Le Client a la possibilité de souscrire :  
\- une assurance relative à l’annulation ou l’interruption du séjour (ci-après l’« Assurance Annulation/ Interruption de Séjour ») ;  
\- une assurance annulation pour manque ou excès de neige (ci-après l’« Assurance Annulation Manque ou Excès de Neige ») ;  
\- une assurance pour garantir les personnes physiques lors de leurs activités de ski (ci-après l’« Assurance Journée Ski ») ;  

**_5.1_**   **L’Assurance Annulation/ Interruption de Séjour**

Lors de la réservation en ligne du Séjour, le Client a la possibilité de souscrire à l’Assurance Annulation/Interruption de Séjour auprès de la Compagnie MUTUAIDE.  
  
L’Assurance permet le remboursement intégral du Prix de l’Hébergement réservé jusqu’à la veille du début du Séjour.  
  
Le coût de l’Assurance proposée varie en fonction de l’Hébergement et de la durée du Séjour.  
  
Les Conditions Générales de l’Assurance sont directement accessibles en ligne en cliquant sur le lien suivant [https://static.locasun.com/pdf/assurance-gritchen/ipid\_4106.pdf](https://static.locasun.com/pdf/assurance-gritchen/ipid_4106.pdf) et doivent être expressément acceptées par le Client avant toute souscription.

**_5.2_**  **L’Assurance Annulation Manque ou Excès de Neige**

Le Client a la possibilité de souscrire, dans les stations de ski à plus de 1.200 mètres d’altitude, l’Assurance Annulation Manque ou Excès de Neige auprès de la Compagnie MUTUAIDE Le Client. Les conditions générales de l’Assurance sont directement accessibles en ligne en cliquant sur le lien [suivant](https://gap.gritchen.fr/partenaire/produits/__commun/client-ipid-cga.php?lang=fr&contrat=client_produit_garantie-40d68c70993ea559ac7e22fe3fefebb1-8397) et doivent être expressément acceptées par le Client avant toute souscription.

**_5.3_**  **L’assurance Journée Ski**

Le Client a la possibilité de souscrire à l’Assurance Journée Ski auprès de la compagnie EUROP ASSISTANCE SA. Les conditions générales de l’Assurance sont directement accessibles en ligne en cliquant sur le lien [suivant](https://gap.gritchen.fr/partenaire/produits/__commun/fiche-ipid.php?lang=fr&contrat=client_produit_garantie-d4641f665ef8b7bf2cf5876ab47b3250-18562) et doivent être expressément acceptées par le Client avant toute souscription.

  

**ARTICLE 6.** **SERVICES ET PRESTATIONS COMPLÉMENTAIRES**

Indépendamment de LOCASUN, un référent sur place (ci-après le « Contact Local ») peut pour certains Hébergements, proposer aux Vacanciers des services et prestations complémentaires (lit supplémentaire, serviettes, ménage, supplément animaux, piscine…) placés sous sa propre responsabilité. LOCASUN transmet les prix pour les services et prestations complémentaires proposés par le Contact Local. Ces derniers sont à titre indicatif et susceptibles d’être modifiés par le Contact Local.  
  
Les informations concernant les activités sportives et de loisirs sont transmises par les Offices de Tourisme à titre indicatif. Elles ne sauraient engager la responsabilité de LOCASUN.  
  
La responsabilité de LOCASUN ne pourra en aucune manière être recherchée en cas de dommage subi par le Client et/ou les Vacanciers dans le cadre des Services et Prestations Complémentaires ou de tout autre service que ceux couverts par la Prestation d’Hébergement.

  

**ARTICLE 7.** **CHARGES ET TAXES DE SÉJOUR**

Des frais supplémentaires peuvent être facturés en sus du Prix de la Prestation d’Hébergement, tels que :  
\- la taxe de séjour, collectée pour le compte des municipalités, calculée par personne et par jour ;  
\- les charges locatives (ex : eau, électricité, internet..).  
  
Ces frais supplémentaires qui peuvent être collectés par LOCASUN ou par le Contact Local, sont détaillés dans l’annonce.

  

**ARTICLE 8.** **OBLIGATIONS ET RESPONSABILITÉ DU CLIENT ET DES VACANCIERS**

**_8.1_**  **Utilisation de l’Hébergement et du matériel meublant l’Hébergement**

Tous les Hébergements proposés par LOCASUN sont mis à disposition du Client et des Vacanciers aux fins de Séjours de loisirs, touristiques ou dans le cadre de déplacements professionnels. Tout autre usage, notamment à des fins commerciales, est formellement interdit.  
  
Le Client et les Vacanciers s’engagent à utiliser l’Hébergement et le matériel le meublant avec soin.  
  
Dans les Hébergements disposant d’un règlement intérieur, le Client et les Vacanciers sont tenus, dès leur arrivée à l’Hébergement, de prendre connaissance de ce dernier qui s’impose à eux immédiatement et sans réserve pendant la durée du Séjour.  
  
En tout état de cause, et même en l’absence de Règlement intérieur, le Client et les Vacanciers sont tenus d’adopter le comportement d’une personne raisonnable.  
  
En cas de non-respect du règlement intérieur ou de comportement inapproprié, LOCASUN et le Contact Local se réservent le droit de contraindre le Client à quitter les lieux à tout moment. Le Client ne pourra prétendre à aucun remboursement pour ce fait.  
  
Le Client et les Vacanciers bénéficient des équipements et installations de l’Hébergement mis à leur disposition dans le cadre de la Prestation d’Hébergement, sous la seule et entière responsabilité du Client.  
  
Pendant toute la durée du Séjour, le Client doit s’assurer que tout meuble ou autre objet meublant l’Hébergement (jardin compris) demeure dans l’enceinte de l’Hébergement réservé. Si des soustractions frauduleuses ont lieu du fait de la négligence du Client, LOCASUN et le Contact Local se réservent le droit de le tenir pour responsable. Des frais pourront lui être réclamés le cas échéant.  
  
Le Client s’engage à indemniser LOCASUN de tout dommage et détérioration que lui et/ou les Vacanciers auraient occasionné durant leur Séjour, tant sur l’Hébergement que sur les meubles et équipements le garnissant, et qui ne serait pas couvert par le dépôt de garantie.

**_8.2_**  **Travaux Urgents**

Le Client s’engage à autoriser toute intervention urgente et nécessaire dans l’Hébergement pendant la période d’occupation. Dans la mesure où ces évènements exceptionnels et inévitables ne sont pas imputables à LOCASUN, le Client ne pourra prétendre à aucun dédommagement pour le trouble causé durant le Séjour.

  

**ARTICLE 9.** **ARRIVÉE ET DÉPART**

**_9.1_**  **Arrivée**

Les Vacanciers se présentent aux coordonnées et au créneau horaire indiqués dans le Bon d’Entrée et munis de ce dernier.  
  
Selon les Hébergements et leur localisation, les Vacanciers pourront être accueillis par le Contact Local, les clés pourront leur être envoyées par courrier préalablement ou elles pourront être déposées à un endroit déterminé auquel les Vacanciers devront se rendre pour les récupérer.  
  
En cas de présentation tardive à l’Hébergement sans que le Client en ait préalablement informé LOCASUN et le Contact Local, l’accès à l’Hébergement peut être refusé. Le Client ne pourra prétendre à aucun remboursement du Séjour pour ce fait. Si l’accès à l’Hébergement est accordé, le Client pourra se voir imputer des frais d’arrivée tardive.  
  
En cas de non-présentation à la location dans le délai de vingt-quatre (24) heures et sans nouvelles du Client, LOCASUN et le Contact Local se réservent le droit d’annuler de plein droit le Contrat de Location. Le Client ne pourra prétendre à aucun remboursement du Séjour pour ce fait.  
  
Si le nombre de Vacanciers dépasse la capacité d’hébergement, LOCASUN peut refuser les personnes supplémentaires, étant précisé que les enfants de moins de deux ans doivent être comptabilisés comme une personne. Dans l’hypothèse où ce refus ne serait pas respecté LOCASUN pourrait mettre fin à la Prestation d’Hébergement.  
  
A l’inverse, lorsque la capacité d’hébergement n’est pas atteinte et que le Client se présente à l’Hébergement avec une ou plusieurs personnes supplémentaires qu’il souhaite définitivement ajouter au Séjour, LOCASUN peut facturer un supplément par personne. Le Client devra alors s’acquitter de ce règlement avant d’intégrer l’Hébergement.  
  
A son arrivée dans l’Hébergement, le Client devra prendre connaissance et signer les éventuels documents spécifiques présents sur place (comme ceux relatifs aux consignes de sécurité, état des lieux, inventaire spécifique…).  
  
Le Client disposera d’un délai de quarante-huit (48) heures après son arrivée pour signaler par écrit à LOCASUN tout élément affecté ou détérioré, ou tout dysfonctionnement ou manquement et devra parallèlement en informer le Contact Local.  
  
Lorsqu’une anomalie qui nuit au bon déroulement du Séjour a été signalée par le Client dans le délai imparti, LOCASUN ainsi que le Contact Local mettent tout en œuvre pour régulariser cette situation. Le Contact Local tient LOCASUN informé de toute diligence accomplie.  
  
A défaut, en cas de contestation ultérieure sur l’existence de dommages, dysfonctionnements ou détériorations affectant l’Hébergement, ses meubles ou équipements, ces derniers seront réputés avoir été mis à disposition des Vacanciers en parfait état à la Date d’Arrivée.

**_9.2_** **Départ**

L’Hébergement devra être restitué en bon état de propreté, vaisselle faite, poubelles vidées. A défaut, les frais engagés pour le nettoyage et la remise en état seront déduits du montant du dépôt de garantie.  
  
Le Client est tenu de remettre tout meuble ou autre objet meublant (jardin compris) à sa place initiale. Il doit veiller à fermer soigneusement l’Hébergement et remettre les clés au Contact Local ou au lieu indiqué à l’heure précisée sur le Bon d’Entrée. Toute heure de retard lors de la restitution des clés peut engendrer des frais supplémentaires à la charge du Client.  
  
Si le Client souhaite procéder à la remise des clés de manière anticipée et en dehors des créneaux horaires proposés, il ne pourra prétendre à aucun remboursement pour la période pendant laquelle il n’a pas occupé l’Hébergement. Des frais pour départ anticipés pourront en outre être appliqués.

  

**ARTICLE 10.** **DÉPÔT DE GARANTIE**

Le versement d’un dépôt de garantie par le Client peut être effectué via l’un des moyens de paiement suivant :  
\- chèque bancaire ;  
\- carte bancaire ;  
\- espèces.  
  
Ce dépôt de garantie sera débité au plus tard dans le délai de trente (30) jours à compter de la fin de la Prestation d’Hébergement, à hauteur des sommes dues par le Client à LOCASUN au titre des réparations ou des dégâts occasionnés et/ou des objets cassés ou manquants, des frais de ménage et plus généralement pour tout dommage direct ou indirect occasionné par le Client et/ou un Vacancier à l’occasion de l’exécution de la Prestation d’Hébergement.  
  
Toutefois, si le montant du dépôt de garantie ne couvrait pas les dégâts ou manques constatés, LOCASUN pourra mettre en œuvre toute procédure pour recouvrer le montant des réparations et remises en état.  
  
Le montant du dépôt de garantie varie en fonction de l’Hébergement et des spécificités du Séjour.

  

**ARTICLE 11.** **ANIMAUX**

Seuls les Hébergements dont le descriptif et le Contrat de Location portent la mention « animaux acceptés » peuvent accueillir des animaux domestiques. LOCASUN devra préalablement en être informé, donner son accord et indiquer au Client que le coût du supplément est à régler sur place. Certains Hébergements conditionnent l’accès de tout animal domestique à la présentation sur place d’un certificat antirabique valide.  
  
En tout état de cause, les animaux relevant de la 1ère catégorie et 2ème catégorie conformément à l’article L211-12 du Code rural et de la pêche maritime sont strictement interdits de Séjour dans les Hébergements proposés par LOCASUN.  
  
Dès son arrivée à l’Hébergement et pendant toute la durée du Séjour, les Vacanciers séjournant en compagnie d’un ou plusieurs animaux domestiques sont seuls responsables des faits de ceux-ci.  
  
Ils doivent s’assurer que leurs animaux domestiques ne nuisent pas à la tranquillité, à la sécurité et au bien-être des autres Vacanciers, du personnel ou de toute autre personne. Les animaux domestiques accueillis demeurent strictement interdits dans certaines parties communes entourant l’Hébergement telles que les restaurants, les piscines, les espaces verts ou tout autre lieu indiqué et doivent être tenus en laisse lorsque le règlement des lieux l’exige.  
  
En cas de non-respect d’une des règles précitées, LOCASUN se réserve le droit de refuser l’accès à l’Hébergement aux Clients et Vacanciers tout en conservant le règlement payé au titre du Contrat de Location.

  

**ARTICLE 12.** **MODIFICATION ET ANNULATION DU FAIT DU CLIENT**

**_12.1_**  **Conditions de Modification**

Toute modification de la Prestation d’Hébergement, qu’il s’agisse notamment de la durée du Séjour, des dates, du choix de l’Hébergement doit être approuvée par LOCASUN en fonction des disponibilités et peut entrainer des frais de modification.  
  
Une demande d’augmentation de la durée du Séjour ne donne pas systématiquement lieu à des frais de modification exception faite de la différence tarifaire de la Prestation d’Hébergement.  
  
Toute demande de diminution de la durée du Séjour est considérée par LOCASUN comme une annulation partielle laquelle donne systématiquement lieu à des frais d’annulation.  
  
Toute autre demande peut être considérée par LOCASUN comme une annulation partielle.

**_12.2_**  **Conditions d’Annulation Totale ou Partielle**

Le Client peut annuler sa réservation de Prestation d’Hébergement à tout moment avant la Date d’Arrivée par lettre recommandée avec accusé de réception ou courriel adressé à LOCASUN aux adresses mentionnées à l’article 20 des présentes.  
  
La date d’annulation prise en compte est la date correspondant au jour ouvrable auquel LOCASUN est informé par lettre recommandée avec accusé de réception ou courriel de l’annulation (ci-après « Date d’Annulation »).  
  
LOCASUN procèdera alors à la restitution des sommes versées par le Client lors de la réservation, dans un délai de trente (30) jours à compter de la Date d’Annulation, dans les conditions mentionnées ci-dessous.  
  
En cas d’annulation, LOCASUN conservera les frais de dossiers versés par le Client ainsi que des options et assurances si elles ont été souscrites.  
  
Les politiques d’annulation peuvent varier d’un hébergement à l’autre. Le détail des frais d’annulation est fourni sur la page d’annonce d’un hébergement lorsque des dates de séjours ont été sélectionnées.  
  
Flexibles :  
\- Sans frais d’annulation jusqu’à 21 jours avant l’arrivée,  
\- 100% à partir de 20 jours  
  
Modérées  
\- Sans frais d’annulation jusqu’à 31 jours avant l’arrivée,  
\- 100% à partir de 30 jours  
  
\- Fermes  
\- 30% de frais d’annulation jusqu’à 36 jours avant l’arrivée,  
\- 100% à partir de 35 jours  
  
\- Strictes  
\- 100% dès la réservation.  
  
  
Le codeTO est indiqué sur chaque annonce.  
  
\- Cas Particulier : si vous réservez un hébergement dont l’annonce indique dans la rubrique « Contact » un des « CODE TO » suivants : 160, 171, 271, 317 ou 1450, alors en cas d'Annulation de la Prestation d’Hébergement, 100% de Frais d'annulation seront dûs et ce dès la réservation (c'est à dire que l’intégralité du Prix de la Prestation d’Hébergement tel qu’indiqué sur le Contrat de Location sera débitée et non remboursable).  
En cas d’absence de “CODE TO” sur l’annonce, les frais suivants s’appliquent si Annulation de la Prestation d’Hébergement :  
Plus de 35 jours avant l’arrivée : frais d’annulation à 30%.  
Moins de 35 jours avant l’arrivée : frais d’annulation à 100%.  
  

**_12.3_**  **Réservation Non Annulable, Non Remboursable, Non Echangeable, Non Modifiable**

Les conditions de modification et d’annulation partielle ou totale de réservation ci-dessus mentionnées ne sont pas applicables aux Réservations Spéciales.  
  
Aucune annulation, aucun échange, ni modification de date, de lieu, de type d’Hébergement, de personne ou autre ne pourra être effectué pour une Réservation Spéciale.  
  
Aucun remboursement ne pourra être effectué en cas d’annulation ou en cas de non-présentation dans le cadre d’une Réservation Spéciale, pour quelque motif que ce soit, le montant total du Séjour restant acquis à LOCASUN.

  

**ARTICLE 13.** **CESSION DU CONTRAT DE LOCATION**

Un Client peut valablement céder à un remplaçant cessionnaire (ci-après le « Remplaçant ») son Contrat de Location à condition que :  
\- la cession intervienne par écrit avant la Date d’Arrivée ;  
\- le Client ait informé LOCASUN par courrier ou courriel aux adresses mentionnées à l’article 20 des présentes au moins sept (7) jours avant la Date d’Arrivée ;  
\- le remplaçant ait pris attache avec LOCASUN dans un délai de trois (3) jours suivant la cession.  
  
A défaut de respect des trois conditions précitées, LOCASUN ou le Contact Local se réservent le droit de refuser l’accès à l’Hébergement au Remplaçant. Ni le Client, ni le Remplaçant ne pourront prétendre à un remboursement du Séjour en raison de cette résiliation du Contrat de Location qui leur est imputable.  
  
La cession du Contrat de Location fait du Remplaçant une partie audit Contrat en lieu et place du Client. Il doit ainsi s’acquitter du paiement du solde de l’Hébergement réservé et des éventuels frais découlant de la cession de contrat de location.  
  
Le Remplaçant fait son affaire personnelle de l’accomplissement des formalités administratives nécessaires et des délais pour les obtenir (documents d’identification valides, passeports, visas…). La responsabilité de LOCASUN ne pourra être engagée de ce fait.  
  
Le Client et le Remplaçant sont responsables solidairement du paiement du solde de l’Hébergement et des éventuels frais de cession notamment en cas de changement de nom.

  

**ARTICLE 14.** **MODIFICATION OU ANNULATION DU FAIT DE LOCASUN**

**_14.1_**  **Conditions de modification**

**_14.1.1_****Modification mineure du Contrat de Location**

LOCASUN peut modifier unilatéralement avant le début du Séjour, des clauses du Contrat autres que le Prix, à condition que cette modification soit mineure, ce que le Client reconnaît et accepte expressément.  
  
LOCASUN informera le Client de la modification mineure sur un support durable.

**_14.1.2_****Modification d’un élément essentiel du Contrat de Location**

La survenance d’un évènement extérieur qui s’impose à la volonté de LOCASUN ou du Contact Local peut rendre impossible le respect d’un des éléments essentiels du Contrat de Location.  
  
Dans ces conditions, LOCASUN peut proposer au Client une modification d’un élément essentiel du Contrat de Location ou un Hébergement de remplacement que le Client peut accepter ou refuser.  
  
Si le Client accepte la modification ou l’Hébergement de remplacement mais que cela entraine une baisse de qualité du voyage, le Client a droit à une indemnité adéquate.  
  
Si le Client refuse la modification ou l’Hébergement de remplacement, le Contrat de Location est annulé.

**_14.2_**  **Conditions d’annulation**

LOCASUN peut annuler la réservation effectuée par le Client. LOCASUN rembourse au Client les paiements effectués, et ce, dans les quatorze (14) jours qui suivent l’annulation. Le Client est alors éligible à obtenir une indemnisation.

  

**ARTICLE 15.** **SURVENANCE DE CIRCONSTANCES EXCEPTIONNELLES ET INÉVITABLES**

En cas de survenance de circonstances exceptionnelles et inévitables au lieu de destination ou à proximité immédiate de celui-ci, ayant des conséquences importantes sur l’exécution du Contrat de Location, le Client et/ou LOCASUN peuvent annuler le Contrat.  
  
LOCASUN rembourse alors le Client des paiements effectués, et ce, dans les quatorze (14) jours qui suivent l’annulation. Le Client n’est pas éligible à obtenir d’indemnisation supplémentaire.

  

**ARTICLE 16.** **RESPONSABILITE DE LOCASUN**

LOCASUN est responsable de la bonne exécution de toutes les obligations résultant du Contrat.  
  
LOCASUN sera exonéré de tout ou partie de sa responsabilité dans le cas où l’inexécution ou la mauvaise exécution du Contrat serait imputable soit au Client, aux Vacanciers ou à un tiers, soit à la survenance d’un fait imprévisible et insurmontable, soit à la survenance d’un cas de force majeure.  
  
Pendant toute la durée du Séjour, LOCASUN et Le Contact Local veilleront à assister le Vacancier pour toute éventuelle difficulté rencontrée.  
  
LOCASUN délivrera au Vacancier des informations d’ordre général concernant les conditions applicables en matière de passeport et visa, y compris la durée approximative d’obtention des visas ainsi que tout renseignement sur les formalités sanitaires du pays.  
  
Le Vacancier doit veiller à ses objets et effets personnels. La responsabilité de LOCASUN ne saurait être engagée en cas de perte, soustraction frauduleuse, dégradation ou destruction desdits objets et effets.

  

**ARTICLE 17.** **RÉCLAMATIONS**

Les éventuelles réclamations envoyées par les Clients doivent se réaliser en respectant le process suivant :  
Le Client peut adresser des réclamations :  
\- auprès de LOCASUN et du Contact Local pendant son Séjour. LOCASUN et le Contact Local mettront tout en œuvre pour satisfaire ses demandes ;  
\- auprès de LOCASUN par courrier recommandé ou courriel dans les quinze (15) jours qui suivent la fin du Séjour. Les réclamations formulées seront traitées dans un délai de soixante (60) jours à compter de la réception du courrier ou courriel.  
  
Si ce process n'est pas respecté, nous vous informons que les frais ne seront pas pris en charge, notamment en cas d’interruption de séjour.

  

**ARTICLE 18.** **RÈGLEMENT DES LITIGES**

Le droit applicable à tout Contrat de Location conclu avec LOCASUN est le droit français.  
  
En cas de litige, les Parties s’engagent, avant toute action contentieuse, à rechercher ensemble, de bonne foi, une solution amiable. Toute réclamation sera donc au préalable portée par écrit auprès de LOCASUN aux coordonnées suivantes : Service Client LOCASUN 24 rue des Jeûneurs 75002 PARIS.(adresse exclusivement réservée à la réception du courrier)  
  
Les Parties sont informées qu’elles peuvent également adresser une réclamation auprès de la plateforme européenne de règlement en ligne des litiges accessible à l’adresse suivante : [https://ec.europa.eu/consumers/odr/main/?event=main.home2.show](https://ec.europa.eu/consumers/odr/main/?event=main.home2.show) .  
  
Conformément aux articles L.616-1 et R.616-1 du Code de la consommation, LOCASUN a mis en place un dispositif de médiation de la consommation. L'entité de médiation retenue est : SAS CNPM MÉDIATION - CONSOMMATION. Dans l’hypothèse où aucun accord n’aurait pu être trouvé à l’issue de la tentative de résolution amiable du litige, le consommateur pourra déposer sa réclamation sur le site : http://cnpm-mediation-consommation.eu ou par voie postale en écrivant à CNPM - MÉDIATION - CONSOMMATION, 27, avenue de la Libération – 42400 SAINT-CHAMOND.  
  
Si aucun accord amiable ne pouvait être trouvé, le litige pourrait être soumis au tribunal compétent.

  

**ARTICLE 19. CONFIDENTIALITÉ ET TRAITEMENT DES DONNÉES**

Dans le cadre de l’exécution du Contrat ou de la prise de mesures précontractuelles LOCASUN est amené à collecter des données personnelles concernant le Client, et le cas échéant le Vacancier, notamment par le biais du Site.  
  
L’ensemble des conditions dans lesquelles LOCASUN procède à la collecte, au traitement et à la conservation de ces données personnelles sont détaillées à l’adresse suivante https://www.locasun.fr/cnil-et-confidentialite

  

**ARTICLE 20.** **DISPOSITIONS DIVERSES**

Aucune tolérance, quelle qu’en soit la nature, l’ampleur, la durée ou la fréquence, ne pourra être considérée comme créatrice d’un quelconque droit et ne pourra conduire à limiter d’une quelconque manière que ce soit, la possibilité d’invoquer chacune des clauses des présentes Conditions Générales, à tout moment, sans aucune restriction.  
  
Toute clause des présentes Conditions Générales qui viendrait à être déclarée nulle ou illicite par un juge compétent serait privée d’effet, mais sa nullité ne saurait porter atteinte aux autres stipulations, ni affecter la validité des Conditions Générales dans leur ensemble ou leurs effets juridiques.

  

**ARTICLE 21.** **CONTACT ET INFORMATIONS LEGALES**

LOCASUN détient la qualité d’agence de voyage sous la licence IM095110020.  
  
La garantie financière de LOCASUN est apportée par GROUPAMA ASSURANCE-CRÉDIT & CAUTION 126 rue de la Piazza – 93199 NOISY-LE-GRAND CEDEX - Tél : 01 49 31 31 31.  
  
L’assureur professionnel de LOCASUN est Allianz I.A.R.D. 1 cours Michelet - CS 30051 - 92076 PARIS LA DEFENSE CEDEX.  
  
Le numéro de TVA intercommunautaire de LOCASUN est FR16432644995.  
  
Pour toute prise de contact avec LOCASUN, le Client peut se rapporter aux coordonnées suivantes :  
\- par courrier au 24 rue des Jeûneurs 75002 PARIS FRANCE. (adresse exclusivement réservée à la réception du courrier)  
\- par courriel à l’adresse suivante : reservation@locasun.com  
\- par téléphone au numéro suivant : 0820 900 406 (Lundi-vendredi : 9h-18h - Samedi : 10h30-14h / 14h30-19h)  
\- à partir du formulaire disponible dans la rubrique « CONTACT » sur le Site.  
  
Formulaire d'information standard pour des contrats portant sur un service de voyage visé au 2° du I de l'article L. 211-1 du code du tourisme.  
  
La combinaison de services de voyage qui vous est proposée est un forfait au sens de la directive (UE) 2015/2302 et de l'article L.211-2 II du code du tourisme. Vous bénéficierez donc de tous les droits octroyés par l'Union européenne applicables aux forfaits, tels que transposés dans le code du tourisme. LOCASUN sera entièrement responsable de la bonne exécution du forfait dans son ensemble.  
  
En outre, comme l'exige la loi, LOCASUN dispose d'une protection afin de rembourser vos paiements et, si le transport est compris dans le forfait, d'assurer votre rapatriement au cas où elle deviendrait insolvable. Pour plus d'informations sur les droits essentiels au titre de la directive (UE) 2015/2302 : [https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32015L2302&from=EN](https://eur-lex.europa.eu/legal-content/FR/TXT/PDF/?uri=CELEX:32015L2302&from=EN). En cliquant sur l'hyperlien, le voyageur recevra les informations suivantes :  
Droits essentiels au titre de la directive (UE) 2015/2302 transposée dans le code du tourisme :  
Les voyageurs recevront toutes les informations essentielles sur le forfait avant de conclure le contrat de voyage à forfait.  
L'organisateur ainsi que le détaillant sont responsables de la bonne exécution de tous les services de voyage compris dans le contrat.  
Les voyageurs reçoivent un numéro de téléphone d'urgence ou les coordonnées d'un point de contact leur permettant de joindre l'organisateur ou le détaillant.  
Les voyageurs peuvent céder leur forfait à une autre personne, moyennant un préavis raisonnable et éventuellement sous réserve de payer des frais supplémentaires.  
Le prix du forfait ne peut être augmenté que si des coûts spécifiques augmentent (par exemple, les prix des carburants) et si cette possibilité est explicitement prévue dans le contrat, et ne peut en tout cas pas être modifié moins de vingt jours avant le début du forfait. Si la majoration de prix dépasse 8 % du prix du forfait, le voyageur peut résoudre le contrat. Si l'organisateur se réserve le droit d'augmenter le prix, le voyageur a droit à une réduction de prix en cas de diminution des coûts correspondants. Les voyageurs peuvent résoudre le contrat sans payer de frais de résolution et être intégralement remboursés des paiements effectués si l'un des éléments essentiels du forfait, autre que le prix, subit une modification importante. Si, avant le début du forfait, le professionnel responsable du forfait annule celui-ci, les voyageurs ont le droit d'obtenir le remboursement et un dédommagement, s'il y a lieu.  
Les voyageurs peuvent résoudre le contrat sans payer de frais de résolution avant le début du forfait en cas de circonstances exceptionnelles, par exemple s'il existe des problèmes graves pour la sécurité au lieu de destination qui sont susceptibles d'affecter le forfait.  
En outre, les voyageurs peuvent, à tout moment avant le début du forfait, résoudre le contrat moyennant le paiement de frais de résolution appropriés et justifiables.  
Si, après le début du forfait, des éléments importants de celui-ci ne peuvent pas être fournis comme prévu, d'autres prestations appropriées devront être proposées aux voyageurs, sans supplément de prix. Les voyageurs peuvent résoudre le contrat sans payer de frais de résolution lorsque les services ne sont pas exécutés conformément au contrat, que cela perturbe considérablement l'exécution du forfait et que l'organisateur ne remédie pas au problème.  
Les voyageurs ont aussi droit à une réduction de prix et/ou à un dédommagement en cas d'inexécution ou de mauvaise exécution des services de voyage.  
L'organisateur ou le détaillant doit apporter une aide si le voyageur est en difficulté.  
Si l'organisateur ou le détaillant devient insolvable, les montants versés seront remboursés. Si l'organisateur ou le détaillant devient insolvable après le début du forfait et si le transport est compris dans le forfait, le rapatriement des voyageurs est garanti. LOCASUN a souscrit une protection contre l'insolvabilité auprès de GROUPAMA ASSURANCE-CRÉDIT & CAUTION. Les voyageurs peuvent prendre contact avec cette entité par courrier : 126 rue de la Piazza – 93199 NOISY-LE-GRAND CEDEX, courriel : cautionvoyage@groupama-ac.fr, ou appel téléphonique au 01 49 31 31 31, si des services leur sont refusés en raison de l'insolvabilité de LOCASUN.  
Directive (UE) 2015/2302 transposée en droit national : [cliquez ici](https://www.legifrance.gouv.fr/affichCodeArticle.do;jsessionid=B6B56671A51841699A8FB7B4B5EB08A2.tplgfr21s_1?idArticle=LEGIARTI000036242695&cidTexte=LEGITEXT000006074073&categorieLien=id&dateTexte=20180701)