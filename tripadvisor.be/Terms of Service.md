Algemene voorwaarden en kennisgevingen van de Tripadvisor-website
=================================================================

Gebruiksvoorwaarden van Tripadvisor

Laatst bijgewerkt: 16 februari 2024

**Gebruiksvoorwaarden**

[GEBRUIK VAN DE SERVICES](#_USE_OF_THE)

[AANVULLENDE PRODUCTEN](#_ADDITIONAL_PRODUCTS)

[VERBODEN ACTIVITEITEN](#_PROHIBITED_ACTIVITIES)

[PRIVACYBELEID EN OPENBARINGEN](#_PRIVACY_POLICY_AND)

[BEOORDELINGEN, REACTIES EN GEBRUIK VAN ANDERE INTERACTIEVE ZONES; LICENTIEVERLENING](#_REVIEWS,_COMMENTS_AND)

* [Licentierechten van Tripadvisor beperken](#_Restricting_Tripadvisor%E2%80%99s_Licence)

[VIA Tripadvisor BIJ DERDE LEVERANCIERS BOEKEN](#_BOOKING_WITH_THIRD-PARTY)

* [Gebruik van de boekingsservices van Tripadvisor](#_Use_of_Tripadvisor)
* [Derde leveranciers](#_Third-Party_Suppliers._The)
* [Vakantiewoningen, restaurantreserveringen en ervaringen boeken bij derde leveranciers die zijn vermeld op de websites van partnerbedrijven.](#_Booking_Holiday_Rentals,)

[REISBESTEMMINGEN](#_TRAVEL_DESTINATIONS)

* [Internationale reizen](#_International_Travel._When)

[AFWIJZING VAN AANSPRAKELIJKHEID](#_LIABILITY_DISCLAIMER_1)

[SCHADELOOSSTELLING](#_Indemnification)

[LINKS NAAR WEBSITES VAN DERDEN](#_LINKS_TO_THIRD-PARTY)

[SOFTWARE ALS ONDERDEEL VAN DE SERVICES; BIJKOMENDE MOBIELE LICENTIES](#_SOFTWARE_AS_PART)

[VERMELDINGEN AUTEURSRECHT EN HANDELSMERK](#_COPYRIGHT_AND_TRADEMARK)

* [Beleid voor de melding en verwijdering van illegale inhoud](#_Notice_and_Take-Down)

[AANPASSINGEN AAN DEZE OVEREENKOMST; DE SERVICES; UPDATES; BEËINDIGING EN INTREKKING](#_MODIFICATIONS_TO_THE)

[RECHTSGEBIED EN TOEPASSELIJK RECHT](#_JURISDICTION_AND_GOVERNING)

[VALUTA-OMREKENMACHINE](#_CURRENCY_CONVERTER)

[ALGEMENE BEPALINGEN](#_GENERAL_PROVISIONS)

[HULP BIJ SERVICES](#_SERVICE_HELP)

Welkom bij de Tripadvisor-websites en mobiele eigendommen die gevestigd zijn op https://www.tripadvisor.be/ en van toepassing zijnde landelijke topleveldomeinen (waaronder de bijbehorende subdomeinen), bijbehorende softwareapplicaties (soms ook wel ‘apps’ genoemd), gegevens, sms-berichten, API’s, e-mails, chat- en telefoongesprekken, knoppen, widgets en advertenties (naar deze items tezamen wordt hierin verwezen met de ‘**Services**’. In algemenere zin wordt hierin naar de Tripadvisor-website en mobiele eigendommen verwezen met ‘websites’).

De Services worden geleverd door Tripadvisor LLC, een naamloze vennootschap uit Delaware in de Verenigde Staten (' **Tripadvisor**') met maatschappelijke zetel te \[Tripadvisor LLC, 400 1st Avenue, Needham, MA 02494, USA\]. De termen 'wij', 'ons', 'onze' worden dienovereenkomstig geïnterpreteerd.

Door een Tripadvisor-account aan te maken, gaat u ermee akkoord gebonden te zijn aan de hieronder vermelde voorwaarden, bepalingen en mededelingen (gezamenlijk deze 'Overeenkomst'). Deze Overeenkomst is niet van toepassing als u alleen de websites bezoekt of de Serviecs gebruikt zonder een Tripadvisor-account aan te maken.  Lees deze Overeenkomst aandachtig door. Deze bevat informatie over uw wettelijke rechten en de beperkingen daarop, aangevuld met een sectie over het toepasselijke recht en het rechtsgebied voor geschillen. Als u een consument bent in de EU of het VK, heeft u het wettelijke recht om zich uit deze Overeenkomst terug te trekken uiterlijk binnen 14 dagen nadat de Overeenkomst tot stand is gekomen. U kunt dit doen, of deze Overeenkomst op elk ander moment beëindigen, door uw account te sluiten (door contact met ons op te nemen of door 'Accountgegevens' te raadplegen terwijl u bent ingelogd en de optie 'Account sluiten' te selecteren) en de Services niet meer te openen of te gebruiken.

Als u een consument bent in de EU of het VK, heeft u bepaalde dwingende consumentenrechten. Als u een consument bent die elders woont, heeft u mogelijk ook consumentenrechten volgens de wetgeving van uw gebied. Niets in deze Overeenkomst heeft invloed op uw dwingende consumentenrechten.

Alle informatie, tekst, links, afbeeldingen, foto's, audio, video’s, gegevens, code of andere materialen of sets materialen die u bekijkt, opent of anderszins gebruikt via de Services wordt hierin ‘Inhoud’ genoemd. ‘Services’, zoals hierboven gedefinieerd, verwijst naar de services geleverd door Tripadvisor of onze partnerbedrijven (Tripadvisor en deze entiteiten, als er naar één of meer worden verwezen, worden tezamen de ‘Bedrijven van Tripadvisor’ genoemd). Ter voorkoming van enige twijfel: de websites zijn eigendom van en worden beheerd door Tripadvisor.  Sommige specifieke Services die via de websites beschikbaar worden gesteld, kunnen echter het eigendom zijn en beheerd worden door partnerbedrijven van Tripadvisor, bijvoorbeeld Services voor het boeken van vakantiewoningen, restaurantreserveringen en ervaringen bij derde leveranciers (zie hieronder). Als onderdeel van onze Services kunnen wij u meldingen sturen over speciale aanbiedingen, producten of andere services die bij ons, de aan ons gelieerde ondernemingen of onze partners beschikbaar zijn en die mogelijk interessant voor u zijn. Dergelijke meldingen worden over het algemeen via de nieuwbrief en marketingberichten gestuurd en vertegenwoordigen onze inspanningen om meer inzicht te krijgen in u en uw voorkeuren op het gebied van onze Services en die van onze gelieerde ondernemingen. Dit zorgt er op zijn beurt voor dat de services aan die voorkeuren kunnen worden aangepast.

De term 'u' of 'gebruiker' verwijst naar de persoon, het bedrijf, de bedrijfsorganisatie of andere juridische entiteit die een Tripadvisor-account heeft aangemaakt en de Services gebruikt en/of er Inhoud aan bijdraagt. De Inhoud die u bijdraagt, verzendt, verstrekt en/of plaatst op, aan of via de Services wordt hierin genoemd ‘uw Inhoud’, ‘Inhoud die u verstrekt’ en/of ‘door u verstrekte Inhoud’.

De Services worden doorlopend geleverd en worden uitsluitend geleverd om:

1. klanten te helpen bij het verzamelen van reisinformatie, plaatsen van Inhoud en zoeken naar en boeken van reisservices en reserveringen; en
2. reis-, toerisme- en horecabedrijven te helpen bij het contact leggen met klanten en potentiële klanten via gratis en/of betaalde services aangeboden door of via Bedrijven van Tripadvisor.

#### Producten, services, informatie en Inhoud op onze Services veranderen regelmatig, zodat deze up-to-date zijn (bijvoorbeeld met actuele informatie en aanbiedingen). Dit betekent dat bijvoorbeeld met betrekking tot services van derden nieuwe vervoersservices, accommodaties, restaurants, tours, activiteiten of ervaringen mogelijk beschikbaar worden voor u om te boeken, terwijl andere services mogelijk niet meer beschikbaar zijn.

Wij kunnen deze Overeenkomst in de toekomst wijzigen of anderszins aanpassen in overeenstemming met de hierin vastgelegde voorwaarden en u begrijpt en aanvaardt dat de voortzetting van uw toegang tot of gebruik van de Services na een wijziging betekent dat u akkoord gaat met de bijgewerkte of gewijzigde Overeenkomst. Wij zullen de datum van de laatste revisies van deze Overeenkomst onderaan deze Overeenkomst vermelden en elke revisie zal vanaf haar publicatie van kracht zijn. We stellen u op de hoogte van belangrijke wijzigingen in deze algemene voorwaarden door een kennisgeving te sturen naar het e-mailadres dat u heeft opgegeven voor uw profiel of door een kennisgeving op onze websites te plaatsen. Bezoek deze pagina regelmatig om de nieuwste versie van deze Overeenkomst te raadplegen. Als u de wijzigingen aan deze Overeenkomst niet wilt accepteren, kunt u uw account sluiten en toegang tot de Services staken (een beschrijving van hoe u dit kunt doen is [hier](https://www.tripadvisorsupport.com/nl-nl/hc/traveler/articles/510) beschikbaar).

**GEBRUIK VAN DE SERVICES**

Als voorwaarde voor uw gebruik van de Services erkent u dat (i) alle informatie die u via de Services aan de Tripadvisor-bedrijven heeft verstrekt waar, accuraat, actueel en volledig is, (ii) u uw accountgegevens beschermt en toezicht houdt op en volledig verantwoordelijk bent voor het gebruik van uw account door iemand anders dan u, (iii) u 13 jaar of ouder bent. De Bedrijven van Tripadvisor verzamelen niet doelbewust informatie van personen onder de 13 jaar. Als u een consument bent die in de EU of het VK woont, kunnen we u de toegang tot de Diensten weigeren als u deze Overeenkomst schendt of om een andere redelijke reden. Als u geen consument bent die in de EU of het VK woont, behouden we het recht om u naar eigen goeddunken toegang tot de Diensten te weigeren, op elk moment en om welke reden dan ook, waaronder, maar niet beperkt tot, voor schending van deze Overeenkomst. Door gebruik te maken van de Diensten, waaronder producten en diensten die het delen van Inhoud van of naar websites van derden mogelijk maken, verklaart u te begrijpen dat u volledig aansprakelijk bent voor alle informatie die u deelt met de Bedrijven van Tripadvisor. U mag de Diensten uitsluitend zoals bedoeld gebruiken via de aangeboden functies van de Diensten en zoals toegestaan krachtens deze Overeenkomst.

Kopiëren, overdragen, reproduceren, repliceren, plaatsen of opnieuw verspreiden van (a) Inhoud of enig deel daarvan en/of (b) de Diensten in het algemeen is ten strengste verboden zonder voorafgaande schriftelijke toestemming van de Bedrijven van Tripadvisor. Om toestemming te vragen, richt u uw verzoek aan:

Director, Partnerships and Business Development

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, Verenigde Staten

Om toegang te krijgen tot bepaalde functies van de Diensten, moet u een account aanmaken. Als u een account aanmaakt dient u volledige en juiste informatie te verstrekken. U bent volledig verantwoordelijk voor de activiteit die middels uw account plaatsvindt, waaronder uw interactie en communicatie met anderen, en u dient uw account veilig te houden. Hiertoe ga je ermee akkoord je contactgegevens up-to-date te houden. 

Als u een Tripadvisor-account aanmaakt met commerciële doeleinden en u deze Overeenkomst aanvaardt namens een bedrijf, organisatie of andere rechtspersoon, verklaart en garandeert u dat u bevoegd bent om dit te doen en dat u de bevoegdheid heeft om de entiteit te binden aan deze Overeenkomst, in welk geval de woorden ‘u’ en ‘uw’ zoals gebruikt in deze Overeenkomst verwijzen naar de entiteit, en de persoon die namens het bedrijf handelt de ‘Bedrijfsvertegenwoordiger’ wordt genoemd.

In uw gebruik van de Diensten kunt u links naar websites en apps van derden tegenkomen of kunt u in contact komen met websites en apps van derden. Hieronder kan vallen de mogelijkheid om Inhoud van de Diensten te delen, waaronder uw Inhoud, met websites en apps van derden. Houd er rekening mee dat websites en apps van derden deze gedeelde Inhoud publiekelijk kunnen weergeven. Deze derde partijen kunnen een vergoeding in rekening brengen voor het gebruik van bepaalde inhoud of diensten die op of via hun websites worden aangeboden. U moet daarom elk onderzoek uitvoeren dat u nodig of gepast acht om te weten of er kosten in rekening gebracht zullen worden als u transacties uitvoert met een derde partij. Indien de Bedrijven van Tripadvisor informatie verstrekken over kosten of bedragen voor dergelijke inhoud of diensten van derde leveranciers, wordt deze informatie uitsluitend verstrekt met als doel te informeren en het gemak te dienen. Alle interacties met websites en apps van derden gebeuren op uw eigen risico. U erkent en komt uitdrukkelijk overeen dat de Bedrijven van Tripadvisor op geen enkele wijze verantwoordelijk of aansprakelijk zijn voor deze websites of apps van derden.

Tripadvisor-bedrijven classificeren en presenteren vermeldingen aan u met behulp van aanbevolen systemen. Verschillende categorieën vermeldingen (zoals hotels en restaurants, bezienswaardigheden en vluchten) worden gerangschikt op basis van verschillende criteria, zoals prijs, 'favorieten van reizigers' en duur. Alle informatie over ons beleid en onze criteria voor classificatie vindt u op de webpagina [Hoe de Site werkt](https://www.tripadvisor.nl/HowTheSiteWorks.html) .

Sommige Inhoud die u ziet of anderszins tegenkomt op of via de Diensten wordt gebruikt voor commerciële doeleinden. U begrijpt en komt overeen dat de Bedrijven van Tripadvisor advertenties en promoties kunnen plaatsen op de Diensten, naast, nabij, langs of anderszins in de buurt van uw Inhoud en de Inhoud van anderen (waaronder, voor video- of ander dynamisch materiaal, voor, tijdens of na de weergave daarvan).

Aanvullende producten

De Bedrijven van Tripadvisor kunnen, van tijd tot tijd, besluiten om bepaalde producten en de functies van bepaalde Diensten te wijzigen, bij te werken, of stop te zetten. U begrijpt en gaat ermee akkoord dat de Bedrijven van Tripadvisor niet verplicht zijn om uw Inhoud of andere informatie die u verstrekt op te slaan of te behouden, behalve in hoeverre dit door toepasselijke wetgeving is vereist.

We leveren tevens andere diensten waarop aanvullende voorwaarden of overeenkomsten van toepassing kunnen zijn. Als u deze andere diensten gebruikt, worden de aanvullende voorwaarden kenbaar gemaakt aan u en worden deze voorwaarden onderdeel van deze Overeenkomst, tenzij deze aanvullende voorwaarden deze Overeenkomst uitdrukkelijk uitsluiten of vervangen. Bijvoorbeeld, als u aanvullende diensten gebruikt of aanschaft voor commerciële of zakelijke doeleinden dient u akkoord te gaan met de van toepassing zijnde aanvullende voorwaarden. Voor zover andere voorwaarden in strijd zijn met de voorwaarden van deze Overeenkomst, genieten de aanvullende voorwaarden voorrang in de mate van het conflict ten aanzien van deze specifieke diensten.

**VERBODEN ACTIVITEITEN**

De Inhoud en informatie die beschikbaar is op en via de Diensten (waaronder, maar niet beperkt tot, berichten, gegevens, informatie, tekst, muziek, geluid, foto's, beeldmateriaal, video's, kaarten, pictogrammen, software, code of ander materiaal) alsmede de infrastructuur die wordt gebruikt om de Inhoud en informatie aan te bieden, zijn het eigendom van de Bedrijven van Tripadvisor of in licentie gegeven aan de Bedrijven van Tripadvisor door derden. Voor alle Inhoud anders dan uw eigen Inhoud, verklaart u dat u informatie, inhoud, software, producten of diensten die u uit of via de Diensten heeft verkregen niet zult wijzigen, kopiëren, distribueren, verzenden, vertonen, uitvoeren, reproduceren, publiceren, in licentie geven, gebruiken om afgeleide werken te creëren, overdragen, kopen of wederverkopen. Daarnaast komt u overeen om:

*  (i) de Diensten of Inhoud niet te gebruiken voor commerciële doeleinden buiten de reikwijdte van de commerciële doeleinden die uitdrukkelijk zijn toegestaan krachtens deze Overeenkomst en de bijbehorende richtlijnen zoals door de Bedrijven van Tripadvisor beschikbaar gemaakt.
* (ii) enige Inhoud van de Diensten waaronder, maar niet beperkt tot, gebruikersprofielen en foto's niet te openen, monitoren, reproduceren, distribueren, verzenden, uitzenden, vertonen, verkopen, in licentie geven, kopiëren of op andere wijze te exploiteren door middel van robots, spiders, scrapers of andere geautomatiseerde processen of enig handmatig proces voor doeleinden die niet in overeenstemming zijn met deze Overeenkomst of zonder onze uitdrukkelijke schriftelijke toestemming;
* (iii) de beperkingen in robotuitsluitingsmeldingen in de Diensten niet te schenden en geen andere maatregelen te omzeilen of te ontwijken die zijn ingesteld om toegang tot de Diensten te voorkomen of beperken;
* (iv) enige actie te ondernemen die naar eigen goeddunken (of, als u een consument bent die in het VK of de EU woont, naar redelijkerwijs goeddunken) een onredelijke of onevenredig grote belasting van onze infrastructuur oplegt of kan opleggen;
* (v) geen diepe links aan te brengen naar een deel van deze Diensten, met om het even welk doel, zonder onze uitdrukkelijke schriftelijke toestemming;
* (vi) geen deel van de Diensten middels een 'frame', 'mirror' of anderszins op te nemen in een andere website of dienst zonder onze uitdrukkelijke schriftelijke toestemming;
* (vii) niet te trachten softwareprogramma's die de Bedrijven van Tripadvisor in verband met de Diensten gebruiken te wijzigen, te vertalen, aan te passen, te bewerken, te decompileren, te disassembleren of te reverse engineeren;
* (viii) de beveiligingsfuncties van de Diensten of de functies die het gebruik of kopiëren van Inhoud voorkomen of beperken niet te omzeilen, uit te schakelen of anderszins te belemmeren; en
* (ix) geen Inhoud te downloaden tenzij het uitdrukkelijk beschikbaar wordt gemaakt om te downloaden door de Bedrijven van Tripadvisor.
* (x) een robot-, spin- of AI-systeem (kunstmatige intelligentie) of een ander geautomatiseerd apparaat, proces of middel te gebruiken, assisteren, aanmoedigen of in staat stellen om toegang te krijgen tot een deel van de Diensten of Inhoud, deze op te vragen, te kopiëren, te schrapen, te aggregeren, te verzamelen, te downloaden of anderszins te indexeren, tenzij uitdrukkelijk schriftelijk toegestaan door Tripadvisor.

**PRIVACYBELEID EN OPENBARINGEN**

Alle persoonlijke informatie die u publiceert of anderszins verzendt in verband met de Diensten, zal gebruikt worden in overeenstemming met ons Privacybeleid. Klik [hier](https://tripadvisor.mediaroom.com/uk-privacy-policy) om onze Privacy- en cookieverklaring te bekijken.

**BEOORDELINGEN, REACTIES EN GEBRUIK VAN ANDERE INTERACTIEVE ZONES; LICENTIEVERLENING**

Wij horen graag van u. Let op: door uw Inhoud aan of via de Diensten te verstrekken, hetzij via e-mail, hetzij via een synchronisatieproduct van Tripadvisor, de diensten en applicaties van anderen of anderszins, met inbegrip van enig gedeelte van de Inhoud dat via een product of dienst van een van de Bedrijven van Tripadvisor naar uw Tripadvisor-account is verzonden, beoordelingen, vragen, foto's en video's, reacties, suggesties, ideeën en soortgelijke informatie in uw Inhoud, verleent u de Bedrijven van Tripadvisor een niet-exclusief, royaltyvrij, eeuwigdurend, overdraagbaar, onherroepelijk en volledig sublicentieerbaar recht om (a) wereldwijd en in elke nu bestaande of in de toekomst ontwikkelde media Inhoud te hosten, gebruiken, reproduceren, wijzigen, beheren, aanpassen, vertalen, verspreiden, publiceren, afgeleide werken daarvan te creëren en openbaar te tonen en uit te voeren; (b) uw Inhoud beschikbaar te maken voor de rest van de wereld en anderen in staat te stellen om hetzelfde te doen; (c) de Diensten te leveren, promoten en verbeteren en uw Inhoud die via de Diensten wordt gedeeld beschikbaar te maken voor andere bedrijven, organisaties of personen voor het uitzenden, verspreiden, promoten, publiceren of via een syndicaat leveren van dergelijke Inhoud aan andere media en diensten die vallen onder ons Privacybeleid en deze Overeenkomst; en (d) de naam en/of het handelsmerk te gebruiken die u in verband met de Inhoud verstrekt. U erkent dat Tripadvisor ervoor kan kiezen om uw Inhoud naar eigen goeddunken toe te wijzen (of, als u een consument bent die in het VK of de EU woont, naar ons redelijke goeddunken). U verleent de Bedrijven van Tripadvisor bovendien het recht om elke persoon of entiteit die uw rechten of de rechten van de Bedrijven van Tripadvisor met betrekking tot uw Inhoud schendt door het overtreden van deze Overeenkomst, gerechtelijk te vervolgen. U bevestigt en gaat ermee akkoord dat de Inhoud niet vertrouwelijk en niet uw eigendom is. U bevestigt, verklaart en garandeert dat u de nodige licenties, rechten (waaronder auteursrecht en andere intellectuele eigendomsrechten), toestemmingen en vergunningen bezit of heeft om uw Inhoud te publiceren of anderszins te gebruiken (en uw Inhoud door de Bedrijven van Tripadvisor te laten publiceren en anderszins te gebruiken) zoals uit hoofde van deze Overeenkomst is toegestaan.

Indien wordt vastgesteld dat u morele rechten (waaronder het recht van naamsvermelding of integriteit) ten aanzien van uw Inhoud bezit, verklaart u hierbij, voor zover de wet dit toestaat, dat (a) u niet eist dat er persoonlijk identificeerbare informatie wordt vermeld bij de Inhoud of afgeleide werken, upgrades of updates van de Inhoud; (b) u geen bezwaar heeft tegen de publicatie, het gebruik, de aanpassing, de verwijdering en de exploitatie van de Inhoud door de Bedrijven van Tripadvisor of hun licentiehouders, opvolgers en rechtverkrijgenden; (c) u definitief afstand doet van en verklaart geen aanspraak te maken op de morele auteursrechten op enig gedeelte van uw Inhoud; en (d) u de Bedrijven van Tripadvisor, hun licentiehouders, opvolgers en rechtverkrijgenden definitief vrijwaart van alle eisen die u anders tegen de Bedrijven van Tripadvisor zou kunnen instellen op grond van dergelijke morele rechten.

Houd er rekening mee dat alle feedback en andere suggesties die u geeft te allen tijde mogen worden gebruikt en dat wij niet verplicht zijn deze geheim te houden.

De Diensten kunnen discussieforums, bulletinboards, beoordelingsdiensten en reisoverzichten bevatten of andere forums waar u uw Inhoud, zoals reiservaringen, berichten, materiaal of andere zaken ('Interactieve Zones') kunt publiceren. Indien Tripadvisor dergelijke Interactieve Zones op de websites tot uw beschikking stelt, bent u als enige verantwoordelijk voor uw gebruik van de Interactieve Zones en gebruikt u ze op eigen risico. De Bedrijven van Tripadvisor garanderen niet de vertrouwelijkheid van enig gedeelte van uw Inhoud die u aan de Diensten of in een Interactieve zone verstrekt.  Voor zover een entiteit die een van de Bedrijven van Tripadvisor is een vorm van een privécommunicatiekanaal tussen Accounthouders aanbiedt, gaat u ermee akkoord dat deze entiteit(en) de inhoud van dergelijke communicatie mag controleren om onze community en de Diensten te beschermen. U begrijpt dat de Bedrijven van Tripadvisor geen redactie uitvoeren op en geen controle uitoefenen over de gebruikersberichten die via de Diensten, waaronder via chatrooms, bulletinboards of andere communicatieforums, worden gepubliceerd of verspreid en dat zij in geen geval verantwoordelijk of aansprakelijk zijn voor dergelijke berichten.  Tripadvisor voert met name geen redactie uit op en oefent geen controle uit over de Inhoud van gebruikers die op de websites wordt weergegeven.  De Bedrijven van Tripadvisor behouden zich echter het recht voor om dergelijke berichten of andere Inhoud van de Diensten zonder nadere kennisgeving te verwijderen indien zij oprecht denken dat dergelijke Inhoud in strijd is met deze Overeenkomst of anderszins van mening zijn dat de verwijdering redelijkerwijs noodzakelijk is om de rechten van de Bedrijven van Tripadvisor en/of andere gebruikers van de Diensten te beschermen. Als u het niet eens bent met het verwijderen van uw Inhoud van de websites, kunt u bezwaar maken tegen onze beslissing via het interne klachtenafhandelingsproces van Tripadvisor. Meer informatie over hoe u bezwaar kunt maken tegen en bezwaar kunt maken tegen een beslissing om uw inhoud te verwijderen of te beperken, vindt u [hier](https://www.tripadvisorsupport.com/en-US/hc/traveler/articles/623) . Door Interactieve Zones te gebruiken, verklaart u uitdrukkelijk dat u alleen Inhoud zult verzenden die overeenkomt met de gepubliceerde Tripadvisor-richtlijnen zoals die gelden op het moment dat u de Inhoud verzendt en ter beschikking zijn gesteld door Tripadvisor.  U verklaart uitdrukkelijk dat u zich zult onthouden van het publiceren, uploaden, verzenden, verspreiden, opslaan, aanmaken of anderszins publiceren van enige Inhoud via de Diensten die:

1. vals, onwettig, misleidend, lasterlijk, smadelijk, obsceen, pornografisch, onzedig, schunnig, of suggestief is of waarmee andere personen gepest worden of pesten wordt aangemoedigd, die bedreigend is, inbreuk maakt op privacy- of publiciteitsrechten, beledigend is, opruiend is of bedrieglijk of anderszins bezwaarlijk is;
2. duidelijk kwetsend is voor de online community, zoals inhoud die racisme, onverdraagzaamheid, haat of fysieke agressie tegen welke groep of individu dan ook aanmoedigt;
3. een illegale activiteit, een strafbaar feit zou vormen, aanmoedigen, promoten of instructies zou geven voor het uitvoeren ervan, aanleiding zou geven tot burgerlijke aansprakelijkheid, de rechten van een partij in enig land ter wereld zou schenden, of anderszins aansprakelijkheid zou creëren of in strijd zou zijn met lokale, nationale of internationale wetgeving, waaronder, maar niet beperkt tot, de regelgeving van de VS Securities and Exchange Commission (SEC) of enige regels van een effectenbeurs, met inbegrip van maar niet beperkt tot de New York Stock Exchange (NYSE), de NASDAQ of de London Stock Exchange;
4. instructies bevat voor illegale activiteiten zoals het maken of kopen van illegale wapens, het schenden van iemands privacy of het leveren of maken van computervirussen;
5. inbreuk kan maken op patenten, handelsmerken, handelsgeheimen, auteursrechten of andere intellectuele eigendomsrechten van om het even welke partij;  in het bijzonder, het maken van illegale of ongeoorloofde kopieën van het auteursrechtelijk beschermd werk van een ander aanmoedigt, zoals het verstrekken van illegale computerprogramma's of links ernaar, het verschaffen van informatie om door een fabrikant geïnstalleerde systemen voor kopieerbeveiliging te omzeilen, of het verstrekken van illegale muziek of links naar illegale muziekbestanden;
6. een massamailing of 'spam', 'junkmail', 'kettingbrief' of 'pyramidesysteem' is;
7. afkomstig lijkt te zijn van een andere persoon of entiteit of die anderszins een verkeerde voorstelling geeft van uw banden met een persoon of entiteit, met inbegrip van de Bedrijven van Tripadvisor;
8. bestaat uit de privégegevens van een derde, waaronder, maar niet beperkt tot, adressen, telefoonnummers, e-mailadressen, burgerservicenummers en creditcardnummers.  Let op: het is mogelijk dat iemands achternaam op onze websites wordt gepubliceerd. Dit gebeurt echter uitsluitend wanneer hiervoor op voorhand uitdrukkelijke toestemming van de geïdentificeerde persoon is verkregen;
9. pagina's bevat die beperkt of alleen met een wachtwoord toegankelijk zijn, of verborgen pagina’s of afbeeldingen (die niet gekoppeld zijn aan een andere, toegankelijke pagina);
10. virussen, corrupte gegevens of andere schadelijke, verstorende of destructieve bestanden bevat of bedoeld is om deze te verspreiden;
11. geen verband houdt met het onderwerp van de Interactieve Zone(s) waar zij wordt gepubliceerd; of
12. die volgens het uitsluitende inzicht van Tripadvisor (a) de bovenstaande subsecties schendt, (b) de bijbehorende richtlijnen van Tripadvisor schendt zoals deze door Tripadvisor aan u beschikbaar zijn gesteld, (c) bezwaarlijk is, (d) de mogelijkheden van een andere persoon om de Interactieve Zones of een ander deel van de Diensten te gebruiken of te benutten, beperkt of belet, of (e) de Bedrijven van Tripadvisor of hun gebruikers kan blootstellen aan enige vorm van schade of aansprakelijkheid.

De Bedrijven van Tripadvisor zijn niet verantwoordelijk of aansprakelijk voor enige Inhoud die is geplaatst, opgeslagen, verzonden of geüpload door u (in het geval van uw Inhoud) of door een derde partij (in het geval van alle Inhoud van algemenere aard), of voor enig verlies van of schade aan dergelijke Inhoud. De Bedrijven van Tripadvisor zijn evenmin verantwoordelijk voor vergissingen, eerroof, laster, smaad, omissies, onwaarheden, obsceniteiten, pornografie of godslastering waarmee u mogelijk wordt geconfronteerd. Tripadvisor is als leverancier van interactieve diensten niet verantwoordelijk voor enige verklaringen, garanties of andere Inhoud die door haar gebruikers (waaronder u ten aanzien van uw Inhoud) op de websites of een ander forum worden geplaatst. Hoewel Tripadvisor niet verplicht is om de Inhoud die geplaatst is op of verspreid wordt via een Interactief gebied te controleren, bewerken of controleren, behoudt Tripadvisor het recht en heeft het absolute discretie (of, als u een consument bent die in het VK of de EU woont, naar redelijke discretie) om inhoud die op enig moment en om welke reden dan ook geplaatst of opgeslagen is op de Diensten te verwijderen, te screenen, te vertalen of te bewerken zonder kennisgeving, of om dergelijke handelingen door derden namens hen te laten uitvoeren. Voor zover toegestaan door de toepasselijke wetgeving, bent u als enige verantwoordelijk voor het maken van reservekopieën van en het vervangen van enige Inhoud die u plaatst of anderszins bij ons indient of opslaat op de Diensten, uitsluitend op uw kosten. Alle informatie over ons beleid voor contentmoderatie, inclusief de beperkingen die we mogelijk aan uw inhoud opleggen, vindt u [hier](https://www.tripadvisor.co.uk/Trust) .

**BEPERKING EN/OF OPSCHORTING VAN UW GEBRUIK VAN DE DIENSTEN**

Elk gebruik van de Interactieve Zones of andere delen van de Diensten in strijd met de bovenstaande voorwaarden vormt een schending van deze Overeenkomst en kan onder meer aanleiding geven tot de beëindiging of opschorting van uw rechten om de Interactieve Zones en/of de Diensten in het algemeen te gebruiken. Tripadvisor kan ook de verwerking van kennisgevingen en klachten die via de Diensten zijn ingediend opschorten als u vaak kennelijk ongegronde kennisgevingen of klachten indient.

Onze benadering van de opschorting van uw gebruik van het platform wordt [hier](https://www.tripadvisor.nl/Trust) nader beschreven .

**Licentierechten van Tripadvisor beperken****.** U kunt ervoor kiezen om het gebruik van uw Inhoud door de Bedrijven van Tripadvisor uit hoofde van deze Overeenkomst in de toekomst te beperken (zoals hierboven beschreven) door ervoor te kiezen om de Bedrijven van Tripadvisor een beperktere licentie te verlenen zoals hieronder nader is beschreven (dergelijke beperkte licentie wordt hierna een 'Beperkte licentie' genoemd).  U kunt deze keuze maken door [**hier**](https://www.tripadvisorsupport.com/nl-NL/hc/traveler/articles/510) een Beperkte licentieverlening te selecteren (om dit te doen moet u zijn uitgelogd). Als u deze keuze maakt, zullen de rechten die u ten aanzien van uw Inhoud en volgens de hierboven beschreven licentievoorwaarden (hierna de 'Standaardlicentie' genoemd) aan de Bedrijven van Tripadvisor verleent, op bepaalde belangrijke manieren worden beperkt, zoals beschreven in de leden 1 tot en met 6 hier direct onder, waardoor de Bedrijven van Tripadvisor niet langer een Standaardlicentie hebben ten aanzien van uw Inhoud anders dan de tekstuele beoordelen en bijbehorende bollenwaarderingen die u plaatst (waarvoor de Bedrijven van Tripadvisor nog altijd een Standaardlicentie hebben), maar een 'Beperkte licentie' ten aanzien van uw Inhoud, zoals hieronder beschreven:

1. Wanneer u uw Inhoud op de Diensten plaatst, zal de licentie die u ten aanzien van uw Inhoud aan de Bedrijven van Tripadvisor verleent worden beperkt tot een niet-exclusieve, royaltyvrije, overdraagbare, sublicentieerbare en wereldwijde licentie voor het hosten, gebruiken, verspreiden, aanpassen, beheren, reproduceren, in het openbaar vertonen of uitvoeren, vertalen en het creëren van afgeleide werken van uw Inhoud met het doel om deze Inhoud op de Diensten te tonen en uw naam en/of handelsmerk in verband met deze Inhoud te gebruiken. Behoudens het bepaalde in onderstaand artikel 6 is de Beperkte licentie van toepassing op al uw Inhoud (nogmaals, anders dan tekstuele beoordelingen en de bijbehorende bollenwaarderingen) die u of die een ander namens u (d.w.z. een derde die een bijdrage levert aan of anderszins uw account beheert) beschikbaar stelt op of in verband met de Diensten.

1. Voor elk individueel onderdeel van uw Inhoud dat onder de Beperkte licentie valt, kunt u de licentierechten van de Bedrijven van Tripadvisor uit hoofde van deze licentie beëindigen door een dergelijke post van de Diensten te verwijderen.  Evenzo kunt u de licentierechten van de Bedrijven van Tripadvisor ten aanzien van al uw Inhoud die onder de Beperkte licentie valt beëindigen door uw account te beëindigen (een beschrijving van hoe u dit doet, kunt u [**hier**](https://www.tripadvisorsupport.com/nl-nl/hc/traveler/articles/510) vinden). Onverminderd andersluidende bepalingen, kan uw Inhoud (a) op de Diensten blijven staan mits u deze met anderen heeft gedeeld en zij de Inhoud hebben gekopieerd voordat u deze heeft verwijderd of uw account heeft beëindigd, (b) gedurende een redelijke termijn nadat u de Inhoud verwijdert of uw account beëindigt op de Diensten blijven staan totdat wij kans zien om deze definitief te verwijderen, en/of (c) bepaalde tijd om technische, reglementaire of wettelijke redenen of om fraude te voorkomen in een reservekopie worden bewaard (maar niet openbaar worden getoond).

1. De Bedrijven van Tripadvisor zullen uw Inhoud niet gebruiken in advertenties voor de producten en diensten van derden aan anderen zonder uw afzonderlijke toestemming daarvoor (inclusief gesponsorde Inhoud), ondanks dat u begrijpt en ermee akkoord gaat dat de Bedrijven van Tripadvisor advertenties en aanbiedingen kunnen plaatsen op de Diensten, naast, nabij, langs of anderszins in de buurt van uw Inhoud en de Inhoud van anderen (waaronder, voor video- of ander dynamisch materiaal, voor, tijdens of na de weergave daarvan).  In alle gevallen waarin uw Inhoud op de Diensten wordt getoond zullen wij de naam en/of handelsmerk gebruiken die u in verband met uw Inhoud heeft opgegeven.  

1. De Bedrijven van Tripadvisor zullen derden niet het recht geven om uw Inhoud buiten de Diensten te publiceren. Wanneer u uw Inhoud echter op de Diensten deelt (behalve voor onze 'Trips'-functie die privé kan worden gemaakt), wordt uw Inhoud '_openbaar_'. Wij zullen een functie inschakelen waarmee andere gebruikers dergelijke Inhoud (behalve, zoals reeds genoemd, als u Trips zodanig instelt dat deze privé is) kunnen delen (door deze openbare post op te nemen in hun eigen bericht of anders) op diensten van derden en wij zullen zoekmachines in staat stellen om dergelijke openbare Inhoud vindbaar te maken via hun diensten.

1. Tenzij anders in de leden 1 tot en met 10 van dit artikel van deze Overeenkomst anders is bepaald, vallen uw en onze rechten en plichten onder de voorwaarden van deze Overeenkomst. De licentie die u aan de Bedrijven van Tripadvisor verleent, zoals gewijzigd door onderhavige leden 1 tot en met 6, wordt hierna een 'Beperkte licentie' genoemd.

1. Voor alle duidelijkheid: de Inhoud die u in verband met andere diensten of programma's van de Bedrijven van Tripadvisor naar de Diensten verzendt, valt niet onder de Beperkte licentie maar onder de voorwaarden van de desbetreffende dienst of programma van Tripadvisor.

 

**VIA Tripadvisor BIJ DERDE LEVERANCIERS BOEKEN**

**Gebruik van de boekingsdiensten van Tripadvisor.** De Bedrijven van Tripadvisor bieden u de mogelijkheid om reisreserveringen te zoeken, te selecteren en te boeken bij derde leveranciers, zonder de Diensten te verlaten.

#### BELANGRIJK: Tripadvisor verkoopt zelf geen reisreserveringen. Met Tripadvisor kunnen derden hun reisreserveringen verkopen op Tripadvisor.

#### Tripadvisor kan de promotie en verkoop van reisreserveringen door derden aan u mogelijk maken. Tripadvisor is echter niet de partij die de promotie en/of verkoop aangaat. Als u een reservering bij een externe leverancier boekt, is het contract voor de reservering altijd alleen tussen u en de derde partij. Tripadvisor is niet (a) de koper of de verkoper van de reservering; (b) een partij bij de overeenkomst met de derde partij of verantwoordelijk voor de uitvoering van die overeenkomst; of (c) een agent van u of de derde partij in verband met aankoop of verkoop. Als u bij een derde leverancier reserveert dan gaat u er, naast deze Overeenkomst, mee akkoord de verkoopvoorwaarden en de gebruiksvoorwaarden voor de website van de leverancier, het privacybeleid en andere voorschriften of beleidsregels gerelateerd aan de site of gelegenheid van de leverancier te lezen en als bindend te aanvaarden. Uw interacties met derde leveranciers gebeuren op uw eigen risico. De Bedrijven van Tripadvisor zijn niet aansprakelijk voor de handelingen, het verzuim, de fouten, voorstellingen, garanties, inbreuken of nalatigheden van derde leveranciers, en evenmin voor persoonlijke letsel, overlijden, schade aan eigendom of andere schade of kosten die voortvloeien uit uw interacties met derde leveranciers.

Uw regresrecht in verband met een contract tussen u en een derde partij (bijvoorbeeld met betrekking tot restituties en annuleringen) is tussen u en de derde partij zoals uiteengezet in het contract met die derde partij. In het onwaarschijnlijke geval dat er een reservering beschikbaar is wanneer u een bestelling plaatst maar niet beschikbaar wordt voor het inchecken, is uw enige rechtsmiddel tussen ons en u dat u contact opneemt met de leverancier om alternatieve regelingen te treffen of om uw reservering te annuleren en een restitutie te verkrijgen (indien van toepassing en onderhevig aan de toepasselijke wetgeving en uw contract met de externe leverancier).

#### Externe leveranciers kunnen bedrijven of consumenten zijn. Als u een consument bent die in het Verenigd Koninkrijk of de EU woont en de derde partij waarmee u een overeenkomst heeft afgesloten ook een consument is, let er dan op dat u geen consumentenrechten tegen die leverancier kunt afdwingen.

Als u via de websites een reisreservering boekt, moet u een account aanmaken als u dit nog niet heeft gedaan en erkent u dat u de praktijken aanvaardt die worden beschreven in ons Privacybeleid en deze Overeenkomst.

ALS GEBRUIKER VAN DE DIENSTEN, WAARONDER DE BOEKINGSFACILITERINGSDIENSTEN VAN TRIPADVISOR-BEDRIJVEN, BEGRIJPT EN STEMT U ERMEE IN DAT, VOOR ZOVER TOEGESTAAN DOOR DE TOEPASSELIJKE WETGEVING: (1) DE BEDRIJVEN VAN TRIPADVISOR NIET AANSPRAKELIJK ZIJN JEGENS U OF ANDEREN VOOR ONGEOORLOOFDE TRANSACTIES DIE MET UW WACHTWOORD OF ACCOUNT WORDEN UITGEVOERD; EN (2) DAT U AANSPRAKELIJK GESTELD KUNT WORDEN VOOR HET ONGEOORLOOFDE GEBRUIK VAN UW WACHTWOORD OF ACCOUNT JEGENS ZOWEL TRIPADVISOR ALS AAN HAAR GELIEERDE BEDRIJVEN EN/OF ANDEREN.

Het gebruik van onze Diensten, waaronder het aanmaken van een Tripadvisor-account, is gratis en een betaling wordt alleen verwerkt wanneer u via de Diensten een reservering boekt. Wanneer u via de Bedrijven van Tripadvisor een reservering maakt, worden uw betalingsgegevens verzameld en doorgestuurd naar de leverancier om de transactie te voltooien, zoals beschreven in ons Privacybeleid. Let op: niet de Bedrijven van Tripadvisor maar de leverancier is verantwoordelijk voor de verwerking van uw betaling en het uitvoeren van uw reservering.

De Bedrijven van Tripadvisor zullen zich niet zomaar in reserveringen mengen, maar zij behouden zich het recht voor om boekingsdiensten stop te zetten wegens bepaalde verzachtende omstandigheden, bijvoorbeeld wanneer een reservering niet langer beschikbaar is of wij reden hebben om aan te nemen dat een reserveringsverzoek bedrieglijk is. De Bedrijven van Tripadvisor behouden zich ook het recht voor om uw identiteit te verifiëren om uw reserveringsverzoek te verwerken.

**Derde leveranciers** De Bedrijven van Tripadvisor zijn geen reisbureaus. Zij verzorgen en bezitten geen eigen vervoersdiensten, accommodaties, restaurants, tours, activiteiten of andere ervaringen. Hoewel de Bedrijven van Tripadvisor informatie geven over gelegenheden van derde leveranciers en reserveringen mogelijk maken bij bepaalde leveranciers op of via de websites van de Bedrijven van Tripadvisor, impliceren, suggereren of vormen deze zaken in geen enkel opzicht een sponsorschap of goedkeuring van de Bedrijven van Tripadvisor ten opzichte van deze derde leveranciers, noch dat er banden bestaan tussen de Bedrijven van Tripadvisor en derde leveranciers. Hoewel Accounthouders bepaalde vervoersdiensten, accommodaties, restaurants, tours, activiteiten of ervaringen kunnen evalueren en bespreken op basis van hun eigen ervaringen, onderschrijven de Bedrijven van Tripadvisor de producten of diensten van derde leveranciers niet en bevelen zij ze ook niet aan, behalve dat Tripadvisor bepaalde awards uitreikt die zijn gebaseerd op de beoordelingen die door Accounthouders zijn geplaatst. De Bedrijven van Tripadvisor onderschrijven geen enkele door een gebruiker of bedrijf geplaatste, verzonden of anderszins verstrekte Inhoud of mening, aanbeveling of advies die daarin wordt gegeven en de Bedrijven van Tripadvisor wijzen uitdrukkelijk alle aansprakelijkheid in verband met dergelijke Inhoud af. U aanvaardt dat de Bedrijven van Tripadvisor niet verantwoordelijk zijn voor de juistheid of volledigheid van de informatie die het van derde leveranciers ontvangt en op de Diensten toont.

De Diensten kunnen links bevatten naar sites van leveranciers of andere sites die Tripadvisor niet beheert of controleert. Raadpleeg voor meer informatie de onderstaande sectie ‘Links naar websites van derden’.

**Reisservices boeken via de mobiele app van Tripadvisor.**  Voor een aantal van de reisdiensten die via de mobiele app van Tripadvisor worden geboekt, gelden aanvullende gebruiksvoorwaarden.  Deze voorwaarden zijn beschikbaar via de app zelf en vind je [hier](https://tripadvisor.mediaroom.com/us-terms-of-use-booking)  .  

**Vakantiewoningen, restaurantreserveringen en ervaringen boeken bij derde leveranciers die zijn vermeld op de websites van partnerbedrijven.** Sommige van de aan Tripadvisor gelieerde bedrijven treden op als marktplaatsen om het reizigers gemakkelijker te maken om (1) een overeenkomst voor de huur van een vakantiewoning aan te gaan met eigenaars en beheerders van gelegenheden ('Vakantiewoningen'), (2) reserveringen te plaatsen bij restaurants ('Restaurants') en/of (3) reserveringen te boeken voor tours, activiteiten en attracties ('Ervaringen') bij derde leveranciers van dergelijke Ervaringen (elke leverancier van een vakantiewoning en/of Ervaring wordt hierna een 'Adverteerder' genoemd). Deze aan Tripadvisor gelieerde bedrijven verspreiden hun advertenties via andere entiteiten binnen de groep Bedrijven van Tripadvisor en daarom ziet u ze op de websites van de Bedrijven van Tripadvisor.  Als gebruiker bent u verantwoordelijk voor uw gebruik van de Diensten (waaronder met name de websites van de Bedrijven van Tripadvisor) en alle transacties rond het reserveren van Vakantiewoningen, Restaurants of Ervaringen die door de aan Tripadvisor gelieerde bedrijven mogelijk worden gemaakt. Wij bezitten, beheren en verzorgen geen boekingen voor de Vakantiewoningen, Restaurants of Ervaringen die op de Diensten zijn vermeld.

Omdat Tripadvisor en haar partners geen partij zijn in transacties tussen reizigers en Adverteerders in verband met de huur van Vakantiewoningen, het boeken van een Ervaring of het plaatsen van een reservering bij een Restaurant, is elke gebruiker volledig verantwoordelijk voor elk geschil of elke betwisting in verband met een feitelijke of mogelijke transactie tussen u en een Adverteerder, met inbegrip van de kwaliteit, de toestand, de veiligheid of de wettelijkheid van een vermelde Vakantiewoning, Restaurant of Ervaring, de nauwkeurigheid van de Inhoud van de vermelding, het vermogen van de Adverteerder om een vakantiewoning te verhuren, een reservering te boeken, een maaltijd of andere dienst bij een Restaurant te verzorgen of een Ervaring te leveren, uw vermogen om de huur van een vakantiewoning, een maaltijd of dienst in een Restaurant of een Ervaring te betalen.

Een van de aan Tripadvisor gelieerde bedrijven kan optreden als beperkte agent van een Adverteerder, uitsluitend om uw betaling door te geven aan de Adverteerder. U gaat ermee akkoord om een Adverteerder, of een zakelijke partner van Tripadvisor die optreedt als incassoagent voor beperkte betalingen namens een Adverteerder, te betalen, alle toeslagen die aan u zijn gespecificeerd voordat u een boeking doet voor een Vakantiewoningreservering of Ervaring en alle toeslagen die aan u in rekening zijn gebracht als gevolg van schade die is veroorzaakt door uw gebruik van de Vakantieboeking of Ervaring in overeenstemming met de toepasselijke voorwaarden of het geldende beleid.

Raadpleeg de voorwaarden van onze partners voor meer informatie over vergoedingen voor de huur van Vakantiewoningen, de betaling van een borgsom, de kosten voor Ervaringen, de verwerking van betalingen, terugbetalingen en dergelijke (ga naar [**Viator**](https://www.viator.com/en-GB/support/termsAndConditions) voor Ervaringen;  bezoek [TheFork](https://www.thefork.nl/#geo_dmn_redirect=20210629AT100) voor Restaurants; ga naar [**Vakantiewoningen van de Bedrijven van Tripadvisor voor het huren van een vakantiewoning).**](https://rentals.tripadvisor.com/nl_NL/termsandconditions/traveler) Door een Vakantiewoning te huren, een reservering te plaatsen bij een Restaurant of een Ervaring te boeken via een onze partners dient u akkoord te gaan met de voorwaarden en het privacybeleid van de desbetreffende partner.

Indien u een geschil heeft met een Adverteerder uit de EU, dan zijn er hier online alternatieve methoden voor het beslechten van het geschil beschikbaar: [http://ec.europa.eu/consumers/odr/](https://ec.europa.eu/consumers/odr/main/index.cfm?event=main.home2.show&lng=EN).

**REISBESTEMMINGEN**

**Internationale reizen.** Wanneer u bij een derde leveranciers reserveringen voor internationale reizen boekt of met behulp van de Diensten internationale reizen plant, bent u verantwoordelijk om ervoor te zorgen dat u aan alle vereisten voldoet om toegang te krijgen tot het betreffende land en dat uw reisdocumenten, met inbegrip van paspoorten en visa, in orde zijn.

Raadpleeg voor informatie over paspoort- en visumvereisten de ambassade of het consulaat van het land dat u wilt bezoeken. Aangezien de vereisten op elk ogenblik kunnen veranderen, moet u actuele informatie raadplegen voor u boekt en vertrekt. De Bedrijven van Tripadvisor zijn niet aansprakelijk voor reizigers die niet op een vlucht of in een land worden toegelaten omdat zij niet de reisdocumenten bezitten die worden geëist door een luchtvaartmaatschappij, een overheid of een land, met inbegrip van landen die de reiziger alleen passeert op weg naar zijn of haar bestemming.

Het is ook uw verantwoordelijkheid uw arts te raadplegen over de huidige aanbevelingen voor inentingen voor u internationaal reist, en ervoor te zorgen dat u aan alle gezondheidsvoorwaarden voor de toegang voldoet en alle medische richtlijnen in verband met uw reis naleeft.

Hoewel de meeste reizen (ook naar internationale bestemmingen) zonder incidenten verlopen, kan aan het reizen naar bepaalde bestemmingen meer risico verbonden zijn dan aan andere bestemmingen. Tripadvisor raadt reizigers dringend aan om kennis te nemen van de verboden, waarschuwingen, aankondigingen en adviezen die door hun eigen overheid en het land van bestemming worden verstrekt alvorens reizen naar internationale bestemmingen te boeken. Zo wordt er door de Amerikaanse overheid informatie over de omstandigheden in diverse landen en de risico's van reizen naar bepaalde internationale bestemmingen verstrekt op [www.state.gov](http://www.state.gov/), [www.tsa.gov](https://www.tsa.gov/),[www.dot.gov](http://www.dot.gov/), [www.faa.gov](http://www.faa.gov/), [www.cdc.gov](http://www.cdc.gov/), [www.treas.gov/ofac](http://www.treas.gov/ofac) en [www.customs.gov](http://www.customs.gov/).

DOOR INFORMATIE OVER REIZEN NAAR BEPAALDE INTERNATIONALE BESTEMMINGEN TE VERSCHAFFEN, VERKLAREN OF GARANDEREN DE BEDRIJVEN VAN TRIPADVISOR NIET DAT REIZEN NAAR DERGELIJKE BESTEMMINGEN AAN TE RADEN OF ZONDER RISICO IS, EN TRIPADVISOR IS NIET AANSPRAKELIJK VOOR SCHADE OF VERLIES VOORTVLOEIEND UIT HET REIZEN NAAR DERGELIJKE BESTEMMINGEN.

**AFWIJZING VAN AANSPRAKELIJKHEID**

LEES DEZE SECTIE AANDACHTIG DOOR. DEZE SECTIE BEPERKT DE AANSPRAKELIJKHEID VAN DE BEDRIJVEN VAN TRIPADVISOR JEGENS U VOOR ZAKEN DIE KUNNEN VOORTVLOEIEN UIT UW GEBRUIK VAN DE DIENSTEN. ALS U DE VOORWAARDEN IN DEZE SECTIE OF ELDERS IN DEZE OVEREENKOMST NIET BEGRIJPT,

DE INFORMATIE, SOFTWARE, PRODUCTEN EN DIENSTEN GEPUBLICEERD OP OF ANDERSZINS AANGEBODEN VIA DE DIENSTEN (WAARONDER, MAAR NIET BEPERKT TOT, DE GEGEVENS DIE ZIJN AANGEMAAKT OF MOGELIJK GEMAAKT DOOR AI) KUNNEN ONNAUWKEURIGHEDEN OF FOUTEN BEVATTEN, WAARONDER FOUTEN MET BETREKKING TOT DE BESCHIKBAARHEID VAN RESERVERINGEN EN PRIJZEN. DE BEDRIJVEN VAN TRIPADVISOR GEVEN GEEN GARANTIES VOOR DE NAUWKEURIGHEID VAN, EN WIJZEN ELKE AANSPRAKELIJKHEID AF VOOR FOUTEN OF ANDERE ONNAUWKEURIGHEDEN IN VERBAND MET DE INFORMATIE OVER EN BESCHRIJVING VAN DE ACCOMMODATIE, ERVARINGEN, VLIEGREIS, CRUISE, RESTAURANT OF ELK ANDER REISPRODUCT DAT OP DE DIENSTEN WORDT GETOOND (WAARONDER, MAAR NIET BEPERKT TOT, DE PRIJZEN, BESCHIKBAARHEID, FOTO'S, LIJSTEN VAN ACCOMMODATIES, ERVARINGEN, VLIEGREIZEN, CRUISES, RESTAURANTS OF ANDERE REISPRODUCTENVOORZIENINGEN, ALGEMENE PRODUCTBESCHRIJVINGEN, BEOORDELINGEN EN WAARDERINGEN, ENZ.). BOVENDIEN BEHOUDEN DE BEDRIJVEN VAN TRIPADVISOR ZICH NADRUKKELIJK HET RECHT VOOR OM EVENTUELE FOUTEN IN INFORMATIE OVER BESCHIKBAARHEID EN PRIJZEN OP DE DIENSTEN EN/OF MET BETREKKING TOT MET EEN VERKEERDE PRIJS GEMAAKTE RESERVERINGEN TE CORRIGEREN.

TRIPADVISOR GEEFT GEEN ENKELE VERKLARING AF TEN AANZIEN VAN DE GESCHIKTHEID VAN DE DIENSTEN, DE INFORMATIE OP HAAR WEBSITES, OF ENIG GEDEELTE DAARVAN, VOOR OM HET EVEN WELK DOEL, EN DE OPNAME VAN AANBIEDINGEN VAN PRODUCTEN OF DIENSTEN OP HAAR WEBSITES OF DE DIENSTEN VORMT GEEN ONDERSCHRIJVING OF AANBEVELING VAN DEZE AANBIEDINGEN VAN PRODUCTEN OF DIENSTEN DOOR TRIPADVISOR, MET UITZONDERING VAN AWARDS DIE ZIJN UITGEREIKT OP BASIS VAN BEOORDELINGEN VAN GEBRUIKERS. DERGELIJKE INFORMATIE, SOFTWARE, PRODUCTEN EN DIENSTEN DIE DOOR OF VIA DE DIENSTEN BESCHIKBAAR WORDEN GESTELD, WORDEN VERMELD 'ZOALS ZE ZIJN', ZONDER ENIGE GARANTIE. TRIPADVISOR WIJST ALLE GARANTIES, BEPALINGEN EN VOORWAARDEN AF WAARUIT ZOU BLIJKEN DAT DE DIENSTEN, HAAR SERVERS OF GEGEVENS (WAARONDER E-MAILS) DIE DOOR TRIPADVISOR WORDEN VERZONDEN VRIJ ZIJN VAN VIRUSSEN OF ANDERE SCHADELIJKE ONDERDELEN. TRIPADVISOR DOET HIERBIJ IN DE RUIMSTE MATE DIE DE TOEPASSELIJKE WETGEVING TOELAAT AFSTAND VAN ALLE WAARBORGEN EN CONDITIES MET BETREKKING TOT DEZE INFORMATIE, SOFTWARE, PRODUCTEN EN DE DIENSTEN, INCLUSIEF ALLE IMPLICIETE GARANTIES EN BEPALINGEN OF VOORWAARDEN VAN OM HET EVEN WELKE AARD MET BETREKKING TOT DE VERKOOPBAARHEID, GESCHIKTHEID VOOR EEN BEPAALD DOEL, AANSPRAAK, STIL-BEZIT EN NIET-SCHENDING.

DE BEDRIJVEN VAN TRIPADVISOR WIJZEN OOK UITDRUKKELIJK ALLE GARANTIES, VERKLARINGEN OF ANDERE VOORWAARDEN AF MET BETREKKING TOT DE NAUWKEURIGHEID OF HET EIGENDOMSKARAKTER VAN DE INHOUD DIE DOOR EN VIA DE DIENSTEN BESCHIKBAAR WORDT GEMAAKT.

DE DERDE LEVERANCIERS DIE ACCOMMODATIES, VLIEGREIZEN, VERHUUR, ERVARINGEN, RESTAURANTS OF CRUISES, INFORMATIE, REIZEN OF ANDERE DIENSTEN OP OF VIA DE DIENSTEN AANBIEDEN, ZIJN ONAFHANKELIJKE AANNEMERS EN GEEN VERTEGENWOORDIGERS OF WERKNEMERS VAN DE BEDRIJVEN VAN TRIPADVISOR. DE BEDRIJVEN VAN TRIPADVISOR ZIJN NIET AANSPRAKELIJK VOOR DE HANDELINGEN, FOUTEN, WEGLATINGEN, VOORSTELLINGEN, GARANTIES, SCHENDINGEN OF NALATIGHEID VAN DERGELIJKE LEVERANCIERS OF VOOR PERSOONLIJK LETSEL, OVERLIJDEN, SCHADE AAN EIGENDOMMEN OF ANDERE SCHADE OF ONKOSTEN DIE HIERUIT VOORTVLOEIEN. TRIPADVISOR IS NIET AANSPRAKELIJK EN ZAL GEEN RESTITUTIE BETALEN BIJ VERTRAGING, ANNULERING, TE VOL BOEKEN, STAKINGEN, OVERMACHT OF ANDERE OORZAKEN BUITEN HAAR RECHTSTREEKSE INVLOED, EN ZIJ IS NIET VERANTWOORDELIJK VOOR EXTRA ONKOSTEN, WEGLATINGEN, VERTRAGINGEN, ROUTEWIJZIGINGEN OF HANDELINGEN VAN EEN OVERHEID OF AUTORITEIT.

VOLGENS DE BOVENSTAANDE VOORWAARDEN GEBRUIKT U DE DIENSTEN OP EIGEN RISICO. DE BEDRIJVEN VAN TRIPADVISOR (OF HUN DIRECTEUREN, BESTUURDERS EN/OF MEDEWERKERS) ZIJN IN GEEN GEVAL AANSPRAKELIJK VOOR DIRECTE OF INDIRECTE SCHADE, STRAFSCHADE, INCIDENTELE, BIJZONDERE OF GEVOLGSCHADE OF VERLIEZEN, OF ELK VERLIES VAN INKOMSTEN, BATEN, GOODWILL, GEGEVENS, CONTRACTEN, GEBRUIK VAN GELD, OF VERLIEZEN OF SCHADE DIE VOORTVLOEIEN UIT OF OP ENIGE WIJZE VERBAND HOUDEN MET DE VERSTORING VAN BEDRIJFSACTIVITEITEN VAN OM HET EVEN WELKE AARD DIE VOORTVLOEIT UIT OF OP ENIGE WIJZE VERBAND HOUDT MET UW BEZOEK, WEERGAVE OF GEBRUIK VAN DE DIENSTEN OF MET DE VERTRAGING OF HET ONVERMOGEN OM DE DIENSTEN TE BEZOEKEN, WEER TE GEVEN OF TE GEBRUIKEN (MET INBEGRIP VAN MAAR NIET BEPERKT TOT UW VERTROUWEN OP BEOORDELINGEN EN MENINGEN DIE OP OF VIA DE DIENSTEN WORDEN WEERGEGEVEN; VIRUSSEN, BUGS, TROJAANSE PAARDEN, INFORMATIE, SOFTWARE, GELINKTE SITES, PRODUCTEN EN DIENSTEN DIE VIA DE DIENSTEN WORDEN VERKREGEN (MET INBEGRIP VAN MAAR NIET BEPERKT TOT EEN SYNCHRONISATIEPRODUCT VAN EEN VAN DE BEDRIJVEN VAN TRIPADVISOR); PERSOONLIJK LETSEL OF SCHADE AAN EIGENDOMMEN, VAN WELKE AARD DAN OOK, VOORTVLOEIENDE UIT UW GEBRUIK VAN DE SERVERS VAN DE DIENSTEN EN/OF DE DAARIN OPGESLAGEN PERSOONSGEGEVENS EN/OF FINANCIËLE INFORMATIE; FOUTEN OF OMISSIES IN INHOUD, OF VERLIES OF SCHADE VAN WELKE AARD DAN OOK ALS GEVOLG VAN HET GEBRUIK VAN INHOUD; OF DIE OP ANDERE WIJZE VOORTVLOEIEN UIT HET BEZOEK, DE WEERGAVE OF HET GEBRUIK VAN DE DIENSTEN), HETZIJ OP BASIS VAN EEN THEORIE VAN NALATIGHEID, CONTRACTRECHT, ONRECHTMATIGE DAAD, STRIKTE AANSPRAKELIJKHEID OF ANDERSZINS, ZELFS INDIEN TRIPADVISOR OF DE AAN HAAR GELIEERDE BEDRIJVEN ZIJN INGELICHT OVER DE MOGELIJKHEID VAN DERGELIJKE SCHADE.

Indien de Bedrijven van Tripadvisor aansprakelijk worden gesteld voor verlies of schade die voortvloeit uit of op enige wijze verband houdt met uw gebruik van de Diensten, dan zal de aansprakelijkheid van de Bedrijven van Tripadvisor in geen geval groter zijn dan het totaal van (a) de aan de Bedrijven van Tripadvisor betaalde vergoedingen voor de transactie(s) op of via de Diensten die aanleiding hebben gegeven tot uw eis, of (b) honderd dollar (USD 100,00).

De beperking van de aansprakelijkheid weerspiegelt de verdeling van het risico tussen de partijen. De beperkingen die in dit artikel zijn vermeld, overleven en zijn zelfs van toepassing indien een beperkte oplossing zoals opgegeven in deze voorwaarden haar doel niet blijkt te hebben bereikt. De beperkingen van de aansprakelijkheid die in deze voorwaarden worden vermeld, komen ten goede aan de Bedrijven van Tripadvisor.

DEZE ALGEMENE VOORWAARDEN EN DE BOVENSTAANDE AFWIJZING VAN AANSPRAKELIJKHEID HEBBEN GEEN INVLOED OP VERPLICHTE WETTELIJKE RECHTEN DIE VOLGENS DE TOEPASSELIJK WET NIET MOGEN WORDEN UITGESLOTEN, ZOALS UIT HOOFDE VAN NATIONALE WETGEVING INZAKE CONSUMENTENBESCHERMING DIE IN BEPAALDE LANDEN VAN TOEPASSING IS. IN HET VK EN DE EU OMVAT DIT AANSPRAKELIJKHEID VOOR OVERLIJDEN OF PERSOONLIJK LETSEL VEROORZAAKT DOOR ONZE NALATIGHEID OF DE NALATIGHEID VAN ONZE WERKNEMERS, AGENTEN OF ONDERAANNEMERS EN VOOR FRAUDE OF FRAUDULEUZE VERKEERDE VOORSTELLING VAN ZAKEN.

Als u een consument bent in het Verenigd Koninkrijk of de EU, sluit deze Overeenkomst geen aansprakelijkheid uit voor verliezen en schade veroorzaakt door een schending van een verplichting om met redelijke zorg en bekwaamheid te handelen of door een schending van deze Overeenkomst, op voorwaarde dat de daaruit voortvloeiende schade voorzienbaar was toen de verplichtingen ontstonden.     

INDIEN DE WETGEVING VAN HET LAND WAARIN U WOONT EEN BEPAALDE BEPERKING OF UITSLUITING VAN DE AANSPRAKELIJKHEID ZOALS BEDOELD IN DIT ARTIKEL NIET TOESTAAT, DAN IS DEZE BEPERKING NIET VAN TOEPASSING. DE AFWIJZING VAN AANSPRAKELIJKHEID IS VAN TOEPASSING IN DE MATE DIE MAXIMAAL UIT HOOFDE VAN UW LOKALE WETGEVING IS TOEGESTAAN.

**SCHADELOOSSTELLING**

Deze sectie 'Vrijwaring' is niet van toepassing op consumenten die in de EU of het VK wonen.

U gaat ermee akkoord om de Bedrijven van Tripadvisor en hun directeuren, bestuurders, werknemers en vertegenwoordigers te verdedigen tegen en te vrijwaren voor alle claims, processen, eisen, terugwinningen, verliezen, schadevergoedingen, boetes, straffen of andere uitgaven van elke aard, met inbegrip van maar niet beperkt tot redelijke juridische en boekhoudkosten die door derden in rekening worden gebracht wegens:

* (i) uw schending van deze Overeenkomst of van de documenten waarnaar hierin wordt verwezen;
* (ii) uw overtreding van een wet of van de rechten van een derde partij, of:
* (iii) uw gebruik van de Diensten, waaronder de websites van de Bedrijven van Tripadvisor.

**LINKS NAAR WEBSITES VAN DERDEN**

De Diensten kunnen hyperlinks bevatten naar websites die door andere partijen dan de Bedrijven van Tripadvisor worden geëxploiteerd. Dergelijke hyperlinks zijn alleen bedoeld als verwijzingen. De Bedrijven van Tripadvisor hebben geen invloed op deze websites en zijn niet verantwoordelijk voor hun inhoud of voor de privacyregels en andere praktijken van deze website. Bovendien dient u voorzorgsmaatregelen te nemen om te garanderen dat links die u volgt of software die u downloadt (hetzij vanaf deze website, hetzij vanaf een andere website) vrij zijn van onder meer virussen, wormen, Trojaanse paarden, defecten en andere schadelijke zaken. Het feit dat de Bedrijven van Tripadvisor hyperlinks naar dergelijke websites opnemen, betekent niet dat zij het materiaal op de websites of apps van derden onderschrijven of dat zij enige relatie hebben met de uitbaters van deze websites.

In sommige gevallen kan een site of app van een derde partij u vragen uw profiel in uw Tripadvisor-account te koppelen aan een profiel op een site van een derde partij. U bent verantwoordelijk voor de beslissing of u dit wilt doen. U bent het absoluut niet verplicht en u kunt op elk gewenst moment terugkomen op uw beslissing om toe te laten dat deze informatie wordt gekoppeld (aan de website of app van de derde partij). Als u ervoor kiest om uw Tripadvisor-account te koppelen aan een site of app van een derde partij, dan zal de site of app van deze derde partij toegang hebben tot de informatie die u in uw Tripadvisor-account hebt opgeslagen, inclusief tot informatie over andere gebruikers waarmee u informatie deelt. U dient de voorwaarden en het privacybeleid van de sites en apps van derden die u bezoekt te lezen, aangezien daarin staat vermeld hoe zij uw informatie mogen gebruiken. Deze kunnen afwijken van die van de Diensten, waaronder onze websites. Wij raden u aan om deze sites en apps van derden eerst goed te beoordelen. Het gebruik daarvan is op uw eigen risico.

Software als onderdeel van de Diensten; Bijkomende Mobiele licenties

De software van de Diensten is onderworpen aan de exportcontroles van de Verenigde Staten. Software van de Diensten mag niet worden gedownload of anderszins worden geëxporteerd of wedergeëxporteerd (a) naar (of naar een onderdaan of inwoner van) Cuba, Irak, Soedan, Noord-Korea, Iran, Syrië of enig ander land waaraan de VS goederen onder embargo heeft geplaatst, of (b) naar personen in de VS. Lijst van Specially Designated Nationals van het Amerikaanse ministerie van Financiën De tafel van het ministerie van Handel om bestellingen te weigeren. Door de Diensten te gebruiken, verklaart en waarborgt u dat u zich niet bevindt in of onder het bewind staat van of een burger of inwoner bent van een van deze landen, en dat u niet voorkomt op een van deze lijsten.

Zoals hierboven reeds is genoemd, bevatten de Diensten software. Deze software wordt soms aangeduide met 'apps'.  Alle software die op via de Diensten beschikbaar wordt gesteld om te worden gedownload ('Software) is auteursrechtelijk beschermd materiaal van Tripadvisor of andere geïdentificeerde derde partijen. Op uw gebruik van dergelijke Software gelden de voorwaarden van de licentieovereenkomst voor eindgebruikers (indien van toepassing) die geleverd wordt bij, of is opgenomen in, de Software. U mag Software die door een licentieovereenkomst wordt vergezeld of die een licentieovereenkomst bevat, pas installeren of gebruiken nadat u akkoord bent gegaan met de voorwaarden van de licentieovereenkomst. Voor Software die op de Diensten beschikbaar wordt gemaakt om te worden gedownload en die niet door een licentieovereenkomst wordt vergezeld, verlenen wij aan u, de gebruiker, hierbij een beperkte, persoonlijke, niet-overdraagbare licentie om de Software te gebruiken voor het bekijken en anderszins gebruiken van de Diensten volgens de algemene voorwaarden van deze Overeenkomst (en de hierin genoemde beleidsregels), en voor geen enkel ander doel.

Let op: alle software, waaronder, maar niet beperkt tot alle HTML-, XML-, Java-codes en Active X-besturingselementen die de Diensten bevatten, is eigendom van of wordt gelicentieerd door Tripadvisor en wordt beschermd door auteursrechten en bepalingen uit internationale verdragen. Het reproduceren of opnieuw verspreiden van de Software is uitdrukkelijk verboden en kan leiden tot civiele en strafrechtelijke vervolging. Overtreders zullen met alle rechtsmiddelen worden vervolgd.

Delen van de mobiele software van Tripadvisor kunnen auteursrechtelijk beschermd materiaal gebruiken. Tripadvisor erkent dit gebruik. Bovendien gelden voor het gebruik van bepaalde mobiele toepassingen van Tripadvisor specifieke voorwaarden. Bezoek de pagina [Mobiele licenties](https://tripadvisor.mediaroom.com/uk-mobile-licenses) voor specifieke kennisgevingen voor de mobiele toepassingen van Tripadvisor.

ZONDER BEPERKING VAN HET VOORGAANDE GELDT DAT HET KOPIËREN OF REPRODUCEREN VAN DE SOFTWARE NAAR EEN ANDERE SERVER OF LOCATIE VOOR VERDERE REPRODUCTIE OF VERSPREIDING UITDRUKKELIJK VERBODEN IS. EVENTUELE GARANTIES VAN DE SOFTWARE ZIJN AFHANKELIJK VAN DE VOORWAARDEN VAN DE LICENTIEOVEREENKOMST OF DEZE OVEREENKOMST (ZOALS VAN TOEPASSING).

Vermeldingen auteursrecht en handelsmerk

Tripadvisor, het uil-logo, de waarderingsbollen en alle andere namen van diensten of producten of slagzinnen die op de Diensten worden getoond, zijn geregistreerde handelsmerken en/of handelsmerken op grond van het gewoonterecht van Tripadvisor LLC en/of haar leveranciers of licentiegevers, en mogen niet worden gekopieerd, nagebootst of gebruikt, geheel of gedeeltelijk, zonder de voorafgaande schriftelijke toestemming van Tripadvisor of de relevante eigenaar van het handelsmerk. Bovendien is de vormgeving van de Diensten, met inbegrip van onze websites en alle paginaheaders, grafische illustraties, knoppictogrammen en gerelateerde scripts, het dienstmerk, handelsmerk en/of de handelsvorm van Tripadvisor en mag deze niet worden gekopieerd, nagebootst of gebruikt, geheel of gedeeltelijk, zonder de voorafgaande schriftelijke toestemming van Tripadvisor. Alle andere handelsmerken, geregistreerde handelsmerken, productnamen en bedrijfsnamen of logo's die op de Diensten worden vermeld, zijn eigendom van de respectieve eigenaren. De verwijzing naar producten, diensten, procedures of andere informatie met handelsnaam, handelsmerk, fabrikant, leveranciers of anderszins, betekent of impliceert niet dat Tripadvisor ze goedkeurt, sponsort of aanbeveelt, behalve in hoeverre anders in deze Overeenkomst wordt genoemd.

Alle rechten voorbehouden. Tripadvisor is niet verantwoordelijk voor inhoud op websites die door andere partijen dan Tripadvisor worden uitgebaat.

**Beleid voor de melding en verwijdering van illegale inhoud**

Tripadvisor werkt op basis van 'melding en verwijdering'. Als u klachten of bezwaren heeft over de Inhoud, met inbegrip van gebruikersberichten die op de Diensten worden geplaatst, of als u meent dat materiaal of inhoud die op de Diensten zijn geplaatst inbreuk maakt op een auteursrecht of handelsmerk in uw bezit, verzoeken wij u om ons onmiddellijk te waarschuwen door onze procedure ‘melding en verwijdering’ te volgen. [**Klik hier om het beleid en de procedure inzake auteursrecht te bekijken**](https://www.tripadvisor.be/StaticVelocityXmlPage-a_xml.noticetakedown__2E__xml?fromPos=nl-NL&toPos=nl-BE&changeSrc=footer&mismatchTestBucket=Test). Als u deze procedure volgt, zal Tripadvisor op geldige en goed onderbouwde klachten reageren door alle redelijke maatregelen te nemen om illegale inhoud binnen een redelijke termijn te verwijderen.

**WIJZIGINGEN AAN DEZE OVEREENKOMST; DE DIENSTEN; UPDATES; BEËINDIGING EN INTREKKING**

**Deze Overeenkomst:**

Tripadvisor kan naar eigen goeddunken de algemene voorwaarden van deze Overeenkomst of elk gedeelte ervan van tijd tot tijd wijzigen, aanvullen of verwijderen wanneer zij meent dat dit om wettelijke, algemeen reglementaire en technische redenen nodig is, of omdat de geleverde Diensten of de aard of indeling van de Diensten zijn gewijzigd. Daarna aanvaardt u gewijzigde algemene voorwaarden van deze Overeenkomst uitdrukkelijk als bindend.

**Wijzigingen:**

De Bedrijven van Tripadvisor kunnen te allen tijde elk aspect van de Diensten, met inbegrip van de beschikbaarheid van een functie, database of Inhoud van de Diensten wijzigen, opschorten of stopzetten.

(a)           om te zorgen voor naleving van de toepasselijke wetgeving en/of om wijzigingen in de relevante wet- en regelgeving weer te geven, zoals verplichte consumentenwetgeving;

(b)           om tijdelijk onderhoud uit te voeren, bugs te verhelpen, technische aanpassingen uit te voeren voor operationele doeleinden of verbeteringen aan te brengen, zoals het aanpassen van de Diensten aan een nieuwe technische omgeving, het overdragen van de Diensten naar een nieuw hostingplatform of het garanderen van de compatibiliteit van de Dienst met de apparaten en software (zoals van tijd tot tijd bijgewerkt);

(c)           om de Diensten te upgraden of aan te passen, waaronder het vrijgeven van nieuwe versies van de websites op bepaalde apparaten of het anderszins wijzigen of wijzigen van bestaande functies en functionaliteit;

(d)           om de structuur, het ontwerp of de lay-out van de Diensten te wijzigen, waaronder het wijzigen van de naam van de Diensten of het herdefiniëren van de Diensten, of het wijzigen, verbeteren en/of uitbreiden van de beschikbare kenmerken en functionaliteiten; en

(e)           om veiligheidsredenen.

Voor gebruikers in het VK en de EU: Als we wijzigingen aanbrengen aan de Diensten waarvan we redelijkerwijs denken dat deze een wezenlijk negatief effect zullen hebben op uw toegang tot of gebruik van de Diensten, geven we u ten minste 30 dagen van tevoren kennis van de wijzigingen. Als u uw contract met ons niet ontbindt voordat deze wijzigingen hebben plaatsgevonden, beschouwen we dat als uw aanvaarding van de wijzigingen.

**Updates:**

We kunnen updates doorvoeren om ervoor te zorgen dat de Diensten in overeenstemming blijven met de toepasselijke consumentenwetgeving. Als automatische updates worden ingeschakeld in uw instellingen, werken we de Diensten automatisch bij. U kunt uw instellingen op elk moment wijzigen als u niet langer automatisch updates wilt ontvangen.

Om de beste ervaring te krijgen en ervoor te zorgen dat de Diensten correct werken, raden we aan dat u alle updates van de Diensten accepteert waarover we u informeren zodra deze beschikbaar zijn. Hierdoor moet u mogelijk ook uw apparaatbesturingssysteem bijwerken. Wanneer nieuwe besturingssystemen en apparaten worden uitgebracht, zullen we na verloop van tijd mogelijk geen oudere versies meer ondersteunen.

Als u ervoor kiest om geen updates te downloaden of te installeren, zijn de Diensten mogelijk niet meer beschikbaar, worden ze mogelijk niet meer ondersteund of hebben ze geen eerdere functionaliteit.

Wij zijn niet verantwoordelijk voor het feit dat u een update of de meest recente gepubliceerde versie van de Diensten niet heeft gedownload of geïnstalleerd om te profiteren van nieuwe of verbeterde functies en/of functionaliteit en/of om te voldoen aan compatibiliteitsvereisten wanneer wij u hebben geïnformeerd over de update, hebben uitgelegd wat de gevolgen zijn als u de update niet heeft geïnstalleerd en installatie-instructies hebben verstrekt.

Wijzigingen aan de Service die niet in overeenstemming zijn met deze 'Updates'-bepaling zijn onderhevig aan de bovenstaande sectie 'Wijzigingen'.

**Beëindiging**

De Bedrijven van Tripadvisor kunnen om technische of veiligheidsredenen ook beperkingen opleggen of anderszins uw toegang tot alle of een deel van de Diensten beperken zonder kennisgeving of aansprakelijkheid om ongeoorloofde toegang, het verlies of de vernietiging van gegevens te voorkomen of wanneer Tripadvisor en/of de aan haar gelieerde bedrijven naar eigen goeddunken menen dat u een bepaling van deze Overeenkomst of van een wet of reglement overtreedt en wanneer Tripadvisor en/of de aan haar gelieerde bedrijven besluiten om een deel van de Diensten stop te zetten.

Tripadvisor kan deze Overeenkomst met u te allen tijde zonder voorafgaande kennisgeving beëindiging wanneer zij oprecht denkt dat u deze Overeenkomst heeft geschonden of anderszins van mening is dat beëindiging redelijkerwijs noodzakelijk is om de rechten van de Bedrijven van Tripadvisor en/of andere gebruikers van de Diensten te beschermen. Dit houdt in dat wij kunnen stoppen met het leveren van Diensten aan u.

**RECHTSGEBIED EN TOEPASSELIJK RECHT**

Deze website is het eigendom van en wordt beheerd door Tripadvisor LLC, een naamloze vennootschap naar Amerikaans recht. Deze Overeenkomst en alle geschillen of claims (waaronder niet-contractuele geschillen of claims) die daaruit voortvloeien of daarmee verband houden of voortvloeien of verband houden met het voorwerp of de bepalingen van deze Overeenkomst zullen worden afgehandeld door en geïnterpreteerd in overeenstemming met de wetgeving van de Commonwealth van Massachusetts, Verenigde Staten. U aanvaardt hierbij de exclusieve jurisdictie en de gerechtshoven in Massachusetts, VS, en de billijkheid en de geschiktheid van procedures in deze rechtbanken voor alle contractuele en niet-contractuele geschillen die uit of in verband met het gebruik van de Diensten door u of een derde partij ontstaan. U aanvaardt dat alle eisen die u tegen Tripadvisor LLC zou hebben als gevolg van of in verband met de Diensten, moeten worden gehoord en beoordeeld door een bevoegde rechtbank van de Commonwealth of Massachusetts. Het gebruik van de Diensten is niet toegestaan in een rechtsgebied dat geen volledige geldigheid geeft aan al deze algemene voorwaarden, met inbegrip van, maar niet beperkt tot, deze paragraaf. Niets in deze bepaling beperkt het recht van Tripadvisor LLC om een procedure tegen u aanhangig te maken in een andere rechtbank of rechtbanken binnen het bevoegde rechtsgebied. Het voornoemde is niet van toepassing voor zover geldende wetgeving in het land waarin u woont de toepassing van een ander recht en/of rechtsgebied vereist – met name wanneer u als consument gebruik maakt van de Diensten – en dit niet contractueel kan worden uitgesloten en dit niet onder het Verdrag der Verenigde Naties inzake internationale koopovereenkomsten betreffende roerende zaken valt, indien anders van toepassing.  Indien u de Diensten gebruikt als consument en niet als bedrijf of Bedrijfsvertegenwoordiger, dan kunt u het recht hebben om vorderingen op Tripadvisor aanhangig te maken bij de rechtbanken in het land waar u woont. Deze bepaling is van toepassing voor zover dit in uw land van herkomst of land van verblijf is toegestaan.

**VALUTA-OMREKENMACHINE**

De wisselkoersen zijn gebaseerd op verschillende openbaar beschikbare bronnen en dienen uitsluitend als richtlijn. De koersen zijn niet als accuraat geverifieerd en de daadwerkelijke koersen kunnen variëren. De wisselkoersen worden mogelijk niet elke dag bijgewerkt. De verstrekte informatie wordt als correct beschouwd, maar de Bedrijven van Tripadvisor geven geen garanties of beloftes af ten aanzien van de juistheid van deze informatie. Als deze informatie voor financiële doeleinden wordt gebruikt, adviseren we u om een ter zake deskundige te raadplegen om de nauwkeurigheid van de wisselkoersen te verifiëren. Wij geven geen toestemming voor het gebruik van deze informatie voor een ander dan persoonlijk doel. Het wederverkopen, verder verspreiden en gebruiken van deze informatie voor commerciële doeleinden is uitdrukkelijk verboden.

Algemene bepalingen

Wij behouden ons het recht voor om een gebruikersnaam, accountnaam, nickname, alias of andere identificaties van gebruikers om welke reden dan ook in te trekken zonder dat wij hiervoor jegens u aansprakelijk zijn.

U aanvaardt dat als gevolg van deze Overeenkomst of het gebruik van de Diensten geen joint venture, vennootschap of arbeidsbetrekking bestaat tussen u en Tripadvisor en/of de aan haar gelieerde bedrijven.

Onze uitvoering van deze Overeenkomst is onderworpen aan de bestaande wetten en juridische processen en niets in deze Overeenkomst doet afbreuk aan ons recht om ons te houden aan verzoeken of eisen van gerechtelijke instanties of de overheid of aan andere wettelijke eisen met betrekking tot uw gebruik van de Diensten of informatie die door ons wordt verzameld met betrekking tot dergelijk gebruik. Voor zover de toepasselijke wet het toelaat, aanvaardt u dat u elke claim of handelingsgrond die voortvloeit uit of verband houdt met uw gebruik van de Diensten zult inbrengen binnen twee (2) jaar vanaf de datum waarop een dergelijke claim of grond is ontstaan, en dat u in het andere geval onherroepelijk afstand doet van de claim of handelingsgrond.

Indien zou worden vastgesteld dat een deel van deze overeenkomst ongeldig of niet-afdwingbaar is volgens de toepasselijke wet, met inbegrip van ,maar niet beperkt tot, de bovenvermelde afwijzingen en beperkingen van aansprakelijkheid, zal ervan worden uitgegaan dat de ongeldige of niet-afdwingbare bepaling wordt vervangen door een geldige, afdwingbare bepaling die de geest van de oorspronkelijke bepaling en de rest van de Overeenkomst zo dicht mogelijk benadert, en zullen de andere bepalingen van deze Overeenkomst geldig blijven.

Deze Overeenkomst (samen met andere algemene voorwaarden waarnaar ze verwijst) vormt de volledige overeenkomst tussen u en Tripadvisor met betrekking tot de Diensten en vervangt alle eerdere of gelijktijdige berichten en voorstellen, hetzij elektronisch, mondeling of schriftelijk, tussen u en Tripadvisor met betrekking tot de Diensten. Een gedrukte versie van deze Overeenkomst en alle mededelingen die in elektronische vorm worden verstrekt, zijn toelaatbaar in juridische of bestuursrechtelijke procedures die gebaseerd zijn op of verband houden met deze Overeenkomst, in dezelfde mate en volgens dezelfde voorwaarden als andere bedrijfsdocumenten en bestanden die oorspronkelijk in schriftelijke vorm zijn aangemaakt en bijgehouden.

De volgende onderdelen blijven na beëindiging van deze Overeenkomst van kracht:

* #### Aanvullende producten
    
* #### Verboden activiteiten
    
* #### Beoordelingen, Reacties en Gebruik van Andere Interactieve Zones; Licentieverlening
    
    * #### Licentierechten van Tripadvisor beperken
        
* Reisbestemmingen
    * Internationale reizen
* **AFWIJZING VAN AANSPRAKELIJKHEID**
* **SCHADELOOSSTELLING**
* Software als onderdeel van de Diensten; Bijkomende Mobiele licenties
* #### Vermeldingen auteursrecht en handelsmerk
    
    * #### Beleid voor de melding en verwijdering van illegale inhoud
        
* #### Aanpassingen aan de diensten; Beëindiging
    
* #### Rechtsgebied en toepasselijk recht
    
* #### Algemene bepalingen
    
* #### Hulp bij Diensten
    

De voorwaarden van deze Overeenkomst zijn beschikbaar in de taal van de websites en/of apps van Tripadvisor die toegang geven tot de Diensten.

Het is mogelijk dat de websites en/of apps die toegang geven tot de diensten niet altijd periodiek of regelmatig worden geüpdatet en derhalve niet uit hoofde van enige toepasselijke wetgeving als redactioneel product hoeven te worden geregistreerd.

Fictieve namen van bedrijven, producten, personen, figuren en/of gegevens die in of via de Diensten worden genoemd, zijn niet bedoeld als verwijzingen naar bestaande personen, bedrijven, producten of gebeurtenissen.

Niets in deze Overeenkomst kent rechten of voordelen aan derden toe, behalve aan Tripadvisor gelieerde ondernemingen die uitdrukkelijk als derden-begunstigden van deze Overeenkomst worden beschouwd.

Het is u niet toegestaan om uw rechten of plichten uit hoofde van deze Overeenkomst aan iemand anders over te dragen zonder onze toestemming.

Rechten die hierin niet nadrukkelijk zijn verleend, worden voorbehouden.

**HULP BIJ DIENSTEN**

Ga voor antwoorden op uw vragen of manieren om contact met ons op te nemen naar ons  [Helpcentrum](https://www.tripadvisorsupport.com/nl-NL/hc/traveler) of stuur ons een e-mail via  [help@tripadvisor.com](mailto:help@tripadvisor.com) . U kunt ook schrijven naar:

Tripadvisor LLC

400 1st Avenue

Needham, MA 02494, Verenigde Staten

Let op: Tripadvisor LLC accepteert geen juridische correspondentie of service van juridische processen op een andere manier dan door middel van een papieren versie van het bericht dat direct hierboven op het adres wordt geplaatst, tenzij het hiertoe door de toepasselijke wetgeving wordt verplicht.  Ter voorkoming van twijfel en zonder uitzonderingen: wij aanvaarden derhalve geen kennisgevingen of dagvaardingen die worden afgegeven bij een van onze partners of dochterondernemingen.

**DIGITAL SERVICES ACT-contactpersoon VOOR GEBRUIKERS**

U kunt rechtstreeks contact met ons opnemen via de onderstaande contactgegevens:

E-mailadres:  [help@tripadvisor.com](mailto:help@tripadvisor.com) 

**DIGITAL SERVICES ACT CONTACT EN WETTELIJKE VERTEGENWOORDIGER VOOR AUTORITEITEN**

Als u een nationale autoriteit van een lidstaat, de Europese Commissie of het Europees Comité voor digitale diensten bent, kunt u rechtstreeks contact met ons opnemen via de onderstaande contactgegevens:

E-mailadres:  [poc-dsa@tripadvisor.com](mailto:poc-dsa@tripadvisor.com) 

Vertrouwde flaggers en beroepsorganisaties kunnen ook contact met ons opnemen via dit e-mailadres.

Overheidsinstanties kunnen ook contact opnemen met Tripadvisor Ireland Limited, onze wettelijke vertegenwoordiger in de zin van de Digital Services Act, aan de hand van de onderstaande gegevens:

Adres: 70 Sir John Rogerson's Quay, Dublin 2, Ierland

E-mailadres:  [dsa.notifications@tripadvisor.com](mailto:dsa.notifications@tripadvisor.com)

Telefoonnummer:

Neem met deze gegevens geen contact met ons op via onze wettelijke vertegenwoordiger, tenzij u namens een van de bovengenoemde overheidsinstanties contact met ons opneemt. Tripadvisor zal niet op uw verzoek reageren. Als u vragen heeft over ons platform of als u een klacht wilt indienen, neem dan contact met ons op via de contactgegevens in de bovenstaande secties '**HULP BIJ DIENSTEN**' of '**DIGITAL SERVICES ACT-contactpersoon VOOR GEBRUIKERS**'.

©2024 Tripadvisor LLC. Alle rechten voorbehouden.

Laatst bijgewerkt 16 februari 2024

* [](#print "print")
* [](https://twitter.com/share?url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34420%26item%3D32414 "Twitter Share")
* [](https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34420%26item%3D32414 "Facebook Share")
* [](https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Ftripadvisor.mediaroom.com%2Findex.php%3Fs%3D34420%26item%3D32414 "Linkedin Share")
* [](#email "email")