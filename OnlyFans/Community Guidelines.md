### Looking for help?

Explore our Help Center and Community Guidelines below.

### For all Users

#### Opening an OnlyFans Account

* [Who can use OnlyFans?](https://onlyfans.com/help/179/180/181)
* [Does OnlyFans have an App?](https://onlyfans.com/help/179/180/182)

#### Contacting OnlyFans

* [Customer Support Queries](https://onlyfans.com/help/179/183/184)
* [Media/Press Queries](https://onlyfans.com/help/179/183/185)
* [Law Enforcement Queries](https://onlyfans.com/help/179/183/186)
* [Brand, or Partnership Queries](https://onlyfans.com/help/179/183/187)
* [Any other Queries](https://onlyfans.com/help/179/183/188)

#### Account Access and Security

* [I've forgotten my password, how do I log in to my account?](https://onlyfans.com/help/179/189/190)
* [I don’t have access to the phone number linked to my account, what should I do?](https://onlyfans.com/help/179/189/191)
* [How do I set up Two-Step Authentication?](https://onlyfans.com/help/179/189/193)
* [Why is Two-Step Authentication not working?](https://onlyfans.com/help/179/189/194)
* [What do I do if I think someone else has accessed my account?](https://onlyfans.com/help/179/189/195)

### For Creators

#### Becoming a Creator

* [How do I open a Creator account?](https://onlyfans.com/help/196/197/198)
* [How many Creator accounts can I have on OnlyFans?](https://onlyfans.com/help/196/197/199)
* [Can I become an OnlyFans Creator where I live?](https://onlyfans.com/help/196/197/201)
* [Why was my Creator account application rejected?](https://onlyfans.com/help/196/197/202)
* [How do I delete my OnlyFans Creator account?](https://onlyfans.com/help/196/197/203)
* [Why can’t I open an OnlyFans Creator account?](https://onlyfans.com/help/196/197/204)
* [Payout percentages](https://onlyfans.com/help/196/197/287)
* [Can I hide my profile?](https://onlyfans.com/help/196/197/288)

#### Sharing Content

* [What content can I post on OnlyFans? A Creator’s Guide to Content Moderation.](https://onlyfans.com/help/196/205/206)
* [Why was my content deactivated?](https://onlyfans.com/help/196/205/207)
* [Can I post explicit/nude content on OnlyFans?](https://onlyfans.com/help/196/205/208)
* [Can I post role play content on OnlyFans?](https://onlyfans.com/help/196/205/209)
* [Can I post content featuring someone else on OnlyFans?](https://onlyfans.com/help/196/205/210)
* [Can I post AI content on OnlyFans?](https://onlyfans.com/help/196/205/211)
* [Can I post AI-generated, altered, or enhanced content of someone else on OnlyFans?](https://onlyfans.com/help/196/205/212)
* [Can I use AI chatbots or AI-generated chat content in chats or direct messages?](https://onlyfans.com/help/196/205/213)
* [Can I share links to other platforms or sites on OnlyFans?](https://onlyfans.com/help/196/205/214)
* [Can I share personal information with Fans?](https://onlyfans.com/help/196/205/215)
* [Can I meet up with Fans?](https://onlyfans.com/help/196/205/216)

#### Using OnlyFans

* [What can my subscription price be on OnlyFans?](https://onlyfans.com/help/196/217/218)
* [How do I send a paid to unlock message?](https://onlyfans.com/help/196/217/219)
* [How much can I charge for a pay to unlock message?](https://onlyfans.com/help/196/217/220)
* [How much can I charge for a pay to unlock post?](https://onlyfans.com/help/196/217/221)
* [How do Tips work on OnlyFans?](https://onlyfans.com/help/196/217/222)
* [How much can my Fans tip me?](https://onlyfans.com/help/196/217/223)
* [How do I offer a Free Trial subscription?](https://onlyfans.com/help/196/217/224)
* [How do I run promotional campaigns on OnlyFans?](https://onlyfans.com/help/196/217/225)
* [How do I promote my OnlyFans account on other platforms?](https://onlyfans.com/help/196/217/226)
* [How do I get OnlyFans to promote my account on OnlyFans or other social media?](https://onlyfans.com/help/196/217/227)
* [How do I go "live" on OnlyFans?](https://onlyfans.com/help/196/217/228)
* [How do I "co-stream" on OnlyFans?](https://onlyfans.com/help/196/217/229)
* [How do I unsend a Direct Message?](https://onlyfans.com/help/196/217/230)
* [What can I include in a Post?](https://onlyfans.com/help/196/217/231)
* [How do I delete or edit a Post?](https://onlyfans.com/help/196/217/232)
* [How do I put a watermark on my content?](https://onlyfans.com/help/196/217/233)
* [How do I enable DRM Protection for my content?](https://onlyfans.com/help/196/217/234)
* [How do I get on OFTV?](https://onlyfans.com/help/196/217/235)

#### Complaints and Reports

* [How do I report leaked or stolen content?](https://onlyfans.com/help/196/236/237)
* [How can I appeal against a decision to deactivate content or suspend my account?](https://onlyfans.com/help/196/236/238)
* [How do I restrict or block a Fan?](https://onlyfans.com/help/196/236/239)
* [How can I report a Fan or Creator for violating OnlyFans Terms of Service?](https://onlyfans.com/help/196/236/240)

#### Referrals

* [How does OnlyFans Creator Referral Program work?](https://onlyfans.com/help/196/241/242)
* [How do I refer another Creator to OnlyFans?](https://onlyfans.com/help/196/241/243)

#### Payments

* [How do I get paid my earnings on OnlyFans?](https://onlyfans.com/help/196/244/245)
* [How long does it take to get paid my earnings on OnlyFans?](https://onlyfans.com/help/196/244/246)
* [How long does it take to get paid?](https://onlyfans.com/help/196/244/247)
* [Why is my payment delayed?](https://onlyfans.com/help/196/244/248)
* [Why have my payouts been suspended/rejected?](https://onlyfans.com/help/196/244/249)
* [What can I do about a chargeback?](https://onlyfans.com/help/196/244/250)
* [Why am I being charged a fee by my bank on payments from OnlyFans?](https://onlyfans.com/help/196/244/251)
* [Can I get Fans to pay me for content on another platform?](https://onlyfans.com/help/196/244/252)

#### Tax

* [Do I have to pay tax on any earnings from OnlyFans?](https://onlyfans.com/help/196/253/254)
* [Why am I being asked to provide OnlyFans with my tax number/tax identity information?](https://onlyfans.com/help/196/253/255)
* [How do I request a 1099 Form?](https://onlyfans.com/help/196/253/256)
* [How do I update my 1099 Form?](https://onlyfans.com/help/196/253/257)

### For Fans

#### Becoming a Fan

* [How do I open a Fan account?](https://onlyfans.com/help/258/259/260)
* [Why am I being asked to prove I am over 18?](https://onlyfans.com/help/258/259/261)
* [How do I delete my OnlyFans Fan account?](https://onlyfans.com/help/258/259/262)

#### Interacting with Creators

* [How do I find a creator?](https://onlyfans.com/help/258/263/264)
* [How do I subscribe to a creator?](https://onlyfans.com/help/258/263/265)
* [Why did my subscription auto renew?](https://onlyfans.com/help/258/263/266)
* [How do I cancel a subscription?](https://onlyfans.com/help/258/263/267)
* [How do free trials or discounts work on OnlyFans?](https://onlyfans.com/help/258/263/268)
* [Can I share personal information with Creators?](https://onlyfans.com/help/258/263/269)
* [Can I meet up with Creators?](https://onlyfans.com/help/258/263/270)
* [Can I download or copy content from OnlyFans?](https://onlyfans.com/help/258/263/271)

#### Payments

* [How do I tip a creator?](https://onlyfans.com/help/258/272/273)
* [How much can I tip a Creator?](https://onlyfans.com/help/258/272/274)
* [How do I make a payment on OnlyFans?](https://onlyfans.com/help/258/272/275)
* [How will payments made on OnlyFans appear on my card statements?](https://onlyfans.com/help/258/272/276)
* [How do I use the OnlyFans Wallet?](https://onlyfans.com/help/258/272/277)
* [How do I change my daily payment limits?](https://onlyfans.com/help/258/272/278)
* [Why am I being charged Tax on my payments on OnlyFans?](https://onlyfans.com/help/258/272/279)
* [Why am I being charged a jurisdiction charge on OnlyFans?](https://onlyfans.com/help/258/272/280)
* [Why am I being charged a fee by my bank for payments on OnlyFans?](https://onlyfans.com/help/258/272/281)

#### Complaints and Reports

* [How do I make a complaint regarding content I am not happy with?](https://onlyfans.com/help/258/282/283)
* [How do I report a Creator for violating OnlyFans Terms of Service?](https://onlyfans.com/help/258/282/284)
* [How do I request a refund?](https://onlyfans.com/help/258/282/285)