**Termini e condizioni di AliExpress per le transazioni sulla piattaforma effettuate da Consumatori UE/SEE/UK** 

**Versione aggiornata il 29 novembre 2024, in vigore dal 14 dicembre 2024**

  

LA RINGRAZIAMO PER AVER UTILIZZATO ALIEXPRESS.

I PRESENTI TERMINI E CONDIZIONI PER I CONSUMATORI DELL'UE/SEE/UK SI RIFERISCONO A [WWW.ALIEXPRESS.COM](http://www.aliexpress.com/) E SI APPLICANO ESCLUSIVAMENTE AI CONSUMATORI DELL'UNIONE EUROPEA/SPAZIO ECONOMICO EUROPEO E DEL REGNO UNITO. SI PREGA DI LEGGERE ATTENTAMENTE I TERMINI E LE CONDIZIONI DEL PRESENTE DOCUMENTO!

  

SE LEI È (a) UN UTENTE DI [WWW.ALIEXPRESS.COM](http://www.aliexpress.ru/) PROVENIENTE DA UN'AREA GEOGRAFICA DIVERSA DALL'UNIONE EUROPEA/SPAZIO ECONOMICO EUROPEO O DAL REGNO UNITO, (b) UN UTENTE DI [WWW.ALIEXPRESS.RU](http://www.aliexpress.ru/) O [WWW.TMALL.RU](http://www.tmall.ru/) , O (c) SE LEI ALTRIMENTI ACCEDE AI SITI IN VESTE DIVERSA DA QUELLA DI CONSUMATORE DELL'UNIONE EUROPEA/SPAZIO ECONOMICO EUROPEO/REGNO UNITO, LA PREGHIAMO DI CLICCARE SU [https://rule.alibaba.com/rule/detail/2054.htm](https://rule.alibaba.com/detail/2054.htm) PER ACCEDERE AI TERMINI E ALLE CONDIZIONI A LEI APPLICABILI.

  

I presenti Termini e condizioni di AliExpress.com per le transazioni sulla piattaforma effettuate da Consumatori dell'UE/SEE/UK (di seguito, il "Contratto") descrivono i termini e condizioni in virtù dei quali Lei, in qualità di membro registrato nonché di consumatore dell'Unione Europea/Spazio Economico Europeo o del Regno Unito, può concludere transazioni online con venditori terzi che siano membri registrati dei Siti (nel senso del termine come definito di seguito) o fornire determinati prodotti o servizi a consumatori dell'Unione Europea/Spazio Economico Europeo o del Regno Unito, secondo i termini e condizioni di vendita stabiliti da ciascun venditore terzo attraverso i siti web, i siti mobili, le applicazioni mobili e gli altri portali di transazioni online di proprietà, gestiti, brandizzati o resi disponibili da AliExpress.com (come definiti di seguito nella clausola 1.2) di volta in volta relativi alla piattaforma di e-commerce AliExpress, che includono, a titolo esemplificativo e non esaustivo, le versioni ottimizzate per il web e per i dispositivi mobili del sito web identificato dall’identificatore univoco URL (uniform resource locator) "www.aliexpress.com", e l'applicazione mobile della piattaforma di e-commerce AliExpress, con espressa esclusione di [www.aliexpress.ru](http://www.aliexpress.ru/) e [www.tmall.ru](http://www.tmall.ru/) (collettivamente, i "Siti").

  

A scanso di equivoci, ogni Transazione Online (come definita di seguito nella clausola 1.5) si intende esclusivamente effettuata da e tra Lei e un venditore terzo, AliExpress.com pertanto non deve essere considerata come parte di tale Transazione Online, né deve essere considerata come un rappresentante suo o del venditore in tale Transazione Online.

  

Poiché Lei agisce in qualità di consumatore dell'UE/SEE/UK, la preghiamo di notare che, nella misura in cui le norme di legge sulla tutela dei consumatori nel suo paese di residenza all'interno dell'UE/SEE/UK prevedano delle disposizioni più vantaggiose per Lei (come, ad esempio, diritti legali di recesso e di garanzia di conformità), tali disposizioni saranno automaticamente applicabili a Lei. Per ulteriori informazioni sui diritti legali dei consumatori UE, consultare [https://sale.aliexpress.com/\_\_mobile/QnoLFBVfqY.htm](https://sale.aliexpress.com/__mobile/QnoLFBVfqY.htm).

  

**1\. Applicazione del presente Contratto e dei Servizi di Transazione**

**1.1** **Consentimento.** Lei prende atto che registrandosi, accedendo o utilizzando i Servizi di Transazione (come descritti di seguito nella clausola 1.5) Lei acconsente alla stipula di un contratto legalmente vincolante con AliExpress.com. Per fare ciò, Lei deve essere maggiorenne e avere la capacità giuridica di stipulare un contratto legalmente vincolante ai sensi della legge applicabile. SI RICORDA CHE I SERVIZI DI TRANSAZIONE SONO VIETATI AI MINORI SECONDO LA DEFINIZIONE DELLA LEGGE VIGENTE. I prodotti destinati ai bambini messi in vendita dai venditori sui Siti sono destinati all'acquisto da parte di adulti. Se Lei non acconsente al presente Contratto, Lei deve astenersi dall’accedere o altrimenti utilizzare qualsiasi dei Servizi di Transazione. Se Lei desidera risolvere il presente Contratto, può farlo in qualsiasi momento chiudendo il suo account di utente e astenendosi definitivamente dall’accedere o utilizzare i Servizi di Transazione (come definiti nella clausola 1.5).

**1.2** **Parte Contraente.** Lei sta stipulando il presente Contratto con AliExpress.com, ma la preghiamo di notare che AliExpress.com può fornire i Servizi di Transazione da sola o anche in collaborazione con le sue affiliate. Nel presente documento il termine "Paesi Designati" viene utilizzato per riferirsi a paesi dell'Unione Europea (UE), dello Spazio Economico Europeo (SEE) e del Regno Unito. Se Lei ha la sua residenza abituale in un Paese Designato o accede ai Siti da un Paese Designato, Lei sta stipulando il presente Contratto con Alibaba.com Singapore E-Commerce Private Limited (società costituita a Singapore, iscritta al Registro delle imprese con il numero 200720572D, con sede legale in 51 Bras Basah Road, n. 04-08 Lazada One, 189554-Singapore (di seguito, " AliExpress.com" o “noi”)). AliExpress.com sarà la titolare del trattamento dei suoi dati personali forniti, raccolti o elaborati a cura di AliExpress.com per la prestazione dei Servizi di Transazione o in relazione con gli stessi. La raccolta e il trattamento dei suoi dati personali sono soggetti alla [Politica sulla Privacy di AliExpress.com](https://terms.alicdn.com/legal-agreement/terms/suit_bu1_aliexpress/suit_bu1_aliexpress201909171350_82407.html?spm=a1zaa.8161610.0.0.711161ec6o4ND3) (che include la Politica sui Cookie) e ogni altro documento citato nella stessa e relativi aggiornamenti.

**1.3** **Solo per Membri.** Quando si registra e aderisce ai Siti, Lei acquisisce la categoria membro registrato ("Membro"). Il presente Contratto si applica a Lei esclusivamente in qualità di Membro, ma non influisce sul suo rapporto contrattuale in essere con AliExpress.com o con le sue affiliate in merito ai Servizi per i Membri dei Siti ai sensi del Contratto di Adesione, salvo espressamente stipulato il contrario nel presente Contratto o nel Contratto di Adesione. Se Lei chiude il suo account di utente o se la sua iscrizione viene disdetta da AliExpress.com nell’esercizio dei propri diritti ai sensi del Contratto di Adesione, Lei non potrà più utilizzare i Servizi di Transazione. Tuttavia, se al momento della chiusura dell'account o della disdetta dell'iscrizione AliExpress.com a conoscenza che Lei ha iniziato una Transazione online valida che non è ancora stata completata, si prenderanno le opportune disposizioni affinché tale Transazione online venga completata o rimborsata, a seconda delle circostanze della transazione e della causa della disdetta della sua iscrizione.

**1.4** **Intero Accordo.** Il presente Contratto è l'unico accordo in essere tra Lei e AliExpress.com avente per oggetto i Servizi di Transazione e sostituisce tutti i contratti precedentemente stipulati per tali Servizi di Transazione. Il presente Contratto incorpora per riferimento diretto i documenti, le politiche, le informative e i regolamenti elencati di seguito, nonché i termini e condizioni contenuti nei contratti proposti da AliExpress.com e convenuti con Lei nel momento in cui Lei si impegna a utilizzare una funzionalità dei Servizi di Transazione, e che Lei accetta di osservare nella misura in cui le siano applicabili:

·       [Contratto di Adesione di AliExpress.com](https://terms.alicdn.com/legal-agreement/terms/suit_bu1_aliexpress/suit_bu1_aliexpress202204182115_37406.html "Contratto di Adesione")

·       [Condizioni di Utilizzo di AliExpress.com](https://terms.alicdn.com/legal-agreement/terms/suit_bu1_aliexpress/suit_bu1_aliexpress202204182115_66077.html "Condizioni di Utilizzo")

·       [Politica sulla Privacy di AliExpress.com](https://terms.alicdn.com/legal-agreement/terms/suit_bu1_aliexpress/suit_bu1_aliexpress201909171350_82407.html?spm=a1zaa.8161610.0.0.711161ec6o4ND3 "Politica sulla Privacy") (che include la Politica sui Cookie) (“Politica sulla Privacy”)

·       [Politica di Protezione dei Diritti di Proprietà Intellettuale (DPI)](https://sell.aliexpress.com/en/__pc/Hh1G3770tV.htm?spm=a1zaa.8161610.0.0.711161ec6o4ND3 "Politica di Protezione dei Diritti di Proprietà Intellettuale")

In alcuni casi, le potrebbe essere richiesto di stipulare un apposito contratto separato con AliExpress.com, con le sue affiliate o altri partner terzi, in relazione a determinati aspetti dei Servizi di Transazione non coperti dal presente Contratto (le "**Condizioni Aggiuntive**"), includenti, nella misura in cui siano applicabili, le [Condizioni dei Servizi Supplementari](https://rulechannel.alibaba.com/icbu#/rules?cId=1303) se Lei stipula dei servizi supplementari a quelli previsti dal presente Contratto; e l’[Accordo sui Servizi Alipay](https://global.alipay.com/docs/ac/platform/sfe2r7) se Lei stipula un contratto per un portafoglio digitale o wallet Alipay,).

  

Nel caso di eventuale contraddizione tra le disposizioni del presente Contratto e le disposizioni delle Condizioni Aggiuntive, le Condizioni Aggiuntive disciplineranno le relative tipologie dei Servizi di Transazione o delle Transazioni Online coperte da tali Condizioni Aggiuntive.

**1.5 Servizi di Transazione.** AliExpress.com fornisce una piattaforma di transazioni online e servizi accessori sui Siti (tale piattaforma e servizi, di seguito, collettivamente, i “Servizi di Transizione”) che permettono ai Membri di concludere transazioni online per prodotti o servizi all’interno dei Siti in conformità con le condizioni del presente Contratto. In particolare, ciò le consentirà di inoltrare, pagare, concludere e ricevere gli ordini evasi per la fornitura online di prodotti e servizi all'interno dei Siti (le "Transazioni Online").

 

**1.6 Modifiche al Contratto o ai Servizi di Transazione.** AliExpress.com si riserva il diritto di modificare il presente Contratto (compresi, a scanso di equivoci, i termini e condizioni relativi ai contratti previsti nella clausola 1.4 di cui sopra), di pubblicare nuovi regolamenti (compresi quelle relativi alle controversie) o di apportare modifiche alle funzionalità o alle applicazioni dei Servizi di Transazione stessi (ad esempio, apportando aggiornamenti, modifiche, creando limiti o sospendendoli). Qualora AliExpress.com apporti alcuna modifica (ad eccezione di correzioni di formattazione o di errori) al presente Contratto (compresa tutta la relativa documentazione) o ai Servizi di Transazione stessi, oppure pubblichi dei nuovi regolamenti, sarà tenuta a notificarlo a Lei attraverso i Siti (compresa la pubblicazione della versione più recente sui Siti) o con altri mezzi, per dare a Lei l'opportunità di rivedere le modifiche o i nuovi termini con almeno quindici (15) giorni di preavviso dalla loro entrata in vigore (dal momento che non sono retroattivi), anche se potrebbero darsi delle circostanze (come ad esempio per ottemperare a un obbligo di legge, per affrontare un pericolo imprevisto e imminente, per proteggere Lei o un utente commerciale da frodi, malware, spam, violazioni di dati personali o altri rischi di sicurezza informatica) in cui AliExpress.com potrebbe vedersi obbligata a dare un periodo di preavviso più breve. Se Lei non è d’accordo con alcuna modifica proposta da AliExpress.com, Lei può chiudere il suo account di utente inoltrando la sua richiesta per e-mail all'indirizzo [DataProtection.AE@aliexpress.com](mailto:DataProtection.AE@aliexpress.com), oppure accedendo al suo "Profilo - Elimina account" nell'applicazione mobile dei Siti o richiedendo la cancellazione del suo account accedendo a privacy.aliexpress.com. L'uso continuato dei Servizi di Transazione dopo la pubblicazione o l'invio della notifica avente per oggetto le modifiche o i nuovi termini del presente Contratto apportati da AliExpress.com e dopo la loro entrata in vigore, significa che Lei accetta i termini aggiornati a partire dalla loro data di entrata in vigore.

  

**2\. Condizioni per Transazioni Online specifich**

**2.1 Rifiuto o Annullamento di Transazioni Online.** AliExpress.com si riserva il diritto di sospendere in attesa di chiarimenti e successivamente o direttamente rifiutare/respingere o annullare qualsiasi Transazione Online ragionevolmente ritenuta in violazione del presente Contratto (compresi i relativi documenti previsti dalla clausola 1.4 di cui sopra), o di imporre penali o restrizioni, per qualsiasi motivo pertinente, incluso in conformità alle leggi e ai regolamenti sulla tutela dei consumatori, sull’e-commerce, sulle frodi, sulla sicurezza informatica o sulla tutela dei diritti di proprietà intellettuale e industriale, oppure dietro richiesta delle autorità competenti, esimendosi da qualsiasi responsabilità per eventuali danni o perdite direttamente o indirettamente derivanti da tale rifiuto o annullamento. QUALSIASI restrizione all'uso dei Siti le sarà notificata tramite il Centro messaggi dell'acquirente.

  

La valutazione della violazione del presente Contratto sarà condotta internamente dagli uffici competenti di AliExpress.com, coinvolgendo i team di Governance della Piattaforma, di Controllo del Rischio e/o del Servizio Clienti. In particolare, AliExpress può sospendere il suo accesso al Servizio per un periodo di tempo ragionevole e dopo aver emesso un avviso preventivo se Lei fornisce frequentemente contenuti manifestamente illegali. In tali casi, e laddove i relativi dati di contatto elettronico siano noti ad AliExpress, AliExpress la informerà con una motivazione e le possibilità di ricorso.

  

Si rimanda alla clausola 5.3. “Violazioni” per l'elenco delle potenziali restrizioni che AliExpress può applicare all'utilizzo dei Siti da parte sua e dei mezzi che AliExpress utilizza per moderare i contenuti sui nostri Siti.

  

Le situazioni che potrebbero risultare in una Transazione Online rifiutata o annullata includono, a titolo esemplificativo e non limitativo, situazioni in cui l’ufficio di controllo del credito e delle frodi rileva delle criticità, o in cui AliExpress.com ha motivo di ritenere che la Transazione Online non sia autorizzata, che violi qualsiasi legge, norma o regolamento o che altrimenti esponga AliExpress.com o qualsiasi delle sue affiliate a responsabilità.

  

Prima di autorizzare la Transazione Online in questione e di procedervi addebitandola all'acquirente, o prima di prendere la decisione finale di rifiutare/respingere o annullare la Transazione Online, a seconda dei casi, AliExpress.com può anche richiederle ulteriori verifiche o informazioni su qualsiasi Transazione Online, nel qual caso Lei accetta di collaborare a tali verifiche e di fornire tali informazioni ad AliExpress.com a richiesta di quest’ultima. La mancata collaborazione alle verifiche e la mancata presentazione delle informazioni da parte sua quando le siano state richieste da AliExpress.com, può comportare l’esito negativo della Transazione Online, rispetto alla quale AliExpress.com non è responsabile. A scanso di equivoci, se AliExpress.com determina finalmente che tale Transazione Online rifiutata o annullata non costituisce una violazione del presente Contratto, AliExpress.com si impegna ad adottare le opportune disposizioni affinché tale Transazione Online venga completata o rimborsata, a seconda delle circostanze della transazione. AliExpress.com non sarà responsabile per eventuali perdite o danni direttamente o indirettamente derivanti dal rifiuto o dall'annullamento della Transazione Online in questione, essendo autorizzata a prendere una decisione su qualsiasi controversia tra Lei e il venditore, compresa la rimessa dei fondi nell'ambito di una Transazione Online che sono detenuti da Alipay (come definito nella seguente clausola 2.2) secondo le istruzioni di AliExpress.com in conformità al presente Contratto e all'[Accordo sui Servizi Alipay](https://global.alipay.com/doc/platform/sfe2r7).

  

**2.2 Servizi Alipay e Servizi Supplementari** **AliExpress.com.**

(i) AliExpress.com potrà, tramite Alipay, fornire alcuni servizi per alcune Transazioni Online (“Servizi Alipay”). I Servizi Alipay sono forniti agli utenti UE/SEE/UK da Alipay (Europe) Limited S.A. e dalle sue affiliate (collettivamente, “Alipay”) per ricevere il pagamento di fondi a sostegno dei Siti per le Transazioni Online. I Servizi Alipay sono forniti in conformità con i termini e le condizioni di cui all’[Accordo sui Servizi Alipay](https://global.alipay.com/doc/platform/sfe2r7).

(i) AliExpress.com potrà, tramite le sue affiliate fornire alcuni servizi accessori per determinate Transazioni Online (i “Servizi Supplementari AliExpress.com ”). I Servizi Supplementari AliExpress.com sono forniti da AliExpress.com per ricevere il pagamento di fondi a sostegno dei Siti per le Transazioni Online. I Servizi Supplementari AliExpress.com sono forniti in conformità con i termini e le condizioni stabiliti nell’[Accordo sui Servizi Supplementari AliExpress.com](https://rule.alibaba.com/rule/detail/3180.htm) .

  

**2.3 Piano di protezione dell’acquirente.** Salvi e impregiudicati i diritti legali a Lei spettanti nell'UE/SEE/UK, AliExpress.com può anche fornire agli acquirenti un piano di protezione per alcune Transazioni Online. Se Lei desidera ricevere ulteriori informazioni sul piano di protezione degli acquirenti, clicchi [qui](https://sale.aliexpress.com/__pc/v8Yr8f629D.htm).

I principali diritti legali a Lei spettanti nell'UE/SEE/UK prevedono, tra l'altro, (i) il diritto di recesso entro quattordici (14) giorni; (ii) un diritto di garanzia legale di conformità di almeno due (2) anni; e (iii) il diritto di risolvere il contratto se il venditore non consegna la merce entro un periodo di tempo supplementare adeguato alle circostanze una vota scaduto il termine di consegna iniziale. Per ulteriori informazioni sui diritti legali dei consumatori UE, consultare [https://sale.aliexpress.com/\_\_mobile/QnoLFBVfqY.htm](https://sale.aliexpress.com/__mobile/QnoLFBVfqY.htm).

Lei prende atto e conviene che la protezione a Lei accordata ai sensi di un piano di protezione dell’acquirente si applica solo alle Transazioni Online per le quali il venditore ha sottoscritto il tale piano e l’acquisto rientra nell’ambito di applicazione del piano di protezione dell’acquirente e che (i) i Servizi Alipay ai sensi della clausola 3.4 dell’[Accordo sui Servizi Alipay](https://global.alipay.com/doc/platform/sfe2r7) e (ii) i Servizi Supplementari AliExpress.com ai sensi della clausola 3.4 dell’[Accordo sui Servizi Supplementari AliExpress.com](https://rule.alibaba.com/rule/detail/3180.htm)【inserire hyperlink】non saranno applicabili allo stesso per tali Transazioni Online se il venditore ha sottoscritto un piano di protezione dell’acquirente e tale piano già copre il suo acquisto. Il servizio di garanzia potrà essere prestato, rivisto, sospeso e/o disdetto in base all’accordo raggiunto tra il fornitore del servizio di garanzia e il venditore. A scanso di equivoci, qualsiasi revisione, sospensione e/o disdetta del Piano di Protezione dell'Acquirente UE/SEE/UK non riguarderà le Transazioni Online già avvenute o che sono state inoltrate ma non ancora completate al momento della revisione, sospensione e/o disdetta del Piano di Protezione dell'Acquirente.

  

**2.4 Transazioni che coinvolgono un fornitore terzo di finanziamenti.** Questa clausola è applicabile a Lei se decide di ottenere un finanziamento per una Transazione Online; nel qual caso Lei conviene che:

(a) AliExpress.com non garantisce che alcun fornitore terzo di finanziamenti (il "Finanziatore") finanzierà la sua Transazione Online, né sarà responsabile nei suoi confronti in relazione ad alcun finanziamento termite terzi;

(b) qualsiasi finanziamento della transazione si intende concesso da una terza parte non vincolata ad AliExpress.com ed è quindi soggetto ai termini e condizioni di tale Finanziatore;

(c) AliExpress.com ha facoltà di divulgare al Finanziatore alcune informazioni raccolte su Lei (ad esempio, dati finanziari e di contatto) se tali informazioni sono necessarie al Finanziatore per esaminare la sua richiesta di finanziamento e formalizzare il contratto di finanziamento, come a Lei previamente comunicato; e

(d) qualsiasi controversia con il Finanziatore in relazione alla Transazione Online deve risolversi tra Lei e il Finanziatore, non avendo AliExpress.com alcun obbligo di risolvere o assistere in alcun modo nella risoluzione di tale controversia.

  

**3\. Transazioni tra Lei e il venditore**

**3.1 Ordine Online.** Per concludere una Transazione Online Lei deve completare, inoltrare e accettare un ordine online utilizzando la procedura applicabile sui Siti e completare tale Transazione Online secondo i termini e condizioni dell'ordine online e del presente Contratto (inclusi i relativi documenti previsti nella clausola 1.4 di cui sopra). Lei e il venditore sarete responsabili di garantire di aver concordato e specificato, in particolare sulla base delle informazioni fornite dal venditore sulla pagina del prodotto o accessibili attraverso la pagina del prodotto, tutti i termini e condizioni pertinenti per la fornitura dei prodotti o i servizi con le modalità adeguate, compresi, a titolo esemplificativo e non limitativo, i prezzi, la quantità, le specifiche tecniche, gli standard di qualità, l'ispezione, la spedizione, ecc. tenuto conto del fatto che tutte le transazioni concluse sui Siti devono consentire ad AliExpress.com, in qualità di titolare della piattaforma commerciale, di calcolare e riscuotere correttamente le imposte nella misura richiesta dalle leggi e dai regolamenti applicabili. A fini doganali e fiscali e salvo diversamente concordato tra Lei e il venditore, Lei agisce in qualità di importatore. A scanso di equivoci, le ricordiamo prima di inoltrare l'ordine la possibile esistenza di tasse e altri costi aggiuntivi che non possono essere ragionevolmente calcolati in anticipo ma che potrebbero essere a suo carico (ad esempio, dazi d'importazione, tasse e spese di gestione per prodotti importati nell'UE da paesi terzi). Lei può accedere alle informazioni obbligatorie relative al venditore cliccando sul nome del negozio attraverso la pagina in cui è presentato il prodotto o attraverso la pagina del negozio.

  

**3.2 Annullamento delle Transazioni Online da parte dell'utente.** Lei può annullare una Transazione Online solo nelle seguenti circostanze: (a) ai fini di esercitare i suoi diritti ai sensi della Direttiva 2011/83/UE o di altre norme a tutela dei consumatori eventualmente applicabili, comprese le leggi nazionali, o (b) se tale annullamento è consentito da qualsiasi.

  

**3.3** Piano di Protezione dell'Acquirente applicabile e/o se consentito specificamente dalla Piattaforma per quel tipo di prodotto o di Transazione Online. La preghiamo di consultare su [https://sale.aliexpress.com/\_\_mobile/QnoLFBVfqY.htm](https://sale.aliexpress.com/__mobile/QnoLFBVfqY.htm) i diritti legali di recesso e di risoluzione che le spettano nell'UE/SEE/UK, che prevedono, tra gli altri (i) il diritto di recesso entro quattordici (14) giorni; (ii) un diritto di garanzia legale di conformità di almeno due (2) anni; e (iii) il diritto di risolvere il contratto se il venditore non consegna la merce entro un periodo di tempo supplementare adeguato alle circostanze dopo il termine di consegna iniziale.

  

**3.4 Transazioni esclusive tra Lei e un venditore.** Ogni Transazione Online si intende esclusivamente effettuata da e tra Lei e un venditore, AliExpress.com pertanto non deve essere considerata come parte di tale Transazione Online, né deve essere considerata come un rappresentante suo o del venditore in tale Transazione Online. Fatto salvo quanto sopra, in qualità di fornitore dei Servizi di Transazione, AliExpress.com ha facoltà di effettuare dei controlli formali su ogni Transazione Online. AliExpress.com non sarà responsabile né della qualità, della sicurezza, della liceità o della disponibilità dei prodotti o servizi offerti ai sensi di qualsiasi Transazione Online, né della capacità di Lei o del venditore di completare qualsiasi Transazione Online (salvo nella misura in cui ciò sia vietato dalla legge applicabile). Né AliExpress.com né le sue affiliate e i suoi agenti saranno ritenuti responsabili per eventuali perdite, danni, pretese, responsabilità, costi o spese derivanti da qualsiasi Transazione Online, tra cui qualsiasi inadempimento, esecuzione parziale o mancata esecuzione della Transazione Online da parte della controparte della transazione.

  

**3.5 Metodi di pagamento.** Si prega di notare che i metodi di pagamento disponibili sui Siti possono essere forniti da partner o affiliate di AliExpress.com, tra cui Alipay, per cui Lei accetta di pagare il prezzo della transazione indicato per qualsiasi Transazione Online utilizzando qualsiasi di tali metodi di pagamento. Qualora la Transazione Online adotti i Servizi Alipay, il pagamento in relazione alle Transazioni Online perfezionate sarà facilitato da Alipay e soggetto all’Accordo sui Servizi Alipay. Alipay non potrà quindi erogare tali fondi salvo diversamente concordato tra Lei e il venditore in conformità al presente Contratto e all'[Accordo sui Servizi Alipay](https://global.alipay.com/doc/platform/sfe2r7).

  

Se la Transazione Online adotta i Servizi Supplementari AliExpress.com, il pagamento in relazione a tale transazione sarà facilitato da AliExpress.com e soggetto all'[Accordo sui Servizi Supplementari AliExpress.com](https://rule.alibaba.com/rule/detail/3180.htm). Utilizzando i Servizi Supplementari AliExpress.com, Lei prende atto e conviene che AliExpress.com non è una banca e che i Servizi Supplementari AliExpress.com non dovranno essere interpretati in alcun modo come la fornitura di servizi bancari. AliExpress.com non agisce in qualità di amministratore fiduciario, di fiduciario o di agente depositario in relazione ai fondi dell’utente e non ha alcun controllo o responsabilità in relazione ai prodotti o servizi che sono pagati con i Servizi Supplementari AliExpress.com. AliExpress.com non è tenuta a garantire l’identità di alcun utente né ad assicurare che Lei o un venditore completi una transazione sui Siti.

  

Nel caso di qualsiasi controversia insorgente in relazione al metodo di pagamento e/o al suo fornitore, AliExpress.com compirà ogni ragionevole sforzo per assistere Lei nella partecipazione al processo di risoluzione delle controversie dei relativi partner fornitori di servizi di pagamento. Ciononostante, se la partecipazione al processo di risoluzione delle controversie del partner di AliExpress.com fosse soggetta a spese aggiuntive, tali spese non saranno a carico di AliExpress.com.

  

**3.6 Il suo Agente.** Se Lei è tenuto a concludere e completare una Transazione Online tramite un agente (per esempio a un acquirente UE/SEE/UK potrebbe essere richiesto di impiegare un agente di importazione qualificato), tale agente è solo un suo agente. Se l’agente dovesse adempiere ad eventuali obblighi, Lei rimarrà l’unico soggetto responsabile nei confronti della controparte della Transazione Online per la mancata esecuzione o l’inadempimento da parte del suo agente.

  

**4\. Tariffe di Servizio di AliExpress.com e Fiscalità**

**4.1 Tariffe di Servizio.** AliExpress.com potrà addebitare delle tariffe di servizio per le Transazioni Online come eventualmente reso noto di volta in volta da AliExpress.com sui Siti. Nel caso in cui la Transazione Online adotti i Servizi Alipay, con la presente Lei autorizza AliExpress.com a dare istruzioni ad Alipay di detrarre ogni Tariffa di Servizio che sia dovuta e pagabile ad AliExpress.com ai sensi di una Transazione Online e di pagare la stessa ad AliExpress.com quando Alipay eroga qualsiasi importo detenuto da quest’ultima ai sensi di una Transazione Online. Né AliExpress.com né Alipay non hanno alcun controllo o alcuna responsabilità in relazione ai prodotti o servizi che sono pagati con i loro Servizi.

  

**4.2 Tariffe di Terzi non incluse.** Se Lei decide di acquistare un servizio o un prodotto da terzi in relazione alla Transazione Online (ad es. un servizio di riparazione, un servizio di evasione ordini e di logistica, ecc.), Lei dovrà pagarne separatamente le spese (nonché le corrispondenti tasse e imposte eventualmente applicabili) di acquisto di tale servizio o prodotto. Sarà sua responsabilità saldare le tariffe con tali fornitori terzi.

  

**4.3 Tasse e oneri finanziari non inclusi.** Tutte le tariffe addebitate da AliExpress.com sono al netto di eventuali tasse (come IVA), dazi o altre imposte governative o di eventuali oneri finanziari. Le imposte applicabili (come l'IVA) saranno visualizzate in anticipo e le verranno addebitate in aggiunta alle tariffe. Lei accetta di pagare e di essere responsabile di tali eventuali tasse, dazi, imposte o oneri per l’utilizzo dei Servizi di Transazione in aggiunta alle Tariffe di Servizio di AliExpress.com. Lei conviene altresì di essere tenuto a pagare qualsivoglia tassa applicabile associata alle Transazioni Online effettuate su AliExpress.com (comprese, senza limitazione, eventuali importi IVA applicabili) e accetta di manlevare e tenere indenne AliExpress.com dal mancato pagamento de eventuali tasse applicabili. Qualora AliExpress.com fosse tenuta a raccogliere o trattenere eventuali tasse o dazi ai sensi di qualunque legge applicabile, Lei accetta di versare tali tasse o dazi ad AliExpress.com. Lei sarà inoltre responsabile di eventuali oneri finanziari per rimborsi di fondi effettuati a suo favore e AliExpress.com avrà il diritto di pagare tali oneri da tali fondi. AliExpress.com e Alipay avranno il diritto di detrarre eventuali oneri finanziari sostenuti a seguito della fornitura dei Servizi di Transazione e la parte che riceve i fondi dovrà farsi carico dei costi di tali spese bancarie.

  

**5\. Responsabilità del Membro**

**5.1 Fornitura di informazioni e assistenza.** Lei accetta di inviare tutti gli avvisi, di fornire tutte le informazioni, i materiali e le approvazioni necessarie e di prestare tutta l'assistenza e la cooperazione ragionevolmente necessarie per il completamento delle Transazioni Online e per la fornitura dei Servizi di Transazione da parte di AliExpress.com, in modo da facilitare la corretta consegna dei prodotti o dei servizi oggetto della Transazione Online con un venditore (incluso, ad esempio, per la personalizzazione del prodotto, se del caso; per facilitare la consegna fornendo dati precisi sul metodo di pagamento, indirizzi, dati di contatto o opzioni alternative di consegna e ritiro). Se la sua inadempienza degli obblighi di cui sopra risultasse in un ritardo nella fornitura di qualsiasi Servizio di Transazione, nella cancellazione di qualsiasi Transazione Online o nell’erogazione di eventuali fondi, AliExpress.com non sarà responsabile per eventuali perdite o danni derivanti da tale inadempienza, ritardo, cancellazione o erogazione.

  

**5.2 Obblighi del Membro.** Lei dichiara e garantisce che:

a) non utilizzerà i Siti, compreso il sistema di feedback, per diffondere contenuti illegali; il suo utilizzo dei Siti, compreso il sistema di feedback, deve essere sempre conforme a tutte le leggi e i regolamenti applicabili;

b) utilizzerà i Servizi di Transazione in buona fede e nel rispetto di tutte le leggi e i regolamenti applicabili, comprese le leggi in materia di antiriciclaggio e di lotta al finanziamento del terrorismo;

c) tutte le informazioni e tutti i materiali che Lei fornisce in relazione all’utilizzo dei Servizi di Transazione sono veritieri, leciti ed accurati, e non falsi, fuorvianti o ingannevoli;

d) non utilizzerà i Servizi di Transazione per frodare AliExpress.com, le sue affiliate, o altri membri o utenti dei Siti, o per intraprendere altre attività illecite (tra cui, senza limitazione, il commercio di prodotti vietati dalla legge);

e) non utilizzerà i Servizi di Transazione in relazione a qualsiasi Transazione Online in modo da (1) violare i diritti legittimi o di titolarità di AliExpress.com o di terzi, compresi i diritti d'autore, i diritti su marchi e brevetti o altri diritti di proprietà intellettuale; (2) violare la [Politica sulla Protezione dei Diritti di Proprietà Intellettuale (DPI)](https://rule.alibaba.com/rule/detail/2049.htm); o (3) violare il presente Contratto o l'[Accordo sui Servizi Supplementari AliExpress.com](https://rule.alibaba.com/rule/detail/3180.htm) .

  

**Violazioni.** Qualora Lei, secondo la ragionevole opinione di AliExpress.com, non agisca in buona fede, abusi dei Servizi di Transazione o altrimenti agisca in violazione del presente Contratto, AliExpress.com avrà il diritto, salvo nella misura in cui ciò sia vietato dalla legge applicabile, di annullare in qualsiasi momento la Transazione Online o le Transazioni Online in questione, esimendosi da qualsiasi responsabilità per eventuali perdite o danni direttamente o indirettamente derivanti da tale annullamento, dandole un preavviso di quindici (15) giorni dalla data effettiva dell’annullamento della Transazione Online o delle Transazioni Online in questione, salvo non si renda necessario un periodo di preavviso più breve per ottemperare a un obbligo di legge o per affrontare un pericolo imprevisto e imminente, per proteggere i servizi di intermediazione online, i consumatori (compresi gli acquirenti UE/SEE/UK) o gli utenti commerciali (compresi i venditori) da frodi, malware, spam, violazioni dei dati o altri rischi di sicurezza informatica.

  

Salvo altrimenti previsto dalla legge applicabile, AliExpress.com si riserva inoltre il diritto di applicare restrizioni al suo utilizzo dei Siti; tali restrizioni includono: (i) il blocco dei suoi contenuti sui Siti, (ii) la cancellazione dei suoi contenuti sui Siti, (iii) il congelamento dei suoi diritti di pubblicazione sui Siti, incluso il sistema di feedback (iv) la sospensione dei suoi diritti di segnalazione, inclusi i diritti di notifica ai sensi della Clausola 11, (v) la sospensione o interruzione temporanea o definitiva dell'utilizzo da parte sua dei Servizi di Transazione, (vi) la sospensione o disdetta temporanea o definitiva o far sospendere o disdire la sua iscrizione ai Siti, dandole un preavviso di quindici (15) giorni prima della data effettiva della sospensione o interruzione o disdetta, salvo non si renda necessario un periodo di preavviso più breve per ottemperare a un obbligo di legge o per affrontare un pericolo imprevisto e imminente, per proteggere i servizi di intermediazione online, i consumatori (compresi gli acquirenti UE/SEE/UK) o gli utenti commerciali (compresi i venditori) da frodi, malware, spam, violazioni dei dati o altri rischi di sicurezza informatica. In tali casi, e salvo diversamente stabilito dalle leggi applicabili, AliExpress.com si riserva inoltre il diritto di: (i) sospendere temporaneamente le funzionalità di transazione del suo account con AliExpress.com per un determinato periodo deciso da AliExpress.com, o di interrompere permanentemente l’utilizzo del suo account AliExpress.com, e/o (ii) autorizzare Alipay a sospendere temporaneamente le funzionalità di transazione del suo account Alipay per un determinato periodo deciso da AliExpress.com, o di interrompere permanentemente l’utilizzo del suo account Alipay, esimendosi da qualsiasi responsabilità per eventuali danni o perdite direttamente o indirettamente derivanti da tale sospensione o interruzione. AliExpress.com potrà anche pubblicare i rilievi, le penali e altri documenti riguardanti le violazioni sui Siti. In tali casi, AliExpress la informerà con una motivazione e le possibilità di ricorso.

  

La valutazione della violazione del presente Contratto può essere condotta sia con mezzi automatizzati che umani. Il primo livello di moderazione viene eseguito automaticamente da un sistema che rileva i contenuti collegati a contenuti illegali o vietati dalle Linee Guida della Community. Questi contenuti rilevati vengono automaticamente bloccati ed eliminati sui Siti. AliExpress impegna anche risorse umane per la moderazione sui propri Siti; i contenuti possono essere controllati manualmente dai reparti interessati di AliExpress.com, tra cui i team di Governance della piattaforma, di controllo dei rischi e/o del Servizio Clienti.

  

La valutazione tiene conto di varie circostanze, tra cui (i) il numero di elementi di contenuto palesemente illegale o il numero di avvisi o reclami infondati, presentati in un determinato lasso di tempo, (ii) la relativa proporzione rispetto al numero totale di informazioni fornite o di avvisi presentati in un determinato lasso di tempo, (iii) la gravità degli abusi, compresa la natura del contenuto illegale, e delle sue conseguenze, (iv) e quando è sufficientemente evidente l'intenzione del destinatario.

  

Prima di prendere una decisione definitiva sul rifiuto/rifiuto o sull'annullamento della Transazione Online o sull'imposizione di altre restrizioni all'uso dei Siti, AliExpress.com può richiedere ulteriori verifiche o informazioni per qualsiasi Transazione Online, e Lei accetta di fornire tali verifiche e informazioni ad AliExpress.com su richiesta.

AliExpress.com può, al ricevimento di un ordine di agire contro uno o più elementi specifici di contenuto illegale, emesso dalle autorità giudiziarie o amministrative competenti, sospendere o annullare le sue Transazioni Online. In tali casi AliExpress la informerà con una descrizione dell'ambito territoriale dell'ordine, una motivazione e le possibilità di ricorso, a meno che ciò non sia vietato dall'autorità giudiziaria o amministrativa competente.

  

**5.3 Obblighi di pagare le Tasse.** Lei sarà l’unico soggetto responsabile per il pagamento di eventuali tasse (come IVA), dazi o altre imposte governative o di eventuali oneri o tariffe che potrebbero essere imposti sui prodotti o servizi acquistati ai sensi delle Transazioni Online o in relazione alle stesse, salvo laddove AliExpress.com sia obbligata a riscuotere direttamente le imposte sulle vendite da Lei effettuate tramite AliExpress.com a norma delle leggi e dei regolamenti applicabili. A scanso di equivoci, le ricordiamo prima di inoltrare l'ordine la possibile esistenza di tasse e altri costi aggiuntivi che non possono essere ragionevolmente calcolati in anticipo ma che potrebbero essere a suo carico (ad esempio, dazi d'importazione, tasse e spese di gestione per prodotti importati nell'UE da paesi terzi).

  

**5.4 Sistema di Feedback.** Lei si impegna a non compromettere l’integrità del sistema di feedback di AliExpress.com, ad esempio fornendo un feedback positivo su di se sui Siti mediante l’utilizzo di identità di Membro secondarie o tramite terzi, minacciando di dare un feedback negativo o valutazioni poco dettagliate del venditore per ottenere qualcosa che non sia incluso nell’elenco dei prodotti originale, fornendo un feedback negativo infondato su un altro membro sui Siti, oppure abusando del Sistema di Feedback in modo da inserire un commento negativo in una valutazione positiva. Lei è informato che le procedure di moderazione di AliExpress per le violazioni e le restrizioni che possiamo imporre all'uso dei Siti descritte nella clausola 5.3. si applicano anche al sistema di feedback.

  

AliExpress.com può, al ricevimento di un ordine di agire contro uno o più elementi specifici di contenuto illegale, emesso dalle autorità giudiziarie o amministrative competenti, cancellare il suo feedback. In tali casi AliExpress la informerà con una descrizione dell'ambito territoriale dell'ordine, una motivazione e le possibilità di ricorso, a meno che ciò non sia vietato dall'autorità giudiziaria o amministrativa competente.

  

**5.5 Indennizzo da parte del Membro.** Lei accetta di indennizzare AliExpress.com nonché le sue affiliate e i rispettivi dipendenti, amministratori, funzionari, agenti e rappresentanti e di manlevare gli stessi da qualunque perdita, danno, azione, pretesa e responsabilità (compreso l’integrale indennizzo delle spese legali) che potrebbe derivare, direttamente o indirettamente, dall’utilizzo da Lei fatto dei Servizi di Transazione o da una sua violazione del presente Contratto. Se in qualsiasi momento AliExpress.com determina che una richiesta di risarcimento indennizzabile possa comportarle effetti negativi, AliExpress.com potrà assumere la difesa esclusiva e il controllo riguardo a qualsiasi questione altrimenti soggetta a indennizzo da parte sua, nel qual caso Lei sarà tenuto a collaborare con AliExpress.com nel far valere ogni possibile difesa.

  

**5.6 Raccolta e Utilizzo delle Informazioni per i Servizi di Transizione.** Se Lei ha fatto domanda per i Servizi di Transizione di AliExpress.com e/o ha utilizzato gli stessi, Lei prende atto e conviene che AliExpress.com, ai fini della prestazione dei Servizi di Transazione, avrà il diritto di accedere ai suoi dati personali. AliExpress.com tratterà i dati personali, le informazioni creditizie, le informazioni commerciali e le sue informazioni finanziarie (le "Informazioni Raccolte") per facilitare l'amministrazione, l'elaborazione e il funzionamento dei Servizi di Transazione da Lei utilizzati. Il trattamento dei suddetti dati personali è ritenuto necessario ed è legittimato dalle seguenti basi giuridiche: (i) l'esecuzione e il monitoraggio del rapporto contrattuale tra Lei e AliExpress.com, e (ii) l'adempimento degli obblighi previsti dal presente Contratto. In relazione al suo utilizzo dei Servizi di Transizione, AliExpress.com potrà utilizzare le Informazioni Raccolte secondo le modalità di cui alla Politica sulla Privacy e/o all’informativa sulla raccolta delle informazioni personali relative ai Servizi di Transizione che avete accettato prima della o durante la domanda per i Servizi di Transizione e il suo utilizzo degli stessi.

**6.** **Sistema di raccomandazione**

I nostri Siti utilizzano un sistema di raccomandazione. Le informazioni visualizzate su tutte le pagine Web contenenti consigli personalizzati e/o annunci personalizzati sono selezionate da questo sistema di raccom ndazione. Il sistema di raccomandazione ci aiuta a determinare quali contenuti e prodotti promuovere selezionando contenuti e prodotti che hanno maggiori probabilità di essere interessanti e pertinenti per te. Per fare ciò, utilizziamo le seguenti informazioni su di te: il tuo indirizzo IP e/o il paese di spedizione indicato (o la destinazione di spedizione più specifica scelta), la cronologia delle ricerche e la cronologia di navigazione durante la visita o l'utilizzo della piattaforma AliExpress, informazioni sul tuo comportamento di acquisto sulla piattaforma (clicca, aggiungi al carrello, Aggiungi alla lista dei desideri, ai prodotti acquistati e alle informazioni del tuo profilo membro (come età, città, interessi, importo acquistato). Oltre alle informazioni personalizzate, utilizziamo anche il profilo dell'articolo (come immagine articolo, ID articolo) e le statistiche degli articoli (esposizione, clic, carrello, desiderio, tasso di clic, tasso di conversione, volume lordo della merce, prezzo, logistica) e informazioni sul contesto (tempo, rete, informazioni sui clienti) Come i parametri principali. Sulla base delle informazioni di cui sopra, estrarremo le tue preferenze per consigliare prodotti o servizi che potrebbero interessarti. Quando utilizzi per la prima volta i Siti e non sei già connesso, ti verrà chiesto di concedere ad AliExpress il diritto di utilizzare i cookie per il nostro sistema di raccomandazione. Se rifiuti, AliExpress non utilizzerà i tuoi dati di comportamento e non ti fornirà consigli personalizzati. Se hai effettuato l'accesso e desideri scegliere quali prodotti o pubblicità dovrebbero (o non dovrebbero) consigliare AliExpress, puoi modificare o disabilitare i parametri principali sopra descritti accedendo alle tue "Impostazioni Privacy" nel "Centro acquirenti". Se non c' è un indirizzo elencato sul tuo profilo, assegneremo un luogo di spedizione alla tua visita in base al sottodominio della lingua a cui hai avuto accesso, al tuo indirizzo IP o alla tua cronologia di navigazione precedente, quando disponibile. Quindi mostreremo gli elenchi di prodotti che possono essere inviati a questa posizione e i prezzi corrispondenti. È possibile regolare le impostazioni della posizione modificando manualmente l'indirizzo di spedizione in qualsiasi momento facendo clic sull'icona "Flag" nell'angolo in alto a destra del sito Web ufficiale o nelle impostazioni personali nell'applicazione. Utilizzando la funzione di ricerca o sfogliando determinate pagine dei canali (come le categorie di prodotti), gli utenti possono modificare i criteri utilizzati per classificare i prodotti, scegliendo tra una delle diverse opzioni, in particolare: -"Better Match": Maggiore rilevanza, determinata principalmente da un algoritmo che tiene conto di diversi fattori che possono cambiare nel tempo. I fattori in questione riguardano i seguenti quattro aspetti: La qualità della descrizione del prodotto (ovvero, migliore è la qualità della descrizione del prodotto, maggiore è la sua classificazione nel risultato della ricerca). La rilevanza dei prodotti rilevanti alla luce delle parole chiave incluse nelle richieste di ricerca degli acquirenti (vale a dire, più la descrizione include le parole chiave inserite dall'utente nella query di ricerca, più il prodotto finirà nei risultati di ricerca). Il tasso di conversione dei prodotti pertinenti (cioè, i prodotti venduti più frequentemente sono classificati più in alto). Servibilità (cioè, i prodotti che hanno maggiori probabilità di essere consegnati rapidamente sono elencati sopra nei risultati). ----"Ordini": in base al volume degli ordini, dai prodotti più ordinati al minimo per quella query di ricerca. -"Prezzo": dal prezzo più alto a quello più basso, o dal più basso al più alto. Puoi ordinare i prodotti in base alle migliori corrispondenze, ordini e prezzi, nonché selezionare criteri di ricerca aggiuntivi nei filtri proposti; questa funzione non si basa sul tuo profilo. Possiamo presentare i prodotti promossi nella pagina dei risultati di ricerca, ad es. Prodotti che i venditori pagano per visualizzare in determinati risultati di ricerca. In tal caso, i risultati della ricerca a pagamento sono presentati in modo differenziato rispetto agli altri risultati mostrati (in particolare, indicando **“Annuncio”**).

  

Puoi visitare il nostro portale [Transparency Center](https://www.aliexpress.com/p/transparencycenter/index.html?spm=a2g0o.best.sitefooter.7.50242c25r9hSWJ&_gl=1*a9fhfx*_gcl_aw*R0NMLjE3MzI4NTE0MzYuRUFJYUlRb2JDaE1Jc1pTanhlUHdoQU1WY1ZVUEFoMU5QZ3lwRUFBWUFTQUFFZ0ktQnZEX0J3RQ..*_gcl_au*NTcwMDkyMjgzLjE3Mjg5NzA4NjM.*_ga*MTMyNDQ3MjI2My4xNzA1MzY4OTM4*_ga_VED1YSGNC7*MTczMjg1MTQzNS4zNTUuMC4xNzMyODUxNDM1LjYwLjAuMA..) per una descrizione più dettagliata del nostro sistema di raccomandazione nella sezione **"politiche"**.

  

**7.** **Informazioni sulla conformità dei prodotti**

Qualora AliExpress venga a conoscenza del fatto che un prodotto illegale le è stato offerto da un venditore, AliExpress la informerà del fatto che il prodotto è illegale, dell'identità del venditore e dei mezzi di ricorso. AliExpress fornirà tali informazioni solo nella misura in cui dispone dei suoi dati di contatto per i prodotti acquistati almeno sei mesi prima di venire a conoscenza dell'illegalità. Qualora AliExpress non disponga dei suoi dati di contatto, le informazioni saranno rese pubbliche.

**8.** **Pubblicità**

AliExpress le mette a disposizione un archivio di pubblicità online sui Siti disponibile [qui](https://www.aliexpress.com/p/ad-search-page/index.html) per tutto il periodo in cui viene presentata una pubblicità e fino a un anno dopo che la pubblicità è stata presentata per l'ultima volta sui Siti.

  

**9\. Riservatezza e Protezione dei dati personali**

**9.1 Obblighi di Riservatezza.** Lei dovrà mantenere riservate tutte le informazioni riservate fornite da altri Membri dei Siti, da AliExpress.com o da qualsivoglia delle sue affiliate in relazione a qualsiasi Transazione Online o ai Servizi di Transazione, salvo e impregiudicato l’esercizio dei diritti a Lei spettanti a norma delle leggi e dei regolamenti applicabili.

  

**9.2 Informazioni Riservate.** Tutte le informazioni e tutti i materiali forniti da un altro Membro dei Siti AliExpress.com, da AliExpress.com o da qualsivoglia delle sue affiliate saranno considerate informazioni riservate a meno che tali informazioni o materiali siano già di dominio pubblico o che lo diventino successivamente per cause diverse da una violazione degli obblighi di riservatezza imputabile a Lei.

  

**9.3 Protezione dei dati personali e della privacy.** La preghiamo di leggere attentamente la Politica sulla Privacy dei Siti che disciplina la protezione e l’utilizzo dei dati personali di acquirenti UE/SEE/UK e venditori in possesso di AliExpress.com e delle sue affiliate e illustrano come viene effettuato il trattamento dei suoi dati personali in relazione ai Servizi di Transazione e alla Transazione Online. La Politica sulla Privacy è disponibile su [Politica sulla Privacy di AliExpress.com](https://service.aliexpress.com/page/knowledge?pageId=37&category=1000022028&knowledge=1060015216&language=en).

  

**9.4 Ruolo dei venditori nella protezione dei dati personali e della privacy.** Gli acquirenti UE/SEE/UK sono informati del fatto che i venditori hanno i propri obblighi di conformità alle leggi applicabili in materia di protezione dei dati e della privacy rispetto ai dati personali degli acquirenti UE/SEE/UK o di altre persone che siano oggetto di trattamento da parte del venditore. Sebbene AliExpress.com imponga per contratto ai venditori di garantire ad AliExpress.com, in relazione alla protezione di tali dati personali, la piena conformità attuale e futura di detti venditori a tutte le leggi applicabili in materia di protezione dei dati e della privacy, tra cui, a titolo esemplificativo, la conservazione dei propri registri di tali dati personali indipendentemente dalla piattaforma (nei limiti richiesti a norma delle leggi applicabili), il rispetto delle leggi in materia di marketing diretto e l’obbligo di rispondere alle richieste degli interessati per l’esercizio dei propri diritti a norma di tali leggi, né AliExpress.com né alcuna delle sue affiliate sono responsabili dell'adempimento da parte di un venditore dei propri obblighi previsti a norma delle leggi applicabili in materia di protezione dei dati e della privacy. A scanso di equivoci, in relazione a tali dati personali, la responsabilità e gli obblighi di AliExpress.com e delle sue affiliate a norma delle leggi applicabili in materia di protezione dei dati e della privacy sono completamente indipendenti e separati dalla responsabilità e dagli obblighi dei venditori a norma di tali leggi.

  

**10\. Clausola di Esonero e Limitazione di Responsabilità**

**10.1 Esclusione di Garanzia.** Lei conviene espressamente che l’utilizzo da Lei fatto dei Servizi di Transazione è a suo rischio esclusivo. Nella misura massima consentita dalla legge applicabile, i Servizi di Transazione vengono forniti “così come sono”, “come disponibili” e “con tutti i difetti”, e AliExpress.com non rilascia alcuna dichiarazione o garanzia sul fatto che la fornitura dei Servizi di Transazione avvenga ininterrottamente, tempestivamente o sia esente da errori. AliExpress.com non rilascia alcuna dichiarazione o garanzia in merito all’accuratezza, alla veridicità e alla completezza delle informazioni fornite da qualsiasi Membro dei Siti in conformità alle leggi e ai regolamenti sul commercio elettronico.

  

**10.2 Esclusione e Limitazione di Responsabilità.** NELLA MISURA MASSIMA CONSENTITA DALLA LEGGE APPLICABILE, ALIEXPRESS.COM, INCLUSE LE SUE AFFILIATE, NON SARÀ RESPONSABILE IN RELAZIONE AL PRESENTE CONTRATTO PER LA PERDITA DI PROFITTI O DI OPPORTUNITÀ COMMERCIALI, LA PERDITA DI REPUTAZIONE (AD ES. PER DICHIARAZIONI OFFENSIVE O DIFFAMATORIE), PER LA PERDITA DI DATI (AD ES. TEMPO DI INATTIVITÀ O PERDITA, UTILIZZO E MODIFICA NON AUTORIZZATI DI INFORMAZIONI O CONTENUTI) O PER QUALSIASI ALTRO DANNO INDIRETTO, INCIDENTALE, CONSEQUENZIALE, SPECIALE O PUNITIVO A LEI DERIVANTE.

  

ALIEXPRESS.COM E LE SUE AFFILIATE NON SARANNO RESPONSABILI NEI CONFRONTI DI LEI IN RELAZIONE AL PRESENTE CONTRATTO PER QUALSIASI IMPORTO SUPERIORE A (A) IL TOTALE DELLE TARIFFE PAGATE O DA PAGARE DA LEI A FAVORE DI ALIEXPRESS.COM PER I SERVIZI DURANTE IL PERIODO DI VALIDITÀ DEL PRESENTE CONTRATTO, SE DEL CASO, O (B) ALLA SOMMA DI 1000 USD.

  

Alcune di queste limitazioni o esclusioni potrebbero non essere applicabili a Lei se il suo stato, la sua provincia o il suo paese non consente la limitazione o l’esclusione di responsabilità per danni incidentali o consequenziali, per cui le esclusioni o limitazioni di cui sopra potrebbero non essere applicabili a Lei. Lei potrebbe anche godere di altri diritti a norma della legge locale vigente nel suo stato, nella sua provincia o nel suo paese che variano da stato a stato. Nessuna disposizione del presente contratto è intesa a pregiudicare tali diritti tali diritti nel caso in cui siano applicabili a Lei.

  

**10.3 Forza maggiore.** In nessun caso AliExpress.com né le sue affiliate e i suoi agenti potranno essere ritenuti responsabili per qualsiasi ritardo o errore o interruzione dei Servizi di Transazione derivante direttamente o indirettamente da eventi naturali, forze o cause al di là del ragionevole controllo di AliExpress.com, tra cui, a titolo esemplificativo e non limitativo, nei limiti consentiti dalle leggi e dai regolamenti applicabili, cause di forza maggiore, malfunzionamenti di internet, malfunzionamenti di computer, telecomunicazioni o altre attrezzature, guasti di alimentazione elettrica, scioperi, vertenze di lavoro, rivolte, insurrezioni, disordini civili, carenza di manodopera o di materiali, incendi, inondazioni, tempeste, esplosioni, cause di forza maggiore, guerre, azioni governative, ordini di giudici o tribunali nazionali o stranieri, ovvero inadempimento di terzi o sospensione o interruzione dei trasporti o delle operazioni commerciali (fra cui, a titolo esemplificativo ma non limitativo, ritardi o interruzioni della ripresa dei lavori o delle operazioni per ordine di agenzie governative) in caso di diffusione epidemica o pandemica a livello nazionale o regionale.

  

**11\. Comunicazioni e notifiche**

**11.1** **Comunicazioni di contenuto illecito**

**11.1.1.** **Meccanismo delle comunicazioni.** Lei può segnalare informazioni specifiche che si considerano contenuti illegali facendo clic su "**Segnala articolo**" nella pagina dell'elenco dei prodotti o contattando il nostro servizio clienti tramite il "**Centro assistenza**". La comunicazione deve includere quanto segue:

(a) una spiegazione sufficientemente motivata delle ragioni per cui si ritiene che le informazioni in questione siano contenuti illegali;

(b) una chiara indicazione dell'esatta ubicazione elettronica di tali informazioni (come l'URL o gli URL esatti e, se necessario, informazioni aggiuntive che consentano di identificare il contenuto illegale);

(c) il suo nome e indirizzo e-mail, tranne nel caso di informazioni che si ritiene comportino uno dei reati di cui agli articoli da 3 a 7 della direttiva 2011/93/UE (abuso e sfruttamento sessuale dei minori e pornografia infantile);

(d) una dichiarazione che confermi la sua convinzione in buona fede che le informazioni e le affermazioni contenute nel documento sono accurate e complete.

**11.1.2.** **Azioni intraprese in seguito alle comunicazioni.** Qualora la sua comunicazione contenga informazioni di contatto elettroniche, AliExpress invierà una conferma di ricezione della comunicazione. Dopo aver esaminato la sua comunicazione, AliExpress le comunicherà la sua decisione in merito alle informazioni a cui la comunicazione si riferisce e le fornirà informazioni sulle possibilità di ricorso.

**11.1.3.** **Sistema di gestione dei reclami.** Se Lei ha inviato una comunicazione, può presentare un reclamo entro 6 mesi dalla data in cui è stato informato della decisione di AliExpress a seguito di tale comunicazione. Può presentare il suo reclamo gratuitamente, utilizzando il modulo accessibile presso il Centro messaggi. AliExpress la informerà della sua decisione motivata in merito alle informazioni a cui il reclamo si riferisce e della possibilità di una risoluzione extragiudiziale della controversia o di altri meccanismi di ricorso. LEI È INFORMATO DEL FATTO CHE SE PRESENTA FREQUENTEMENTE AVVISI O RECLAMI MANIFESTAMENTE INFONDATI ALIEXPRESS PUÒ SOSPENDERE, DOPO AVERNE DATO AVVISO TRAMITE UN POP UP, L'ELABORAZIONE DEI SUOI AVVISI E RECLAMI.

**11.1.4.** **Risoluzione extragiudiziale certificata delle controversie**. Nel caso in cui Lei abbia presentato un avviso o un reclamo, può scegliere un qualsiasi organismo certificato di risoluzione extragiudiziale delle controversie per risolvere le controversie relative alle decisioni di AliExpress a seguito di una comunicazione o di un reclamo. In tal caso, Lei e AliExpress si impegneranno, in buona fede, con l'organismo certificato di risoluzione extragiudiziale delle controversie selezionato al fine di risolvere la controversia.

**11.2.**  **Note legali**

Salvo quanto diversamente specificato, le comunicazioni legali le saranno notificate tramite l’invio di comunicazioni all’indirizzo di posta elettronica nel suo più recente profilo di membro sui Siti. Una comunicazione sarà considerata come notificata 24 ore dopo l’invio del messaggio di posta elettronica, a meno che non ci venga comunicato che l’indirizzo di posta elettronica non è valido. In alternativa, possiamo notificarle le comunicazioni legali per posta al recapito nel suo più recente profilo di membro, nel qual caso la comunicazione sarà considerata come notificata trentasei (36) giorni dopo la data di spedizione. Le comunicazioni dovranno essere notificate ad AliExpress.com all’indirizzo riportato nella clausola 1.2 di cui sopra o a mezzo corriere o lettera raccomandata al seguente indirizzo: 26/F, Tower One, Times Square, 1 Matheson Street, Causeway Bay, Hong Kong, all’attenzione dell’Ufficio Legale.

  

**11.3 Sistema di gestione dei reclami**

  

SEI INFORMATO CHE SE SI PRESENTANO FREQUENTAMENTE AVVISI O RECLAMI CHE SONO MANIFESTIAMENTE INFONDATI ALIEXPRESS PU SOSPENDERE LA TUA FUNZIONE O CONTO PER UN PERIODO DI TEMPO, DOPO L'EMISSIONE DI UN AVVISO PRIORE TRASFERIMENTO ORIENTAMENTI COMUNITARI ALIEXPRESS O ALTRE NORME APPLICABILI.

  

**12.** **Contatti**

  

Lei può contattarci tramite chat online per qualsiasi richiesta utilizzando il nostro "Centro assistenza" nella sezione “Assistenza”. Può fare clic sulla funzione di messaggistica e accedere a un servizio di chat online.

**13.** **Legge applicabile e risoluzione delle controversie**

  

**13.1.** **Legge applicabile**

Qualsiasi disputa, controversia o reclamo tra Lei e AliExpress.com, o quelli derivanti da o relativi al presente Contratto, o l'interpretazione, la violazione, la risoluzione o la validità dello stesso, o qualsiasi controversia relativa a obblighi extracontrattuali derivanti da o relativi al presente Contratto (di seguito, collettivamente, "controversie") saranno disciplinati e interpretati in tutti gli aspetti dalle leggi della Regione Amministrativa Speciale di Hong Kong della Repubblica Popolare Cinese ("Hong Kong").  L'applicazione della Convenzione delle Nazioni Unite sui contratti di vendita internazionale di merci è espressamente esclusa. Tale scelta della legge applicabile non la priva della protezione offerta dalle disposizioni obbligatorie delle leggi dell'UE/UK e del paese di residenza abituale, il che significa che Lei ha diritto a qualsiasi diritto pertinente e al suo esercizio ai sensi delle leggi dell'UE/UK e delle leggi del paese di residenza abituale.

**13.2.** **Risoluzione delle controversie**

  

**La presente clausola si applica salvi e impregiudicati i diritti a** **Lei** **spettanti in qualità di consumatore dell’Unione Europea/Spazio Economico Europeo/Regno Unito a norma dell’articolo 18 del Regolamento Bruxelles I bis (GUUE 2012 l351/1) o delle normative applicabili nel Regno Unito, il che significa che** **Lei** **può avanzare una pretesa sulla base dei suoi diritti legali nel paese UE/SEE/UK in cui** **Lei** **vive, generalmente nei tribunali del luogo in cui** **Lei** **ha il suo domicilio.**

**13.2.1. Controversie tra Lei e un venditore.**

**13.2.1.1. Negoziazioni amichevoli.** Nel caso in cui sorga una controversia tra Lei e un Venditore, la controversia dovrà prima essere risolta attraverso una negoziazione amichevole tra Lei e il Venditore attraverso il nostro "Sistema di risoluzione delle controversie del servizio clienti" (dalla pagina dell'ordine all'interno del "Centro acquirenti" o attraverso il portale "Servizio clienti" all'indirizzo https://sale.aliexpress.com/i18n\_help\_center.htm) (il "Sistema di risoluzione delle controversie della piattaforma"), entro i periodi di tempo prescritti e in conformità con le norme e i regolamenti stabiliti per il Sistema di risoluzione delle controversie della piattaforma, che sono pienamente incorporati per riferimento nel presente documento e ne fanno parte come se fossero esposti in dettaglio.

  

**13.2.1.2. Presentazione di una controversia ad AliExpress.com.** Se la controversia non può essere risolta attraverso il nostro Sistema di risoluzione delle controversie della Piattaforma, Lei può sottoporre la controversia a noi, che avremo il potere di prendere una decisione in merito a tale controversia o di delegare o subappaltare tale potere a un'altra parte per la decisione. Al ricevimento di una controversia, avremo il diritto di basarci sui nostri registri e di richiedere a a Lei o al venditore o a entrambi di fornire documenti di supporto. Avremo la facoltà, agendo ragionevolmente, di rifiutare o ricevere qualsiasi documento di supporto. Alibaba Singapore non è un'istituzione giudiziaria o arbitrale e prenderà le decisioni solo come una persona comune non professionista. Inoltre, non garantiamo che i documenti di supporto presentati dalle parti della controversia siano veritieri, completi o accurati. A scanso di equivoci, Alibaba Singapore e le nostre affiliate non saranno responsabili di alcun materiale non veritiero o fuorviante, e Alibaba Singapore (e i nostri agenti, affiliate, direttori, funzionari e dipendenti) saranno esonerati e indennizzati da tutte le rivendicazioni, le richieste, le azioni, i procedimenti, i costi, le spese e i danni (compresi, senza limitazioni, i danni effettivi, speciali, incidentali o consequenziali) derivanti da o in relazione a tale controversia.

**13.2.1.3.**  **Presentazione di una controversia all’arbitrato****.** Se Lei non è soddisfatto della nostra decisione e salvo quanto diversamente stabilito dalla legge applicabile, dovrà rivolgersi al Centro Internazionale di Arbitrato di Hong Kong ("HKIAC") per l'arbitrato e notificare ad Alibaba Singapore tale richiesta entro 20 giorni di calendario dalla data di ricevimento della nostra decisione. Tale scelta non la priva della protezione offerta dalle disposizioni obbligatorie delle leggi dell'UE e del paese in cui risiede abitualmente, il che significa che Lei ha diritto a qualsiasi diritto pertinente e al suo esercizio ai sensi delle leggi dell'UE e delle leggi del paese in cui risiede abitualmente, compreso il diritto di rivolgersi a un organismo certificato di risoluzione extragiudiziale delle controversie per la risoluzione non vincolante delle stesse. Se Lei non richiede l'arbitrato entro 20 giorni di calendario, si riterrà che abbia accettato che la nostra decisione sia definitiva e vincolante nei suoi confronti. Con una determinazione definitiva, nel caso in cui la Transazione online adotti i Servizi Alipay, possiamo dare istruzioni ad Alipay di disporre dei fondi detenuti da Alipay in base a tale determinazione, e nel caso in cui la Transazione online adotti i Servizi supplementari di Alibaba Singapore, possiamo disporre dei fondi da noi detenuti in base a tale determinazione. Inoltre, in questo caso si riterrà che Lei e il Venditore abbiate rinunciato a qualsiasi rivendicazione nei confronti di Alibaba Singapore, Alipay e dei nostri affiliati e agenti. Fermo restando quanto sopra, Lei può esercitare i Suoi diritti di consumatore dell'Unione Europea/Spazio Economico Europeo/Regno Unito ai sensi dell'articolo 18 del Regolamento Bruxelles I Bis (GU UE 2012 L351/1) o delle normative britanniche applicabili, e presentare un reclamo relativo ai Suoi diritti legali nel paese dell'Unione Europea/Spazio Economico Europeo/Regno Unito in cui risiede, generalmente presso i tribunali del luogo in cui è domiciliato. La Commissione europea gestisce anche una piattaforma online per la risoluzione delle controversie, consultabile qui: \[http://ec.europa.eu/odr\]([http://ec.europa.eu/odr](http://ec.europa.eu/odr)).

**13.2.2.****Controversie tra Lei e Alibaba Singapore**

Nel caso in cui sorga una controversia tra Lei e noi, compresa qualsiasi disputa, controversia o reclamo tra Lei e Alibaba Singapore, o quelle derivanti da o relative al presente Contratto, o all'interpretazione, alla violazione, alla risoluzione o alla validità dello stesso, o qualsiasi controversia relativa a obblighi extracontrattuali derivanti da o relativi al presente Contratto, se non risolta tra Lei e noi mediante trattative amichevoli, sarà deferita e risolta in via definitiva mediante arbitrato presso l'HKIAC, salvo quanto diversamente stabilito dalla legge applicabile e fatti salvi i diritti che Lei, in quanto consumatore dell'Unione Europea/Spazio Economico Europeo/Regno Unito, può vantare ai sensi dell'articolo 18 del Regolamento Bruxelles I Bis (GU UE 2012 l351/1) o delle normative britanniche applicabili, il che significa che Lei può presentare una richiesta di risarcimento per i propri diritti legali nel paese dell'Unione Europea/Spazio Economico Europeo/Regno Unito in cui vive, generalmente presso i tribunali della sua residenza abituale. A scanso di equivoci, la legge applicabile alla presente clausola arbitrale sarà quella di Hong Kong.

Lei ha inoltre diritto alla risoluzione delle controversie presso organismi certificati di risoluzione extragiudiziale delle controversie, ai sensi della clausola 11.2.3., qualora abbia presentato una comunicazione o un reclamo. 

**13.2.3.****Arbitrato dell’HKIAC**

  

Se una controversia viene sottoposta all'arbitrato dell'HKIAC, l'arbitrato sarà condotto in base al Regolamento arbitrale amministrato dall'HKIAC in vigore al momento della richiesta di arbitrato, come modificato dalla presente clausola.  Il collegio arbitrale sarà composto da un unico arbitro. Salvo diverso accordo tra le Parti, l'arbitrato sarà condotto in lingua inglese e avrà luogo a Hong Kong. L'arbitrato può essere condotto a distanza, ad esempio per telefono, online tramite software di videotelefonia, e/o esclusivamente sulla base di comunicazioni scritte, come specificato dalla parte che avvia l'arbitrato. Il lodo del tribunale arbitrale dell'HKIAC sarà definitivo e vincolante per tutte le parti interessate e le spese dell'arbitrato saranno a carico della parte soccombente, a meno che il lodo non stabilisca diversamente. Qualsiasi parte della controversia può rivolgersi a un tribunale della giurisdizione competente per l'esecuzione di tale lodo.

  

**13.2.4. Indennizzo**

Qualora Lei avviasse un procedimento legale contro  Alibaba Singapore o le sue affiliate in violazione della presente clausola, tra cui, quando Lei acconsente ad addivenire a una risoluzione della controversia con  Alibaba Singapore_**,**_ qualsiasi procedimento legale volto a contestare una determinazione di  Alibaba Singapore che è diventata vincolante nei suoi confronti secondo la presente clausola, Lei sarà tenuto a manlevare e tenere indenne  Alibaba Singapore e le sue affiliate nonché i rispettivi agenti, dipendenti, amministratori e funzionari da ogni reclamo, perdita, danno che dovessimo subire.

  

**13.2.5. Prescrizione**

In ogni caso, Lei non potrà avanzare alcuna pretesa nei confronti di AliExpress.com o delle sue affiliate ai sensi del presente Contratto dopo un anno dal verificarsi della questione che ha dato origine alla pretesa, salvo diversamente previsto dalla legge applicabile.

  

**13.2.6. Provvedimenti ingiuntivi**

Fatte salve le disposizioni di cui sopra, ciascuna parte potrà richiedere un provvedimento ingiuntivo o altro provvedimento equitativo nei confronti della controparte in qualsiasi tribunale competente (tra cui, ad esempio, i tribunali della sua residenza abituale) prima dell’arbitrato o durante lo stesso.

  

**14\. Disposizioni generali**

  

**14.1 Clausola Salvatoria**. Se una qualsiasi delle disposizioni del presente Accordo fosse ritenuta invalida o inapplicabile da un tribunale competente sul presente Contratto, Lei conviene con AliExpress.com che tale disposizione dovrà essere modificata dal tribunale per renderla valida ed applicabile tenendo in considerazione la volontà delle parti. Se il tribunale non è in grado di farlo, Lei conviene con AliExpress.com di richiedere al tribunale di rimuovere la disposizione invalida o inapplicabile e di preservare il resto del presente Contratto.

  

**14.2 Nessuna Rinuncia.** Anche nel caso in cui AliExpress.com non agisca per far adempiere qualsivoglia obbligazione del presente Contratto, ciò non implica comunque la rinuncia da parte di AliExpress.com a far valere il proprio diritto all’adempimento del presente Contratto.

  

**14.3 Cessione.** AliExpress.com avrà il diritto di cedere il presente Contratto (compresi tutti i propri diritti, titoli, benefici, interessi e obblighi e doveri ai sensi dello stesso) a qualsiasi delle sue affiliate e a qualsiasi successore o avente causa. AliExpress.com potrebbe anche delegare alcuni dei propri diritti e obblighi ai sensi del presente Contratto a degli appaltatori indipendenti o altri terzi, a condizione che tale delega non pregiudichi i diritti a Lei spettanti ai sensi del presente Contratto. 

  

Lei non potrà cedere, né in tutto né in parte, il presente Accordo a qualsiasi persona o entità.