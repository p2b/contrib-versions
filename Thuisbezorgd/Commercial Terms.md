Contents



General Terms and Conditions for Partners

Section A (Definitions) 1

Section B (General Terms) 5

Section C (Supplemental Terms – Payment Services) 20

Section D (Supplemental Terms – Delivery Services) 23

Section E (Supplemental Terms – Stampcards) 25



General Terms and Conditions for Partners



SECTION A. DEFINITIONS



Capitalised words in the Agreement have the following meanings:



Affiliate means:

(i) for you, any relative, spouse, subsidiary, holding company,

ultimate holding company or sister company of you; and

(ii) for Thuisbezorgd.nl, a person who is, from time to time, a

(direct or indirect) subsidiary or holding company of

Thuisbezorgd.nl, or is a subsidiary of Thuisbezorgd.nl’s (direct

or indirect) holding company, or is a party in which

Thuisbezorgd.nl’s (direct or indirect) holding company owns

30% (thirty percent) or more of the paid up share capital or

controls 30% (thirty percent) or more of the voting rights.

Agreement means the registration form or otherwise, the General Terms and Conditions

for Partners and, insofar as applicable, the Supplemental Terms as annexed

hereto.

Business Day means a weekday that is not a public holiday in the relevant operating

country of the Platform.

Card Order means an Order that is not a Cash Order.

Cash Order means an Order paid for by cash.

Chargeback means a fee charged to us by a financial institution (e.g. a Customer’s card

issuing bank or merchant acquirer) in relation to the reversal of a card

transaction.

Connection Method means ordering devices and or any software, program or application

provided which enables you to receive Orders.

Courier means a person working directly or indirectly for Thuisbezorgd.nl who

performs the delivery of an Order in the context of the Delivery Services.

Courier Waiting Time means, where we have agreed to procure the Delivery Services, a situation

where an Order is not ready to be picked up when the Courier arrives at the

Premises at the expected Pick-up Time.

Customer a natural person or legal entity who has used the Platform to place an

Order.

Data Protection

Legislation

means all laws, regulations and regulatory guidance containing rules for the

protection of individuals with regard to the Processing of Personal Data

including applicable national, federal, provincial or state legislation and/or



01.11.2024 1

binding regulations (e.g. GDPR) implementing or made pursuant to them, as

amended or updated from time to time and/or any successor legislation.

"data controller", "data processor", "data subject", "personal data",

"personal data breach", "processing", and "appropriate technical and

organisational measures" will be interpreted in accordance with the Data

Protection Legislation.

Delivery Distance means the distance between a Premisesand the Customer, determined solely

by us based on radius and polygon.



Delivery Services means a service provided by us to collect and deliver the Orders from a

Premisesto a Customer.

Delivery Time means, where you arrange delivery yourself, either the delivery time

indicated by you via the Connection Method (with a maximum of 60

minutes) or the delivery time priorly indicated by the Customer and

confirmed by you, by which the Order will be delivered to the Customer.

Fees means the commission fee, administration costs and, if applicable, other

fees specified on the registration form or otherwise confirmed by us in

writing or charged to you by us for the Services.

Force Majeure Event means an event beyond the reasonable control of either party including but

not limited to strikes, lock-outs or other industrial disputes (whether

involving the workforce of the party or a third party), failure of a material

utility service or transport network, act of God, war, riot, act of terrorism, civil

commotion, epidemic or pandemic, malicious damage by a third party,

compliance with any law or governmental order, rule, regulation or direction

by a third party, material accident, cyber-attacks, breakdown of plant or

machinery, fire, weather phenomena (e.g.: lightning, ice, flooding, heavy

snowfall) or capacity problems.

Foundation means Stichting Derdengelden Takeaway.com, a foundation incorporated

under Dutch law, established in Amsterdam at Piet Heinkade 61, 1019 GM

and registered in the Commercial Register of the Chamber of Commerce

under number 64593932.

Goods means the dishes, food and drinks, and any other menu items and products

offered by you.



Gross Order Value means the total amount charged by you to the Customer for an Order

including the value of the Goods, any delivery charges levied by you and

any applicable taxes. To be clear, where we provide Delivery Services, we

will levy the delivery charges to the Customer and such charges will not

form part of the Gross Order Value.

IPRs mean any and all intellectual property rights of any nature anywhere in the

world whether registered, unregistered, registrable or otherwise, including

any Trademark.

KYC Information means all documents and data required by Thuisbezorgd.nl in compliance

with know-your-customer obligations pursuant to the Dutch Money

Laundering and Terrorist Financing Prevention Act, the Dutch Sanctions Act

and the EU Directive on the prevention of the use of the financial system for

the purposes of money laundering or terrorist financing and the applicable

Sanctions List, all as amended from time to time.

Platform means any of the Thuisbezorgd.nl websites and its affiliated websites,

mobile applications and ordering platforms, including any website we may

create for you.



Order means an order for Goods placed by a Customer via the Platform.



01.11.2024 2

Partner Information means all information about you and your Premises, and includes KYC

Information, the information about your Goods, menu, address, opening

hours, delivery radius and contact details, and may, at our request, include

a complete set of records to trace the supply chain of all Goods and

services you provide.

Partner Hub means the online portal and any accompanying tools and services made

available by us to you for managing your business on the Platform.



Payment Partner means a Partner that receives the Payment Services.

Payment Services means collecting and securing on your behalf payments from Customers

and transferring these payments to you, Thuisbezorgd.nl or TP; as well as

the managing and processing of refunds on your behalf.

Personal Data means as defined in the Data Protection Legislation

Premises means the physical premises at which the Partneroperates, where Goods

are prepared pursuant to Orders placed by Customers via the Platform.

Preparation Time /

Pick-up Time

means, where we have agreed to procure the Delivery Services, either the

(i) preparation time indicated by you via the Connection Method (with a

maximum of 45 minutes)



or

(ii) the delivery time priorly indicated by the Customer minus 15

minutes and confirmed by you

by which the Order will be ready for pick-up (Pick-up Time) by a Courier.

Processing means as defined in the Data Protection Legislation. "Process" or

"Processed" shall be construed accordingly.

Same Price

Guarantee

means your guarantee that the prices, discounts and special offers you

provide in respect of Goods and services offered by you on the Platform

(including any delivery charges and minimum order values set by you) are

the same as Goods and services ordered via your own online channels. We

require you to do this for commercial reasons, and to ensure that the prices,

discounts and special offers are just as competitive on the Platform as they

are on your own online channels.

Sanction Lists means overviews of sanctioned countries, persons and entities and issued

by organisations such as the United Nations, European Union or by national

authorities in the EU member states in which TP provides the Payment

Services.

Security Measure means the physical, technical, organisational security measures necessary to

ensure the adequate protection of Personal Data, in compliance with Data

Protection Legislation

Sensitive Personal

Data

means Processing of personal data revealing racial or ethnic origin, political

opinions, religious or philosophical beliefs, or trade union membership, and

the processing of genetic data, biometric data for the purpose of uniquely

identifying a natural person, data concerning health or data concerning a

natural person’s sex life or sexual orientation; and as further defined in Data

Protection Legislation.

Services has the meaning given in Clause 2.1

Stampcard Program means the award program, which allows Customers to collect stamps

awarded by a participating Partner on a Stampcard. The Customer earns a

Stampcard Voucher (at the cost of a participating Partner) once the

Stampcard is full, which is redeemable when placing an Order with the

participating Partner.



01.11.2024 3

Stampcard Voucher means a Premises-specific voucher awarded to the Customer for a full

Stampcard, with a value of 10% of the Gross Order Value, minus delivery

costs, of the five Orders for which the Stamps were collected.

Statement means a statement of amounts owed between you and us relating to the

previous week (being Monday to Sunday inclusive).

Thuisbezorgd.nl means Takeaway.com European Operations B.V., a private company with

limited liability incorporated under Dutch law, having its registered office at

Piet Heinkade 61, 1019 GM Amsterdam and registered in the Commercial

Register of the Chamber of Commerce under number 69769753 acting for

itself and on behalf of any corporate entity or person that controls, are

controlled by or under the common control, directly or indirectly, with

Takeaway.com Group B.V.

Thuisbezorgd.nl

Administration

means the books and records of Thuisbezorgd.nl.



Tip means the amount paid by a Customer intended for the courier delivering

the Order.

TP means Takeaway.com Payments B.V., a private company with limited

liability incorporated under Dutch law, established in Amsterdam at Piet

Heinkade 61, 1019 GM and registered in the Commercial Register of the

Chamber of Commerce under number 67982778.

Trademark means the words “Thuisbezorgd.nl”, “Takeaway.com”, the house logo, and

any other registered or unregistered trademarks and logos used by us or

any Affiliates, separately and in combination (whether registered or not),

and references to “our Trademark” are references to the Trademark.



These General Terms and Conditions for Partners apply between you and Thuisbezorgd.nl (“us” or “we”). The

term “you” or “Payment Partner” means you individually or the entity you represent in accepting the

Agreement.



01.11.2024 4

SECTION B. GENERAL TERMS

1. INTERMEDIARY ROLE

1.1 We provide the Services to you, as a result of which legally binding contracts for the sale of Goods to

Customers will be concluded. Our role in the conclusion of these contracts is that of an intermediary

on your behalf; we are not a party thereto.

1.2 You recognize and acknowledge that Thuisbezorgd.nl also represents and acts on behalf of other

restaurants, stores or professionals that may be your direct competitors.



2. OUR OBLIGATIONS

2.1 We will provide to you:

(a) an order processing service which enables Customers to place Orders with you via the Platform;

(b) the Payment Services as specified in Section C (Supplemental Terms – Payment Services) for

all Card Orders made on the Platform;

(c) the Delivery Services as specified in Section D (Supplemental Terms – Delivery Services) where

we have agreed to procure the Delivery Services;

(d) the Stampcard Program as specified in Section E (Supplemental Terms – Stampcards) where

you have indicated to participate in the Stampcard Program;

(e) the Connection Method you choose to be installed at the Premises to enable you to receive

Orders; and

(f) other services as may be agreed from time to time,

(together, the “Services”)

2.2 We will do the following for you:

(a) provide reasonable training to enable you to use the Connection Method;

(b) provide you with access to support services;

(c) where appropriate, replace or repair any Connection Method as Thuisbezorgd.nl deems

necessary; and

(d) manage, process and collect any necessary refunds for Orders on your behalf which are to be

paid to Customers in accordance with our refund policy.

2.3 We will charge you the Fees that are specified in the registration form or otherwise.



3. MARKETING

3.1 In order to promote you, you agree that we will use your name, logo, Partner Information and other

intellectual property for the purpose of marketing activities during the Term, for example to:

(a) conduct search engine marketing to promote you on the Platform using your brand name or

other keywords relating to you and include you in promotional activities through several

marketing channels (physical and digital), including via (third-party) affiliated platforms (such as

search engine marketing), and will use your approved logo and Partner Information in these

activities;

(b) engage in search engine optimisation and display your brand name or menu in search results

(c) unless you advise us otherwise, create, register and promote a website using your brand, menu

and logo, and with a domain name using your brand. We will own that domain name and all

intellectual property rights related to that website (with the exception of your brand, menu and

logo) and link that website or any other online assets controlled by you (such as Google My

Business pages) to the Platform to enable customers to place Orders;

(d) include links to the Platform for search engine optimisation purposes, for example by facilitating

links from Google My Business pages and online maps to the Platform, noting that you have the



01.11.2024 5

ability to manage your preferences for the availability of such links in the online assets controlled

by you at all times, and you have the ability to request us to remove such links;

(e) provide you with various marketing collateral and guidelines on how you may use this collateral

to promote your business and your association with us.

From time to time, we may send you direct marketing communications relating to our products or services.

You can opt-out from this marketing at any time by unsubscribing from the email.



4. REVIEWS

We may display on the Platform ratings and comments ("Reviews") provided by Customers regarding you

or an Order. We will remove or edit Reviews where the reviews are, in our view,

unreasonably defamatory or otherwise objectionable. We will only do so in accordance with our guidelines

at (https://www.thuisbezorgd.nl/en/customerservice/topic/ratings-and-reviews) and applicable legislation.

You will not yourself post, cause or allow any other party to post any Reviews about yourself. If you do so

we reserve the right to remove such Reviews and suspend our Services to you as per Section 14.

When you respond to Reviews that are about you, your responses shall be in accordance with our

guidelines. We may suspend you with prior warning, if we detect your responses frequently contain illegal

content or that you frequently submit notices or complaints that are manifestly unfounded about Reviews

on the Platform.



5. RECOMMENDER SYSTEMS AND RANKING PARAMETERS

On our Platform, we use recommender systems to display information to Customers that will help

Customers discover relevant partners and offers. When listing our partners, we use ranking parameters on

the Platform, especially to determine the order and prominence in which partners and Goods appear in

search results in response to a Customer's search. In order to help you understand how they function, we

have set out details and a description of the recommender systems and the main ranking parameters that

we use at (https://www.thuisbezorgd.nl/en/customerservice/topic/restaurant-list) in detail and this

description also forms part of this Agreement.

You can influence your ranking on the Platform through the TopRank program under certain conditions.

The terms to the TopRank program can be found here

(https://s3.eu-west-1.amazonaws.com/toprank-notifications/toprank/terms-and-conditions/en.pdf).



6. CONNECTION METHODS

6.1 As part of the Service, we offer a connection between the Platform and you to pass on Orders via our

standard connection methods such as T-Connect or the hardware box terminal.

6.2 We may provide API connection methods connecting you with our servers to provide an alternative

connection method for processing Orders. Depending on the connection method used, separate terms

and fees may apply.

6.3 We will use best efforts to enable you to use the connection methods available and to maintain these

during the entire duration of the Agreement. We will, under normal circumstances, maintain such

connections. If, for whatever reason, we (temporarily or permanently) terminate or disable connection

methods used by you, we will inform you as soon as reasonably possible.

6.4 You agree that where these alternative connection methods are provided, they are provided upon your

request and they are provided as is without any warranties in terms of availability and connectivity and

are under development.

6.5 If you use a POS-system provider not provided by us, you take full responsibility for the POS-system

provider and guarantee that the POS-system provider shall not use any data processed via the

connection methods for its own purposes.

6.6 With regard to any data or information (specifically including order and customer data) provided to you

via the connection methods provided by us, we will retain all intellectual property rights and you are only

allowed to use this data for executing the Agreement.



01.11.2024 6

7. REGISTRATION TO THE PLATFORM

7.1 You can register to the Platform by submitting a completed registration form to us including all

specified documents and KYC Information. Any Agreement for the provision of the Services is

subject to Thuisbezorgd.nl having confirmed the receipt and approval of the KYC Information and

your registration.



7.2 We can refuse a request for the registration of a certain Premises at our sole discretion, for example,

when we do not provide Delivery Services in a certain area, or due to KYC issues.



7.3 You may only register a maximum of one (1) Premises on the Platform per address, regardless of

whether there are physically separated (operational) kitchens on the Premises.



7.4 By registering to the Platform, you guarantee that you are not bankrupt, nor under insolvency or

restructuring procedures, that no moratorium of payments has been granted and that you are not

subject to investigation or prosecution by any authority.



7.5 After the acceptance and complete processing of a registration, we will provide you with access to

the Partner Hub. In the Partner Hub, we will publish information relevant to you, such as the

processed Orders, Card Orders, Payments owed and invoices.



7.6 From time to time, we shall check all partners against the Sanctions Lists and will perform a client

due diligence assessment. If you or your owners are present on one of these lists or if, on the basis

of the client due diligence assessment, we otherwise determine that the services cannot be

performed, we may restrict, suspend, or terminate part of, the Services, or terminate this Agreement,

in accordance with Clause 14.1 or 14.3 (as applicable).



7.7 Access to the Partner Hub is personal and confidential. You must at all times keep your log-in

credentials and authentication methods confidential and secure and must only provide access to the

Partner Hub to your authorised employees or representatives. You are at all times responsible for

your use of the Partner Hub and any use of your accounts and the settings of your accounts on the

Partner Hub. Any actions on your Partner Hub account will be deemed by us to be authorised activity

on your behalf.



8. YOUR OBLIGATIONS

Obligations to Thuisbezorgd.nl

8.1 You must supply the PartnerInformation that we request, and you must ensure that the

PartnerInformation is always accurate, compliant with laws and kept up to date at all times, we

reserve the right to suspend our Services to you until you do so. If we detect that the Partner

Information provided contains illegal content such as non-compliant product information or

counterfeit goods, we may suspend our Services to you reasonably with prior warning. The

PartnerInformation you provide will be reproduced and displayed to Customers on the Platform as

per regulatory requirements.. We reserve the right to correct any obvious spelling or formatting errors

in the PartnerInformation being reproduced, but you retain full responsibility for ensuring the

accuracy of this PartnerInformation and for ensuring that it is up to date at all times.

8.2 Where appropriate and possible, you are authorized to make changes to certain parts of the

PartnerInformation and manage your listing on the Platform yourself. We do not undertake to check

and are not responsible or liable for checking PartnerInformation provided or changes made and you

remain at all times fully responsible and liable for any PartnerInformation provided or changes made,

the accuracy and completeness of PartnerInformation and compliance of PartnerInformation with all

applicable laws and regulations in this regard, including (but not limited to) any requirements related

to alcohol, food safety, allergens and additives and other requirements as documented in the

Agreement. Where it is not possible or authorized to make such changes yourself, you must

communicate changes to the PartnerInformation to us at least fourteen (14) days before they are

scheduled to take effect, so that we can process the amended PartnerInformation as displayed on

the Platform.



01.11.2024 7

8.3 You may not include any links to third party websites or advertisements on the Platform and will not

use SEA (Search Engine Advertising) and/or similar services using the Trademarks without our

explicit consent.

8.4 You must promptly provide us with accurate and complete details of any allergens in your Goods (for

example, identifying which items contain nuts, shellfish, etc.), nutritional information of the Goods (if

and as required by applicable law) and any other accurate and complete details of your Goods as

may be required by applicable law and regulations. We may also require you to provide further

information including the ingredient list for each menu item. We will include the allergen information

you provide on the Platform. You are solely responsible for ensuring that all the information including

but not limited to the allergen information you provide (both to us for inclusion on the Platform and

directly to any Customers or customer services contacting you to request details relating to the

Goods) is entirely accurate, complete and up to date in relation to your in-house item information or

food items being prepared at your Premises at that time and that the information complies with all

regulations and laws applicable to you and your business (including but not limited to food packaging

and labelling requirements and in relation to allergy information). Customers may be directed to

contact you (or we may contact you on behalf of Customers) with any questions regarding allergens.

We do not undertake to check and are not liable for checking any provided information on Goods

(including allergen information) on your behalf. You will promptly report in writing to us any errors in

the information displayed on the Platform or any changes to such information, including any allergy

information. You agree that you will solely be responsible for any discrepancies between the

information provided on the Platform and the information in your in-house information, and any

adverse effect resulting out of such discrepancies.

8.5 You must immediately inform us if any food inspection authority or any other authority establishes an

offence by you or your employees under applicable foodstuff legislation or any other legislation or

regulations.

8.6 If a Customer complains to us about an Order or your delivery and we give you details of the

complaint, you must respond to the Customer as soon as possible and act reasonably and cooperate

with that Customer and us to reach a prompt resolution.

8.7 You may not use any marketing materials containing logos/branding of our competitors in

connection with fulfilling any Orders.

8.8 If we have incurred costs as a result of a complaint about your Goods, services or conduct (including

where you have rejected an Order), you will be obliged to reimburse us in full for those costs.

8.9 The Connection Method software always remains our property, or that of our licensors. We may

upgrade or alter the Connection Method hardware or Connection Method software at any time, and

we may remotely access the Connection Method software at any time for any lawful reason, in

particular to update the software or to perform software maintenance. You are authorised to use this

software in accordance with any end-user licence which we give you notice of from time to time. You

must not reverse engineer, decompile, disassemble, reproduce or otherwise misuse the Connection

Method software.

8.10 You must keep the Connection Method in good working condition and return any physical device to

us at the end of this Agreement, unless you have purchased the device. If it is not returned, or if it is

damaged, we may charge you for the reasonable costs of repairing or replacing it. If the Connection

Method contains a SIM card, you must ensure that it is only used in connection with this Agreement.

We may invoice you for any costs incurred by us which we believe result from the SIM card being

used for any other reason.

8.11 During the Term, you must not:

(a) operate your business in a manner which is, harmful to our business, goodwill or reputation;



(b) engage in any act or omission which is harmful to our business, goodwill or reputation;

(c) do or say anything derogatory that might bring us into disrepute or adversely affect our

reputation; or



(d) behave in an indecent or unrespectful manner towards customers, our employees or suppliers.

If you are in breach of this Clause 8.11, we may restrict, suspend, or terminate part of, the Services,



01.11.2024 8

or terminate this Agreement, in accordance with Clause 14.1 or 14.3 (as applicable).

8.12 You are responsible for the quality of the Goods, and you will ensure that they are of good

merchantable quality, not expired and safe for consumption and/or use.

8.13 You may not, without our prior written approval which can be withheld, delayed or made subject to

conditions at our sole discretion at any time, list:

(a) any and all Goods which are, based on national or local laws and regulations, illegal to sell or

deliver (or facilitate the sale or delivery of) to a Customer, including (a) goods requiring a specific

licence, approval or registration for sale (including online sale) which you do not hold; and (b)

goods requiring a specific licence, approval or registration for transportation, carriage handling

or delivery;



(b) any and all Goods which are, based on national or local laws and regulations, illegal to sell or

deliver (or facilitate the sale or delivery of) to a Customer under a certain age;



(c) any and all Goods which are pharmaceuticals (including but not limited to any and all medicines

and medical devices), tobacco or smoking products; or



(d) any other potentially hazardous or unsafe items, or any other Goods indicated by us from time to

time in the Prohibited and Restricted Items Policy or any other product specific policy we may

provide you.

8.14 Any Partner which lists and sells Goods which require a licence (e.g. the sale of alcohol products, or

hot food or drinks between certain hours (as applicable)), or is subject to certain rules such as time

or volume restrictions, buyer verifications or age restrictions (as applicable), must ensure that they

hold the required licences or adhere to the said rules, and will provide to us such details on request.

We are not responsible for the compliance of any legal requirement or condition imposed on you by

way of specific, general, local, regional, national or any other laws and regulations.

8.15 To improve Customer experience and support you with managing the delivery of Orders, we provide

the Courier App, the Courier App Portal and any accompanying tools and services (together, the

‘’Courier App’’) as part of the Services provided by us to you. The Courier App is provided by us to

you where you manage your own delivery of Orders. The following terms:

https://takeaway-restaurant-portal.s3.eu-west-1.amazonaws.com/partnersTerms/courierApp/Terms-of

-Use-TK-CourierApp-EN.pdf shall apply to your use of the Courier App, which for clarity shall include

any employees and/or its couriers engaged or contracted by you, who use the Courier App.

Same Price Guarantee

8.16 During the term, you must apply the Same Price Guarantee. If you breach the Same Price

Guarantee, the price difference between the price as listed on the Platform and the price charged

outside the Platform is:

i. billed to you by us; and

ii. reimbursed by us to the Customer by providing a voucher.

You will immediately adjust the prices, discounts or otherwise on the Platform to match those on your

own website and menu.

Obligations to Customers



8.17 You agree to receive, process, prepare and package for delivery to the Customer in a timely manner,

using skill, care, diligence and quality consistent with best industry practices and all applicable laws,

rules, requirements and regulations.



8.18 The Goods included in the Order must correspond to what is stated in the Order as received by you

without error, and be in good and fair quality within the description of the Order. You will prepare,

handle and package the Goods ordered by Customers on the Premises and with all reasonable care

and skill, making sure that the Goods are:

(a) safe to eat;

(b) of the standard expected;



01.11.2024 9

(c) hygienically made, packaged, contained in such way that (e.g. appropriate temperature and

environmental thresholds, appropriate segregation of products) which does not compromise their

quality or safety in transit to the Customer, transported (if applicable) and stored;

(d) labelled correctly;

(e) made in accordance with additional reasonable instructions provided by the Customer (with the

Customer comments included in the Order);

(f) made with the correct ingredients as communicated to Customers; and

(g) packaged separately and readily identifiable in case of age restricted products.

8.19 You will ensure that potentially hazardous foods and perishable foods are properly prepared and

packaged for delivery to avoid cross-contamination (i.e. separating raw foods from cooked foods;

meat from non-meat, warm/hot food from cold/frozen food; non-allergen foods from allergen foods;

foods for people from pet foods; foods from household chemicals and personal care products; etc.)

and to maintain temperature controls during transportation.

8.20 You acknowledge and agree that once a Customer has placed an Order via the Platform, a contract

for the supply of Goods has been created between you and the Customer, and you must fulfil an

Order placed by a Customer in accordance with the Order details. If you do not prepare and deliver

(where you are responsible for the delivery) an Order you receive within a reasonable timeframe -

where delivery within a maximum of one hour from expiration of the Delivery Time is considered

reasonable - or if you otherwise fail to comply with your obligations under this Agreement, we may

take any reasonable action we see fit (including refunding the Customer on your behalf and at your

expense) in order to mitigate a negative customer experience.

8.21 You must use your best efforts to be available to accept, and in fact accept, all Orders received from

us and to be contacted via email and telephone during your opening hours. If you are unable to fulfil

Orders, you must change your status to ‘offline’ so Customers cannot place Orders. If you cannot

fulfil an Order, you must advise us as soon as possible so that we can inform the Customer. If you

cannot provide one or more menu items contained in an Order, you must inform the Customer within

ten (10) minutes after receiving the Order and offer the Customer a reasonable alternative.

Non-compliance with Clauses 8.17, 8.18, 8.19, 8.20 or 8.21 gives us the right to suspend provision

of Services.

8.22 If you list alcohol products on your menu, you must provide the volume and ‘alcohol by volume’

(ABV) of each alcohol product available on the Platform. Where the Order contains alcohol or any

other age-restricted Goods, you acknowledge that you are solely responsible for ensuring that the

Customer is over the relevant legal age for the purchase of any age-restricted Goods. You are

obliged to request the Customer to provide identification in accordance with applicable law upon

delivery or pick-up. If the Customer cannot identify himself adequately or does not meet the minimum

age requirements, you must refuse to deliver the relevant products to the Customer. When an age

restricted product is returned to you, you will accept return of the Good and log such return in

accordance with the local laws and regulations, if applicable.

8.23 You may cancel an Order if:

(a) the Customer has placed the Order with incorrect contact or address information;

(b) your Goods are no longer available and the Customer does not accept an alternative offered by

you; or

(c) in case of force majeure at your Premises.



If an Order is cancelled, you will notify us thereof within two (2) days. Cancellations will not be taken

into consideration after this period.

8.24 We reserve the right to introduce a tipping function to the Platform, which will give Customers the

option to tip couriers via the available online payment methods on the Platform. If the courier is

working for you (i.e. not for Thuisbezorgd.nl), we will transfer the Tip to you. You are obliged to

transfer any Tip received from us to the courier in question and indemnify us against any claims from

couriers and Customers arising from or in connection to this responsibility. To the extent applicable,

you are solely responsible for any (wage) tax implications relating to the payment of the Tip to the

courier.

8.25 You must provide the Customer with a receipt (and a tax invoice, if applicable) in respect of an Order,



01.11.2024 10

if you are asked to do so.

8.26 Following obligations also apply to you if you list or sell Goods that are not solely ready to consume

meals:

(a) you shall ensure that all Goods are adequately, properly and safely prepared and packaged for

delivery to avoid any damage to the Goods or inflicting personal injury, including ensuring safe

transport of potentially breakable items, sharp items, and preventing humidity that could harm the

Goods.

(b) you are solely responsible for the quality of your Goods, and to make sure they are safe to use,

they comply with any applicable conformity requirements under applicable law or additional

commercial guarantees provided by you, such as being free from defects and functioning

properly.

(c) you are also responsible for any processing and managing questions, aftercare, customer

service, and the settlement of other issues. We may refer Customers directly to you.

(d) you are solely responsible for complying with any applicable warranties, returns and revocation

responsibilities related to Goods provided on the Platform. It is up to you to accept and process

returns of Goods and collect these in accordance with applicable law and unless agreed

otherwise, we take no part in these procedures and cannot be held liable or responsible for any

damages or costs relating thereto.

(e) you shall notify us forthwith of product safety issues or non-compliance and shall prevent such

items from being placed within any Order as soon as you become aware of such.

(f) you shall solely determine whether Goods are to be recalled from your offering (a "Product

Recall"). In the event of a Product Recall, your shall send information regarding the Goods being

recalled (including the reason for non-compliance and / or safety risk and the actions to be taken

by the Customer; the affected items, including batch and / or serial numbers and contact

numbers to obtain further information) as soon as reasonably practicable to us including any

relevant guidance as to whether we must refund any Customers that have purchased the recalled

Goods or whether we must advise Customers to return the Goods back to you. If we refund any

relevant Customer (as decided by you at your sole discretion), we will charge any refund back to

you.

(g) where applicable, you will compensate us in full against any charges (including Chargebacks),

losses, damages or claims (and all related costs, (including legal fees), penalties, fines, interest,

expenses and other liabilities incurred by us in connection with:

a. actual or alleged issues with the quality of their Goods; and

b. any Product Recall, revocation, return or collection of Goods.

General

8.27 You must comply with, and ensure your employees’, agents’ and contractors’ compliance with, your

obligations under this Agreement.

8.28 You must follow any reasonable instructions we give you in relation to the performance of your

obligations under this Agreement, including the use of the Connection Method.



9. PAYMENTS FROM CUSTOMERS

Fees \& Invoicing

9.1 Where we receive payment from Customers for Card Orders, the payment received (the Gross Order

Value) less any outstanding Fees, plus any fees or charges charged by us to the Customer (in

respect of any Order), will be held on your behalf until it is payable to you in accordance with our

Payment Terms (Section C).

9.2 You will receive the payment from Customers for Cash Orders. Cash payments cannot be accepted

where we procure the Delivery Services.



9.3 Each week we will provide a combined Statement to you in the Partner Hub which includes:

(a) the aggregate Gross Order Value of all Orders, split between Cash Orders and Card

Orders;

(b) the Fees we are charging you. These may include amounts in relation to the Connection

Method, Delivery Services or other services provided to you, or any other amounts



01.11.2024 11

which we have given you due notice are chargeable to you in accordance with the

Agreement; and

(c) any balance brought forward from, and any amounts paid or received by us since the

date of the previous Statement.

9.4 We may charge each Customer who places an Order a service charge, as we see fit.

Payments

9.5 We will set off any amounts owed to us by you (including late payment interest and cost) against any

amount owed to you by us at any time. We may also withhold certain amounts, costs and expenses

from any amounts we hold on your behalf, such as: (i) any Chargebacks (and associated costs and

expenses); (ii) any other cost or expense which we incur or reasonably expect to incur as a result of

a breach by you of this Agreement, or as a result of any apparent fraudulent activity in relation to

your operations; and/or (iii) any amounts where we are, or expect to be, ordered to do so by a

competent authority. If we make any withholdings, we will do only for so long as is reasonable, and (if

applicable) we will make a credit to you in the next Statement after it becomes clear that we will not

incur any liability in respect of it. If we do incur loss or liability, we will retain the amount withheld to

the extent of the loss or liability.

9.6 If the aggregate Gross Order Value is less than what you owe us (including any amounts relating to

Commission Fees for Cash Orders) over the same period, we may stop accepting Cash Orders and

invoice any outstanding balance until you no longer owe us money. Further, we may at our

reasonable discretion suspend the acceptance of, or cease to accept, Cash Orders at any time. Any

invoices are payable by you within 15 (fifteen) days of the date of the Statement to the bank account

of Thuisbezorgd.nl, or an Affiliate of Thuisbezorgd.nl, details of which are specified in the Statement.

9.7 We may charge you monthly interest at 2% (two percent) above the Euribor rate on any unpaid

amount from the payment due date until the earlier of the date that the amount is set off or the

amount is received by us. You acknowledge that the late payment interest is a genuine estimate of

our loss caused by your late payment. You are also liable for any collection costs incurred by us in

connection with the recovery of any overdue payment, and for any costs that we have additionally

incurred to this end.



10\. CONFIDENTIAL INFORMATION

10.1 You and we (and our respective officers, employees, agents and advisers) (the "Receiving Party")

must keep in safe storage and not use or disclose for purposes not contemplated by this Agreement

each other's Confidential Information (the "Disclosing Party"), and the Confidential Information of

any Affiliate of the Disclosing Party.

For the purposes of this clause, "Confidential Information" means any information, data or material

which relates to the business or affairs of the Disclosing Party (or Affiliate or business contacts). To

be clear, Confidential Information includes:

(a) the Customers’ Personal Data;

(b) all data stored on the Platform or any information technology systems owned or operated by

Thuisbezorgd.nl relating to the Platform;

(c) the terms of this Agreement; and

(d) the functionality of the Connection Method’s hardware and software.

10.2 The restrictions in Clause 10.1 do not apply to:

(a) any disclosure by us to any of our Affiliates;

(b) any use or disclosure authorised by the Disclosing Party or by law;

(c) any information which is already in, or comes into, the public domain other than through the

Receiving Party’s unauthorised disclosure; or

(d) any Confidential Information which is required to be disclosed by law or order of a court,

provided that before making any disclosure, the Receiving Party will give written notice to the

Disclosing Party of the reasons for and nature of the disclosure, and will give the Disclosing



01.11.2024 12

Party a reasonable opportunity to consider the same and will, at the expense of the Receiving

Party, do all things the Disclosing Party may reasonably request.

10.3 This Clause 10 shall remain in full force and effect for a period of 5 (five) years after the termination

of this Agreement.



11\. INTELLECTUAL PROPERTY

11.1 You may not use our IPRs in relation to anything we have not given you express permission for. You

may not use our IPRs on packaging, clothing, stationery, vehicles etc, unless you have our prior

written permission. You may use items branded with our Trademark that we or our suppliers have

provided to you, but you may only use them in accordance with our instructions.

11.2 You may not use our IPRs in relation to any Premises that are not registered or active on the

Platform.

11.3 If someone else claims or we have any other reason to believe that your use of information or

designs on the Platform, including but not limited to logos, names, brand names, trademarks or any

other information or designs infringes any right from us or any third party, you agree to follow any

instruction we give you in relation to the use of this information or these designs. This might mean

we suspend the Services and/or you from the Platform with prior warning in accordance with Clause

14.1 until any dispute is settled.

11.4 During the Term, you must not do or say anything derogatory that might bring our Trademark,

business or brand into disrepute or adversely affect our reputation.

11.5 You must stop using our IPRs on our request as soon as this Agreement ends or the Services are

suspended. This means that you must stop all use of our IPRs including taking down all in-shop

references to our IPRs, and you must stop all on-line and print advertising connecting your business

to our platform and IPRs.

11.6 By entering into this Agreement with us, you confirm to us that you have the right to use your brand

name, logos and any other associated information and designs on the Platform, and that using your

brand name, logos and any other associated information and designs will not bring you into conflict

with anyone else. You grant us a royalty-free licence to use your name, logo and any other IPR

(including intellectual property in any photographs or PartnerInformation) and the right to sublicense

the same to third parties, to enable us to comply with our obligations under this Agreement, including

the marketing activities detailed in Clause 3 above and you confirm to us that you have the ability to

grant this licence to us. Your intellectual property will, however, at all times remain your property.

Your licence to us will end when this Agreement ends, with the exception of any website (including

domain) we may create for you or any marketing activities or other use which we have already

initiated or planned, and/or cannot reasonably be reversed or stopped, in which case the license will

continue to exist for the extent, and for as long as, reasonably needed.



12\. LIABILITY \& INSURANCE

12.1 Nothing in this Agreement will limit or exclude a party's liability for: (i) death or personal injury caused

by its own negligence, or the negligence of its employees, agents or contractors; (ii) fraud or

fraudulent misrepresentation; or (iii) breach of any term implied by any statute or any liability which

(in each case) cannot lawfully be limited or excluded.

12.2 Subject to Clause 12.1, we are not liable to you for: (i) any indirect damages, any loss of goodwill,

reputation, business, profits, data, actual or anticipated income or profits or loss of contract or any

indirect losses (loss is indirect if, at the time this Agreement was entered into, you and we knew it

might happen); or (ii) any damages, costs, direct losses (loss is direct if it is obvious that it may

happen), or indirect losses, which relate to faults, breakdowns or other interruptions to the ability of

Customers to place Orders for any reason.

12.3 Subject Clause 12.1, our total liability to you in respect of any losses arising in connection with this

Agreement howsoever caused is limited to an amount equal to an amount of money held by us on

your behalf as at the date of the event giving rise to the claim.

12.4 You will compensate us in full against any charges (including Chargebacks), losses, damages or



01.11.2024 13

claims (and all related costs, (including legal fees), penalties, interest, expenses and other liabilities

incurred by us in connection with a breach by you of this Agreement. In addition, you will

compensate us and our Affiliates in full against any losses, damages, or claims (and all related costs,

including legal fees), penalties, interest, expenses and other liabilities resulting from a third party

claim against us or any of our Affiliates arising from our relationship with you (whether or not in the

fulfilment of either party’s obligations under this Agreement). You will also compensate us and our

Affiliates for any loss, damages or claims, when a third party claims that we or our Affiliates have

infringed the third party’s intellectual property rights by (i) using or permitting the use of, or being or

having been the registered proprietor of a domain name, a brand name, trademark, logo or other

intellectual property, or by (ii) assisting or permitting you to use or to be a registered proprietor of

such rights, which infringe the third party’s rights.

12.5 You must maintain, at your own expense, insurance policies which are required by law and/or would

be expected to be maintained as a matter of good industry practice to a reasonable level. Upon our

request, you must produce evidence of having the required insurance policies.



13\. ACCESS TO DATA, AND PROTECTION OF PERSONAL DATA

Access to data

13.1 In operating the Platform, and providing services to you and your Premises, we and certain of our

Affiliates collect and have access to the data that this generates. We and our Affiliates use this data

in a variety of ways and for various reasons, including analytics, operational purposes, data matching

(sometimes performed by a third party service provider), and using aggregated data in

communications to shareholders and in some public materials (e.g. on our website, or in our annual

report). We and certain of our Affiliates also collect and have access to data relating to Orders and

Customer Personal Data (as set out in the Takeaway.com privacy statement for Customers).

13.2 You have access to certain data relating to you and the Orders via the Partner Hub. However, you do

not have access to any other data, and we do not share data relating to you, the Orders, or any other

data, with any other partner.

Protection of Personal Data

13.3 You and we shall be separate data controllers concerning the Personal Data that is processed in the

context of this Agreement, each for its own purposes and by its own means and/or on their

respective behalf. This means that you and we are responsible for the Processing of the Personal

Data independently of each other, albeit that the Parties may – in the context of this Agreement -

restrict the purposes and means for which the Personal Data may be used.



13.4 You acknowledge to be familiar with Takeaway.com’s privacy statement for Customers and will act in

accordance therewith and the Data Protection Legislation. The most recent version of the privacy

statement can be found on the Platform.



13.5 You will not provide Personal Data of Customers to third parties and will not engage in

communication with Customers other than for purposes of preparation and delivery of the Order.You

shall implement and maintain appropriate Security Measures, internal controls and information

Security Measures routines intended to protect Personal Data against accidental, unauthorised or

unlawful access, disclosure, alteration, loss, or destruction to ensure a level of security appropriate to

the risk presented.



13.6 In case Sensitive Personal Data of Customers is Processed as part the Order and within the context

of this Agreement you shall implement additional Security Measures if and where needed; and in any

event you shall comply with 13.5. above and shall ensure that you Process Sensitive Personal Data

in accordance with the Data Protection Legislation.You shall promptly (and without undue delay)

notify us in writing of any Personal Data breach of which you become aware relating to Personal Data

of Customers and keep us up to date in regard to such Personal Data breach.



13.7 Nothing in this Agreement or the arrangements contemplated by it, is intended to construe either

party as i) the processor of the other party; or ii) joint controllers with one another, with respect to

personal data that is shared by one party with the other.



01.11.2024 14

14\. RESTRICTION, SUSPENSION AND TERMINATION

14.1 If:

(a) we (acting reasonably) believe that you are in default of your obligations under this Agreement;

and/or

(b) any of the events in Clauses 14.3(b) apply to you, or in Clause 14.3(c) occur; and/or

(c) we have the specific right to as set out in the rest of this Agreement (which we have under

Clauses 7.6, 8.17, 8.18, 8.19, 8.20, 8.21, 8.22, 11.3, 16.2, and Clause 2.9 of Section D

(Supplemental Terms - Delivery)), and we validly exercise that right,

we may at any time on written notice, restrict, suspend, or terminate part of, the provision of the

Services under this Agreement, including by suspending your profile on the Platform. If you operate

more than one Premises, we are entitled to invoke this clause in respect of only one Premisesor all

of them, in our sole discretion.

14.2 If we restrict, suspend, or terminate part of, the provision of Services to you as set out in Clause 14.1

above, we will provide you with a clear explanation of our reasons for doing so (including the

grounds we're relying on) by email on or before the date on which the restriction, suspension, or

termination, becomes effective. If you wish, you can clarify the facts or circumstances that led to the

restriction, suspension, or termination, using our complaint-handling system. We will then engage

with you to discuss these, and if we determine after that discussion that the restriction, suspension,

or termination, is not appropriate, we will reinstate the applicable Services, including where

applicable your profile on the Platform, without undue delay.

14.3 Without affecting any other right or remedy available, either you or we may at any time on written

notice terminate this Agreement:

(a) if the other is in significant breach of any of its obligations under this Agreement and that breach

is not capable of remedy or, if the breach is capable of remedy, it has not been remedied to the

satisfaction of the non-breaching party within 14 (fourteen) days' of notice of the breach by the

non-breaching party;

(b) if the other becomes insolvent, bankrupt, or enters into any similar or analogous solvency related

procedure;

(c) if a Force Majeure Event makes the provision of the Services impractical or non-commercially

viable; or

(d) you or we are required to by a legal or regulatory obligation.

14.4 For a reasonable period of time and with prior warning, and in accordance with our guidelines, we

may suspend

(a) our Services to you if you frequently provide illegal content,

(b) the processing of notices and complaints submitted by you through various means if you

frequently submit notices \& complaints that are manifestly unfounded.

14.5 You can also terminate this Agreement for convenience, but you must provide us with at least 30

(thirty) days’ written notice before termination takes effect.

14.6 If we terminate this Agreement, we will give you prior notice, and also provide you with a clear

explanation of our reasons for doing so (including the grounds we're relying on), by email at least 30

(thirty) days' before termination takes effect, except where we terminate this Agreement under

Clause 14.3, in which case we will give you as much reasonable prior notice as is possible in the

circumstances, and we will provide the clear explanation of our reasons without undue delay (except

if we are legally restricted from doing that, or we have terminated this Agreement as you have

repeatedly breached you obligations, in which we case we may provide that explanation).

14.7 If you wish, you can clarify the facts or circumstances that led to the restriction, suspension or

termination using our complaint-handling system as per Section 26.

14.8 Promptly after termination of this Agreement for any reason, we will remove your profile from the

Platform. You will return any Connection Method in your possession to us in good condition.

14.9 Regardless of anything else in this Agreement, you acknowledge and agree that search engines



01.11.2024 15

which have a licence to use your intellectual property or personal information arising from this

Agreement may continue to hold or use same post termination. Cached versions of the Platform may

continue to exist in the web browser and web servers of search engines and customers following

termination. We will not have any liability to you in connection with these matters to the extent they

lie outside of our control.

14.10 Your and our rights and obligations under Clauses 10, 11, 12, 13, 14.7, 17, 23, 24, 26 and 27, and

any provisions of this Agreement necessary for the interpretation or enforcement of it, will continue

and survive beyond termination of this Agreement.



15\. YOUR AUTHORITY \& CHANGE OF OWNERSHIP

15.1 Unless we have agreed otherwise in writing, we are authorised to accept instructions in respect of

your account from, and provide information about your account to: (i) the person who signed this

Agreement; (ii) any person who appears to us to be employed by that person ; and (iii) any other

person who we (acting reasonably) are satisfied has authority to act on your behalf. You must

promptly provide us with any information or evidence we may request for the purpose of proving

ownership of your business or Premises.

15.2 We will not be liable to you if we, acting reasonably, decline to provide the Services or decline to act

on your instructions because we are on notice from any person who we have reason to believe is

your duly authorised franchisor that to do so would breach any agreement made between you and

that franchisor.

15.3 If you sell or transfer the ownership of your business, you must advise the new owner or transferee

of this Agreement and give us at least 14 days notice in writing of the proposed sale or transfer. If

you do not give us sufficient notice and we make any payments to you that should have been made

to the new owner or transferee we will not be liable to make that payment to the new owner and

transferee and you will be liable to do so.

15.4 If we are notified or informed by a third party, of a change to the ownership of your business or other

important details relating to your business (for example a change of name, or a change to bank

account details) we will make reasonable efforts to contact you. If you don't respond, or if you don't

give us the notice referred to in Clause 15.3, we may change your status to ‘offline’ or terminate this

Agreement. If we make any payments to you that should have been made to the new owner or

transferee we will not be liable to make that payment to the new owner and transferee and you will

be liable to do so.

15.5 Existing Reviews by Customers about a Partner may not be carried over once there is a change of

ownership.



16\. CONSENTS \& COMPLIANCE WITH LAWS

16.1 You confirm and promise that you have obtained and will maintain all necessary consents, licences,

permits, registrations (including food business registration), approvals or authorisations (“Consents”)

of any relevant person or government authority in relation to your business.

16.2 You must notify us in writing immediately, providing all relevant information, if:

(a) any of the Consents are revoked, suspended or altered;

(b) you are served with an administrative prohibition notice or order or equivalent; or

(c) you have reason to believe that any Goods that you have supplied or provided for supply to a

Customer are unsafe.

To be clear, other than an alteration to the Consents, the occurrence of any of the events in Clause

16.2(a) to (c) constitutes a breach of this Agreement, and we reserve the right to (amongst other

things) suspend your profile on the Platform in accordance with Clause 14.1.

16.3 You must comply with all applicable laws and regulations including, without limitation in relation to

health and safety, tax, data protection, food and product standards, (including maintaining an

appropriate food safety management system), food labelling, product labelling and packaging laws,

regulations and requirements, the sale of alcohol, hygiene and consumer information (including



01.11.2024 16

allergen labelling), and will provide reasonable evidence to us of this compliance upon reasonable

request and any other laws, regulations, permits, licences, orders, codes and directions of any

governmental authority or agency or other regulatory body relating to your business and the

operation of your business and the sale, preparation, handling, packaging, labelling, processing of

any Goods pursuant to an Order. You hereby confirm that (i) all products and services that you offer

for sale will comply with applicable laws and regulations, and with any policy we provide to you; (ii)

you will not offer for sale any products or services which are illegal, counterfeit, dangerous or which

otherwise infringe on consumer rights; and (iii) you are economic operator and have adequate

Product Recall procedures in place.

16.4 You should observe any guidance or training materials that we may provide from time to time to

support your compliance with applicable laws.

16.5 Each party is solely responsible for self-assessing, claiming and remitting all its applicable taxes.

16.6 In performing your obligations under this Agreement, you must:

(a) comply with all applicable anti-bribery, anti-corruption, anti-slavery and human trafficking laws,

statutes, regulations and codes from time to time in force; and

(b) notify us as soon as you become aware of any actual or suspected slavery or human trafficking

that has a connection with this Agreement or in any part of your business or of any request or

demand for any undue financial or other advantage of any kind received by us or you, as the

case may be, in connection with the performance of this Agreement.



17\. RECORDS

Each party must collect, maintain and retain accurate records relating to the proper performance of its

obligations pursuant to this Agreement and/or as required by law.



18\. ASSIGNMENT \& SUB-CONTRACTING

Other than to an Affiliate or, in the case of Thuisbezorgd.nl only, in relation to the procurement of Delivery

Services, neither you or we have the right to assign or sub-contract all or any of our respective rights or

obligations under this Agreement without the prior written consent of the other, which must not to be

unreasonably withheld or delayed, or where expressly permitted in this Agreement. Any consent, if given,

will not affect such your or our obligations or liabilities under this Agreement.



19\. FORCE MAJEURE

Neither you or we will be liable to the other as a result of any delay or failure to perform its obligations

under this Agreement resulting from a Force Majeure Event.



20\. WAIVER, VARIATION \& THIRD-PARTY RIGHTS

20.1 Failure to exercise or delay in exercising a right or remedy under this Agreement does not operate as

a waiver (in other words, a voluntary giving-up) or prevent further exercise of that or of any other

right or remedy.

20.2 The waiver by either party of any breach of this Agreement will not prevent the subsequent exercise

of a right.



21\. AMENDMENTS TO THIS AGREEMENT

21.1 We may amend this Agreement from time to time. We will notify you of any proposed amendment(s)

via email, via a message on the Partner Hub or via messaging on the Connection Method or any

other similar communication method in line with the applicable laws. Except in the limited situations

described in Clause 21.3, the proposed amendment(s) will not take effect until at least 15 (fifteen)

days from the date on which we notify you about them (and we will set out the effective date of the



01.11.2024 17

amendment(s) in the relevant notification).

21.2 Whenever we notify you of a proposed amendment(s) to this Agreement, you will have the right to

terminate this Agreement before expiry of the applicable notice period. If you do wish to terminate,

and let us know during that notice period, termination will then take effect 15 (fifteen) days from your

receipt of the notification. You may also choose to give up your right to terminate by either letting us

know in writing, or by taking a clear affirmative action. If we do not hear from you by the end of the

notice period, you will be deemed to have agreed to the amendment(s).

21.3 The minimum 15 (fifteen) day notice period in Clause 21.1 will not apply where:

(a) we are subject to a legal or regulatory obligation which requires us to amend this Agreement in a

way which does not allow us to give you that length of notice period; and

(b) we need to amend this Agreement to address an unforeseen and imminent danger that relates

to defending the Services, the Platform, Customers or partners from fraud, malware, spam, data

breaches or other cybersecurity risks.



22\. ENTIRE AGREEMENT

This Agreement constitutes the entire agreement between the parties and supersedes all previous

agreements, arrangements and understandings between the parties relating to its subject matter.



23\. NOTICES

23.1 All notices under this Agreement must be in writing, and any notice sent for the purposes of this

clause will be considered received:

(a) if delivered by hand, before 5:00pm, on that Business Day;

(b) if sent by mail, on the third Business Day after posting; or

(c) if sent electronically, at the time of sending unless the sender’s electronic system receives a

delivery failure notification.

except that a delivery by hand, mail or email received after 5:00pm (local time of the receiving Party)

will be deemed to be given on the next Business Day.

23.2 The parties agree that service of proceedings or other documents in any legal action or, where

applicable, any arbitration or other method of dispute resolution must not be given solely by email.

23.3 The addresses for services of notices are as specified in this Agreement and may be varied by

written notice.



24\. GENERAL

24.1 Any phrase introduced by the expression "including", "in particular" or any similar expression is

illustrative, and will not limit the sense of the words preceding those terms.

24.2 If any of the terms or conditions of this Agreement are declared wholly or partly invalid, illegal or

unenforceable, the remainder of the Agreement will remain in full force and effect and any wholly or

partly invalid term or condition will be deemed modified to the minimum extent possible to make it

valid, legal and enforceable.



25\. EXECUTION

This Agreement may be executed electronically. Notwithstanding the use of the words “writing,”

“execution,” “signed,” “signature,” or other similar words, the parties intend that the use of an electronic

signatures and the keeping of records in electronic form will have the same legal effect, validity or

enforceability as a signature affixed by hand or the use of a paper-based record keeping system.



01.11.2024 18

26\. COMPLAINT-HANDLING

26.1 We operate a complaint-handling system that you can use free of charge. It allows you to submit

complaints to us across a range of issues, including but not limited to suspension, restriction and

termination cases, and we will deal with any complaints transparently, equally and proportionally. We

will then engage with you to discuss your complaints, and if we determine after that discussion that

our decision was not appropriate, we will take the necessary actions without undue delay.

26.2 You can submit a complaint to us via the Partner Hub, email or telephone. For the contact details,

please refer to our contact section in the Partner Hub.

All complaints will be dealt with appropriately by the person receiving them, and as part of that may

then be escalated internally so that the appropriate Thuisbezorgd.nl internal team can then consider

it and respond to you (for example, we have internal compliance and legal teams that deal with

certain types of complaint). We will respond to all complaints as soon as we reasonably can.



27\. MEDIATORS, GOVERNING LAW AND JURISDICTION

27.1 Mediation is a process where a neutral third party facilitates negotiations between the parties to a

dispute to help them come to an outcome that they can all agree on. We work with the Centre for

Effective Dispute Resolution ("CEDR"), who we are willing to engage with to attempt to reach an

agreement with you on the settlement, out of court, of any disputes we may have with you arising out

of this Agreement, including complaints that could not be resolved by means of our

complaint-handling system referred to in Clause 26. Although mediation is a voluntary process, you

and we both agree to engage in good faith throughout any mediation attempts, and to also do so in

accordance with the CEDR Model Mediation Procedure.

27.2 You and we both agree to notify the other in writing if one of us wishes to submit a dispute to

mediation. Unless you and we agree otherwise within 14 (fourteen) days of that notice, the mediator

will be nominated by CEDR. We will bear a reasonable proportion of the total costs of mediation. Any

attempt to reach an agreement through mediation on the settlement of a dispute will not affect your

or our right to initiate court proceedings at any time before, during or after the mediation process. Let

us know if you need any further information of the functioning and effectiveness of mediation.

27.3 This Agreement and any dispute or claim (including a non-contractual dispute or claim) arising out of

or in connection with it will be governed by and construed in accordance with Dutch law.

27.4 The commercial courts of Amsterdam, The Netherlands will have exclusive jurisdiction to settle any

dispute or claim (including any non-contractual dispute or claim) arising out of this Agreement.



01.11.2024 19

SECTION C. SUPPLEMENTAL TERMS – PAYMENT SERVICES



1. APPLICATION

1.1 These ‘Supplemental Terms – Payment Services’ (“Payment Terms”) apply to the Payment Services

provided by TP to the Payment Partner. The applicability of other general terms and conditions related to

payment services, such as those of the Payment Partner, are expressly rejected. TP and Thuisbezorgd.nl

are never parties to the contracts concluded via the Platform on Payment Partner’s behalf. The Payment

Partner is solely responsible for the performance of these contracts.



1.2 These Payment Terms can be found online at http://restaurants.takeaway.com and are also available for

inspection at TP headquarters at Piet Heinkade 61, 1019 GM Amsterdam.



1.3 The Payment Partner is familiar with the privacy statement of Takeaway.com and will act in accordance

with this privacy statement. The Payment Partner will not disclose any personal data of Customers to third

parties.



2. REGISTRATION OF THE PAYMENT PARTNER

2.1 The Payment Partner is registered with TP by submitting the completed Thuisbezorgd.nl registration form

(and related documentation) before TP, as the Payment Services provider of Thuisbezorgd.nl, will provide

the Payment Partner with the Payment Services.



2.2 TP may resolve not to accept you as Payment Partner.



2.3 The Payment Partner commits to provide TP with any information that TP in its sole discretion deems

required in order to identify and verify the Payment Partner and the ultimate beneficial owner of the

Payment Partner.



2.4 The Payment Partner is obliged to inform TP immediately of any significant change in the information

provided per Clause 2.3 of these Payment Terms.



2.5 For the purposes of Clauses 2.1 – 2.5 of these Payment Terms, it is noted that the registration process

may, in whole or in part, be performed by Thuisbezorgd.nl for the benefit of TP and that the Payment

Partner is obliged to act upon instructions of Thuisbezorgd.nl as it were instructions from TP.



3. INDEMNITIES OF THE PAYMENT PARTNER

3.1 The Payment Partner indemnifies TP for claims of Customers relating to the behaviour of the Payment

Partner, its agents or employees, or relating to the performance of Orders.



4. PERIOD AND TERMINATION OF THE PAYMENT SERVICES

4.1 TP provides the Payment Services for an indefinite period of time from the date of confirmation of the

receipt and approval of the registration and the KYC Information of the Payment Partner.



4.2 TP only provides Payment Services to the Payment Partner as long as the Payment Partner uses the

Services and, if applicable, a winding up period thereafter to ensure that outstanding payment transactions

are settled in accordance with these Payment Terms.



4.3 TP may suspend and terminate the Payment Services and freeze funds if the Payment Partner violates a

provision in these Payment Terms or acts contrary to the good name and reputation of TP or the group of

companies which TP and Thuisbezorgd.nl are part of, or if the Payment Partner and/or its owners are

listed on one or more of the Sanction Lists.



5. REGULATED PAYMENT SERVICES BY TP

5.1 TP has a license from De Nederlandsche Bank N.V., a public limited company incorporated under Dutch

law, established in (1017 ZN) Amsterdam at Westeinde 1 and registered in the Commercial Register of

the Chamber of Commerce in Amsterdam under number 33003396 (“DNB”) on the basis of which it may

provide Payment Services.



01.11.2024 20

5.2 TP provides the Payment Services in the Netherlands as well as in certain other EU member states in

support of the Platform only.



5.3 DNB shall exercise the prudential supervision of TP.



5.4 DNB keeps a public register in which all licensed payment institutions are included. This register can be

consulted through the DNB website (click here).



6. EXECUTION OF THE PAYMENT SERVICE

6.1 TP is obliged to safeguard the funds it receives in its capacity as Payment Service provider on a current

account that has been separated from the assets of TP. This current account is held by the Foundation.



6.2 The Foundation may rely upon all provisions of the Agreement, including these Payment Terms. Wherever

necessary, this Article will apply as a third-party clause in the meaning of Section 6:253 of the Netherlands

Civil Code in favour of the Foundation, which TP hereby accepts beforehand on behalf of the Foundation,

should the circumstances ever occur.



6.3 The Payment Partner authorises TP to provide the Payment Services to the Payment Partner until the

moment the Payment Services are terminated or suspended in accordance with the provisions in Clause 4

of these Payment Terms.



6.4 TP will ensure settlement of all payment obligations towards Customers (for refunds), to the Payment

Partner, Thuisbezorgd.nl and TP relating to the monies received by the Foundation from Customers in

connection with Orders. TP will do so on the basis of the Thuisbezorgd.nl Administration.



6.5 TP will, assisted by and/or represented by Thuisbezorgd.nl, provide an overview of balances and

transactions for the Payment Partner and ensure a transfer of a positive balance by the Foundation to the

Payment Partner on a weekly basis. This cashless payment by the Foundation to the Payment Partner

takes place no later than ten (10) business days from the Statement, or, later if a more detailed

investigation of the transaction is required.



6.6 The Payment Partner accepts that on the calculation of the reciprocal rights and obligations between

Thuisbezorgd.nl and the Payment Partner, TP acts on the basis of the Thuisbezorgd.nl Administration.



6.7 The Payment Partner shall have a claim on the Foundation for the payments referred to in Clause 6.5 of

these Payment Terms, for the amount reflected in the Thuisbezorgd.nl Administration.



6.8 The Payment Partner agrees that any claims from Customers for a refund rank higher than payment

obligations to a Payment Partner.



6.9 Communication between TP and the Payment Partner regarding the provision of Payment Services will

take place by electronic mail and/or orally. Electronic mail regarding the provision of Payment Services by

TP may be addressed to Thuisbezorgd.nl’s customer service. This communication will take place in the

local or English language.



7. COMPLAINT AND FRAUD PROCEDURES

7.1 TP has a complaints procedure in place with regard to the provision of Payment Services. This complaints

procedure can be found online at http://restaurants.takeaway.com and is also available for inspection at

the TP headquarters at Piet Heinkade 61, 1019 GM Amsterdam.



7.2 If TP is aware of (a specific suspicion of) fraud or a security threat that may or will have an impact on the

Payment Partner, TP will inform the Payment Partner of this via an encrypted email as soon as possible.



8. LIABILITY OF TP

8.1 TP is not liable towards the Payment Partner unless the Payment Partner incurred damage that is directly

attributable to intent or wilful recklessness by TP. TP's total liability towards the Payment Partner is

restricted (cumulatively) to the amount that is paid in the relevant case on the grounds of the liability

insurance taken out by TP. If, for any reason whatsoever, no payment of this insurance should take place,



01.11.2024 21

any liability of TP towards the Payment Partner, regardless the legal ground, is restricted (cumulatively) to

EUR 10,000 (ten thousand euros) or the equivalent in the local currency.



8.2 Amounts payable by Customers shall only be contributed to the balance payable by TP to the Payment

Partner in accordance with Clause 6.5 to the extent that such amount is received by TP or the

Foundation.TP is not liable for any damage that occurs because Customers do not fulfil their financial

obligations towards the Payment Partner.



8.3 The Payment Partner will notify TP immediately and no later than thirteen (13) months after the value date

of a non-permitted or incorrect payment transaction in via e-mail.



9. OTHER PROVISIONS

9.1 The TP Payment Service administration in combination with the Thuisbezorgd.nl Administration provide

full proof, but the Payment Partner may provide evidence to the contrary.



9.2 The Payment Partner waives any right to invoke suspension of payments or set-off.



9.3 TP may transfer its rights or obligations arising from the Payment Services to third parties.



9.4 The Payment Partner will observe confidentiality in respect of the Payment Services and will not disclose

to third parties any information it has received with respect to the Payment Services, unless this takes

place after prior approval of TP or disclosure of the relevant information is required on the basis of

legislation or regulations, any requirement from an exchange acknowledged by the government, or a

binding decision of the court or other government body.



9.5 If a provision of these Payment Terms proves to be null and void, invalid or unenforceable in full or in part,

the relevant provision or the relevant part thereof will be deemed not to be a part of these Payment Terms,

but this will have no consequences for the validity, binding effect and enforceability of the other provisions

in these Payment Terms.



9.6 The following provisions of the Netherlands Civil Code do not apply in the relationship between TP and the

Payment Partner under the Payments Terms: articles 7:516, 7:517, 7:518 and 7:519, article 7:520(1),

article 7:522(3), article 7:527, articles 7:529 to 7:531, article 7:534 and articles 7:543, 7:544 and 7:545

Netherlands Civil Code and all legislation pursuant to and/or based on such articles. Furthermore, if not

already covered by the articles referred to in the preceding sentence, the rules regarding provision of

information in the Market Conduct Supervision (Financial Institutions) Decree (Besluit gedragstoezicht

financiële ondernemingen Wft) that follow from Title III PSD 2 do not apply.



9.7 Article 9.6 of these Payment Terms contains the exclusion of the relevant provisions of PSD 2 as

implemented in Dutch law. Consequently, all rules with regard to the content and provision of the

information required by Title III of PSD 2 and the following provisions of Title IV of PSD 2 do not apply to

the Payment Services that TP provides under these Payments Terms: article 62(1), article 64(3), article 72,

article 74, article 76, article 77, article 80 and article 89 of PSD 2 and all legislation pursuant to and/or

based on such articles, including, to the extent applicable, the foreign law implementation of these articles

of PSD2.



01.11.2024 22

SECTION D. SUPPLEMENTAL TERMS - DELIVERY SERVICES



1. APPLICATION

This Section D applies where we have agreed to procure Delivery Services for you.

2. THUISBEZORGD.NL RIGHTS AND OBLIGATIONS

In procuring the Delivery Services, we:



2.1 will use commercially reasonable efforts to have the Order delivered to the Customer;



2.2 may charge each Customer a fee on a per Order basis for Delivery (which we may vary from time to

time);



2.3 may determine the service times during which Delivery Services are offered, which may vary depending

on the Premises and area;



2.4 may determine the maximum distance that will be permitted between a Premisesand the Customer for a

particular Order, which will be determined based on radius and polygon analysis and may change from

time to time at our discretion;



2.5 have the right to withdraw Delivery Services for an Order within a reasonable timeframe following

receipt of the Order confirmation, if the Delivery Services cannot be performed on time due to capacity

problems, in which case you will be reimbursed for reasonable costs made on a case-by-case basis;



2.6 reserve the right to set a minimum Order value before any Orders using our Delivery Services are

processed and accepted;



2.7 have the right to charge you for all reasonable costs made if, and insofar, an Order cannot be delivered

or is substantially delayed due to a cause attributable to you;



2.8 will reimburse reasonable costs made by you if, and insofar as, an Order cannot be delivered or is

substantially delayed due to a cause attributable to us; and



2.9 may suspend or cease to provide Delivery Services in accordance with Clause 14.1 of the General

Terms if you do not comply with any reasonable rules or guidelines relating to Orders for delivery which

we may give you notice of from time to time, or in the event of Force Majeure.



3. YOUR OBLIGATIONS

Where we procure the Delivery Services, you must:



3.1 choose a Preparation Time for each Order received via the Connection Method and then confirm the

Order; after confirmation of the Order, the Preparation Time cannot be changed;



3.2 prepare the Goods which have been requested in the Order so they are ready for collection by us at the

Pick-up Time;



3.3 not exceed the maximum Courier Waiting Time of 3 minutes;



3.4 use packaging which is suitable for delivery (including by preventing cross-contamination and maintain

safe temperatures of the food during delivery) and does not exceed the size of the delivery bag (450mm

x 430mm x 450mm (including packaging) and the weight of the delivery bag (weight limit of 7.5kg

including packaging) used by the Courier to deliver the Order;



3.5 check each Order to ensure it is accurate, complete and contains all the Goods ordered;



3.6 ensure that accurate allergen information relevant to that Order is provided with the Goods at the point

of collection;



01.11.2024 23

3.7 ensure the Order number you hold corresponds with the Order number presented by us or the Delivery

Partner (as applicable); and



3.8 take reasonable steps to avoid significant delays in handing Orders to us or the Delivery Partner (as

applicable).



4. CHARGES AND INVOICING

For each Order picked up or delivered using Delivery Services, you must pay the relevant Fees set out

in the Agreement which may apply. This will be included in each Tax Invoice/Statement.



01.11.2024 24

SECTION E. SUPPLEMENTAL TERMS – STAMPCARDS



1. APPLICATION

This Section E applies in case you have signed up for the Stampcard Program.



2. PARTICIPATION

2.1 To participate in the Stampcard Program and award Stamps to Customers, you need to sign up for the

Stampcard Program. You can sign up by ticking the relevant option on the registration form if available, by

registering via our customer service or by clicking the ‘Join Now’ button in the Partner Hub.



2.2 We may refuse the participation of a Premisesin the Stampcard Program at our sole discretion.



3. STAMPS



3.1 Stamps are awarded exclusively by you to a Customer. We are only the technical provider of the

Stampcard Program.



3.2 Customers with a valid email address will automatically receive 1 Stamp for an Order placed at your

Premises. Customers can receive a maximum of 1 Stamp per day.

3.3 Within 48 hours of placing an Order with your Premises, a Customer that is signed up for the

Thuisbezorgd.nl newsletter will automatically receive an email with an up-to-date overview of their

Stampcard of that Premisesand a link to the total overview of Stampcards.



3.4 A Stampcard is full and complete after having collected five Stamps from the same Premises. A Stampcard

Voucher will be emailed to the Customer within 48 hours after the Stampcard is full.



3.5 Stamps expire 12 months following the date the Stamps have been awarded to the Customer.



3.6 Stamps are not transferable between Premises, Customers, email addresses, Stampcards or otherwise.



3.7 We will correct Stamps that were awarded while the Order and/ or payment thereof was cancelled.

However, if such Stamp completed a Stampcard and resulted in the generation of a Stampcard Voucher,

the Stamp is considered valid and the Stampcard Voucher will be redeemable.



4. STAMPCARD VOUCHERS



4.1 A Stampcard Voucher:



- can only be redeemed with an Order with the Premises from which the Stamps were received;



- can only be redeemed in combination with an online payment; and



- cannot be combined with other vouchers of any kind.



4.2 The Stampcard Voucher is redeemable up and until 90 days after the Stampcard Voucher was issued.



4.3 The value of a Stampcard Voucher equals 10% of the total Order amount of the five Orders for which the

Stamps were collected.



4.4 The costs of a redeemed Stampcard Voucher are for your account.



4.5 The total amount that is paid by Customers with Stampcard Vouchers will be visible on the Statement in

the Partner Hub.



4.6 The Fees you owe to us will be based on the Gross Order Value of the Order, thus also over the amount of

the Stampcard Voucher if a Stampcard Voucher is used.



01.11.2024 25

5. DURATION, MODIFICATION AND TERMINATION OF THE STAMPCARD PROGRAM



5.1 You are free to terminate your participation in the Stampcard Program for convenience with respect to a

prior 15 (fifteen) days’ written notice. You can do so by contacting our customer service. Termination will

not affect the validity of existing Stampcard Vouchers in any way.



5.2 If a Customer has collected Stamps that can no longer be redeemed because you terminated your

participation in the Stampcard Program, we may claim the amount corresponding to the value of the

outstanding Stamps from you.



5.3 We reserve the right to cancel the Stampcard Program for convenience with a prior 30 (thirty) days’ written

notice. In case of cancellation of the Stampcard Program, Customers can no longer collect Stamps.

However, Stampcard Vouchers will remain valid until their expiration date.



5.4 We reserve the right to withhold or suspend you or your Premises from participation in the Stampcard

Program in case of a reasonable (suspected) violation of the Agreement.



01.11.2024 26