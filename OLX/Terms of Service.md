Termos de Utilização
====================

   ![olx logo.png](/olxpthelp/servlet/rtaImage?eid=ka0J8000000YOWU&feoid=00N3X00000M5uad&refid=0EM090000044IML)

 

Termos e Condições de Utilização

POR FAVOR LEIA ATENTAMENTE ESTES TERMOS E CONDIÇÕES DE SERVIÇO ANTES DE UTILIZAR ESTE WEBSITE OU ESTA APLICAÇÃO E CONTRATAR OS NOSSOS SERVIÇOS

(Última atualização em 29 de Abril de 2024)

 

1. Definições

Salvo se do contexto claramente decorrer um sentido diferente, nos presentes Termos e Condições, os termos e expressões usados com letra maiúscula a seguir enumerados, independentemente de serem usados no singular ou no plural, têm o seguinte significado:

1. Entrega - serviço de transporte de Artigos coberto pela Transação, prestado por transportadores profissionais selecionados através do serviço prestado pelos parceiros do OLX. O presente serviço não é diretamente prestado ou disponibilizado através das funcionalidades do OLX no Website, pelo que o acesso ao serviço de transporte que permite solicitar o Serviço de Entrega do Artigo será fornecido através do site(s) do(s) transportadores profissionais, sendo da inteira responsabilidade e iniciativa do Utilizador a contratação dos serviços de transporte e aceitação dos seus termos e condições.
2. Dia - significa 24 horas consecutivas.
3. Visitante: pessoa, singular ou coletiva, que aceda ao Website ou à Aplicação, sem possuir uma conta ou iniciar a sessão/login na mesma.
4. OLX \- OLX PORTUGAL, S.A., sociedade comercial anónima, no Edifício Atrium Saldanha, Praça Duque de Saldanha, N.º 1, 6.º andar, 1050 – 094 Lisboa, registada na Conservatória do Registo Comercial de Lisboa, sob o número 508 069 491, com capital social de € 360,000,00 (trezentos e sessenta mil euros). O OLX pode ser contactado através do seguinte correio eletrónico: [ajuda@olx.pt](mailto:ajuda@olx.pt).
5. Candidato - Visitante ou Utilizador que consulta os Anúncios na Categoria Emprego ou que utiliza as funcionalidades dedicadas que se especificam no Anexo 1 "Emprego OLX".
6. Categoria \- categoria temática (p.ex. Serviços ou Tecnologia) à qual o Anúncio se encontra atribuído. Uma categoria pode dividir-se em várias subcategorias temáticas relacionadas.
7. Consumidor – o utilizador que é uma pessoa singular, que executa, no Website, atividades não diretamente ligadas à sua atividade comercial, industrial, artesanal ou profissional.
8. Conta - conjunto de dados relacionados com um Utilizador específico, incluindo informações sobre a sua atividade no Website, incluindo informações fornecidas pelo utilizador no Website. As regras relativas à conta encontram-se especificadas na      Cláusula 3 dos presentes Termos e Condições.
9. Comprador – o utilizador que realiza Transações com o Vendedor, através das funcionalidades do Website.
10. Limite - número de Anúncios gratuitos disponíveis nas Categorias selecionadas que o Utilizador pode publicar no Website durante um período especificado. Os limites de cada Categoria podem ser utilizados de forma independente.
11. Anúncio – uma proposta de venda ou fornecimento do Artigo/Produto disponibilizado pelo Vendedor ou um convite para participar no processo de recrutamento iniciado pela Entidade Patronal ou Recrutador e publicado no Serviço, de acordo com os princípios especificados no Artigo 4 dos Termos e Condições.
12. Saldo OLX \- funcionalidade gratuita da Conta que se descreve na Cláusula 9 do Termos e Condições, que permite a realização de pagamentos ao OLX.
13. Entidade Patronal/Recrutador - Utilizador que publica um Anúncio na categoria Emprego do Website ou utiliza as funcionalidades dedicadas que se especificam no Anexo 1 "Emprego OLX".
14. Artigo/Produto\- os bens (móveis ou imóveis) ou serviços cobertos pelo Anúncio; nos Anúncios da Categoria Emprego, o artigo do Anúncio consiste na descrição e nos requisitos do cargo oferecido pelo Anúncio da Entidade Patronal/Recrutador.
15. Diretrizes: as instruções que os Anunciantes devem assegurar que são cumpridas pelos anúncios que divulgam no Website e na Aplicação, descritas na Cláusula 10 abaixo.
16. Período de Colocação: o período de disponibilização de cada anúncio no OLX, o qual tem uma duração de 28 (vinte e oito) Dias ou 365 Dias (quando na categoria Carros, motos e barcos | Carros), renováveis, estando a renovação, em determinadas circunstâncias, sujeita a pagamento.
17. Preço Dinâmico: um preço que varia em resposta à procura do mercado. O preço dinâmico atual está disponível ao preencher o formulário de Anúncio ou antes de comprar um determinado Serviço.
18. Serviço de Divulgação de Destaques: a colocação e disponibilização pública de anúncios com destaques pagos no Site ou na Aplicação, mencionada nas Cláusulas 2.16 e 7 infra e como se descreve em pormenor no Anexo 2 "Serviços Promocionais".
19. Serviços: cedência de espaços no Website e/ou na Aplicação necessários à colocação e disponibilização pública de anúncios classificados do Anunciante para que este possa divulgar e promover a venda de Produtos, durante os períodos selecionados por si.
20. Serviços Pagos \- serviços pagos prestados pelo OLX ao Utilizador, incluindo, entre outros: publicação de um Anúncio Pago ou de um pacote de Anúncios e Serviços Promocionais.
21. Termos e Condições - estes Termos e Condições e os respetivos anexos, que estabelecem as regras de utilização do Website. A versão atual do Termos e Condições está disponível no Website a qualquer momento, num formato que pode ser transferido e guardado no disco rígido do dispositivo ou impresso.
22. Registo - processo de criação de uma Conta pelo Utilizador, depois de fornecer os seus dados, aceitar o Termos e Condições e ativar a Conta.
23. Website (“Serviço”) - plataforma online gerida pelo OLX, disponível no domínio olx.pt e na aplicação móvel OLX.pt.
24. Vendedor - Utilizador que publica um Anúncio no Website e realiza Transações com o Comprador.
25. Transação - qualquer contrato celebrado entre os Utilizadores ou o Anunciante e o Visitante relativamente ao Artigo/Produto.
26. Anunciante/Utilizador \- pessoa singular, pessoa coletiva ou entidade organizacional sem personalidade jurídica, com capacidade jurídica plena para a promoção e/ou celebração de transações relativas ao Produto que tem intenção de anunciar, de acordo com o direito português, e que utiliza o Serviço depois de iniciar a sessão na Conta/fazer login.
27. Anunciante Profissional \- Utilizador do Serviço que atue enquanto comerciante ou profissional, que é uma pessoa singular, pessoa coletiva ou entidade organizacional sem personalidade jurídica, à qual a lei confere capacidade jurídica para, em seu próprio nome, exercer atividades comerciais ou profissionais e utilizar os serviços do OLX no Website, no âmbito da sua atividade comercial ou profissional.
28. Mediação\- um processo estruturado, na aceção do artigo 3.º, alínea a), da Diretiva 2008/52/CE.

 

2. Disposições Gerais

 

Estes Termos e Condições fornecem-lhe as regras de utilização do nosso site [www.olx.pt](http://www.olx.pt/) (“Website”) e da nossa aplicação para dispositivos móveis denominada “OLX” (“Aplicação”) e contêm a informação pré-contratual obrigatória nos termos da lei aplicável.

 

Os contratos de prestação de serviços com a OLX PORTUGAL, S.A. são celebrados em português.

 

A contratação através desta plataforma é uma contratação eletrónica, nos termos do disposto nos artigos 26.º do Decreto-Lei n.º 7/2004, de 7 de janeiro e 3.º do Decreto-Lei n.º 290-D/99, de 2 de agosto e cumpre o disposto no Decreto-Lei n.º 24/2014, de 14 de fevereiro. Caso não concorde com a celebração do contrato por meios eletrónicos, não deverá utilizar o Website ou a Aplicação. Ao aceitar os presentes Termos e Condições o Utilizador aceita e reconhece que:

 

1. Os Termos e condições de utilização do Website, incluindo as regras de Registo, publicação de Anúncios e compra de Serviços Pagos, bem como questões relacionadas com o procedimento de pagamento e reclamação, encontram-se estabelecidos nos Termos e Condições. Qualquer pessoa que utilize o Serviço está obrigada a tomar conhecimento do teor do Termos e Condições.
2. A prestação do serviço OLX está dependente da total aceitação destas condições, pelo que qualquer utilizador que não esteja de acordo ou não se comprometa a comportar-se de acordo com estas condições não poderá utilizar o referido serviço.
3. Qualquer alteração aos presentes Termos e Condições será comunicada num suporte duradouro aos utilizadores dentro de um prazo razoável (de pré-aviso) igual ou não inferior a 15 Dias, salvo se outro prazo legal for aplicável.
4. Os Visitantes apenas podem utilizar as funções limitadas do Serviço nos termos previstos nos presentes Termos e Condições, respeitando a lei e os princípios de integridade.
5. Na utilização através da funcionalidade de navegação como Visitante são solicitados apenas os dados imprescindíveis para poder realizar a sua consulta ou pedido. O OLX irá apenas processar os dados pessoais dos Visitantes, de acordo com as disposições da [Política de Privacidade](https://help.olx.pt/hc/pt/articles/360000900765) e [Política de Cookies](https://help.olx.pt/hc/pt/articles/360000893149) e tecnologias similares do Website.
6. Os conteúdos publicados no Serviço, incluindo em particular Anúncios, qualquer que seja a sua forma, isto é texto, gráfico ou vídeo, estão sujeitos à proteção dos direitos de propriedade intelectual, incluindo copyright e direitos de propriedade industrial do OLX, dos Vendedores ou de terceiros. É proibido fazer qualquer utilização destes conteúdos sem o consentimento por escrito das pessoas autorizadas. É proibida qualquer agregação e processamento de dados e outras informações disponíveis no Website tendo em vista a sua partilha com terceiros noutros Websites e fora da Internet. É igualmente proibido o uso do Website e das designações do OLX, incluindo elementos gráficos característicos sem o consentimento do OLX.
7. Conforme a licença concedida ao OLX em conformidade com a Cláusula 4.3, nada nos presentes Termos e Condições constituirá um consentimento à utilização dos direitos do OLX ou dos direitos de terceiros mencionados na Cláusula 2.6 ou será considerado uma renúncia a tais direitos.
8. O OLX não é uma parte na Transação, assim sendo:
    1. O OLX não se responsabiliza pelo comportamento dos anunciantes, ou itens e serviços anunciados por eles para venda como descrito nos seus anúncios. Ao infringir num determinado anúncio as regras vigentes no OLX, o utilizador assumirá toda e qualquer responsabilidade resultando em danos ou prejuízos perante qualquer entidade, pessoa singular ou coletiva, estando o OLX isento de qualquer tipo de responsabilidade resultante.
    2. A legalidade do acesso ao serviço está dependente do facto de não serem celebrados qualquer tipo de contratos, onerosos ou não, sob a alçada do OLX, pelo que o acesso ao serviço é permitido aos utilizadores, independentemente da posse de total capacidade jurídica, por parte destes. Não detendo um carácter mediador de transações, o OLX não se responsabiliza por qualquer eventualidade resultante da falta de capacidade jurídica dos utilizadores.
    3. O OLX não se responsabiliza por qualquer dano que ocorra na sequência de uma transação, ou comportamento inadequado de uma das partes da transação.
9. Como parte do Website, é possível:
    1. consultar o conteúdo do Website;
    2. utilizar a Conta e as funcionalidades relacionadas, incluindo o Saldo OLX;
    3. utilizar as funcionalidades descritas no Anexo 1 “Empregos OLX”;
    4. publicar Anúncios dentro dos limites de gratuitidade indicados no Anexo 3 "Limites de Anúncios";
    5. publicar Anúncios pagos fora dos Limites;
    6. utilizar Serviços Pagos (incluindo Serviços Promocionais).
10. Os serviços de consulta do conteúdo do Website, Conta e publicação de Anúncios dentro dos limites de gratuitidade são prestados gratuitamente. Os restantes serviços são prestados mediante pagamento.
11. Os serviços de Entrega e pagamento são prestados por fornecedores de serviços externos para comodidade dos Utilizadores, tendo por base relações jurídicas separadas das quais o OLX não é parte.
12. As provisões das presentes Disposições Gerais e dos Termos e Condições aplicar-se-ão aos Anúncios publicados na Categoria Emprego, salvo indicação diferente no Anexo 1 "Empregos OLX".
13. Para utilizar o Website na sua totalidade, é necessário ter ligado à Internet um dispositivo que cumpra os seguintes requisitos, mas não limitado a:
    1. ligação ativa à Internet que permita comunicações bidirecionais através do protocolo https, sendo necessário que o endereço de Internet Protocol (IP) seja Português;
    2. devidamente instalado e configurado, Web browser atualizado que suporte a norma HTML5 e tecnologia de folhas de estilo em cascata (CSS3), p. ex., Google Chrome, Mozilla Firefox, Opera, Microsoft Edge;
    3. JavaScript e cookies ativados (normalmente ativados por predefinição no browser);
    4. Em dispositivos móveis:

Para as versões Android e iOS, devem ser utilizadas as versões mais recentes e compatíveis com o respetivo sistema operativo, por forma a permitir uma melhor performance ou utilização dos nossos Serviços OLX

* Para a utilização das aplicações OLX é necessário a transferência a partir de uma loja oficial (p. ex. App Store ou Google Play);
* O Website pode não ser mostrado corretamente em televisores ou telefones Blackberry e Windows.

14. O OLX envidará todos os esforços no sentido de garantir um funcionamento contínuo do Website. Para garantir uma elevada qualidade e a conformidade dos serviços, e um funcionamento eficiente dos Serviços, sendo o mesmo responsável por qualquer falta de conformidade que: (i) Exista no momento do fornecimento, nos contratos em que seja estipulado um único ato de fornecimento ou uma série de atos individuais de fornecimento, durante o prazo de dois anos, em conformidade com o disposto na Lei; ou; (ii) Ocorra ou se manifeste no período durante o qual os conteúdos ou serviços digitais devam ser fornecidos, nos contratos em que seja estipulado um fornecimento contínuo. O OLX reserva-se, porém, o direito de efetuar paragens no funcionamento do Serviço ou de suspender temporariamente as suas operações por razões técnicas, ou causas fora do controlo do OLX.     
15. Poderão ainda ser alterados os conteúdos ou serviços para além do necessário para os manter em conformidade. Qualquer alteração aos serviços digitais não afetará os serviços previamente adquiridos relativamente à data de produção de efeitos das alterações ou incorrerá em custos para o consumidor. 
16. O consumidor tem direito a rescindir o contrato caso a alteração tenha um impacto negativo no acesso ou na utilização dos conteúdos ou serviços, a menos que tal impacto seja apenas menor. Neste caso, o consumidor tem direito a rescindir o contrato, a título gratuito, no prazo de 30 Dias a contar da data de receção da notificação ou do momento em que os conteúdos ou serviços digitais foram alterados, consoante a data que for posterior.
17. Os anúncios devem respeitar, obrigatoriamente, as condições indicadas na secção "Conteúdo e Anúncios" dos Termos e Condições.
18. Os Utilizadores que comuniquem com outros Utilizadores através da funcionalidade de chat do Serviço reconhecem que estas conversações não são privadas e que o respetivo conteúdo pode ser acedido, recolhido e lido pelo OLX. Ao aceitar os presentes Termos e Condições, o Utilizador aceita e reconhece o direito do OLX consultar e aceder ao conteúdo das conversações mantidas através da funcionalidade de chat do Website, tendo em vista o aumento da segurança e proteção dos Utilizadores, bem como para efeitos de prevenção de fraudes e o aperfeiçoamento do Website. Para mais informações sobre a forma e os motivos pelos quais o OLX acede e analisa o conteúdo das conversações mantidas através das funcionalidades de sala de chat do Serviço, consulte a [Política de Privacidade.](https://help.olx.pt/olxpthelp/s/article/poltica-de-privacidade-V8)
19. As mensagens e respetivos anexos armazenados no Website, através do registo da conta, permanecem no sistema por um período de 4 (quatro) meses.
20. Os Anúncios no Serviço são apresentados aos Utilizadores com base em filtros dedicados que permitem a seleção de princípios de ordenação. Os filtros utilizados no Serviço permitem:
    1. O posicionamento dos Anúncios com base num sistema de recomendação que tem em conta os critérios de pesquisa do Utilizador e o conteúdo, os parâmetros e a exaustividade dos Anúncios (a forma de apresentação dos conteúdos por defeito);
    2. Posicionamento dos Anúncios pela data em que foram adicionados;
    3. Posicionamento dos Anúncios pelo preço do produto ou serviço oferecido.
21. As recomendações de Anúncios baseiam-se numa análise de critérios que avaliam a proximidade entre a expressão ou frase procurada pelo Utilizador e o título do Anúncio, os seus parâmetros, os filtros de pesquisa aplicados e a categoria mais relevante relacionada com a expressão ou frase. O sistema de recomendação tem em conta os seguintes parâmetros:
    1. A correspondência entre o que foi introduzido durante a pesquisa e o conteúdo e título do Anúncio;
    2. As interações do Utilizador com o Anúncio no passado;
    3. A data de criação do Anúncio;
    4. A data em que a função "Atualizar" foi aplicada ao Anúncio;
    5. Se o Anúncio se enquadra na sua categoria de pesquisa;
    6. A distância entre a localização selecionada e a localização no Anúncio;
    7. O estado do artigo (novo ou usado);
    8. O tipo de Vendedor (empresa ou particular).

Dependendo da categoria de pesquisa, dos filtros aplicados pelo Utilizador para restringir os resultados da pesquisa a parâmetros de Anúncios específicos, e das expressões ou frases pesquisadas, diferentes parâmetros podem aumentar ou diminuir a pontuação final de correspondência e a posição de Anúncios individuais nos resultados da pesquisa.

22. No âmbito do Serviço, os Utilizadores podem também receber recomendações relacionadas com outros Anúncios oferecidos por um Anunciante específico e outros Anúncios semelhantes. As recomendações relativas a outros Anúncios semelhantes baseiam-se nos critérios definidos na Cláusula 2.21 dos Termos e Condições.
23. O Serviço também apresenta conteúdos publicitários. Os conteúdos publicitários disponíveis no Serviço são apresentados de acordo com as escolhas do Utilizador relativamente à utilização de cookies ou às definições do browser. Se o Utilizador tiver consentido na personalização do conteúdo, os conteúdos publicitários apresentados são adaptados às preferências do Utilizador com base na sua atividade no Serviço. Se o OLX não tiver obtido o consentimento para a personalização do conteúdo publicitário, o conteúdo apresentado não será personalizado e irá basear-se em criações publicitárias preparadas para a base geral de Utilizadores.
24. O disposto na Cláusula 16 aplica-se ao procedimento de reclamações apresentadas pelos Utilizadores do Serviço, incluindo pessoas singulares ou coletivas que tenham denunciado conteúdos proibidos, às quais são dirigidas decisões relacionadas com o bloqueio ou suspensão de conteúdos disponíveis no Serviço. As pessoas que denunciem conteúdos proibidos podem apresentar uma reclamação relativa à decisão recebida relacionada com as ações tomadas relativamente ao bloqueio ou suspensão de conteúdos ou Contas que violem os Termos e Condições ou as leis aplicáveis, no prazo de 6 meses a contar da data de receção da decisão do OLX.

 

3. Contas

1. Para fazer uso de toda a funcionalidade do Serviço, o Visitante deve registar uma conta e utilizar o Serviço como um Utilizador com sessão iniciada. A conta dá ao Utilizador a possibilidade de utilizar, entre outras, as seguintes funcionalidades do Website:
    1. publicar e gerir Anúncios publicados;
    2. consultar os Anúncios de outros Utilizadores;
    3. Adicionar anúncios e anunciantes à lista dos seus favoritos;
    4. Adicionar uma pesquisa feita no      Website e receber notificações que ofereçam Produtos semelhantes ou novos anúncios da mesma categoria;
    5. gerir pagamentos e faturas relacionadas com os serviços prestados no Website pelo OLX;
    6. enviar e receber mensagens para e de outros utilizadores;
    7. solicitar Serviços Promocionais, de acordo com o regulamento vigente;
    8. utilizar a funcionalidade disponibilizada em conformidade com o Anexo 1 "Emprego OLX".
    9. enviar e/ou receber mensagens através da sua conta, facilitando a comunicação com outros utilizadores;
    10. a possibilidade de escolher um serviço de envio online de Produtos até 10 kg, com condições especiais, através do nosso Parceiro de transporte. O serviço de transporte selecionado pelo Utilizador é da exclusiva responsabilidade do Parceiro de transporte, estando o OLX isento de qualquer responsabilidade perante o Utilizador pela falha de serviço ou atraso no envio/entrega.
2. Conforme disposto na nossa [Política de Privacidade](https://help.olx.pt/hc/pt/articles/360000900765), para se registar e usar os nossos Serviços, terá que adotar uma das seguintes modalidades de registo:

− Registo através do seu endereço de e-mail;

− Registo através de Conta Google, caso em que recolhemos o seu nome, apelido e endereço de e-mail;

− Registo através de conta Facebook, caso em que recolhemos o seu nome e apelido, conforme constar da sua conta do Facebook e respetivo perfil. Caso tenha dado permissão ao Facebook através da opção de privacidade na aplicação, poderemos recolher ainda o seu género, idade ou endereço de e-mail, conforme as autorizações que tenha dado.

3. Poderá optar ainda por fornecer adicionalmente os seguintes dados:

− Nome ou designação social;

− Morada de residência ou domicílio profissional;

− Número de identificação fiscal;

− Número de telemóvel.

4. O Utilizador apenas pode ser uma pessoa singular com capacidade jurídica total, pessoa coletiva ou unidade organizacional sem personalidade jurídica, à qual o ato confira capacidade jurídica. No caso de pessoas coletivas e unidades organizacionais sem personalidade jurídica, deve criar uma Conta no seu nome, sendo que apenas a pessoa autorizada a agir em nome dessas entidades pode executar todas as atividades no Website. 
5. O Utilizador pode ter apenas uma conta no Serviço, pelo que a cada Anunciante ou Visitante apenas pode corresponder um registo. Podemos cancelar e bloquear definitivamente qualquer registo subsequente efetuado pelo mesmo Anunciante ou Visitante, sendo o Anunciante ou Visitante notificado para o efeito.
6. O disposto no número anterior não é aplicável nas seguintes situações:

1. O Utilizador utiliza diferentes Contas no âmbito da sua atividade profissional em conexão com o serviço dessas Contas por diferentes representantes ou ramos de atividade desenvolvidos pelo Utilizador, com a reserva de que no âmbito de qualquer das referidas Contas não há demora em pagamentos pelos serviços prestados pelo OLX, e os Anúncios colocados nas Contas não se repetem no âmbito das mesmas localidades;
2.  é necessário configurar outra conta devido à falta de possibilidade de acesso à conta (esquecimento da senha).

No entanto, todas as exclusões a este respeito serão verificadas e analisadas em detalhes pelo OLX, que tem o direito de suspender a Conta pelo tempo de verificação ou excluir as Contas na ausência de confirmação das circunstâncias que justificam a aplicação das exclusões acima. As exclusões acima não serão aplicáveis se forem utilizadas pelo Utilizador única e exclusivamente com o propósito de evitar o pagamento de contas pelos serviços prestados no Website.

7. O registo da conta requer:
    1. o preenchimento do formulário disponível no Website e o fornecimento dos dados nele solicitados, incluindo o respetivo endereço de correio eletrónico e uma palavra-passe única ou a autenticação através de um fornecedor de serviços externo como, por exemplo, o Facebook, Google ou Apple;
    2. a leitura dos Termos e Condições e dos respetivos anexos e a aceitação das respetivas provisões.
8. Após o registo, o Anunciante ou Utilizador passa a ser titular de um código de acesso à sua conta (login e password), sendo a conta do Anunciante ou Visitante pessoal e intransmissível. O titular da conta é o único responsável pelas ações efetuadas com o seu registo.
9. O OLX processa os dados pessoais dos Utilizadores, das pessoas que atuam em nome dos Utilizadores e dos Visitantes em conformidade com as provisões da [Política de Privacidade](https://help.olx.pt/olxpthelp/s/article/poltica-de-privacidade-V8) [](https://help.olx.pt/olxpthelp/s/article/poltica-de-privacidade-V8)e da [Política de Cookies](https://help.olx.pt/olxpthelp/s/article/poltica-de-cookies-26112020-V9)
10. Uma vez preenchidos os dados requeridos para o Registo, será enviada uma confirmação de Registo de Conta para o endereço de correio eletrónico indicado pelo Utilizador, com uma ligação para ativar a Conta e os Termos e Condições em vigor. O Registo é concluído quando o Utilizador ativa a sua Conta. Neste momento, é concluído um contrato de serviços de Conta. Se a ativação não for realizada no prazo de 30 Dias após a receção do correio eletrónico de confirmação do Registo de Conta, a ligação de ativação expira e a Conta não é ativada. O utilizador só poderá voltar a registar-se utilizando o mesmo endereço de correio eletrónico contactando o OLX: [ajuda@olx.pt](mailto:ajuda@olx.pt).
11. O Utilizador declara que os dados fornecidos durante o processo de Registo e a utilização do Serviço são verdadeiros, corretos e atualizados e que está autorizado a utilizar tais dados. O utilizador compromete-se a atualizar os dados se os mesmos sofrerem alterações. O OLX reserva-se o direito de bloquear uma conta se os dados fornecidos não cumprirem os requisitos acima, em conformidade com o procedimento estabelecido na Cláusula 10 dos Termos e Condições.
12. Uma pessoa que atue no Website por ou em nome de um Utilizador, que seja uma pessoa coletiva ou uma entidade organizacional sem personalidade jurídica, à qual tenha sido conferida capacidade jurídica pela lei, declara que está devidamente autorizada a agir e executar todas as atividades no Website por conta e em nome do Utilizador.
13. O utilizador compromete-se a manter os dados de acesso à conta em segredo e a protegê-los contra o acesso por terceiros não autorizados. O Utilizador deverá informar imediatamente o OLX caso tome conhecimento de qualquer acesso não autorizado à sua Conta por terceiros e, se possível, deverá alterá-la imediatamente.
14. O contrato de serviços de Conta é concluído por um período de tempo indefinido após a ativação da Conta. O contrato de assistência à Conta pode ser rescindido pelo Utilizador de acordo com as seguintes regras e nos termos da Cláusula 14:
    1. o direito de eliminar a conta não afetará o direito do Utilizador de se retratar ou rescindir o contrato, nos termos previstos da lei e para efeitos destes Termos e Condições;
    2. É possível eliminar uma conta: (i) selecionando a opção correspondente nas opções de Conta, (ii) enviando uma declaração de resolução para um endereço de correio eletrónico: [ajuda@olx.pt](mailto:ajuda@olx.pt). ou (iii) enviando uma declaração de resolução por escrito à sede do OLX;
    3. depois de o Utilizador eliminar a Conta, os restantes acordos entre o OLX e o Utilizador respeitantes aos serviços prestados no Website expirarão;
    4. A rescisão do contrato de Conta tornar-se-á efetiva no momento da sua execução (para o futuro), o que significa que os pagamentos realizados pelo Utilizador pelos serviços prestados não serão reembolsáveis. O OLX também não é obrigado a reembolsar o equivalente aos valores não utilizados e quantias já pagas para utilização dos Serviços previamente contratados (p.ex. colocação de destaques);
    5. caso o Utilizador tenha acumulado fundos no Saldo OLX, a Conta apenas poderá ser eliminada depois de levantados esses fundos ou contactando o OLX: [ajuda@olx.pt](mailto:ajuda@olx.pt)., situação da qual o Utilizador será informado quando tentar eliminar a Conta e rescindir o contrato;
    6. Uma vez eliminada a Conta ou rescindido o contrato de Conta, o Utilizador perderá o acesso às informações fornecidas ou geradas durante o período de utilização do Serviço.
15. O OLX poderá rescindir o contrato com o Utilizador, se este:
    1. não tiver iniciado sessão na Conta durante mais de 24 meses. Neste caso, o Utilizador deixará de poder utilizar a Conta eliminada. As informações sobre a rescisão do contrato serão enviadas ao Utilizador com uma antecedência mínima de 30 Dias, para o endereço de correio eletrónico indicado durante o Registo. O Utilizador poderá expressar a sua vontade de continuar a utilizar a Conta utilizando a funcionalidade disponibilizada para o efeito, devendo o Utilizador iniciar sessão na Conta. O direito do OLX de rescindir o contrato em conformidade com esta provisão não limita o direito do Utilizador de voltar a registar-se no Serviço. Contudo, o OLX não garante que o nome de Utilizador existente, associado à Conta anterior, estará disponível para utilização no novo registo;
    2. apesar de ser notificado pelo OLX para que ponha fim a alguns atos ou omissões que violem as provisões dos Termos e Condições ou as provisões da lei geralmente aplicável, continuar a agir como lhe é advertido na notificação.
16. O OLX pode ainda, a qualquer momento, cancelar ou eliminar, de forma temporária ou permanente, o registo do Anunciante ou Visitante se este: (i) não cumprir os presentes Termos e Condições; (ii) ceder a sua posição contratual a terceiros sem a nossa prévia autorização por escrito; e/ou (iii) atuar de forma a acarretar prejuízos para nós, restantes Anunciantes e/ou Visitantes do Site ou da Aplicação. Contudo, qualquer alteração será a título definitivo será comunicada num suporte duradouro aos utilizadores dentro de um prazo razoável de pré-aviso não inferior a 30 Dias.
17. Para assegurar o funcionamento adequado do Serviço, proteger e garantir a segurança das pessoas que o utilizam, o OLX reserva-se o direito de levar a cabo uma verificação adicional da validade e veracidade dos dados fornecidos pelo Utilizador e de solicitar ao Utilizador que confirme a sua identidade, as informações contidas no Anúncio ou as informações relacionadas com a transação realizada, da forma determinada pelo OLX. Se a verificação dos dados ou da identidade do Utilizador não for bem sucedida, o OLX poderá suspender ou bloquear a operação da Conta conforme as regras estabelecidas na Cláusula 10 dos Termos e Condições.
18. Direito de Resolução. O consumidor poderá, no prazo de 14 Dias após a celebração de um contrato com o OLX, terminar o referido contrato relativamente a um determinado serviço sem apresentar qualquer motivo, enviando uma declaração para esse efeito para o seguinte endereço de correio eletrónico: [ajuda@olx.pt](mailto:ajuda@olx.pt). ou por escrito para a sede do OLX. Ao terminar o contrato de serviços de Conta, os restantes contratos entre o OLX e o Utilizador ficam sem efeito automaticamente. Está especificado, nas instruções do Anexo 4 “Modelo de Formulário de Resolução”, um formulário que pode ser utilizado pelo Utilizador.
19. O OLX reserva-se o direito de converter os registos dos anunciantes não profissionais em Anunciantes Profissionais sempre que os mesmos exerçam uma atividade económica com carácter profissional ou os anunciantes adotem comportamentos característicos de anunciantes profissionais. Os indícios de comportamentos característicos de anunciantes profissionais são aferidos discriminatoriamente pelo OLX tendo em consideração diversos fatores, nomeadamente o nome de utilizador, os dados de contacto e a frequência de colocação da mesma tipologia de anúncios, a título de exemplo, os Anunciantes que se registem no OLX como profissionais ou cujos registos venham a ser convertidos em Anunciantes Profissionais e estão adstritos a um pagamento pela colocação de anúncios colocados na categoria de “Carros, Motos e Barcos”, nos termos definidos no nosso Tarifário.
20. O Contrato de serviço de Conta pode ser rescindido por um Utilizador/Anunciante Profissional em conformidade com as regras indicadas na Cláusula 3.14, em caso de não aceitação de alterações ao Termos e Condições, das quais o Utilizador será informado de acordo com o 17.1.c. Neste caso, para rescindir o Contrato de serviço de Conta, o Utilizador/Anunciante Profissional deverá notificar o OLX imediatamente e no máximo dentro de 15 Dias após o anúncio das alterações. A rescisão pelo Utilizador na modalidade de contrato acima, relativa ao serviço de Conta, entrará em vigor 15 Dias após a notificação feita ao Utilizador das alterações aos Termos e Condições, a menos que o Utilizador/Anunciante Profissional renuncie a tal notificação, apresente uma declaração relevante neste sentido ou tome uma medida explícita no Serviço/Website no sentido da aceitação e confirmação de tais condições (p. ex. publicação de um novo Anúncio).  

 **4\. Regras de publicação de Anúncios**

1. O OLX permite ao Utilizador publicar o Anúncio no Website. A publicação, pelo Utilizador, de um Anúncio no Website tem lugar após o preenchimento do formulário adequado e sob a condição de que o SMS seja verificado. A verificação por SMS é levada a cabo uma vez e consiste no envio de um código de verificação para o número de telefone indicado pelo Utilizador, código esse que o Utilizador introduz seguidamente no formulário, onde um número de telefone é utilizado para a verificação de SMS, em uma conta. A verificação por SMS pode ser repetida no âmbito de procedimentos de segurança internos.
2. Um anúncio publicado pelo Utilizador no Serviço está disponível para todos os utilizadores e visitantes do Website. Com o Anúncio, também será disponibilizado um formulário para permitir aos Utilizadores contactar o anunciante e enviar-lhe uma mensagem. Dependendo da opção do Utilizador, o respetivo número de telefone também pode ser disponibilizado ao público no Anúncio.
3. No momento da publicação do Anúncio, o Utilizador concede ao OLX uma licença não exclusiva, territorialmente ilimitada e gratuita, para registar, multiplicar e distribuir a totalidade ou parte do conteúdo dos Anúncios divulgados no Website, bem como aos parceiros do OLX através dos quais a promoção do Serviço seja levada a cabo e, também, em qualquer local através da Internet, incluindo motores de busca (como, por exemplo, o Google) e redes sociais (como, por exemplo, o Facebook). A concessão de uma licença é necessária para uma utilização completa do Serviço. Devido à natureza específica da Internet, o OLX não tem um total controlo sobre a distribuição dos conteúdos publicados ou transmitidos utilizando as funcionalidades do Serviço para outros Utilizadores e não assume qualquer responsabilidade nesta matéria por terceiros, em particular no caso da cópia e distribuição de Anúncios por tais pessoas em Websites e portais não relacionados com o presente Serviço.
4. O Anúncio é preparado pelo Anunciante, sendo o seu conteúdo da exclusiva responsabilidade do respetivo Anunciante.
5. O conteúdo de cada Anúncio deverá cumprir os requisitos estabelecidos nas Cláusulas 10 e 11 dos Termos e Condições, ser verídico, não conter ambiguidades e satisfazer os requisitos técnicos especificados pelo OLX. O Utilizador pode determinar o conteúdo do Anúncio dentro dos limites da lei e em conformidade com os seguintes requisitos:
    1. O anúncio deverá ser redigido em português e não deverá conter palavras habitualmente consideradas vulgares ou ofensivas, informações dúbias ou falsas. Na Categoria Emprego, o anúncio também pode ser preparado numa língua estrangeira;
    2. O Utilizador escolherá uma Categoria e uma subcategoria de assunto que sejam adequadas, às quais o Anúncio deverá ser atribuído;
    3. O Utilizador indicará o preço total em euros (indicando o valor do IVA, se aplicável) e especificará, possivelmente, que o preço é negociável ou que o Artigo é disponibilizado gratuitamente, ou dará a possibilidade de trocar o Artigo por outro bem ou serviço. Esta provisão não se aplica aos Anúncios das Categorias Emprego, Serviço, não sendo obrigatória a indicação da remuneração neste caso;
    4. O Utilizador, nas categorias Serviços e Emprego, apenas pode publicar um anúncio por cada freguesia;
    5. O Utilizador indicará o estado do Artigo (novo ou usado, exceto em anúncios das categorias Serviço, Empresa e Emprego);
    6. O conteúdo do anúncio deve conter uma descrição clara, precisa e completa do Artigo, incluindo informações verídicas e não enganosas sobre as suas características. É proibido transmitir esta informação sem o Serviço;
    7. O conteúdo do Anúncio não pode indicar informações de contacto, podendo as mesmas ser fornecidas apenas nos campos do formulário disponibilizados para o efeito;
    8. O conteúdo e inventário dos bens divulgados no Anúncio devem estar na posse (titularidade) do Utilizador e localizados em Portugal (exceto nos Anúncios das categorias Serviço e Emprego), incluindo, mas sem limitar a, os imóveis e veículos automóveis, que terão de estar localizados em território Português;
    9. Sem prejuízo do disposto na alínea anterior, os artigos e serviços anunciados não se podem encontrar onerados por direitos de terceiros; têm de ter disponibilidade imediata (assim, não é permitido vender artigos que não estejam na posse direta do anunciante; não sendo p. ex. aceites igualmente reservas de animais para entrega futura);
    10. Em cada anúncio, o anunciante apenas pode anunciar um Produto e uma transação relativa a esse mesmo Produto.
    11. Caso o Anunciante opte por colocar fotografias, asseguramos que poderá colocar entre 8 a 12 fotografias por anúncio, em formato JPG, PNG ou GIF, sendo que 12 fotografias será o máximo de fotografias para as categorias Carros, Motos e Barcos/ Imóveis e 8 para as demais categorias.
    12. É necessário indicar se o Anúncio diz respeito a uma única unidade, várias unidades ou um conjunto/pacote (um conjunto significa um grupo de elementos que constituem um todo). É possível adicionar informações sobre a disponibilidade do Artigo em cores diferentes ou versões equivalentes num Anúncio (exceto nas categorias Serviço, Empresa e Emprego);
    13. O mesmo Artigo pode ser publicado por apenas um Anúncio em cada momento, mas tal também se aplica aos Anúncios completados pelo utilizador no prazo de 14 Dias a contar da data de adição do Anúncio;
    14. O conteúdo do Anúncio não pode incluir informações como, em particular: anúncios, conteúdos promocionais e publicitários, endereços de websites e outros elementos quando:
        1. conduzam os Utilizadores a outros websites que prestem serviços idênticos ou equivalentes aos do OLX (isto é, serviços que publiquem ofertas ou anúncios de outros utilizadores da Internet), 
        2. remetam para outros websites que contenham softwares maliciosos e/ou que possam afetar os Utilizadores, 
        3. remetam para outros websites que contenham conteúdos ilícitos, difamatórios, e/ou que não cumpram com as boas práticas da navegabilidade na internet e da sociedade da informação.
    15. No seguimento do disposto no número anterior, o conteúdo do Anúncio só poderá conter endereços de outros websites/hyperlinks quando efetuado por um Anunciante Profissional.
    16. Ao colocar o anúncio no Website ou na Aplicação, o Anunciante poderá ocultar do anúncio os seus dados pessoais, salvo no que se refere à localização (freguesia e cidade) e nome do utilizador.
    17. A oferta relativa ao Produto publicitado pelo Anunciante ou Vendedor não obriga à conclusão da respetiva transação.
    18. Quando o objeto do Anúncio for um imóvel, não pode ser efetuada qualquer tipo de discriminação de género no conteúdo do Anúncio, nomeadamente, sem limitar, indicar que o Anúncio é dirigido especificamente a determinado género..
6. Se o Utilizador publicar um Anúncio na Categoria errada, o OLX poderá alterar a Categoria, mas se a alteração resultar na publicação de um Anúncio fora dos Limites, o Anúncio terá de ser eliminado.
7. Não é permitido apagar, de forma sistemática, anúncios e voltar a colocá-los, uma vez que tal prejudica gravemente a navegabilidade do Website e da Aplicação.
8. Não é permitido que o mesmo anúncio seja utilizado pelo Anunciante para, de forma sequencial, divulgar diferentes Produtos.
9. O Artigo do Anúncio não pode constituir:
    1. procura de Artigos (exceto em anúncios das categorias Serviço, Empresa e Emprego);
    2. uma oferta de namoro, matrimónio ou natureza sexual;
    3. mercadorias proibidas enunciadas no Anexo 5 "Artigos proibidos ou de venda condicionada" e qualquer outro bem que esteja excluído pela lei aplicável, um bem proveniente de roubo ou furto ou relativamente ao qual exista um litígio judicial ou administrativo ou um bem sujeito a procedimentos de execução ou que façam parte de um património em falência ou saneamento.
10. A publicação de um Anúncio no Website é realizada pelo Utilizador:
    1. clicando no ícone "Adicionar anúncio" ou equivalente;
    2. efetuando o pagamento (apenas para Anúncios Pagos).
11. A emissão de um Anúncio no Serviço inicia imediatamente após a sua publicação e tem uma duração de 28 Dias ou 365 Dias (quando na categoria Carros, motos e barcos | Carros), mas nunca superior, até que ocorra uma das seguintes situações:
    1. o Utilizador vendeu o Artigo;
    2. o Utilizador alterou o Anúncio de modo a indicar que diz respeito a um Artigo diferente daquele a que se referia inicialmente;
    3. Eliminou o Anúncio.
12. Caso o OLX tome conhecimento da ocorrência das situações indicadas na Cláusula 4.9.a) ou 4.9.b), o OLX terá o direito de remover o Anúncio em conformidade com os princípios especificados na Cláusula 10 dos Termos e Condições.
13. Durante o período de publicação de um Anúncio no Serviço, o Utilizador pode modificar o conteúdo e alguns dos parâmetros do Anúncio e eliminá-lo. Durante os 3 Dias antes do termo da publicação do Anúncio a contar da respetiva data de fim da publicação, o Utilizador pode prolongar o tempo de publicação do Anúncio por outros 28 Dias ou 365 Dias (quando na categoria Carros, motos e barcos | Carros), no entanto, a prorrogação será paga nas Categorias a pagar ou quando seja ultrapassado os limites de número de colocação gratuita de anúncios de uma determinada Categoria indicada no Anexo 3 "Limites de Anúncios”.
14. A possibilidade de modificação do conteúdo de um Anúncio não se aplica às alterações de preço ou de quaisquer outros parâmetros do Anúncio publicado, com o intuito de evitar o pagamento de valores devidos ao OLX, pelos serviços prestados no âmbito do Website, ou receção da ordem de devolução de fundos devidos ao Utilizador nos casos previstos nos Termos e Condições.
15. Findo o tempo de publicação do Anúncio, qualquer que seja o motivo, o Anúncio é arquivado no separador correspondente das Contas, onde estará disponível para o Utilizador por um período de 6 meses, a menos que o Utilizador elimine, ele mesmo, o Anúncio antes do termo deste período. O Utilizador deve arquivar o conteúdo do Anúncio, bem como as informações sobre as Transações concluídas.
16. Nos casos em que o Anúncio contenha hyperlinks para outros sites ou aplicações ou informações fornecidas por terceiros, estes são incluídos no Website, no mini-site OLX - e na Aplicação apenas para informação do Anunciante e do Visitante. Esses hyperlinks e essas informações não são aprovados por nós e não temos controlo sobre o conteúdo desses sites, aplicações, materiais ou informações. Deverá sempre verificar os termos de utilização e as políticas de privacidade dos sites a que aceder a partir do Site ou da Aplicação antes de os utilizar.

5 . Anúncios gratuitos

1. A publicação de um Anúncio no Website é realizada pelo Utilizador dentro dos limites da gratuitidade: Os limites relativos a Categorias específicas, incluindo os Limites para Anunciantes Profissionais, e as situações em que o limite não é aplicado são indicados      na Cláusula 3 do Anexo 3 “Limite de Anúncios”.
2. O Utilizador será informado pelo OLX quando o Limite estiver prestes a ser atingido, através de uma mensagem disponível na Conta. A publicação, pelo Utilizador, de outro Anúncio no Serviço ou de um Anúncio numa Categoria em que não seja concedido um Limite é paga.

6\. Anúncios Pagos (Taxa de Colocação)

1. A publicação pelo Utilizador de um Anúncio pago no Website é possível, depois de o Utilizador comprar:
    1. um Anúncio - em conformidade com a tabela de preços correspondente a uma dada Categoria;
    2. um Anúncio em conjunto com um pacote de Serviços Pagos – um Anúncio Premium conforme o Anexo 6 "Pacotes Básicos, Premium e Mega";
    3. pacote de Anúncios disponível na oferta do Website em conformidade com Anexo 6 "Pacotes Básicos, Premium e Mega", com a reserva de, no caso da compra de um pacote de Anúncios antes de ser atingido o limite, o pacote de Anúncios tem de ser utilizado primeiro.
2. É possível comprar um pacote de Anúncios ou um Anúncio utilizando o formulário correspondente que se encontra disponível no Website.
3. A compra de um pacote de Anúncios resulta na atribuição ao Utilizador o status de Anunciante Profissional.
4. A ativação de um Anúncio pago no Serviço terá lugar quando o pagamento tiver sido creditado na conta do OLX.
5. Os Pacotes de Anúncios incluem igualmente os Serviços Pagos indicados no Anexo 6 "Pacotes Básicos, Premium e Mega".

7\. Serviços de Promoção de Anúncios

1. Para incrementar a atratividade do Anúncio, o Utilizador pode utilizar Serviços Promocionais pagos sob a forma de Atualização do Anúncio, Destaque do Anúncio e Destaque do Anúncio na página principal do Website como se descreve em pormenor no Anexo 2 "Serviços Promocionais".
2. O Utilizador pode utilizar qualquer um dos Serviços Promocionais no momento da publicação do Anúncio ou durante a respetiva emissão.
3. Os Serviços Promocionais e respetivos pacotes são aplicados a um único Anúncio, isto é, não é possível utilizar um pacote de Serviços Promocionais para Anúncios diferentes.
4. Os Serviços Promocionais também se encontram disponíveis em pacotes, como se define na Cláusula 2 do Anexo 2 “Serviços Promocionais”. A ativação dos Serviços Promocionais tem lugar quando se começa a utilizar os Serviços Promocionais incluídos no Pacote e estarão disponíveis após o seu pagamento. As regras respeitantes aos Serviços Promocionais cobertos pelo Pacote permanecem inalteradas.
5. A aquisição de um Pacote nos termos previstos no Anexo 2 “Serviços Promocionais” não altera a validade do Anúncio publicado pelo Anunciante. 
6. O OLX não é responsável pela eficácia dos Serviços Promocionais, que se entende como um aumento efetivo do interesse no Anúncio ou no respetivo Artigo.

8\. Pagamentos

1. O Utilizador será informado de forma visível no Website acerca do preço atual dos Serviços Pagos selecionados em cada momento. Todos os preços apresentados no Website ou nas tabelas de preços são preços brutos (que incluem o imposto sobre o valor acrescentado, isto é, IVA), denominados em Euro (Eur). As tabelas de preços estão disponíveis no Centro de Ajuda do Website em https://help.olx.pt/olxpthelp/s/topic/0TO090000009u6JGAQ/lista-de-pre%C3%A7os. As tabelas de preço contêm informações sobre se um determinado Serviço Pago está abrangido por um Preço Dinâmico.
2. Os pagamentos do Serviço podem ser realizados através do sistema Paypal®, referência Multibanco®, MB WAY ou através de compensação de créditos disponíveis no saldo de conta do Utilizador (conforme a opção do Anunciante), mediante as informações que remetemos para o e-mail do Anunciante durante o processo de contratação online do Serviço de Divulgação de Destaques.
3. Se o pagamento for realizado com antecedência, a prestação do Serviço Pago apenas terá início depois de o valor do Serviço Pago ter sido integralmente creditado na conta do OLX.
4. O Utilizador, por sua opção, pode realizar um pagamento utilizando um dos métodos disponíveis de acordo com as informações fornecidas no processo de seleção de um dado serviço e através de uma instrução de pagamento no Saldo OLX.
5. O uso de qualquer um dos métodos de pagamento (exceto no caso de pagamentos a partir do Saldo OLX) exige o estabelecimento de um vínculo jurídico separado com o fornecedor de um determinado serviço de pagamento e a aceitação dos respetivos Termos e Condições. O OLX não é uma parte em tal relação e não pode interferir no seu conteúdo ou implementação. Em caso de quaisquer problemas com o pagamento, o utilizador deve contactar o operador de pagamentos relevante para esclarecer quaisquer dúvidas ou fazer uma reclamação. A título excecional, e dentro dos limites das possibilidades técnicas e organizacionais, o OLX pode, contudo, ajudar o Utilizador a esclarecer a questão. O OLX não assume qualquer responsabilidade pelo serviço de pagamento prestado por um fornecedor de serviços externos.
6. A prestação de serviços pelo OLX ao Utilizador é documentada por faturas IVA, em conformidade com os Termos e Condições aplicáveis.
7. O Utilizador está obrigado a atualizar os dados registados na Conta necessários para o Prestador de Serviços emitir uma fatura IVA pelo OLX.
8. Os recibos correspondentes a quaisquer pagamentos para efeitos de prestação do Serviço de Divulgação de Destaques são emitidos mediante solicitação expressa do Utilizador/Anunciante e enviados por e-mail após o pagamento ter sido creditado.
9. Nas condições estabelecidas no presente Termos e Condições, o OLX disponibiliza as faturas IVA ao Utilizador em formato eletrónico, garantindo a autenticidade da origem, a integridade do conteúdo e a legibilidade das faturas IVA, em particular, guardando-as num ficheiro .PDF e disponibilizando-as ao Utilizador na Conta, podendo este transferi-las. O utilizador que recebe as faturas IVA enviadas (disponibilizadas) em formato eletrónico está obrigado a guardá-las ele mesmo.
10. Em caso de fim antecipado da emissão do Anúncio devido à remoção do Anúncio pelo Utilizador, ao facto de o Artigo ter sido vendido ou à alteração, pelo Utilizador, do assunto do Anúncio de uma forma que indique tratar-se de um Artigo diferente do inicial, não será reembolsado o valor cobrado pela publicação do Artigo e pelos Serviços Pagos relacionados correspondente ao período não utilizado.
11. Os Anúncios que sejam ilegais ou violem as provisões do      Termos e Condições, em particular: inseridos na categoria errada, equivalentes ou duplicados, repetidos com frequência (do tipo spam), considerados ofensivos, com conteúdo pornográfico ou erótico, conteúdo que possa ser interpretado como promoção de atividades sexuais ou eróticas em troca de dinheiro, que constituam uma tentativa de fraude ou violem qualquer direito de propriedade intelectual (copyright) serão removidos, e o valor cobrado pela publicação do Anúncio e pelos Serviços Pagos relacionados com o Anúncio será reembolsado na conta do Utilizador sob a forma de pontos para reutilização do Serviço no prazo de 90 Dias a contar do momento da remoção do Anúncio (cada ponto corresponde a 1 EUR). A pedido do Utilizador, o valor pode ser devolvido em dinheiro, diretamente para a conta bancária a partir da qual o pagamento tenha sido efetuado ou de acordo com o método de pagamento utilizado.

9\. Serviço Saldo OLX

1. O OLX permite ao Utilizador, na Conta serviço, utilizar gratuitamente o serviço Saldo OLX, que simplifica os pagamentos dos Serviços Pagos ao OLX. Para evitar dúvidas, o serviço Saldo OLX não constitui um serviço de pagamento e as unidades atribuídas no Saldo OLX não constituem moeda eletrónica no sentido das provisões relativas à prestação de serviços de pagamento.
2. O uso do Saldo OLX baseia-se nas seguintes regras:
    1. O Utilizador deve "creditar" o Saldo OLX, depositando fundos na conta do OLX, numa das moedas aceites de acordo com as informações fornecidas no Serviço;
    2. Uma vez creditado o pagamento na conta do OLX, este atribui ao Saldo OLX de um determinado Utilizador a quantidade de unidades equivalente ao montante pago. Esta quantidade é atualizada continuamente após cada operação realizada pelo Utilizador que resulte numa alteração do seu montante (o recarregamento resulta num aumento da quantidade de unidades no Saldo OLX), o pagamento do Serviço Pago selecionado resulta numa diminuição da quantidade de unidades existente no Saldo OLX);
    3. O Utilizador é inteira e exclusivamente livre de dispor dos fundos atribuídos ao seu Saldo OLX durante toda a vigência do contrato de serviços de Conta, desde que a Conta esteja ativa;
    4. no caso da realização de pagamentos de um determinado Serviço Pago a entidades a partir do Saldo OLX, o OLX deduzirá o preço devido pela prestação desse Serviço Pago, debitando o valor adequado no Saldo OLX do Utilizador (redução de saldo).
3. O Utilizador pode, a qualquer momento, solicitar o reembolso dos fundos carregados no Saldo OLX, num montante que não exceda o saldo apresentado no Saldo OLX, tendo em conta o pagamento e os reforços que tenha efetuado. Este pedido é realizado através de correio eletrónico ([ajuda@olx.pt](mailto:ajuda@olx.pt)) ou por escrito ao OLX.
4. A devolução de fundos do Saldo OLX será realizada, no prazo de 14 Dias, a contar da data de receção do pedido, através de transferência para a conta bancária a partir da qual o pagamento tenha sido efetuado ou em conformidade com o método de pagamento utilizado para efetuar o pagamento. O pedido de devolução dos fundos do Saldo OLX não se aplica aos pontos promocionais atribuídos à conta como parte de ações promocionais, concursos, pontos de desconto e "cashbacks" em Pacotes Premium, em conformidade com Anexo 6 "Pacotes Básicos, Premium e Mega". Os pontos promocionais são sempre atribuídos com base em Termos e Condições separados das promoções, são válidos durante 90 Dias a contar do momento em que são atribuídos e são utilizados em conformidade com as regras especificadas nos referidos Termos e Condições.

10 . Ações ilícitas dos Utilizadores e incompatíveis com os Termos e Condições

1. O Utilizador pode usar o Serviço em conformidade com a sua finalidade, dentro dos limites da lei e das boas práticas da sociedade de informação, respeitando os direitos e interesses dos outros. Em particular, o Utilizador compromete-se a
    1. não executar ações que possam interferir com o correto funcionamento do Serviço, incluindo não interferir com o conteúdo do Serviço, a Conta ou Contas dos Utilizadores ou os elementos informáticos do Serviço;
    2. não executar ações ilegais utilizando as funcionalidades do Serviço, incluindo envios ou publicações, conteúdo que viole a lei, direitos pessoais, conteúdo que apresente ou distribua pornografia infantil ou conteúdo terrorista ou que viole os direitos de propriedade intelectual de outros, bem como conteúdo de natureza discriminatória ou racista;
    3. não realizar, utilizando as funcionalidades do Website, a venda (ou não executar outras ações com efeito equivalente) de Artigos enunciados no Anexo 5 "Artigos proibidos ou de venda condicionada" nem realizar a venda de Artigos que não cumpram as condições exigidas em conformidade com o Anexo 5, bem como outros Artigos cuja comercialização esteja excluída pela lei aplicável e Artigos provenientes de furto ou roubo ou relativamente aos quais exista um litígio legal ou administrativo pendente ou Artigos sujeitos a procedimentos de execução ou incluídos no património do falido ou que visem o seu saneamento;
    4. não enganar os utilizadores do Serviço e o OLX, p. ex., fornecendo informações falsas sobre o Artigo ou ocultando informações relevantes;
    5. abster-se de agir de má fé, abusar da funcionalidade do Serviço, utilizar o Serviço de uma forma diferente do seu objetivo ou que viole os Termos e Condições.
    6. abster-se de efetuar qualquer tipo de discriminação de género no conteúdo de Anúncios que tenham por objeto imóveis, nomeadamente, sem limitar, indicar que o Anúncio é dirigido especificamente a determinado género.
2. O Utilizador está obrigado a observar a obrigação especificada na Cláusula 10.1 dos Termos e Condições em cada fase de utilização do serviço, relativamente a cada funcionalidade proporcionada, incluindo, em particular, no caso de publicação de Anúncios e envio de mensagens a outros utilizadores.
3. A publicação ou transmissão de qualquer conteúdo do Website é realizada automaticamente, utilizando a interface do Website e sem a participação do OLX, sendo que o OLX não verifica automaticamente todos os conteúdos transmitidos utilizando a funcionalidade do Website.
4. Caso o Utilizador efetue alterações ao conteúdo do Anúncio após a respetiva publicação no Serviço, o OLX está autorizado a investigar se o conteúdo atualizado viola os Termos e Condições e a tomar as medidas adequadas, incluindo a remoção do conteúdo que viola os Termos e Condições      .
5. Cada pessoa que utilize o Serviço tem a oportunidade de comunicar ao OLX qualquer conteúdo que seja ilegal ou contrário às regras e aos Termos e Condições, distribuído utilizando as funcionalidades do Serviço, através da funcionalidade "comunicar uma violação" que se encontra disponível no Anúncio ("Comunicação de uma violação"). O Notificador deve, tanto quanto possível, fornecer dados que permitam ao OLX verificar o Relatório de Abuso, incluindo, em particular, o ID da comunicação, uma explicação dos motivos pelos quais considera o conteúdo ilegal ou contrário aos Termos e Condições bem como as suas informações de contacto se solicitado no formulário do relatório.
6. Caso seja recebida uma mensagem fidedigna (através de uma comunicação de abuso ou de qualquer outro meio) de que o conteúdo enviado ou publicado utilizando a funcionalidade do Serviço é ilegal ou viola as provisões do Termos e Condições, o OLX pode:
    1. bloquear imediatamente o conteúdo em questão, incluindo em particular o Anúncio e os Serviços Promocionais relacionados. Em caso de bloqueio, o conteúdo não estará visível ou disponível para as pessoas que utilizam o Serviço ou no painel da Conta de um dado Utilizador;
    2. tomar medidas investigatórias no sentido de determinar as circunstâncias do caso, agindo proporcional e razoavelmente, incluindo, por exemplo, contactar o denunciante, o fornecedor do conteúdo, as autoridades competentes ou um consultor externo;
    3. informar, em caso de bloqueio de um determinado conteúdo, o mais tardar quando o bloqueio se tornar efetivo, o fornecedor desse conteúdo, através de correio eletrónico, indicando o conteúdo bloqueado, os motivos da decisão de bloqueio (incluindo o conteúdo do Relatório de Abuso após a sua anonimização, se tal for exigido pela lei geralmente aplicável) e indicando a base da decisão com referência ao conteúdo do Termos e Condições;
    4. As disposições previstas na Cláusula 10.6.c) não serão aplicáveis em virtude de notificação da autoridade pública competente, por imposição legal ou  necessidade de garantir a segurança de vida e de saúde das pessoas ou do Estado, e que resulte numa obrigação de não divulgar a informação referida      nesta      Cláusula.
7. Os Anunciantes Profissionais podem contestar a decisão de bloquear o conteúdo por eles transmitido ou publicado, utilizando as funcionalidades do Serviço, no sentido de obstar o fornecimento de informações sobre o bloqueio de um determinado conteúdo pelo OLX. Aplicar-se-ão as provisões que regem o procedimento de reclamação.
8. Se a objeção referida na Cláusula 10.7. for considerada justificada ou em caso de dúvida de que as circunstâncias justifiquem a ilegalidade ou violação dos Termos e Condições, sobre um determinado conteúdo, tenham deixado de existir:
    1. o conteúdo bloqueado é restaurado sendo disponibilizado através de uma cópia provisória do Anúncio ou mensagem disponível na Conta. O Utilizador poderá optar por publicá-lo ou enviá-lo através do Serviço;
    2. se o Anúncio se encontrava coberto por um dos Serviços Pagos, a comissão previamente cobrada pela publicação do Anúncio e pelos Serviços Pagos relacionados com o Anúncio em questão será devolvida na conta do Utilizador, sob a forma de pontos utilizados para reutilização dos serviços do Website no prazo de 90 Dias a contar do momento da remoção do Anúncio (um ponto é equivalente a 1 EUR). A pedido do Utilizador, o valor pode ser devolvido em dinheiro, diretamente para a conta bancária a partir da qual o pagamento tenha sido efetuado ou de acordo com o método de pagamento utilizado.
9. Se a objeção referida na Cláusula 10.7 for considerada injustificada ou o Anunciante Profissional não contestar no prazo de 7 Dias após obter as informações especificadas na      Cláusula 10.6.c.:
    1. O conteúdo bloqueado será removido irrevogavelmente do Serviço;
    2. Se o conteúdo em questão se encontrava coberto por um dos Serviços Pagos, a comissão previamente cobrada pela publicação do Anúncio e pelos Serviços Pagos relacionados com o Anúncio em questão será devolvida na conta do Utilizador, sob a forma de pontos utilizados para reutilização dos serviços do Website no prazo de 90 Dias a contar do momento da remoção do Anúncio (um ponto é equivalente a 1 EUR). A pedido do Utilizador, o valor pode ser devolvido em dinheiro, diretamente para a conta bancária a partir da qual o pagamento tenha sido efetuado ou de acordo com o método de pagamento utilizado.
10. O OLX pode remover o Anúncio ou bloquear a conta caso exista uma suspeita de possibilidade de ocorrência de atos que ameacem a segurança dos outros Utilizadores do Serviço através do Anúncio ou da Conta e, também, no caso de o Anúncio afetar negativamente o bom nome do OLX ou prejudicar o OLX de outro modo.
11. Em caso de violação grave da lei utilizando o Serviço, bem como de violação grave repetida das provisões dos Termos e Condições, em particular no caso de tentativas repetidas de publicar ou disseminar conteúdo ilegal por parte de um Utilizador ou vários Utilizadores atuando em conjunto ou em acordo, incluindo o uso de várias Contas, o OLX pode, de acordo com os princípios de proporcionalidade e respeito à liberdade      contratual e autonomia privada, suspender ou bloquear temporária ou permanentemente a Conta ou Contas, que será equivalente a uma suspensão temporária da prestação dos serviços ao Utilizador, tendo em conta as seguintes regras:
    1. A suspensão da conta significa a interrupção temporária da sua funcionalidade. O Utilizador cuja Conta tenha sido suspensa perde a possibilidade de utilizar ativamente o serviço, o que significa que não pode enviar mensagens utilizando a respetiva funcionalidade, publicar Anúncios ou realizar Transações. Pode, no entanto, consultar o Serviço, o histórico de mensagens e os Anúncios publicados, bem como fazer depósitos no seu Saldo OLX. Contudo, para levantar os fundos que lhe estejam atribuídos, deverá contactar a Assistência ao Cliente/Centro Helpdesk através do seguinte endereço de correio eletrónico: ajuda@olx.pt;
    2. O bloqueio de uma conta significa que o Utilizador deixa de poder iniciar sessão na Conta.
12. O Utilizador será informado em caso de bloqueio ou suspensão de um determinado conteúdo ou da Conta, o mais tardar quando a decisão se tornar efetiva, através de uma mensagem de correio eletrónico. Esta notificação irá incluir detalhes sobre o conteúdo ou a Conta bloqueado ou suspenso, os motivos da decisão e referências à Cláusula dos Termos e Condições que justificam a decisão.
13. Caso o Utilizador discorde da decisão de bloqueio de conteúdo ou da Conta poderá apresentar reclamação nos termos da Cláusula 3 dos Termos e Condições.  Para evitar qualquer confusão, o facto de ser ou não apresentada uma objeção efetiva nas situações especificadas nas Cláusulas 10.6 ou 10.11 dos Termos e Condições não afeta o direito do Utilizador de apresentar uma queixa em conformidade com a Cláusula 15 dos Termos e Condições. 
14. O OLX pode bloquear ou suspender conteúdos ou Contas com base em políticas internas e procedimentos de moderação. Essas política internas e procedimentos de moderação adotados pelo OLX definem os métodos e meios de identificação de conteúdos ou ações dos Utilizadores que serão examinados por violação dos Termos e Condições ou das leis aplicáveis. O objetivo principal das políticas e procedimentos de moderação é garantir a segurança dos Utilizadores e combater abusos.
15. O processo de identificação do conteúdo ou das ações do Utilizador pode basear-se em ferramentas de moderação automatizadas ou semi-automatizadas. Estas ferramentas permitem a identificação de conteúdos ou ações que violam ou podem violar os Termos e Condições e as leis aplicáveis, analisando o conteúdo dos Anúncios ou a atividade do Utilizador no Serviço. As ferramentas utilizadas no processo de moderação dos conteúdos disponibilizados no Serviço utilizam soluções que permitem a análise do conteúdo dos Anúncios ou a identificação de ações que possam violar os Termos e Condições ou as leis aplicáveis, reportando-as às equipas internas de moderação do OLX. As decisões relacionadas com as violações dos Termos e Condições referidas na Cláusula 2 ou as ações relacionadas com a violação da segurança do Serviço ou dos Utilizadores podem ser tomadas de forma totalmente automatizada. Outras decisões relacionadas com o conteúdo ou ações dos Utilizadores estão sujeitas a avaliação pelas equipas de moderação do OLX. As ações relacionadas com conteúdos ou atividades de Utilizadores que violem ou possam violar os Termos e Condições ou as leis aplicáveis são também tomadas em resposta a denúncias apresentadas por outros Utilizadores, autoridades ou organizações dedicadas à segurança dos utilizadores da Internet ou ao combate a conteúdos ilegais.

11\. Conteúdo obrigatório de certos anúncios

1. O anúncio deve estar em língua portuguesa e deve ser legível para os demais utilizadores, sem violar as disposições da lei geralmente aplicáveis, nomeadamente:

1. palavrões, conteúdo obsceno e pornográfico ou fomentar a disseminação de ódio, racismo, xenofobia e conflitos entre nações;
2. endereços de websites ou hyperlinks para outros sites, nos seguintes casos:
    1. sejam introduzidos em Anúncios de Utilizadores Particulares;
    2. conduzam os Utilizadores outros websites que prestem serviços idênticos ou equivalentes aos do OLX (isto é, serviços que publiquem ofertas ou anúncios de outros utilizadores da Internet); ou,
    3. remetam para outros websites que contenham softwares maliciosos e/ou que possam afetar os Utilizadores; ou,
    4. remetam para outros websites que contenham conteúdos ilícitos, difamatórios, e/ou que não cumpram com as boas práticas da navegabilidade na internet e da sociedade da informação;
3. conteúdo publicitário ou outro conteúdo comercial;
4. dados de Utilizadores ou de outras pessoas singulares, em especial: nome e sobrenome, local de residência, número de telefone ou endereço de e-mail, número da conta bancária;
5. conteúdo que viole direitos de terceiros, incluindo direitos de propriedade intelectual (“trademarks/copyrights”), decência, direitos pessoais, incluindo o bom nome, a reputação de outros utilizadores ou de terceiros;
6. conteúdo que constitua o resultado de um contrato com outros utilizadores ou  
    terceiros para efeitos de classificação ou opinião;
7. conteúdo falso, difamatório ou que constitua um ato de concorrência desleal.

2. Caso o item objeto de um anúncio seja um imóvel ou parte de um imóvel, como é, nomeadamente, um quarto, divulgado para efeitos de utilização como alojamento local, o anúncio deve indicar o número de registo do estabelecimento de alojamento local, nos termos do disposto no Decreto-Lei n.º 128/2014, de 29 de agosto, conforme alterado.
3. Os anúncios relativos a animais devem respeitar o disposto na lei aplicável, nomeadamente, o disposto no Decreto-Lei n.º 276/2001, de 17 de outubro, conforme alterado. Em particular, qualquer anúncio relativo à transmissão de animais, a título oneroso, deve conter obrigatoriamente a seguinte informação: (i) idade do animal; (ii) tratando-se de cão ou gato, a indicação se é animal de raça pura ou indeterminada, sendo que, tratando-se de animal de raça pura, deve obrigatoriamente ser referido o número de registo no livro de origens português; (iii) número de identificação eletrónica da cria e da fêmea reprodutora; (iv) número de inscrição de criador; e (v) o número de animais da ninhada.
4. Os anúncios relativos a equipamentos elétricos e eletrónicos e os equipamentos de rádio e equipamentos terminais de telecomunicações, devem respeitar o disposto na lei aplicável, nomeadamente, o disposto no Decreto-Lei n.º 31/2017, de 22 de março, conforme alterado e Decreto-Lei n.º 57/2017, de 9 de junho, respetivamente. Em particular, qualquer anúncio relativo à disponibilização no mercado ou à oferta de equipamentos de rádio para distribuição, consumo ou utilização no mercado da UE no âmbito de uma atividade comercial, a título oneroso ou gratuito, devem obedecer necessariamente os requisitos essenciais disponíveis para consulta na [Checklist para Operadores Económicos.](https://www.anacom.pt/streaming/Flyer_RED%26CEM_vers%E3o+final_30abril2021.pdf?contentId=1635848&field=ATTACHED_FILE) 
5. Sem prejuízo do disposto no número anterior, a consulta da Checklist não dispensa a consulta dos respetivos diplomas disponíveis em [Regime RED e CEM](https://www.anacom.pt/render.jsp?categoryId=392414) no [sítio da ANACOM](https://www.anacom.pt/). A ANACOM tem competências de fiscalização no cumprimento da legislação em referência, sem prejuízo de competências atribuídas a outras entidades. Para tal, procede à análise da marcação, dos manuais e das instruções e informações constantes dos aparelhos, bem como à análise da documentação técnica (manuais, dossiês técnicos de construção e declarações de conformidade), no sentido de verificar o cumprimento dos requisitos dispostos nos referidos diplomas, bem como nos anúncios em linha. Procede ainda à recolha de aparelhos para efetuar ensaios laboratoriais (em laboratórios acreditados para o efeito).
6. Neste sentido, os anunciantes que divulguem os equipamentos em questão são obrigados a facilitar aos agentes da ANACOM, ou às entidades por esta mandatadas, a verificação dos equipamentos de rádio e a fornecer a informação necessária à verificação e fiscalização das obrigações resultantes do regime RED e CEM, facilitando o acesso à documentação e equipamentos nos termos da legislação em vigor. O Utilizador possui 10 Dias para disponibilizar a informação necessária à verificação de conformidade ou à avaliação do aparelho anunciado no Website que abranja todos os requisitos pertinentes. Sem prejuízo de outro prazo legal aplicável, e após o referido prazo, se a avaliação de conformidade não for concluída com sucesso por insuficiência de elementos que permita a avaliação em causa ou quando existam indícios suficientes de não conformidade, os anúncios serão suspensos pela falta de informação/requisitos técnicos ou pela ausência de conformidade.

7. Apenas podem ser anunciados produtos alimentares por Anunciantes detentores de licença ou que tenham efetuado o procedimento de mera comunicação prévia, conforme aplicável, e que se encontrem legalmente habilitados para o exercício desta atividade, nos termos e para os efeitos da lei aplicável, nomeadamente, nos termos do disposto no Decreto-Lei n.º 169/2012, de 1 de agosto.
8. No caso de anúncios relativos a ofertas de emprego, o Anunciante, tratando-se de agência, terá que, obrigatoriamente, divulgar os seus dados de identificação e a eventual existência de caução ou instrumento financeiro equivalente com a finalidade de garantir o repatriamento de candidato a emprego colocado fora do território nacional.
9. No caso de anúncios relativos a imóveis cujos Anunciantes sejam uma agência imobiliária ou um consultor imobiliário, nos termos e para os efeitos da Lei n.º 15/2013, de 8 de fevereiro, conforme alterada, o anúncio deve indicar a denominação comercial e o número da respetiva licença de mediação imobiliária.
10. No caso de anúncios relativos a arrendamento habitacional de imóveis, o Anunciante terá que, obrigatoriamente, indicar o número da licença ou a autorização de utilização do imóvel, a tipologia, bem como a área útil do imóvel em causa. Caso o anúncio seja referente a arrendamento habitacional de parte de um imóvel, deverá ser indicado o número de quartos disponibilizados em arrendamento, bem como a área útil a utilizar pelo potencial inquilino (área arrendada em exclusividade e área de utilização comum).
11. As informações referidas acima serão obrigatoriamente visíveis nos anúncios relativos àqueles Produtos para garantir a transparência e segurança dos consumidores/utilizadores.
12. Aplicam-se aos anúncios aos quais são associados Serviços de Divulgação de Destaques as regras aplicáveis ao conteúdo dos anúncios inseridos.

 12 .Avaliações e Opiniões - beta

1. A classificação dos Produtos na plataforma tem um impacto importante na escolha do consumidor e, por conseguinte, no sucesso das transações dos anunciantes profissionais que oferecem esses Produtos e Serviços aos consumidores.
2. O sistema de classificação e opiniões está na versão de produção - Beta, o que significa que nem todas as funcionalidades ou soluções descritas infra funcionam corretamente ou estão disponíveis a todos os utilizadores.
3. O      Website fornece um sistema de classificação e feedback que permite o utilizador deixar uma avaliação subjetiva ao outro utilizador relativamente à experiência com a transação estabelecida entre eles. Por experiência com a transação, entende-se, em particular: a apresentação pelo anunciante de uma descrição clara e fidedigna do Produto, a forma e o método de comunicação entre os utilizadores, capacidade de resposta, o empenho, o tempo de envio ou de entrega do Produto adquirido e a pontualidade. A base para a avaliação é o contacto feito entre os utilizadores da sua Conta, e não necessariamente a conclusão de uma Transação.
4. A classificação é feita na forma, data e categoria indicadas no      Website. A classificação será apresentada de uma escala de 1 a 10 (com uma casa decimal) com base na média de todas as classificações atribuídas pelos utilizadores.
5. As opiniões descritivas estarão visíveis apenas para o utilizador que está a ser avaliado, através da sua conta. Além disso, a classificação constituirá uma funcionalidade complementar para verificar as ações dos utilizadores do OLX.
6. Se mais de uma classificação for atribuída a um determinado Utilizador, apenas a última classificação atribuída será incluída na classificação média calculada para um determinado Utilizador.
7. A opinião não pode violar as disposições da lei geralmente aplicáveis, nomeadamente:

1. palavrões, conteúdo obsceno e pornográfico ou fomentar a disseminação de ódio, racismo, xenofobia e conflitos entre nações;
2. endereços de sites ou hyperlinks para outros sites;
3. conteúdo publicitário ou outro conteúdo comercial;
4. dados de Utilizadores ou de outras pessoas singulares, em especial: nome e sobrenome, local de residência, número de telefone ou endereço de e-mail, número da conta bancária;
5. conteúdo que viole direitos de terceiros, incluindo direitos de propriedade intelectual, decência, direitos pessoais, incluindo o bom nome, a reputação de outros utilizadores ou de terceiros;
6. conteúdo que constitua o resultado de um contrato com outros utilizadores ou  
    terceiros para efeitos de classificação ou opinião;
7. conteúdo falso, difamatório ou que constitua um ato de concorrência desleal.

8. O OLX tem o direito de desconsiderar a avaliação atribuída na classificação média do  
    utilizador ou excluir a opinião total ou parcialmente quando a avaliação ou opinião:

1. fizer referência a um utilizador que não seja o avaliado;
2. for atribuída pelo próprio utilizador;
3. for atribuída por funcionários, parentes, familiares do Utilizador;
4. for atribuída como resultado do contacto com o Utilizador apenas com a finalidade de atribuir uma avaliação e opinião (por exemplo, para aumentar ou subestimar artificialmente a credibilidade do Utilizador);
5. for atribuída a partir de uma conta criada com base em um endereço de e-mail temporário;
6. for atribuída por forma a indicar a automação do problema ou por meio de soluções que evitam a análise do tráfego de rede;
7. salvo disposição em contrário, viole os Termos e Condições.

1. Em termos de conteúdo de opiniões, após a sua emissão, o Utilizador concede ao OLX uma licença não exclusiva, territorialmente ilimitada e gratuita, nos termos estabelecidos na Cláusula 4.3.

13\. Teste de Preço

1. Poderão ser realizados, ocasionalmente, testes aos preços dos Serviços praticados no      Website e na Aplicação. A realização dos testes aos preços dos Serviços conduzi-lo-á a diferentes versões do Website ou da Aplicação onde os preços praticados pelos Serviços disponibilizados são distintos entre si.
2. Poderão também ser definidos, em função dos referidos testes, Serviços diferenciados não disponíveis a todos os Anunciantes.
3. Os dados recolhidos por estes testes serão depois tratados estatisticamente, contribuindo para a definição de preços mais ajustados às expetativas dos Anunciantes e consequentemente para a prestação de um melhor serviço aos mesmos.
4. Nesta medida, o Anunciante reconhece, ao aceitar os presentes Termos e Condições, que estes testes têm natureza aleatória, não havendo qualquer espécie de critério para a alocação de um dado Anunciante numa das versões do Website ou da Aplicação criadas para realizar os referidos testes aos preços.
5. O Anunciante reconhece ainda que a realização de testes aos preços não implica que lhe esteja a ser dado um tratamento desinformado ou discriminatório em relação às ofertas disponibilizadas a outros Anunciantes.
6. O Anunciante que deseje contratar connosco com base nas demais condições disponibilizadas no âmbito das ações de testes aos preços referidas acima poderá contactar-nos pelos meios de contacto previstos nestes Termos e Condições.
7. Nunca serão cobrados ao Anunciante preços mais altos do que os constantes do tarifário em vigor em cada momento em resultado da realização de testes de preço, limitando-se eventuais variações de preço, quando se trate de Serviço de Divulgação de Destaques, a reduções dos preços que podem ir de > 0% a </= 30% face ao preço praticado a cada momento.

14 . Direito de resolução

1. Direito de livre resolução
2. O Anunciante tem o direito de livre resolução do contrato relativo à prestação dos Serviços no prazo de 14 Dias de calendário contados da data de conclusão do procedimento de contratação online no Website ou na Aplicação, sem necessidade de indicar qualquer motivo.
3. O prazo para exercício do direito de livre resolução expira 14 Dias a contar do dia seguinte ao dia da conclusão do procedimento de contratação online no Website ou na Aplicação.
4. A fim de exercer o seu direito de livre resolução, tem de comunicar à OLX PORTUGAL, S.A., através da Aplicação ou ainda  através do nosso [formulário de contacto](https://help.olx.pt/olxpthelp/s/contactsupport#top) ou por carta registada com aviso de receção, a sua decisão de resolução do contrato por meio de uma declaração inequívoca. Pode também utilizar o “Modelo de Formulário de Resolução”, que consta do Anexo 4. Contudo, o Anunciante pode exercer o direito de livre resolução por qualquer meio, não sendo obrigatória a utilização de qualquer dos meios referidos.
5. Para que o prazo de livre resolução seja respeitado, basta que a sua comunicação referente ao exercício do direito de livre resolução seja enviada antes do termo do prazo de resolução.

2. Exceção ao direito de livre resolução

1. Caso o Anunciante tenha aceitado através do procedimento de contratação online no Website ou na Aplicação que pretende que a prestação dos Serviços escolhidos se inicie durante o prazo previsto para a livre resolução (ou seja, 14 Dias), daremos início à prestação dos Serviços após a boa receção do pagamento dos mesmos.
2. Neste caso, e na medida em que o anúncio já se encontre publicado no Website ou na Aplicação, o Anunciante não pode livremente resolver o seu contrato. O Anunciante reconhece que perde o direito à livre resolução uma vez que os Serviços já se encontrem plenamente executados, ou seja, a partir do momento em que o anúncio tenha sido publicado.
3. O Anunciante pode sempre cancelar o seu anúncio em qualquer momento, não havendo direito ao reembolso pela não utilização total do Período de Colocação do mesmo, sendo o Anunciante reembolsado proporcionalmente pela utilização que foi dada.

3. Efeitos da livre resolução

1. Em caso de livre resolução lícita do contrato e após ter pago o preço dos Serviços contratados (ou seja, se tiver solicitado os Serviços e tiver pago os mesmos, mas o seu anúncio ainda não se encontrar publicitado), ser-lhe-ão reembolsados todos os pagamentos efetuados, sem demora injustificada e, em qualquer caso, o mais tardar 14 Dias a contar da data em que formos informados da sua decisão de resolução do contrato. Efetuamos esses reembolsos transferindo os montantes a reembolsar para o mesmo meio de pagamento por si utilizado no Website ou na Aplicação.
2. Após o exercício da livre resolução, o consumidor abstém-se de utilizar os conteúdos ou serviços digitais e de os colocar à disposição de terceiros.

4. Alterações aos Termos e Condições     

1. Em caso de alteração às cláusulas gerais dos presentes Termos e Condições do OLX, com exceção das propostas de alteração de redação (que não alterem o seu conteúdo), e no seguimento da notificação do OLX com o mínimo de antecedência de 15 Dias, o Anunciante profissional, caso assim o entenda, terá o direito de resolver o contrato no prazo de 15 Dias após a receção da referida notificação de alteração, salvo se se aplicar ao contrato um prazo mais curto, por exemplo, em aplicação do direito civil nacional.

15 . Lei aplicável e resolução de litígios

1. A lei aplicável ao Website, à Aplicação e aos presentes Termos e Condições é a lei portuguesa.
2. O Utilizador poderá entrar em contato com o OLX, por escrito, sobre os Serviços através de um dos seguintes meios de contacto:

1. para o seguinte endereço: Edifício Atrium Saldanha, Praça Duque de Saldanha, N.º 1, 6.º andar, 1050 – 094 Lisboa; ou
2. eletronicamente através do formulário de contato disponível no Web     Site;

3. Os tribunais portugueses são exclusivamente competentes para dirimir quaisquer litígios relacionados com a utilização do Website e da Aplicação e com os presentes Termos e Condições.
4. Informa-se os consumidores de que, nos termos e para os efeitos do disposto no artigo 14.º da Lei n.º 24/96, de 31 de Julho, conforme alterada, os conflitos de consumo cujo valor não exceda a alçada dos tribunais de primeira instância (i.e., € 5.000,00), estão sujeitos a arbitragem necessária ou mediação quando, por opção expressa do consumidor, sejam submetidos à apreciação de tribunal arbitral adstrito aos centros de arbitragem de conflitos de consumo legalmente autorizados.
5. Nos termos e para os efeitos do disposto no artigo 18.º da Lei n.º 144/2015, de 8 de Setembro, conforme alterada, informa-se todos os consumidores de que a OLX Portugal, S.A. se encontra vinculada, por imposição legal decorrente de arbitragem necessária, aos tribunais arbitrais adstritos aos centros de arbitragem de conflitos de consumo legalmente autorizados, relativamente a conflitos de consumo cujo valor não exceda a alçada dos tribunais de primeira instância (i.e., € 5.000,00) quando, por opção expressa do consumidor, estes tenham sido submetidos à apreciação dos referidos tribunais arbitrais.
6. Os Utilizadores do Serviço, incluindo pessoas singulares ou coletivas que tenham denunciado conteúdos ilegais, aos quais são dirigidas decisões relacionadas com o bloqueio ou a suspensão de conteúdos disponíveis no Serviço, têm o direito de escolher qualquer organismo extrajudicial de resolução de litígios que tenha recebido um certificado emitido pelo Coordenador dos Serviços Digitais.

Em situação de litígio, o consumidor pode recorrer aos centros de arbitragem de conflitos de consumo legalmente autorizados, nomeadamente:

− Centro de Arbitragem de Conflitos de Consumo de Lisboa

(CACCL)

Morada: Rua dos Douradores, n.º 116 – 2.º, 1100 - 207 Lisboa

Telef.: +351 218807030

Fax: +351 218807038

E-mail:[juridico@centroarbitragemlisboa.pt](mailto:juridico@centroarbitragemlisboa.pt)

Sítio electrónico: [http://www.centroarbitragemlisboa.pt/](http://www.centroarbitragemlisboa.pt/)

− Centro de Informação de Consumo e Arbitragem Porto

(CICAP)  
Morada: Rua Damião de Góis, 31, Loja 6, 4050-225, Porto

Telef.:+351 225508349/+351225029791

Fax:+351 225026109

E-mail: [cicap@cicap.pt](mailto:cicap@cicap.pt)  
Sítio electrónico: [http://www.cicap.pt/](http://www.cicap.pt/)

− Centro de Arbitragem de Conflitos de Consumo do Ave, Tâmega e Sousa

(TRIAVE)  
Morada: Rua Capitão Alfredo Guimarães, 1, 4800-019 Guimarães

Telef.:+351 253422410

Fax:+351   253422411

E-mail: [triave@gmail.com](mailto:triave@gmail.com)  
Sítio electrónico: [http://www.triave.pt/](http://www.triave.pt/)

− Centro Nacional de Informação e Arbitragem de Conflitos de Consumo

(CNIACC)  
Morada: Rua D. Afonso Henriques, 1, 4700-030 Braga

Telef.: +351 253619107

E-mail: [geral@cniacc.pt](mailto:geral@cniacc.pt)  
Sítio electrónico: [https://www.cniacc.pt/pt/](https://www.cniacc.pt/pt/)

Em situação de litígio relacionada com veículos automóveis, o consumidor pode recorrer ao:

− Centro de Arbitragem do Sector Automóvel

(CASA)

Tel.: 217951696 e 217827330;

Endereço eletrónico:[http://www.arbitragemauto.pt/](http://www.arbitragemauto.pt/)

  
Para mais informações, os consumidores poderão consultar o [Portal do Consumidor](http://www.consumidor.pt/) ou ainda aceder à plataforma de resolução de litígios em linha no sítio eletrónico [aqui](https://webgate.ec.europa.eu/odr/main/index.cfm?event=main.home.chooseLanguage) .

Se é um consumidor residente noutro Estado-Membro, poderá ainda efectuar a sua reclamação através do Centro Europeu do Consumidor, acedendo [aqui](http://cec.consumidor.pt/).

Para Queixas e Reclamações relativas à plataforma online poderá contactar a mesma através do Portal da Queixa do OLX ou adicionalmente através do nosso [livro de reclamações.](https://www.livroreclamacoes.pt/inicio)

  
Mais informações no site da Direção-Geral do Consumidor: [www.consumidor.pt](https://www.consumidor.gov.pt/).

16 . Mediação

1. A fim de facilitar a resolução de litígios relacionados com a prestação de Serviços do OLX, e para efeitos do Regulamento (UE) 2019/1150 do Parlamento Europeu e do Conselho de 20 de junho de 2019 (“Regulamento 2019/1150”) que visa promover a equidade e a transparência para os utilizadores profissionais de serviços de intermediação em linha, o OLX disponibiliza uma lista de mediadores com vista a oferecer ao Anunciante Profissional uma resolução rápida, justa e com resultados satisfatórios.
2. Um Anunciante Profissional tem direito a uma reclamação nos seguintes casos:

1. alegada falha do OLX no cumprimento das suas obrigações nos termos dos presentes Termos e Condições e Regulamento 2019/1150;
2. dificuldades do ponto de vista tecnológico relativamente ao funcionamento do Website que possam afetar a possibilidade de utilização dos Serviços oferecidos pelo utilizador;
3. bloqueio do conteúdo selecionado publicado ou enviado pelo utilizador ou bloqueio da conta do utilizador de acordo com a Cláusula 10 destes Termos e Condições.

3. A reclamação pode ser enviada através da funcionalidade "Submeter pedido", disponível no Website, através do nosso [formulário de contacto](https://help.olx.pt/hc/pt/requests/new?ticket_form_id=29562#top) ou por escrito para o endereço do OLX. Para submeter uma reclamação, o Utilizador deve, se possível, fornecer dados ou informações que permitam o OLX verificar a reclamação, incluindo, em particular, o ID do Anúncio, exposição de motivos pelos quais não considera o conteúdo ilegal ou contrário aos presentes Termos e Condições e, a seu critério, detalhes de contato.
4. O Anunciante profissional pode recorrer ao procedimento de mediação para resolver as reclamações que surjam entre o OLX e um determinado Anunciante Profissional em relação aos Serviços prestados pelo OLX, incluindo sobre assuntos que possam ser objeto de uma reclamação a que se refere o n.º 1 da presente Cláusula. O OLX está disponível para chegar a um acordo sobre tais reclamações por meio de mediadores que colaboram com o CEDR - Centro para a Resolução Eficaz de Disputas). A lista de mediadores e regulamentos de mediação está disponível em: [https://www.cedr.com/p2bmediation](https://www.cedr.com/p2bmediation). A mediação tem custos associados, sendo o OLX responsável por uma parte razoável dos custos totais da mediação, de acordo com as disposições previstas na legislação aplicável e na presente Cláusula.
5. O Anunciante profissional é livre de escolher outro mediador sempre e quando tenha as competências necessárias para exercer o procedimento de mediação.
6. A opção de mediação é voluntária, e o OLX reserva-se o direito de recusar participar na mediação.
7. Qualquer tentativa de alcançar um acordo através de mediação para fins de resolução de litígios, nos termos da presente Cláusula, não afeta os direitos do OLX e dos Anunciantes profissionais em causa de intentarem uma ação judicial em qualquer momento, antes, durante ou após o procedimento de mediação.
8. A pedido de um Anunciante profissional, antes de iniciar (ou durante) a mediação, o OLX deve disponibilizar ao Anunciante Profissional informações sobre o funcionamento e a eficácia da mediação relacionada com as suas atividades.

17\. Disposições Finais

1. O OLX reserva-se o direito de alterar os Termos e Condições, quando se verifique uma das seguintes situações:
    1. a necessidade de melhorar a segurança do Utilizador;
    2. a necessidade de melhorar o funcionamento do Website, sendo que alterações efetuadas nesta base não resultarão num aumento ou introdução de taxas adicionais em relação às taxas já cobradas (pagas) e permitirão uma maior utilização do Website;
    3. a necessidade de combater abusos relacionados com a utilização do Website;
    4. alteração das condições de prestação de Serviços Pagos ou retirada de um Serviço Pago específico, sendo que as alterações efetuadas nesta base não afetarão os Serviços Pagos adquiridos em momento anterior à entrada em vigor das alterações;
    5. a introdução de um novo Serviço Pago, bem como a introdução de funcionalidades adicionais do Website, ambos de utilização voluntária;
    6. alteração na legislação aplicável que tenha um impacto direto no conteúdo destes Termos e Condições.
2. O Utilizador será informado de cada alteração através da publicação de informação no Website e por via eletrónica.
3. As alterações entram em vigor na data indicada pelo OLX, num prazo não inferior a 15 Dias a contar da data de notificação da alteração aos Termos e Condições, salvo disposição em contrário prevista na legislação aplicável.
4. O OLX pode alterar os Termos e Condições sem observar o período de notificação de 15 (quinze) Dias referido acima, incluindo com efeito imediato no caso de:
    1. estar sujeito a uma obrigação legal ou regulamentar segundo a qual é obrigado a alterar os Termos e Condições de forma que a impeça de cumprir o referido prazo de 15 Dias,
    2. necessitar, a título excecional, alterar os Termos e Condições de modo a fazer face a uma ameaça imprevista e imediata relacionada com a proteção da intermediação de serviços online ou proteção de Usuários contra fraude, malware, spam, violação de dados ou outras ameaças à segurança cibernética.
5. Aplicar-se-ão as provisões atuais destes Termos e Condições aos Serviços Pagos que se encontrem ativos antes da data de entrada em vigor dos novos Termos e Condições.
6. Um Utilizador que não aceite o conteúdo das alterações aos Termos e Condições tem o direito de rescindir, a qualquer momento, o Contrato de Serviço da Conta.
7. Para efeitos de execução do Serviço, o OLX reserva-se o direito de introduzir novos serviços e funcionalidades, que podem ser precedidos de testes de produtos, sem prejuízo dos direitos adquiridos dos Utilizadores.
8. Salvo estipulação diferente pela lei, a lei aplicável aos contratos entre o Utilizador e o OLX será a lei portuguesa. A escolha da lei portuguesa não priva o Consumidor da proteção que lhe é proporcionada ao abrigo de provisões que não possam ser derrogadas contratualmente em virtude da lei aplicável em caso de ausência de escolha.
9. Os Anexos aos presentes Termos e Condições são parte integrante do mesmo.
10. O não exercício, ou o exercício tardio ou parcial, de qualquer direito que nos assista ao abrigo destes Termos e Condições, em nenhum caso poderá significar a renúncia a esse direito, ou acarretar a sua caducidade, pelo que o mesmo se manterá válido e eficaz, não obstante o seu não exercício.
11. Caso alguma das cláusulas destes Termos e Condições de Utilização venha a ser julgada nula ou vier a ser anulada, tal não afetará a validade das restantes cláusulas nem a validade dos restantes Termos e Condições de Utilização, que se considerarão automaticamente reduzidos nos termos do artigo 292.º do Código Civil.
12. A marca “OLX” encontra-se registada sob os números 499415, 525072, 525074, 525076, 10881456, 11326287, 13135512, 13135629, 13135744, 18035888, 4883741, 569183, 502600, 499721 e 499720, pela OLX B.V. e pela OLX Portugal, S.A.
13. É completamente proibida a utilização da nossa marca sem a nossa aprovação.

![olx logo.png](/olxpthelp/servlet/rtaImage?eid=ka0J8000000YOWU&feoid=00N0900000KQbfz&refid=0EM090000044ISn)      _Válido a partir de 08 de Março de 2024_