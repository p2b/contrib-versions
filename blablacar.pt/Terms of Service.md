**Condições gerais de utilização em vigor a partir de 20 de agosto de 2024.**

**1\. Assunto**
---------------

A Comuto SA (doravante designada “**BlaBlaCar**”) desenvolveu uma plataforma de partilha de veículo acessível num sítio web no endereço [www.blablacar.pt](http://www.blablacar.pt/) ou na forma de uma aplicação móvel, e concebida (i) para pôr os condutores que viajam para um determinado destino em contacto com passageiros que vão na mesma direção, de forma a poderem partilhar a Viagem e, portanto, os custos associados e (ii) para reservar bilhetes para viagens de autocarro operadas por Operadores de Autocarros (doravante designada a “**Plataforma**”).

Os presentes termos e condições têm a finalidade de reger o acesso e os termos de utilização da Plataforma. Agradecemos que os leia atentamente. Entende e reconhece que a BlaBlaCar não é uma parte em qualquer acordo, contrato ou relação contratual, seja de que natureza for, celebrados entre os Membros da Plataforma ou entre o si e o Operador de Autocarros.

Ao clicar em “Iniciar sessão com o Facebook” ou “Registar-se com um endereço de e-mail”,  reconhece ter lido e aceitado todas as presentes condições gerais de utilização.

Se utilizar a sua Conta para iniciar sessão na plataforma BlaBlaCar de outro país (por exemplo, [www.blablacar.com.br)](http://www.blablacar.ru/), lembramos que serão aplicados (i) os termos e condições dessa plataforma, (ii) a política de privacidade dessa plataforma e (iii) a legislação e regulamentos desse país. Isto também significa que as informações da sua Conta, incluindo os dados pessoais poderão ter de ser transferidas para a entidade legal que opera a outra plataforma. Tenha em atenção que a BlaBlaCar reserva-se o direito de restringir o acesso à Plataforma para utilizadores razoavelmente identificados como estando localizados fora do território da União Europeia.

**2\. Definições**
------------------

Neste documento,

“**Anúncio**” refere-se ao anúncio de uma Viagem publicado na Plataforma por um Condutor;

“**Anúncio de Carpooling**” significa um anúncio referente a uma Viagem publicado na Plataforma por um Condutor;

“**Anúncio de Autocarro**” refere-se a um anúncio relativo a uma Viagem de autocarro de um Operador de Autocarro publicado na Plataforma;

“**Bilhete**” significa o título de transporte válido nominativo entregue ao Cliente, após a Reserva de uma Viagem de Autocarro, como prova do contrato de transporte existente entre os Passageiros e o Operador de Autocarro regido pelos T&CG de venda, sem prejudicar qualquer condição particular estipulada adicionalmente entre o Passageiro e o Operador de Autocarro e referido no Bilhete;

“**BlaBlaCar**” tem o significado que lhe é dado no Artigo 1.º, supra;

“**Cliente**” significa qualquer pessoa física (Membro ou não) que compre, para si mesmo, ou para outra pessoa que será o Passageiro, um Bilhete através da Plataforma para realizar uma Viagem organizada pelo Operador de Autocarros;

“**Conta**” refere-se à conta que deve ser criada para se tornar Membro e aceder a determinados serviços oferecidos pela Plataforma;

“**Conta do Facebook**” tem o significado que lhe é dado no Artigo 3.2, infra;

“**Condutor**” refere-se ao Membro que utiliza a Plataforma para se oferecer para transportar outro indivíduo em troca da Contribuição para Custos, para uma Viagem e a uma hora definidas apenas pelo Condutor;

“**Confirmação de Reserva**” tem o significado que lhe é dado no Artigo 4.2.1, infra;

“**Conteúdo de Membro**” tem o significado que lhe é dado no Artigo 11.2, infra;

“**Contribuição para Custos**” refere-se, para uma determinada Viagem, à soma de dinheiro solicitada pelo Condutor e aceite pelo Passageiro como contribuição deste para os custos da viagem;

“**Encomenda**” significa a operação pela qual o Cliente reserva os seus serviços com o Autocarro da BlaBlaCar, independentemente do meio utilizado, com a única exceção da compra dos Bilhetes realizada diretamente num Ponto de Venda, e que engloba a obrigação de o Passageiro ou possivelmente um Agente de pagar o preço relativo aos Serviços relevantes;

“**Fornecedor da** **Solução de Pagamento Hyperwallet**” significa a PayPal (Europe) S.à r.l. et Cie, S.C.A.

“**Lugar**” refere-se ao lugar reservado por um Passageiro no veículo do Condutor ou no veículo do Operador do Autocarro;

“**Membro**” refere-se a qualquer indivíduo que tenha criado uma Conta na Plataforma;

“**Operador de Autocarros**” refere-se a uma empresa profissional de transporte de passageiros cujos bilhetes de viagem são distribuídos na Plataforma pela BlaBlaCar;

“**Passageiro**” refere-se ao Membro que aceitou a oferta de ser transportado pelo Condutor ou, se for o caso, a pessoa em cujo nome o Membro reservou um Lugar;

“**Percurso**” tem o significado que lhe é dado no Artigo 4.1, infra;

“**Plataforma**” tem o significado que lhe é dado no Artigo 1.º, supra;

“**Ponto de Venda**” refere-se aos contadores físicos e terminais listados no sítio Web e nos quais os Bilhetes são oferecidos para venda;

“**Preço**” refere-se, para uma determinada viagem de Autocarro, ao preço, incluindo todos os impostos, taxas e custos de reserva relevantes incluídos, pagos pelo Cliente na Plataforma, na altura da validação da Encomenda, por um Lugar numa determinada viagem de Autocarro;

“**Reserva**” tem o significado que lhe é dado no Artigo 4.2.1., infra;

“**Serviços de Transporte**” refere-se aos serviços de transporte subscritos por um Passageiro de uma viagem de Autocarro e fornecidos pelo Operador do Autocarro;

“**Serviços**” refere-se a todos os serviços fornecidos pela BlaBlaCar através da Plataforma;

“**Sítio Web**” refere-se ao sítio web acessível no endereço [www.blablacar.pt](http://www.blablacar.pt/);

“**Solução de Pagamento Hyperwallet**” tem o significado que lhe é dado no Artigo 5.4.2., (a), infra;

“**Taxa de Reserva**” tem o significado que lhe é dado no Artigo 5.2, infra;

“**T&C**” refere-se a estes Termos e Condições;

“**T&CG de Venda**” significa os [Termos e Condições Gerais de Venda do Operador de Autocarros](https://blog.blablacar.pt/blablalife/comunidade/eventos/condicoes-gerais-de-venda-de-transporte) envolvido dependendo da Viagem de Autocarro selecionada pelo Cliente, e os termos e condições especiais, disponíveis no Website, e que foram reconhecidos como lidos pelo Cliente antes da encomenda;

“**Viagem**” indistintamente, refere-se a uma Viagem de Autocarro ou Viagem de Carpooling

“**Viagem de Carpooling**” refere-se à viagem objeto de um Anúncio de Carpooling publicado por um Condutor na Plataforma, e pela qual o mesmo aceita transportar Passageiros em troca da Contribuição para Custos;

“**Viagem de Autocarro**” significa a viagem objeto de um anúncio de autocarro na Plataforma e para a qual o Operador de Autocarros propõe lugares nos autocarros em troca do Preço.

**3\. Registo na Plataforma e criação de uma Conta**
----------------------------------------------------

### **3.1. Condições de registo na Plataforma**

A Plataforma pode ser utilizada por indivíduos com idade igual ou superior a 18 anos. O registo na plataforma por um menor é estritamente proibido. Ao aceder, utilizar ou registar-se na Plataforma, declara e garante que tem idade igual ou superior a 18 anos.

### **3.2. Criação de uma Conta**

A Plataforma permite que os Membros publiquem e visualizem Anúncios de Carpooling, e que interajam entre si para reservar um Lugar. Pode visualizar os Anúncios se não estiver registado na Plataforma. No entanto, não pode publicar um Anúncio de Carpooling nem reservar um Lugar sem ter primeiro criado uma Conta e se ter tornado Membro.

Para criar a sua Conta, pode:

(i) preencher todos os campos obrigatórios no formulário de registo;

(ii) ou iniciar sessão na sua conta do Facebook através da nossa Plataforma (doravante designada como a sua “Conta do Facebook”). Ao utilizar tal funcionalidade, entende que a BlaBlaCar terá acesso, publicará na Plataforma e guardará determinadas informações da sua Conta do Facebook. Poderá eliminar a hiperligação entre a sua Conta e a sua Conta do Facebook a qualquer momento através da secção “Certificações” do seu perfil. Se pretender saber mais acerca da utilização dos seus dados da Conta do Facebook, leia a nossa [Política de Privacidade](https://www.blablacar.pt/about-us/privacy-policy) e a do Facebook.

Para se registar na Plataforma, deverá ter lido e aceitado os presentes T&C.

Ao criar a sua Conta, independentemente do método escolhido, aceita fornecer informações exatas e verdadeiras, e atualizá-las através do seu perfil ou notificando a BlaBlaCar, de forma a garantir a relevância e exatidão das mesmas durante as suas relações contratuais com a BlaBlaCar.

No caso do registo por e-mail,  aceita manter a palavra-passe escolhida após a criação da sua Conta confidencial e a não a comunicar a ninguém. Se perder ou divulgar a sua palavra-passe, compromete-se a informar de imediato a BlaBlaCar. É o único responsável pela utilização da sua Conta por terceiros, salvo se tiver expressamente notificado a BlaBlaCar da perda, do uso fraudulento por um terceiro ou da divulgação da sua palavra-passe a terceiros.

A aceita não criar ou utilizar, sob a sua própria identidade ou a de outra pessoa, outras Contas que não a inicialmente criada.

### **3.3. Verificação**

A BlaBlaCar pode, por questões de transparência, melhoria da confiança ou prevenção ou deteção de fraude, criar um sistema para verificação de algumas das informações que fornece no seu perfil. Tal é, em particular, o que se passa quando introduz o seu número de telefone ou nos fornece um documento de Identidade.

Reconhece e aceita que qualquer referência na Plataforma ou nos Serviços a informação “certificada”, ou qualquer termo similar, significa apenas que um Membro passou com êxito o procedimento de verificação existente na Plataforma ou nos Serviços de forma a fornecer-lhe mais informações acerca do Membro com quem está a pensar viajar. A BlaBlaCar não pode garantir a veracidade, fiabilidade ou validade da informação objeto do procedimento de verificação.

**4\. Utilização dos Serviços**
-------------------------------

### **4.1. Publicação de Anúncios**

Como Membro, e desde que cumpra as condições infra, pode criar e publicar Anúncios de Carpooling na Plataforma introduzindo informações acerca da Viagem que pretende fazer (datas/horas e pontos de recolha e chegada, número de lugares oferecidos, opções disponíveis, montante da Contribuição para Custos, etc.).

Ao publicar o seu Anúncio de Carpooling, pode indicar as cidades nas quais aceita parar e recolher ou deixar Passageiros. As secções da Viagem de Carpooling entre essas cidades ou entre uma delas e o ponto de recolha ou destino da Viagem de Carpooling constituem os “**Percursos**”.

Apenas está autorizado a publicar um Anúncio se cumprir todas as condições seguintes:

(i) tem uma carta de condução válida;

(ii) só oferece Anúncios de Carpooling para veículos seus ou que utiliza com permissão expressa do proprietário e que, de qualquer forma, está autorizado a utilizar para fins de partilha de transporte;

(iii) é e continua a ser o principal condutor do veículo objeto do Anúncio de Carpooling;

(iv) o veículo tem um seguro contra terceiros válido;

(v) não tem contraindicações ou incapacidade médica para a condução;

(vi) o veículo que pretende utilizar para a Viagem é um veículo ligeiro de passageiros com 4 rodas e o máximo de 7 lugares, excluindo os veículos “sem carta” – automóveis que não necessitam de licença de condução.

(vii) não tenciona publicar outro anúncio para a mesma Viagem de Carpooling na Plataforma;

(viii) não oferece mais Lugares do que o número disponível no seu veículo;

(ix) todos os Lugares oferecidos têm cinto de segurança, ainda que o veículo esteja aprovado com lugares sem cinto de segurança;

(x) a utilizar um veículo em boas condições de funcionamento e que cumpra as disposições legais aplicáveis, nomeadamente com um certificado de IPO atualizado;

(xi) é um consumidor e não atua como profissional.

Reconhece que é o único responsável pelo conteúdo do Anúncio de Carpooling que publica na Plataforma. Por conseguinte, declara e garante a exatidão e veracidade de todas as informações contidas no Anúncio de Carpooling e compromete-se a realizar a Viagem nas condições descritas no seu Anúncio de Carpooling.

O seu Anúncio de Carpooling cumpirá os T&C, o mesmo será publicado na Plataforma ficando, assim, visível para os Membros e todos os visitantes, ainda que não sejam membros, que efetuem uma pesquisa na Plataforma ou no sítio web de parceiros da BlaBlaCar. A BlaBlaCar reserva-se o direito de não publicar ou de remover, a qualquer momento e por sua única iniciativa, qualquer Anúncio de Carpooling que não cumpra os T&C ou que a mesma considere que prejudica a sua imagem, a imagem da Plataforma ou dos Serviços e/ou suspender a Conta do Membro que publique tais Anúncios de Carpooling de acordo com a Secção 9 dos presentes T&C.

Reconhece e aceita que os critérios tidos em conta na classificação e na ordem de apresentação do seu Anúncio entre os outros Anúncios ficam exclusivamente ao critério da BlaBlaCar.

Informamos que caso se apresente como um consumidor ao utilizar a Plataforma quando na realidade atua como profissional, fica exposto a sanções contempladas pela lei aplicável.

### 4.2. Reservar um Lugar em carpooling

A BlaBlaCar estabeleceu um sistema para reservar Lugares online (“Reserva”) para as Viagens oferecidas na Plataforma.

Os métodos de reserva de um Lugar dependem da natureza da Viagem em questão.

A BlaBlaCar fornece-lhe na Plataforma um motor de pesquisa com base em diferentes critérios de pesquisa (Local de origem, destino, datas, número de passageiros, etc.). Certas funcionalidades adicionais são fornecidas no motor de pesquisa quando está ligado à sua Conta. A BlaBlaCar convida-o(a), independentemente do processo de reserva utilizado, a consultar atentamente e utilizar o motor de pesquisa para determinar a oferta mais adaptada às suas necessidades. Poderá encontrar mais informações [aqui](https://blog.blablacar.pt/about-us/transparencia-das-plataformas). O Cliente, ao reservar uma Viagem de Autocarro num Ponto de Venda, também pode solicitar ao Operador de Autocarros ou agente de balcão para realizar a pesquisa.

**4.2.1** **Viagem de Carpooling**

Quando um Passageiro está interessado num Anúncio de Carpooling que beneficie desta Reserva, pode efetuar um pedido de reserva online. Este pedido de Reserva é (i) aceite automaticamente (se o Condutor escolher esta opção ao publicar o seu Anúncio), ou (ii) aceite manualmente pelo Condutor. No momento da Reserva, o Passageiro efetua o pagamento online da Contribuição para Custos e da Taxa de Reserva relacionadas, se for o caso. Após receber o pagamento pela BlaBlaCar e a validação do pedido de Reserva pelo Condutor, se for o caso, o Passageiro recebe uma confirmação de Reserva (a “**Confirmação de Reserva**”).

Se for um Condutor e tiver escolhido tratar manualmente os pedidos de reserva ao publicar o seu Anúncio de Carpooling, terá de responder a qualquer pedido de Reserva especificado pelo Passageiro durante o pedido de Reserva. Caso contrário, o pedido de reserva expirará automaticamente e o Passageiro será reembolsado por todas as somas pagas no momento do pedido de Reserva, se as houver.

No momento da Confirmação de Reserva, a BlaBlaCar envia-lhe o número de telefone do Condutor (se for o Passageiro) ou do Passageiro (se for o Condutor), se o Membro tiver aceitado apresentar o seu número de telefone. Será, então, o único responsável pela execução do contrato que o vincula ao outro Membro.

**4.2.2** **Viagem de Autocarro**

Para Viagens de Autocarro, a BlaBlaCar permite a reserva dos Bilhetes de Autocarro para uma determinada Viagem de Autocarro através da Plataforma.

Os Serviços de Transporte são regidos pelos T&CG de Venda do Operador de Autocarros relevante, dependendo da Viagem selecionada pelo Cliente, que tem de ser aceite pelo Cliente antes de realizar a encomenda. A BlaBlaCar não fornece quaisquer serviços de transporte em relação a Viagens de Autocarro. Os Operadores de Autocarro são exclusivamente parte dos T&CG de Venda. Reconhece que a reserva de Lugares para uma determinada Viagem de Autocarro está sujeita aos T&C de Venda do operador de Autocarro relevante.

A BlaBlaCar chama a sua atenção para o facto de que certos Serviços de Transporte oferecidos pelo Operador de Autocarros e mencionados no Sítio Web podem ser retirados, em particular por motivos climáticos, uso sazonal ou em caso de Força Maior.

**4.2.3** **Natureza designada da reserva do Lugar e termos de utilização dos Serviços em nome de terceiro**

Qualquer utilutilização dos Serviços, na qualidade de Passageiro ou Condutor, está relacionada com um nome específico. O Condutor e o Passageiro devem corresponder à identidade comunicada à BlaBlaCar e aos outros Membros que participam na Viagem de Carpooling ou Operador de Autocarros.

Contudo, a BlaBlaCar permite que os seus Membros reservem um ou mais Lugares em nome de um terceiro. Nesse caso, compromete-se a indicar com precisão ao Condutor, no momento da Reserva, os apelidos, idade e número de telefone da pessoa em cujo nome está a reservar um Lugar. É estritamente proibido reservar um Lugar para um menor com idade inferior a 13 anos que viaje sozinho para uma Viagem de Carpooling. Se reservar um lugar numa Viagem de Carpooling para um menor com idade superior a 13 anos que viaje sozinho, compromete-se a solicitar o consentimento prévio do Condutor e a fornecer-lhe a permissão devidamente preenchida e assinada dos representantes legais da criança.

Além disso, a Plataforma destina-se à reserva de Lugares para indivíduos. É proibido reservar um Lugar para transportar qualquer objeto, pacote, animal a viajar sozinho ou qualquer tipo de material.

É ainda proibido publicar um Anúncio para um Condutor que não seja o utilizador.

### **4.3.** **Conteúdo de Membro, moderação e s****istema de avaliação**

**4.3.1. Funcionamento do sistema de avaliação**

A BlaBlaCar incentiva-o(a) a deixar a sua avaliação acerca de um Condutor (se for um Passageiro) ou acerca de um Passageiro (se for um Condutor) com quem tenha partilhado uma Viagem ou com quem deveria ter partilhado uma Viagem (ou seja, reservado uma Viagem).. No entanto, não poderá deixar avaliações acerca de outro Passageiro, se for um Passageiro, ou acerca de um Membro com o qual não tenha viajado ou com o qual não deveria ter viajado.

A sua avaliação e a avaliação deixada por outro Membro acerca de si, se aplicável, só são visíveis e publicadas na Plataforma passado o mais curto dos seguintes períodos: (i) imediatamente após terem ambos deixado uma avaliação no prazo de 14 dias a seguir à Viagem; ou (ii) passado um período de 14 dias após a primeira avaliação.

A opção de deixar uma avaliação deixará de estar disponível 14 dias após a Viagem. No entanto, tem a opção de responder a uma avaliação que outro Membro tenha deixado no seu perfil num prazo de 14 dias a seguir à data da avaliação recebida. A avaliação e a sua resposta, se for o caso, serão publicadas no perfil deste.

**4.3.2.** **Moderação**

**a. Conteúdo de membro**

Reconhece e aceita que a BlaBlaCar pode moderar, antes da publicação, utilizando ferramentas automatizadas ou de forma manual, o Conteúdo de Membro, tal como definido na secção 11.2. Se a BlaBlaCar considerar que tal Conteúdo de Membro viola as leis aplicáveis ou os presentes T&C, ela reserva-se o direito de:

● Impedir a publicação ou excluir tal Conteúdo de Membro

● Enviar um aviso ao Membro, lembrando-o da obrigação de cumprimento das leis aplicáveis ou dos presentes Termos e Condições, e/ou

● Aplicar as medidas restritivas de acordo com a Secção 9 dos presentes T&C.

O uso pela BlaBlaCar de tais ferramentas automatizadas ou de moderação manual não deve ser interpretado como um compromisso de monitorizar, ou uma obrigação de procurar ativamente, atividades ilegais e ou Conteúdo de Membro publicado na Plataforma e, na medida permitida pela lei aplicável, não dá origem a qualquer responsabilidade para a BlaBlaCar.

**b. Mensagens**

A troca de mensagens entre Membros através da nossa Plataforma (“Mensagens”) tem como único objetivo a troca de informações relativas a Viagens de Carpooling.

A BlaBlaCar pode, através de software e algoritmos automatizados, detetar o conteúdo das Mensagens com a finalidade de prevenção de fraudes, melhoria do serviço, apoio ao cliente e execução dos contratos celebrados com os nossos membros (tais como estes T&C). Em caso de deteção de qualquer conteúdo numa Mensagem que mostre sinais de comportamento fraudulento ou ilegal, evasão da Plataforma ou que seja contrário aos presentes T&C:

● Esse conteúdo não pode ser publicado

● O Membro que enviou a Mensagem pode receber um aviso lembrando-o da obrigação de respeitar a legislação aplicável ou os presentes T&C, ou

● A conta do Membro poderá ser suspensa de acordo com a Secção 9 dos presentes T&C.

O uso de tal software automatizado pela BlaBlaCar não pode ser interpretado como um compromisso de monitorizar, ou uma obrigação de procurar ativamente atividades e/ou conteúdos ilícitos na Plataforma e, na medida do permitido pelas leis aplicáveis, não gera qualquer responsabilidade para a BlaBlaCar.

**4.3.3.** **Limite**

A BlaBlaCar reserva-se o direito de suspender a sua Conta, limitar o acesso deste aos Serviços ou cessar os presentes T&C caso (i)  tenha recebido pelo menos três avaliações e (ii) a média das avaliações recebidas seja igual ou inferior a 3.

**5\. Condições financeiras**
-----------------------------

O acesso à Plataforma e o registo na mesma, bem como a pesquisa, visualização e publicação de Anúncios, são gratuitos. No entanto, a Reserva é cobrada segundo as condições descritas infra.

### 5.1. Contribuição para Custos e Preço

5.1.1 Em caso de viagem de Carpooling, a Contribuição para Custos é determinada por si, na qualidade de Condutor, sob sua única responsabilidade. É estritamente proibido lucrar, seja de que forma for, da utilização da nossa Plataforma. Por conseguinte, aceita limitar a Contribuição para Custos cujo pagamento solicita aos seus Passageiros aos custos realmente incorridos por si para realizar a Viagem de Carpooling. Caso contrário, será o único a suportar os riscos da recaraterização da transação concluída através da Plataforma.

Quando publica um Anúncio de Carpooling, a BlaBlaCar sugere um montante para a Contribuição para Custos, que tem, nomeadamente, em conta a natureza da Viagem de Carpooling e a distância percorrida. Este montante é dado apenas como orientação e cabe a si aumentá-lo ou reduzi-lo para ter em conta os custos em que  incorre realmente na Viagem de Carpooling. Para evitar abusos, a BlaBlaCar limita a possibilidade de ajustamento da Contribuição para Custos.

5.1.2 Relativamente à Viagem de Autocarro, o Preço por Lugar é definido ao próprio critério do Operador do Autocarro. O Cliente é convidado a referir-se aos T&CG de Venda para compreender as modalidades aplicáveis em relação à realização de encomenda de Bilhetes e modalidades de pagamento.

### **5.2. Taxa de Reserva**

A BlaBlaCar, em troca da utilização da Plataforma, no momento da Reserva irá cobrar taxa de reserva (doravante designadas como as “**Taxa de Reserva**”). Será informado antes de qualquer aplicação das Taxa de reserva, quando necessárias. Os métodos de cálculo das Taxa de Reserva em vigor que se encontram-se [aqui](https://blog.blablacar.pt/blablalife/lp/gastos-de-gestao) e são apenas para fins informativos  e não têm valor contratual. A BlaBlaCar reserva-se o direito de alterar em qualquer altura os métodos de cálculo das Taxa de Reserva. Estas modificações  não afetarão as Taxa de Reserva aceites pelo si antes da data de entrada em vigor destas modificações.

No caso das viagens transfronteiras, lembramos que os métodos de cálculo do montante das Taxa de Reserva e o IVA aplicável variam de acordo com  o ponto de recolha e/ou destino da Viagem.

Ao utilizar a Plataforma para viagens transfronteiras ou fora de Portugal, as Taxa de Reserva poderão ser cobradas por uma empresa afiliada da BlaBlaCar a operar a plataforma local.

### **5.3. Arredondamentos**

Reconhece e aceita que a BlaBlaCar pode, por sua única iniciativa, arredondar para números inteiros ou decimais as Taxa de Reserva e a Contribuição para Custos por excesso ou por defeito.

### **5.4. Métodos de pagamento e reembolso da Contribuição para Custos ao Condutor ou Preço para Operadores de Autocarros**

**5.4.1.** **Instruções do Condutor**

Ao utilizar a Plataforma como Condutor para Viagens, confirma que leu e aceitou os [termos e condições](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) do Fornecedor de Soluções de Pagamento Hyperwallet – o nosso fornecedor de pagamentos que processa as transferências das Contribuições para Custos dos Condutores através da Solução de Pagamento Hyperwallet definida no artigo 5.4.2., (a). Estes termos e condições são ainda referidos como “Termos e Condições da Hyperwallet”.

Deve ter em atenção que as exclusões à Diretiva (UE) 2015/2366 do Parlamento Europeu e do Conselho, de 25 de novembro de 2015, relativa aos serviços de pagamento no mercado interno, enumeradas no artigo 11.º, n.º 4, alíneas i) e ii), dos Termos e Condições da Hyperwallet não se aplicam aos Membros da Plataforma enquanto não profissionais.

Na qualidade de Condutor, dá instruções:

* à BlaBlaCar para pedir ao Fornecedor de Soluções de Pagamento Hyperwallet para efetuar a transferência da Contribuição para Custos, e
* ao Fornecedor de Soluções de Pagamento Hyperwallet para fazer a transferência da Contribuição para Custos para a sua conta bancária ou conta do PayPal em seu nome e por sua conta, de acordo com os Termos e Condições da Hyperwallet.

No âmbito de uma Viagem de Carpooling, e após aceitação manual ou automática da Reserva, a totalidade da soma paga pelo Passageiro (Taxa de Reserva e Contribuição para Custos) é mantida numa conta de garantia gerida pelo Fornecedor de Soluções de Pagamento Hyperwallet.

**5.4.2.** **Pagamento da Contribuição para Custos ao Condutor**

Após a Viagem de Carpooling, os Passageiros terão um período de 24 horas após o final da Viagem para apresentar uma contestação à BlaBlaCar. Na ausência de contestação da parte dos Passageiros dentro deste período, a BlaBlaCar considera a viagem confirmada.

A partir do momento da confirmação expressa ou tácita, terá, na qualidade de Condutor, um crédito na sua Conta. Esse crédito corresponde ao montante total pago pelo Passageiro no momento da confirmação da Reserva, depois de deduzidas as Taxa de Reserva, ou seja, a Contribuição para Custos paga pelo Passageiro.

**a. Solução de Pagamento Hyperwallet**

A Solução de Pagamento Hyperwallet é um serviço de pagamento fornecido pelo Fornecedor de Soluções de Pagamento Hyperwallet. Para que não subsistam dúvidas, a BlaBlaCar não fornece nenhum serviço de processamento de pagamentos para Membros e não entra na posse de fundos dos Membros.

Quando a Viagem é confirmada pelo Passageiro, tem a opção, na qualidade de Condutor, de receber a sua Contribuição para Custos na sua conta bancária ou conta do PayPal.

Para este efeito, o Condutor pode utilizar a Solução de Pagamento Hyperwallet celebrando um contrato diretamente com o Fornecedor de Soluções de Pagamento Hyperwallet e aceitando os Termos e Condições da Hyperwallet.

Para receber a primeira transferência da Contribuição para Custos, será solicitado ao Condutor que selecione um método de pagamento, o seja, que forneça os dados da sua conta bancária ou PayPal, bem como endereço postal e data de nascimento. Caso não o faça, ou caso as informações que forneça estejam incorretas, inexatas ou desatualizadas, a Contribuição para Custos não será transferida.

A transferência será realizada pelo Fornecedor de Soluções de Pagamento Hyperwallet.

No final do período de 20 anos, qualquer soma relativa à Contribuição para Custos não reclamada por si a partir da data em que apareceu na sua Conta na qualidade de Condutor através da Solução de Pagamento Hyperwallet será considerada como pertencente à BlaBlaCar.

**b. Procedimento de verificação KYC (“Conheça o seu Cliente”)**

Na utilização da Solução de Pagamento Hyperwallet, aceita que pode estar sujeito aos procedimentos regulamentares aplicados pelo Fornecedor de Soluções de Pagamento, sujeito aos seus [termos e condições](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml).

Esses procedimentos podem incluir verificações de identidade e outros requisitos do procedimento “Conheça o seu cliente” (KYC) com base nos critérios estabelecidos pelo Fornecedor de Soluções de Pagamento Hyperwallet. Esses critérios podem incluir limites financeiros, dependendo do valor total dos pagamentos da Contribuição para Custos efetuados por si.

Para efeitos da verificação KYC, pode-lhe ser pedido que forneça informações adicionais, conforme solicitado pelo Fornecedor de Soluções de Pagamento Hyperwallet. Caso não apresente os documentos exigidos, não poderá receber a Contribuição para Custos até que a sua identidade seja confirmada.

Podem ocorrer situações em que o Fornecedor de Soluções de Pagamento Hyperwallet deva, de acordo com a legislação aplicável, suspender a transferência da Contribuição para Custos ou o acesso à Solução de Pagamento. A BlaBlaCar não tem qualquer influência relativamente a essas ações por parte do Fornecedor de Soluções de Pagamento Hyperwallet.

**5.4.3** **Paragamento do Preço**

O pagamento por qualquer Encomenda realizada através da Plataforma é realizado através de um dos meios autorizados abaixo. O Cliente tem a opção de guardar as informações relativas a um ou mais cartões de crédito na sua Conta de Cliente para que não tenha de introduzir sistematicamente estas informações quando realizar pagamentos subsequentes.

Os meios de pagamento podem variar dependendo da plataforma BlaBlaCar que utiliza. Os meios autorizados de pagamento são os seguintes:

* Cartão bancário (as marcas aceites são apresentadas na plataforma. No caso de ter um cartão multimarca, pode selecionar uma marca específica na fase de pagamento
* Paypal
* Vales
* Apple Pay, Google Pay.

Nenhuma confirmação de Encomenda será emitida até ao pagamento completo e efetivo do preço dos Serviços de Transporte selecionados pelo Cliente. Se o pagamento for irregular, incompleto ou não for realizado por algum motivo atribuível ao Cliente, a encomenda será cancelada imediatamente.

A Encomenda é confirmada ao enviar um e-mail para o Cliente, contendo os detalhes da Encomenda.

O Passageiro é convidado a verificar as definições para a caixa de entrada do seu endereço de e-mail e, em particular, garantir que o e-mail de confirmação não é enviado diretamente para o Spam.

A confirmação da encomenda é final. Consequentemente, quaisquer alterações resultarão na troca ou cancelamento ao abrigo das condições dos T&C de Veda aplicáveis. É da responsabilidade do Cliente garantir que os Serviços de Transporte são escolhidos de acordo com as necessidades e expetativas do Passageiro. A exatidão das Informações Pessoais introduzidas pelo Cliente é da responsabilidade deste último, exceto em caso de falha comprovada da BlaBlaCar em recolher, armazenar ou proteger essas Informações pessoais.

**6\. Finalidade não comercial e não negocial dos Serviços e da Plataforma**
----------------------------------------------------------------------------

Aceita utilizar os Serviços e a Plataforma apenas para ser posto em contacto, de forma não negocial e não comercial, com pessoas que pretendam partilhar uma Viagem de Carpooling consigo ou reservar um Lugar no contexto de uma viagem de Autocarro.

No contexto da Viagem de Carpooling, reconheceu que os direitos do consumidor resultantes da lei de proteção do consumidor podem não se aplicar à sua relação com outros Membros.

Como Condutor, aceita não solicitar uma Contribuição para Custos superior aos custos realmente incorridos e que possa gerar lucro, ficando especificado que, no âmbito de uma partilha de custos, terá de suportar, como Condutor, a sua própria parte dos custos decorrentes da Viagem de Carpooling. É o único responsável pelo cálculo dos custos em que incorre para a Viagem de Carpooling e pela verificação de que o montante pedido aos seus Passageiros não ultrapassa os custos em que realmente incorreu (excluindo a sua parcela dos custos), nomeadamente ao referir-se à escala de tributação aplicável.

A BlaBlaCar reserva-se o direito de suspender a sua Conta caso utilize um veículo com motorista ou outro veículo comercial ou táxi, ou um automóvel de empresa e, por esse motivo, obtenha lucro através da Plataforma. Aceita fornecer à BlaBlaCar, mediante o simples pedido da mesma, uma cópia do certificado de matrícula do seu automóvel e/ou qualquer outro documento que mostre que está autorizado a usar o veículo na Plataforma e que não aufere qualquer lucro dessa forma.

A BlaBlaCar reserva-se também o direito de suspender a sua Conta, limitar o seu acesso aos Serviços ou cessar estes T&C, caso realize alguma atividade na Plataforma que, em virtude da natureza das Viagens oferecidas, da sua frequência, do número de Passageiros transportados e da Contribuição para Custos solicitada, implique uma situação de lucro para si, ou que, de alguma forma, sugira à BlaBlaCar que está a tirar lucro da Plataforma.

**7\. Política de cancelamento**
--------------------------------

### **7.1.** Termos de reembolso em caso de cancelamento  

Apenas as Viagens de Carpooling estão sujeitas a esta política de Cancelamento; a BlaBlaCar não oferece qualquer garantia, seja de que natureza for, no caso de cancelamento por qualquer motivo, por parte de um Condutor ou de um Passageiro. No contexto de uma Viagem de Autocarro, a política de cancelamento está sujeita aos T&CG de Venda dos Operadores de Autocarros. O cliente é convidado a consultar os T&CG de Venda para confirmar as condições de trocas e cancelamento da sua Viagem de Autocarro.

O cancelamento de um Lugar numa Viagem de Carpooling por parte do Condutor ou do Passageiro após a Confirmação de Reserva está sujeito às estipulações que se seguem:

No caso de cancelamento da parte do Condutor, o Passageiro é reembolsado pelo total da soma paga (ou seja, a Contribuição para Custos e as Taxa de Reserva relacionadas). Tal é, nomeadamente, o que acontece quando o Condutor cancela uma Viagem de Carpooling ou não chega ao ponto de encontro 15 minutos após a hora acordada;

No caso de cancelamento por parte do Passageiro:

* Se o Passageiro efetuar o cancelamento mais de 24 horas antes da hora de partida planeada, conforme mencionado no Anúncio de Carpooling, o Passageiro só será reembolsado pela Contribuição para Custos. As Taxa de Reserva são retidas pela BlaBlaCar e o Condutor não recebe qualquer soma, seja de que natureza for;
* Se o Passageiro efetuar o cancelamento 24 horas ou menos antes da hora de partida planeada, conforme mencionado no Anúncio de Carpooling, e mais de trinta minutos após a Confirmação de Reserva, ser-lhe-á reembolsada metade da Contribuição para Custos paga no momento da Reserva, sendo as Taxa de Reserva retidas pela BlaBlaCar, e o Condutor receberá 50% da Contribuição para Custos, através da Solução de Pagamento Hyperwallet;
* Se o Passageiro efetuar o cancelamento 24 horas ou menos antes da hora de partida planeada, conforme mencionado no Anúncio de Carpooling, e trinta minutos ou menos após a Confirmação de Reserva, ser-lhe-á reembolsada a totalidade da Contribuição para Custos, através da Solução de Pagamento Hyperwallet. As Taxa de Reserva são retidas pela BlaBlaCar e o Condutor não recebe qualquer soma, seja de que natureza for;
* Se o Passageiro efetuar o cancelamento após a hora de partida planeada, conforme mencionado no Anúncio de Carpooling, ou se não tiver chegado ao local de encontro 15 minutos após a hora acordada, não será efetuado qualquer reembolso. O Condutor será compensado pela totalidade da Contribuição para Custos, através da Solução de Pagamento Hyperwallet e as Taxa de Reserva ficarão para a BlaBlaCar.

Quando o cancelamento ocorrer antes da partida e por parte do Passageiro, o(s) Lugar(es) cancelado(s) pelo Passageiro será(ão) automaticamente disponibilizado(s) a outros Passageiros que os poderão reservar online e que ficam, em conformidade, sujeitos às condições destes T&C.

A BlaBlaCar aprecia, por sua única iniciativa e com base na informação disponível, a legitimidade dos pedidos de reembolso.

### **7.2. Direito de retratação**

Não tem direito de rescisão a partir do momento da confirmação da reserva, desde que o contrato entre si e a BlaBlaCar, que consiste em colocá-lo em contacto com outro Membro, tenha sido totalmente executado.

**8\. Comportamento dos utilizadores da Plataforma e dos Membros**
------------------------------------------------------------------

### **8.1. Compromisso de todos os utilizadores da Plataforma**

O utilizador reconhece ser o único responsável por respeitar todas as leis, regulamentos e obrigações aplicáveis à sua utilização da Plataforma.

Além disso, ao utilizar a Plataforma e durante as Viagens, o utilizador compromete-se:

* (i) a não utilizar a Plataforma para fins profissionais, comerciais ou lucrativos;
* (ii) a não enviar à BlaBlaCar (nomeadamente, após a criação ou atualização da sua Conta) ou aos outros Membros qualquer informação falsa, enganadora, maliciosa ou fraudulenta;
* (iii) a não falar ou comportar-se de qualquer forma, ou publicar qualquer conteúdo (incluindo Mensagens) na Plataforma, de natureza difamatória, injuriosa, obscena, pornográfica, grosseira, ofensiva, agressiva, não solicitada, violenta, ameaçadora, assediadora, racista ou xenofóbica, ou com conotações sexuais, de incitação à violência, discriminação ou ódio, encorajando atividades ou a utilização de substâncias ilegais ou, mais em geral ilegais, contrárias às leis aplicáveis, a estes T&C e ao propósito da Plataforma, que possa infringir os direitos da BlaBlaCar ou de terceiros, ou vá contra a boa moral;
* (iv) a não infringir os direitos e a imagem da BlaBlaCar, nomeadamente, os seus direitos de propriedade intelectual;
* (v) a não abrir mais de uma Conta na Plataforma e a não abrir uma Conta em nome de terceiros;
* (vi) a não tentar contornar o sistema de reserva online da Plataforma, nomeadamente tentando enviar a outro Membro os seus dados de contacto para efetuar a reserva fora da Plataforma e evitar pagar as Taxa de Reserva;
* (vii) a não contactar outro Membro, nomeadamente através da Plataforma, para fins que não os definidos nos termos da partilha de transporte;
* (viii) a não aceitar ou efetuar pagamentos fora da Plataforma ou da Solução de Pagamento Hyperwallet, salvo nos casos autorizados pelos presentes T&C para as Viagens sem Reserva;
* (ix) a cumprir estes T&C.

### **8.2. Compromissos dos Condutores**

Além disso, quando utiliza a Plataforma como condutor, compromete-se:

  (i) a respeitar todas as leis, regulamentos e códigos aplicáveis à condução e ao veículo, nomeadamente a ter um seguro de responsabilidade civil válido no momento da Viagem de Carpooling e a ser portador de uma carta de condução válida;

  (ii) a confirmar se o seu seguro cobre a partilha de transporte e se os seus Passageiros são considerados como terceiros no seu veículo e estão, dessa forma, abrangidos pelo seu seguro;

  (iii) a não assumir riscos ao conduzir, a não consumir nenhum produto que possa prejudicar a sua atenção e a sua capacidade para realizar uma condução vigilante e completamente segura;

  (iv) a publicar Anúncios de Carpooling que correspondam apenas a Viagens realmente planeadas;

  (v) a realizar a Viagem de Carpooling conforme descrito no anúncio (nomeadamente no que se refere à utilização ou não da autoestrada) e a respeitar os horários e locais acordados com os outros Membros (nomeadamente no que se refere ao local de encontro e ao ponto de largada);

  (vi) a não transportar mais Passageiros do que o número de Lugares indicado no Anúncio de Carpooling;

  (vii) a utilizar um veículo em boas condições de funcionamento e que cumpra as disposições legais aplicáveis, nomeadamente com um certificado de IPO atualizado;

  (viii) a apresentar à BlaBlaCar, ou a qualquer Passageiro que o solicite, a sua carta de condução, o seu certificado de matrícula, a apólice de seguro, o certificado de IPO e qualquer documento que comprove a sua capacidade para utilizar o veículo como Condutor na Plataforma;

  (ix) caso fique retido ou ocorra uma alteração à hora ou Viagem de Carpooling, a informar os seus Passageiros sem demora;

  (x) no caso de uma Viagem de Carpooling transfronteira, a ser portador e disponibilizar ao Passageiro e a qualquer autoridade que o solicite qualquer documento que comprove a sua identidade e o seu direito de atravessar a fronteira;

  (xi) a aguardar pelos Passageiros no local de encontro acordado durante, pelo menos, 15 minutos após a hora combinada;

  (xii) a não publicar um Anúncio de Carpooling relativo a um veículo que não lhe pertence ou que não está autorizado a utilizar para a partilha de transporte;

  (xiii) a certificar-se de que pode ser contactado telefonicamente pelos seus Passageiros pelo número registado no seu perfil;

  (xiv) a não gerar qualquer lucro através da Plataforma;

  (xv) a não ter qualquer contraindicação ou incapacidade médica para conduzir;

  (xvi) a comportar-se de forma apropriada e responsável durante a Viagem de Carpooling, e em conformidade com o espírito da partilha de transporte.

### **8.3. Compromissos dos Passageiros**

**8.3.1** **Para Viagem de Carpooling**

Quando utiliza a Plataforma como Passageiro de uma Viagem de Carpooling, compromete-se:

  (i) a adotar um comportamento apropriado durante a Viagem de Carpooling, de forma a não prejudicar a concentração ou a condução do Condutor ou a paz e sossego dos outros Passageiros;

  (ii) a respeitar o veículo do Condutor e a limpeza do mesmo;

  (iii) caso fique retido, a informar o Condutor sem demora;

  (iv) a aguardar pelo Condutor no local de encontro durante, pelo menos, 15 minutos para além da hora combinada;

  (v) a apresentar à BlaBlaCar, ou a qualquer Condutor que o solicite, o seu cartão de identidade ou qualquer documento que comprove a sua identidade;

  (vi) a não transportar, durante uma Viagem de Carpooling, qualquer artigo, bem, substância ou animal que possa prejudicar a condução e a concentração do Condutor, ou cuja natureza, posse ou transporte sejam contrários às disposições legais em vigor;

  (vii) no caso de uma Viagem de Carpooling transfronteira, a ser portador e disponibilizar ao Condutor e a qualquer autoridade que o solicite qualquer documento que comprove a sua identidade e o seu direito de atravessar a fronteira;

  (viii) a certificar-se de que pode ser contactado telefonicamente pelo seu Condutor pelo número registado no seu perfil, inclusive no ponto de encontro.

Caso tenha realizado uma Reserva para um ou mais Lugares em nome de terceiros, em conformidade com o estipulado no Artigo 4.2.3 supra, garante o respeito, por esses terceiros, das estipulações deste artigo e, em geral, dos T&C. A BlaBlaCar reserva-se o direito de suspender a sua Conta, limitar o seu acesso aos Serviços ou cessar estes T&C, em caso de violação pelo terceiro em cujo nome reservou um Lugar ao abrigo destes T&C, em conformidade com o ponto 9 das presentes T&C.

8.3.2 **Para Viagem de Autocarro**

No contexto de uma Viagem de Autocarro, o Passageiro compromete-se a cumprir com os T&CG de Venda do Operador de Autocarros relevante.

**8.4. Comunicação de conteúdos inadequados ou ilegais (mecanismo de notificação e ação)**

O utilizador pode denunciar um Conteúdo de Membro ou uma Mensagem suspeitos, inadequados ou ilegais, conforme descrito [aqui](https://support.blablacar.com/s/article/Denunciar-conte%C3%BAdo-ilegal-1729197120606?language=pt_PT).

A BlaBlaCar, após ter sido devidamente notificada nos termos da presente secção ou pelas autoridades competentes, removerá imediatamente qualquer Conteúdo de Membro ilegal se:

* O Conteúdo do Membro for obviamente ilegal ou contrário aos regulamentos aplicáveis; ou 
* A BlaBlaCar considerar que tal conteúdo viola estes T&Cs.

Nesses casos, a BlaBlaCar reserva-se o direito de remover o Conteúdo do Membro que foi devidamente denunciado, de forma suficientemente detalhada e clara, e/ou suspender imediatamente a Conta denunciada.

O Membro em questão pode recorrer das decisões da BlaBlaCar, conforme descrito na secção 15.1 abaixo.

**9\. Restrições relacionadas ao uso da Plataforma, suspensão de contas, limitação de acesso e cessação**
---------------------------------------------------------------------------------------------------------

Pode cessar as suas relações contratuais com a BlaBlaCar a qualquer momento, sem qualquer custo ou motivo. Para o fazer, basta entrar no separador “Encerrar a minha conta” da sua página de Perfil.

No caso de (i) violação dos presentes T&C, por sua parte, incluindo mas não limitado às suas obrigações como Membro mencionadas nos artigos 6.º e 8.º supra, (ii) que ultrapasse o limite estabelecido no Artigo 4.3.3 supra, ou (iii) se a BlaBlaCar tiver uma razão genuína para crer que tal é necessário para proteger a sua segurança e integridade e/ ou a segurança e integridade dos Membros ou de terceiros, ou por questões de prevenção de fraude ou investigações, a BlaBlaCar reserva-se o direito de:

  (i) cessar os T&C que o vinculam  à BlaBlaCar, de imediato e sem aviso; e/ou

  (ii) impedir a publicação ou remover qualquer Conteúdo de Membro por si na Plataforma; e/ou

  (iii) limitar o seu acesso à Plataforma, bem como o uso da mesma; e/ou

  (iv) suspender, temporária ou permanentemente, a sua Conta.

A suspensão da conta também pode significar, quando aplicável, que o utilizador não receberá os pagamentos pendentes.

Quando aplicável, a BlaBlaCar poderá também enviar-lhe um aviso lembrando-o da obrigação de cumprir as leis aplicáveis e/ou os presentes T&C.

Quando tal seja necessário,  será notificado do estabelecimento de tal medida de forma a ter a possibilidade de confirmar o seu compromisso de aderir a estes T&Cs e às leis aplicáveis,  de se explicar à BlaBlaCar ou contestar a sua decisão. A BlaBlaCar decidirá, levando em conta todas as circunstâncias de cada caso e a gravidade da infração, por sua própria iniciativa, se irá ou não anular as medidas implementadas.

**10\. Dados pessoais**
-----------------------

No contexto da utilização da Plataforma por parte do utilizador, a BlaBlaCar irá recolher e processar alguns dados pessoais deste tal como descrito na sua [Política de Privacidade](https://blog.blablacar.pt/about-us/privacy-policy). 

**11\. Propriedade intelectual**
--------------------------------

### **11.1. Conteúdo publicado pela BlaBlaCar**

Salvaguardando os conteúdos fornecidos pelos seus Membros, a BlaBlaCar é a única titular de todos os direitos de propriedade intelectual relacionados com o Serviço, a Plataforma, o seu conteúdo (nomeadamente textos, imagens, desenhos, logótipos, vídeos, sons, dados, gráficos) e com o software e as bases de dados que asseguram o seu funcionamento.

A BlaBlaCar concede-lhe um direito não exclusivo, pessoal e não transmissível para utilizar a Plataforma e os Serviços, para seu uso particular e pessoal, de forma não comercial e em conformidade com a finalidade da Plataforma e dos Serviços.

Está proibido de realizar qualquer outra utilização ou exploração da Plataforma e dos Serviços, e do respetivo conteúdo, sem a prévia permissão por escrito da BlaBlaCar. Nomeadamente, está proibido de:

  (i) reproduzir, modificar, adaptar, distribuir, representar e divulgar publicamente a Plataforma, os Serviços e o conteúdo, à exceção do expressamente autorizado pela BlaBlaCar;

  (ii) descompilar e utilizar engenharia reversa na Plataforma ou nos Serviços, salvaguardando as exceções estipuladas pelos textos em vigor;

  (iii) extrair ou tentar extrair (nomeadamente utilizando robôs de extração de dados ou qualquer outra ferramenta similar de recolha de dados) uma parte substancial dos dados da Plataforma.

### **11.2. Conteúdo publicado por si na Plataforma**

Para permitir a prestação dos Serviços, e em conformidade com a finalidade da Plataforma, concede à BlaBlaCar uma licença não exclusiva para utilizar o conteúdo e os dados por si fornecidos no contexto da sua utilização dos Serviços, que podem incluir os seus pedidos de Reserva, Anúncios de Carpooling e comentários nos mesmos, biografias, fotografias, avaliações e respostas a avaliações (doravante designados como o seu “**Conteúdo de Membro**”) e Mensagens. Para permitir à BlaBlaCar distribuir através da rede digital e de acordo com qualquer protocolo de comunicação (nomeadamente internet e rede móvel), e fornecer o conteúdo da Plataforma ao público, autoriza a BlaBlaCar, a reproduzir, representar, adaptar e traduzir o seu Conteúdo de Membro, para todo o mundo e durante todo o período das suas relações contratuais com a BlaBlaCar, da seguinte forma:

  (i)  autoriza a BlaBlaCar a reproduzir, no todo ou em parte, o seu Conteúdo de Membro em qualquer suporte de gravação digital, conhecido ou ainda desconhecido e, nomeadamente, em qualquer servidor, disco rígido, cartão de memória ou qualquer outro suporte equivalente, em qualquer formato e por qualquer processo, conhecido ou ainda desconhecido, no limite do necessário para qualquer operação de armazenamento, cópia de segurança, transmissão ou transferência associada à operação da Plataforma e à prestação do Serviço;

  (ii) autoriza a BlaBlaCar a adaptar e traduzir o seu Conteúdo de Membro, e a reproduzir tais adaptações em qualquer suporte digital, existente ou futuro, estipulado no ponto (i) supra, com o objetivo de fornecer os Serviços, particularmente em diferentes idiomas. Este direito inclui, nomeadamente, a opção de efetuar modificações à formatação do seu Conteúdo de Membro, respeitando o seu direito moral, para fins de respeitar a carta de gráficos da Plataforma e/ou de o tornar tecnicamente compatível com vista à sua publicação através da Plataforma.

O utilizador é responsável e tem todos os direitos sobre o Conteúdo dos Membros e as Mensagens que carrega na nossa Plataforma.

**12\. Função da BlaBlaCar**
----------------------------

A Plataforma constitui uma plataforma de networking online na qual os Membros podem criar e publicar Anúncios de Carpooling para Viagens de Carpooling para fins de partilha de transporte e reservar bilhetes de autocarro de Operadores de Autocarros. Esses Anúncios de Carpooling podem, nomeadamente, ser vistos pelos outros Membros para ficarem a saber os termos da Viagem e, se for o caso, para reservarem diretamente um Lugar no veículo em questão com o Membro que tiver publicado o Anúncio na Plataforma.  A Plataforma também permite a reserva de Lugares para Viagens de Autocarro.

Ao utilizar a Plataforma e aceitar os presentes T&C, reconhece que a BlaBlaCar não é parte em qualquer acordo celebrado entre si e os outros Membros com vista à partilha dos custos relacionados com uma Viagem nem qualquer acordo realizado entre si e o Operador de Autocarros.

A BlaBlaCar não tem qualquer controlo sobre o comportamento dos seus Membros, Operadores de Autocarros e respetivos agentes e utilizadores da Plataforma. Não detém, explora, fornece ou gere os veículos objeto dos Anúncios e não oferece nenhuma Viagem na Plataforma.

Reconhece e aceita que a BlaBlaCar não controla a validade, veracidade ou legalidade dos anúncios, Lugares e Viagens oferecidos. Na sua qualidade de intermediária na partilha de transporte, a BlaBlaCar não fornece nenhum serviço de transporte e não age na qualidade de transportadora; a função da BlaBlaCar limita-se a facilitar o acesso à Plataforma.

No contexto de Viagens de Carpooling, os Membros (Condutores ou Passageiros) atuam sob sua única e total responsabilidade.

Na sua qualidade de intermediária, a BlaBlaCar não pode ser responsabilizada pela ocorrência efetiva de uma Viagem ou uma viagem de autocarro, nomeadamente, devido a:

  (i) informação errada comunicada pelo Condutor ou Operador de Autocarros no seu Anúncio, ou por qualquer outro meio em relação a uma Viagem e respectivos termos;;

  (ii) cancelamento ou modificação de uma Viagem por um Membro ou por um pelo Operador de Autocarros;

  (iii) comportamento dos seus Membros durante, antes ou depois da Viagem ou de uma viagem de autocarro.

**13\. Operação, disponibilidade e funcionalidades da Plataforma**
------------------------------------------------------------------

A BlaBlaCar tentará, dentro do possível, manter a Plataforma acessível permanentemente – 24 horas por dia, sete dias por semana. Não obstante, o acesso à Plataforma poderá ser temporariamente suspenso, sem aviso, devido a operações de manutenção técnica, migração ou atualização ou devido a falhas ou limitações associadas ao funcionamento da rede.

Além disso, a BlaBlaCar reserva-se o direito de alterar a Plataforma ou partes da mesma para alguns utilizadores testarem novas funcionalidades e oferecerem uma melhor experiência de utilizador, bem como modificar ou suspender todo ou parte do acesso à Plataforma ou às suas funcionalidades, a seu exclusivo critério.

**14\. Modificação dos T&C**
----------------------------

Os presentes T&C e os documentos integrados por referência expressam o acordo total entre si e a BlaBlaCar em relação à utilização dos Serviços por sua parte. Qualquer outro documento, nomeadamente qualquer referência na Plataforma (Perguntas Frequentes, etc.), destina-se exclusivamente a fins de orientação.

A BlaBlaCar pode modificar os presentes T&C para os adaptar ao seu ambiente tecnológico e comercial, e para cumprir a legislação em vigor. Qualquer modificação destes T&C será publicada na Plataforma indicando a data de entrada em vigor, e será notificado pela BlaBlaCar antes da entrada em vigor da mesma.

**15\. Legislação aplicável – Litígio**
---------------------------------------

Os presentes T&C estão redigidos em Português e regem-se pela legislação portuguesa.

**15.1 Sistema interno de tratamento de queixas**

O utilizador pode recorrer das decisões que possamos tomar relativamente a:

* Conteúdo de Membro: por exemplo, removemos, restringimos a visibilidade ou recusámo-nos a remover qualquer Conteúdo de Membro que o utilizador forneça ao utilizar a Plataforma, ou

* A sua conta: suspendemos o seu acesso à Plataforma,

quando tomámos essas decisões com base no facto de o Conteúdo de Membro constituir um conteúdo ilegal ou ser incompatível com estes T&C. O processo de recurso é descrito [aqui](https://support.blablacar.com/s/article/Como-contestar-a-remo%C3%A7%C3%A3o-de-conte%C3%BAdo-ou-a-suspens%C3%A3o-da-conta-1729197123038?language=pt_PT).

**15.2 Resolução extrajudicial de litígios**

Pode também, se necessário, apresentar as suas reclamações em relação à nossa Plataforma ou aos nossos Serviços na plataforma de resolução de litígios colocada online pela Comissão Europeia, acessível [aqui](https://ec.europa.eu/consumers/odr/main/?event=main.home2.show). A Comissão Europeia enviará a sua reclamação para o provedor de justiça nacional competente. Em conformidade com as regras aplicáveis à mediação, antes de qualquer pedido de mediação, terá de ter notificado a BlaBlaCar, por escrito, de qualquer litígio de forma a obter uma solução amigável.

Pode também apresentar um pedido a uma entidade de resolução de litígios do seu país (pode encontrar a lista [aqui](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)). 

**16\. Avisos legais**
----------------------

A Plataforma é publicada pela Comuto SA, sociedade limitada com o capital social de 166,591.952 euros, registada no Registo Comercial de Paris sob o número 491.904.546 (número de IVA intracomunitário: FR76491904546), com sede social sita em 84, avenue de la République, 75011 Paris (França), representada pelo seu Diretor Executivo Nicolas Brusson, Diretor de Publicação do Sítio Web.

O sítio Web está alojado nos servidores da Google Ireland Limited sedeada em Gordon House, Barrow Street, Dublin 4 (Irlanda).

Comuto SA está registada no registo de Operadores de Viagens e Visitas ao abrigo do seguinte número: IM075180037

A garantia final é fornecida por: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Paris – França

O seguro de responsabilidade civil profissional é subscrito a: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paris – França.

Comuto SA está registada no registo de mediador de seguros, banca e financiamento ao abrigo do número registado (Orias) 15003890.

Para esclarecer qualquer dúvida, poderá contactar a Comuto SA utilizando este [formulário de contacto](https://support.blablacar.com/s/contactsupport?language=pt_PT).

**17\. Digital Services Act (Regulamento dos Serviços Digitais)**
-----------------------------------------------------------------

**17.1.  Informações sobre os destinatários activos médios mensais do serviço na União**

Em conformidade com o n.º 2 do artigo 24.º do Regulamento do Parlamento Europeu e do Conselho relativo a um mercado interno dos serviços digitais e que altera a Diretiva 2000/31/CE (“DSA”), os fornecedores de plataformas em linha são obrigados a publicar informações sobre a média mensal de destinatários activos do seu serviço na União, calculada como uma média dos últimos seis meses. O objetivo desta publicação é determinar se um fornecedor de plataformas em linha cumpre o critério de “plataformas em linha de muito grande dimensão” ao abrigo da DSA, ou seja, se excede o limiar de 45 milhões de destinatários ativos mensais médios na União.

Em 17 de fevereiro de 2025, o número médio mensal de destinatários ativos da BlaBlaCar para o período entre agosto de 2024 e janeiro de 2025, calculado tendo em conta o considerando 77 e o artigo 3.º da DSA (contando os utilizadores expostos ao conteúdo da Plataforma apenas uma vez durante o período de 6 meses), antes da adoção de um ato delegado específico, era de aproximadamente 3,75 milhões na UE.

Esta informação é publicada apenas para efeitos de cumprimento dos requisitos da DSA e não deve ser utilizada para outros fins. Será actualizada, pelo menos, de seis em seis meses. A nossa abordagem para efetuar este cálculo pode evoluir ou exigir alterações ao longo do tempo, por exemplo, devido a alterações nos produtos ou a novas tecnologias.

**17.2. Ponto de contacto para as autoridades**

Em conformidade com o artigo 11.º do DSA, se for membro das autoridades competentes da UE, da Comissão Europeia ou do Conselho Europeu para os Serviços Digitais, pode contactar-nos relativamente a questões relacionadas com o DSA através do e-mail [\[email protected\]](https://blog.blablacar.pt/cdn-cgi/l/email-protection).

Pode contactar-nos em inglês e francês.

Note-se que este endereço de correio eletrónico não é utilizado para comunicações com os membros. Para quaisquer questões relacionadas com a utilização da BlaBlaCar, o Membro pode contactar-nos através deste [formulário de contacto](https://support.blablacar.com/s/contactsupport?language=pt_PT).

**18\. Utilização do BlaBlaBono Energético (apenas para Espanha) – aplicável a partir de 17 de janeiro de 2025**
----------------------------------------------------------------------------------------------------------------

Os passageiros elegíveis para a oferta BlaBlaBono Energético, disponibilizada na plataforma [www.blablacar.es](http://www.blablacar.es/), têm a possibilidade de reservar uma viagem partilhada em Espanha utilizando o seu bónus BlaBlaBono na plataforma [www.blablacar.pt](http://www.blablacar.pt/) ou na aplicação em Portugal, sujeita às Condições de Utilização deste bónus disponíveis [aqui](https://blog.blablacar.pt/about-us/terms-and-conditions).

* * *

**Condições gerais de utilização em vigor a partir de 17 de fevereiro de 2024.**

**1\. Assunto**
---------------

A Comuto SA (doravante designada “**BlaBlaCar**”) desenvolveu uma plataforma de partilha de veículo acessível num sítio web no endereço [www.blablacar.pt](http://www.blablacar.pt/) ou na forma de uma aplicação móvel, e concebida (i) para pôr os condutores que viajam para um determinado destino em contacto com passageiros que vão na mesma direção, de forma a poderem partilhar a Viagem e, portanto, os custos associados e (ii) para reservar bilhetes para viagens de autocarro operadas por Operadores de Autocarros (doravante designada a “**Plataforma**”).

Os presentes termos e condições têm a finalidade de reger o acesso e os termos de utilização da Plataforma. Agradecemos que os leia atentamente. Entende e reconhece que a BlaBlaCar não é uma parte em qualquer acordo, contrato ou relação contratual, seja de que natureza for, celebrados entre os Membros da Plataforma ou entre o si e o Operador de Autocarros.

Ao clicar em “Iniciar sessão com o Facebook” ou “Registar-se com um endereço de e-mail”,  reconhece ter lido e aceitado todas as presentes condições gerais de utilização.

Se utilizar a sua Conta para iniciar sessão na plataforma BlaBlaCar de outro país (por exemplo, [www.blablacar.com.br)](http://www.blablacar.ru/), lembramos que serão aplicados (i) os termos e condições dessa plataforma, (ii) a política de privacidade dessa plataforma e (iii) a legislação e regulamentos desse país. Isto também significa que as informações da sua Conta, incluindo os dados pessoais poderão ter de ser transferidas para a entidade legal que opera a outra plataforma. Tenha em atenção que a BlaBlaCar reserva-se o direito de restringir o acesso à Plataforma para utilizadores razoavelmente identificados como estando localizados fora do território da União Europeia.

**2\. Definições**
------------------

Neste documento,

“**Anúncio**” refere-se ao anúncio de uma Viagem publicado na Plataforma por um Condutor;

“**Anúncio de Carpooling**” significa um anúncio referente a uma Viagem publicado na Plataforma por um Condutor;

“**Anúncio de Autocarro**” refere-se a um anúncio relativo a uma Viagem de autocarro de um Operador de Autocarro publicado na Plataforma;

“**Bilhete**” significa o título de transporte válido nominativo entregue ao Cliente, após a Reserva de uma Viagem de Autocarro, como prova do contrato de transporte existente entre os Passageiros e o Operador de Autocarro regido pelos T&CG de venda, sem prejudicar qualquer condição particular estipulada adicionalmente entre o Passageiro e o Operador de Autocarro e referido no Bilhete;

“**BlaBlaCar**” tem o significado que lhe é dado no Artigo 1.º, supra;

“**Cliente**” significa qualquer pessoa física (Membro ou não) que compre, para si mesmo, ou para outra pessoa que será o Passageiro, um Bilhete através da Plataforma para realizar uma Viagem organizada pelo Operador de Autocarros;

“**Conta**” refere-se à conta que deve ser criada para se tornar Membro e aceder a determinados serviços oferecidos pela Plataforma;

“**Conta do Facebook**” tem o significado que lhe é dado no Artigo 3.2, infra;

“**Condutor**” refere-se ao Membro que utiliza a Plataforma para se oferecer para transportar outro indivíduo em troca da Contribuição para Custos, para uma Viagem e a uma hora definidas apenas pelo Condutor;

“**Confirmação de Reserva**” tem o significado que lhe é dado no Artigo 4.2.1, infra;

“**Conteúdo de Membro**” tem o significado que lhe é dado no Artigo 11.2, infra;

“**Contribuição para Custos**” refere-se, para uma determinada Viagem, à soma de dinheiro solicitada pelo Condutor e aceite pelo Passageiro como contribuição deste para os custos da viagem;

“**Encomenda**” significa a operação pela qual o Cliente reserva os seus serviços com o Autocarro da BlaBlaCar, independentemente do meio utilizado, com a única exceção da compra dos Bilhetes realizada diretamente num Ponto de Venda, e que engloba a obrigação de o Passageiro ou possivelmente um Agente de pagar o preço relativo aos Serviços relevantes;

“**Fornecedor da** **Solução de Pagamento Hyperwallet**” significa a PayPal (Europe) S.à r.l. et Cie, S.C.A.

“**Lugar**” refere-se ao lugar reservado por um Passageiro no veículo do Condutor ou no veículo do Operador do Autocarro;

“**Membro**” refere-se a qualquer indivíduo que tenha criado uma Conta na Plataforma;

“**Operador de Autocarros**” refere-se a uma empresa profissional de transporte de passageiros cujos bilhetes de viagem são distribuídos na Plataforma pela BlaBlaCar;

“**Passageiro**” refere-se ao Membro que aceitou a oferta de ser transportado pelo Condutor ou, se for o caso, a pessoa em cujo nome o Membro reservou um Lugar;

“**Percurso**” tem o significado que lhe é dado no Artigo 4.1, infra;

“**Plataforma**” tem o significado que lhe é dado no Artigo 1.º, supra;

“**Ponto de Venda**” refere-se aos contadores físicos e terminais listados no sítio Web e nos quais os Bilhetes são oferecidos para venda;

“**Preço**” refere-se, para uma determinada viagem de Autocarro, ao preço, incluindo todos os impostos, taxas e custos de reserva relevantes incluídos, pagos pelo Cliente na Plataforma, na altura da validação da Encomenda, por um Lugar numa determinada viagem de Autocarro;

“**Reserva**” tem o significado que lhe é dado no Artigo 4.2.1., infra;

“**Serviços de Transporte**” refere-se aos serviços de transporte subscritos por um Passageiro de uma viagem de Autocarro e fornecidos pelo Operador do Autocarro;

“**Serviços**” refere-se a todos os serviços fornecidos pela BlaBlaCar através da Plataforma;

“**Sítio Web**” refere-se ao sítio web acessível no endereço [www.blablacar.pt](http://www.blablacar.pt/);

“**Solução de Pagamento Hyperwallet**” tem o significado que lhe é dado no Artigo 5.4.2., (a), infra;

“**Taxa de Reserva**” tem o significado que lhe é dado no Artigo 5.2, infra;

“**T&C**” refere-se a estes Termos e Condições;

“**T&CG de Venda**” significa os [Termos e Condições Gerais de Venda do Operador de Autocarros](https://blog.blablacar.pt/blablalife/comunidade/eventos/condicoes-gerais-de-venda-de-transporte) envolvido dependendo da Viagem de Autocarro selecionada pelo Cliente, e os termos e condições especiais, disponíveis no Website, e que foram reconhecidos como lidos pelo Cliente antes da encomenda;

“**Viagem**” indistintamente, refere-se a uma Viagem de Autocarro ou Viagem de Carpooling

“**Viagem de Carpooling**” refere-se à viagem objeto de um Anúncio de Carpooling publicado por um Condutor na Plataforma, e pela qual o mesmo aceita transportar Passageiros em troca da Contribuição para Custos;

“**Viagem de Autocarro**” significa a viagem objeto de um anúncio de autocarro na Plataforma e para a qual o Operador de Autocarros propõe lugares nos autocarros em troca do Preço.

**3\. Registo na Plataforma e criação de uma Conta**
----------------------------------------------------

### **3.1. Condições de registo na Plataforma**

A Plataforma pode ser utilizada por indivíduos com idade igual ou superior a 18 anos. O registo na plataforma por um menor é estritamente proibido. Ao aceder, utilizar ou registar-se na Plataforma, declara e garante que tem idade igual ou superior a 18 anos.

### **3.2. Criação de uma Conta**

A Plataforma permite que os Membros publiquem e visualizem Anúncios de Carpooling, e que interajam entre si para reservar um Lugar. Pode visualizar os Anúncios se não estiver registado na Plataforma. No entanto, não pode publicar um Anúncio de Carpooling nem reservar um Lugar sem ter primeiro criado uma Conta e se ter tornado Membro.

Para criar a sua Conta, pode:

(i) preencher todos os campos obrigatórios no formulário de registo;

(ii) ou iniciar sessão na sua conta do Facebook através da nossa Plataforma (doravante designada como a sua “Conta do Facebook”). Ao utilizar tal funcionalidade, entende que a BlaBlaCar terá acesso, publicará na Plataforma e guardará determinadas informações da sua Conta do Facebook. Poderá eliminar a hiperligação entre a sua Conta e a sua Conta do Facebook a qualquer momento através da secção “Certificações” do seu perfil. Se pretender saber mais acerca da utilização dos seus dados da Conta do Facebook, leia a nossa [Política de Privacidade](https://www.blablacar.pt/about-us/privacy-policy) e a do Facebook.

Para se registar na Plataforma, deverá ter lido e aceitado os presentes T&C.

Ao criar a sua Conta, independentemente do método escolhido, aceita fornecer informações exatas e verdadeiras, e atualizá-las através do seu perfil ou notificando a BlaBlaCar, de forma a garantir a relevância e exatidão das mesmas durante as suas relações contratuais com a BlaBlaCar.

No caso do registo por e-mail,  aceita manter a palavra-passe escolhida após a criação da sua Conta confidencial e a não a comunicar a ninguém. Se perder ou divulgar a sua palavra-passe, compromete-se a informar de imediato a BlaBlaCar. É o único responsável pela utilização da sua Conta por terceiros, salvo se tiver expressamente notificado a BlaBlaCar da perda, do uso fraudulento por um terceiro ou da divulgação da sua palavra-passe a terceiros.

A aceita não criar ou utilizar, sob a sua própria identidade ou a de outra pessoa, outras Contas que não a inicialmente criada.

### **3.3. Verificação**

A BlaBlaCar pode, por questões de transparência, melhoria da confiança ou prevenção ou deteção de fraude, criar um sistema para verificação de algumas das informações que fornece no seu perfil. Tal é, em particular, o que se passa quando introduz o seu número de telefone ou nos fornece um documento de Identidade.

Reconhece e aceita que qualquer referência na Plataforma ou nos Serviços a informação “certificada”, ou qualquer termo similar, significa apenas que um Membro passou com êxito o procedimento de verificação existente na Plataforma ou nos Serviços de forma a fornecer-lhe mais informações acerca do Membro com quem está a pensar viajar. A BlaBlaCar não pode garantir a veracidade, fiabilidade ou validade da informação objeto do procedimento de verificação.

**4\. Utilização dos Serviços**
-------------------------------

### **4.1. Publicação de Anúncios**

Como Membro, e desde que cumpra as condições infra, pode criar e publicar Anúncios de Carpooling na Plataforma introduzindo informações acerca da Viagem que pretende fazer (datas/horas e pontos de recolha e chegada, número de lugares oferecidos, opções disponíveis, montante da Contribuição para Custos, etc.).

Ao publicar o seu Anúncio de Carpooling, pode indicar as cidades nas quais aceita parar e recolher ou deixar Passageiros. As secções da Viagem de Carpooling entre essas cidades ou entre uma delas e o ponto de recolha ou destino da Viagem de Carpooling constituem os “**Percursos**”.

Apenas está autorizado a publicar um Anúncio se cumprir todas as condições seguintes:

(i) tem uma carta de condução válida;

(ii) só oferece Anúncios de Carpooling para veículos seus ou que utiliza com permissão expressa do proprietário e que, de qualquer forma, está autorizado a utilizar para fins de partilha de transporte;

(iii) é e continua a ser o principal condutor do veículo objeto do Anúncio de Carpooling;

(iv) o veículo tem um seguro contra terceiros válido;

(v) não tem contraindicações ou incapacidade médica para a condução;

(vi) o veículo que pretende utilizar para a Viagem é um veículo ligeiro de passageiros com 4 rodas e o máximo de 7 lugares, excluindo os veículos “sem carta” – automóveis que não necessitam de licença de condução.

(vii) não tenciona publicar outro anúncio para a mesma Viagem de Carpooling na Plataforma;

(viii) não oferece mais Lugares do que o número disponível no seu veículo;

(ix) todos os Lugares oferecidos têm cinto de segurança, ainda que o veículo esteja aprovado com lugares sem cinto de segurança;

(x) a utilizar um veículo em boas condições de funcionamento e que cumpra as disposições legais aplicáveis, nomeadamente com um certificado de IPO atualizado;

(xi) é um consumidor e não atua como profissional.

Reconhece que é o único responsável pelo conteúdo do Anúncio de Carpooling que publica na Plataforma. Por conseguinte, declara e garante a exatidão e veracidade de todas as informações contidas no Anúncio de Carpooling e compromete-se a realizar a Viagem nas condições descritas no seu Anúncio de Carpooling.

O seu Anúncio de Carpooling cumpirá os T&C, o mesmo será publicado na Plataforma ficando, assim, visível para os Membros e todos os visitantes, ainda que não sejam membros, que efetuem uma pesquisa na Plataforma ou no sítio web de parceiros da BlaBlaCar. A BlaBlaCar reserva-se o direito de não publicar ou de remover, a qualquer momento e por sua única iniciativa, qualquer Anúncio de Carpooling que não cumpra os T&C ou que a mesma considere que prejudica a sua imagem, a imagem da Plataforma ou dos Serviços e/ou suspender a Conta do Membro que publique tais Anúncios de Carpooling de acordo com a Secção 9 dos presentes T&C.

Reconhece e aceita que os critérios tidos em conta na classificação e na ordem de apresentação do seu Anúncio entre os outros Anúncios ficam exclusivamente ao critério da BlaBlaCar.

Informamos que caso se apresente como um consumidor ao utilizar a Plataforma quando na realidade atua como profissional, fica exposto a sanções contempladas pela lei aplicável.

### **4.2.** Reservar um Lugar em carpooling

A BlaBlaCar estabeleceu um sistema para reservar Lugares online (“Reserva”) para as Viagens oferecidas na Plataforma.

Os métodos de reserva de um Lugar dependem da natureza da Viagem em questão.

A BlaBlaCar fornece-lhe na Plataforma um motor de pesquisa com base em diferentes critérios de pesquisa (Local de origem, destino, datas, número de passageiros, etc.). Certas funcionalidades adicionais são fornecidas no motor de pesquisa quando está ligado à sua Conta. A BlaBlaCar convida-o(a), independentemente do processo de reserva utilizado, a consultar atentamente e utilizar o motor de pesquisa para determinar a oferta mais adaptada às suas necessidades. Poderá encontrar mais informações [aqui](https://blog.blablacar.pt/about-us/transparencia-das-plataformas). O Cliente, ao reservar uma Viagem de Autocarro num Ponto de Venda, também pode solicitar ao Operador de Autocarros ou agente de balcão para realizar a pesquisa.

**4.2.1** **Viagem de Carpooling**

Quando um Passageiro está interessado num Anúncio de Carpooling que beneficie desta Reserva, pode efetuar um pedido de reserva online. Este pedido de Reserva é (i) aceite automaticamente (se o Condutor escolher esta opção ao publicar o seu Anúncio), ou (ii) aceite manualmente pelo Condutor. No momento da Reserva, o Passageiro efetua o pagamento online da Contribuição para Custos e da Taxa de Reserva relacionadas, se for o caso. Após receber o pagamento pela BlaBlaCar e a validação do pedido de Reserva pelo Condutor, se for o caso, o Passageiro recebe uma confirmação de Reserva (a “**Confirmação de Reserva**”).

Se for um Condutor e tiver escolhido tratar manualmente os pedidos de reserva ao publicar o seu Anúncio de Carpooling, terá de responder a qualquer pedido de Reserva especificado pelo Passageiro durante o pedido de Reserva. Caso contrário, o pedido de reserva expirará automaticamente e o Passageiro será reembolsado por todas as somas pagas no momento do pedido de Reserva, se as houver.

No momento da Confirmação de Reserva, a BlaBlaCar envia-lhe o número de telefone do Condutor (se for o Passageiro) ou do Passageiro (se for o Condutor), se o Membro tiver aceitado apresentar o seu número de telefone. Será, então, o único responsável pela execução do contrato que o vincula ao outro Membro.

**4.2.2** **Viagem de Autocarro**

Para Viagens de Autocarro, a BlaBlaCar permite a reserva dos Bilhetes de Autocarro para uma determinada Viagem de Autocarro através da Plataforma.

Os Serviços de Transporte são regidos pelos T&CG de Venda do Operador de Autocarros relevante, dependendo da Viagem selecionada pelo Cliente, que tem de ser aceite pelo Cliente antes de realizar a encomenda. A BlaBlaCar não fornece quaisquer serviços de transporte em relação a Viagens de Autocarro. Os Operadores de Autocarro são exclusivamente parte dos T&CG de Venda. Reconhece que a reserva de Lugares para uma determinada Viagem de Autocarro está sujeita aos T&C de Venda do operador de Autocarro relevante.

A BlaBlaCar chama a sua atenção para o facto de que certos Serviços de Transporte oferecidos pelo Operador de Autocarros e mencionados no Sítio Web podem ser retirados, em particular por motivos climáticos, uso sazonal ou em caso de Força Maior.

**4.2.3** **Natureza designada da reserva do Lugar e termos de utilização dos Serviços em nome de terceiro**

Qualquer utilutilização dos Serviços, na qualidade de Passageiro ou Condutor, está relacionada com um nome específico. O Condutor e o Passageiro devem corresponder à identidade comunicada à BlaBlaCar e aos outros Membros que participam na Viagem de Carpooling ou Operador de Autocarros.

Contudo, a BlaBlaCar permite que os seus Membros reservem um ou mais Lugares em nome de um terceiro. Nesse caso, compromete-se a indicar com precisão ao Condutor, no momento da Reserva, os apelidos, idade e número de telefone da pessoa em cujo nome está a reservar um Lugar. É estritamente proibido reservar um Lugar para um menor com idade inferior a 13 anos que viaje sozinho para uma Viagem de Carpooling. Se reservar um lugar numa Viagem de Carpooling para um menor com idade superior a 13 anos que viaje sozinho, compromete-se a solicitar o consentimento prévio do Condutor e a fornecer-lhe a permissão devidamente preenchida e assinada dos representantes legais da criança.

Além disso, a Plataforma destina-se à reserva de Lugares para indivíduos. É proibido reservar um Lugar para transportar qualquer objeto, pacote, animal a viajar sozinho ou qualquer tipo de material.

É ainda proibido publicar um Anúncio para um Condutor que não seja o utilizador.

### **4.3.** **Conteúdo de Membro, moderação e s****istema de avaliação**

**4.3.1. Funcionamento do sistema de avaliação**

A BlaBlaCar incentiva-o(a) a deixar a sua avaliação acerca de um Condutor (se for um Passageiro) ou acerca de um Passageiro (se for um Condutor) com quem tenha partilhado uma Viagem ou com quem deveria ter partilhado uma Viagem (ou seja, reservado uma Viagem).. No entanto, não poderá deixar avaliações acerca de outro Passageiro, se for um Passageiro, ou acerca de um Membro com o qual não tenha viajado ou com o qual não deveria ter viajado.

A sua avaliação e a avaliação deixada por outro Membro acerca de si, se aplicável, só são visíveis e publicadas na Plataforma passado o mais curto dos seguintes períodos: (i) imediatamente após terem ambos deixado uma avaliação no prazo de 14 dias a seguir à Viagem; ou (ii) passado um período de 14 dias após a primeira avaliação.

A opção de deixar uma avaliação deixará de estar disponível 14 dias após a Viagem. No entanto, tem a opção de responder a uma avaliação que outro Membro tenha deixado no seu perfil num prazo de 14 dias a seguir à data da avaliação recebida. A avaliação e a sua resposta, se for o caso, serão publicadas no perfil deste.

**4.3.2.** **Moderação**

**a. Conteúdo de membro**

Reconhece e aceita que a BlaBlaCar pode moderar, antes da publicação, utilizando ferramentas automatizadas ou de forma manual, o Conteúdo de Membro, tal como definido na secção 11.2. Se a BlaBlaCar considerar que tal Conteúdo de Membro viola as leis aplicáveis ou os presentes T&C, ela reserva-se o direito de:

● Impedir a publicação ou excluir tal Conteúdo de Membro

● Enviar um aviso ao Membro, lembrando-o da obrigação de cumprimento das leis aplicáveis ou dos presentes Termos e Condições, e/ou

● Aplicar as medidas restritivas de acordo com a Secção 9 dos presentes T&C.

O uso pela BlaBlaCar de tais ferramentas automatizadas ou de moderação manual não deve ser interpretado como um compromisso de monitorizar, ou uma obrigação de procurar ativamente, atividades ilegais e ou Conteúdo de Membro publicado na Plataforma e, na medida permitida pela lei aplicável, não dá origem a qualquer responsabilidade para a BlaBlaCar.

**b. Mensagens**

A troca de mensagens entre Membros através da nossa Plataforma (“Mensagens”) tem como único objetivo a troca de informações relativas a Viagens de Carpooling.

A BlaBlaCar pode, através de software e algoritmos automatizados, detetar o conteúdo das Mensagens com a finalidade de prevenção de fraudes, melhoria do serviço, apoio ao cliente e execução dos contratos celebrados com os nossos membros (tais como estes T&C). Em caso de deteção de qualquer conteúdo numa Mensagem que mostre sinais de comportamento fraudulento ou ilegal, evasão da Plataforma ou que seja contrário aos presentes T&C:

● Esse conteúdo não pode ser publicado

● O Membro que enviou a Mensagem pode receber um aviso lembrando-o da obrigação de respeitar a legislação aplicável ou os presentes T&C, ou

● A conta do Membro poderá ser suspensa de acordo com a Secção 9 dos presentes T&C.

O uso de tal software automatizado pela BlaBlaCar não pode ser interpretado como um compromisso de monitorizar, ou uma obrigação de procurar ativamente atividades e/ou conteúdos ilícitos na Plataforma e, na medida do permitido pelas leis aplicáveis, não gera qualquer responsabilidade para a BlaBlaCar.

**4.3.3.** **Limite**

A BlaBlaCar reserva-se o direito de suspender a sua Conta, limitar o acesso deste aos Serviços ou cessar os presentes T&C caso (i)  tenha recebido pelo menos três avaliações e (ii) a média das avaliações recebidas seja igual ou inferior a 3.

**5\. Condições financeiras**
-----------------------------

O acesso à Plataforma e o registo na mesma, bem como a pesquisa, visualização e publicação de Anúncios, são gratuitos. No entanto, a Reserva é cobrada segundo as condições descritas infra.

### **5.1.** Contribuição para Custos e Preço

5.1.1 Em caso de viagem de Carpooling, a Contribuição para Custos é determinada por si, na qualidade de Condutor, sob sua única responsabilidade. É estritamente proibido lucrar, seja de que forma for, da utilização da nossa Plataforma. Por conseguinte, aceita limitar a Contribuição para Custos cujo pagamento solicita aos seus Passageiros aos custos realmente incorridos por si para realizar a Viagem de Carpooling. Caso contrário, será o único a suportar os riscos da recaraterização da transação concluída através da Plataforma.

Quando publica um Anúncio de Carpooling, a BlaBlaCar sugere um montante para a Contribuição para Custos, que tem, nomeadamente, em conta a natureza da Viagem de Carpooling e a distância percorrida. Este montante é dado apenas como orientação e cabe a si aumentá-lo ou reduzi-lo para ter em conta os custos em que  incorre realmente na Viagem de Carpooling. Para evitar abusos, a BlaBlaCar limita a possibilidade de ajustamento da Contribuição para Custos.

5.1.2 Relativamente à Viagem de Autocarro, o Preço por Lugar é definido ao próprio critério do Operador do Autocarro. O Cliente é convidado a referir-se aos T&CG de Venda para compreender as modalidades aplicáveis em relação à realização de encomenda de Bilhetes e modalidades de pagamento.

### **5.2. Taxa de Reserva**

A BlaBlaCar, em troca da utilização da Plataforma, no momento da Reserva irá cobrar taxa de reserva (doravante designadas como as “**Taxa de Reserva**”). Será informado antes de qualquer aplicação das Taxa de reserva, quando necessárias. Os métodos de cálculo das Taxa de Reserva em vigor que se encontram-se [aqui](https://blog.blablacar.pt/blablalife/lp/gastos-de-gestao) e são apenas para fins informativos  e não têm valor contratual. A BlaBlaCar reserva-se o direito de alterar em qualquer altura os métodos de cálculo das Taxa de Reserva. Estas modificações  não afetarão as Taxa de Reserva aceites pelo si antes da data de entrada em vigor destas modificações.

No caso das viagens transfronteiras, lembramos que os métodos de cálculo do montante das Taxa de Reserva e o IVA aplicável variam de acordo com  o ponto de recolha e/ou destino da Viagem.

Ao utilizar a Plataforma para viagens transfronteiras ou fora de Portugal, as Taxa de Reserva poderão ser cobradas por uma empresa afiliada da BlaBlaCar a operar a plataforma local.

### **5.3. Arredondamentos**

Reconhece e aceita que a BlaBlaCar pode, por sua única iniciativa, arredondar para números inteiros ou decimais as Taxa de Reserva e a Contribuição para Custos por excesso ou por defeito.

### **5.4. Métodos de pagamento e reembolso da Contribuição para Custos ao Condutor ou Preço para Operadores de Autocarros**

**5.4.1.** **Instruções do Condutor**

Ao utilizar a Plataforma como Condutor para Viagens, confirma que leu e aceitou os [termos e condições](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml) do Fornecedor de Soluções de Pagamento Hyperwallet – o nosso fornecedor de pagamentos que processa as transferências das Contribuições para Custos dos Condutores através da Solução de Pagamento Hyperwallet definida no artigo 5.4.2., (a). Estes termos e condições são ainda referidos como “Termos e Condições da Hyperwallet”.

Deve ter em atenção que as exclusões à Diretiva (UE) 2015/2366 do Parlamento Europeu e do Conselho, de 25 de novembro de 2015, relativa aos serviços de pagamento no mercado interno, enumeradas no artigo 11.º, n.º 4, alíneas i) e ii), dos Termos e Condições da Hyperwallet não se aplicam aos Membros da Plataforma enquanto não profissionais.

Na qualidade de Condutor, dá instruções:

* à BlaBlaCar para pedir ao Fornecedor de Soluções de Pagamento Hyperwallet para efetuar a transferência da Contribuição para Custos, e
* ao Fornecedor de Soluções de Pagamento Hyperwallet para fazer a transferência da Contribuição para Custos para a sua conta bancária ou conta do PayPal em seu nome e por sua conta, de acordo com os Termos e Condições da Hyperwallet.

No âmbito de uma Viagem de Carpooling, e após aceitação manual ou automática da Reserva, a totalidade da soma paga pelo Passageiro (Taxa de Reserva e Contribuição para Custos) é mantida numa conta de garantia gerida pelo Fornecedor de Soluções de Pagamento Hyperwallet.

**5.4.2.** **Pagamento da Contribuição para Custos ao Condutor**

Após a Viagem de Carpooling, os Passageiros terão um período de 24 horas após o final da Viagem para apresentar uma contestação à BlaBlaCar. Na ausência de contestação da parte dos Passageiros dentro deste período, a BlaBlaCar considera a viagem confirmada.

A partir do momento da confirmação expressa ou tácita, terá, na qualidade de Condutor, um crédito na sua Conta. Esse crédito corresponde ao montante total pago pelo Passageiro no momento da confirmação da Reserva, depois de deduzidas as Taxa de Reserva, ou seja, a Contribuição para Custos paga pelo Passageiro.

**a. Solução de Pagamento Hyperwallet**

A Solução de Pagamento Hyperwallet é um serviço de pagamento fornecido pelo Fornecedor de Soluções de Pagamento Hyperwallet. Para que não subsistam dúvidas, a BlaBlaCar não fornece nenhum serviço de processamento de pagamentos para Membros e não entra na posse de fundos dos Membros.

Quando a Viagem é confirmada pelo Passageiro, tem a opção, na qualidade de Condutor, de receber a sua Contribuição para Custos na sua conta bancária ou conta do PayPal.

Para este efeito, o Condutor pode utilizar a Solução de Pagamento Hyperwallet celebrando um contrato diretamente com o Fornecedor de Soluções de Pagamento Hyperwallet e aceitando os Termos e Condições da Hyperwallet.

Para receber a primeira transferência da Contribuição para Custos, será solicitado ao Condutor que selecione um método de pagamento e que forneça os dados da sua conta bancária, endereço postal e data de nascimento, ou ligue a sua conta do PayPal. Caso não o faça, ou caso as informações que forneça estejam incorretas, inexatas ou desatualizadas, a Contribuição para Custos não será transferida.

A transferência será realizada pelo Fornecedor de Soluções de Pagamento Hyperwallet.

No final dos períodos de limite temporal aplicáveis, na aceção da legislação aplicável, qualquer soma relativa à Contribuição para Custos não reclamada por si na qualidade de Condutor através da Solução de Pagamento Hyperwallet será considerada como pertencente à BlaBlaCar.

**b. Procedimento de verificação KYC (“Conheça o seu Cliente”)**

Na utilização da Solução de Pagamento Hyperwallet, aceita que pode estar sujeito aos procedimentos regulamentares aplicados pelo Fornecedor de Soluções de Pagamento, sujeito aos seus [termos e condições](https://www.paylution.com/hw2web/consumer/page/legalAgreement.xhtml).

Esses procedimentos podem incluir verificações de identidade e outros requisitos do procedimento “Conheça o seu cliente” (KYC) com base nos critérios estabelecidos pelo Fornecedor de Soluções de Pagamento Hyperwallet. Esses critérios podem incluir limites financeiros, dependendo do valor total dos pagamentos da Contribuição para Custos efetuados por si.

Para efeitos da verificação KYC, pode-lhe ser pedido que forneça informações adicionais, conforme solicitado pelo Fornecedor de Soluções de Pagamento Hyperwallet. Caso não apresente os documentos exigidos, não poderá receber a Contribuição para Custos até que a sua identidade seja confirmada.

Podem ocorrer situações em que o Fornecedor de Soluções de Pagamento Hyperwallet deva, de acordo com a legislação aplicável, suspender a transferência da Contribuição para Custos ou o acesso à Solução de Pagamento. A BlaBlaCar não tem qualquer influência relativamente a essas ações por parte do Fornecedor de Soluções de Pagamento Hyperwallet.

**5.4.3** **Paragamento do Preço**

O pagamento por qualquer Encomenda realizada através da Plataforma é realizado através de um dos meios autorizados abaixo. O Cliente tem a opção de guardar as informações relativas a um ou mais cartões de crédito na sua Conta de Cliente para que não tenha de introduzir sistematicamente estas informações quando realizar pagamentos subsequentes.

Os meios de pagamento podem variar dependendo da plataforma BlaBlaCar que utiliza. Os meios autorizados de pagamento são os seguintes:

* Cartão bancário (as marcas aceites são apresentadas na plataforma. No caso de ter um cartão multimarca, pode selecionar uma marca específica na fase de pagamento
* Paypal
* Vales
* Apple Pay, Google Pay.

Nenhuma confirmação de Encomenda será emitida até ao pagamento completo e efetivo do preço dos Serviços de Transporte selecionados pelo Cliente. Se o pagamento for irregular, incompleto ou não for realizado por algum motivo atribuível ao Cliente, a encomenda será cancelada imediatamente.

A Encomenda é confirmada ao enviar um e-mail para o Cliente, contendo os detalhes da Encomenda.

O Passageiro é convidado a verificar as definições para a caixa de entrada do seu endereço de e-mail e, em particular, garantir que o e-mail de confirmação não é enviado diretamente para o Spam.

A confirmação da encomenda é final. Consequentemente, quaisquer alterações resultarão na troca ou cancelamento ao abrigo das condições dos T&C de Veda aplicáveis. É da responsabilidade do Cliente garantir que os Serviços de Transporte são escolhidos de acordo com as necessidades e expetativas do Passageiro. A exatidão das Informações Pessoais introduzidas pelo Cliente é da responsabilidade deste último, exceto em caso de falha comprovada da BlaBlaCar em recolher, armazenar ou proteger essas Informações pessoais.

**6\. Finalidade não comercial e não negocial dos Serviços e da Plataforma**
----------------------------------------------------------------------------

Aceita utilizar os Serviços e a Plataforma apenas para ser posto em contacto, de forma não negocial e não comercial, com pessoas que pretendam partilhar uma Viagem de Carpooling consigo ou reservar um Lugar no contexto de uma viagem de Autocarro.

No contexto da Viagem de Carpooling, reconheceu que os direitos do consumidor resultantes da lei de proteção do consumidor podem não se aplicar à sua relação com outros Membros.

Como Condutor, aceita não solicitar uma Contribuição para Custos superior aos custos realmente incorridos e que possa gerar lucro, ficando especificado que, no âmbito de uma partilha de custos, terá de suportar, como Condutor, a sua própria parte dos custos decorrentes da Viagem de Carpooling. É o único responsável pelo cálculo dos custos em que incorre para a Viagem de Carpooling e pela verificação de que o montante pedido aos seus Passageiros não ultrapassa os custos em que realmente incorreu (excluindo a sua parcela dos custos), nomeadamente ao referir-se à escala de tributação aplicável.

A BlaBlaCar reserva-se o direito de suspender a sua Conta caso utilize um veículo com motorista ou outro veículo comercial ou táxi, ou um automóvel de empresa e, por esse motivo, obtenha lucro através da Plataforma. Aceita fornecer à BlaBlaCar, mediante o simples pedido da mesma, uma cópia do certificado de matrícula do seu automóvel e/ou qualquer outro documento que mostre que está autorizado a usar o veículo na Plataforma e que não aufere qualquer lucro dessa forma.

A BlaBlaCar reserva-se também o direito de suspender a sua Conta, limitar o seu acesso aos Serviços ou cessar estes T&C, caso realize alguma atividade na Plataforma que, em virtude da natureza das Viagens oferecidas, da sua frequência, do número de Passageiros transportados e da Contribuição para Custos solicitada, implique uma situação de lucro para si, ou que, de alguma forma, sugira à BlaBlaCar que está a tirar lucro da Plataforma.

**7\. Política de cancelamento**
--------------------------------

### **7.1.** Termos de reembolso em caso de cancelamento  

Apenas as Viagens de Carpooling estão sujeitas a esta política de Cancelamento; a BlaBlaCar não oferece qualquer garantia, seja de que natureza for, no caso de cancelamento por qualquer motivo, por parte de um Condutor ou de um Passageiro. No contexto de uma Viagem de Autocarro, a política de cancelamento está sujeita aos T&CG de Venda dos Operadores de Autocarros. O cliente é convidado a consultar os T&CG de Venda para confirmar as condições de trocas e cancelamento da sua Viagem de Autocarro.

O cancelamento de um Lugar numa Viagem de Carpooling por parte do Condutor ou do Passageiro após a Confirmação de Reserva está sujeito às estipulações que se seguem:

No caso de cancelamento da parte do Condutor, o Passageiro é reembolsado pelo total da soma paga (ou seja, a Contribuição para Custos e as Taxa de Reserva relacionadas). Tal é, nomeadamente, o que acontece quando o Condutor cancela uma Viagem de Carpooling ou não chega ao ponto de encontro 15 minutos após a hora acordada;

No caso de cancelamento por parte do Passageiro:

* Se o Passageiro efetuar o cancelamento mais de 24 horas antes da hora de partida planeada, conforme mencionado no Anúncio de Carpooling, o Passageiro só será reembolsado pela Contribuição para Custos. As Taxa de Reserva são retidas pela BlaBlaCar e o Condutor não recebe qualquer soma, seja de que natureza for;
* Se o Passageiro efetuar o cancelamento 24 horas ou menos antes da hora de partida planeada, conforme mencionado no Anúncio de Carpooling, e mais de trinta minutos após a Confirmação de Reserva, ser-lhe-á reembolsada metade da Contribuição para Custos paga no momento da Reserva, sendo as Taxa de Reserva retidas pela BlaBlaCar, e o Condutor receberá 50% da Contribuição para Custos, através da Solução de Pagamento Hyperwallet;
* Se o Passageiro efetuar o cancelamento 24 horas ou menos antes da hora de partida planeada, conforme mencionado no Anúncio de Carpooling, e trinta minutos ou menos após a Confirmação de Reserva, ser-lhe-á reembolsada a totalidade da Contribuição para Custos, através da Solução de Pagamento Hyperwallet. As Taxa de Reserva são retidas pela BlaBlaCar e o Condutor não recebe qualquer soma, seja de que natureza for;
* Se o Passageiro efetuar o cancelamento após a hora de partida planeada, conforme mencionado no Anúncio de Carpooling, ou se não tiver chegado ao local de encontro 15 minutos após a hora acordada, não será efetuado qualquer reembolso. O Condutor será compensado pela totalidade da Contribuição para Custos, através da Solução de Pagamento Hyperwallet e as Taxa de Reserva ficarão para a BlaBlaCar.

Quando o cancelamento ocorrer antes da partida e por parte do Passageiro, o(s) Lugar(es) cancelado(s) pelo Passageiro será(ão) automaticamente disponibilizado(s) a outros Passageiros que os poderão reservar online e que ficam, em conformidade, sujeitos às condições destes T&C.

A BlaBlaCar aprecia, por sua única iniciativa e com base na informação disponível, a legitimidade dos pedidos de reembolso.

### **7.2. Direito de retratação**

Não tem direito de rescisão a partir do momento da confirmação da reserva, desde que o contrato entre si e a BlaBlaCar, que consiste em colocá-lo em contacto com outro Membro, tenha sido totalmente executado.

**8\. Comportamento dos utilizadores da Plataforma e dos Membros**
------------------------------------------------------------------

### **8.1. Compromisso de todos os utilizadores da Plataforma**

O utilizador reconhece ser o único responsável por respeitar todas as leis, regulamentos e obrigações aplicáveis à sua utilização da Plataforma.

Além disso, ao utilizar a Plataforma e durante as Viagens, o utilizador compromete-se:

* (i) a não utilizar a Plataforma para fins profissionais, comerciais ou lucrativos;
* (ii) a não enviar à BlaBlaCar (nomeadamente, após a criação ou atualização da sua Conta) ou aos outros Membros qualquer informação falsa, enganadora, maliciosa ou fraudulenta;
* (iii) a não falar ou comportar-se de qualquer forma, ou publicar qualquer conteúdo (incluindo Mensagens) na Plataforma, de natureza difamatória, injuriosa, obscena, pornográfica, grosseira, ofensiva, agressiva, não solicitada, violenta, ameaçadora, assediadora, racista ou xenofóbica, ou com conotações sexuais, de incitação à violência, discriminação ou ódio, encorajando atividades ou a utilização de substâncias ilegais ou, mais em geral ilegais, contrárias às leis aplicáveis, a estes T&C e ao propósito da Plataforma, que possa infringir os direitos da BlaBlaCar ou de terceiros, ou vá contra a boa moral;
* (iv) a não infringir os direitos e a imagem da BlaBlaCar, nomeadamente, os seus direitos de propriedade intelectual;
* (v) a não abrir mais de uma Conta na Plataforma e a não abrir uma Conta em nome de terceiros;
* (vi) a não tentar contornar o sistema de reserva online da Plataforma, nomeadamente tentando enviar a outro Membro os seus dados de contacto para efetuar a reserva fora da Plataforma e evitar pagar as Taxa de Reserva;
* (vii) a não contactar outro Membro, nomeadamente através da Plataforma, para fins que não os definidos nos termos da partilha de transporte;
* (viii) a não aceitar ou efetuar pagamentos fora da Plataforma ou da Solução de Pagamento Hyperwallet, salvo nos casos autorizados pelos presentes T&C para as Viagens sem Reserva;
* (ix) a cumprir estes T&C.

### **8.2. Compromissos dos Condutores**

Além disso, quando utiliza a Plataforma como condutor, compromete-se:

  (i) a respeitar todas as leis, regulamentos e códigos aplicáveis à condução e ao veículo, nomeadamente a ter um seguro de responsabilidade civil válido no momento da Viagem de Carpooling e a ser portador de uma carta de condução válida;

  (ii) a confirmar se o seu seguro cobre a partilha de transporte e se os seus Passageiros são considerados como terceiros no seu veículo e estão, dessa forma, abrangidos pelo seu seguro;

  (iii) a não assumir riscos ao conduzir, a não consumir nenhum produto que possa prejudicar a sua atenção e a sua capacidade para realizar uma condução vigilante e completamente segura;

  (iv) a publicar Anúncios de Carpooling que correspondam apenas a Viagens realmente planeadas;

  (v) a realizar a Viagem de Carpooling conforme descrito no anúncio (nomeadamente no que se refere à utilização ou não da autoestrada) e a respeitar os horários e locais acordados com os outros Membros (nomeadamente no que se refere ao local de encontro e ao ponto de largada);

  (vi) a não transportar mais Passageiros do que o número de Lugares indicado no Anúncio de Carpooling;

  (vii) a utilizar um veículo em boas condições de funcionamento e que cumpra as disposições legais aplicáveis, nomeadamente com um certificado de IPO atualizado;

  (viii) a apresentar à BlaBlaCar, ou a qualquer Passageiro que o solicite, a sua carta de condução, o seu certificado de matrícula, a apólice de seguro, o certificado de IPO e qualquer documento que comprove a sua capacidade para utilizar o veículo como Condutor na Plataforma;

  (ix) caso fique retido ou ocorra uma alteração à hora ou Viagem de Carpooling, a informar os seus Passageiros sem demora;

  (x) no caso de uma Viagem de Carpooling transfronteira, a ser portador e disponibilizar ao Passageiro e a qualquer autoridade que o solicite qualquer documento que comprove a sua identidade e o seu direito de atravessar a fronteira;

  (xi) a aguardar pelos Passageiros no local de encontro acordado durante, pelo menos, 15 minutos após a hora combinada;

  (xii) a não publicar um Anúncio de Carpooling relativo a um veículo que não lhe pertence ou que não está autorizado a utilizar para a partilha de transporte;

  (xiii) a certificar-se de que pode ser contactado telefonicamente pelos seus Passageiros pelo número registado no seu perfil;

  (xiv) a não gerar qualquer lucro através da Plataforma;

  (xv) a não ter qualquer contraindicação ou incapacidade médica para conduzir;

  (xvi) a comportar-se de forma apropriada e responsável durante a Viagem de Carpooling, e em conformidade com o espírito da partilha de transporte.

### **8.3. Compromissos dos Passageiros**

**8.3.1** **Para Viagem de Carpooling**

Quando utiliza a Plataforma como Passageiro de uma Viagem de Carpooling, compromete-se:

  (i) a adotar um comportamento apropriado durante a Viagem de Carpooling, de forma a não prejudicar a concentração ou a condução do Condutor ou a paz e sossego dos outros Passageiros;

  (ii) a respeitar o veículo do Condutor e a limpeza do mesmo;

  (iii) caso fique retido, a informar o Condutor sem demora;

  (iv) a aguardar pelo Condutor no local de encontro durante, pelo menos, 15 minutos para além da hora combinada;

  (v) a apresentar à BlaBlaCar, ou a qualquer Condutor que o solicite, o seu cartão de identidade ou qualquer documento que comprove a sua identidade;

  (vi) a não transportar, durante uma Viagem de Carpooling, qualquer artigo, bem, substância ou animal que possa prejudicar a condução e a concentração do Condutor, ou cuja natureza, posse ou transporte sejam contrários às disposições legais em vigor;

  (vii) no caso de uma Viagem de Carpooling transfronteira, a ser portador e disponibilizar ao Condutor e a qualquer autoridade que o solicite qualquer documento que comprove a sua identidade e o seu direito de atravessar a fronteira;

  (viii) a certificar-se de que pode ser contactado telefonicamente pelo seu Condutor pelo número registado no seu perfil, inclusive no ponto de encontro.

Caso tenha realizado uma Reserva para um ou mais Lugares em nome de terceiros, em conformidade com o estipulado no Artigo 4.2.3 supra, garante o respeito, por esses terceiros, das estipulações deste artigo e, em geral, dos T&C. A BlaBlaCar reserva-se o direito de suspender a sua Conta, limitar o seu acesso aos Serviços ou cessar estes T&C, em caso de violação pelo terceiro em cujo nome reservou um Lugar ao abrigo destes T&C, em conformidade com o ponto 9 das presentes T&C.

8.3.2 **Para Viagem de Autocarro**

No contexto de uma Viagem de Autocarro, o Passageiro compromete-se a cumprir com os T&CG de Venda do Operador de Autocarros relevante.

**8.4. Comunicação de conteúdos inadequados ou ilegais (mecanismo de notificação e ação)**

O utilizador pode denunciar um Conteúdo de Membro ou uma Mensagem suspeitos, inadequados ou ilegais, conforme descrito [aqui](https://support.blablacar.com/s/article/Denunciar-conte%C3%BAdo-ilegal-1729197120606?language=pt_PT).

A BlaBlaCar, após ter sido devidamente notificada nos termos da presente secção ou pelas autoridades competentes, removerá imediatamente qualquer Conteúdo de Membro ilegal se:

* O Conteúdo do Membro for obviamente ilegal ou contrário aos regulamentos aplicáveis; ou 
* A BlaBlaCar considerar que tal conteúdo viola estes T&Cs.

Nesses casos, a BlaBlaCar reserva-se o direito de remover o Conteúdo do Membro que foi devidamente denunciado, de forma suficientemente detalhada e clara, e/ou suspender imediatamente a Conta denunciada.

O Membro em questão pode recorrer das decisões da BlaBlaCar, conforme descrito na secção 15.1 abaixo.

**9\. Restrições relacionadas ao uso da Plataforma, suspensão de contas, limitação de acesso e cessação**
---------------------------------------------------------------------------------------------------------

Pode cessar as suas relações contratuais com a BlaBlaCar a qualquer momento, sem qualquer custo ou motivo. Para o fazer, basta entrar no separador “Encerrar a minha conta” da sua página de Perfil.

No caso de (i) violação dos presentes T&C, por sua parte, incluindo mas não limitado às suas obrigações como Membro mencionadas nos artigos 6.º e 8.º supra, (ii) que ultrapasse o limite estabelecido no Artigo 4.3.3 supra, ou (iii) se a BlaBlaCar tiver uma razão genuína para crer que tal é necessário para proteger a sua segurança e integridade e/ ou a segurança e integridade dos Membros ou de terceiros, ou por questões de prevenção de fraude ou investigações, a BlaBlaCar reserva-se o direito de:

  (i) cessar os T&C que o vinculam  à BlaBlaCar, de imediato e sem aviso; e/ou

  (ii) impedir a publicação ou remover qualquer Conteúdo de Membro por si na Plataforma; e/ou

  (iii) limitar o seu acesso à Plataforma, bem como o uso da mesma; e/ou

  (iv) suspender, temporária ou permanentemente, a sua Conta.

A suspensão da conta também pode significar, quando aplicável, que o utilizador não receberá os pagamentos pendentes.

Quando aplicável, a BlaBlaCar poderá também enviar-lhe um aviso lembrando-o da obrigação de cumprir as leis aplicáveis e/ou os presentes T&C.

Quando tal seja necessário,  será notificado do estabelecimento de tal medida de forma a ter a possibilidade de confirmar o seu compromisso de aderir a estes T&Cs e às leis aplicáveis,  de se explicar à BlaBlaCar ou contestar a sua decisão. A BlaBlaCar decidirá, levando em conta todas as circunstâncias de cada caso e a gravidade da infração, por sua própria iniciativa, se irá ou não anular as medidas implementadas.

**10\. Dados pessoais**
-----------------------

No contexto da utilização da Plataforma por parte do utilizador, a BlaBlaCar irá recolher e processar alguns dados pessoais deste tal como descrito na sua [Política de Privacidade](https://blog.blablacar.pt/about-us/privacy-policy). 

**11\. Propriedade intelectual**
--------------------------------

### **11.1. Conteúdo publicado pela BlaBlaCar**

Salvaguardando os conteúdos fornecidos pelos seus Membros, a BlaBlaCar é a única titular de todos os direitos de propriedade intelectual relacionados com o Serviço, a Plataforma, o seu conteúdo (nomeadamente textos, imagens, desenhos, logótipos, vídeos, sons, dados, gráficos) e com o software e as bases de dados que asseguram o seu funcionamento.

A BlaBlaCar concede-lhe um direito não exclusivo, pessoal e não transmissível para utilizar a Plataforma e os Serviços, para seu uso particular e pessoal, de forma não comercial e em conformidade com a finalidade da Plataforma e dos Serviços.

Está proibido de realizar qualquer outra utilização ou exploração da Plataforma e dos Serviços, e do respetivo conteúdo, sem a prévia permissão por escrito da BlaBlaCar. Nomeadamente, está proibido de:

  (i) reproduzir, modificar, adaptar, distribuir, representar e divulgar publicamente a Plataforma, os Serviços e o conteúdo, à exceção do expressamente autorizado pela BlaBlaCar;

  (ii) descompilar e utilizar engenharia reversa na Plataforma ou nos Serviços, salvaguardando as exceções estipuladas pelos textos em vigor;

  (iii) extrair ou tentar extrair (nomeadamente utilizando robôs de extração de dados ou qualquer outra ferramenta similar de recolha de dados) uma parte substancial dos dados da Plataforma.

### **11.2. Conteúdo publicado por si na Plataforma**

Para permitir a prestação dos Serviços, e em conformidade com a finalidade da Plataforma, concede à BlaBlaCar uma licença não exclusiva para utilizar o conteúdo e os dados por si fornecidos no contexto da sua utilização dos Serviços, que podem incluir os seus pedidos de Reserva, Anúncios de Carpooling e comentários nos mesmos, biografias, fotografias, avaliações e respostas a avaliações (doravante designados como o seu “**Conteúdo de Membro**”) e Mensagens. Para permitir à BlaBlaCar distribuir através da rede digital e de acordo com qualquer protocolo de comunicação (nomeadamente internet e rede móvel), e fornecer o conteúdo da Plataforma ao público, autoriza a BlaBlaCar, a reproduzir, representar, adaptar e traduzir o seu Conteúdo de Membro, para todo o mundo e durante todo o período das suas relações contratuais com a BlaBlaCar, da seguinte forma:

  (i)  autoriza a BlaBlaCar a reproduzir, no todo ou em parte, o seu Conteúdo de Membro em qualquer suporte de gravação digital, conhecido ou ainda desconhecido e, nomeadamente, em qualquer servidor, disco rígido, cartão de memória ou qualquer outro suporte equivalente, em qualquer formato e por qualquer processo, conhecido ou ainda desconhecido, no limite do necessário para qualquer operação de armazenamento, cópia de segurança, transmissão ou transferência associada à operação da Plataforma e à prestação do Serviço;

  (ii) autoriza a BlaBlaCar a adaptar e traduzir o seu Conteúdo de Membro, e a reproduzir tais adaptações em qualquer suporte digital, existente ou futuro, estipulado no ponto (i) supra, com o objetivo de fornecer os Serviços, particularmente em diferentes idiomas. Este direito inclui, nomeadamente, a opção de efetuar modificações à formatação do seu Conteúdo de Membro, respeitando o seu direito moral, para fins de respeitar a carta de gráficos da Plataforma e/ou de o tornar tecnicamente compatível com vista à sua publicação através da Plataforma.

O utilizador é responsável e tem todos os direitos sobre o Conteúdo dos Membros e as Mensagens que carrega na nossa Plataforma.

**12\. Função da BlaBlaCar**
----------------------------

A Plataforma constitui uma plataforma de networking online na qual os Membros podem criar e publicar Anúncios de Carpooling para Viagens de Carpooling para fins de partilha de transporte e reservar bilhetes de autocarro de Operadores de Autocarros. Esses Anúncios de Carpooling podem, nomeadamente, ser vistos pelos outros Membros para ficarem a saber os termos da Viagem e, se for o caso, para reservarem diretamente um Lugar no veículo em questão com o Membro que tiver publicado o Anúncio na Plataforma.  A Plataforma também permite a reserva de Lugares para Viagens de Autocarro.

Ao utilizar a Plataforma e aceitar os presentes T&C, reconhece que a BlaBlaCar não é parte em qualquer acordo celebrado entre si e os outros Membros com vista à partilha dos custos relacionados com uma Viagem nem qualquer acordo realizado entre si e o Operador de Autocarros.

A BlaBlaCar não tem qualquer controlo sobre o comportamento dos seus Membros, Operadores de Autocarros e respetivos agentes e utilizadores da Plataforma. Não detém, explora, fornece ou gere os veículos objeto dos Anúncios e não oferece nenhuma Viagem na Plataforma.

Reconhece e aceita que a BlaBlaCar não controla a validade, veracidade ou legalidade dos anúncios, Lugares e Viagens oferecidos. Na sua qualidade de intermediária na partilha de transporte, a BlaBlaCar não fornece nenhum serviço de transporte e não age na qualidade de transportadora; a função da BlaBlaCar limita-se a facilitar o acesso à Plataforma.

No contexto de Viagens de Carpooling, os Membros (Condutores ou Passageiros) atuam sob sua única e total responsabilidade.

Na sua qualidade de intermediária, a BlaBlaCar não pode ser responsabilizada pela ocorrência efetiva de uma Viagem ou uma viagem de autocarro, nomeadamente, devido a:

  (i) informação errada comunicada pelo Condutor ou Operador de Autocarros no seu Anúncio, ou por qualquer outro meio em relação a uma Viagem e respectivos termos;;

  (ii) cancelamento ou modificação de uma Viagem por um Membro ou por um pelo Operador de Autocarros;

  (iii) comportamento dos seus Membros durante, antes ou depois da Viagem ou de uma viagem de autocarro.

**13\. Operação, disponibilidade e funcionalidades da Plataforma**
------------------------------------------------------------------

A BlaBlaCar tentará, dentro do possível, manter a Plataforma acessível permanentemente – 24 horas por dia, sete dias por semana. Não obstante, o acesso à Plataforma poderá ser temporariamente suspenso, sem aviso, devido a operações de manutenção técnica, migração ou atualização ou devido a falhas ou limitações associadas ao funcionamento da rede.

Além disso, a BlaBlaCar reserva-se o direito de modificar ou suspender, no todo ou em parte, o acesso à Plataforma ou às suas funcionalidades, por sua única iniciativa, e de forma temporária ou permanente.

**14\. Modificação dos T&C**
----------------------------

Os presentes T&C e os documentos integrados por referência expressam o acordo total entre si e a BlaBlaCar em relação à utilização dos Serviços por sua parte. Qualquer outro documento, nomeadamente qualquer referência na Plataforma (Perguntas Frequentes, etc.), destina-se exclusivamente a fins de orientação.

A BlaBlaCar pode modificar os presentes T&C para os adaptar ao seu ambiente tecnológico e comercial, e para cumprir a legislação em vigor. Qualquer modificação destes T&C será publicada na Plataforma indicando a data de entrada em vigor, e será notificado pela BlaBlaCar antes da entrada em vigor da mesma.

**15\. Legislação aplicável – Litígio**
---------------------------------------

Os presentes T&C estão redigidos em Português e regem-se pela legislação portuguesa.

**15.1 Sistema interno de tratamento de queixas**

O utilizador pode recorrer das decisões que possamos tomar relativamente a:

* Conteúdo de Membro: por exemplo, removemos, restringimos a visibilidade ou recusámo-nos a remover qualquer Conteúdo de Membro que o utilizador forneça ao utilizar a Plataforma, ou

* A sua conta: suspendemos o seu acesso à Plataforma,

quando tomámos essas decisões com base no facto de o Conteúdo de Membro constituir um conteúdo ilegal ou ser incompatível com estes T&C. O processo de recurso é descrito [aqui](https://support.blablacar.com/s/article/Como-contestar-a-remo%C3%A7%C3%A3o-de-conte%C3%BAdo-ou-a-suspens%C3%A3o-da-conta-1729197123038?language=pt_PT).

**15.2 Resolução extrajudicial de litígios**

Pode também, se necessário, apresentar as suas reclamações em relação à nossa Plataforma ou aos nossos Serviços na plataforma de resolução de litígios colocada online pela Comissão Europeia, acessível [aqui](https://ec.europa.eu/consumers/odr/main/?event=main.home2.show). A Comissão Europeia enviará a sua reclamação para o provedor de justiça nacional competente. Em conformidade com as regras aplicáveis à mediação, antes de qualquer pedido de mediação, terá de ter notificado a BlaBlaCar, por escrito, de qualquer litígio de forma a obter uma solução amigável.

Pode também apresentar um pedido a uma entidade de resolução de litígios do seu país (pode encontrar a lista [aqui](https://ec.europa.eu/consumers/odr/main/?event=main.adr.show)). 

**16\. Avisos legais**
----------------------

A Plataforma é publicada pela Comuto SA, sociedade limitada com o capital social de 166,591.952 euros, registada no Registo Comercial de Paris sob o número 491.904.546 (número de IVA intracomunitário: FR76491904546), com sede social sita em 84, avenue de la République, 75011 Paris (França), representada pelo seu Diretor Executivo Nicolas Brusson, Diretor de Publicação do Sítio Web.

O sítio Web está alojado nos servidores da Google Ireland Limited sedeada em Gordon House, Barrow Street, Dublin 4 (Irlanda).

Comuto SA está registada no registo de Operadores de Viagens e Visitas ao abrigo do seguinte número: IM075180037

A garantia final é fornecida por: Groupama Assurance-Crédit & Caution, 8-10 rue d’Astorg, 75008 Paris – França

O seguro de responsabilidade civil profissional é subscrito a: Hiscox Europe Underwriting Limited, 19, rue Louis Le Grand, 75002 Paris – França.

Comuto SA está registada no registo de mediador de seguros, banca e financiamento ao abrigo do número registado (Orias) 15003890.

Para esclarecer qualquer dúvida, poderá contactar a Comuto SA utilizando este [formulário de contacto](https://support.blablacar.com/s/contactsupport?language=pt_PT).

**17\. Digital Services Act (Regulamento dos Serviços Digitais)**
-----------------------------------------------------------------

**17.1.  Informações sobre os destinatários activos médios mensais do serviço na União**

Em conformidade com o n.º 2 do artigo 24.º do Regulamento do Parlamento Europeu e do Conselho relativo a um mercado interno dos serviços digitais e que altera a Diretiva 2000/31/CE (“DSA”), os fornecedores de plataformas em linha são obrigados a publicar informações sobre a média mensal de destinatários activos do seu serviço na União, calculada como uma média dos últimos seis meses. O objetivo desta publicação é determinar se um fornecedor de plataformas em linha cumpre o critério de “plataformas em linha de muito grande dimensão” ao abrigo da DSA, ou seja, se excede o limiar de 45 milhões de destinatários ativos mensais médios na União.

Em 31 de janeiro de 2024, o número médio mensal de destinatários ativos da BlaBlaCar para o período entre agosto de 2023 e janeiro de 2024, calculado tendo em conta o considerando 77 e o artigo 3.º da DSA, antes da adoção de um ato delegado específico, era de aproximadamente 4,82 milhões na UE.

Esta informação é publicada apenas para efeitos de cumprimento dos requisitos da DSA e não deve ser utilizada para outros fins. Será actualizada, pelo menos, de seis em seis meses. A nossa abordagem para efetuar este cálculo pode evoluir ou exigir alterações ao longo do tempo, por exemplo, devido a alterações nos produtos ou a novas tecnologias.

**17.2. Ponto de contacto para as autoridades**

Em conformidade com o artigo 11.º do DSA, se for membro das autoridades competentes da UE, da Comissão Europeia ou do Conselho Europeu para os Serviços Digitais, pode contactar-nos relativamente a questões relacionadas com o DSA através do e-mail [\[email protected\]](https://blog.blablacar.pt/cdn-cgi/l/email-protection).

Pode contactar-nos em inglês e francês.

Note-se que este endereço de correio eletrónico não é utilizado para comunicações com os membros. Para quaisquer questões relacionadas com a utilização da BlaBlaCar, o Membro pode contactar-nos através deste [formulário de contacto](https://support.blablacar.com/s/contactsupport?language=pt_PT).